const cache = {
    keys: {
        app_version: 'APP_VERSION',
        masters: 'masters',
        calculator_save_data_status: 'calculator_save_data_status',
        calculator_auto_calculate: 'calculator_auto_calculate',
        calculator_master_commission: 'calculator_master_commission',
        store: {
            actions_results: 'store_actions_results_',
            arts: 'art_list',
            monitor_arts: 'monitor_art_list',
            lots_by_art: 'lots_for_',
            meta_by_art: 'meta_for_',
            user: 'current_user',
            categories: 'categories',
            marketCategories: 'marketCategories',
            sets: 'sets',
        },
        lots_table: 'lots_table_',
        monitor: {
            options: 'options_for_',
            arts_sorted: 'arts_sorted',
        },
        stat: {
            options: 'stat_options',
            filters: 'stat_filters',
        },
    },
};

export default cache;
