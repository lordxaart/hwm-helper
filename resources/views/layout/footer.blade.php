<footer class="bg-light">
	<div class="container">
		<div class="d-flex flex-column-reverse flex-sm-row justify-content-between">
			<span class="logo-text-light text-nowrap flex-fill">@ {{config('app.name')}} {{date('Y')}} <span class="text-muted small" v-tooltip="'Version'">{{ getLastVersion() }}</span></span>
			<div class="text-success ml-2">
				<a @click.prevent="$router.push('contacts')" href="{{route('contacts')}}">{{ __('app.main.contacts') }}</a>&nbsp;|&nbsp;
				<a @click.prevent="$router.push('privacy')" href="{{route('privacy')}}">{{ __('app.main.privacy') }}</a>&nbsp;|&nbsp;
				<a @click.prevent="$router.push('terms')" href="{{route('terms')}}">{{ __('app.main.terms') }}</a>
			</div>
		</div>
	</div>
</footer>
