(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["user-settings"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/pages/settings/NotificationsTab.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/components/pages/settings/NotificationsTab.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var lodash_find__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash/find */ "./node_modules/lodash/find.js");
/* harmony import */ var lodash_find__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash_find__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash/cloneDeep */ "./node_modules/lodash/cloneDeep.js");
/* harmony import */ var lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var JS_app_components_global_SelectArt__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! JS/app/components/global/SelectArt */ "./resources/assets/js/app/components/global/SelectArt.vue");
/* harmony import */ var JS_app_config__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! JS/app/config */ "./resources/assets/js/app/config/index.js");
/* harmony import */ var JS_app_utils_clipboard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! JS/app/utils/clipboard */ "./resources/assets/js/app/utils/clipboard.js");
/* harmony import */ var JS_app_utils_notify__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! JS/app/utils/notify */ "./resources/assets/js/app/utils/notify.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    SelectArt: JS_app_components_global_SelectArt__WEBPACK_IMPORTED_MODULE_3__["default"]
  },

  data() {
    return {
      subscriptionArtsList: [],
      settings: null
    };
  },

  methods: _objectSpread(_objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])('Arts', ['loadArts'])), Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])('User', ['updateSettings', 'checkTelegramToken', 'removeTelegramProfile'])), {}, {
    onSelectArtForNewLots(art) {
      if (!lodash_find__WEBPACK_IMPORTED_MODULE_1___default()(this.subscriptionArtsList, item => item.hwmId === art.hwmId)) {
        this.subscriptionArtsList.push(art);
      }
    },

    removeArtFromSubscripion(index) {
      if (index === 'all') {
        this.subscriptionArtsList = [];
      } else {
        this.subscriptionArtsList.splice(index, 1);
      }
    },

    changeNotification(key, channel) {
      const settings = {};
      settings[key] = {};
      settings[key][channel] = this.settings.notifications[key][channel];
      this.updateSettings(settings).then(() => {
        JS_app_utils_notify__WEBPACK_IMPORTED_MODULE_6__["default"].success(this.$t('app.main.successfuly_save'));
      });
    },

    deleteTelegramIntegration() {
      this.removeTelegramProfile().then(() => {
        JS_app_utils_notify__WEBPACK_IMPORTED_MODULE_6__["default"].success(this.$t('app.user.succes_delete_telegram_account'));
      });
    },

    sendRequestToCheckToken() {
      this.checkTelegramToken().then(() => {
        JS_app_utils_notify__WEBPACK_IMPORTED_MODULE_6__["default"].success(this.$t('app.user.success_connect_telegram_bot'));
      });
    },

    saveNewLotsSubscribe() {
      const lots = this.subscriptionArtsList.map(art => art.hwmId);
      this.updateSettings({
        subscription_new_lots: lots
      }).then(() => {
        JS_app_utils_notify__WEBPACK_IMPORTED_MODULE_6__["default"].success(this.$t('app.main.successfuly_save'));
        this.updateSubscriptionLots();
      });
    },

    copy(text) {
      Object(JS_app_utils_clipboard__WEBPACK_IMPORTED_MODULE_5__["default"])(text);
    },

    updateSubscriptionLots() {
      this.subscriptionArtsList = [];
      this.user.subscription_new_lots.forEach(hwmId => {
        const searchArt = this.arts.find(art => art.hwmId === hwmId);

        if (searchArt !== undefined) {
          this.subscriptionArtsList.push(searchArt);
        }
      });
    },

    canChangeNotification(key, channel) {
      if (channel === 'telegram' && !this.settings.telegram_chat_id) {
        return false;
      }

      return key !== 'default' && !this.settings.notifications.default[channel];
    }

  }),
  computed: _objectSpread(_objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('User', {
    user: 'getUser',
    loadingUpdateSettings: 'getLoadingUpdateSettings'
  })), Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('Arts', {
    arts: 'getArts',
    loadingArts: 'getLoadingArts'
  })), {}, {
    channels() {
      return JS_app_config__WEBPACK_IMPORTED_MODULE_4__["default"].app.notificationChannels;
    },

    instruction2Html() {
      return this.$t('app.user.telegram_instruction_2', {
        link: `<a href="${JS_app_config__WEBPACK_IMPORTED_MODULE_4__["default"].app.telegramBotLink}" target="_blank">${this.$t('app.main.chat')}</a>`,
        button: this.$t('app.user.check_token')
      });
    },

    canSaveNewLotsSubscribe() {
      return this.user.subscription_new_lots.join(',') !== this.subscriptionArtsList.map(art => art.hwmId).join(',');
    }

  }),
  watch: {
    user() {
      this.settings = lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_2___default()(this.user.settings);
    }

  },

  created() {
    this.settings = lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_2___default()(this.user.settings);
    this.loadArts().then(() => {
      this.updateSubscriptionLots();
    });
  }

});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/pages/settings/PasswordTab.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/components/pages/settings/PasswordTab.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('User', {
    user: 'getUser'
  }))
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/pages/settings/PersonalTab.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/components/pages/settings/PersonalTab.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  methods: {
    requestToIncreaseLimits() {//
    },

    preventDefault(event) {
      event.preventDefault();
    }

  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('User', {
    user: 'getUser'
  }))
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/pages/UserSettings.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/pages/UserSettings.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var JS_app_components_pages_settings_PersonalTab__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! JS/app/components/pages/settings/PersonalTab */ "./resources/assets/js/app/components/pages/settings/PersonalTab.vue");
/* harmony import */ var JS_app_components_pages_settings_PasswordTab__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! JS/app/components/pages/settings/PasswordTab */ "./resources/assets/js/app/components/pages/settings/PasswordTab.vue");
/* harmony import */ var JS_app_components_pages_settings_NotificationsTab__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! JS/app/components/pages/settings/NotificationsTab */ "./resources/assets/js/app/components/pages/settings/NotificationsTab.vue");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  metaInfo() {
    return {
      title: this.$t('app.user.account_settings')
    };
  },

  data() {
    return {
      tabs: [{
        id: 'personal',
        title: this.$t('app.user.personal_information'),
        component: JS_app_components_pages_settings_PersonalTab__WEBPACK_IMPORTED_MODULE_1__["default"]
      }, {
        id: 'password',
        title: this.$t('app.user.change_password'),
        component: JS_app_components_pages_settings_PasswordTab__WEBPACK_IMPORTED_MODULE_2__["default"]
      }, {
        id: 'notifications',
        title: this.$t('app.main.notifications'),
        component: JS_app_components_pages_settings_NotificationsTab__WEBPACK_IMPORTED_MODULE_3__["default"]
      }]
    };
  },

  methods: {//
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('User', {
    user: 'getUser',
    loadingUser: 'getLoadingUser',
    loadingUpdateSettings: 'getLoadingUpdateSettings'
  })), {}, {
    loading() {
      return this.loadingUser || this.loadingUpdateSettings;
    }

  })
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/pages/settings/NotificationsTab.vue?vue&type=style&index=0&id=8545cb16&scoped=true&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--5-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--5-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/components/pages/settings/NotificationsTab.vue?vue&type=style&index=0&id=8545cb16&scoped=true&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.nofity-list .list-group-item.default[data-v-8545cb16] {\n    padding: .75rem;\n    font-size: 130%;\n}\n.notify-channels > *[data-v-8545cb16] {\n    display: inline-block;\n    width: 50px;\n}\n.notify-channels > *[data-v-8545cb16]:not(:last-child) {\n    margin-right: 1rem;\n}\n.notifications > .card-body[data-v-8545cb16] {\n    overflow-x: auto;\n}\n.notifications .nofity-list[data-v-8545cb16] {\n    min-width: 500px;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/pages/UserSettings.vue?vue&type=style&index=0&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--5-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--5-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/pages/UserSettings.vue?vue&type=style&index=0&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.card-header-tabs .nav-link {\n    white-space: nowrap;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/pages/settings/NotificationsTab.vue?vue&type=style&index=0&id=8545cb16&scoped=true&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--5-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--5-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/components/pages/settings/NotificationsTab.vue?vue&type=style&index=0&id=8545cb16&scoped=true&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../../node_modules/css-loader??ref--5-1!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--5-2!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./NotificationsTab.vue?vue&type=style&index=0&id=8545cb16&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/pages/settings/NotificationsTab.vue?vue&type=style&index=0&id=8545cb16&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/pages/UserSettings.vue?vue&type=style&index=0&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--5-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--5-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/pages/UserSettings.vue?vue&type=style&index=0&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--5-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--5-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./UserSettings.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/pages/UserSettings.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/pages/settings/NotificationsTab.vue?vue&type=template&id=8545cb16&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/components/pages/settings/NotificationsTab.vue?vue&type=template&id=8545cb16&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "row" },
    [
      _c("loading", {
        attrs: { type: "block", show: _vm.loadingUpdateSettings }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "col-lg-7 col-12 form-group" }, [
        _c("div", { staticClass: "card notifications" }, [
          _c("div", { staticClass: "card-header d-flex align-items-center" }, [
            _vm._v(
              "\n                " +
                _vm._s(_vm.$t("app.user.settings.general_notifications")) +
                "\n            "
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "card-body" }, [
            _c(
              "ul",
              { staticClass: "list-group nofity-list" },
              [
                _c(
                  "li",
                  {
                    staticClass:
                      "list-group-item d-flex justify-content-between align-items-center"
                  },
                  [
                    _c("span", { staticClass: "font-weight-bold" }, [
                      _vm._v(
                        _vm._s(_vm.$t("app.user.settings.notification_type"))
                      )
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "notify-channels" },
                      _vm._l(_vm.channels, function(channel) {
                        return _c(
                          "div",
                          { key: channel },
                          [
                            _c("icon", {
                              attrs: {
                                name: channel,
                                size: "2x",
                                tooltip: _vm.$t(
                                  "app.notifications.channels." + channel
                                )
                              }
                            })
                          ],
                          1
                        )
                      }),
                      0
                    )
                  ]
                ),
                _vm._v(" "),
                _vm._l(_vm.settings.notifications, function(value, notify) {
                  return _c(
                    "li",
                    {
                      key: notify,
                      staticClass:
                        "list-group-item d-flex justify-content-between align-items-center",
                      class: notify === "default" ? "default bg-secondary" : ""
                    },
                    [
                      _c(
                        "span",
                        { staticClass: "font-weight-bold" },
                        [
                          _vm.$te("app.notifications.types_desc." + notify)
                            ? _c("icon", {
                                staticClass: "mr-1",
                                attrs: {
                                  name: "info",
                                  tooltip: _vm.$t(
                                    "app.notifications.types_desc." + notify
                                  )
                                }
                              })
                            : _vm._e(),
                          _vm._v(
                            "\n                            " +
                              _vm._s(
                                _vm.$t("app.notifications.types." + notify)
                              ) +
                              "\n                        "
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "notify-channels" },
                        _vm._l(_vm.channels, function(channel) {
                          return _c(
                            "div",
                            { key: channel },
                            [
                              _c("b-checkbox", {
                                directives: [
                                  {
                                    name: "tooltip",
                                    rawName: "v-tooltip",
                                    value: value[channel]
                                      ? _vm.$t("app.main.disable")
                                      : _vm.$t("app.main.enable"),
                                    expression:
                                      "(value[channel] ? $t('app.main.disable') : $t('app.main.enable'))"
                                  }
                                ],
                                attrs: {
                                  switch: "",
                                  disabled:
                                    _vm.canChangeNotification(
                                      notify,
                                      channel
                                    ) ||
                                    (channel == "telegram" &&
                                      !_vm.settings.telegram_chat_id),
                                  size: notify === "default" ? "lg" : ""
                                },
                                nativeOn: {
                                  change: function($event) {
                                    return _vm.changeNotification(
                                      notify,
                                      channel
                                    )
                                  }
                                },
                                model: {
                                  value: value[channel],
                                  callback: function($$v) {
                                    _vm.$set(value, channel, $$v)
                                  },
                                  expression: "value[channel]"
                                }
                              })
                            ],
                            1
                          )
                        }),
                        0
                      )
                    ]
                  )
                })
              ],
              2
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-lg-5 col-12 form-group" }, [
        _c("div", { staticClass: "card" }, [
          _c("div", { staticClass: "card-header" }, [
            _vm._v(
              "\n                " +
                _vm._s(_vm.$t("app.main.telegram")) +
                "\n            "
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "card-body" }, [
            _vm.settings.telegram_chat_id
              ? _c("div", { staticClass: "d-flex justify-content-between" }, [
                  _c("span", { staticClass: "text-muted" }, [
                    _vm._v(
                      "\n                        " +
                        _vm._s(_vm.settings.telegram_first_name) +
                        " " +
                        _vm._s(_vm.settings.telegram_last_name) +
                        " "
                    ),
                    _vm.settings.telegram_username
                      ? _c("span", [
                          _vm._v(
                            "[@" + _vm._s(_vm.settings.telegram_username) + "]"
                          )
                        ])
                      : _vm._e()
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "text-center d-inline-block ml-2" },
                    [
                      _c("c-button", {
                        attrs: {
                          type: "error",
                          size: "sm",
                          tooltip: _vm.$t("app.main.delete"),
                          icon: "delete"
                        },
                        on: { click: _vm.deleteTelegramIntegration }
                      })
                    ],
                    1
                  )
                ])
              : _c("div", { staticClass: "overflow-auto" }, [
                  _c("span", {}, [
                    _vm._v(_vm._s(_vm.$t("app.user.telegram_instruction_1")))
                  ]),
                  _vm._v(" "),
                  _c("span", {
                    domProps: { innerHTML: _vm._s(_vm.instruction2Html) }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "text-muted" }, [
                    _vm._v(
                      "(" +
                        _vm._s(
                          _vm.$t("app.user.telegram_instruction_3", {
                            minutes: 5
                          })
                        ) +
                        ")"
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "text-center mt-2" },
                    [
                      _c("icon", {
                        staticClass: "mr-2 curp hover",
                        attrs: {
                          name: "clipboard",
                          tooltip: _vm.$t("app.main.copy")
                        },
                        nativeOn: {
                          click: function($event) {
                            return _vm.copy(_vm.user.tokenForTelegramAuth)
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("code", { staticClass: "text-nowrap" }, [
                        _vm._v(_vm._s(_vm.user.tokenForTelegramAuth))
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "text-center mt-2" },
                    [
                      _c(
                        "c-button",
                        {
                          attrs: { size: "sm", type: "primary" },
                          on: { click: _vm.sendRequestToCheckToken }
                        },
                        [
                          _vm._v(
                            "\n                            " +
                              _vm._s(_vm.$t("app.user.check_token")) +
                              "\n                        "
                          )
                        ]
                      )
                    ],
                    1
                  )
                ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-lg-6 form-group" }, [
        _c("div", { staticClass: "card" }, [
          _c(
            "div",
            { staticClass: "card-header" },
            [
              _c("icon", {
                staticClass: "text-muted",
                attrs: {
                  name: "info",
                  tooltip: _vm.$t("app.user.settings.new_lots_info")
                }
              }),
              _vm._v(
                "\n                " +
                  _vm._s(_vm.$t("app.user.settings.new_lots")) +
                  "\n            "
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "card-body" },
            [
              _c("loading", {
                attrs: { type: "block", show: _vm.loadingArts }
              }),
              _vm._v(" "),
              _vm.subscriptionArtsList.length <
              _vm.user.settings.max_subscription_new_lots
                ? _c(
                    "div",
                    {
                      staticClass:
                        "d-flex justify-content-between align-items-center"
                    },
                    [
                      _c("span", { staticClass: "d-sm-block d-none mr-4" }, [
                        _vm._v(_vm._s(_vm.$t("app.main.add")) + ":")
                      ]),
                      _vm._v(" "),
                      _c("select-art", {
                        staticClass: "flex-grow-1 flex-fill",
                        attrs: { "clear-after-select": "" },
                        on: { "select-art": _vm.onSelectArtForNewLots }
                      })
                    ],
                    1
                  )
                : _c("p", { staticClass: "text-warning" }, [
                    _vm._v(
                      _vm._s(_vm.$t("app.user.settings.max_value")) +
                        ": " +
                        _vm._s(_vm.user.settings.max_subscription_new_lots)
                    )
                  ]),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _vm.subscriptionArtsList.length
                ? _c(
                    "div",
                    _vm._l(_vm.subscriptionArtsList, function(art, index) {
                      return _c(
                        "div",
                        {
                          key: art.hwmId,
                          staticClass: "d-inline-block mr-2 mb-2"
                        },
                        [
                          _c("img", {
                            directives: [
                              {
                                name: "tooltip",
                                rawName: "v-tooltip",
                                value: art.title,
                                expression: "art.title"
                              }
                            ],
                            staticClass: "art-img",
                            attrs: {
                              src: art.images.small.fullUrl,
                              alt: art.title
                            }
                          }),
                          _vm._v(" "),
                          _c("icon", {
                            attrs: {
                              name: "close",
                              tooltip: _vm.$t("app.main.delete")
                            },
                            nativeOn: {
                              click: function($event) {
                                return _vm.removeArtFromSubscripion(index)
                              }
                            }
                          })
                        ],
                        1
                      )
                    }),
                    0
                  )
                : _c("span", { staticClass: "text-muted" }, [
                    _vm._v(
                      _vm._s(_vm.$t("app.monitoring.list_of_arts_is_empty"))
                    )
                  ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "card-footer d-flex justify-content-between" },
            [
              _c(
                "c-button",
                {
                  staticClass: "flex-grow-1 mr-4",
                  attrs: {
                    block: "",
                    size: "sm",
                    type: "primary",
                    disabled: !_vm.canSaveNewLotsSubscribe
                  },
                  on: { click: _vm.saveNewLotsSubscribe }
                },
                [_vm._v(_vm._s(_vm.$t("app.main.save")))]
              ),
              _vm._v(" "),
              _c("c-button", {
                staticClass: "text-nowrap",
                attrs: {
                  size: "sm",
                  disabled: !_vm.subscriptionArtsList.length,
                  text: _vm.$t("app.main.clear_all"),
                  type: "error"
                },
                on: {
                  click: function($event) {
                    return _vm.removeArtFromSubscripion("all")
                  }
                }
              })
            ],
            1
          )
        ])
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/pages/settings/PasswordTab.vue?vue&type=template&id=74f14c7c&":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/components/pages/settings/PasswordTab.vue?vue&type=template&id=74f14c7c& ***!
  \****************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "form",
    {
      staticClass: "card form-lock-submit",
      attrs: { action: _vm.route("profile.updatePassword"), method: "POST" }
    },
    [
      _c("div", { staticClass: "card-body" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-4 mb-md-0 mb-4" }, [
            _c("label", { staticClass: "required" }, [
              _vm._v(
                "\n                    " +
                  _vm._s(_vm.$t("validation.attributes.old_password")) +
                  "\n                "
              )
            ]),
            _vm._v(" "),
            _c("input", {
              staticClass: "form-control form-control-sm",
              attrs: {
                type: "password",
                name: "old_password",
                required: _vm.user.hasPassword,
                disabled: !_vm.user.hasPassword
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4 mb-md-0 mb-4" }, [
            _c("label", { staticClass: "required" }, [
              _vm._v(
                "\n                    " +
                  _vm._s(_vm.$t("validation.attributes.password")) +
                  "\n                "
              )
            ]),
            _vm._v(" "),
            _c("input", {
              staticClass: "form-control form-control-sm",
              attrs: {
                type: "password",
                name: "password",
                minlength: "6",
                required: ""
              }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4 mb-0" }, [
            _c("label", { staticClass: "required" }, [
              _vm._v(
                "\n                    " +
                  _vm._s(
                    _vm.$t("validation.attributes.password_confirmation")
                  ) +
                  "\n                "
              )
            ]),
            _vm._v(" "),
            _c("input", {
              staticClass: "form-control form-control-sm",
              attrs: {
                type: "password",
                name: "password_confirmation",
                minlength: "6",
                required: ""
              }
            })
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "card-footer" }, [
        _c(
          "button",
          {
            staticClass: "btn btn-sm btn-primary btn-block",
            attrs: { type: "submit" }
          },
          [_vm._v(_vm._s(_vm.$t("app.main.refresh")))]
        )
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/pages/settings/PersonalTab.vue?vue&type=template&id=298a7a7d&":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/components/pages/settings/PersonalTab.vue?vue&type=template&id=298a7a7d& ***!
  \****************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "row" }, [
    _c("div", { staticClass: "col-md-6 mb-md-0 mb-4" }, [
      _c("div", { staticClass: "card" }, [
        _c("div", { staticClass: "card-header" }, [
          _vm._v(_vm._s(_vm.$t("app.user.settings.base")))
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card-body" }, [
          _c("ul", { staticClass: "list-group" }, [
            _c(
              "li",
              { staticClass: "list-group-item d-flex justify-content-between" },
              [
                _c("label", [
                  _vm._v(_vm._s(_vm.$t("validation.attributes.email")) + ":")
                ]),
                _vm._v(" "),
                _c(
                  "span",
                  {
                    staticClass:
                      "text-muted font-weight-bold ml-2 text-truncate notification-email"
                  },
                  [_vm._v(_vm._s(_vm.user.email))]
                )
              ]
            ),
            _vm._v(" "),
            _c(
              "li",
              {
                staticClass:
                  "list-group-item d-flex justify-content-between align-items-center"
              },
              [
                _c("label", { staticClass: "d-block d-md-inline" }, [
                  _vm._v(_vm._s(_vm.$t("app.user.social_networks")) + ":")
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "d-inline-block socials-blocks" },
                  _vm._l(_vm.user.socialProviders, function(provider) {
                    return _c(
                      "a",
                      {
                        directives: [
                          {
                            name: "tooltip",
                            rawName: "v-tooltip",
                            value: _vm.$t(
                              "app.user." +
                                (provider.active
                                  ? "social_already_connected"
                                  : "connect_social"),
                              { social: provider.title }
                            ),
                            expression:
                              "$t('app.user.' + (provider.active ? 'social_already_connected' : 'connect_social'), { social: provider.title })"
                          }
                        ],
                        key: provider.provider,
                        class:
                          "btn btn-link social-" +
                          provider.provider +
                          (provider.active ? " active" : ""),
                        attrs: {
                          href: !provider.active
                            ? _vm.route("profile.connectSocial", {
                                social: provider.provider
                              })
                            : "#"
                        },
                        on: {
                          click: function($event) {
                            provider.active ? _vm.preventDefault : ""
                          }
                        }
                      },
                      [
                        _c("icon", {
                          attrs: { name: provider.provider, size: "lg" }
                        })
                      ],
                      1
                    )
                  }),
                  0
                )
              ]
            )
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "col-md-6" }, [
      _c("div", { staticClass: "card" }, [
        _c(
          "div",
          { staticClass: "card-header d-flex justify-content-between" },
          [
            _vm._v(
              "\n                " +
                _vm._s(_vm.$t("app.user.settings.limits")) +
                "\n                "
            ),
            false
              ? undefined
              : _vm._e()
          ],
          1
        ),
        _vm._v(" "),
        _c("div", { staticClass: "card-body" }, [
          _c("ul", { staticClass: "list-group" }, [
            _c(
              "li",
              {
                staticClass:
                  "list-group-item d-flex justify-content-between align-items-center"
              },
              [
                _vm._v(
                  "\n                        " +
                    _vm._s(_vm.$t("app.user.arts_for_monitoring")) +
                    "\n                        "
                ),
                _c("span", [
                  _c(
                    "span",
                    {
                      directives: [
                        {
                          name: "tooltip",
                          rawName: "v-tooltip",
                          value: _vm.$t("app.user.settings.current_value"),
                          expression: "$t('app.user.settings.current_value')"
                        }
                      ],
                      staticClass: "badge badge-secondary mr-2"
                    },
                    [_vm._v(_vm._s(_vm.user.monitored_arts_count))]
                  ),
                  _vm._v(" "),
                  _c(
                    "span",
                    {
                      directives: [
                        {
                          name: "tooltip",
                          rawName: "v-tooltip",
                          value: _vm.$t("app.user.settings.max_value"),
                          expression: "$t('app.user.settings.max_value')"
                        }
                      ],
                      staticClass: "badge badge-primary"
                    },
                    [_vm._v(_vm._s(_vm.user.settings.max_monitored_arts))]
                  )
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "li",
              {
                staticClass:
                  "list-group-item d-flex justify-content-between align-items-center"
              },
              [
                _vm._v(
                  "\n                        " +
                    _vm._s(_vm.$t("app.user.filters_for_monitoring")) +
                    "\n                        "
                ),
                _c("span", [
                  _c(
                    "span",
                    {
                      directives: [
                        {
                          name: "tooltip",
                          rawName: "v-tooltip",
                          value: _vm.$t("app.user.settings.current_value"),
                          expression: "$t('app.user.settings.current_value')"
                        }
                      ],
                      staticClass: "badge badge-secondary mr-2"
                    },
                    [_vm._v(_vm._s(_vm.user.monitored_filters_count))]
                  ),
                  _vm._v(" "),
                  _c(
                    "span",
                    {
                      directives: [
                        {
                          name: "tooltip",
                          rawName: "v-tooltip",
                          value: _vm.$t("app.user.settings.max_value"),
                          expression: "$t('app.user.settings.max_value')"
                        }
                      ],
                      staticClass: "badge badge-primary"
                    },
                    [_vm._v(_vm._s(_vm.user.settings.max_monitored_filters))]
                  )
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "li",
              {
                staticClass:
                  "list-group-item d-flex justify-content-between align-items-center"
              },
              [
                _vm._v(
                  "\n                        " +
                    _vm._s(_vm.$t("app.user.lots_for_subscribe")) +
                    "\n                        "
                ),
                _c("span", [
                  _c(
                    "span",
                    {
                      directives: [
                        {
                          name: "tooltip",
                          rawName: "v-tooltip",
                          value: _vm.$t("app.user.settings.current_value"),
                          expression: "$t('app.user.settings.current_value')"
                        }
                      ],
                      staticClass: "badge badge-secondary mr-2"
                    },
                    [_vm._v(_vm._s(_vm.user.subscription_new_lots.length))]
                  ),
                  _vm._v(" "),
                  _c(
                    "span",
                    {
                      directives: [
                        {
                          name: "tooltip",
                          rawName: "v-tooltip",
                          value: _vm.$t("app.user.settings.max_value"),
                          expression: "$t('app.user.settings.max_value')"
                        }
                      ],
                      staticClass: "badge badge-primary"
                    },
                    [
                      _vm._v(
                        _vm._s(_vm.user.settings.max_subscription_new_lots)
                      )
                    ]
                  )
                ])
              ]
            )
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/pages/UserSettings.vue?vue&type=template&id=00e82d79&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/pages/UserSettings.vue?vue&type=template&id=00e82d79& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c(
      "div",
      { staticClass: "user-settings" },
      [
        _c(
          "div",
          {
            staticClass:
              "mb-4 d-flex justify-content-between flex-sm-row flex-column align-items-center"
          },
          [
            _c("div", { staticClass: "align-middle" }, [
              _c("h1", { staticClass: "d-inline-block" }, [
                _vm._v(_vm._s(_vm.$t("app.user.account_settings")))
              ])
            ])
          ]
        ),
        _vm._v(" "),
        _c(
          "b-card",
          { attrs: { "no-body": "" } },
          [
            _c("loading", { attrs: { type: "block", show: _vm.loading } }),
            _vm._v(" "),
            _vm.user.id
              ? _c(
                  "b-tabs",
                  { attrs: { card: "", lazy: "", justified: "" } },
                  _vm._l(_vm.tabs, function(tab) {
                    return _c(
                      "b-tab",
                      {
                        key: tab.id,
                        attrs: {
                          id: tab.id,
                          title: tab.title,
                          active: _vm.tabIsActive("#" + tab.id)
                        },
                        on: { click: _vm.clickOnTab }
                      },
                      [_c(tab.component, { tag: "component" })],
                      1
                    )
                  }),
                  1
                )
              : _vm._e()
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/assets/js/app/components/pages/settings/NotificationsTab.vue":
/*!********************************************************************************!*\
  !*** ./resources/assets/js/app/components/pages/settings/NotificationsTab.vue ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _NotificationsTab_vue_vue_type_template_id_8545cb16_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./NotificationsTab.vue?vue&type=template&id=8545cb16&scoped=true& */ "./resources/assets/js/app/components/pages/settings/NotificationsTab.vue?vue&type=template&id=8545cb16&scoped=true&");
/* harmony import */ var _NotificationsTab_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./NotificationsTab.vue?vue&type=script&lang=js& */ "./resources/assets/js/app/components/pages/settings/NotificationsTab.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _NotificationsTab_vue_vue_type_style_index_0_id_8545cb16_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./NotificationsTab.vue?vue&type=style&index=0&id=8545cb16&scoped=true&lang=css& */ "./resources/assets/js/app/components/pages/settings/NotificationsTab.vue?vue&type=style&index=0&id=8545cb16&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _NotificationsTab_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _NotificationsTab_vue_vue_type_template_id_8545cb16_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _NotificationsTab_vue_vue_type_template_id_8545cb16_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "8545cb16",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/app/components/pages/settings/NotificationsTab.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/app/components/pages/settings/NotificationsTab.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************!*\
  !*** ./resources/assets/js/app/components/pages/settings/NotificationsTab.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NotificationsTab_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./NotificationsTab.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/pages/settings/NotificationsTab.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NotificationsTab_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/app/components/pages/settings/NotificationsTab.vue?vue&type=style&index=0&id=8545cb16&scoped=true&lang=css&":
/*!*****************************************************************************************************************************************!*\
  !*** ./resources/assets/js/app/components/pages/settings/NotificationsTab.vue?vue&type=style&index=0&id=8545cb16&scoped=true&lang=css& ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_NotificationsTab_vue_vue_type_style_index_0_id_8545cb16_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/style-loader!../../../../../../../node_modules/css-loader??ref--5-1!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/postcss-loader/src??ref--5-2!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./NotificationsTab.vue?vue&type=style&index=0&id=8545cb16&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/pages/settings/NotificationsTab.vue?vue&type=style&index=0&id=8545cb16&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_NotificationsTab_vue_vue_type_style_index_0_id_8545cb16_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_NotificationsTab_vue_vue_type_style_index_0_id_8545cb16_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_NotificationsTab_vue_vue_type_style_index_0_id_8545cb16_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_NotificationsTab_vue_vue_type_style_index_0_id_8545cb16_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/assets/js/app/components/pages/settings/NotificationsTab.vue?vue&type=template&id=8545cb16&scoped=true&":
/*!***************************************************************************************************************************!*\
  !*** ./resources/assets/js/app/components/pages/settings/NotificationsTab.vue?vue&type=template&id=8545cb16&scoped=true& ***!
  \***************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NotificationsTab_vue_vue_type_template_id_8545cb16_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./NotificationsTab.vue?vue&type=template&id=8545cb16&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/pages/settings/NotificationsTab.vue?vue&type=template&id=8545cb16&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NotificationsTab_vue_vue_type_template_id_8545cb16_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NotificationsTab_vue_vue_type_template_id_8545cb16_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/assets/js/app/components/pages/settings/PasswordTab.vue":
/*!***************************************************************************!*\
  !*** ./resources/assets/js/app/components/pages/settings/PasswordTab.vue ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PasswordTab_vue_vue_type_template_id_74f14c7c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PasswordTab.vue?vue&type=template&id=74f14c7c& */ "./resources/assets/js/app/components/pages/settings/PasswordTab.vue?vue&type=template&id=74f14c7c&");
/* harmony import */ var _PasswordTab_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PasswordTab.vue?vue&type=script&lang=js& */ "./resources/assets/js/app/components/pages/settings/PasswordTab.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PasswordTab_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PasswordTab_vue_vue_type_template_id_74f14c7c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PasswordTab_vue_vue_type_template_id_74f14c7c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/app/components/pages/settings/PasswordTab.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/app/components/pages/settings/PasswordTab.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************!*\
  !*** ./resources/assets/js/app/components/pages/settings/PasswordTab.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PasswordTab_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./PasswordTab.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/pages/settings/PasswordTab.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PasswordTab_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/app/components/pages/settings/PasswordTab.vue?vue&type=template&id=74f14c7c&":
/*!**********************************************************************************************************!*\
  !*** ./resources/assets/js/app/components/pages/settings/PasswordTab.vue?vue&type=template&id=74f14c7c& ***!
  \**********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PasswordTab_vue_vue_type_template_id_74f14c7c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./PasswordTab.vue?vue&type=template&id=74f14c7c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/pages/settings/PasswordTab.vue?vue&type=template&id=74f14c7c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PasswordTab_vue_vue_type_template_id_74f14c7c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PasswordTab_vue_vue_type_template_id_74f14c7c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/assets/js/app/components/pages/settings/PersonalTab.vue":
/*!***************************************************************************!*\
  !*** ./resources/assets/js/app/components/pages/settings/PersonalTab.vue ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PersonalTab_vue_vue_type_template_id_298a7a7d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PersonalTab.vue?vue&type=template&id=298a7a7d& */ "./resources/assets/js/app/components/pages/settings/PersonalTab.vue?vue&type=template&id=298a7a7d&");
/* harmony import */ var _PersonalTab_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PersonalTab.vue?vue&type=script&lang=js& */ "./resources/assets/js/app/components/pages/settings/PersonalTab.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PersonalTab_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PersonalTab_vue_vue_type_template_id_298a7a7d___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PersonalTab_vue_vue_type_template_id_298a7a7d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/app/components/pages/settings/PersonalTab.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/app/components/pages/settings/PersonalTab.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************!*\
  !*** ./resources/assets/js/app/components/pages/settings/PersonalTab.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PersonalTab_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./PersonalTab.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/pages/settings/PersonalTab.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PersonalTab_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/app/components/pages/settings/PersonalTab.vue?vue&type=template&id=298a7a7d&":
/*!**********************************************************************************************************!*\
  !*** ./resources/assets/js/app/components/pages/settings/PersonalTab.vue?vue&type=template&id=298a7a7d& ***!
  \**********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PersonalTab_vue_vue_type_template_id_298a7a7d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./PersonalTab.vue?vue&type=template&id=298a7a7d& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/pages/settings/PersonalTab.vue?vue&type=template&id=298a7a7d&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PersonalTab_vue_vue_type_template_id_298a7a7d___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PersonalTab_vue_vue_type_template_id_298a7a7d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/assets/js/app/pages/UserSettings.vue":
/*!********************************************************!*\
  !*** ./resources/assets/js/app/pages/UserSettings.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _UserSettings_vue_vue_type_template_id_00e82d79___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UserSettings.vue?vue&type=template&id=00e82d79& */ "./resources/assets/js/app/pages/UserSettings.vue?vue&type=template&id=00e82d79&");
/* harmony import */ var _UserSettings_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UserSettings.vue?vue&type=script&lang=js& */ "./resources/assets/js/app/pages/UserSettings.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _UserSettings_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./UserSettings.vue?vue&type=style&index=0&lang=css& */ "./resources/assets/js/app/pages/UserSettings.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _UserSettings_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _UserSettings_vue_vue_type_template_id_00e82d79___WEBPACK_IMPORTED_MODULE_0__["render"],
  _UserSettings_vue_vue_type_template_id_00e82d79___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/app/pages/UserSettings.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/app/pages/UserSettings.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/assets/js/app/pages/UserSettings.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UserSettings_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./UserSettings.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/pages/UserSettings.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_UserSettings_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/app/pages/UserSettings.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************!*\
  !*** ./resources/assets/js/app/pages/UserSettings.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UserSettings_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--5-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--5-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./UserSettings.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/pages/UserSettings.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UserSettings_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UserSettings_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UserSettings_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_5_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_5_2_node_modules_vue_loader_lib_index_js_vue_loader_options_UserSettings_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/assets/js/app/pages/UserSettings.vue?vue&type=template&id=00e82d79&":
/*!***************************************************************************************!*\
  !*** ./resources/assets/js/app/pages/UserSettings.vue?vue&type=template&id=00e82d79& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserSettings_vue_vue_type_template_id_00e82d79___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./UserSettings.vue?vue&type=template&id=00e82d79& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/pages/UserSettings.vue?vue&type=template&id=00e82d79&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserSettings_vue_vue_type_template_id_00e82d79___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserSettings_vue_vue_type_template_id_00e82d79___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);