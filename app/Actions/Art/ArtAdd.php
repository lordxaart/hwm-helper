<?php

declare(strict_types=1);

namespace App\Actions\Art;

use App\Events\HWM\ArtAddedEvent;
use App\Models\Art;
use Illuminate\Console\Command;
use Lorisleiva\Actions\Concerns\AsAction;

/**
 * Class ArtAdd
 * @package App\Actions\Art
 * @property string $title
 */
class ArtAdd
{
    use AsAction;

    public string $commandSignature = 'hwm:art:add {art_id : Art Hwm ID}';
    public string $commandDescription = 'Add art by :art_id';

    /**
     * @throws \App\HWM\Exceptions\HwmException
     */
    public function asCommand(Command $command): int
    {
        $art = $this->handle(strval($command->argument('art_id')));

        $command->info("Art {$art->getTitle()} was successfully added");

        return 0;
    }

    /**
     * @throws \App\HWM\Exceptions\HwmException
     */
    public function handle(string $artId): Art
    {
        if ($art = Art::findByHwmId($artId)) {
            return $art;
        }

        $hwm = hwmClient();
        $art = $hwm->getArtInfo($artId);
        $data = $art->toArray();
        $data['original_image'] = $art->image;
        unset($data['image']);

        $art = Art::create(array_merge($data, ['hwm_id' => $artId]));
        $art->title = '';
        $art->setTranslation('title', $hwm->getLocale(), $data['title']);

        event(new ArtAddedEvent($art));

        return $art;
    }
}
