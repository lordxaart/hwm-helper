<?php

return [
    'allowed_config' => [
        'user.max_monitored_arts',
        'view.sprite_key',
        'app.telegramBotLink',
    ],
];
