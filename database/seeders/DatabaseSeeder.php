<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        $this->importantSeeds();

        if (isProdEnv()) {
            $this->prod();
        }

        if (isDevEnv()) {
            $this->dev();
        }

        if (isLocalEnv()) {
            $this->local();
        }

        if (isTestEnv()) {
            $this->testing();
        }
    }

    private function prod()
    {
        //
    }

    private function dev()
    {
        $this->developmentSeeds();

        $this->runCronTasks();
    }

    private function local()
    {
        $this->developmentSeeds();

        $this->runCronTasks();
    }

    private function testing()
    {
        //
    }

    private function importantSeeds()
    {
        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(SuperAdminSeeder::class);
        $this->call(UserFeaturesSeeder::class);
        $this->call(SubscriptionsSeeder::class);
        $this->call(HwmEntitiesSeeder::class);
    }

    private function developmentSeeds()
    {
        $this->call(UsersSeeder::class);
        $this->call(CharactersSeeder::class);
        $this->call(ElementPricesSeeder::class);
        // $this->call(MonitoredArtsAndFiltersSeeder::class);
        // $this->call(LotsSeeder::class);
    }

    private function runCronTasks()
    {
//        $this->command->info('Start generate art images sprite');
//        \App\Jobs\RunArtisanCommand::dispatch('hwm:art:generate:sprite');
//
//        $this->command->info('Start updated elements prices');
//        \App\Jobs\RunArtisanCommand::dispatch('hwm:elements:parse_price');
//
//        $this->command->info('Start updated sets list');
//        \App\Jobs\RunArtisanCommand::dispatch('hwm:set:update:from_market');
//
//        $this->command->info('Start updated arts list');
//        \App\Jobs\RunArtisanCommand::dispatch('hwm:art:update:from_market');
    }
}
