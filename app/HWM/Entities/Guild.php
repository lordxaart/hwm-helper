<?php

declare(strict_types=1);

namespace App\HWM\Entities;

use App\HWM\Entities\Interfaces\HasRepository;
use App\HWM\Repositories\GuildRepository;

class Guild extends HwmEntity implements HasRepository
{
    public static function expToNextLevel(int|string $key, string $experience): int
    {
        // TODO
        return 0;
    }

    public function getRepository(): GuildRepository
    {
        return app(GuildRepository::class);
    }
}
