<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alias')->unique();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::create('subscription_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alias');
            $table->unsignedInteger('subscription_id');
            $table->string('name')->nullable();
            $table->string('duration'); // ISO 8601 (duration) https://tc39.es/proposal-temporal/docs/duration.html
            $table->string('trial')->nullable(); // ISO 8601 (duration)
            $table->unsignedInteger('price')->nullable();
            $table->char('currency', 3)->default('USD');
            $table->timestamps();

            $table->unique(['alias', 'subscription_id']);

            $table->foreign('subscription_id')
                ->on('subscriptions')
                ->references('id')
                ->onDelete('cascade');
        });

        Schema::create('user_subscriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('subscription_id');
            $table->unsignedInteger('subscription_plan_id');
            $table->timestamp('started_at');
            $table->timestamp('trial_ended_at')->nullable();
            $table->timestamp('ended_at');

            $table->unique(['user_id', 'subscription_id', 'started_at']);
            $table->index('ended_at');

            $table->foreign('subscription_id')
                ->on('subscriptions')
                ->references('id')
                ->onDelete('cascade');

            $table->foreign('subscription_plan_id')
                ->on('subscription_plans')
                ->references('id')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('subscriptions');
        Schema::dropIfExists('subscription_plans');
        Schema::dropIfExists('user_subscriptions');
        Schema::enableForeignKeyConstraints();
    }
};
