<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

class EnableAdsenseAds extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('general.enable_adsense_script', false);
    }
}
