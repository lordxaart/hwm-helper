<tr>
    <td class="header navbar-dark">
        <a href="{{ $url }}" class="navbar-brand">
            <img src="{{ assetEmail('images/logo/logo_32x32.png') }}" alt="Logo" class="d-inline-block align-top mr-3">
            <span class="d-inline-block logo-text">{{ $slot }}</span>
        </a>
    </td>
</tr>
