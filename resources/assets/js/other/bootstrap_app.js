// eslint-disable-next-line no-unused-vars
const initLoadedResources = (MIX_CSS, MIX_MANIFEST, MIX_VENDOR, MIX_APP, HIDE_PAGE_LOADER) => {
    const preloader = '.page-preloader';
    const contentLoader = '.content-loader';

    const loadedResources = [];
    const resources = {
        // eslint-disable-next-line no-undef
        css: MIX_CSS,
        // eslint-disable-next-line no-undef
        manifest: MIX_MANIFEST,
        // eslint-disable-next-line no-undef
        vendor: MIX_VENDOR,
        // eslint-disable-next-line no-undef
        app: MIX_APP,
    };

    const timers = [];

    const hidePreload = (selector, force = false) => {
        const element = document.querySelector(selector);

        if (element && force) {
            element.remove();
        } else if (element && timers.indexOf(element.id) === -1) {
            element.classList.add('fade-out');
            setTimeout(() => {
                const el = document.querySelector(selector);
                if (el) {
                    el.remove();
                }
            }, 200);
            timers.push(element.id);
        }
    };

    const loadResource = (src) => {
        loadedResources.push(src);
    };

    const addResource = (src, attrs = {}) => new Promise((resolve) => {
        let tag;
        if (src.indexOf('.css') !== -1) {
            tag = document.createElement('link');
            tag.setAttribute('href', src);
            tag.setAttribute('rel', 'stylesheet');
            tag.setAttribute('type', 'text/css');
        }
        if (src.indexOf('.js') !== -1) {
            tag = document.createElement('script');
            tag.setAttribute('src', src);
        }

        if (tag) {
            Object.keys(attrs).forEach((key) => {
                if (attrs[key] !== null) {
                    tag.setAttribute(key, attrs[key]);
                } else {
                    tag.setAttribute(key, '');
                }
            });

            tag.onload = () => {
                loadResource(src);
                resolve(src);
            };

            tag.onerror = () => {
                resolve(src);
            };

            if (src.indexOf('.css') !== -1) {
                document.head.appendChild(tag);
            } else {
                document.body.appendChild(tag);
            }
        }
    });

    const run = async () => {
        addResource(resources.manifest, { async: null });
        await addResource(resources.vendor, { async: null });
        await addResource(resources.app, { defer: null, async: null, id: 'app-script' });

        hidePreload(contentLoader);
    };

    try {
        document.querySelector(preloader).remove();
    } catch (error) {
        //
    }

    // eslint-disable-next-line no-undef
    if (HIDE_PAGE_LOADER) {
        addResource(resources.css).then(() => {
            hidePreload(contentLoader);
        });
    } else {
        addResource(resources.css).then(() => {
            hidePreload(preloader);
        });
    }

    run();

    setTimeout(() => {
        hidePreload(preloader, true);
        hidePreload(contentLoader, true);
    }, 15000);
};
