<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('logs_block_enemies_requests', function (Blueprint $table) {
            $table->id();
            $table->string('ip');
            $table->unsignedInteger('user_id')->nullable();
            $table->char('country', 2);
            $table->string('url', 500);
            $table->timestamp('created_at', 6)->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('table_logs_block_enemies');
    }
};
