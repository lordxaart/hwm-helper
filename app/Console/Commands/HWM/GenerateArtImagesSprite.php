<?php

declare(strict_types=1);

namespace App\Console\Commands\HWM;

use App\Components\ImageSprite\ImageSpriteGenerator;
use App\Console\Commands\Command;
use App\Models\Art;
use Illuminate\Support\Facades\Storage;

class GenerateArtImagesSprite extends Command
{
    public const KEY_STORAGE_PATH = 'sprite_key.txt';

    public string $prefix = '_small';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hwm:art:generate:sprite';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate sprite png and css from arts images';

    /**
     * @throws \Exception
     */
    public function handle(): void
    {
        $this->generateWithPrefix($this->prefix);

        $this->info('Success generated sprite for arts. Size - ' . $this->prefix);
    }

    /**
     * @throws \Exception
     */
    public function generateWithPrefix(string $prefix = null): void
    {
        $this->info('Start generate sprite ' . $this->prefix);

        $uniqid = uniqid();

        $files = ['default' => public_path("images/art_default$prefix.png")];

        Art::all()->each(function (Art $art) use (&$files, $prefix) {
            $art->handleImages();
            $files[$art->hwm_id] = $art->images[str_replace('_', '', $prefix ?: '')]->getRealPath();
        });

        $this->info('Checked exists images for all arts');

        $generator = new ImageSpriteGenerator($files);
        $width = 30;
        $generator->maxSpriteWidth = $width * 28;
        $generator->spriteImagePath = Storage::path("sprite$prefix.png");
        $generator->spriteCssPath = Storage::path("sprite$prefix.css");
        $generator->cssPrefix = "art$prefix-";
        $generator->customStyle = "
        .art-img-bg {
            width: {$width}px;
            height: {$width}px;
            background-image: url('/storage/sprite_small.png?$uniqid');
        }
        .art-img-sm {
            width: {$width}px;
        }
        ";
        $generator->generate()->optimize();

        Storage::put(self::KEY_STORAGE_PATH, $uniqid);
    }
}
