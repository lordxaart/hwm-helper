<?php

return [
    'rules' => [
        'max_login_length' => 100,
        'max_hwm_id_length' => 45,
        'max_art_title_length' => 100,
    ],
    // Default master, used in calculator as default
    'default_master' => [
        'rate' => 100,
        'repair' => 90,
        'commission' => 0,
    ],

    'last_lot_id' => 108769162, // 15-06-2021 15:53:00

    'default_character' => [
        'hwm_id' => env('HWM_ID'),
        'login' => env('HWM_LOGIN'),
        'password' => env('HWM_PASSWORD'),
    ],
    'second_character' => [
        'hwm_id' => env('HWM_ID_2', env('HWM_ID')),
        'login' => env('HWM_LOGIN_2', env('HWM_LOGIN')),
        'password' => env('HWM_PASSWORD_2', env('HWM_PASSWORD')),
    ],

    'use_proxy' => false,

    'gold_img' => 'https://dcdn2.heroeswm.ru/i/r/48/gold.png',
    'not_exist_lots' => [
        94784991,
    ],
];
