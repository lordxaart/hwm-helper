<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLotParserLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lot_parser_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('parser_id');
            $table->string('parser_type');
            $table->unsignedInteger('lot_id');
            $table->string('state');
            $table->tinyInteger('attempts')->default(0);
            $table->string('time_execute')->nullable();
            $table->string('server_ip')->nullable();
            $table->text('errors')->nullable();

            $table->timestamps(6);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lot_parser_logs');
    }
}
