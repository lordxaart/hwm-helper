<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V1;

use App\Exceptions\ClientError;
use App\Http\Requests\API\FeedbackStore;
use App\Http\Requests\API\UpdateUserSettings;
use App\Http\Resources\UserResource;
use App\Models\Feedback;
use App\Models\Objects\UserSettings;
use App\Models\User;
use App\Services\Notifications\Enums\NotifyType;
use App\Services\Telegram\Entities\User as TelegramUser;
use App\Services\Telegram\UserTelegramService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends ApiController
{
    public function index()
    {
        return $this->response(new UserResource(Auth::user()));
    }

    public function connectTelegram(Request $request)
    {
        try {
             /** @var User $user */
            $user = $request->user();

            $chatId = (int) $request->query('chatId');
            $from = (array) $request->query('from');

            $telegramUser = new TelegramUser($from);


            app(UserTelegramService::class)->connectTelegramToUser($user, $chatId, $telegramUser);
        } catch (\Throwable $e) {
            \Log::error($e->getMessage());

            return redirect()
                ->route('profile.settings')
                ->withError(trans('app.user.error_during_connect_telegram_bot'));
        }


        return redirect()
            ->route('profile.settings')
            ->with('success', trans('app.user.success_connect_telegram_bot'))
            ->withFragment('#notifications');
    }

    /**
     * @throws ClientError
     */
    public function disconnectTelegram(UserTelegramService $userTelegramService)
    {
        /** @var User $user */
        $user = Auth::user();

        $userTelegramService->disconnectTelegram($user);

        return $this->response(new UserResource($user));
    }

    public function updateNotificationsSettings(UpdateUserSettings $request)
    {
        /** @var User $user */
        $user = Auth::user();

        $user->settings->setNotifications($request->except([NotifyType::IMPORTANT->value]));

        $user->save();

        return $this->response(new UserResource($user));
    }

    public function updateSettings(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();

        $gameLink = $request->get(UserSettings::GAME_LINK);
        $lang = $request->get(UserSettings::LANG);
        $gameSettings = $request->get(UserSettings::GAME_SETTINGS);

        if ($gameLink) {
            $user->settings([UserSettings::GAME_LINK => $gameLink]);
        }

        if ($lang) {
            $user->settings([UserSettings::LANG => $lang]);
        }

        if ($gameSettings && $user->features()->can_edit_game_settings) {
            $user->settings([UserSettings::GAME_SETTINGS => $gameSettings]);
        }

        return $this->response(new UserResource($user));
    }

    public function sendFeedback(FeedbackStore $request)
    {
        $feedback = Feedback::customCreate($request->all());
        $feedback->sendToAdmin();

        // store request
        return $this->response([], 201);
    }
}
