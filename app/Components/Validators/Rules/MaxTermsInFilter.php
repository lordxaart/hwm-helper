<?php

declare(strict_types=1);

namespace App\Components\Validators\Rules;

use App\Services\LotFilter\ValueObjects\FilterTerm;
use Illuminate\Contracts\Validation\Rule;

class MaxTermsInFilter implements Rule
{
    public int $maxTerms;

    public function __construct(int $maxTerms)
    {
        $this->maxTerms = $maxTerms;
    }
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        try {
            $array = [];
            if (is_array($value)) {
                foreach ($value as $item) {
                    if (is_array($item)) {
                        $array[] = FilterTerm::parseItemFromArray($item);
                    } else {
                        $array[] = FilterTerm::parseItemFromString($item);
                    }
                }
            } else {
                $array = FilterTerm::parserCollectionFromString($value)->toArray();
            }
        } catch (\InvalidArgumentException) {
            return false;
        }

        return !empty($array) && count($array) <= $this->maxTerms;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return trans('validation.max_terms_in_filter', ['max' => $this->maxTerms]);
    }
}
