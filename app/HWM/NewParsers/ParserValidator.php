<?php

declare(strict_types=1);

namespace App\HWM\NewParsers;

use App\HWM\Entities\Character;
use App\HWM\Entities\Lot;
use App\HWM\Objects\LotOperation;
use App\HWM\Repositories\ArtCategoryRepository;
use App\HWM\Repositories\MarketCategoryRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class ParserValidator
{
    private array $rules;

    private array $config;

    public function __construct()
    {
        $this->initConfig();

        $this->initRules();
    }

    /**
     * @throw ParserValidationException
     */
    public function validateEntity(string $type, array $data): void
    {
        $errors = [];

        $items = isAssocArray($data) ? [$data] : $data;

        foreach ($items as $item) {
            $validator = Validator::make($item, $this->getRulesByType($type));
            if ($validator->fails()) {
                $errors[] = json_encode(
                    $validator->errors()->getMessages(),
                    JSON_UNESCAPED_UNICODE,
                ) . '. Item: ' . json_encode($item, JSON_UNESCAPED_UNICODE);
            }
        }

        if (count($errors)) {
            $errors = Arr::first($errors);

            throw new ParserValidationException($type, $errors);
        }
    }

    private function getRulesByType(string $type): array
    {
        if (!isset($this->rules[$type]) || !is_array($this->rules[$type])) {
            throw new \InvalidArgumentException("Not found rules for $type entity");
        }

        return $this->rules[$type];
    }

    private function initRules(): void
    {
        $baseLot = [
            'lot_id' => 'required|integer|min:1',
            'lot_type' => 'required|in:' . $this->config['lot_types'],
            'entity_type' => 'required|in:' . $this->config['entity_types'],
            'entity_id' => 'present|nullable|string',
            'entity_title' => 'required|string|max:' . $this->config['max_art_title_length'],
            'art_uid' => 'present|nullable|integer',
            'art_crc' => 'present|nullable|string|size:10',
            'craft' => 'present|nullable|string|max:15|craft',
            'current_strength' => 'present|nullable|integer|min:0|lte:base_strength',
            'base_strength' => 'present|nullable|integer|min:1|gte:current_strength',
            'price' => 'required|integer|min:1',
            'blitz_price' => 'present|nullable|integer|min:1',
            'quantity' => 'required|integer|min:0',
            'ended_at' => 'required|date_format:' . $this->config['date_format'],
            'seller_name' => 'required|string|max:' . $this->config['max_login_length'],
            'seller_id' => 'required|integer',
        ];

        $rules = [
            'art' => [
                'hwm_id' => 'required|string|max:' . $this->config['max_hwm_id_length'],
                'title' => 'required|string|max:' . $this->config['max_art_title_length'],
                'image' => 'required|url',
                'need_level' => 'present|nullable|integer|min:1',
                'strength' => 'required|integer|min:1',
                'oa' => 'present|nullable|integer|min:1',
                'repair' => 'present|nullable|integer|min:0',
                'description' => 'nullable|string',
                'modifiers' => 'present|nullable|array',
                'set_id' => 'present|nullable|string|max:45',
                'from_shop' => 'present|boolean',
                'as_from_shop' => 'present|boolean',
                'shop_price' => 'present|nullable|integer|min:0',
                'art_category' => 'present|nullable|in:' . $this->config['art_categories'],
                'market_category' => 'present|nullable|in:' . $this->config['market_categories'],
            ],
            'art_market_list' => [
                'hwm_id' => 'required|string|max:' . $this->config['max_hwm_id_length'],
                'title' => 'required|string|max:'  . $this->config['max_art_title_length'],
                'market_category_id' => 'required|string|max:' . $this->config['max_hwm_id_length'],
            ],
            'market_lots' => array_merge($baseLot, [
                'image' => 'present|nullable|url',
                'bids' => 'present|nullable|integer|min:0',
                'last_buyer_bid' => 'present|nullable|integer|min:0',
                'last_buyer_name' => 'present|nullable|string|max:' . $this->config['max_login_length'],
            ]),
            'lot' => array_merge($baseLot, [
                'started_at' => 'required|date_format:' . $this->config['date_format'],
                'buyed_at' => 'present|nullable|string|date_format:' . $this->config['date_format'],
                'operations.*' => 'array',
                'operations.*.time' => 'required|',
                'operations.*.type' => 'required|in:' . $this->config['operation_types'],
                'operations.*.buyed_quantity' => 'present|nullable',
                'operations.*.current_quantity' => 'required|integer',
                'operations.*.current_price' => 'required|integer',
                'operations.*.rate_diff' => 'present|nullable|integer',
                'operations.*.buyer_id' => 'present|nullable|integer',
                'operations.*.buyer_name' => 'present|nullable|string',
            ]),
            'sets' => [
                'hwm_id' => 'required|string|max:45',
                'title' => 'required|string|max:45',
                'description' => 'present|nullable|string',
                'arts' => 'required|array',
                'arts.*.art_id' => 'required|string|max:' . $this->config['max_hwm_id_length'],
                'arts.*.art_name' => 'present|nullable|string|max:' . $this->config['max_art_title_length'],
                'arts.*.art_image' => 'present|nullable|string',
            ],
            'character' => [
                'hwm_id' => 'required|integer',
                'login' => 'required|string',
                'status' => 'nullable|in:' . $this->config['character_statuses'],
                'level' => 'required|integer|min:1',
                'experience' => 'required|integer|min:0',
                'resources' => 'required|array',
                'resources.*' => 'required|integer',
                'guilds' => 'required|array',
                'guilds.*' => 'required|numeric',
                'fractions' => 'required|array',
                'fractions.*' => 'required|numeric',
            ]
        ];

        $this->rules = $rules;
    }

    private function initConfig(): void
    {
        $this->config = [
            'date_format' => config('app.date_format'),
            'lot_types' => implode(',', Lot::getLotTypes()),
            'entity_types' => implode(',', Lot::getEntityTypes()),
            'character_statuses' => implode(',', Character::getStatuses()),
            'operation_types' => implode(',', LotOperation::getTypes()),
            'art_categories' => implode(',', app(ArtCategoryRepository::class)->asArrayKeys()),
            'market_categories' => implode(',', app(MarketCategoryRepository::class)->asArrayKeys()),
            'max_art_title_length' => config('hwm.rules.max_art_title_length'),
            'max_hwm_id_length' => config('hwm.rules.max_hwm_id_length'),
            'max_login_length' => config('hwm.rules.max_login_length'),
        ];
    }
}
