<?php

declare(strict_types=1);

namespace App\Notifications;

trait MarkdownHelpers
{
    public function bold(string $text): string
    {
        return "*$text*";
    }

    public function link(string $title, string $url): string
    {
        return "[{$title}]({$url})";
    }

    public function br(): string
    {
        return PHP_EOL;
    }

    public function li(string $text): string
    {
        return "- $text" . $this->br();
    }

    public function quote(string $text): string
    {
        return "> $text";
    }

    public function space(): string
    {
        return ' ';
    }
}
