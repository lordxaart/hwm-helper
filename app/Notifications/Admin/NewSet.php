<?php

declare(strict_types=1);

namespace App\Notifications\Admin;

use App\Core\Enums\Queue;
use App\Models\Art;
use App\Models\ArtSet;
use App\Services\Notifications\Channels\Telegram\Entities\TelegramMessage;
use App\Services\Notifications\Enums\Channel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class NewSet extends Notification implements ShouldQueue
{
    use Queueable;

    public ArtSet $set;

    public function __construct(ArtSet $set)
    {
        $this->queue = Queue::NOTIFICATIONS->value;

        $this->set = $set;
    }

    public function via(): array
    {
        return ['database', Channel::TELEGRAM->getLaravelChannelName()];
    }

    public function toTelegram(): TelegramMessage
    {
        $content = "Successfully added set [{$this->set->title}]({$this->set->getLink()}), {$this->set->arts()->count()} arts:\n";
        /** @var Art $art */
        foreach ($this->set->arts as $art) {
            $content .= " - [{$art->getTitle()}]({$art->getLink()})\n";
        }

        return TelegramMessage::create()
            ->setTypeContent('new')
            ->content($content);
    }

    public function toArray(): array
    {
        return [
            'set' => $this->set->only(['hwm_id', 'title']),
        ];
    }
}
