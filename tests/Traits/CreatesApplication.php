<?php

namespace Tests\Traits;

use Dotenv\Dotenv;
use Illuminate\Contracts\Console\Kernel;

trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $dotenv = Dotenv::createMutable(__DIR__ . '/../../', '.env');
        $dotenv->load();

        $dotenv = Dotenv::createMutable(__DIR__ . '/../../', '.env.testing');
        $dotenv->load();

        $app = require __DIR__ . '/../../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        if (!isTestEnv()) {
            throw new \Exception('Allow run tests only on testing env, current env ' . env('APP_ENV'));
        }

        return $app;
    }
}
