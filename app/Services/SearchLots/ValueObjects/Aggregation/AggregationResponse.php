<?php

declare(strict_types=1);

namespace App\Services\SearchLots\ValueObjects\Aggregation;

class AggregationResponse
{
    public readonly array $response;
    /** @var Aggregation[] */
    public readonly array $aggregations;
    public readonly float $requestExec;
    private array $parsedResponse;

    public function __construct(array $response, array $aggregations,  float $requestExec = 0)
    {
        $this->response = $response;
        $this->aggregations = $aggregations;
        $this->requestExec = $requestExec;

        $this->parsedResponse = $this->parseResponse($this->response);
    }

    public function toArray(): array
    {
        return $this->parsedResponse;
    }

    private function parseResponse(array $response): array
    {
        $result = [];

        foreach ($response as $aggName => $body) {
            $result[$aggName] = $this->parseBuckets($body['buckets']);
        }

        return $result;
    }

    private function parseBuckets(array $buckets): array
    {
        $result = [];

        foreach ($buckets as $bucket) {
            $key = $bucket['key_as_string'] ?? $bucket['key'];
            $item = [];

            $aggs = \Arr::except($bucket, ['key', 'key_as_string', 'doc_count']);

            foreach ($aggs as $aggName => $body) {
                if (isset($body['buckets'])) {
                    $item[$aggName] = $this->parseBuckets($body['buckets']);
                } elseif (isset($body['value'])) {
                    $item[$aggName] = $body['value'];
                }
            }

            $result[$key] = $item;
        }

        return $result;
    }
}
