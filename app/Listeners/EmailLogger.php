<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Models\EmailLog;
use Illuminate\Mail\Events\MessageSending;
use Illuminate\Mail\Events\MessageSent;
use Symfony\Component\Mime\Email as SymfonyEmail;

class EmailLogger
{
    public const EMAIL_STATUS_SENDING = 0;
    public const EMAIL_STATUS_SENT = 1;

    public const TABLE = 'email_log';

    public function handleSending(MessageSending $event): void
    {
        $message = $event->message;
        $id = $this->getHashFromMessage($message);

        if (EmailLog::findByEmailId($id)) {
            return;
        }

        EmailLog::create([
            'email_id' => $id,
            'status' => self::EMAIL_STATUS_SENDING,
            'date' => date('Y-m-d H:i:s'),
            'from' => $this->formatAddressField($message, 'From'),
            'to' => $this->formatAddressField($message, 'To'),
            'cc' => $this->formatAddressField($message, 'Cc'),
            'bcc' => $this->formatAddressField($message, 'Bcc'),
            'subject' => $message->getSubject(),
            // 'body' => $message->getBody()->bodyToString(),
            'body' => '',
            // 'headers' => $message->getHeaders()->toString(),
            'attachments' => null,
        ]);
    }

    public function handleSent(MessageSent $event): void
    {
        $id = $this->getHashFromMessage($event->message);

        $email = EmailLog::findByEmailId($id);

        if (!$email) {
            return;
        }

        $email->update([
            'status' => self::EMAIL_STATUS_SENT,
            'date_sent' => date('Y-m-d H:i:s'),
        ]);
    }

    /**
     * Format address strings for sender, to, cc, bcc.
     */
    public function formatAddressField(SymfonyEmail $message, string $field): ?string
    {
        $headers = $message->getHeaders();

        if (!$headers->has($field)) {
            return null;
        }

        return $headers->get($field)?->getBodyAsString();
    }

    private function getHashFromMessage(SymfonyEmail $message): string
    {
        try {
            $date = $message->getHeaders()->get('date')?->getBody()?->getTimestamp();
        } catch (\Exception) {
            $date = date('Y-m-d');
        }


        $data = [
            'body' => $message->getBody(),
            'from' => $message->getHeaders()->get('from')?->getBodyAsString(),
            'to' => $message->getHeaders()->get('to')?->getBodyAsString(),
            'subject' => $message->getHeaders()->get('subject')?->getBodyAsString(),
            'date' => $date,
        ];
        $serialize = serialize($data);

        return md5($serialize);
    }
}
