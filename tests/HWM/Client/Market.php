<?php

namespace Tests\HWM\Client;

use App\HWM\Enums\EntityType;
use Tests\TestCase;

class Market extends TestCase
{
    /**
     * @var EntityType[]
     */
    public array $lots = [
        105740819 => EntityType::OBJECT_SHARE,
        105708565 => EntityType::ART_PART,
        105749710 => EntityType::ART,
        105746995 => EntityType::CERTIFICATE,
        105747687 => EntityType::HOUSE,
        105749444 => EntityType::ELEMENT,
        105749435 => EntityType::RESOURCE,
    ];

    public array $marketLots = [
        'helm',
        'coldring_n' => 'ring',
        'elements',
    ];

    /**
     * @test
     */
    public function test_lots_pages()
    {
        foreach ($this->lots as $lotId => $type) {
            $client = hwmClient();
            $lot = $client->getLot($lotId);
            $this->assertEquals($lotId, $lot->lot_id);
            $this->assertEquals($type->value, $lot->entity_type);
        }
    }

    /**
     * @test
     */
    public function test_market_lots_pages()
    {
        foreach ($this->marketLots as $entityId => $category) {
            $client = hwmClient();
            $lots = $client->getMarketLots($category, is_string($entityId) ? $entityId : null);
            $this->assertTrue($lots->count() > 0);
        }
    }
}
