<?php

declare(strict_types=1);

namespace App\Services\LotFilter\Repositories;

use App\Core\ValueObject\UserId;
use App\HWM\Enums\EntityType;
use App\HWM\Enums\LotType;
use App\Models\LotFilter;
use App\Services\SearchLots\ValueObjects\LimitOffset;
use Illuminate\Database\Eloquent\Collection;

class LotFiltersRepository
{
    public function findAllByUserId(UserId $userId, LimitOffset $limitOffset = null): Collection
    {
        $query = LotFilter::query()->where('user_id', $userId->value)->orderByDesc('created_at');

        if ($limitOffset) {
            $query->limit($limitOffset->getLimit())->offset($limitOffset->getOffset());
        }

        return $query->get();
    }

    public function findAllByUserIdAndLotEntity(
        UserId $userId,
        LotType $lotType,
        EntityType $entityType,
        string $entityId = null
    ): Collection {
        $query = LotFilter::query()
            ->where('user_id', $userId->value)
            ->where('lot_type', $lotType->value)
            ->where('entity_type', $entityType->value);

        if ($entityId) {
            $query->where('entity_id', $entityId);
        }

        return $query->orderByDesc('created_at')->get();
    }

    public function findAll(
        LotType $lotType,
        EntityType $entityType,
        string $entityId = null
    ): Collection {
        $query = LotFilter::query()
            ->where('lot_type', $lotType->value)
            ->where('entity_type', $entityType->value);

        if ($entityId) {
            $query->where('entity_id', $entityId);
        }

        return $query->orderByDesc('order')->get();
    }
}
