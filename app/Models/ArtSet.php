<?php

declare(strict_types=1);

namespace App\Models;

use App\HWM\Helpers\HwmLink;

class ArtSet extends BaseCategory
{
    protected $fillable = ['hwm_id', 'title'];

    public function arts(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Art::class, 'set_id', 'hwm_id');
    }

    public function getLink(): string
    {
        return HwmLink::get(HwmLink::SETS) . '#' . $this->hwm_id;
    }
}
