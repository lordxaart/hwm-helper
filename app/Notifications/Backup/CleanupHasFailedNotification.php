<?php

declare(strict_types=1);

namespace App\Notifications\Backup;

use App\Services\Notifications\Channels\Telegram\Entities\TelegramMessage;
use Spatie\Backup\Notifications\Notifications\CleanupHasFailedNotification as BaseNotification;

class CleanupHasFailedNotification extends BaseNotification
{
    public function toTelegram(): TelegramMessage
    {
        $content = "The cleanup backup of {$this->applicationName()} to disk {$this->diskName()} has failed";

        return TelegramMessage::create()
            ->error()
            ->content($content);
    }

    public function toArray(): array
    {
        return [
            'disk' => $this->diskName(),
            'message' => $this->event->exception->getMessage(),
        ];
    }
}
