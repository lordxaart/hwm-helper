<?php

declare(strict_types=1);

namespace App\Exceptions\User;

use App\Exceptions\ClientError;
use App\Models\User;
use Throwable;

class UserWasDeleted extends ClientError
{
    public function __construct(User $user, Throwable $previous = null)
    {
        /** @var string $message */
        $message = trans('app.notifications.change_status.your_account_was', [
            'action' => trans('app.user.actions.deleted'),
            'until_to' => '',
        ]);

        $message .= ' ' . $user->email;

        // find reason and add to message
        parent::__construct(ClientError::USER_IS_DELETED, $message, $previous);
    }
}
