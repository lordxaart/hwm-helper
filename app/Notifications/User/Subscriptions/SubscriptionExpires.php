<?php

declare(strict_types=1);

namespace App\Notifications\User\Subscriptions;

use App\Models\User;
use App\Notifications\MarkdownHelpers;
use App\Services\Notifications\Channels\Telegram\Entities\TelegramMessage;
use App\Services\Notifications\Enums\Channel;
use App\Services\Notifications\Enums\NotifyType;
use App\Services\Notifications\UserNotifyService;
use App\Services\Subscriptions\Entities\UserSubscription;
use App\Services\Subscriptions\SubscriptionService;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SubscriptionExpires extends Notification implements ShouldQueue
{
    use Queueable;
    use MarkdownHelpers;

    protected UserSubscription $sub;

    public function __construct(int $subId)
    {
        /** @var SubscriptionService $service */
        $service = app(SubscriptionService::class);
        $this->sub = $service->getSubscriptionById($subId);
    }

    public function via(User $user): array
    {
        $channels = app(UserNotifyService::class)->getConfiguredChannels($user, NotifyType::IMPORTANT);
        return [
            ...\Arr::only(
                $channels,
                [Channel::TELEGRAM->value, Channel::MAIL->value]
            ),
            Channel::DATABASE,
        ];
    }

    public function toMail(User $user): MailMessage
    {
        $end = Carbon::parse($this->sub->ended_at);
        $diff = $end->isSameDay() ? trans('app.notifications.subscriptions.today') : $end->diffForHumans();
        return (new MailMessage())
            ->subject(trans('app.notifications.subscriptions.sub_expires', ['sub' => $this->sub->subscription->name]))
            ->line(trans('app.notifications.subscriptions.expire_sub_is_expires', ['diff' => $diff]))
            ->line(trans('app.notifications.subscriptions.contact_to_admin_for_continue'))
            ->action(trans('app.main.contacts'), route('contacts'));
    }

    public function toTelegram(User $user): TelegramMessage
    {
        $end = Carbon::parse($this->sub->ended_at);
        $diff = $end->isSameDay() ? trans('Сегодня') : $end->diffForHumans();
        $message = TelegramMessage::create()->setTypeContent('error');
        $message->line($this->bold(trans('app.notifications.subscriptions.sub_expires', ['sub' => mb_strtoupper($this->sub->subscription->name)])))
            ->line(trans('app.notifications.subscriptions.expire_sub_is_expires', ['diff' => $diff]))
            ->line(trans('app.notifications.subscriptions.contact_to_admin_for_continue'))
            ->button(trans('app.main.contacts'), route('contacts'));
        return $message;
    }

    public function toArray()
    {
        return [
            'sub' => $this->sub->toArray(),
        ];
    }
}
