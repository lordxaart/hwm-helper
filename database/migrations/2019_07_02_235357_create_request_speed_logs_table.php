<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestSpeedLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_speed_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uniqid')->nullable()->index(); // need for identify group of requests (301 -> 200)
            $table->decimal('speed', 6, 5); // request execution time in seconds
            $table->string('url', 500);
            $table->string('method', 10)->default('get');
            $table->char('code', 3)->default('200');
            $table->text('request')->nullable(); // deleted below
            $table->text('response')->nullable(); // deleted below
            $table->timestamp('created_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_speed_logs');
    }
}
