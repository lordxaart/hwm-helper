<?php

declare(strict_types=1);

namespace App\HWM\NewParsers;

use Throwable;

class ParserException extends \Exception
{
    public function __construct(string $message = "", Throwable $previous = null)
    {
        parent::__construct($message, 0, $previous);
    }
}
