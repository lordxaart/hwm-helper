<?php

declare(strict_types=1);

namespace App\Console\Commands\HWM\Lot;

use App\Console\Commands\Command;
use App\HWM\Enums\EntityType;
use App\Models\Lot;
use Illuminate\Support\Str;

class RefreshLotsCertificated extends Command
{
    //SELECT * FROM `lots` WHERE entity_type = 'certificate' and entity_id IS NULL
    /** @var string  */
    protected $signature = 'hwm:lost:refresh:certificate';

    /** @var string  */
    protected $description = 'Refresh certificate lots. Set entity_id from slug(entity_title)';

    public function handle(): void
    {
        $query = Lot::query()
            ->select(['lot_id', 'entity_type', 'entity_id', 'entity_title'])
            ->where('entity_type', EntityType::CERTIFICATE->value)
            ->whereNull('entity_id');

        $this->output->progressStart();

        $query->chunk(1000, function ($items) {
            /** @var Lot $item */
            foreach ($items as $item) {
                $item->entity_id = Str::slug($item->entity_title);
                $item->save();
                $this->output->progressAdvance();
            }
        });

        $this->output->progressFinish();
        $this->log('RefreshCertificateIds');
    }
}
