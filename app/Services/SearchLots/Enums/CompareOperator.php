<?php

declare(strict_types=1);

namespace App\Services\SearchLots\Enums;

enum CompareOperator: string
{
    case MORE = '>';
    case MORE_OR_EQUAL = '>=';
    case LESS = '<';
    case LESS_OR_EQUAL = '<=';
    case EQUAL = '=';
    case HAS = 'has';
    case BETWEEN = 'between';

    public const BASE_OPERATORS = [
    self::MORE_OR_EQUAL,
    self::LESS_OR_EQUAL,
    self::MORE,
    self::LESS,
    ];
}
