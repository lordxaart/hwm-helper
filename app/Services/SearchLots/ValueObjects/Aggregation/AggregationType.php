<?php

declare(strict_types=1);

namespace App\Services\SearchLots\ValueObjects\Aggregation;

class AggregationType
{
    public const AVAILABLE_TYPES = ['min', 'max', 'avg', 'median_absolute_deviation', 'date_histogram', 'terms', 'sum'];
    public const AVAILABLE_FIELDS = ['started_at', 'entity_type', 'entity_id', 'lot_type', 'price', 'price_per_fight', 'quantity'];

    public readonly string $type;
    public readonly string $field;
    public readonly array $attributes; //other attributions

    public function __construct(string $type, string $field, array $attributes = [])
    {
        $this->validateType($type);
        $this->validateField($field);

        $this->type = $type;
        $this->field = $field;
        $this->attributes = $attributes;
    }

    private function validateType(string $type): void
    {
        if (!in_array($type, self::AVAILABLE_TYPES)) {
            throw new \InvalidArgumentException(sprintf('Type [%s] is invalid', $type));
        }
    }

    private function validateField(string $field): void
    {
        if (!in_array($field, self::AVAILABLE_FIELDS)) {
            throw new \InvalidArgumentException(sprintf('Field [%s] is invalid', $field));
        }
    }
}
