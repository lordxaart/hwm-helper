<?php

declare(strict_types=1);

namespace App\Core\Enums;

enum Queue: string
{
    case DEFAULT = 'default';
    case NOTIFICATIONS = 'notifications';
    case HWM_NEW_LOTS = 'hwm_new_lots';
    case HWM_NEW_MARKET_LOTS = 'hwm_new_market_lots';
    case HWM_UPDATE_LOTS = 'hwm_update_lots';
    case HWM_ES_LOTS = 'hwm_es_lots';
    case USER_NOTIFY_NEW_LOTS = 'user_notify_new_lots';
}
