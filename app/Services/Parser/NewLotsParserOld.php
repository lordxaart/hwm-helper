<?php

declare(strict_types=1);

namespace App\Services\Parser;

use App\Components\LotCache;
use App\HWM\Exceptions\ParserErrors\EmptyLotPage;
use App\Services\Parser\Storages\StorageForNewLots;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @deprecated Парсер уже неактуальний бо немає можливості моніторити нові лоти, для нових лотів потрібен спец. код (crc) який можна отримати тільки на ринку з лотами
 * @see NewLotsParser
 *
 * @property ?LotRunner $currentRunner
 */
class NewLotsParserOld extends AbstractParser
{
    public const USLEEP_TO_WAIT_NEXT_LOT = 500000; // 0.5sec

    protected StorageForNewLots $storage;
    protected bool $waitNextLot = false;
    private bool $reverse;

    public function __construct(OutputInterface $output = null, bool $reverse = false)
    {
        $this->reverse = $reverse;
        $this->storage = new StorageForNewLots();

        parent::__construct($output);

        $this->setRunner($this->getNextLotId());
    }

    public function getType(): string
    {
        $type = parent::getType();

        if ($this->reverse) {
            $type .= '.reverse';
        }

        return $type;
    }

    public function getState(): string
    {
        if (!$this->reverse && $this->waitNextLot) {
            return 'wait';
        }

        return parent::getState();
    }

    public function __destruct()
    {
        parent::__destruct();

        $this->deleteCurrentRunner();
    }

    protected function setRunner(int $lotId): void
    {
        // set lot id to current parsers
        if ($this->currentRunner) {
            $this->storage->delete((string)$this->currentRunner->getLotId());
        }

        if (!$this->storage->add((string)$lotId)) {
            $this->setNextRunner();
            return;
        }

        $fireEvent = !$this->reverse;

        $this->currentRunner = new LotRunner(LotRunner::TYPE_NEW, $lotId, $fireEvent);
    }

    protected function getNextLotId(): int
    {
        if (!$this->reverse) {
            $maxCache = LotCache::getMaxLotId();
            $max = $this->storage->getMaxId();
            $lotId = $max && $max > $maxCache ? $max : $maxCache;
        } else {
            $minCache = LotCache::getMinLotId();
            $min = $this->storage->getMinId();
            $lotId = $min && $min < $minCache ? $min : $minCache;
        }

        if (!$lotId) {
            $lotId = intval(config('hwm.last_lot_id'));
        }

        $lotId = !$this->reverse ? ($lotId + 1) : ($lotId - 1);

        if ($this->currentRunner && $this->currentRunner->getLotId() === $lotId) {
            $lotId = !$this->reverse ? ($lotId + 1) : ($lotId - 1);
        }

        return $lotId;
    }

    protected function setNextRunner(): void
    {
        if ($this->waitNextLot) {
            $this->log('Wait next lot, sleep ' . self::USLEEP_TO_WAIT_NEXT_LOT . ' microseconds');
            usleep(self::USLEEP_TO_WAIT_NEXT_LOT);
            $this->waitNextLot = false;
            $lotId = $this->currentRunner?->getLotId();
        } else {
            // get next lot id
            $lotId = $this->getNextLotId();
        }

        $this->setRunner($lotId ?: 0);
    }

    protected function handleRunException(\Throwable $e): void
    {
        if ($e instanceof EmptyLotPage) {
            $this->waitNextLot = true;
            return;
        }

        parent::handleRunException($e);
    }

    protected function deleteCurrentRunner(): void
    {
        if (!$this->currentRunner) {
            return;
        }

        $this->storage->delete(strval($this->currentRunner->getLotId()));
    }
}
