<?php

declare(strict_types=1);

namespace App\Services\LotFilter\ValueObjects;

use Illuminate\Support\Collection;

/**
 * @property FilterTerm[] $items
 * @method FilterTerm current()
 */
final class FilterTermCollection extends Collection
{
    public function __construct(array $items = [])
    {
        foreach ($items as $item) {
            $this->validateElement($item);
        }

        parent::__construct($items);
    }

    public function toArray(): array
    {
        return $this->items;
    }

    public function toArrayOfValues(): array
    {
        return array_map(fn (FilterTerm $term) => $term->toArray(), $this->items);
    }

    private function validateElement(mixed $item): void
    {
        if (! ($item instanceof FilterTerm)) {
            throw new \InvalidArgumentException(sprintf('Each element of collection must be instance of %s', FilterTerm::class));
        }
    }
}
