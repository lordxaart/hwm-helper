<?php

declare(strict_types=1);

namespace App\Services\Other;

class RandomUserAgent
{
    private array $agentDetails;

    private array $filterParams = [
        'agent_name',
        'agent_type',
        'device_type',
        'os_name',
        'os_type',
    ];

    public function __construct(string $pathToAgentList)
    {
        $this->agentDetails = json_decode(file_get_contents($pathToAgentList) ?: '', true);
    }

    /**
     * Grab a random user agent from the library's agent list
     *
     * @throws \Exception
     */
    public function random(array $filterBy = []): string
    {
        $agents = $this->loadUserAgents($filterBy);

        if (empty($agents)) {
            throw new \Exception('No user agents matched the filter');
        }

        return $agents[mt_rand(0, count($agents) - 1)];
    }


    /**
     * Validates the filter so that no unexpected values make their way through
     */
    private function validateFilter(array $filterBy = []): array
    {
        $outputFilter = [];

        foreach ($this->filterParams as $field) {
            if (!empty($filterBy[$field])) {
                $outputFilter[$field] = $filterBy[$field];
            }
        }

        return $outputFilter;
    }

    /**
     * Returns an array of user agents that match a filter if one is provided
     */
    private function loadUserAgents(array $filterBy = []): array
    {
        $filterBy = $this->validateFilter($filterBy);

        $agentStrings = [];

        for ($i = 0; $i < count($this->agentDetails); $i++) {
            foreach ($filterBy as $key => $value) {
                if (!isset($this->agentDetails[$i][$key]) || !$this->inFilter($value, $this->agentDetails[$i])) {
                    continue 2;
                }
            }
            $agentStrings[] = $this->agentDetails[$i]['agent_string'];
        }

        return array_values($agentStrings);
    }

    private function inFilter(string $key, array $array): bool
    {
        return in_array(strtolower($key), array_map('strtolower', $array));
    }
}
