<?php

declare(strict_types=1);

namespace App\Services\Telegram\Commands;

use App\Services\Telegram\Entities\Update;
use App\Services\Telegram\TelegramCommand;
use App\Services\Telegram\UserTelegramService;

class DisconnectCommand implements TelegramCommand
{
    public const COMMAND = '/disconnect';

    public function handle(Update $update): void
    {
        if (!$update->message || !$update->message->from) {
            return;
        }

        $chatId = $update->message->chat->id;

        $user = app(UserTelegramService::class)->getUserByChatId($chatId);

        if (!$user) {
            throw new \InvalidArgumentException("Cannot find user with chat id [{$chatId}}]");
        }

        app(UserTelegramService::class)->disconnectTelegram($user);
    }
}
