<?php

declare(strict_types=1);

// Main
use App\Http\Controllers\Webhook\HandleTelegramUpdatesAction;

Route::view('/', 'pages.router_view')->name('index');

Route::view('/calculator', 'pages.router_view')->name('calculator');
Route::view('/market', 'pages.router_view')->name('market');

Route::view('/monitoring', 'pages.router_view')->name('monitoring')->middleware(['auth', 'verified']);
Route::view('/filters', 'pages.router_view')->name('filters')->middleware(['auth', 'verified']);
Route::view('/stat', 'pages.router_view')->name('stat');
Route::view('/analytics', 'pages.router_view')->name('analytics');

// Static pages
Route::view('/contacts', 'pages.router_view')->name('contacts');
Route::view('/privacy', 'pages.router_view')->name('privacy');
Route::view('/terms', 'pages.router_view')->name('terms');

// Profile routes
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'middleware' => ['auth', 'verified']], function () {
    Route::get('/', 'UsersController@index')->name('index');
    Route::view('settings', 'pages.router_view')->name('settings');

    Route::post('settings/password', 'UsersController@updatePassword')->name('updatePassword');

    Route::get('connect-social/{social}', 'UsersController@connectSocialNetwork')->name('connectSocial');
});

// Auntentication routes
Auth::routes(['verify' => true]);
Route::get('logout', 'Auth\LoginController@logout');

Route::get('auth/login/{social}', 'Auth\LoginController@redirectToSocial')->name('login.social');
Route::get('auth/{social}/callback', 'Auth\LoginController@handleSocialRedirect')->name('login.social.redirect');

Route::post('webhook/telegram/{token}', [HandleTelegramUpdatesAction::class, 'handle'])->name('webhook.telegram');

Route::group(['prefix' => 'test',], function () {
    Route::get('/', 'TestController@index');
});
