import isObject from 'lodash/isObject';
import has from 'lodash/has';
import notify from 'JS/app/utils/notify';
import i18n from 'JS/app/utils/localization';

const dataToString = (response) => {
    const { data } = response;
    let message = '';
    let title = i18n.t('errors.400');

    if (data.message) {
        message = `<p>${data.message}</p>`;
    }

    const simpleStatuses = [403, 405, 419, 429, 413, 500, 503];
    const status = Number.parseInt(response.status, 10);

    if (simpleStatuses.includes(status)) {
        title = `<p>${i18n.t(`errors.${status}`)}</p>`;
    }

    if (data.errors && isObject(data.errors)) {
        const { errors } = data;
        let errorText = '<ul class="mt-2">';
        Object.keys(errors).forEach((key) => {
            errorText += `<li>${errors[key]}</li>`;
        });

        errorText += '</ul>';

        message += errorText;
    }

    if (data.debug) {
        message += '<br>';
        message += '<p><strong>Debug:</strong></p>';

        message += `<p><em>Class: </em>${data.debug.class}</p>`;
        message += `<p><em>File: </em>${data.debug.file}</p>`;

        // if (data.debug.trace && Array.isArray(data.debug.trace)) {
        //     message += '<ul>';
        //     data.debug.trace.forEach((line) => {
        //         message += `<li>${line}</li>`;
        //     });
        //     message += '</ul>';
        // }
    }

    return { title, message };
};

// eslint-disable-next-line consistent-return
export default (error) => {
    // if has response show the error
    if (error.response) {
        const { title, message } = dataToString(error.response);
        notify.error(message, title);
    }

    // check for errorHandle config
    if (has(error.config, 'errorHandle') && error.config.errorHandle === false) {
        return Promise.reject(error);
    }
};
