<?php

declare(strict_types=1);

namespace App\Console\Commands\HWM;

use App\Console\Commands\Command;
use App\HWM\Helpers\HwmHelper;
use App\Models\Art;

class GenerateStaticArtsFromDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hwm:art:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate arts list from db to json (hwm storage) and update db';

    protected array $attributes = [
        'hwm_id',
        'title',
        'original_image',
        'repair',
        'strength',
        'shop_price',
        'from_shop',
        'as_from_shop',
        'need_level',
        'oa',
        'description',
        'category_id',
        'market_category_id',
        'set_id',
    ];

    /**
     * @throws \JsonException
     */
    public function handle(): void
    {
        $arts = Art::get($this->attributes);

        if (!$arts->count()) {
            $this->warn('Not found any arts');
            return;
        }

        $previousArts = HwmHelper::getJson('arts') ?: [];
        // add previous arts if not exist
        foreach ($previousArts as $previousArt) {
            if (!$arts->where('hwm_id', $previousArt['hwm_id'])->count()) {
                $arts->push($previousArt);
            }
        }

        $content = json_encode($arts->toArray(), JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

        file_put_contents(hwm_storage_path('arts.json'), $content);

        $this->info('Count arts - ' . $arts->count());
    }
}
