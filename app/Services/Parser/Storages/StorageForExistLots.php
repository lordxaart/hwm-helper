<?php

declare(strict_types=1);

namespace App\Services\Parser\Storages;

use Illuminate\Support\Facades\Redis;

class StorageForExistLots
{
    public const PREFIX_KEY = 'storage_parser_';

    protected string $cacheKey;

    public function __construct(string $cacheKey)
    {
        $this->cacheKey = self::PREFIX_KEY . $cacheKey;
    }

    public function hasMore(): bool
    {
        /** @phpstan-ignore-next-line */
        return count(Redis::smembers($this->cacheKey)) > 0;
    }

    public function set(array $data): bool
    {
        if (!count($data)) {
            return false;
        }
        /** @phpstan-ignore-next-line */
        return boolval(Redis::sadd($this->cacheKey, $data));
    }

    public function pop(): int
    {
        /** @phpstan-ignore-next-line */
        return intval(Redis::spop($this->cacheKey));
    }

    public function locked(string $key): bool
    {
        /** @phpstan-ignore-next-line */
        Redis::setex($key, true, 60);

        return true;
    }

    public function unlocked(string $key): bool
    {
        /** @phpstan-ignore-next-line */
        return (bool) Redis::del($key);
    }

    public function isLocked(string $key): bool
    {
        /** @phpstan-ignore-next-line */
        return !!Redis::exists($key);
    }
}
