<?php

declare(strict_types=1);

namespace App\Jobs\Lot;

use App\Core\Enums\Queue;
use App\HWM\Exceptions\ParserErrors\UnexpectedPage;
use App\HWM\Exceptions\RequestErrors\LoginDuringAnotherScriptLogin;
use App\Models\Lot;
use App\Models\LotFilterNotification;
use App\Models\LotOperation;
use App\Models\LotParserLog;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class LotUpdate implements ShouldQueue, ShouldBeUnique
{
    use InteractsWithQueue;
    use Queueable;

    public const SLEEP_ON_LOGIN_ANOTHER_SCRIPT = 5;

    public int $lotId;

    private array $state = [];

    public function __construct(int $lotId)
    {
        $this->lotId = $lotId;

        $this->queue = Queue::HWM_UPDATE_LOTS->value;
        // $this->connection = 'redis';
    }

    /**
     * @return void
     */
    public function handle(): void
    {
        $start = microtime(true);
        $attempts = 0;
        $state = UNDEFINED;
        $errors = [];

        try {
            retry(3, function () use (&$attempts, &$state) {
                $attempts++;
                $this->handleLotId();
                $state = 'success';
            }, 1, function (\Throwable $e) use (&$state) {
                $state = 'error';

                if ($e instanceof LoginDuringAnotherScriptLogin) {
                    sleep(self::SLEEP_ON_LOGIN_ANOTHER_SCRIPT);
                }

                return false;
            });
        } catch (\Throwable $e) {
            $state = 'error';

            if ($e instanceof LoginDuringAnotherScriptLogin || $e instanceof UnexpectedPage) {
                \Log::warning($e::class);
            } else {
                $errors[] = $e->getMessage();
                \Log::error($e->getMessage(), ['exception' => $e]);
            }
        }

        LotParserLog::insert([
            'parser_id' => 'lot_update',
            'parser_type' => 'lot_update',
            'lot_id' => $this->lotId,
            'state' => $state,
            'attempts' => $attempts,
            'time_execute' => microtime(true) - $start,
            'server_ip' => getServerIp(),
            'errors' => implode(' | ', $errors),
            'created_at' => now()->format(LotParserLog::getModel()->getDateFormat()),
            'updated_at' => now()->format(LotParserLog::getModel()->getDateFormat()),
        ]);
    }

    public function uniqueId(): int
    {
        return $this->lotId;
    }

    private function handleLotId(): void
    {
        $lot = Lot::where('lot_id', $this->lotId)->firstOrFail();
        $lotEntity = hwmClient()->getLot($lot->lot_id, $lot->crc);
        if ($lot->parsing_status === Lot::PARSING_STATUS_NEW_MARKET) {
            $this->updateNotificationDelay($lotEntity->lot_id, $lotEntity->started_at);
        }

        $lot->update($lotEntity->toArray());

        if ($lot->is_parsed) {
            LotOperation::createFromArray($lotEntity->lot_id, $lotEntity->operations);
        }

        IndexLotToElasticSearch::dispatchSync($lot->asLotEntity());
    }

    private function updateNotificationDelay(int $lotId, Carbon $startedAt): void
    {
        LotFilterNotification::query()->where('lot_id', $lotId)->each(function (LotFilterNotification $record) use ($startedAt) {
            $record->update([
                'created_diff_seconds' => $record->created_at->diffInSeconds($startedAt),
                'created_at' => $record->created_at->format(LotFilterNotification::newModelInstance()->getDateFormat()),
            ]);
        });
    }
}
