<?php

declare(strict_types=1);

namespace App\Notifications\User;

use App\Models\User;
use App\Services\Notifications\Enums\Channel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class DetectTelegramChatBlocked extends Notification
{
    use Queueable;

    public string $message;

    public string $type;

    public function via(User $user): array
    {
        return [
            Channel::MAIL->getLaravelChannelName(),
        ];
    }

    public function toMail(): MailMessage
    {
        return (new MailMessage())
            ->subject(trans('app.notifications.telegram.bot_disconnected_from_profile'))
            ->line(trans('app.notifications.telegram.detect_channel_blocked'));
    }
}
