@extends(backpack_view('blank'))

@section('header')
    <section class="container-fluid">
        <h2>
            <span class="text-capitalize">Status page</span>
        </h2>
    </section>
@endsection

@section('content')
    <statuspage></statuspage>
@endsection