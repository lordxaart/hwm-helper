<?php

namespace Tests\Feature\API;

class ApiTest extends ApiTestCase
{
    /**
     * @test
     */
    public function can_get_base_api_route()
    {
        $response = $this->getJson(self::HOME_URL);

        $response->assertSuccessful()->assertJson([
            'message' => 'API V1 is work',
        ]);
    }
}
