<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Services\Subscriptions\Enums\Subscription;
use App\Services\UserFeatures\Enums\Feature;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Seeder;

class SubscriptionsSeeder extends Seeder
{
    public function run()
    {
        $subscriptions = [
            Subscription::PREMIUM->value => [
                'monthly' => ['name' => 'Місячний план', 'duration' => 'P1M', 'trial' => 'P5D', 'price' => 299], // 3$/m
                '3_monthly' => ['name' => '3-х місячний план', 'duration' => 'P3M', 'trial' => 'P5D', 'price' => 749], // 2.5$/m
                'yearly' => ['name' => 'Річний план', 'duration' => 'P1Y', 'trial' => 'P5D', 'price' => 2399], // 2$/m
                'features' => [
                    Feature::MAX_MONITORED_ARTS->value => 20,
                    Feature::MAX_MONITORED_FILTERS->value => 20,
                    Feature::LOT_FILTERS_NOTIFY_DELAY->value => 0,
                    Feature::CAN_EDIT_GAME_SETTINGS->value => true,
                    Feature::IS_SHOW_ADS->value => false,
                ],
            ],
            Subscription::GOLD->value => [
                'monthly' => ['name' => 'Місячний план', 'duration' => 'P1M', 'trial' => 'P5D', 'price' => 499], // 5$/m
                '3_monthly' => ['name' => '3-х місячний план', 'duration' => 'P3M', 'trial' => 'P5D', 'price' => 1199], // 4$/m
                'yearly' => ['name' => 'Річний план', 'duration' => 'P1Y', 'trial' => 'P5D', 'price' => 3499], // 3$/m
                'features' => [
                    Feature::MAX_MONITORED_ARTS->value => 50,
                    Feature::MAX_MONITORED_FILTERS->value => 50,
                    Feature::LOT_FILTERS_NOTIFY_DELAY->value => 0,
                    Feature::CAN_EDIT_GAME_SETTINGS->value => true,
                    Feature::IS_SHOW_ADS->value => false,
                ],
            ]
        ];


        /** @var DatabaseManager $db */
        $db = app(DatabaseManager::class);
        $features = features();
        $db_s = $db->table('subscriptions');
        $db_p = $db->table('subscription_plans');

        foreach ($subscriptions as $s_key => $plans) {
            $s = $db_s->clone()->where('alias', $s_key)->first();

            if (!$s) {
                $s_id = $db_s->clone()->insertGetId([
                    'alias' => $s_key,
                    'name' => ucwords($s_key),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
                $this->command->info("Create subscription $s_key");

                if (!empty($plans['features'])) {
                    $features->setSubscriptionFeatures((string)$s_id, $plans['features']);
                }
            } else {
                $s_id = $s->id;
            }

            foreach ($plans as $p_key => $plan) {
                if ($p_key === 'features') {
                    continue;
                }

                $p = $db_p->clone()->where('alias', $p_key)->where( 'subscription_id', $s_id)->first();
                if (!$p) {
                    $db_p->clone()->insert([
                        'alias' => $p_key,
                        'subscription_id' => $s_id,
                        'name' => $plan['name'],
                        'duration' => $plan['duration'],
                        'trial' => $plan['trial'] ?: null,
                        'price' => $plan['price'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
                    $this->command->info("Create plan $p_key for $s_key");
                }
            }
        }
    }
}
