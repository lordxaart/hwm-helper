<?php

declare(strict_types=1);

namespace App\Components\Validators\Rules;

use App\Models\Objects\FilterTerm;
use Illuminate\Contracts\Validation\Rule;

class NumberOrCompare implements Rule
{
    protected bool $notFoundOperator = false;

    /**
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $operators = FilterTerm::operators();
        $operators[] = 'between';

        if (intval($value) == $value) {
            return true;
        }

        $data = explode(';', $value);
        $data = array_filter(array_map('trim', $data));

        if (!count($data)) {
            return false;
        }

        foreach ($data as $item) {
            foreach ($operators as $operator) {
                if (!str_contains($item, $operator)) {
                    $this->notFoundOperator = true;
                    return false;
                }
            }
        }

        return true;
    }

    public function message(): string
    {
        return trans('validation.number_or_compare');
    }
}
