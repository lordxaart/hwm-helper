<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmptyLot extends Model
{
    public $fillable = ['lot_id'];

    public $timestamps = false;

    protected $primaryKey = 'lot_id';
}
