<?php

declare(strict_types=1);

namespace App\HWM\Repositories;

use App\HWM\Repositories\Base\EloquentRepository;
use App\Models\ArtSet;

class ArtSetRepository extends EloquentRepository
{
    public function __construct()
    {
        parent::__construct(new ArtSet());
    }
}
