<?php

namespace Tests\Feature;

use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * @test
     */
    public function test_guest_login_page(): void
    {
        $this->assertOnlyForGuest(self::LOGIN_URL);
    }

    /**
     * @test
     */
    public function test_guest_register_page(): void
    {
        $this->assertOnlyForGuest(self::REGISTER_URL);
    }
}
