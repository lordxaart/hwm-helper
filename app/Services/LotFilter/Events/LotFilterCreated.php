<?php

declare(strict_types=1);

namespace App\Services\LotFilter\Events;

use App\Models\LotFilter;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class LotFilterCreated
{
    use Dispatchable;
    use SerializesModels;

    public LotFilter $lotFilter;

    public function __construct(LotFilter $lotFilter)
    {
        $this->lotFilter = $lotFilter;
    }
}
