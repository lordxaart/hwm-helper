<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Closure;
use Illuminate\Support\Arr;

class BlockEnemyCountries
{
    private array $enemyCountries = [
         'RU', // Russian
         'BL', // Belarus
    ];

    public function handle(Request $request, Closure $next): mixed
    {
        $requestCountry = strtoupper($this->getCountryFromRequest($request) ?: '');
        if ($requestCountry && in_array($requestCountry, $this->enemyCountries)) {
            \DB::table('logs_block_enemies_requests')->insert([[
                'ip' => getUserIp(),
                'user_id' => user()?->getId()->value,
                'country' => $requestCountry,
                'url' => $request->path(),
                'created_at' => date('Y-m-d H:i:s.u'),
            ]]);

            return response(view('errors.block_by_country', ['country' => $requestCountry]))->setStatusCode(403);
        }

        return $next($request);
    }

    private function getCountryFromRequest(Request $request): ?string
    {
        $country = $request->header('cf-ipcountry');

        if (is_array($country)) {
            return (string) Arr::first($country);
        }

        return $country ?: null;
    }
}
