<?php

declare(strict_types=1);

namespace App\Models\Casts;

use App\Services\LotFilter\ValueObjects\FilterTerm;
use App\Services\LotFilter\ValueObjects\FilterTermCollection;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class CastFilterTerms implements CastsAttributes
{
    public function get($model, string $key, $value, array $attributes): FilterTermCollection
    {
        $data = json_decode($value, true) ?: [];
        $terms = [];

        foreach ($data as $item) {
            $terms[] = FilterTerm::parseItemFromString($item);
        }

        return new FilterTermCollection($terms);
    }

    public function set($model, string $key, $collection, array $attributes)
    {
        if (! ($collection instanceof FilterTermCollection)) {
            throw new \InvalidArgumentException('Invalid value for filter terms, expect ' . FilterTermCollection::class);
        }

        $terms = [];

        /** @var FilterTerm $filterTerm */
        foreach ($collection as $filterTerm) {
            $terms[] = $filterTerm->toString();
        }

        return json_encode($terms);
    }
}
