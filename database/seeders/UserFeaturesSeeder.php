<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Services\UserFeatures\Enums\Feature;
use App\Services\UserFeatures\UserFeatureRepository;
use Illuminate\Database\Seeder;

class UserFeaturesSeeder extends Seeder
{
    public function run()
    {
        /** @var UserFeatureRepository $repo */
        $repo = app(UserFeatureRepository::class);
        $existsFeatures = $repo->getFeatureList();

        $defaults = [
            Feature::MAX_MONITORED_ARTS->value => 2,
            Feature::MAX_MONITORED_FILTERS->value => 1,
            Feature::LOT_FILTERS_NOTIFY_DELAY->value => 60,
            Feature::CAN_EDIT_GAME_SETTINGS->value => false,
            Feature::IS_SHOW_ADS->value => true,
        ];

        foreach (Feature::cases() as $feature) {
            if (isset($existsFeatures[$feature->value])) {
                $this->command->warn("Feature [$feature->value] already exists");
                continue;
            }

            $repo->addFeature(
                id: $feature->value,
                type: $feature->getType(),
                default_value: $defaults[$feature->value],
            );

            $this->command->info("Insert feature [$feature->value]");
        }
    }
}
