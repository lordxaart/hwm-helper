<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $lot_filter_id
 * @property string $notify_channel
 * @property bool $is_available
 */
class LotFilterNotifyChannel extends Model
{
    public const TABLE_NAME = 'lot_filter_notify_channels';

    public $timestamps = false;

    protected $primaryKey = null;

    public $incrementing = false;

    protected $table = self::TABLE_NAME;

    protected $casts = [
        'lot_filter_id' => 'int',
        'is_available' => 'boolean',
    ];

    protected $fillable = [
        'is_available',
    ];
}
