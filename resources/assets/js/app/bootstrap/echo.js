import EchoBase from 'laravel-echo';
import config from 'JS/app/config';

window.Pusher = require('pusher-js');

const options = {
    broadcaster: 'pusher',
    key: config.app.pusher_app_key,
    wsHost: window.location.host,
    wsPort: !config.app.secure_scheme ? 2052 : 2053,
    wssPort: !config.app.secure_scheme ? 2052 : 2053,
    disableStats: true,
    encrypted: config.app.secure_scheme,
    enabledTransports: ['ws', 'wss'],
};

window.Echo = null;
if (process.env.NODE_ENV === 'prod') {
    try {
        const Echo = new EchoBase(options);
        window.Echo = Echo;
    } catch (e) {
        window.Echo.connector.disconnect();
    }
}
