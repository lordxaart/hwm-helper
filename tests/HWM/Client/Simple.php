<?php

namespace Tests\HWM\Client;

use App\Actions\ElementPricesFromMarketUpdate;
use App\HWM\HwmClient;
use App\Models\Role;
use Tests\TestCase;
use Tests\Traits\WithStudUser;

class Simple extends TestCase
{
    use WithStudUser;

    public array $arts = [
        'sun_ring',
        'krest2',
        'krest2_en',
        'thief_paper',
        'potion02'
    ];

    /**
     * @test
     */
    public function test_arts_parser()
    {
        foreach ($this->arts as $art) {
            $client = app(HwmClient::class);
            if (str_contains($art, EN)) {
                $art = str_replace('_' . EN, '', $art);
                $client->setLocale(EN);
            }
            $artObj = $client->getArtInfo($art);
            $this->assertEquals($art, $artObj->getHwmId());
        }
    }

    /**
     * @test
     */
    public function test_update_element_prices()
    {
        \Auth::login($this->createStubUser(Role::ADMIN));
        $prices = (new ElementPricesFromMarketUpdate())->run();

        $this->assertCount(11, $prices);

        \Auth::logout();
    }
}
