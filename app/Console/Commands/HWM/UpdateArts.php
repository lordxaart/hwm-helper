<?php

declare(strict_types=1);

namespace App\Console\Commands\HWM;

use App\Actions\Art\ArtAdd;
use App\Console\Commands\Command;
use App\HWM\Repositories\ArtRepository;
use App\Models\Art;
use Exception;

class UpdateArts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = 'hwm:art:update:from_market';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update arts list from market and sets page';

    /**
     * @throws Exception
     */
    public function handle(): void
    {
        retry(3, function () {
            $hwm = hwmClient();

            $artRepository = app(ArtRepository::class);

            $newArtsFromMarket = $hwm->getListArtsFromMarket()
                ->filter(fn($item) => !$artRepository->has($item['hwm_id']))
                ->take(3);

            foreach ($newArtsFromMarket as $item) {
                /** @var Art $newArt */
                $newArt = ArtAdd::run($item['hwm_id']);

                // save market category
                $newArt->market_category_id = $item['market_category_id'];
                $newArt->save();
            }

            $this->info("Count new arts - {$newArtsFromMarket->count()}");
        }, 5);
    }
}
