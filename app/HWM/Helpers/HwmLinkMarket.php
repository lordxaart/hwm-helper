<?php

declare(strict_types=1);

namespace App\HWM\Helpers;

class HwmLinkMarket
{
    const TYPE_ALL = 0; // sbn=1&sau=1
    const TYPE_ONLY_SALE = 1; // sbn=1&sau=0
    const TYPE_ONLY_AUCTION = 2; // sbn=0&sau=1

    public static function link(
        string $category = null,
        string $art_type = null,
        int $sort = HwmMarketSort::CHEAPEST->value,
        int $type = self::TYPE_ONLY_SALE,
        bool $full_durability = false,

    ): string {
        if (!in_array($sort, [1, 2, 3, 4])) {
            throw new \InvalidArgumentException("{Sort} value [$sort] is invalid");
        }

        if (!in_array($type, [0, 1, 2])) {
            throw new \InvalidArgumentException("{Type} value [$type] is invalid");
        }

        $params = [
            'cat' => $category,
            'art_type' => $art_type,
            'sort' => $sort,
            'snew' => $full_durability ? 1 : 0, // 1 - Full durability, 0 - any durability
            'sbn' => 1,
            'sau' => 1,
        ];

        if ($type === self::TYPE_ONLY_SALE) {
            $params['sbn'] = 1;
            $params['sau'] = 0;
        }

        if ($type === self::TYPE_ONLY_AUCTION) {
            $params['sbn'] = 0;
            $params['sau'] = 1;
        }

        return HwmLink::get(HwmLink::MARKET, $params);
    }
}
