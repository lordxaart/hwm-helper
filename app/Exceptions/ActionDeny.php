<?php

declare(strict_types=1);

namespace App\Exceptions;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

class ActionDeny extends ClientError
{
    public function __construct(string $action, int $statusCode = 0, Throwable $previous = null, int $code = 0)
    {
        $message = trans('errors.action_not_allowed', ['action' => $action]);
        $statusCode = $statusCode ?: Response::HTTP_FORBIDDEN;

        parent::__construct($statusCode, $message, $previous, $code);
    }
}
