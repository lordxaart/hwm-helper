<nav class="navbar navbar-expand-lg fixed-top navbar-dark">
	<div class="container">
		<a class="navbar-brand logo-link" href="/" @click.prevent="$router.push('/').catch(()=>{})">
			<img src="{{asset('images/logo/logo_32x32.png')}}" width="26" height="26" alt="Logo" class="d-inline-block align-top mr-1">
			<span class="logo-text">{{config('app.name')}}</span>
		</a>

        <b-navbar-toggle target="navbarmain" class="navbar-toggler"></b-navbar-toggle>

		<b-collapse id="navbarmain" is-nav>
			<ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <router-link to="/calculator" class="nav-link">{{ __('app.main.calculator') }}</router-link>
                </li>
                <li class="nav-item">
                    <router-link to="/market" class="nav-link">{{ __('app.main.market') }}</router-link>
                </li>
                <li class="nav-item">
                    <router-link
                        to="/monitoring"
                        class="nav-link"
                        v-tooltip="'{{ !user() ? __('app.user.access_only_for_auth_users') : '' }}'"
                    >
                        {{ __('app.main.monitoring') }}
                    </router-link>
                </li>
                <li class="nav-item">
                    <router-link
                        to="/filters"
                        class="nav-link"
                        v-tooltip="'{{ !user() ? __('app.user.access_only_for_auth_users') : '' }}'"
                    >
                        {{ __('app.main.filters') }}
                    </router-link>
                </li>
                <li class="nav-item">
                    <router-link to="/stat" class="nav-link">{{ __('app.main.statictic_market') }}</router-link>
                </li>
                @if(isLocalEnv())
                <li class="nav-item">
                    <router-link to="/analytics" class="nav-link">Аналітика</router-link>
                </li>
                @endif
			</ul>
			<hr class="d-lg-none d-xl-none d-none">
			<ul class="navbar-nav v-cloak align-items-center" v-cloak>
				@if(!isset($auth) && Auth::check())
{{--					@include('elements.user_notification_button')--}}
                    <b-nav-item-dropdown id="navbarAuth">
                        <template v-slot:button-content>
                            <span>{{ Auth::user()->email }}</span>&nbsp;<icon name="user"></icon>
                        </template>
                        @role('super-admin')
                        <b-dropdown-item href="{{route('admin.backpack')}}">
                            <icon name="admin"></icon> @{{ $t('app.main.admin_panel') }}
                        </b-dropdown-item>
                        @endrole
                        <b-dropdown-item href="{{route('profile.settings')}}" @click.prevent="$router.push('/profile/settings')">
                            <icon name="conf"></icon> @{{ $t('app.main.settings') }}
                        </b-dropdown-item>
                        <b-dropdown-item href="/logout">
                            <icon name="logout"></icon> @{{ $t('app.auth.logout') }}
                        </b-dropdown-item>
                    </b-nav-item-dropdown>
				@else
					<li class="nav-item {{ isRoute('login') ? 'active' : '' }}">
                        <a href="{{ route('login') }}" class="nav-link" v-tooltip="$t('app.auth.enter')">
                            <icon name="login"></icon>
                        </a>
					</li>
                    <li class="nav-item {{ isRoute('register') ? 'active' : '' }}">
                        <a href="{{ route('register') }}" class="nav-link" v-tooltip="$t('app.auth.registration')" onclick="{{ isRoute('register') ? 'event.preventDefault();' : '' }}">
                            <icon name="register"></icon>
                        </a>
					</li>
				@endif

                <select-locale></select-locale>

                <li class="nav-item">
                    <a href="#" @click.prevent="openFeedbackModal" class="nav-link text-info" v-tooltip="$t('app.feedback.write_to_admin')">
                        <icon size="lg" name="feedback"></icon>
                    </a>
                </li>

                {{-- Feedback Modal --}}
                <b-modal
                    :id="feedbackModalId"
                    :title="$t('app.feedback.write_to_admin')"
                    hide-footer
                >
                    <keep-alive>
                        <component :is="FeedbackComponent" @submit="hideModal(feedbackModalId)" />
                    </keep-alive>
                </b-modal>
			</ul>
		</b-collapse>
	</div>
</nav>
