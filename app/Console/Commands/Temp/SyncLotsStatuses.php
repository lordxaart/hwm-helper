<?php

declare(strict_types=1);

namespace App\Console\Commands\Temp;

use App\Models\Lot;
use Illuminate\Console\Command;

class SyncLotsStatuses extends Command
{
    protected $signature = 'tmp:lots:statuses:sync';

    public function handle(): void
    {
        $this->output->progressStart(Lot::countApproximate());
        Lot::orderBy('lot_id', 'DESC')->chunk(1000, function ($items) {
            /** @var Lot $lot */
            foreach ($items as $lot) {
                $lot->setParsedStatus();
                $this->output->progressAdvance();
            }
        });
        $this->output->progressFinish();
    }
}
