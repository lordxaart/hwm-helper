<?php

declare(strict_types=1);

namespace App\HWM\Objects;

use Carbon\Carbon;
use Spatie\DataTransferObject\DataTransferObject;

class LotOperation extends DataTransferObject
{
    public const OPERATION_CREATE = 'create';
    public const OPERATION_SALE = 'sale';
    public const OPERATION_RATE = 'rate';
    public const OPERATION_END = 'end';
    public const OPERATION_UNDEFINED = 'undefined';

    public string $type;
    public Carbon $time;
    public ?int $buyed_quantity;
    public ?int $current_quantity;
    public ?int $current_price;
    public ?int $rate_diff;
    public ?int $buyer_id;
    public ?string $buyer_name;

    public function __construct(array $parameters = [])
    {
        $parameters['time'] = Carbon::parse($parameters['time']);

        parent::__construct($parameters);
    }

    public static function getTypes(): array
    {
        return [
            self::OPERATION_CREATE,
            self::OPERATION_SALE,
            self::OPERATION_RATE,
            self::OPERATION_END,
            self::OPERATION_UNDEFINED,
        ];
    }
}
