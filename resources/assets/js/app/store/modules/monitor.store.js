import Vue from 'vue';
import findIndex from 'lodash/findIndex';

import { objectToQueryString } from 'JS/app/utils/helpers';
import request from 'JS/app/utils/request';

const st = {
    arts: [],
    loadingArts: false,
    loadingAddArt: false,
    loadingRemoveArt: false,
    loadingUpdateArt: false,
};

// Getter functions
const getters = {
    getArts: (state) => state.arts,
    getLoadingArts: (state) => state.loadingArts,
    getLoadingAddArt: (state) => state.loadingAddArt,
    getLoadingUpdateArt: (state) => state.loadingUpdateArt,
    getLoadingRemoveArt: (state) => state.loadingRemoveArt,
};

// Actions
const actions = {
    async loadArts({ state, commit }, params = {}) {
        if (state.loadingArts) {
            return;
        }

        // eslint-disable-next-line no-param-reassign
        params.include = 'art';
        commit('START_LOADING');
        try {
            const response = await request.get(`/api/v1/monitor/arts${objectToQueryString(params)}`);
            commit('SET_ARTS', response.data.data);
            commit('END_LOADING');
        } catch (e) {
            commit('END_LOADING');
            throw e;
        }
    },
    async addArt({ commit }, artId) {
        commit('START_LOADING', 'loadingAddArt');

        try {
            const response = await request.post('/api/v1/monitor/arts?include=art', {
                art_id: artId,
            });
            if (response.status === 201) {
                commit('ADD_ART', response.data.data);
            }

            commit('END_LOADING', 'loadingAddArt');
        } catch (e) {
            commit('END_LOADING', 'loadingAddArt');
            throw e;
        }
    },
    async removeArt({ commit }, artId) {
        commit('START_LOADING', 'loadingRemoveArt');

        try {
            await request.delete(`/api/v1/monitor/arts/${artId}`);
            commit('END_LOADING', 'loadingRemoveArt');
            commit('REMOVE_ART', artId);
        } catch (e) {
            commit('END_LOADING', 'loadingRemoveArt');
            throw e;
        }
    },
    async changeArtState({ commit }, { artId, state }) {
        commit('START_LOADING', 'loadingUpdateArt');

        const action = state ? 'enable' : 'disable';

        try {
            await request.put(`/api/v1/monitor/arts/${artId}/${action}`);
            commit('END_LOADING', 'loadingUpdateArt');
            commit('UPDATE_ART_STATE', { artId, value: state });
        } catch (e) {
            commit('END_LOADING', 'loadingUpdateArt');
            throw e;
        }
    },
    updateArtsList({ commit }, data) {
        commit('SET_ARTS', data);
    },
    updateArtsPositions({ commit }, data) {
        commit('SORT_ARTS', data);
    },
};

// Mutations
const mutations = {
    START_LOADING(state, key = 'loadingArts') {
        state[key] = true;
    },
    END_LOADING(state, key = 'loadingArts') {
        state[key] = false;
    },
    SET_ARTS(state, data) {
        if (!Array.isArray(data)) {
            throw new Error('Data is not array');
        }
        if (!data.length) {
            return;
        }
        state.arts = data;
    },
    ADD_ART(state, data) {
        if (!data.art.hwmId) {
            throw new Error('Art not include information about art');
        }

        state.arts.push(data);
    },
    REMOVE_ART(state, artId) {
        const index = findIndex(state.arts, ((art) => art.hwmId === artId));

        if (index === -1) {
            throw new Error('Not found art in list');
        }

        state.arts.splice(index, 1);
    },
    UPDATE_ART_STATE(state, { artId, value }) {
        const index = findIndex(state.arts, ((art) => art.hwmId === artId));

        if (index === -1) {
            throw new Error('Not found art in list');
        }

        const art = state.arts[index];
        art.isActive = !!value;

        Vue.set(state.arts, index, art);
    },
    SORT_ARTS(state, sortingArr) {
        state.arts.sort((a, b) => sortingArr.indexOf(a.hwmId) - sortingArr.indexOf(b.hwmId));
    },
};

export default {
    namespaced: true,
    state: st,
    getters,
    actions,
    mutations,
};
