<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('art_market_categories', 'market_categories');
        Schema::rename('art_monitored_filters', 'monitored_art_filters');
        Schema::rename('art_monitored_by_user', 'monitored_arts');
        Schema::rename('market_lots', 'lots');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
