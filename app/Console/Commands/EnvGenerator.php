<?php

declare(strict_types=1);

namespace App\Console\Commands;

use Exception;

class EnvGenerator extends Command
{
    /** @var string  */
    protected $signature = 'generate:env {env=testing}';

    /** @var string  */
    protected $description = 'Generate env';

    public function handle(): void
    {
        $env = strval($this->argument('env'));

        if (method_exists($this, $env)) {
            $file = $this->$env();
            $this->info("Generated $file");
        }
    }

    public function testing(): string
    {
        $values = [
            'APP_ENV' => 'testing',
            'LOG_CHANNEL' => 'testing',
            'DB_HOST' => env('DB_TEST_HOST'),
            'DB_DATABASE' => env('DB_TEST_DATABASE'),
            'DB_USERNAME' => env('DB_TEST_USERNAME'),
            'DB_PASSWORD' => env('DB_TEST_PASSWORD'),
            'DB_PORT' => env('DB_TEST_PORT', env('DB_PORT')),
            'ENABLE_GA_EVENTS' => 'false',
        ];

        $envContent = '';

        foreach ($values as $key => $value) {
            $envContent .= $key . '=' . $value . PHP_EOL;
        }

        $path = base_path('.env.testing');
        file_put_contents($path, $envContent);

        return $path;
    }

    public function dusk(): string
    {
        $envName = '.env.dusk.' . env('APP_ENV');

        copy(base_path('.env'), base_path($envName));

        if (file_exists(base_path('.env.testing'))) {
            $envTesting = $this->envFileToArray(base_path('.env.testing'));
            $envDusk = $this->envFileToArray(base_path($envName));

            foreach ($envTesting as $key => $value) {
                if (array_key_exists($key, $envDusk)) {
                    $envDusk[$key] = $value;
                }
            }

            file_put_contents(base_path($envName), $this->arrayToEnv($envDusk));

            return base_path($envName);
        }

        return '';
    }

    protected function envFileToArray(string $path): array
    {
        $content = strval(file_get_contents($path));

        $lines = explode(PHP_EOL, $content);

        $data = [];

        foreach ($lines as $line) {
            if (!$line || str_starts_with($line, '#')) {
                continue;
            }

            try {
                [$key, $value] = explode('=', $line, 2);
            } catch (Exception) {
                continue;
            }

            $data[$key] = $value;
        }

        return $data;
    }

    protected function arrayToEnv(array $data): string
    {
        $content = '';

        foreach ($data as $key => $value) {
            $content .= $key . '=' . $value . PHP_EOL;
        }

        return $content;
    }
}
