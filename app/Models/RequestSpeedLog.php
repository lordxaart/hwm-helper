<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequestSpeedLog extends Model
{
    public const MAX_LOGS_IN_QUEUE = 20;

    /** @var array  */
    protected $fillable = ['uniqid', 'speed', 'url', 'method', 'code', 'headers', 'proxy', 'html', 'user_agent'];

    /** @var array  */
    protected $casts = [
        'headers' => 'array',
    ];

    /** @var bool  */
    public $timestamps = false;

    public static function addRecord(array $data): void
    {
        // convert some data
        $data['created_at'] = $data['created_at'] ?? now()->format('Y-m-d H:i:s');
        $data['speed'] = isset($data['speed']) ? round($data['speed'], 5) : null;

        self::insert($data);
    }
}
