<?php

declare(strict_types=1);

namespace App\HWM\Entities;

use App\HWM\Helpers\HwmLink;
use App\HWM\Entities\Interfaces\HasLinkOnPage;
use App\HWM\Repositories\CharacterRepository;
use App\HWM\Repositories\Interfaces\EntityRepositoryInterface;

class Character extends HwmEntityWithModel implements HasLinkOnPage
{
    public const STATUS_ONLINE = 'online';
    public const STATUS_BLOCKED = 'blocked';
    public const STATUS_PRISON = 'prison';
    public const STATUS_INACTIVE = 'inactive';
    public const STATUS_OFFLINE = 'offline';
    public const STATUS_UNDEFINED = UNDEFINED;

    public string $login;
    public ?string $status = null;
    public int $level;
    public int $experience;
    public array $resources;
    public array $guilds;
    public array $fractions;

    public function __construct(string $hwmId, array $parameters = [])
    {
        $parameters['title'] = $parameters['login'] ?? UNDEFINED;

        parent::__construct($hwmId, $parameters);
    }

    public static function getStatuses(): array
    {
        return [
            self::STATUS_ONLINE,
            self::STATUS_BLOCKED,
            self::STATUS_PRISON,
            self::STATUS_INACTIVE,
            self::STATUS_OFFLINE,
            self::STATUS_UNDEFINED,
        ];
    }

    public function getHwmLink(): string
    {
        return HwmLink::get(HwmLink::CHARACTER_INFO, ['id' => $this->getHwmId()]);
    }

    public function getRepository(): EntityRepositoryInterface
    {
        return app(CharacterRepository::class);
    }

    public function toArray(): array
    {
        return array_merge(parent::toArray(), ['hwm_id' => $this->getHwmId()]);
    }
}
