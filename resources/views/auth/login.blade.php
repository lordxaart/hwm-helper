@extends('layout.app', ['js' => false])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-4 mx-auto">
            @include('elements.notification')

            <div class="card">

                <div class="card-body">
                    <form class="form-signin" method="POST" action="{{ route('login') }}">
                        @csrf

                        <legend class="text-center mb-4">@{{ $t('app.auth.enter_form') }}</legend>

                        {{-- Email --}}
                        <div class="form-group">
                            <label for="email" class="col-sm-4 col-form-label text-md-right sr-only">@{{ $t('validation.attributes.email') }}</label>

                            <input
                                id="email"
                                type="email"
                                class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                name="email"
                                value="{{ old('email') }}"
                                :placeholder="$t('validation.attributes.email')"
                                required
                                autocomplete="username"
                                autofocus
                            >

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('email') !!}</strong>
                                </span>
                            @endif
                        </div>

                        {{-- Password --}}
                        <div class="form-group">
                            <label for="password" class="col-md-4 col-form-label text-md-right sr-only">@{{ $t('validation.attributes.password') }}</label>

                            <input
                                id="password"
                                type="password"
                                class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                name="password"
                                :placeholder="$t('validation.attributes.password')"
                                required
                                autocomplete="current-password"
                            >

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('password') !!}</strong>
                                </span>
                            @endif
                        </div>

                        {{-- Google reCAPTCHA --}}
{{--                        @include('auth.recaptcha')--}}

                        {{-- Via Remember --}}
                        <div class="d-none">
                            <input class="custom-control-input" type="checkbox" id="check-remember" name="remember" checked="">
                        </div>

                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary btn-block mb-2">
                                @{{ $t('app.auth.enter') }}
                            </button>

                            <a class="btn btn-sm btn-link d-block" href="{{ route('register') }}">
                                @{{ $t('app.auth.registration') }}
                            </a>
                            <a class="btn btn-sm btn-link d-block" href="{{ route('password.request') }}">
                                @{{ $t('app.auth.forget_password') }}
                            </a>
                        </div>

                        <div class="form-group mb-0">
                            @include('auth.socials', ['action' => 'enter'])
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
