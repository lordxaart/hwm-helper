<?php

declare(strict_types=1);

namespace App\HWM\Parsers\Traits;

use App\HWM\Entities\BaseLot;
use App\HWM\Enums\EntityType;
use App\HWM\Enums\LotType;
use App\HWM\Exceptions\ParserError;
use App\HWM\Exceptions\ParserErrors\UnexpectedPage;
use App\HWM\Helpers\HwmHelper;
use App\HWM\Helpers\HwmLink;
use App\Models\MarketCategory;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use PHPHtmlParser\Dom\HtmlNode;

trait MarketTrait
{
    /**
     * @throws ParserError
     * @throws UnexpectedPage
     */
    public function getMarketLots(): array
    {
        $lots = [];

        if (!$mainBlock = $this->getMainMarketBlock()) {
            throw new UnexpectedPage('market');
        }

        /** @var HtmlNode $tr */
        foreach ($mainBlock->find('tr.wb') as $tr) {
            try {
                // columns
                $td1 = $tr->firstChild();
                $td2 = $td1->nextSibling();
                $td3 = $td2->nextSibling();
                $td4 = $td3->nextSibling();
                $td5 = $td4->nextSibling();

                // Default values
                $entityType = UNDEFINED;
                $entityImage = null;
                $craft = null;
                $currentStrength = null;
                $baseStrength = null;
                $crc = null;

                // lot ID
                $lotId = intval($td1->find('a', 0)->getAttribute('name'));

                // CRC
                if ($a = $td1->find('a.pi', 0)) {
                   $href = $a->getAttribute('href');
                   parse_str($href, $params);
                   $crc = $params['crc'] ?? null;
                }

                // price
                $priceTd = $td3->find('table > tr > td > div > table > tr > td', 1);
                $price = $priceTd ? HwmHelper::clearIntValue($priceTd->text) : 0;

                // Quantity
                $result = getTextByRegex($td1->innerHtml, '/[0-9]+\sшт\./');
                $quantity = intval($result) ?: 1;

                // Entity Title
                $entityTitle = getTextBetweenValues($td1->innerHtml, '</a> - ', '&nbsp;');

                // Image
                if ($img = $td1->find('table > tr > td > img', 0)) {
                    $entityImage = $img->getAttribute('src');
                }

                // Strength
                if ($durabilityBlock = $td1->find('.art_durability_hidden1', 0)) {
                    [$currentStrength, $baseStrength] = explode('/', $durabilityBlock->text);
                    $currentStrength = intval($currentStrength);
                    $baseStrength = intval($baseStrength);
                } elseif (str_contains($td1->innerHtml, $this->getText('strength'))) {
                    $text = getTextByRegex($td1->innerHtml, '/' . $this->getText('strength') . ':\s(\d+\/\d+)/');
                    $text = explode(' ', $text)[1];
                    [$currentStrength, $baseStrength] = explode('/', $text);
                    $currentStrength = intval($currentStrength);
                    $baseStrength = intval($baseStrength);
                }

                // Time
                $timeSrt = trim($td4->text);
                $startTime = null;
                $endTime = null;
                if (preg_match('/\d{2}\-\d{2}/', $timeSrt)) { // 13-01 08:24
                    $startTime = Carbon::createFromFormat('d-m H:i', $timeSrt, HwmHelper::HEROES_TZ)
                        ->timezone(getDefaultTimezone());
                } else {
                    $endTime = self::parseTimeStr($timeSrt)->timezone; // need parse str
                    if ($endTime) {
                        $endTime = Carbon::parse($endTime, HwmHelper::HEROES_TZ)->timezone(getDefaultTimezone());
                    }
                }

                // Seller info
                $link = $td5->find('a.pi', 0);
                $sellerId = $link ? intval(str_replace('pl_info.php?id=', '', $link->getAttribute('href'))) : null;
                $sellerName = $link ? $link->find('b', 0)->text : null;

                if (str_contains($td2->innerHtml, $this->getText('buy_now'))) {
                    $lotType = LotType::SALE->value;
                } else {
                    if (str_contains($td2->innerHtml, $this->getText('blitz'))) {
                        $lotType = LotType::BLITZ->value;
                        // Blitz price
                        $blitzPrice = HwmHelper::clearIntValue($td2->find('table > tr > td', 1)->innerHtml);
                    } else {
                        $lotType = LotType::AUCTION->value;
                    }
                    // Bids
                    $bids = intval($td2->text);
                    // Last Buyer Name
                    $buyerLink = $td3->find('a.pi', 0);
                    $lastBuyerName = $buyerLink ? $td3->find('a.pi', 0)->text : null;
                    // // Last Buyer Bid
                    $lastBuyerBid = $price ?? 0;
                }

                // Entity
                if (str_contains($td1->innerHtml, '1/100')) {
                    // ART PART
                    $entityType = EntityType::ART_PART->value;
                    // Entity Title
                    $entityTitle = explode('1/100', $entityTitle)[0];
                    $entityHwmId = getTextBetweenValues($td1->innerHtml, HwmLink::ART_INFO . '?id=', '\'');
                } elseif (str_contains($td1->innerHtml, HwmLink::ART_INFO)) {
                    // ART
                    $entityType = EntityType::ART->value;

                    if ($lotBlock = $td1->find('.arts_info', 0)) {
                        $img = $lotBlock->find('img', 1);
                        $entityImage = $img ? $img->getAttribute('src') : null;

                        $link = $lotBlock->find('a', 0);
                        $href = $link ? $link->getAttribute('href') : '';
                        $queryString = Arr::last(explode('?', $href));
                        parse_str($queryString, $queryParameters);

                        $entityHwmId = $queryParameters['id'];
                        $artUid = intval($queryParameters['uid'] ?? 0) ?? null;
                        $artCrc = $queryParameters['crc'] ?? null;
                    }

                    // Craft
                    $craftRegex = HwmHelper::getRegex('craft_full');
                    if (preg_match($craftRegex, $entityTitle, $result)) {
                        $craft = str_replace(['[', ']'], '', $result[0]);
                        $entityTitle = trim(str_replace($result[0], '', $entityTitle));
                    }
                } elseif ($element = $this->stringHasElementTitle($td1->innerHtml)) {
                    // Element
                    $entityType = EntityType::ELEMENT->value;
                    $entityHwmId = $element->getHwmId();
                    $entityTitle = $element->title;
                } elseif ($resource = $this->stringHasResourceTitle($td1->innerHtml)) {
                    // Resource
                    $entityType = EntityType::RESOURCE->value;
                    $entityHwmId = $resource->getHwmId();
                    $entityTitle = $resource->title;
                } elseif (str_contains($td1->innerHtml, HwmLink::OBJECT_INFO)) {
                    // Object Share
                    $entityType = EntityType::OBJECT_SHARE->value;
                    $entityHwmId = getTextBetweenValues($td1->innerHtml, HwmLink::OBJECT_INFO . '?id=', '"');
                    $entityTitle = getTextByRegex($td1->innerHtml, '/\W+\[\d+\]/');
                    $entityTitle = str_replace('/>', '', $entityTitle);
                    $entityTitle = preg_replace('/\[\d+\]/', '', $entityTitle);
                    $entityTitle = trim($entityTitle);
                } elseif (str_contains($td1->innerHtml, HwmLink::HOUSE_INFO)) {
                    // House
                    $entityType = EntityType::HOUSE->value;
                    $entityHwmId = getTextBetweenValues($td1->innerHtml, HwmLink::HOUSE_INFO . '?id=', '"');
                    $entityTitle = getTextBetweenValues($td1->innerHtml, '<br />', '&nbsp;');
                } elseif (str_contains($td1->innerHtml, $this->getText('certificat'))) {
                    // Certificat
                    $entityType = EntityType::CERTIFICATE->value;
                    $entityTitle = getTextBetweenValues($td1->innerHtml, '1%<br />', '&nbsp;<b>');
                    $input = $td5->find('[name="art_type"]', 0);
                    $entityHwmId = $input ? $input->getAttribute('value') : Str::slug($entityTitle);
                }
            } catch (\Exception $e) {
                throw new ParserError($e->getMessage(), 0, $e);
            }

            $lots[] = [
                'lot_id' => $lotId,
                'crc' => $crc,
                'lot_type' => $lotType,
                'entity_type' => $entityType,
                'entity_id' => $entityHwmId ?? null,
                'entity_title' => $entityTitle,
                'art_uid' => $artUid ?? null,
                'art_crc' => $artCrc ?? null,
                'craft' => $craft ?? null,
                'image' => $entityImage,
                'current_strength' => $currentStrength ?? null,
                'base_strength' => $baseStrength ?? null,
                'quantity' => $quantity,
                'price' => $price,
                'started_at' => $startTime?->format(config('app.date_format')),
                'ended_at' => $endTime?->format(config('app.date_format')),
                'seller_name' => $sellerName,
                'seller_id' => $sellerId,
                'bids' => $bids ?? null,
                'last_buyer_bid' => $lastBuyerBid ?? null,
                'last_buyer_name' => $lastBuyerName ?? null,
                'blitz_price' => $blitzPrice ?? null,
            ];
        }

        $this->validateData('market_lots', $lots);

        return $lots;
    }

    /**
     * @throws ParserError
     * @throws UnexpectedPage
     */
    public function getListArtsFromMarket(): array
    {
        if (!$this->getMainMarketBlock()) {
            throw new UnexpectedPage('market');
        }

        $parsedArts = [];

        $options = $this->find('select[name=ss2] option', null);

        try {
            foreach ($options as $item) {
                if ($item->hasAttribute('value') && str_contains($item->getAttribute('value'), '#')) {
                    [$category, $artId] = explode('#', $item->getAttribute('value'));
                    $parsedArts[] = [
                        'hwm_id' => $artId,
                        'title' => $item->text,
                        'market_category_id' => $category,
                    ];
                }
            }
            $text = $this->getDom()->getOriginalContent();
            $partsStartText = "function a_part(){ document.getElementById('mark_info_part').innerHTML";
            $partsBlock = getTextBetweenValues($text, $partsStartText, "';}", false);
            $partsBlocks = explode('<a href="/auction.php?', $partsBlock);
            foreach ($partsBlocks as $part) {
                if (str_contains($part, 'art_type=')) {
                    $parsedArts[] = [
                        'hwm_id' => getTextBetweenValues($part, 'art_type=part_', '">'),
                        'title' => trim(getTextBetweenValues($part, 'font-size:9px;">', '(')),
                        'market_category_id' => MarketCategory::ART_PART_MARKET_ID,
                    ];
                }
            }
        } catch (\Exception $e) {
            throw new ParserError($e->getMessage());
        }

        $this->validateData('art_market_list', $parsedArts);

        return $parsedArts;
    }

    /**
     * @return \App\HWM\Parsers\CustomDom|null
     */
    protected function getMainMarketBlock(): mixed
    {
        if ( $block = $this->find('body > center > table > tr > td > table > tr > td.wbwhite > table') )
            return $block;

        return $this->find('table');
    }

    public static function parseTimeStr(string $timeSrt): ?Carbon
    {
        $time = now();

        collect(explode('.', $timeSrt))->map(function ($item) {
            return trim($item);
        })->filter(function ($item) {
            return $item;
        })->each(function ($item) use ($time) {
            if (strpos($item, 'мин')) {
                $time->addMinutes(intval($item));
            } elseif (strpos($item, 'ч')) {
                $time->addHours(intval($item));
            } elseif (strpos($item, 'д')) {
                $time->addDays(intval($item));
            }
        });

        return $time !== now() ? $time : null;
    }

    public static function handleLongMarkerResponse(string $content): string
    {
        $approximateLotsLimit = 100;
        $approximateLengthLot = 3000;
        $length = $approximateLotsLimit * $approximateLengthLot;

        $startPos = mb_strpos($content, 'Товар');
        if ( $startPos === false )
            return $content;

        $startPos += mb_strlen('Товар') + 5; // [</td>] = 5 chars
        $newHtml = mb_substr($content, $startPos, $length);
        $endHtml = mb_substr($newHtml, -5000);
        $endPost =  5000 - mb_strrpos($endHtml, '</tr><tr bgcolor');
        $newHtml = mb_substr($newHtml, 0, -$endPost + 5); // [</tr>] = 5 chars
        $trPos = mb_strpos($newHtml, '<tr');
        if ( !$trPos )
            return $content;

        $newHtml = mb_substr($newHtml, $trPos);
        $newHtml = '<table>' . $newHtml . '</table>';

        return $newHtml;
    }
}
