<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Services\LangGenerator\Generator;
use Exception;

class LangGenerator extends Command
{
    /** @var string  */
    protected $signature = 'generate:lang {--umd} {--multi} {--with-vendor} {--file-name=} {--lang-files=} {--format=es6} {--multi-locales}';

    /** @var string  */
    protected $description = 'Generates a vue-i18n|vuex-i18n compatible js array out of project translations';

    /**
     * Execute the console command.
     * @throws Exception
     */
    public function handle(): void
    {
        $root = base_path() . config('lang-generator.langPath');
        $config = config('lang-generator');

        // options
        $umd = boolval($this->option('umd'));
        $multipleFiles = boolval($this->option('multi'));
        $withVendor = boolval($this->option('with-vendor'));
        $fileName = strval($this->option('file-name'));
        $langFiles = strval($this->option('lang-files'));
        $format = strval($this->option('format'));
        $multipleLocales = boolval($this->option('multi-locales'));

        if ($umd) {
            // if the --umd option is set, set the $format to 'umd'
            $format = 'umd';
        }

        if (!$this->isValidFormat($format)) {
            throw new \RuntimeException('Invalid format passed: ' . $format);
        }

        if ($multipleFiles || $multipleLocales) {
            $files = (new Generator($config))->generateMultiple($root, $format, $multipleLocales);

            if ($config['showOutputMessages']) {
                $this->info("Written to : " . $files);
            }

            return;
        }

        if ($langFiles) {
            $langFiles = explode(',', $langFiles);
        }

        $data = (new Generator($config))->generateFromPath($root, $format, $withVendor, $langFiles);


        $jsFile = $this->getFileName($fileName);
        file_put_contents($jsFile, $data);

        if ($config['showOutputMessages']) {
            $this->info("Written to : " . $jsFile);
        }
    }

    private function getFileName(string $fileNameOption): string
    {
        if ($fileNameOption) {
            return base_path() . $fileNameOption;
        }

        return base_path() . config('lang-generator.jsFile');
    }

    private function isValidFormat(string $format): bool
    {
        $supportedFormats = ['es6', 'umd', 'json'];

        return in_array($format, $supportedFormats);
    }
}
