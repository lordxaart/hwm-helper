@php
    /** @var \App\Models\Art $art */
    $art = $entry;
@endphp

@foreach($art->images as $key => $image)
    @if(file_exists($image->getRealPath()))
    <div class="card d-inline-block mr-1 mb-1 align-top text-center" style="min-width: 230px; width: {{ $image->toArray()['width'] }}px">
        <img src="{{ $image->getUrl() }}" alt="{{ $key }}" title="{{ $key }}" width="{{ $image->toArray()['width'] }}" height="{{ $image->toArray()['height'] }}">
        <div class="card-body bg-secondary">
            <p class="card-text">
                <strong>Size:</strong> {{ formatBytes(filesize($image->getRealPath())) }}, <strong>Width:</strong> {{ $image->toArray()['width'] }}px, <strong>Height:</strong> {{ $image->toArray()['height'] }}px, <strong>Ext: </strong>{{ $image->getImageExt() }}
            </p>
        </div>
    </div>
    @else
        <div class="alert alert-danger">Not found image {{ $image->getImageFilename() }}</div>
    @endif
@endforeach
