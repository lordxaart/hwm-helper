<?php

declare(strict_types=1);

namespace App\HWM\HtmlParsers\Interfaces;

interface HtmlNodeInterface
{
    public static function loadFromString(string $html): HtmlNodeInterface;

    public function find(string $selector): ?HtmlNodeInterface;

    /**
     * @return HtmlNodeInterface[]
     */
    public function findAll(string $selector): array;

    public function getNextSibling(): ?HtmlNodeInterface;

    public function getFirstChild(): ?HtmlNodeInterface;

    public function getAttribute(string $attribute): ?string;

    public function asHtml(): string;

    public function asString(): string;
}
