<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UpdateCategoryRequest;
use App\Models\BaseCategory;
use App\Models\MarketCategory;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;

/**
 * Class UserCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class BaseCategoryCrudController extends CrudController
{
    use ListOperation;
    use ShowOperation;
    use UpdateOperation;

    public function setup()
    {
        $this->crud->addClause('withCount', 'arts');
        $this->crud->with('arts');
        $this->crud->addClause('orderBy', 'id');

        $this->crud->set('show.setFromDb', false);
    }

    protected function setupListOperation()
    {
        $this->crud->setDefaultPageLength(100);
        $this->crud->setColumns([
            ['name' => 'id', 'label' => 'ID', 'type' => 'number'],
            ['name' => 'hwm_id', 'label' => 'HWM ID', 'type' => 'text'],
            ['name' => 'title', 'label' => 'Title', 'type' => 'text'],
            [
                'name' => 'arts_count',
                'label' => 'Count arts',
                'type' => 'number',
                'orderable' => true,
                'orderLogic' => function ($query, $column, $columnDirection) {
                    return $query->orderBy('arts_count', $columnDirection);
                }],
        ]);
    }

    protected function setupShowOperation()
    {
        $this->crud->addColumns([
            ['name' => 'hwm_id', 'label' => 'ID'],
            ['name' => 'title', 'label' => 'Title'],
            ['name' => 'arts', 'label' => 'Arts', 'type' => 'view', 'view' => 'crud::columns.arts_preview'],
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->crud->setValidation(UpdateCategoryRequest::class);

        $this->crud->addFields([
            ['name' => 'title', 'label' => 'Title', 'type' => 'text_transable',],
        ]);
    }

    protected function update($id): \Illuminate\Http\Response|array
    {
        $this->crud->hasAccessOrFail('update');

        $request = $this->crud->validateRequest();

        /** @var BaseCategory $model */
        $model = $this->crud->model::findOrFail($id);

        $model->title = $request->title;

        if ($model instanceof MarketCategory) {
            $model->only_art = $request->only_art;
        }

        $model->save();

        // show a success message
        \Alert::success(trans('backpack::crud.update_success'))->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($model->getKey());
    }
}
