<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckIfAdmin
{
    /**
     * Checked that the logged in user is an administrator.
     *
     * --------------
     * VERY IMPORTANT
     * --------------
     * If you have both regular users and admins inside the same table,
     * change the contents of this method to check that the logged in user
     * is an admin, and not a regular user.
     *
     * @return bool [description]
     */
    private function checkIfUserIsAdmin(User $user): bool
    {
        return $user->isSuperAdmin();
    }

    private function respondToUnauthorizedRequest(Request $request): Response
    {
        if ($request->ajax() || $request->wantsJson()) {
            return response(trans('backpack::base.unauthorized'), 401);
        } else {
            return redirect()->guest(route('login'));
        }
    }

    /**
     * Handle an incoming request.
     */
    public function handle(Request $request, Closure $next): mixed
    {
        $user = user();

        if (!$user) {
            return $this->respondToUnauthorizedRequest($request);
        }

        if (! $this->checkIfUserIsAdmin($user)) {
            return $this->respondToUnauthorizedRequest($request);
        }

        // Set EN for all admin panel
//        app()->setLocale('en');

        return $next($request);
    }
}
