<?php

declare(strict_types=1);

namespace App\Services\SearchLots\ValueObjects;

use InvalidArgumentException;

class OrderBy
{
    public const ASC = 'asc';
    public const DESC = 'desc';

    private array $sorts = [];

    public function __construct(array $sorts)
    {
        foreach ($sorts as $field => $desc) {
            $this->addSort((string)$field, strtolower($desc ?: self::ASC));
        }
    }

    /**
     * @throws InvalidArgumentException
     */
    public function addSort(string $field, string $direction = self::ASC): void
    {
        $direction = strtolower($direction);

        if (!in_array($field, $this->getAllowedFields())) {
            throw new InvalidArgumentException(
                sprintf('Field %s is not supported for sort', $field)
            );
        }

        if (!in_array($direction, [self::ASC, self::DESC])) {
            throw new InvalidArgumentException(
                sprintf('Direction %s is not supported', $field)
            );
        }

        $this->sorts[$field] = $direction;
    }

    public function getOrders(): array
    {
        return $this->sorts;
    }

    private function getAllowedFields(): array
    {
        return [
            'current_strength',
            'base_strength',
            'quantity',
            'price',
            'price_per_fight',
            'blitz_price',
            'seller_name',
            'seller_id',
            'started_at',
            'buyed_at',
            'ended_at',
        ];
    }
}
