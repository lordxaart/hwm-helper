<?php

declare(strict_types=1);

namespace App\Notifications\User;

use Illuminate\Notifications\Messages\MailMessage;

class VerifyEmail extends \Illuminate\Auth\Notifications\VerifyEmail
{
    /**
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable): MailMessage
    {
        $verificationUrl = $this->verificationUrl($notifiable);

        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $verificationUrl);
        }

        return (new MailMessage())
            ->subject(trans('app.notifications.verify_email.subject'))
            ->line(trans('app.notifications.verify_email.line_1'))
            ->action(trans('app.notifications.verify_email.action'), $verificationUrl)
            ->line(trans('app.notifications.verify_email.line_2'));
    }
}
