<?php

declare(strict_types=1);

namespace App\Notifications\Admin;

use App\Services\Notifications\Channels\Telegram\Entities\TelegramFile;
use App\Services\Notifications\Channels\Telegram\Entities\TelegramMessage;
use App\Services\Notifications\Enums\Channel;
use App\Core\Enums\Queue;
use App\Models\Feedback;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewFeedback extends Notification implements ShouldQueue
{
    use Queueable;

    public Feedback $feedback;

    public function __construct(Feedback $feedback)
    {
        $this->queue = Queue::NOTIFICATIONS->value;

        $this->feedback = $feedback;
    }

    public function via(): array
    {
        return [
            Channel::TELEGRAM->getLaravelChannelName(),
            Channel::MAIL->getLaravelChannelName(),
        ];
    }

    public function toMail(): MailMessage
    {
        $message = (new MailMessage())
            ->subject(trans('app.feedback.new_feedback'))
            ->markdown('emails.feedback', [
                'feedback' => $this->feedback,
            ]);

        foreach ($this->feedback->attachments as $file) {
            $tmpFile = saveTmpFile(base64_decode($file));
            if ($tmpFile) {
                $message->attach($tmpFile);
            }
        }

        return $message;
    }

    public function toTelegram(): TelegramMessage | TelegramFile
    {
        $content = trans('app.feedback.new_feedback');
        $content .= PHP_EOL . $this->feedback->data['message'];

        foreach ($this->feedback->data as $key => $value) {
            $key = strval($key);
            if ($key != 'message') {
                $key = mb_ucfirst(str_replace('_', ' ', $key));
                $content .= PHP_EOL . " - *$key*: $value";
            }
        }

        $countFiles = count($this->feedback->attachments);
        $content .= PHP_EOL . ' - *' . mb_ucfirst(trans('app.main.attachments')) . '*: ' . trans('app.main.count_files', ['count' => $countFiles]);
        $content .= PHP_EOL . ' - *' . mb_ucfirst(trans('app.main.created')) . '*: ' . $this->feedback->created_at?->diffForHumans();

        if (!$countFiles) {
            $message = TelegramMessage::create($content)->setTypeContent('new');
        } else {
            $message = TelegramFile::create()
                ->setTypeContent('new')
                ->content($content);
            $file = $this->feedback->attachments[0];

            $tmpFile = saveTmpFile(base64_decode($file));
            if ($tmpFile) {
                $message->file($tmpFile, 'photo');
            }
        }

        return $message;
    }
}
