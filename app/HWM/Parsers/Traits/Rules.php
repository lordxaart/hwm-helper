<?php

declare(strict_types=1);

namespace App\HWM\Parsers\Traits;

use App\HWM\Entities\Character;
use App\HWM\Entities\Lot;
use App\HWM\Objects\LotOperation;

trait Rules
{
    public function getRules(string $type): array
    {
        $dateFormat = config('app.date_format');
        $lotTypes = implode(',', Lot::getLotTypes());
        $entityTypes = implode(',', Lot::getEntityTypes());
        $characterStatuses = implode(',', Character::getStatuses());
        $operationTypes = implode(',', LotOperation::getTypes());

        $baseLot = [
            'lot_id' => 'required|integer|min:1',
            'lot_type' => 'required|in:' . $lotTypes,
            'entity_type' => 'required|in:' . $entityTypes,
            'entity_id' => 'present|string',
            'entity_title' => 'required|string|max:' . config('hwm.rules.max_art_title_length'),
            'art_uid' => 'present|nullable|integer',
            'art_crc' => 'present|nullable|string|size:10',
            'craft' => 'present|nullable|string|max:15|craft',
            'current_strength' => 'present|nullable|integer|min:0|lte:base_strength',
            'base_strength' => 'present|nullable|integer|min:1|gte:current_strength',
            'price' => 'required|integer|min:1',
            'blitz_price' => 'present|nullable|integer|min:1',
            'quantity' => 'required|integer|min:0',
            'ended_at' => 'present|nullable|date_format:' . $dateFormat,
            'seller_name' => 'required|string|max:' . config('hwm.rules.max_login_length'),
            'seller_id' => 'required|integer',
        ];

        $rules = [
            'art' => [
                'hwm_id' => 'required|string|max:' . config('hwm.rules.max_hwm_id_length'),
                'title' => 'required|string|max:' . config('hwm.rules.max_art_title_length'),
                'image' => 'required|url',
                'need_level' => 'present|nullable|integer|min:1',
                'strength' => 'required|integer|min:1',
                'oa' => 'present|nullable|integer|min:1',
                'repair' => 'present|nullable|integer|min:1',
                'description' => 'nullable|string',
                'modifiers' => 'present|nullable|array',
                'set_id' => 'present|nullable|string|max:45',
                'from_shop' => 'present|boolean',
                'as_from_shop' => 'present|boolean',
                'shop_price' => 'present|nullable|integer|min:0',
            ],
            'art_market_list' => [
                'hwm_id' => 'required|string|max:' . config('hwm.rules.max_hwm_id_length'),
                'title' => 'required|string|max:'  . config('hwm.rules.max_art_title_length'),
                'market_category_id' => 'required|string|max:' . config('hwm.rules.max_hwm_id_length'),
            ],
            'market_lots' => array_merge($baseLot, [
                'image' => 'present|nullable|url',
                'bids' => 'present|nullable|integer|min:0',
                'last_buyer_bid' => 'present|nullable|integer|min:0',
                'last_buyer_name' => 'present|nullable|string|max:' . config('hwm.rules.max_login_length'),
                'crc' => 'present|nullable|string',
                'started_at' => 'present|nullable|date_format:' . $dateFormat,
            ]),
            'lot' => array_merge($baseLot, [
                'started_at' => 'required|date_format:' . $dateFormat,
                'buyed_at' => 'present|nullable|string|date_format:' . $dateFormat,
                'operations.*' => 'array',
                'operations.*.time' => 'required|',
                'operations.*.type' => 'required|in:' . $operationTypes,
                'operations.*.buyed_quantity' => 'present|nullable',
                'operations.*.current_quantity' => 'required|integer',
                'operations.*.current_price' => 'required|integer',
                'operations.*.rate_diff' => 'present|nullable|integer',
                'operations.*.buyer_id' => 'present|nullable|integer',
                'operations.*.buyer_name' => 'present|nullable|string',
            ]),
            'sets' => [
                'hwm_id' => 'required|string|max:45',
                'title' => 'required|string|max:45',
                'description' => 'present|nullable|string',
                'arts' => 'required|array',
                'arts.*.art_id' => 'required|string|max:' . config('hwm.rules.max_hwm_id_length'),
                'arts.*.art_name' => 'present|nullable|string|max:' . config('hwm.rules.max_art_title_length'),
                'arts.*.art_image' => 'present|nullable|string',
            ],
            'character' => [
                'hwm_id' => 'required|integer',
                'login' => 'required|string',
                'status' => 'nullable|in:' . $characterStatuses,
                'level' => 'required|integer|min:1',
                'experience' => 'required|integer|min:0',
                'resources' => 'required|array',
                'resources.*' => 'required|integer',
                'guilds' => 'required|array',
                'guilds.*' => 'required|numeric',
                'fractions' => 'required|array',
                'fractions.*' => 'required|numeric',
            ]
        ];

        return $rules[$type];
    }
}
