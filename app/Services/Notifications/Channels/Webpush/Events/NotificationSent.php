<?php

declare(strict_types=1);

namespace App\Services\Notifications\Channels\Webpush\Events;

use Illuminate\Queue\SerializesModels;

class NotificationSent
{
    use SerializesModels;

    /**
     * @var \Minishlink\WebPush\MessageSentReport
     */
    public $report;

    /**
     * @var \App\Services\Notifications\Channels\Webpush\PushSubscriptionModel
     */
    public $subscription;

    /**
     * @var \App\Services\Notifications\Channels\Webpush\WebPushMessage
     */
    public $message;

    /**
     * Create a new event instance.
     *
     * @param  \Minishlink\WebPush\MessageSentReport  $report
     * @param  \App\Services\Notifications\Channels\Webpush\PushSubscriptionModel  $subscription
     * @param  \App\Services\Notifications\Channels\Webpush\WebPushMessage  $message
     * @return void
     */
    public function __construct($report, $subscription, $message)
    {
        $this->report = $report;
        $this->subscription = $subscription;
        $this->message = $message;
    }
}
