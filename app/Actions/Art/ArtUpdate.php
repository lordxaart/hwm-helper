<?php

declare(strict_types=1);

namespace App\Actions\Art;

use App\HWM\HwmClient;
use App\Models\Art;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Lorisleiva\Actions\Concerns\AsAction;

/**
 * @property Art $art
 */
class ArtUpdate
{
    use AsAction;

    public string $commandSignature = 'hwm:art:update {art_id : Art Hwm ID}';
    public string $commandDescription = 'Update art by :art_id';

    /**
     * @throws \App\HWM\Exceptions\HwmException
     */
    public function asCommand(Command $command): int
    {
        $art = $this->handle(strval($command->argument('art_id')));

        $command->info("Art {$art->getTitle()} was successfully updated");

        return 0;
    }

    /**
     * @throws \App\HWM\Exceptions\HwmException
     */
    public function handle(Art|string $art): Art
    {
        $artObj = $art instanceof Art ? $art : Art::where('hwm_id', $art)->firstOrFail();

        $art = hwmClient()->getArtInfo($artObj->hwm_id);

        $data = Arr::only($art->toArray(), [
            'title',
            'need_level',
            'strength',
            'oa',
            'repair',
            'description',
            'modifiers',
            'from_shop',
            'as_from_shop',
            'shop_price',
            'set_id',
        ]);

        $data['original_image'] = $art->image;

        $artObj->update($data);

        return $artObj;
    }
}
