<?php

declare(strict_types=1);

namespace App\Models;

use App\Events\ArtMonitor\AddArtToMonitorEvent;
use App\Events\ArtMonitor\DeleteMonitoredArtEvent;
use App\Models\Traits\HasActiveState;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class MonitoredArt
 * @package App\Models
 * @property User $user
 * @property Art $art
 * @method static active()
 */
class MonitoredArt extends Model
{
    use CrudTrait;
    use HasFactory;
    use HasActiveState;

    /** @var string string */
    protected $table = 'monitored_arts';

    /** @var array  */
    protected $fillable = ['art_hwm_id'];

    /** @var array  */
    protected $casts = [
        'is_active' => 'boolean',
    ];

    protected static function boot()
    {
        parent::boot();

        if (!isAdminPanel()) {
            static::addGlobalScope('active', function (Builder $builder) {
                /** @var Builder|MonitoredArt $builder */
                $builder->active();
            });
        }

        static::created(function (MonitoredArt $art) {
            event(new AddArtToMonitorEvent($art));
        });

        static::deleting(function (MonitoredArt $art) {
            event(new DeleteMonitoredArtEvent($art->user, $art->art));
        });
    }

    public function art(): BelongsTo
    {
        return $this->belongsTo(Art::class, 'art_hwm_id', 'hwm_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function getLink(): string
    {
        return $this->art->getLink(); // HwmLink::get(HwmLink::ART_INFO, ['id' => $this->art_hwm_id]);
    }

    public function scopeActive(Builder $builder): void
    {
        $builder->where('is_active', 1);
    }
}
