@extends('layout.app', ['js' => false])

@section('content')
    <div class="container">
        @yield('content')
    </div>
@endsection