// import axios
import req from 'axios';
import NProgress from 'nprogress';

import ajaxErrorHandler from './ajaxErrorHandler';

req.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

const token = window.CSRF_TOKEN;
if (token) {
    req.defaults.headers.common['X-CSRF-TOKEN'] = token;
} else {
    console.warn('CSRF token not found');
}

if (window.USER) {
    const { apiToken } = window.USER;
    if (apiToken) {
        req.defaults.headers.common.Authorization = `Bearer ${apiToken}`;
    }
}
//

// before a request is made start the nprogress
req.interceptors.request.use((config) => {
    NProgress.start();

    config.onUploadProgress = (progressEvent) => {
        const percentCompleted = Math.floor((progressEvent.loaded) / progressEvent.total);
        NProgress.set(percentCompleted);
    };

    return config;
});
// before a response is returned stop nprogress
req.interceptors.response.use((response) => {
    NProgress.done();

    return response;
}, (error) => {
    NProgress.done();

    ajaxErrorHandler(error);

    return Promise.reject(error.response);
});

export default req;
