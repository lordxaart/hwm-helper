<?php

declare(strict_types=1);

namespace App\HWM\Enums;

enum LotType: string
{
    case SALE = 'sale';
    case AUCTION = 'auction';
    case BLITZ = 'blitz';
}
