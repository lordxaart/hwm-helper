<?php

declare(strict_types=1);

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class EmailLog extends Model
{
    use CrudTrait;

    /** @var string  */
    protected $table = 'email_log';

    /** @var bool  */
    public $timestamps = false;

    /** @var string[]  */
    protected $guarded = ['id'];

    /** @var array  */
    protected $fillable = [
        'email_id',
        'status',
        'date',
        'date_sent',
        'from',
        'to',
        'cc',
        'bbc',
        'subject',
        'body',
        'headers',
        'attachments',
    ];

    public static function findByEmailId(string $id): ?EmailLog
    {
        return self::query()->where('email_id', $id)->first();
    }
}
