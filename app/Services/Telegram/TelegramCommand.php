<?php

declare(strict_types=1);

namespace App\Services\Telegram;

use App\Services\Telegram\Entities\Update;

interface TelegramCommand
{
    public function handle(Update $update): void;
}
