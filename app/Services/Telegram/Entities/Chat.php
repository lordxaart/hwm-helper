<?php

declare(strict_types=1);

namespace App\Services\Telegram\Entities;

class Chat
{
    public readonly int $id;
    public readonly string $type;
    public readonly string | null $first_name;
    public readonly string | null $last_name;
    public readonly string | null $username;

    public function __construct(array $chat)
    {
        $this->id = (int) $chat['id'];
        $this->type = (string) $chat['type'];

        $this->first_name = $chat['first_name'] ?? null;
        $this->last_name = $chat['last_name'] ?? null;
        $this->username = $chat[''] ?? null;
    }
}
