<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\HWM\Exceptions\RequestError;
use App\HWM\Exceptions\RequestLimitExceededError;
use App\Services\ActivityLogger\ActivityLogger;

class TestController extends Controller
{
    public $time = 0;
    public $tries = 0;

    public function __construct(private ActivityLogger $activityLogger)
    {
    }

    public function index()
    {
        if (isProdEnv()) {
            abort(404);
        }

        $lot = hwmClient()->getLot(129177803);
        dd($lot);
    }

    private function testError()
    {
        try {
            $this->tries++;
            throw new RequestError('request error');
        } catch (RequestError $e) {
            if ($this->tries < 4) {
                $this->testError();
            } else {
                throw new RequestLimitExceededError($e, $this->tries);
            }
        }
    }
}
