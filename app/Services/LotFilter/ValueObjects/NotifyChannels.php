<?php

declare(strict_types=1);

namespace App\Services\LotFilter\ValueObjects;

use App\Repositories\NotificationRepository;
use App\Services\Notifications\Enums\Channel;

class NotifyChannels
{
    private const DEFAULT_CHANNEL_AVAILABLE = true;

    private array $channels = [];

    public function __construct()
    {
        $this->initDefaultValue();
    }

    public function setChannelAvailability(Channel $channel, bool $isAvailable): void
    {
        $this->channels[$channel->value] = $isAvailable;
    }

    public function getChannels(): array
    {
        return $this->channels;
    }

    private function initDefaultValue(): void
    {
        $allChannels = app(NotificationRepository::class)->getAllChannels();

        /** @var Channel $channel */
        foreach ($allChannels as $channel) {
            $this->channels[$channel->channel->value] = $channel->channel->defaultAvailability();
        }
    }
}
