import Vue from 'vue';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import duration from 'dayjs/plugin/duration';
import relativeTime from 'dayjs/plugin/relativeTime';
import timezone from 'dayjs/plugin/timezone';
import numeral from 'numeral';
import config from 'JS/app/config';

if (config.app.locale === 'en') {
    // eslint-disable-next-line global-require
    require('dayjs/locale/en');
} else if (config.app.locale === 'uk') {
    // eslint-disable-next-line global-require
    require('dayjs/locale/uk');
} else {
    // eslint-disable-next-line global-require
    require('dayjs/locale/ru');
}

dayjs.extend(utc);
dayjs.extend(duration);
dayjs.extend(relativeTime);
dayjs.extend(timezone);

Vue.filter('hwmNumber', (value) => numeral(value).format('0,0'));

Vue.filter('number', (value) => numeral(value).format('0,0'));

Vue.filter('hwmPPF', (value) => (value ? parseFloat(value).toFixed(2) : null));

Vue.filter('utcToCurrentTz', (value) => {
    const date = dayjs.utc(value);

    // eslint-disable-next-line no-underscore-dangle
    return date.tz('Europe/Moscow').format('YYYY-MM-DD HH:mm:ss');
});

Vue.filter('humanizeSeconds', (seconds) => {
    if (seconds <= 0) {
        return '';
    }
    return dayjs.duration(Number.parseInt(seconds, 10), 'seconds').locale('ru').humanize();
});

Vue.filter('formatBytes', (bytes, precision = 2) => {
    if (typeof bytes !== 'number' || Number.isNaN(bytes)) {
        throw new TypeError('Expected a number');
    }
    let result = bytes >= 0 ? bytes : 0;

    const units = ['b', 'kb', 'mb', 'gb', 'tb'];

    let pow = Math.floor((result ? Math.log(result) : 0) / Math.log(1024));
    pow = Math.min(pow, units.length - 1);

    // eslint-disable-next-line no-bitwise
    result /= (1 << (10 * pow));

    return `${Math.round(result, precision)} ${units[pow]}`;
});

Vue.filter('capitalize', (text) => {
    if (!text) {
        return '';
    }

    text = text.toString();

    return text.charAt(0).toUpperCase() + text.slice(1);
});

Vue.filter('formatDate', (date, format) => dayjs(date).format(format));
Vue.filter('dateHumanize', (date) => {
    const now = (new Date()).getTime() / 1000;
    const time = (new Date(date)).getTime() / 1000;
    return dayjs.duration(time - now, 'seconds').locale(config.app.locale).humanize();
});
