<?php

declare(strict_types=1);

namespace App\HWM\Exceptions\ParserErrors;

use App\HWM\Exceptions\ParserError;

class EmptyLotPage extends ParserError
{
    public function __construct(int $lotId)
    {
        $message = trans('errors.hwm.not_found_lot_page') . ' [' . $lotId . ']';

        parent::__construct($message);
    }
}
