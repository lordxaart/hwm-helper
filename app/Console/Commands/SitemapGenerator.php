<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\Art;
use Carbon\Carbon;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;

class SitemapGenerator extends Command
{
    public const SITEMAP = 'sitemap.xml';

    /** @var string  */
    protected $signature = 'generate:sitemap';

    /** @var string  */
    protected $description = 'Generate ' . self::SITEMAP;

    /**
     * @var string[]
     */
    protected array $pages = [
        '/',
        'calculator',
        'market',
        'stat',
        'login',
        'register',
        'contacts',
        'privacy',
        'terms',
    ];

    public function handle(): void
    {
        $sitemap = Sitemap::create();

        foreach ($this->pages as $page) {
            $sitemap->add(Url::create($page)
                ->setLastModificationDate(Carbon::yesterday())
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_YEARLY)
                ->setPriority(0.5));
        }

        Art::each(function (Art $art) use ($sitemap) {
            $url = Url::create('/market?art=' . $art->hwm_id)
                ->setChangeFrequency(Url::CHANGE_FREQUENCY_YEARLY)
                ->setPriority(0.1);
            if ($art->created_at) {
                $url->setLastModificationDate($art->created_at);
            }
            $sitemap->add($url);
        });

        $sitemap->writeToFile(public_path(self::SITEMAP));

        $this->log('Generated public/' . self::SITEMAP);
    }
}
