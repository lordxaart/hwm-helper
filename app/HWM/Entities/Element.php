<?php

declare(strict_types=1);

namespace App\HWM\Entities;

use App\HWM\Entities\Interfaces\CanBeLot;
use App\HWM\Entities\Interfaces\HasRepository;
use App\HWM\Enums\EntityType;
use App\HWM\Repositories\ElementRepository;

class Element extends HwmEntity implements CanBeLot, HasRepository
{
    public const MARKET_CATEGORY_ID = 'elements';

    public function getEntityType(): EntityType
    {
        return EntityType::ELEMENT;
    }

    public function getMarketCategoryId(): string
    {
        return self::MARKET_CATEGORY_ID;
    }

    public function getImage(bool $realPath = false, string $size = null): string
    {
        $path = 'images/icons/hwm/elements/' . $this->getHwmId() . '.gif';

        return !$realPath ? asset($path) : public_path($path);
    }

    public function getRepository(): ElementRepository
    {
        return app(ElementRepository::class);
    }
}
