<?php

declare(strict_types=1);

namespace App\Actions\Character;

use App\Events\HWM\CharacterAddedEvent;
use App\HWM\HwmClient;
use App\Models\Character;
use App\Models\User;
use Illuminate\Console\Command;
use Lorisleiva\Actions\Concerns\AsAction;

/**
 * @property User $user
 * @property string $login
 * @property string|null $password
 */
class CharacterAdd
{
    use AsAction;

    public string $commandSignature = 'hwm:character:add
                                            {user_id : Associated User}
                                            {login : Character login}
                                            {password=null : Character password}';

    public string $commandDescription = 'Add character by :user_id :login :password';

    public function asCommand(Command $command): int
    {
        $character = $this->handle(
            User::findOrFail($command->argument('user_id')),
            strval($command->argument('login')),
            $command->hasArgument('password') ? strval($command->argument('password')) : null
        );

        $command->info("Character $character->login was successfully updated");

        return 0;
    }

    public function handle(User $user, string $login, string $password = null): Character
    {
        $entity = app(HwmClient::class)->getCharacterByLogin($login);

        /** @var Character $character */
        $character = $user->characters()->create($entity->toArray());

        try {
            if ($password) {
                // todo: check login and password
                $character->password = $password;
            }
        } catch (\Exception) {
            //
        }

        event(new CharacterAddedEvent($character));

        return $character;
    }
}
