<?php

declare(strict_types=1);

namespace App\Models\Traits;

trait HwmEntity
{
    public static function findByHwmId(string $id): ?static
    {
        return static::where('hwm_id', $id)->first();
    }
}
