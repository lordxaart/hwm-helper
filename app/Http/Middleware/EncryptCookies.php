<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Core\Enums\Cookie;
use Illuminate\Contracts\Encryption\Encrypter as EncrypterContract;
use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;

class EncryptCookies extends Middleware
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    protected $except = [];

    public function __construct(EncrypterContract $encrypter)
    {
        parent::__construct($encrypter);

        $this->disableFor([
            Cookie::LOCALE->value,
            Cookie::USERID->value,
        ]);
    }
}
