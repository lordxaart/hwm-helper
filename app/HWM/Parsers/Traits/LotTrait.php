<?php

declare(strict_types=1);

namespace App\HWM\Parsers\Traits;

use App\HWM\Enums\EntityType;
use App\HWM\Enums\LotType;
use App\HWM\Exceptions\ParserError;
use App\HWM\Exceptions\ParserErrors\EmptyLotPage;
use App\HWM\Exceptions\ParserErrors\UnexpectedPage;
use App\HWM\Helpers\HwmHelper;
use App\HWM\Helpers\HwmLink;
use App\HWM\Objects\LotOperation;
use Carbon\Carbon;
use Carbon\Exceptions\InvalidFormatException;
use Illuminate\Support\Str;

trait LotTrait
{
    /**
     * @throws EmptyLotPage
     * @throws ParserError
     * @throws UnexpectedPage|\App\HWM\Exceptions\HwmException
     */
    public function getLotInfo(int $lotId): array
    {
        if ($this->locale !== RU) {
            throw new ParserError('Method supported only RU locale');
        }

        if (!$lotBlock = $this->getLotBlock()) {
            throw new UnexpectedPage("lot_info #$lotId");
        }

        preg_match('/Протокол,\sлот\s#(\d+)/', $lotBlock->innerHtml, $matches);
        $currentLotId = $matches[1] ?? null;

        $texts = $this->getLotTexts(RU);
        $locale = RU;

        if (!str_contains($lotBlock->innerHtml, 'Продавец:')) {
            if (str_contains($lotBlock->innerHtml, 'Seller:')) {
                $texts = $this->getLotTexts(EN);
                $locale = EN;
            } else {
                throw new EmptyLotPage($lotId ?: 0);
            }
        }

        $this->locale = $locale;

        $lotId = intval($currentLotId);

        $allOperations = array_reverse(explode('<br />', $lotBlock->innerHtml));
        // Delete first two lines (link to market and empty line)
        $allOperations = array_filter(array_slice($allOperations, 2));
        // remove title of the lot
        array_pop($allOperations);
        $allOperations = array_values($allOperations);

        // Default Values
        $currentPrice = 1;
        $currentQuantity = 1;
        $entityId = null;
        $entityTitle = null;

        // Parse all operation
        $parsedOperations = [];
        foreach ($allOperations as $operation) {
            // Default values
            $operationType = LotOperation::OPERATION_UNDEFINED;

            // Time
            $operationTime = $this->parseLotOperationTime($operation);

            if (str_contains($operation, $texts['seller'])) {
                $operationType = LotOperation::OPERATION_CREATE;
            } elseif (str_contains($operation, $texts['goods_sold']) || str_contains($operation, $texts['sold_out'])) {
                $operationType = LotOperation::OPERATION_END;
            } elseif (str_contains($operation, $texts['bid'])) {
                $operationType = LotOperation::OPERATION_RATE;
            } elseif (str_contains($operation, $texts['buyed'])) {
                $operationType = LotOperation::OPERATION_SALE;
            }

            // First operation
            if ($operationType === LotOperation::OPERATION_CREATE) {
                // Created at
                $createdAt = $operationTime;
                // Ended at
                $endedAt = $this->parseLotOperationTime(substr($operation, 19));

                [$entityId, $entityTitle] = HwmHelper::parseEntityLink($operation, HwmLink::ART_INFO);
                $entityTitle = $entityTitle ? strip_tags($entityTitle) : null;
                $entityTitle = str_replace(' [i]', '', strval($entityTitle));

                // Quantity
                preg_match('/\s(\d+)\s' . $texts['pc'] . '/', $operation, $matches);
                $startQuantity = intval($matches[1] ?? 1);
                $currentQuantity = $startQuantity;

                // Seller
                $link = getTextBetweenValues($operation, $texts['seller'], '</a>', false);
                $sellerId = intval(getTextBetweenValues($link, 'pl_info.php?id=', '">'));
                $sellerName = getTextBetweenValues($link, '<b>', '</b>');

                if (!$sellerName && $sellerId) {
                    $seller = hwmClient()->getCharacterInfo($sellerId);
                    $sellerName = $seller->getTitle();
                }

                // Price
                preg_match('/([\d,]+)\s' . $texts['gold'] . '/', $operation, $matches);
                $startPrice = HwmHelper::clearIntValue($matches[1] ?? '1');
                $currentPrice = $startPrice;

                // Lot type
                if (strpos($operation, $texts['blitz_price'])) {
                    $lotType = LotType::BLITZ->value;
                    $t = str_replace(' ', '\s', $texts['blitz_price']);
                    preg_match('/' . $t . '\s-\s([\d,]+)/', $operation, $matches);
                    $blitzPrice = HwmHelper::clearIntValue(strval($matches[1] ?? 1));
                } elseif (strpos($operation, $texts['start_price'])) {
                    $lotType = LotType::AUCTION->value;
                } else {
                    $lotType = LotType::SALE->value;
                }

                if ($locale === EN && !str_contains($operation, $texts['gold'])) {
                    $lotType = LotType::AUCTION->value;
                    $currentPrice = 1;
                }

                // Entity type
                if (str_contains($operation, $texts['part_of_art'])) { // Art part
                    $entityType = EntityType::ART_PART->value;
                } elseif (str_contains($operation, HwmLink::ART_INFO)) { // Art
                    $entityType = EntityType::ART->value;

                    $href = getTextBetweenValues($operation, '<a href="' . HwmLink::ART_INFO . '?', '">');
                    parse_str($href, $parts);
                    $artUid = isset($parts['uid']) ? intval($parts['uid']) : null;
                    $artCrc = $parts['crc'] ?? null;

                    // Strength
                    if ($locale === EN) {
                        $regex = preg_replace('/^.+\s(\d+\/\d+)(\s|,|.).*$/', '$1', $operation);
                    } else {
                        $regex = preg_replace('/^.+\[(\d+\/\d+)\].*$/', '$1', $operation);
                    }

                    if (strlen($regex) > 20) {
                        $regex = preg_replace('/^.+\s(\d+\/\d+)(\s|,|.).*$/', '$1', $operation);
                    }

                    [$currentStrength, $baseStrength] = explode('/', $regex);

                    $currentStrength = intval($currentStrength);
                    $baseStrength = intval($baseStrength);

                    // Craft
                    $craftRegex = HwmHelper::getRegex('craft_full');
                    if (preg_match($craftRegex, $entityTitle, $result)) {
                        $craft = $result[0];
                        $entityTitle = trim(str_replace($craft, '', $entityTitle));
                        $craft = str_replace(['[', ']'], '', $craft);
                    }
                } elseif (str_contains($operation, HwmLink::HOUSE_INFO)) { // House
                    $entityType = EntityType::HOUSE->value;
                    [$entityId, $entityTitle] = HwmHelper::parseEntityLink($operation, HwmLink::HOUSE_INFO);
                } elseif (str_contains($operation, HwmLink::OBJECT_INFO)) { // Object
                    $entityType = EntityType::OBJECT_SHARE->value;
                    [$entityId] = HwmHelper::parseEntityLink($operation, HwmLink::OBJECT_INFO);
                    $entityTitle = getTextBetweenValues($operation, "#$entityId</a>", ',');
                } elseif (str_contains($operation, $texts['certificate'])) { // Cert
                    $entityType = EntityType::CERTIFICATE->value;
                    preg_match('/1%\s' . $texts['for'] . '\s"(.*?)"/', $operation, $matches);
                    $entityTitle = $matches[1] ?? UNDEFINED;
                    $entityId = Str::slug($entityTitle);
                } elseif ($element = $this->stringHasElementTitle($operation)) { // Element
                    $entityType = EntityType::ELEMENT->value;
                    $entityId = $element->getHwmId();
                    $entityTitle = $element->getTitle();
                } elseif ($resource = $this->stringHasResourceTitle($operation)) { // Element
                    $entityType = EntityType::RESOURCE->value;
                    $entityId = $resource->getHwmId();
                    $entityTitle = $resource->getTitle();
                } elseif (str_contains($operation, $texts['skelotons'])) { // Skelets
                    $entityTitle = $texts['skelotons'];
                    $entityId = EntityType::SKELETON->value;
                    $entityType = EntityType::SKELETON->value;
                }

                if (
                    isset($entityType) && $locale === EN && in_array(
                        $entityType,
                        [
                            EntityType::RESOURCE->value,
                            EntityType::SKELETON->value,
                        ]
                    )
                ) {
                    $startQuantity = intval(getTextBetweenValues($operation, 'Displayed for sale:', $entityTitle));
                    $currentQuantity = $startQuantity;
                }
            }
            // rate operation
            elseif (str_contains($operation, $texts['bid'])) {
                $operationType = LotOperation::OPERATION_RATE;
                preg_match('/<b>([\d,]+)<\/b>/', $operation, $matches);
                $operationRate = HwmHelper::clearIntValue($matches[1] ?? 0);
                $rateDiff = $operationRate - $currentPrice;
                if ($operationRate) {
                    $currentPrice = $operationRate;
                }
            }

            // sale operation
            elseif (str_contains($operation, $texts['buyed'])) {
                $operationType = LotOperation::OPERATION_SALE;
                preg_match('/<b>(\d+)\s' . $texts['pc_o'] . '\.<\/b>/', $operation, $matches);
                $buyedQuantity = intval($matches[1] ?? 0);
                $currentQuantity -= $buyedQuantity;
            }

            // Last operation
            elseif (str_contains($operation, $texts['goods_sold'])) {
                $operationType = LotOperation::OPERATION_END;
                $buyedAt = $operationTime;
            }

            if (str_contains($operation, $texts['sold_out']) || str_contains($operation, 'Auction over')) {
                $operationType = LotOperation::OPERATION_END;
                if (str_contains($operation, $texts['sold']) || str_contains($operation, $texts['solden'])) {
                    $buyedAt = $operationTime;
                }

                if (str_contains($operation, HwmLink::HOUSE_INFO)) { // House
                    $entityType = EntityType::HOUSE->value;
                    [$entityId, $entityTitle] = HwmHelper::parseEntityLink($operation, HwmLink::HOUSE_INFO);
                }
            }

            // for old record "Торги закончены. Новый дом"
            if (str_contains($operation, $texts['new_house'])) {
                $entityType = EntityType::HOUSE->value;
                [$entityId, $entityTitle] = HwmHelper::parseEntityLink($operation, HwmLink::HOUSE_INFO);
            }

            if ($operationType == LotOperation::OPERATION_SALE || $operationType == LotOperation::OPERATION_RATE) {
                if (str_contains($operation, HwmLink::CHARACTER_INFO . '?nick=')) {
                    [$operationBuyerId, $operationBuyerName] = HwmHelper::parseEntityLink(
                        $operation,
                        HwmLink::CHARACTER_INFO,
                        true
                    );
                } else {
                    [$operationBuyerId, $operationBuyerName] = HwmHelper::parseEntityLink(
                        $operation,
                        HwmLink::CHARACTER_INFO
                    );
                }

                if (!intval($operationBuyerId)) {
                    try {
                        $character = hwmClient()->getCharacterByLogin($operationBuyerId);
                        $operationBuyerId = $character ? $character->getHwmId() : null;
                    } catch (\Exception $e) {
                        unset($e);
                    }
                }

                $operationBuyerId = $operationBuyerId ? intval($operationBuyerId) : null;
                $operationBuyerName = $operationBuyerName ? strip_tags($operationBuyerName) : null;

                if (!$operationBuyerName && $operationBuyerId) {
                    $seller = hwmClient()->getCharacterInfo($operationBuyerId);
                    $operationBuyerName = $seller->getTitle();
                }
            }

            $parsedOperations[] = [
                'time' => $operationTime,
                'type' => $operationType,
                'buyed_quantity' => $buyedQuantity ?? null,
                'current_quantity' => $currentQuantity,
                'current_price' => $currentPrice,
                'rate_diff' => $rateDiff ?? null,
                'buyer_id' => $operationBuyerId ?? null,
                'buyer_name' => $operationBuyerName ?? null,
            ];
        }

        if (!$entityId) {
            $entityId = Str::slug($entityTitle);
        }

        $lot = [
            'lot_id' => $lotId,
            'lot_type' => $lotType ?? UNDEFINED,
            'entity_type' => $entityType ?? UNDEFINED,
            'entity_id' => $entityId,
            'entity_title' => $entityTitle,
            'art_uid' => $artUid ?? null,
            'art_crc' => $artCrc ?? null,
            'craft' => $craft ?? null,
            'current_strength' => $currentStrength ?? null,
            'base_strength' => $baseStrength ?? null,
            'quantity' => max($currentQuantity, 0),
            // 'start_quantity' => $startQuantity ?? 1,
            'price' => $currentPrice ?? 1,
            // 'start_price' => $startPrice ?? 1,
            'blitz_price' => $blitzPrice ?? null,
            'seller_name' => $sellerName ?? null,
            'seller_id' => $sellerId ?? null,
            'started_at' => $createdAt ?? null,
            'buyed_at' => $buyedAt ?? null,
            'ended_at' => $endedAt ?? null,
            'operations' => $parsedOperations,
        ];
        dd($lot);
        $this->validateData('lot', $lot);

        return $lot;
    }

    protected function parseLotOperationTime(string $operation): string
    {
        $formats = [
            'd-m-Y H:i:s' => '/\d{2}-\d{2}-\d{4}\s\d{2}:\d{2}:\d{2}/',
            'd-m-Y H:i' => '/\d{2}-\d{2}-\d{4}\s\d{2}:\d{2}/',
            'd-m-y H:i' => '/\d{2}-\d{2}-\d{2}\s\d{2}:\d{2}/',
        ];

        foreach ($formats as $format => $regex) {
            $time = getTextByRegex($operation, $regex);

            try {
                return Carbon::createFromFormat($format, $time, HwmHelper::HEROES_TZ)
                    ->timezone(getDefaultTimezone())
                    ->format(DATE_FORMAT);
            } catch (InvalidFormatException $e) {
                //
            }
        }

        throw new \InvalidArgumentException(sprintf('[parseLotOperationTime] is failed, cant parser "%s"', $operation));
    }

    /**
     * @return \App\HWM\Parsers\CustomDom|mixed|\PHPHtmlParser\Dom\Collection|null
     */
    protected function getLotBlock(): mixed
    {
        return $this->find('body > center > table:nth-child(2) > tr > td > table > tr > td');
    }

    private function getLotTexts(string $locale): array
    {
        $texts = [
            RU => [
                'seller' => 'Продавец:',
                'goods_sold' => 'Товар распродан',
                'sold_out' => 'Торги закончены',
                'bid' => 'ставка',
                'buyed' => 'куплено',
                'gold' => 'золота',
                'blitz_price' => 'блиц цена',
                'start_price' => 'стартовая цена',
                'part_of_art' => 'часть артефакта',
                'skelotons' => 'Скелеты',
                'pc' => 'шт',
                'pc_o' => 'шт',
                'sold' => 'Продано',
                'solden' => 'продан',
                'new_house' => 'Торги закончены. Новый дом',
                'certificate' => 'Сертификат',
                'for' => 'для',
            ],
            EN => [
                'seller' => 'Seller:',
                'goods_sold' => 'Goods sold out',
                'sold_out' => 'Auction expired',
                'bid' => 'ставка',
                'buyed' => 'куплено',
                'gold' => 'gold',
                'blitz_price' => 'buyout price',
                'start_price' => 'starting price',
                'part_of_art' => 'часть артефакта',
                'skelotons' => 'Skeletons',
                'pc' => 'pcs',
                'pc_o' => 'шт',
                'sold' => 'Sold',
                'solden' => 'продан',
                'new_house' => 'Торги закончены. Новый дом',
                'certificate' => 'Certificate',
                'for' => 'for',
            ],
        ];

        return $texts[$locale];
    }
}
