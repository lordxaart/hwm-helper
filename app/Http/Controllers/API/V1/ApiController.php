<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\Core\ApiCollection;
use App\Http\Resources\Core\ApiResource;
use App\Services\AppData\AppDataProvider;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends Controller
{
    public function home(): Response
    {
        return $this->response(['message' => 'API V1 is work']);
    }

    public function getApp(AppDataProvider $appDataProvider): Response
    {
        return $this->response(['data' => $appDataProvider->getData()]);
    }

    protected function response(
        array|ApiResource|ApiCollection|AnonymousResourceCollection $data,
        int $statusCode = 200
    ): Response {
        if (
            $data instanceof ApiResource
            || $data instanceof ApiCollection
            || $data instanceof AnonymousResourceCollection
        ) {
            return $data->toResponse(request());
        }

        return response()->json($data, $statusCode);
    }

    protected function responseNoContent(): Response
    {
        return response()->noContent();
    }
}
