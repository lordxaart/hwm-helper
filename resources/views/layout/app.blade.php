<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">
<head>
	@include('layout.head')

	@yield('styles')

	@stack('styles')

	@yield('head')
</head>
<body>
    @include('layout.gtm_body')

	<div class="page-wrapper {{ $extra_wrapper_class ?? '' }}" id="app">
		<div class="main d-flex">

			@include('layout.navbar')

			<div class="content posr w-100">
                <div class="v-cloak" v-cloak>
                    @yield('content')
                </div>

                <div id="content-preloader" class="wrapper-loader content-loader">
                    <div class="loader">
                        <div></div>
                        <div></div>
                    </div>
                </div>
			</div>

		</div>

		@include('layout.footer')
	</div>

    @if(!Crawler::isCrawler())
	    @include('elements.accept_cookies')

        @include('elements.preloader')
    @endif

	@include('layout.scripts')

    @yield('scripts')

	@stack('js')
</body>
</html>
