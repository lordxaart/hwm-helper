<?php

declare(strict_types=1);

use App\Core\Enums\AppEnv;

function getAppType(): string
{
    return config('app.type');
}

function isWebApp(): bool
{
    return getAppType() === 'web';
}

function isParserApp(): bool
{
    return getAppType() === 'parser';
}

function isLocalEnv(): bool
{
    return env('APP_ENV') === AppEnv::LOCAL->value;
}

function isTestEnv(): bool
{
    return env('APP_ENV') === AppEnv::TEST->value;
}

function isDevEnv(): bool
{
    return env('APP_ENV') === AppEnv::DEV->value;
}

function isProdEnv(): bool
{
    return env('APP_ENV') === AppEnv::PROD->value;
}

function isDebugMode(): bool
{
    return function_exists('config')
        ? config('app.debug') === true
        : env('APP_DEBUG') === true;
}

function isAcceptedCookies(): bool
{
    return boolval($_COOKIE['accept_cookies'] ?? false);
}

function isAdminPanel(): bool
{
    $path = substr(request()->getPathInfo(), 1);
    $path = explode('/', $path);

    return $path[0] === ADMIN_PREFIX;
}

// define is it current route
function isRoute(string $routeName): bool
{
    $path = substr(request()->getPathInfo(), 1);
    $routes = Route::getRoutes()->getRoutesByName();
    $route = in_array($routeName, $routes) ? $routes[$routeName]->uri : $routeName;

    return $path === $route;
}

function showGtm(): bool
{
    return !Crawler::isCrawler();
}

function isDocker(): bool
{
    return boolval(env('APP_PATH', false));
}

function requireFile(string $path): void
{
    require base_path($path);
}
