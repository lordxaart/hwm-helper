<?php

declare(strict_types=1);

namespace App\Services\ImageOptimize;

interface OptimizerInterface
{
    public function optimize(string $path, int $quality): bool;
}
