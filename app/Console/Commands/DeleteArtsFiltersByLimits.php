<?php

declare(strict_types=1);

namespace App\Console\Commands;

class DeleteArtsFiltersByLimits extends Command
{
    /** @var string */
    protected $signature = 'arts-filters:clear:limits {--soft}';

    /** @var string */
    protected $description = 'Delete arts and limits by limits';

    public function handle(): void
    {
        $soft = boolval($this->option('soft'));

        \App\Models\User::each(function (\App\Models\User $user) use ($soft) {
            $filters = $user->filters();
            $monitoredArts = $user->monitoredArts();

            $needDeleteArts = $monitoredArts->count() - $user->features()->max_monitored_arts;
            if ($needDeleteArts > 0) {
                $this->info('USER ' . $user->email);
                $this->info('User has too many arts, was deleted ' . $needDeleteArts);
                $deleted = $monitoredArts->take($needDeleteArts)->get();
                foreach ($deleted as $item) {
                    $this->warn('Delete art ' . $item->id);
                    if (!$soft) {
                        $item->delete();
                    }
                }
            }

            $needDeleteFilters = $filters->count() - $user->features()->max_monitored_filters;
            if ($needDeleteFilters > 0) {
                $this->info('USER ' . $user->email);
                $this->info('User has too many filters, was deleted ' . $needDeleteFilters);
                $deleted = $filters->take($needDeleteFilters)->get();
                foreach ($deleted as $item) {
                    $this->warn('Delete filter ' . $item->id);
                    if (!$soft) {
                        $item->update(['is_active' => 0]);
                    }
                }
            }
        });
    }
}
