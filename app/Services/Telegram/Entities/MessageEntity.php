<?php

declare(strict_types=1);

namespace App\Services\Telegram\Entities;

class MessageEntity
{
    private const TYPE_BOT_COMMAND = 'bot_command';

    public readonly string $type;
    public readonly int $offset;
    public readonly int $length;

    public function __construct(array $entity)
    {
        $this->type = (string) $entity['type'];
        $this->offset = (int) $entity['offset'];
        $this->length = (int) $entity['length'];
    }

    public function isBotCommand(): bool
    {
        return $this->type == self::TYPE_BOT_COMMAND;
    }
}
