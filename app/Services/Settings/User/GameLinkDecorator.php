<?php

declare(strict_types=1);

namespace App\Services\Settings\User;

use App\HWM\Helpers\HwmLink;
use App\Models\Objects\UserSettings;
use App\Models\User;
use App\Services\UserFeatures\UserFeatureService;
use Illuminate\Log\Logger;

class GameLinkDecorator
{
    public function __construct(private Logger $logger, private UserFeatureService $userFeatureService)
    {
        //
    }

    public function replaceGameLinkForUser(string $text, User $user): string
    {
        $allLinks = $this->getAllGameLinks();

        $userLink = (string) $user->settings(UserSettings::GAME_LINK);

        if (!in_array($userLink, $allLinks)) {
            $this->logger->warning(sprintf(
                '[GameLinkDecorator] User game link [%s] not found in available links: %s',
                $userLink,
                implode(', ', $allLinks)
            ));

            return $text;
        }

        $newLink = $text;

        foreach ($allLinks as $link) {
            $newLink = str_replace($link, $userLink, $newLink);
        }

        if (str_contains($newLink, HwmLink::MARKET)) {
            $newLink = $this->prepareMarketLinkForUser($newLink, $user);
        }

        return $newLink;
    }

    public function prepareMarketLinkForUser(string $link, User $user): string
    {
        $canEditMarketLink = $this->userFeatureService->getUserFeatures($user->getId())->can_edit_game_settings;
        if (!$canEditMarketLink) {
            return $link;
        }
        $gameSettings = $user->settings(UserSettings::GAME_SETTINGS);
        $sort = $gameSettings['link_market_sort'] ?? null;

        /** @var string $query */
        $query = parse_url($link, PHP_URL_QUERY) ?: '';
        parse_str($query, $queryParams);

        if ($sort) {
            $queryParams['sort'] = $sort;
        }

        return explode('?', $link)[0] . '?' . http_build_query($queryParams);
    }

    /**
     * @return string[]
     */
    private function getAllGameLinks(): array
    {
        return HwmLink::AVAILABLE_LINKS;
    }
}
