<?php

declare(strict_types=1);

namespace App\Components\Validators\Rules\Lot;

use App\HWM\Enums\LotType;
use Illuminate\Contracts\Validation\Rule;

class IsLotType implements Rule
{
    public function passes($attribute, $value)
    {
        return LotType::tryFrom((string) $value) !== null;
    }

    public function message()
    {
        return 'Is not valid lot type. Allowed values: ' . implode(',', enumToArrayOfValues(LotType::cases()));
    }
}
