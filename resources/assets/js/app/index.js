// Other Components
require('JS/app/bootstrap/components');

// Filters
require('JS/app/bootstrap/vueFilters');

// Mixins
require('JS/app/bootstrap/vueMixins');

// UI Components
require('JS/app/bootstrap/ui');

// fontawesome icons
require('JS/app/bootstrap/fontawesome');

require('JS/app/bootstrap/echo');

// Main application
require('JS/app/bootstrap/appVue');

// other scripts
require('JS/app/bootstrap/index');
