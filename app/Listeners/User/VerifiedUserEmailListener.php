<?php

declare(strict_types=1);

namespace App\Listeners\User;

use App\Jobs\AdminNotify;
use App\Models\User;
use App\Notifications\User\Register;
use App\Notifications\Admin\Notify;
use App\Services\ActivityLogger\ActivityLogger;
use App\Services\ActivityLogger\ValueObjects\Activity;
use Illuminate\Auth\Events\Verified as Event;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifiedUserEmailListener implements ShouldQueue
{
    public function __construct(private readonly ActivityLogger $activityLogger)
    {
    }

    public function handle(Event $event): void
    {
        /** @var User|null $user */
        $user = $event->user;

        $this->activityLogger->log(
            Activity::action('USER_VERIFY_EMAIL')->causedBy($user)
        );

        if (!$user) {
            return;
        }

        $user->notify(new Register());

        AdminNotify::dispatchSync(new Notify(trans('Verified Email') . " - $user->email"));
    }
}
