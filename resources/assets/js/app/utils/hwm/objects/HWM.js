import Master from 'JS/app/utils/hwm/masters';
import calculator from 'JS/app/utils/hwm/calculator';
import calculatorReverse from 'JS/app/utils/hwm/calculator-reverse';
import Link from 'JS/app/utils/hwm/objects/Link';
import i18n from 'JS/app/utils/localization';
import Lot from 'JS/app/utils/hwm/objects/Lot';
import { links } from 'JS/app/utils/hwm/static';

export default class HWM {
    constructor() {
        this.init();
    }

    init() {
        this.master = Master;
        this.calculator = calculator;
        this.calculatorReverse = calculatorReverse;
        this.link = new Link(links.main);
        this.setGameLink(links.main);
        this.lot = new Lot();

        this.defaultArtImg = '/images/art_default_small.png';
    }

    setGameLink(gameLink) {
        this.gameLink = gameLink;
        this.link.setHost(this.gameLink);
    }

    // eslint-disable-next-line class-methods-use-this
    lotHasAccordedFilters(lot) {
        return lot.accorded_filters && lot.accorded_filters.data && lot.accorded_filters.data.length;
    }

    termsToValue(terms, throwError = true) {
        if (!terms || !Array.isArray(terms)) {
            if (throwError) {
                throw new Error(i18n.t('app.monitoring.term_is_not_valid'));
            }
            return false;
        }

        let value = '';

        terms.forEach((term) => {
            if (!term.field || !term.operator) {
                return;
            }

            if (!this.termIsValid(term)) {
                if (throwError) {
                    throw new Error(i18n.t('app.monitoring.term_is_not_valid'));
                }
            }

            value += ` ${term.field} ${term.operator} ${term.value};`;
        });

        return value ? value.trim().slice(0, -1) : value;
    }

    termsToString(terms, separator = ';') {
        if (!terms || !Array.isArray(terms)) {
            return false;
        }

        let value = '';

        terms.forEach((term) => {
            if (!this.termIsValid(term)) {
                return;
            }

            value += ` ${i18n.t(`app.art.columns.${term.field}`)} ${term.operator} ${term.value}${separator}`;
        });

        return value.trim().slice(0, separator.length * -1);
    }

    // eslint-disable-next-line class-methods-use-this
    termIsValid(term) {
        return term && term.field && term.operator && (term.value || term.value === 0);
    }
}
