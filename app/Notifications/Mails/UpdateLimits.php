<?php

declare(strict_types=1);

namespace App\Notifications\Mails;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UpdateLimits extends Mailable
{
    use Queueable;
    use SerializesModels;

    public User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function build(): void
    {
        $this
            ->subject('Изменения лимитов на мониториг !!!')
            ->markdown('emails.update_limits', [
                'user' => $this->user,
                'monitoringArts' => $this->user->monitoredArts()->count(),
                'filters' => $this->user->filters()->count(),
            ]);
    }
}
