<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Http\Resources\Core\ApiResource;
use Illuminate\Http\Request;

/**
 * @property \App\Models\BaseCategory $resource
 */
class ArtCategoryResource extends ApiResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return $this->transformByRequest([
            'hwmId' => $this->getHwmId(),
            'title' => $this->getTitle(),
        ], $request);
    }
}
