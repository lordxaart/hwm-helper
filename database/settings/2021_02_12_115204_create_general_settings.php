<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

class CreateGeneralSettings extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('general.app_full_name', 'Heroeswm Helper');
        $this->migrator->add('general.enable_speed_log', false);
        $this->migrator->add('general.enable_request_proxy', false);
        $this->migrator->add('general.max_execution_time_for_query_for_log', 1);
        $this->migrator->add('general.optimize_img_quality', 85);
        $this->migrator->add('general.default_max_monitored_arts', 10);
        $this->migrator->add('general.default_max_monitored_filters', 5);
        $this->migrator->add('general.max_subscriptions_new_lots', 10);
    }
}
