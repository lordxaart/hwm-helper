<?php

declare(strict_types=1);

namespace App\Services\LotFilter\Repositories;

use App\Models\LotFilterNotifyChannel;
use Illuminate\Database\Eloquent\Collection;

class LotFilterNotifyChannelRepository
{
    public function getMapCollectionByFilterIds(array $filterIds): Collection
    {
        return LotFilterNotifyChannel::query()
            ->whereIn('lot_filter_id', $filterIds)
            ->get();
    }
}
