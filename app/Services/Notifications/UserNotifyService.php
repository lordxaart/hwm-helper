<?php

declare(strict_types=1);

namespace App\Services\Notifications;

use App\Models\Objects\UserSettings;
use App\Models\User;
use App\Services\Notifications\Enums\Channel;
use App\Services\Notifications\Enums\NotifyType;
use App\Services\Settings\User\UserSettingsProvider;
use Illuminate\Notifications\Notification;

class UserNotifyService
{
    public function __construct(private UserSettingsProvider $userSettingsProvider)
    {
    }

    public function sendNotifyToPublicChannel(Notification $notification): void
    {
        (new PublicChannelNotifable())->notify($notification);
    }

    /**
     * @param User $user
     * @param NotifyType $notifyType
     *
     * @return string[]
     */
    public function getConfiguredChannels(User $user, NotifyType $notifyType): array
    {
        if ($notifyType === NotifyType::IMPORTANT) {
            return $this->mapChannelsToLaravelType($this->getAvailableChannels($user));
        }

        $userSettings = $this->getUserNotifySettings($user);

        $defaultUserSettings = $userSettings[NotifyType::DEFAULT->value] ?? [];
        $notifyTypeUserSettings = $userSettings[$notifyType->value] ?? [];

        /** @var Channel[] $configuredChannels */
        $configuredChannels = [];

        foreach ($defaultUserSettings as $channel => $isAvailable) {
            if ($isAvailable) {
                $configuredChannels[$channel] = Channel::from($channel);
            }
        }

        if (empty($configuredChannels)) {
            return [];
        }

        if ($notifyType === NotifyType::DEFAULT) {
            return $this->mapChannelsToLaravelType($configuredChannels);
        }

        foreach ($notifyTypeUserSettings as $channel => $isAvailable) {
            if (!$isAvailable && array_key_exists($channel, $configuredChannels)) {
                unset($configuredChannels[$channel]);
            }
        }

        $availableChannels = $this->getAvailableChannels($user);

        foreach ($configuredChannels as $channel => $value) {
            if (!array_key_exists($channel, $availableChannels)) {
                unset($configuredChannels[$channel]);
            }
        }

        return $this->mapChannelsToLaravelType($configuredChannels);
    }

    /**
     * @param User $user
     *
     * @return Channel[]
     */
    private function getAvailableChannels(User $user): array
    {
        $availableChannels = [];

        foreach (Channel::cases() as $channel) {
            $availableChannels[$channel->value] = $channel;
        }

        if (isset($availableChannels[Channel::TELEGRAM->value]) && !$user->canSendTelegramNotify()) {
            unset($availableChannels[Channel::TELEGRAM->value]);
        }

        if (isset($availableChannels[Channel::BROWSER->value]) && !$user->canSendBrowserNotify()) {
            unset($availableChannels[Channel::BROWSER->value]);
        }

        return $availableChannels;
    }

    private function mapChannelsToLaravelType(array $channels): array
    {
        return array_map(fn (Channel $channel) => $channel->getLaravelChannelName(), $channels);
    }

    /**
     * @param User $user
     *
     * @return array [$notifyType] => [$channel => boolean]
     */
    private function getUserNotifySettings(User $user): array
    {
        return $this->userSettingsProvider
            ->getSettingsByUserId($user->getId())
            ->get(UserSettings::NOTIFICATIONS);
    }
}
