<?php

declare(strict_types=1);

namespace App\HWM\Entities;

use Carbon\Carbon;

class MarketLot extends BaseLot
{
    public ?string $image;
    public ?int $last_buyer_bid;
    public ?string $last_buyer_name;
    public ?Carbon $started_at;

    public function __construct(array $parameters = [])
    {
        if (isset($parameters['started_at'])) {
            $parameters['started_at'] = Carbon::parse($parameters['started_at']);
        }

        parent::__construct($parameters);
    }

    public function toArray(): array
    {
        $array = parent::toArray();
        unset($array['title']);

        $array['started_at'] = $this->started_at?->format(DATE_FORMAT);
        $array['ended_at'] = $this->ended_at?->format(DATE_FORMAT);

        return $array;
    }
}
