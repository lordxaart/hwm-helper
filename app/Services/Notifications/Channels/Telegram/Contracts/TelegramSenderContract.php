<?php

declare(strict_types=1);

namespace App\Services\Notifications\Channels\Telegram\Contracts;

use App\Services\Notifications\Channels\Telegram\Exceptions\CouldNotSendNotification;
use Psr\Http\Message\ResponseInterface;

interface TelegramSenderContract
{
    /**
     * Send the message.
     *
     * @return ResponseInterface|array|null
     *
     * @throws CouldNotSendNotification
     */
    public function send(): ResponseInterface|array|null;
}
