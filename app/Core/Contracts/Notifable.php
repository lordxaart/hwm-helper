<?php

declare(strict_types=1);

namespace App\Core\Contracts;

use Illuminate\Notifications\Notification;

interface Notifable
{
    public function routeNotificationFor(string $driver, Notification $notification = null);
    public function toArray(): array;
}