<?php

declare(strict_types=1);

namespace App\HWM\Entities\Interfaces;

use App\HWM\Repositories\Interfaces\EntityRepositoryInterface;

interface HasRepository
{
    public function getRepository(): EntityRepositoryInterface;
}
