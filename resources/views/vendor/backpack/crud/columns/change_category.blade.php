@php
    $columnName = $column['name'];
    $entity = $column['entity'];
    $attributes = $entry->$entity()->getModel()->all();
@endphp

<select class="custom-select change_category" style="max-width: 220px" data-url="{{ url('admin/arts/' . $entry->id) }}" data-column="{{ $columnName }}">
    @foreach($attributes as $category)
        <option value="{{ $category->hwm_id }}" {{ $category->hwm_id === $entry->$columnName ? 'selected' : '' }}>{{ $category->title }}</option>
    @endforeach
        <option value="" {{ !$entry->$columnName ? 'selected' : '' }}>-</option>
</select>
