<?php

use App\HWM\Enums\EntityType;
use App\HWM\Enums\LotType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLotFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lot_filters', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->enum('lot_type', enumToArrayOfValues(LotType::cases()));
            $table->enum('entity_type', enumToArrayOfValues(EntityType::cases()));
            $table->string('entity_id')->nullable();
            $table->json('terms');
            $table->tinyInteger('is_active')->default(1);
            $table->unsignedInteger('order')->default(0);
            $table->timestamps();

            $table->index(['entity_id', 'entity_type']);

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lot_filters');
    }
}
