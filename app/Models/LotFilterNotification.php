<?php

declare(strict_types=1);

namespace App\Models;

use App\Core\ValueObject\UserId;
use App\Services\Notifications\Enums\Channel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\HWM\Entities\Lot;

class LotFilterNotification extends Model
{
    public $timestamps = false;
    protected $dateFormat = 'Y-m-d H:i:s.u';
    protected $casts = [
        'created_at' => 'datetime',
        'data' => 'array',
    ];
    protected $fillable = [
        'user_id',
        'channel',
        'created_at',
        'lot_id',
        'created_diff_seconds',
        'data',
    ];

    /**
     * @param LotFilter[] $filters
     *
     * @return $this
     */
    public static function addRecord(
        UserId $userId,
        Channel $channel,
        Lot $lot,
        array $filters
    ): void {
        /** @var $this $model */
        self::create([
            'user_id' => $userId->toString(),
            'channel' => $channel->value,
            'created_at' => now()->format(self::getModel()->getDateFormat()),
            'lot_id' => $lot->lot_id,
            'created_diff_seconds' => Carbon::now()->diffInSeconds($lot->started_at),
            'data' => [
                'filters' => array_map(fn($filter) => [
                    'id' => $filter->id,
                    't' => $filter->terms->toArrayOfValues(),
                ], $filters),
            ],
        ]);
    }
}
