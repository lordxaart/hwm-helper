<?php

namespace Tests\HWM\Parser;

use App\HWM\Entities\Art;
use App\HWM\Entities\Set;
use App\HWM\HtmlParsers\HtmlNode;
use App\HWM\NewParsers\Pages\SetsParser;
use App\HWM\Parsers\HwmParser;
use Tests\TestCase;

class ArtsTest extends TestCase
{
    public $countSets = 39;
    public $countArts = [
        'rec' => 5,
        'dems' => 6,
        'surv' => 23,
        'tm_set' => 63,
    ];

    /**
     * @test
     */
    public function test_sets_parser(string $locale = RU)
    {
        $page = $locale === EN ? 'pages/sets_page_en.html' : 'pages/sets_page.html';
        $sets = (new SetsParser(new HtmlNode($page)))->handle();
        $this->assertCount($this->countSets, $sets);
        foreach ($sets as $set) {
            $setObject = new Set($set['hwm_id'], $set);
            $this->assertTrue($setObject->getHwmId() === $set['hwm_id']);

            if ($locale === RU && array_key_exists($set['hwm_id'], $this->countArts)) {
                $this->assertEquals($this->countArts[$set['hwm_id']], count($set['arts']));
            }
        }

        return $sets;
    }

    /**
     * @test
     */
    public function test_sets_en_parser()
    {
        $this->test_sets_parser(EN);
    }

    /**
     * @test
     */
    public function test_art_page_a_dagger2()
    {
        $this->test_art('a_dagger2');
    }

    /**
     * @test
     */
    public function test_art_page_krest2()
    {
        $this->test_art('krest2');
    }

    /**
     * @test
     */
    public function test_art_page_krest2_en()
    {
        $this->test_art('krest2_en');
    }

    /**
     * @test
     */
    public function test_art_page_thief_paper()
    {
        $this->test_art('thief_paper');
    }

    /**
     * @test
     */
    public function test_art_page_potion02()
    {
        $this->test_art('potion02');
    }

    /**
     * @test
     */
    public function test_art_page_myhelmet15()
    {
        $this->test_art('myhelmet15');
    }

    protected function test_art(string $artId)
    {
        $artId = str_replace('_en', '', $artId);
        $exeptKeys = ['description', 'modifiers', 'image', 'hwm_id'];
        $parser = new HwmParser(file_get_contents(hwm_storage_path("pages/arts/$artId.html")));
        $art = $parser->getArtInfo($artId);
        $this->assertArraySimilar($this->getExample($artId), array_except($art, $exeptKeys));

        $artObject = new Art($art['hwm_id'], $art);
        $this->assertArraySimilar($this->getExample($artId), array_except($artObject->toArray(), $exeptKeys));
        $this->assertEquals($artObject->getHwmId(), $artObject->getModel()->hwm_id);
        $this->assertEquals($artObject->title, $artObject->getModel()->title);
    }

    protected function getExample(string $key): array
    {
        $examples = [
            'a_dagger2' => [
                "title" => "Простой кинжал авантюриста",
                "need_level" => 6,
                "strength" => 1,
                "oa" => 8,
                "repair" => 10000,
                "set_id" => "avan_set",
                "set_name" => "Авантюрист",
                "from_shop" => false,
                "as_from_shop" => false,
                "shop_price" => null,
            ],
            'krest2' => [
                  "title" => "Знамя крестоносца",
                  "need_level" => 9,
                  "strength" => 1,
                  "oa" => 5,
                  "repair" => 9000,
                  "set_id" => null,
                  "set_name" => null,
                  "from_shop" => false,
                  "as_from_shop" => false,
                  "shop_price" => null,
            ],
            'krest2_en' => [
                  "title" => "Crusader gonfalon",
                  "need_level" => 9,
                  "strength" => 1,
                  "oa" => 5,
                  "repair" => 9000,
                  "set_id" => null,
                  "set_name" => null,
                  "from_shop" => false,
                  "as_from_shop" => false,
                  "shop_price" => null,
            ],
            'thief_paper' => [
                  "title" => "Воровское приглашение",
                  "need_level" => 6,
                  "strength" => 1,
                  "oa" => null,
                  "repair" => null,
                  "set_id" => null,
                  "set_name" => null,
                  "from_shop" => false,
                  "as_from_shop" => false,
                  "shop_price" => null,
            ],
            'potion02' => [
                  "title" => "Зелье защиты",
                  "need_level" => 1,
                  "strength" => 1,
                  "oa" => 1,
                  "repair" => null,
                  "set_id" => null,
                  "set_name" => null,
                  "from_shop" => false,
                  "as_from_shop" => false,
                  "shop_price" => null,
            ],
            'myhelmet15' => [
                  "title" => "Шлем пламени",
                  "need_level" => 15,
                  "strength" => 70,
                  "oa" => 7,
                  "repair" => 6583,
                  "set_id" => null,
                  "set_name" => null,
                  "from_shop" => true,
                  "as_from_shop" => false,
                  "shop_price" => 6930,
            ]
        ];

        return $examples[$key];
    }
}
