<?php

declare(strict_types=1);

namespace App\Components\Elastic;

use App\Components\Elastic\Exceptions\ElasticConnectionException;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use Elasticsearch\Common\Exceptions\ServerErrorResponseException;

class ElasticClient
{
    private const CONNECTION_RETRIES = 2;

    public Client $client;

    /**
     * @throws ElasticConnectionException
     */
    public function __construct(array $hosts, string $user = null, $password = null)
    {
        try {
            $client = ClientBuilder::create()
                ->setRetries(self::CONNECTION_RETRIES)
                ->setHosts($hosts);

            if ($user && $password) {
                $client->setBasicAuthentication($user, $password);
            }

            $this->client = $client->build();

            $this->client->ping();
        } catch (ServerErrorResponseException $e) {
            throw new ElasticConnectionException($e->getMessage(), $e->getCode(), $e->getPrevious());
        }
    }
}
