<?php

namespace Tests\Feature\API;

use App\Models\Role;
use App\Models\Art;
use App\Models\ArtCategory;
use App\Models\MarketCategory;
use App\Models\ArtSet;
use Illuminate\Foundation\Testing\WithFaker;
use LaravelPropertyBag\tests\Classes\Admin;
use Tests\Traits\WithStudUser;

class ArtsTest extends ApiTestCase
{
    use WithFaker;
    use WithStudUser;

    const TEST_ART_ID = 'coldring_n';
    const TEST_UNEXIST_ART_ID = 'undefined_art_id';

    /**
     * @test
     */
    public function check_arts_response()
    {
        $response = $this->getJson(self::API_ARTS_URL);

        // check json structure for response
        $response->assertJsonStructure(['data' => [$this->getArtsResponseJsonStructure()]]);
    }

    /**
     * @test
     */
    public function check_art_categories_response()
    {
        $this->checkCategoryResponse(self::API_CATEGORIES_URL, ArtCategory::class);
    }

    /**
     * @test
     */
    public function check_art_market_categories_response()
    {
        $this->checkCategoryResponse(self::API_MARKET_CATEGORIES_URL, MarketCategory::class);
    }

    /**
     * @test
     */
    public function check_art_sets_response()
    {
        $this->checkCategoryResponse(self::API_SETS_URL, ArtSet::class);
    }

    /**
     * @test
     */
    public function check_store_exists_art()
    {
        $artId = self::TEST_ART_ID;

        $response = $this->actingAs($this->createStubUser(Role::ADMIN))
            ->postJson(self::API_ARTS_URL, ['art_id' => $artId]);

        $response->assertStatus(200)->assertJsonStructure(['data' => $this->getArtsResponseJsonStructure()]);

        $this->assertDatabaseHas('arts', ['hwm_id' => $artId]);
    }

    /**
     * @test
     */
    public function check_update_from_original_art()
    {
        // NEED TO DO - add check
        $artId = self::TEST_ART_ID;
        $url = str_replace(
            '{artId}',
            $artId,
            self::API_REFRESH_ART_FROM_ORIGINAL_URL
        );

        $response = $this->actingAs($this->createStubUser(Role::ADMIN))
            ->post($url);

        $response->assertStatus(200)->assertJsonStructure(['data' => $this->getArtsResponseJsonStructure()]);
    }

    /**
     * @test
     */
    public function check_store_or_update_as_guest()
    {
        $response = $this->post(self::API_REFRESH_ART_FROM_ORIGINAL_URL);

        $response->assertStatus(401);

        $response = $this->post(self::API_ARTS_URL);

        $response->assertStatus(401);
    }

    protected function checkCategoryResponse($url, $model)
    {
        $response = $this->getJson($url);

        $catsCount = $model::count();

        $json = $response->decodeResponseJson();

        // check json structure for response
        $response->assertJsonStructure($this->getCategoriesArtResponseJsonStructure());

        // Check count same at in DB
        $this->assertEquals($catsCount, count($json->json('data')));
    }

    protected function getArtsResponseJsonStructure()
    {
        return [
            'hwmId',
            'title',
            'link',
            'image',
            'images',
            'repair',
            'strength',
            'shopPrice',
            'pricePerFight',
            'fromShop',
            'asFromShop',
            'categoryId',
            'marketCategoryId',
            'setId',
            'createdAt',
            'updatedAt',
        ];
    }

    protected function getCategoriesArtResponseJsonStructure()
    {
        return [
            'data' => [
                [
                    'hwmId',
                    'title',
                ]
            ]
        ];
    }
}
