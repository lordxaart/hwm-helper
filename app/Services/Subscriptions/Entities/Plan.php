<?php

declare(strict_types=1);

namespace App\Services\Subscriptions\Entities;

class Plan
{
    public readonly int $id;
    public readonly int $subscription_id;
    public readonly string $alias;
    public readonly string $name;
    public readonly string $duration;
    public readonly ?string $trial;
    public readonly \DateInterval $duration_interval;
    public readonly ?\DateInterval $trial_interval;
    public readonly int $price_full;
    public readonly float $price;
    public readonly string $currency;

    public function __construct(
        int $id,
        int $subscription_id,
        string $alias,
        string $name,
        string $duration,
        ?string $trial,
        int $price,
        string $currency
    ) {
        $this->id = $id;
        $this->subscription_id = $subscription_id;
        $this->alias = $alias;
        $this->name = $name;
        $this->duration = $duration;
        $this->trial = $trial;
        $this->price_full = $price;
        $this->price = $price / 100;
        $this->currency = $currency;
        $this->duration_interval = new \DateInterval($duration);
        $this->trial_interval = $trial ? new \DateInterval($trial) : null;
    }

    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'alias' => $this->alias,
        ];
    }
}
