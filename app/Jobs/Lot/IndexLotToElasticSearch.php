<?php

declare(strict_types=1);

namespace App\Jobs\Lot;

use App\Core\Enums\Queue;
use App\HWM\Entities\Lot;
use App\Services\SearchLots\Repositories\ElasticLotRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class IndexLotToElasticSearch implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected Lot $lot;

    public function __construct(Lot $lot)
    {
        $this->lot = $lot;

        $this->queue = Queue::HWM_ES_LOTS->value;
    }

    public function handle(ElasticLotRepository $repository): bool
    {
        if (!$repository->addLotToIndex($this->lot)) {
            throw new \Exception('Can not reindex lot ' . $this->lot->lot_id);
        }

        return true;
    }

    public function uniqueId(): int
    {
        return $this->lot->lot_id;
    }
}
