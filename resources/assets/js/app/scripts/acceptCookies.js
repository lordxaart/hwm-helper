import Cookies from 'js-cookie';

const acceptAlert = document.querySelector('.cookiealert');

if (acceptAlert) {
    setTimeout(() => {
        acceptAlert.classList.add('show');
    }, 1000);

    acceptAlert.querySelector('button').addEventListener('click', () => {
        Cookies.set('accept_cookies', 1, { expires: 365, path: '/' });
        acceptAlert.classList.add('hide');
        acceptAlert.classList.remove('show');
    });
}
