<?php

use App\Models\Lot;
use Illuminate\Database\Migrations\Migration;

class RenamePrimaryColumnForLots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('lots', 'id')) {
            $table = Lot::getModel()->getTable();

            DB::statement("ALTER TABLE `$table` MODIFY COLUMN id INT"); // delete auto increment
            DB::statement("ALTER TABLE `$table` DROP PRIMARY KEY, ADD PRIMARY KEY (`lot_id`, `started_at_year`) USING BTREE"); // change primary key
            DB::statement("ALTER TABLE `$table` DROP `id`;"); // drop ID column
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
