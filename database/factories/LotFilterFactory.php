<?php

declare(strict_types=1);

namespace Database\Factories;

use App\HWM\Enums\EntityType;
use App\HWM\Enums\LotType;
use App\Models\LotFilter;
use App\Services\LotFilter\Enums\FilterLotField;
use App\Services\LotFilter\Enums\FilterOperator;
use App\Services\LotFilter\ValueObjects\FilterTerm;
use App\Services\LotFilter\ValueObjects\FilterTermCollection;
use Illuminate\Database\Eloquent\Factories\Factory;

class LotFilterFactory extends Factory
{
    protected $model = LotFilter::class;

    /**
     * @return array
     */
    public function definition()
    {
        return [
            'lot_type' => LotType::SALE->value,
            'entity_type' => EntityType::CERTIFICATE,
            'entity_id' => null,
            'terms' => $this->generateRandomTerms(),
            'is_active' => true,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }

    private function generateRandomTerms(): FilterTermCollection
    {
        $terms = [
            sprintf(
                '%s %s %s',
                FilterLotField::C_STRENGTH->value,
                FilterOperator::LESS_OR_EQUAL->value,
                mt_rand(1, 100),
            ),
            sprintf(
                '%s %s %s',
                FilterLotField::B_STRENGTH->value,
                FilterOperator::MORE->value,
                mt_rand(1, 100),
            ),
        ];

        return FilterTerm::parseCollectionFromArray($terms);
    }

}
