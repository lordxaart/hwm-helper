<?php

declare(strict_types=1);

namespace App\HWM\Repositories;

use App\HWM\Repositories\Base\StorageRepository;
use Illuminate\Support\Collection;

class ExperienceRepository extends StorageRepository
{
    protected function getStorageKey(): string
    {
        return 'exp_tables';
    }

    protected function mapData(array $data): Collection
    {
        return collect($data);
    }

    protected function validateData(): void
    {
        //
    }
}
