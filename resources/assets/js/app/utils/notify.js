import Noty from 'noty';

Noty.overrideDefaults({
    layout: 'bottomRight',
    theme: 'darkly',
    timeout: 5000, //
    progressBar: true,
    closeWith: ['click', 'button'],
});

const parseMessage = (message, title = null) => {
    if (title) {
        // eslint-disable-next-line no-param-reassign
        message = `<h6><strong>${title}</strong></h6>${message}`;
    }

    return message;
};

const layouts = {
    error: 'topRight',
    warning: 'topRight',
};

const timeouts = {
    error: 10000,
    warning: 8000,
    alert: 25000,
    info: 25000,
};

const noty = (message, type = 'alert') => {
    new Noty({
        text: message,
        type,
        layout: layouts[type] || 'bottomRight',
        timeout: timeouts[type] || 5000,
    }).show();
};

const notify = {
    error: (message, title = null) => {
        noty(parseMessage(message, title), 'error');
    },
    success: (message, title = null) => {
        noty(parseMessage(message, title), 'success');
    },
    warning: (message, title = null) => {
        noty(parseMessage(message, title), 'warning');
    },
    info: (message, title = null) => {
        noty(parseMessage(message, title), 'info');
    },
    alert: (message, title = null) => {
        noty(parseMessage(message, title), 'alert');
    },
    all: (message, title = null) => {
        noty(parseMessage(message, title), 'success');
        noty(parseMessage(message, title), 'info');
        noty(parseMessage(message, title), 'alert');
        noty(parseMessage(message, title), 'warning');
        noty(parseMessage(message, title), 'error');
    },
};

export default notify;
