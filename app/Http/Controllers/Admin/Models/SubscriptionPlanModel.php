<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin\Models;

use Faker\Core\DateTime;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;

class SubscriptionPlanModel extends Model
{
    protected $table = 'subscription_plans';

    protected $appends = ['end_at', 'trial_at'];

    public function endAt(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->durationToTime($this->duration, new \DateTimeImmutable())
        );
    }

    public function trialAt(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->trial ? $this->durationToTime($this->trial, new \DateTimeImmutable()) : null
        );
    }

    private function durationToTime(string $duration, \DateTimeImmutable $from): \DateTimeImmutable
    {
        return $from->add(new \DateInterval($duration));
    }
}
