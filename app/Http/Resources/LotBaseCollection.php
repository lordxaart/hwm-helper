<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Http\Resources\Core\ApiCollection;
use Illuminate\Http\Request;

class LotBaseCollection extends ApiCollection
{
    public $collects = LotBaseResource::class;

    private array $accordedFilters = [];

    private array $entities = [];

    public function setAccordedFilters(array $filters): self
    {
        $this->accordedFilters = $filters;
        return $this;
    }

    public function setEntities(array $entities): self
    {
        $this->entities = $entities;
        return $this;
    }

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return $this->collection->map(function (LotBaseResource $resource) {
            if (isset($this->accordedFilters[$resource->resource->lot_id])) {
                $resource->setFilters($this->accordedFilters[$resource->resource->lot_id]);
            }
            $entityId = $resource->resource->entity_type . '_' . $resource->resource->entity_id;
            if (!empty($this->entities[$entityId])) {
                $resource->setEntity($this->entities[$entityId]);
            }

            return $resource;
        })->values()->toArray();
    }
}
