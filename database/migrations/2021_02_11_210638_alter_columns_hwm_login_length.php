<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterColumnsHwmLoginLength extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(\App\Models\Lot::getModel()->getTable(), function (Blueprint $table) {
            $table
                ->string('seller_name', config('hwm.rules.max_login_length'))
                ->nullable()
                ->change();
        });

        Schema::table(\App\Models\Character::getModel()->getTable(), function (Blueprint $table) {
            $table
                ->string('login', config('hwm.rules.max_login_length'))
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
