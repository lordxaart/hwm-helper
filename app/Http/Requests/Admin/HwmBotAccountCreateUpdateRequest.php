<?php

declare(strict_types=1);

namespace App\Http\Requests\Admin;

use App\Models\User;
use App\Components\Validators\Rules\OldPasswordConfirmed;
use Illuminate\Foundation\Http\FormRequest;

class HwmBotAccountCreateUpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'login' => 'required',
            'password' => 'required',
            'target' => 'required',
        ];
    }
}
