// Loaded all scripts from functions
const context = require.context('JS/app/scripts', true, /\.js$/);
context.keys().forEach((key) => {
    context(key);
});

// Register service worket
if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
        navigator.serviceWorker.register('/sw.js');
    });
} else {
    console.warn('ServiceWorker is not available');
}
