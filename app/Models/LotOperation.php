<?php

declare(strict_types=1);

namespace App\Models;

use App\ValueObjects\HashSha1;
use Illuminate\Database\Eloquent\Model;
use App\HWM\Objects\LotOperation as Entity;
use Illuminate\Database\QueryException;

class LotOperation extends Model
{
    protected $table = 'lot_operations';

    public $timestamps = false;

    protected $casts = [
        'time' => 'date',
        'data' => 'array',
    ];

    protected $fillable = [
        'lot_id',
        'type',
        'time',
        'buyed_quantity',
        'current_quantity',
        'current_price',
        'rate_diff',
        'buyer_id',
        'buyer_name',
    ];

    public static function prepareFromEntity(int $lotId, Entity $entity): array
    {
        $operation = $entity->toArray();
        $operation['id'] = self::makeHashFromEntity($lotId, $entity)->getValue();
        $operation['lot_id'] = $lotId;
        $operation['time'] = $entity->time->format(config('app.date_format'));

        return $operation;
    }

    public static function makeHashFromEntity(int $lotId, Entity $entity): HashSha1
    {
        $data = $entity->toArray();
        $data['lot_id'] = $lotId;
        $data['time'] = $entity->time->format(DATE_FORMAT);
        $data = array_filter($data);
        $data = serialize($data);

        return new HashSha1(sha1($data));
    }

    public static function createFromArray(int $lotId, array $operations): void
    {
        LotOperation::where('lot_id', $lotId)->delete();

        /** @var \App\HWM\Objects\LotOperation $operation */
        foreach ($operations as $operation) {
            LotOperation::insert(LotOperation::prepareFromEntity($lotId, $operation));
        }
    }
}
