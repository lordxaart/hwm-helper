<?php

declare(strict_types=1);

namespace App\HWM\Exceptions;

class ParserError extends HwmException
{
}
