import snakeCase from 'lodash/snakeCase';
import request from 'JS/app/utils/request';
import cloneDeep from 'lodash/cloneDeep';

const initialState = {
    filters: [],
    loadingFilters: false,
};

const getters = {
    getFilters(state) {
        return state.filters;
    },
    getLoading(state) {
        return state.loadingFilters;
    },
};

function mapFilterToRequest(filter) {
    const result = {};

    Object.keys(filter).forEach((key) => {
        result[snakeCase(key)] = filter[key];
    });

    return result;
}

const actions = {
    async loadFilters({ state, commit }) {
        if (state.loadingArts) {
            return;
        }

        commit('SET_LOADING', true);

        const params = {
            limit: 200,
            offset: 0,
        };
        try {
            const response = await request.get('/api/v1/market/lots/filters', { params });
            commit('SET_FILTERS', response.data.data);
            commit('SET_LOADING', false);
        } catch (e) {
            commit('SET_LOADING', false);
            throw e;
        }
    },
    async createFilter({ commit }, { filter }) {
        commit('SET_LOADING', true);

        const body = mapFilterToRequest(filter);

        try {
            const response = await request.post('/api/v1/market/lots/filters', body);
            commit('SET_LOADING', false);

            if (response.status === 201) {
                commit('ADD_FILTER', { filter: response.data.data });

                return true;
            }

            return false;
        } catch (e) {
            commit('SET_LOADING', false);
            throw e;
        }
    },
    async updateFilter({ commit }, { filterId, data }) {
        commit('SET_LOADING', true);

        const body = mapFilterToRequest(data);

        try {
            const response = await request.post(`/api/v1/market/lots/filters/${filterId}`, body);
            commit('SET_LOADING', false);

            if (response.status === 200) {
                commit('UPDATE_FILTER', { filter: response.data.data });

                return true;
            }

            return false;
        } catch (e) {
            commit('SET_LOADING', false);
            throw e;
        }
    },
    async deleteFilter({ commit }, { filterId }) {
        commit('SET_LOADING', true);

        try {
            const response = await request.delete(`/api/v1/market/lots/filters/${filterId}`);
            commit('SET_LOADING', false);

            if (response.status === 204) {
                commit('REMOVE_FILTER', { filterId });

                return true;
            }

            return false;
        } catch (e) {
            commit('SET_LOADING', false);
            throw e;
        }
    },
    async updateNotifyChannel({ commit }, { filterId, channel, isAvailable }) {
        commit('SET_LOADING', true);

        const body = {
            channel,
            is_available: isAvailable,
        };

        try {
            await request.post(`/api/v1/market/lots/filters/${filterId}/notification`, body);
            commit('SET_LOADING', false);

            commit('UPDATE_FILTER_NOTIFICATIONS', { filterId, channel, isAvailable });

            return true;
        } catch (e) {
            commit('SET_LOADING', false);
            throw e;
        }
    },
};

const mutations = {
    SET_FILTERS(state, data) {
        if (!Array.isArray(data)) {
            throw new Error('Filters Data is not array');
        }

        state.filters = data;
    },
    ADD_FILTER(state, { filter }) {
        state.filters.unshift(filter);
    },
    UPDATE_FILTER(state, { filter }) {
        const filterIndex = state.filters.findIndex((innerFilter) => innerFilter.id === filter.id);

        if (filterIndex !== -1) {
            state.filters.splice(filterIndex, 1, filter);
        }
    },
    REMOVE_FILTER(state, { filterId }) {
        const filterIndex = state.filters.findIndex((filter) => filter.id === filterId);

        if (filterIndex !== -1) {
            state.filters.splice(filterIndex, 1);
        }
    },
    UPDATE_FILTER_NOTIFICATIONS(state, { filterId, channel, isAvailable }) {
        const filterIndex = state.filters.findIndex((filter) => filter.id === filterId);

        if (filterIndex === -1) {
            return;
        }

        const filter = cloneDeep(state.filters[filterIndex]);
        filter.notifications[channel] = isAvailable;
        state.filters.splice(filterIndex, 1, filter);
    },
    SET_LOADING(state, value) {
        state.loadingFilters = value;
    },
};

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations,
};
