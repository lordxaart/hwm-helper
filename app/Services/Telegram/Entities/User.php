<?php

declare(strict_types=1);

namespace App\Services\Telegram\Entities;

class User
{
    public readonly int $id;
    public readonly bool $is_bot;
    public readonly string | null $first_name;
    public readonly string | null $last_name;
    public readonly string | null $username;
    public readonly string | null $lang;

    public function __construct(array $user)
    {
        $this->id = (int) $user['id'];
        $this->is_bot = (bool) ($user['is_bot'] ?? false);
        $this->first_name = $user['first_name'] ?? null;
        $this->last_name = $user['last_name'] ?? null;
        $this->username = $user['username'] ?? null;
        $this->lang = $user['language_code'] ?? null;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'is_bot' => $this->is_bot,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'username' => $this->username,
            'lang' => $this->lang,
        ];
    }
}
