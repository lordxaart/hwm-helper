<?php

declare(strict_types=1);

namespace App\Services\Notifications\Channels\Telegram;

use App\Services\Notifications\Channels\Telegram\Enums\Emoji;
use Exception;
use Monolog\Formatter\FormatterInterface;
use Monolog\Formatter\LineFormatter;
use Monolog\LogRecord;

class TelegramMessageFormatter extends LineFormatter implements FormatterInterface
{
    public function __construct(
        ?string $format = null,
        ?string $dateFormat = null,
        bool $allowInlineLineBreaks = false,
        bool $ignoreEmptyContextAndExtra = false
    ) {
        parent::__construct($format, $dateFormat, $allowInlineLineBreaks, $ignoreEmptyContextAndExtra);

        // Disable errors stacktrace
        $this->includeStacktraces(false);
    }

    public static function getEmoji(string $type): string
    {
        $emoji = [
            'emergency' => Emoji::FIRE->value,
            'alert' => Emoji::FIRECRACKER->value,
            'critical' => Emoji::COLLISION->value,
            'error' => Emoji::COLLISION->value,
            'warning' => Emoji::WARNING->value,
            'notice' => Emoji::BELL->value,
            'info' => Emoji::INFORMATION->value,
            'debug' => Emoji::LOUDSPEAKER->value,

            'new' => Emoji::NEW_BUTTON->value,
            'success' => Emoji::OK_HAND->value,
        ];

        return $emoji[strtolower($type)] ?? Emoji::INFORMATION->value;
    }

    public function format(LogRecord $record): string
    {
        $record = $record->toArray();
        $output = self::getEmoji($record['level_name']) . " [{$this->formatDate($record['datetime'])}]";

        $output .= " {$record['level_name']} ( " . getServerIp() . " )\n";

        $output .= "{$record['message']}\n";

        if (isset($record['context']['exception']) && $record['context']['exception'] instanceof Exception) {
            /** @var Exception $e */
            $e = $record['context']['exception'];
            $output .= "File: {$e->getFile()} ({$e->getLine()})\n";
            unset($record['context']['exception']);
        }

        if (!empty($record['context'])) {
            $output .= "Context: " . json_encode($record['context']) . "\n";
        }

        if (!empty($record['extra'])) {
            $output .= "Extra: " . json_encode($record['extra']) . "\n";
        }

        return $output;
    }
}
