<?php

namespace Deployer;

desc('Restart supervisor');
task('supervisor:restart', function () {
    run('service supervisor restart');
});

// Rewrite composer install - delete --no-dev parameters (for phpunit)
task('deploy:vendors', function () {
    run('cd {{ release_path }} && {{ bin/composer }} install --verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader --no-suggest');
});

// pull git lfs
desc('Git LFS pull');
task('git_lfs:update', function () {
    run('cd {{ release_path }} && git lfs pull');
});

desc('Set http user for deploy path');
task('permissions:fix', function () {
    run('chown -R {{ http_user }} {{ deploy_path }}');
});

// Code from deployer official repository (npm receipt)
task('api_node_npm:install', function () {
    set('bin/npm', function () {
        return run('which npm');
    });

    if (!get('api_node_path')) {
        set('api_node_path', 'hwm-bot-api');
    }

    if (has('previous_release')) {
        if (test('[ -d {{previous_release}}/{{api_node_path}}/node_modules ]')) {
            run('cp -R {{previous_release}}/{{api_node_path}}/node_modules {{release_path}}');

            // If package.json is unmodified, then skip running `npm install`
            if (!run('diff {{previous_release}}/{{api_node_path}}/package.json {{release_path}}/{{api_node_path}}/package.json')) {
                return;
            }
        }
    }
    run("cd {{release_path}}/{{api_node_path}} && {{bin/npm}} install");
    writeln('<info>Npm (' . get('api_node_path') . ') install complete!</info>');
});

set('branch', function () {
    if (input()->hasOption('branch') && !empty(input()->getOption('branch'))) {
        return input()->getOption('branch');
    }
    return null;
});
task('deploy:update_code', function () {
    $repository = get('repository');
    $branch = get('branch');
    $git = get('bin/git');

    $at = 'HEAD';
    if (!empty($branch)) {
        $at = $branch;
    }

    // If option `tag` is set.
    if (input()->hasOption('tag')) {
        $tag = input()->getOption('tag');
        if (!empty($tag)) {
            $at = $tag;
        }
    }

    // If option `tag` is not set and option `revision` is set.
    if (empty($tag)) {
        $revision = input()->getOption('revision');
        if (!empty($revision)) {
            $at = $revision;
        }
    }

    // Populate known hosts.
    preg_match('/.*(@|\/\/)([^\/:]+).*/', $repository, $match);
    if (isset($match[2])) {
        $repositoryHostname = $match[2];
        try {
            run("ssh-keygen -F $repositoryHostname");
        } catch (RunException $e) {
            run("ssh-keyscan -H $repositoryHostname >> ~/.ssh/known_hosts");
        }
    }

    $bare = parse('{{deploy_path}}/.dep/repo');

    start:
    // Clone the repository to a bare repo.
    run("[ -d $bare ] || mkdir -p $bare");
    run("[ -f $bare/HEAD ] || $git clone --mirror $repository $bare 2>&1");

    cd($bare);

    // If remote url changed, drop `.git/repo` and reinstall.
    if (run("$git config --get remote.origin.url") !== $repository) {
        cd('{{deploy_path}}');
        run("rm -rf $bare");
        goto start;
    }

    // Copy to release_path. With Git LFS Pull
    run("$git remote update && $git lfs pull 2>&1");
    run("$git archive $at | tar -x -f - -C {{release_path}} 2>&1");

    // Put git version
    run("$git --git-dir $bare log --pretty='%h' -n1 HEAD > {{release_path}}/.version");

    // Save revision in releases log.
    $rev = run("$git rev-list $at -1");
    run("sed -ibak 's/revision/$rev/' {{deploy_path}}/.dep/releases");
});
