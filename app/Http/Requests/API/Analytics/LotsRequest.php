<?php

declare(strict_types=1);

namespace App\Http\Requests\API\Analytics;

use App\Components\Validators\Rules\Lot\IsEntityType;
use App\Services\SearchLots\ValueObjects\Aggregation\AggregationsByEntityRequest;
use Illuminate\Foundation\Http\FormRequest;

class LotsRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'entity_type' => [
                'required',
                new IsEntityType(),
            ],
            'entity_id' => [
                'nullable',
                'string',
            ],
            'from' => [
                'required',
                'date_format:' . DATE_SHORT_FORMAT,
            ],
            'to' => [
                'required',
                'date_format:' . DATE_SHORT_FORMAT,
            ],
            'interval' => [
                'required',
                'in:' . implode(',', AggregationsByEntityRequest::AVAILABLE_INTERVALS),
            ],
            'metrics' => [
                'required',
            ],
        ];
    }
}
