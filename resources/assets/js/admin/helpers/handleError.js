import { forEach } from 'lodash';
import Noty from 'noty';

const handleError = (error) => {
    const res = error.response;

    if (!res) {
        console.log(error);
    }

    res.status_code = Number.parseInt(res.status_code, 10);

    if (res.status_code === 422) {
        let message = '';
        forEach(res.data.errors, (value) => {
            value.forEach((msg) => {
                message += `<p>${msg}</p>`;
            });
        });

        new Noty({
            type: 'error',
            text: message,
        }).show();
    } else {
        new Noty({
            type: 'error',
            text: `${res.data.message} ${res.status}`,
        }).show();
    }
};

export default handleError;
