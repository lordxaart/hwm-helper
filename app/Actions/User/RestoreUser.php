<?php

declare(strict_types=1);

namespace App\Actions\User;

use App\Models\User;
use InvalidArgumentException;
use Lorisleiva\Actions\Concerns\AsAction;

class RestoreUser
{
    use AsAction;

    public function handle(User $user): bool
    {
        if (!$user->trashed()) {
            throw new InvalidArgumentException('Cannot restore not deleted user');
        }

        return (bool) $user->restore();
    }
}
