<?php

declare(strict_types=1);

namespace App\HWM\Repositories\Interfaces;

use App\HWM\Entities\Interfaces\HwmEntityInterface;
use Illuminate\Support\Collection;

interface EntityRepositoryInterface
{
    public function all(): Collection;
    public function findByHwmId(string $id): ?HwmEntityInterface;
    public function findByTitle(string $title): ?HwmEntityInterface;
    public function has(string $hwmId): bool;
}
