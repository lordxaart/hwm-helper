<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

class MarketCategoryCrudController extends BaseCategoryCrudController
{
    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel('App\Models\MarketCategory');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/market-categories');
        $this->crud->setEntityNameStrings('market-category', 'market-categories');

        parent::setup();
    }

    protected function setupListOperation()
    {
        parent::setupListOperation();

        $this->crud->addColumn(['name' => 'only_art', 'label' => 'Only Art', 'type' => 'boolean']);
    }

    protected function setupShowOperation()
    {
        parent::setupShowOperation();

        $this->crud->addColumn(['name' => 'only_art', 'label' => 'Only Art', 'type' => 'boolean']);
    }

    protected function setupUpdateOperation()
    {
        parent::setupUpdateOperation();

        $this->crud->addField(['name' => 'only_art', 'label' => 'Only Art', 'type' => 'boolean',]);
    }
}
