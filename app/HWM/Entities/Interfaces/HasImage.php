<?php

declare(strict_types=1);

namespace App\HWM\Entities\Interfaces;

interface HasImage
{
    public function getImage(bool $realPath = false): string;
}
