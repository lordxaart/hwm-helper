<?php

declare(strict_types=1);

namespace App\Notifications\User;

use App\Core\ValueObject\UserId;
use App\Services\Notifications\Channels\Telegram\Entities\TelegramMessage;
use App\Services\Notifications\Enums\Channel;
use App\Services\Notifications\Enums\NotifyType;
use App\Core\Enums\Queue;
use App\Notifications\MarkdownHelpers;
use App\Services\Notifications\UserNotifyService;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Arr;

class ChangeStatus extends Notification
{
    use Queueable;
    use MarkdownHelpers;

    protected string $action;

    protected UserId | null $adminId;

    protected array $availableActions = User::ADMIN_ALL_ACTIONS;

    public function __construct(string $action, UserId $adminId = null)
    {
        $this->queue = Queue::NOTIFICATIONS->value;

        if (!in_array($action, $this->availableActions)) {
            throw new \InvalidArgumentException(trans(':action is unsupported for this notification', ['action' => ucfirst($action)]));
        }

        $this->action = $action;
        $this->adminId = $adminId;
    }

    public function via(User $user): array
    {
        $channels = app(UserNotifyService::class)
            ->getConfiguredChannels($user, NotifyType::IMPORTANT);

        return [...Arr::only($channels, [
            Channel::MAIL->value,
            Channel::TELEGRAM->value,
        ]), Channel::DATABASE];
    }

    public function toTelegram(User $user): TelegramMessage
    {
        $content = $this->getMessage($user) . ' ' . trans('app.notifications.change_status.contact_to_admin');

        return TelegramMessage::create()->content($content);
    }

    public function toMail(User $user): MailMessage
    {
        return (new MailMessage())
            ->subject(trans('app.notifications.change_status.subject', ['action' => trans('app.user.actions.' . $this->action)]))
            ->line($this->getMessage($user))
            ->line(trans('app.notifications.change_status.contact_to_admin'))
            ->action(trans('app.notifications.change_status.action'), route('contacts'));
    }

    protected function getMessage(User $user): string
    {
        return trans('app.notifications.change_status.your_account_was', [
            'action' => mb_strtoupper(trans('app.user.actions.' . $this->action)),
            'until_to' => $user->blockedUntilTo()
                ? trans('app.notifications.change_status.until_to_time', [
                    'time' => $user->blockedUntilTo()->format(config('app.date_format'))
                ])
                : '',
        ]);
    }

    public function toArray(): array
    {
        return [
            'action' => $this->action,
            'admin_id' => $this->adminId?->value,
        ];
    }
}
