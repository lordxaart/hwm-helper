<div class="d-flex justify-content-center socials-blocks">
    @forelse(\App\Core\Enums\SocialProvider::cases() as $provider)
        <a
            href="{{ route('login.social', ['social' => $provider->value]) }}"
            class="btn btn-link social-{{ $provider->value }}"
            v-tooltip="$t('app.auth.{{ $action }}_with_social', { social: '{{ $provider->getTitle() }}' })"
        >
            <icon name="{{ $provider->value }}" size="2x"></icon>
        </a>
    @empty
        <p class="text-info">@{{ $t('app.auth.socials_comming_soon') }}</p>
    @endforelse
</div>
