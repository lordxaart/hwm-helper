<?php

declare(strict_types=1);

namespace App\HWM\Exceptions\ParserErrors;

use App\HWM\Exceptions\ParserError;

class EmptyArtPage extends ParserError
{
    public function __construct(string $artId)
    {
        $message = trans('errors.hwm.not_found_art_page') . ' [' . $artId . ']';

        parent::__construct($message);
    }
}
