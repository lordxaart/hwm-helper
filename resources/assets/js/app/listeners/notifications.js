import notify from 'JS/app/utils/notify';
import i18n from 'JS/app/utils/localization';
import {
    LotToNotifyBlock,
    LotToNotifyBlockShort,
    ArtToNotifyBlockShort,
    ChangeSettings,
} from './_templates';

const notificationTypes = {
    accorded_filter: 'accorded_filter',
    found_new_lot: 'found_new_lot',
    found_new_art: 'found_new_art',
    change_settings: 'change_settings',
    test: 'test',
};

export default (userId) => {
    if (!window.Echo) {
        return;
    }

    window.Echo.private(`App.Models.User.${userId}`)
        .notification((notification) => {
            if (notification.type === notificationTypes.accorded_filter) {
                const title = i18n.t('app.notifications.browser.new_lot_for_filter', { filter: notification.filter_id });
                const message = LotToNotifyBlock(notification.lot);

                notify.alert(message, title);
            }

            if (notification.type === notificationTypes.found_new_lot) {
                const title = i18n.t('app.notifications.found_new_lot');
                const message = LotToNotifyBlockShort(notification.lot);

                notify.alert(message, title);
            }

            if (notification.type === notificationTypes.found_new_art) {
                const title = i18n.t('app.notifications.found_new_art');
                const message = ArtToNotifyBlockShort(notification.art);

                notify.info(message, title);
            }

            if (notification.type === notificationTypes.change_settings) {
                const title = i18n.t('app.notifications.value_for_account_changed');
                const message = ChangeSettings(notification.items);

                notify.info(message, title);
            }

            if (notification.type === notificationTypes.test) {
                notify.alert(notification.message);
            }
        });
};
