import { objectToQueryString } from 'JS/app/utils/helpers';
import { links } from 'JS/app/utils/hwm/static';
import has from 'lodash/has';

class Link {
    constructor(host) {
        this.host = host;
    }

    setHost(host) {
        this.host = host;
    }

    to(name, attributes = null) {
        if (!has(links, name)) {
            console.error(`Can't find link "${name}"`);
            return null;
        }

        let queryString = '';

        if (attributes && typeof attributes === 'object') {
            queryString = objectToQueryString(attributes);
        }

        return this.host + links[name] + queryString;
    }

    art(id) {
        return this.to('art_info', { id });
    }

    character(id) {
        return this.to('profile_info', { id });
    }

    lot(id, crc = null) {
        return this.to('lot_info', { id, crc });
    }

    market(art, category, sort = 0, sbn = 1, sau = 0, onlyWhole = 0) {
        return this.to('auction', {
            art_type: art,
            cat: category,
            sort, // 0 - cheapest
            sbn, // 1 - Only sold
            sau, // 0 - Only bargaining
            snew: onlyWhole, // 1 - Only whole
        });
    }

    // eslint-disable-next-line class-methods-use-this
    calculator(params) {
        return `${window.location.origin}/calculator${objectToQueryString(params)}`;
    }
}

export default Link;
