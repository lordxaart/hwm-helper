<?php

use App\HWM\Enums\EntityType;
use App\HWM\Enums\LotType;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('market_lots', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('lot_id')->unique();
            $table->string('lot_type')->default(LotType::SALE->value);
            $table->string('art_type', config('hwm.rules.max_hwm_id_length'))->default(EntityType::ART->value);

            $table->string('art_id', config('hwm.rules.max_hwm_id_length'))->nullable();
            $table->string('art_name', config('hwm.rules.max_art_title_length'));
            $table->unsignedInteger('art_uid')->nullable();
            $table->string('art_crc', 10)->nullable();

            $table->string('craft')->nullable();

            $table->unsignedInteger('current_strength')->nullable();
            $table->unsignedInteger('base_strength')->nullable();

            $table->tinyInteger('start_quantity')->default(1);
            $table->tinyInteger('current_quantity')->default(1);

            $table->unsignedBigInteger('start_price');
            $table->unsignedBigInteger('current_price');
            $table->unsignedBigInteger('blitz_price')->nullable();

            $table->unsignedDecimal('price_per_fight', 10, 2)->nullable();

            $table->unsignedInteger('seller_id'); // hwm seller id
            $table->string('seller_name', config('hwm.rules.max_login_length'))->nullable();

            $table->text('operations')->nullable(); // all operations from lot page (created, sale, rate, end)

            $table->timestamp('started_at')->nullable(); // date of created lot
            $table->timestamp('buyed_at')->nullable(); // date of buyed lot
            $table->timestamp('ended_at')->nullable(); // date of end lot (created + duration), getting from lot page
            $table->unsignedInteger('duration')->nullable(); // duration of lot in seconds

            $table->tinyInteger('is_parsed')->default(0); // status if lot already parsed after created (for parser)

            $table->timestamps();

            // indexes
            $table->index(['art_type', 'lot_type']);
            $table->index('art_id');
            $table->index('seller_id');
            $table->index('price_per_fight');
            $table->index('buyed_at');
            $table->index('started_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lots');
    }
}
