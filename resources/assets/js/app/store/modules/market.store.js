import Vue from 'vue';
import dayjs from 'dayjs';

import config from 'JS/app/config';
import cache from 'JS/app/utils/cache';
import Craft from 'JS/app/utils/hwm/objects/Craft';
import request from 'JS/app/utils/request';
import { objectToQueryString, parseIncludes } from 'JS/app/utils/helpers';

const st = {
    loadingElementPrices: false,
    elementPrices: {},
    artLots: {},
    artLotsMeta: {},
    loadingLotsAll: false,
    loadingLots: {},
    loadArtsFromCache: true,
};

const getters = {
    getElementPrices: (state) => state.elementPrices,
    getLoadingPrices: (state) => state.loadingElementPrices,
    getLoadingLots: (state) => state.loadingLotsAll,
    getLotsByArt: (state) => (artId) => (state.artLots[artId] ? state.artLots[artId] : []),
    getMetaByArt: (state) => (artId) => (state.artLotsMeta[artId] ? state.artLotsMeta[artId] : {}),
    getLoadingLotsByArt: (state) => (artId) => state.loadingLots[artId] || false,
};

const actions = {
    async loadElementPrices({ commit }) {
        commit('START_LOADING');

        try {
            const response = await request.get('/api/v1/market/elements');
            commit('SET_ELEMENT_PRICES', response.data.data);
            commit('END_LOADING');
        } catch (e) {
            commit('END_LOADING');
            throw e;
        }
    },
    updateElementAvgPrice({ commit }, payload) {
        commit('SET_ELEMENT_AVG_PRICE', {
            element: payload.element,
            price: payload.price,
        });
    },
    async loadLotsByArt({ state, commit, dispatch }, { artId, include }) {
        if (state.loadArtsFromCache) {
            dispatch('loadLotsByArtFromCache', artId);
        }

        // if (state.loadingLotsAll) {
        //     return;
        // }
        if (!include || !Array.isArray(include)) {
            include = [];
        }
        include.push('accorded_filters');

        const includes = parseIncludes(include);

        commit('START_LOADING', 'loadingLotsAll');
        commit('START_LOADING_ART_LOT', artId);
        try {
            const response = await request.get(`/api/v1/market/lots?art_id=${artId}&include=${includes}`);
            commit('SET_ART_LOTS', { artId, data: response.data.data, meta: response.data.meta });
            commit('END_LOADING', 'loadingLotsAll');
            commit('END_LOADING_ART_LOT', artId);
            dispatch('updateCache', {
                key: config.cache.keys.store.lots_by_art + artId,
                value: state.artLots[artId],
            });
            dispatch('updateCache', {
                key: config.cache.keys.store.meta_by_art + artId,
                value: state.artLotsMeta[artId],
            });
        } catch (e) {
            commit('END_LOADING', 'loadingLotsAll');
            commit('END_LOADING_ART_LOT', artId);
            throw e;
        }
    },
    loadLotsByArtFromCache({ commit }, artId) {
        if (cache.has(config.cache.keys.store.lots_by_art + artId)) {
            const payload = {
                artId,
                data: cache.get(config.cache.keys.store.lots_by_art + artId),
                meta: cache.get(config.cache.keys.store.meta_by_art + artId),
            };
            commit('SET_ART_LOTS', payload);
        }
    },
    // eslint-disable-next-line no-empty-pattern
    async loadStatLots({}, params = null) {
        if (!params) {
            params = {};
        }
        params.fields = 'lotId,marketLink,,lotType,price,blitzPrice,quantity,currentStrength,,baseStrength,pricePerFight,endedAt,startedAt,updatedAt,craft,sellerName,sellerId,artUid,artCrc,entity,isActive,isBuyed,isParsed';
        params.timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;

        const response = await request.get(`/api/v1/market/lots/search${objectToQueryString(params)}`);

        return response.data;
    },

    // eslint-disable-next-line no-empty-pattern
    async updateLot({}, lotId) {
        const response = await request.post('/api/v1/market/updateLot', { lot_id: lotId });

        // eslint-disable-next-line consistent-return
        return response.data.data;
    },

    // eslint-disable-next-line no-empty-pattern
    updateCache({}, payload) {
        cache.set(payload.key, payload.value);
    },
    disableCache({ commit }) {
        commit('DISABLE_LOAD_ARTS_FROM_CACHE');
    },
};

const mutations = {
    START_LOADING(state, key = 'loadingElementPrices') {
        state[key] = true;
    },
    END_LOADING(state, key = 'loadingElementPrices') {
        state[key] = false;
    },
    START_LOADING_ART_LOT(state, artId) {
        Vue.set(state.loadingLots, artId, true);
    },
    END_LOADING_ART_LOT(state, artId) {
        Vue.set(state.loadingLots, artId, false);
    },
    SET_ELEMENT_PRICES(state, data) {
        data.forEach((item) => {
            state.elementPrices[item.hwmId] = item;
        });
    },
    SET_ELEMENT_AVG_PRICE(state, data) {
        state.elementPrices[data.element].avgPrice = data.price;
    },
    SET_ART_LOTS(state, payload) {
        if (!Array.isArray(payload.data)) {
            throw new Error('Data is not array');
        }

        const art = payload.data.map((item) => {
            if (item.craft) {
                // eslint-disable-next-line no-param-reassign
                item.craftObject = new Craft(item.craft, payload.meta.art.categoryId);
            }
            return item;
        });

        const meta = payload.meta ? payload.meta : {};
        // eslint-disable-next-line no-prototype-builtins
        if (!Object.prototype.hasOwnProperty.call(meta, 'lastUpdatedAt')) {
            meta.lastUpdatedAt = dayjs();
        } else {
            meta.lastUpdatedAt = dayjs(meta.lastUpdatedAt);
        }

        Vue.set(state.artLots, payload.artId, art);
        Vue.set(state.artLotsMeta, payload.artId, meta);
    },
    DISABLE_LOAD_ARTS_FROM_CACHE(state) {
        state.loadArtsFromCache = false;
    },
};

export default {
    namespaced: true,
    state: st,
    getters,
    actions,
    mutations,
};
