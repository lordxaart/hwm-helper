<?php

declare(strict_types=1);

namespace App\HWM\Exceptions;

use App\Exceptions\LimitExceededError;

class RequestLimitExceededError extends LimitExceededError
{
    //
}
