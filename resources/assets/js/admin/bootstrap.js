// Fontawesome icons
import { library, dom } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { faSword } from 'JS/icons/faSword';
import Vue from 'vue';

window._ = require('lodash');

// import axios
const axios = require('axios');

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
const token = document.head.querySelector('meta[name="csrf-token"]');
if (token) {
    axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.warn('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

const apiToken = document.querySelector('meta[name=api_token]').content;
axios.defaults.headers.common.Authorization = `Bearer ${apiToken}`;

global.axios = axios;

// We are only using the user-astronaut icon
library.add(fas, fab, faSword);
// Replace any existing <i> tags with <svg> and set up a MutationObserver to
// continue doing this as the DOM changes.
dom.watch();

Vue.component('font-awesome-icon', FontAwesomeIcon);
