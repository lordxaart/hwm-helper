<?php

use App\Models\Lot;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PartitionLotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $table = Lot::getModel()->getTable();
        $yearColumn = 'started_at_year';

        // add year column
        if (!Schema::hasColumn($table, $yearColumn)) {
            Schema::table($table, function (Blueprint $table) use ($yearColumn) {
                $table->unsignedSmallInteger($yearColumn)->after('ended_at');
            });

            // change primary key for partition
            DB::unprepared("ALTER TABLE {$table} DROP PRIMARY KEY, ADD PRIMARY KEY (`id`, `{$yearColumn}`) USING BTREE;");
            if (!isTestEnv()) {
                try {
                    DB::unprepared("
                        ALTER TABLE ${table} DROP INDEX `market_lots_lot_id_unique`,
                        ADD UNIQUE `market_lots_lot_id_unique_index` (`lot_id`, `{$yearColumn}`) USING BTREE;
                    ");
                } catch (\Exception $e) {
                    DB::unprepared("
                        ALTER TABLE ${table} ADD UNIQUE `market_lots_lot_id_unique_index` (`lot_id`, `{$yearColumn}`) USING BTREE;
                    ");
                }
            }
        }

        // create trigger for auto fill year
        $this->createTrigger($table, $yearColumn);

        // explode lots table on partitions by YEAR
        // TO 2031
        $years = range(2007, 2031);

        $sql = "ALTER TABLE $table PARTITION BY RANGE ({$yearColumn})(" . PHP_EOL;
        foreach ($years as $year) {
            $yearPrev = $year + 1;
            $sql .= "PARTITION p{$year} VALUES LESS THAN ({$yearPrev}) ," . PHP_EOL;
        }

        $sql = substr($sql, 0, -3) . PHP_EOL;

        $sql .= ');';

        if (!isTestEnv()) {
            \Illuminate\Support\Facades\DB::statement($sql);
        }
    }

    protected function createTrigger(string $table, string $column)
    {
        try {
            DB::unprepared("
                CREATE TRIGGER auto_insert_year_lot_from_started_at BEFORE INSERT ON `{$table}` FOR EACH ROW
                    BEGIN
                        SET NEW.{$column} = YEAR(NEW.started_at);
                    END
                ");
            DB::unprepared("
                CREATE TRIGGER auto_update_year_lot_from_started_at BEFORE UPDATE ON `{$table}` FOR EACH ROW
                    BEGIN
                        SET NEW.{$column} = YEAR(OLD.started_at);
                    END
                ");
        } catch (Throwable $e) {
            //skip if trigger already exists
            if ($e instanceof \Illuminate\Database\QueryException && $e->getCode() == 'HY000') {
                return;
            }

            throw $e;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
