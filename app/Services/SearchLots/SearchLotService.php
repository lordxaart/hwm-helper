<?php

declare(strict_types=1);

namespace App\Services\SearchLots;

use App\Services\SearchLots\Repositories\LotAggregationsRepository;
use App\Services\SearchLots\Repositories\LotRepository;
use App\Services\SearchLots\ValueObjects\Aggregation\Aggregation;
use App\Services\SearchLots\ValueObjects\Aggregation\AggregationsByEntityRequest;
use App\Services\SearchLots\ValueObjects\Aggregation\AggregationType;
use App\Services\SearchLots\ValueObjects\Aggregation\AggregationResponse;
use App\Services\SearchLots\ValueObjects\LimitOffset;
use App\Services\SearchLots\ValueObjects\LotFilters;
use App\Services\SearchLots\ValueObjects\OrderBy;
use App\Services\SearchLots\ValueObjects\Response;

class SearchLotService
{
    private LotRepository&LotAggregationsRepository $repository;

    public function __construct(LotRepository&LotAggregationsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function searchLotsByFilter(LotFilters $filters, LimitOffset $limitOffset, OrderBy $orderBy): Response
    {
        return $this->repository->findByFilters($filters, $limitOffset, $orderBy);
    }

    public function getAggregationsByEntity(AggregationsByEntityRequest $request): array
    {
        $metrics = [];
        foreach ($request->metrics as $metric) {
            $metrics[] = new Aggregation(
                $metric->field . '_' . $metric->type,
                $metric,
            );
        }

        $aggregationDateRange = new Aggregation(
            'started_at_date_histogram',
            new AggregationType(
                'date_histogram',
                'started_at',
                [
                    'calendar_interval' => $request->interval,
                    'format' => $request->interval === '1h' ? 'yyyy-MM-dd HH' : 'yyyy-MM-dd',
                ],
            ),
            $metrics
        );

        $aggregation = new Aggregation(
            'entity_id_terms',
            new AggregationType('terms', 'entity_id', ['size' => 100]),
            [$aggregationDateRange]
        );

        $filters = new LotFilters(array_filter([
            'entity_type' => $request->entityType->value,
            'entity_id' => $request->entityId,
            'started_at' => implode(',', $request->dateRange->toArrayFormatted(DATE_SHORT_FORMAT)),
        ]));

        $response = $this->repository->getAggregations([$aggregation], $filters);

        $result = [];

        // todo: add parser for this response
        foreach ($response->toArray()['entity_id_terms'] as $entityHwmId => $entity) {
            $metrics = [];

            foreach ($entity['started_at_date_histogram'] as $date => $values) {
                $metrics[] = array_merge($values, ['date' => $date]);
            }

            $result[$entityHwmId] = $metrics;
        }

        return $result;
    }
}
