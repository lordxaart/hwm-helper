<?php

declare(strict_types=1);

namespace App\Events\ArtMonitor;

use App\Models\Art;
use App\Models\User;
use Illuminate\Foundation\Events\Dispatchable;

class DeleteMonitoredArtEvent
{
    use Dispatchable;

    public function __construct(public readonly User $user, public readonly Art $art)
    {
    }
}
