<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateRequestSpeedLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_speed_logs', function (Blueprint $table) {
            if (Schema::hasColumn('request_speed_logs', 'request')) {
                $table->dropColumn('request');
            }
            if (Schema::hasColumn('request_speed_logs', 'response')) {
                $table->dropColumn('response');
            }

            $table->json('headers')->nullable()->after('code');
            $table->string('proxy')->nullable()->after('headers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_speed_logs', function (Blueprint $table) {
            $table->dropColumn('headers');
            $table->dropColumn('proxy');
        });
    }
}
