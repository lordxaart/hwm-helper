<?php

declare(strict_types=1);

namespace App\Models;

use App\Exceptions\ServerError;
use App\Exceptions\Social\EmailScopeRequired;
use App\Exceptions\Social\InvalidState;
use Illuminate\Auth\Events\Registered;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Socialite\Two\InvalidStateException;
use Laravel\Socialite\Two\ProviderInterface;

/**
 * Class SocialAccount
 * @package App\Models
 * @property User $user
 */
class SocialAccount extends Model
{
    use SoftDeletes;

    protected $casts = ['deleted_at' => 'datetime'];

    protected $fillable = ['user_id', 'provider_id', 'provider'];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param string $social
     * @return self
     * @throws EmailScopeRequired
     * @throws InvalidState
     * @throws ServerError
     */
    public static function firstOrCreateCustom(string $social): self
    {
        try {
            /** @var ProviderInterface $provider */
            $provider = \Socialite::driver($social);
            $providerUser = $provider->user();
        } catch (InvalidStateException) {
            throw new InvalidState($social);
        } catch (\Exception $e) {
            throw new ServerError($e->getMessage(), $e->getCode(), $e);
        }

        if (!$providerUser->getEmail()) {
            throw new EmailScopeRequired($social);
        }

        $account = self::where('provider', $social)
            ->where('provider_id', $providerUser->getId())
            ->with('user')
            ->first();

        if (!$account) {
            $account = new SocialAccount();
            $account->provider_id = $providerUser->getId();
            $account->provider = $social;
            $user = User::where('email', $providerUser->getEmail())->withTrashed()->first();
            if (!$user) {
                /** @var User $user */
                $user = User::create([
                    'email' => $providerUser->getEmail(),
                ]);
                event(new Registered($user));
            }

            $account->user()->associate($user);
            $account->save();

            //
            if (!$user->hasVerifiedEmail()) {
                $user->markEmailAsVerified();
            }
        }

        return $account;
    }

    public function isNew(): bool
    {
        return $this->created_at === $this->freshTimestamp();
    }
}
