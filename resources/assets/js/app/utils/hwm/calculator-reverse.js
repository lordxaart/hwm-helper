import has from 'lodash/has';
import hwmMaster from './masters';
import hwmCalculator from './calculator';

// helper function
const getResult = (art, price, master) => {
    const { currentStrength, baseStrength, repair } = art;

    // need for base result
    const calculator = hwmCalculator({
        price,
        currentStrength,
        baseStrength,
        repair,
    }, master);

    return {
        pricePerFight: calculator[0].pricePerFight,
        optimalPrice: price,
        fights: calculator[0].fights,
        price: calculator[0].price,
        strength: calculator[0].strength,
        masterRate: master.pRate,
        masterRepair: master.pRepair,
    };
};

const calculatorReverse = (art, masters) => {
    const results = [];

    if (!Array.isArray(masters)) {
        masters = [masters];
    }

    if (!hwmMaster.checkMaster(masters)) {
        return results;
    }

    if (!has(art, 'pricePerFight')
        || !has(art, 'currentStrength')
        || !has(art, 'baseStrength')
        || !has(art, 'repair')) {
        console.error('Missed required property', art);
        return results;
    }

    masters.forEach((master) => {
        let basePricePerFight = Number.parseFloat(art.pricePerFight);
        let currentOptimalPrice;
        let prevOptimalPrice;
        let currentPricePerFight;
        let prevPricePerFight;
        let result;

        // Main logics
        do {
            prevOptimalPrice = currentOptimalPrice || null;
            currentOptimalPrice = currentOptimalPrice ? currentOptimalPrice * 2 : 1;

            // calculate price per fight
            result = getResult(art, currentOptimalPrice, master);
            prevPricePerFight = currentPricePerFight || null;
            currentPricePerFight = result.pricePerFight;
        } while (currentPricePerFight <= basePricePerFight);

        if (currentOptimalPrice > 1) {
            let prevPrice = prevOptimalPrice;
            let nextPrice = currentOptimalPrice;
            let differencePrice = Math.round((nextPrice - prevPrice) / 2);
            let currentPrice = prevPrice + differencePrice;
            let nextPricePerFight = currentPricePerFight;

            currentPricePerFight = getResult(art, currentPrice, master).pricePerFight;

            while (differencePrice !== 1) {
                if (prevPricePerFight === basePricePerFight) {
                    currentPrice = prevPrice;
                    break;
                }
                if (nextPricePerFight === basePricePerFight) {
                    currentPrice = nextPrice;
                    break;
                }
                if (currentPricePerFight === basePricePerFight) {
                    break;
                }

                if (currentPricePerFight > basePricePerFight) {
                    nextPrice = currentPrice;
                    nextPricePerFight = currentPricePerFight;
                } else {
                    prevPrice = currentPrice;
                    prevPricePerFight = currentPricePerFight;
                }

                differencePrice = Math.round((nextPrice - prevPrice) / 2);
                currentPrice = prevPrice + differencePrice;

                currentPricePerFight = getResult(art, currentPrice, master).pricePerFight;
            }

            if (currentPricePerFight !== basePricePerFight) {
                basePricePerFight += 0.0049;

                do {
                    prevPricePerFight = currentPricePerFight;
                    prevPrice = currentPrice;
                    currentPrice += 1;
                    currentPricePerFight = getResult(art, currentPrice, master).pricePerFight;
                } while (currentPricePerFight <= basePricePerFight);

                currentPrice = prevPrice;
            }

            result = getResult(art, currentPrice, master);
        }

        results.push(result);
    });

    return results;
};

export default calculatorReverse;
