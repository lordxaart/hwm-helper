<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Components\LocalizationDetect;
use App\Models\Objects\UserSettings;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class Localization
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        if (\Crawler::isCrawler()) {
            return $next($request);
        }


        /** @var LocalizationDetect $detect */
        $detect = app(LocalizationDetect::class);

        // check if locale set in cookie
        $locale = $detect->getLocaleFromCookie();

        if (user() && user()->settings(UserSettings::LANG) !== $locale) {
            $locale = user()->settings(UserSettings::LANG);
        }

        if (!$locale && config('localization.useAcceptLanguageHeader')) {
            $locale = $detect->detectLocaleFromHttpHeader();
        }

        // get locale from ip
        if (!$locale && config('localization.useIpForDetectLocale')) {
            $locale = $detect->detectLocaleFromIp();
        }

        if (!$locale) {
            $locale = config('app.locale');
        }

        if (!Cookie::has(LocalizationDetect::COOKIE_KEY)) {
            // save locale in cookie
            Cookie::queue(Cookie::make(LocalizationDetect::COOKIE_KEY, $locale, now()->diffInMinutes(now()->addYear()), null, null, null, false));
        }

        if (app()->getLocale() !== $locale) {
            app()->setLocale($locale);
        }

        return $next($request);
    }
}
