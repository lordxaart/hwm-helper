<?php

declare(strict_types=1);

namespace App\HWM\Entities\Interfaces;

use App\HWM\Enums\EntityType;

interface CanBeLot
{
    public function getEntityType(): EntityType;
    public function getMarketCategoryId(): string;
    public function getImage(bool $realPath = false, string $size = null): string;
}
