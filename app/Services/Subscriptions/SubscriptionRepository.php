<?php

declare(strict_types=1);

namespace App\Services\Subscriptions;

use App\Core\ValueObject\UserId;
use App\Services\Subscriptions\Entities\Plan;
use App\Services\Subscriptions\Entities\Sub;
use Illuminate\Database\DatabaseManager;

class SubscriptionRepository
{
    public const TABLE_SUBSCRIPTIONS = 'subscriptions';
    public const TABLE_PLANS = 'subscription_plans';
    public const TABLE_USER_SUBSCRIPTIONS = 'user_subscriptions';

    public function __construct(private DatabaseManager $db)
    {
    }

    /**
     * @return Sub[]
     */
    public function getSubscriptions(): array
    {
        return $this->db->table(self::TABLE_SUBSCRIPTIONS)->get()
            ->map(fn($row) => $this->mapRowToSubscription((array)$row))
            ->toArray();
    }

    public function getSubscriptionByAlias(string $alias): ?Sub
    {
        $row = $this->db->table(self::TABLE_SUBSCRIPTIONS)->where('alias', $alias)->first();

        if (!$row) {
            return null;
        }

        return $this->mapRowToSubscription((array)$row);
    }

    public function getSubscription(int $id): ?Sub
    {
        $row = $this->db->table(self::TABLE_SUBSCRIPTIONS)->where('id', $id)->first();

        if (!$row) {
            return null;
        }

        return $this->mapRowToSubscription((array)$row);
    }

    /**
     * @return Plan[]
     */
    public function getPlansBySubscription(?int $subId = null, ?string $subAlias = null): array
    {
        $query = $this->db->table(self::TABLE_PLANS);

        if ($subId) {
            $query->where('subscription_id', $subId);
        }

        if ($subAlias) {
            $sub = $this->db->table(self::TABLE_SUBSCRIPTIONS)->where('alias', $subAlias)->first('id');

            if (!$sub) {
                return [];
            }

            $query->where('subscription_id', $sub->id);
        }

        return $query->get()->map(fn($row) => $this->mapRowToPlan((array)$row))->toArray();
    }

    public function getPlan(int $id): ?Plan
    {
        $row = $this->db->table(self::TABLE_PLANS)->where('id', $id)->first();

        if (!$row) {
            return null;
        }

        return $this->mapRowToPlan((array)$row);
    }

    public function getUserSubscriptions(
        UserId $userId,
        int $subId = null,
        bool $active = false,
        int $limit = 10
    ): array {
        $query = $this->db->table(self::TABLE_USER_SUBSCRIPTIONS)->where('user_id', $userId->value);

        if ($subId) {
            $query->where('subscription_id', $subId);
        }

        if ($active) {
            $query->where('ended_at', '>=', date('Y-m-d H:i:s'));
        }

        $rows = $query->orderBy('ended_at', 'DESC')->take($limit)->get();

        return $rows->map(fn($row) => (array)$row)->toArray();
    }

    public function getActiveSubscriptionIds(UserId $userId): array
    {
        return $this->db->table(self::TABLE_USER_SUBSCRIPTIONS)
            ->where('user_id', $userId->value)
            ->where('ended_at', '>=', date('Y-m-d H:i:s'))
            ->pluck('subscription_id')
            ->unique()
            ->toArray();
    }

    public function getUserSubscriptionById(int $subId): ?array
    {
        $row = $this->db->table(self::TABLE_USER_SUBSCRIPTIONS)->where('id', $subId)->first();
        return $row ? (array)$row : null;
    }

    public function createUserSubscription(
        UserId $userId,
        int $subId,
        int $planId,
        \DateTimeInterface $start,
        \DateTimeInterface $end,
        \DateTimeInterface $trial = null
    ): void {
        $this->db->table(self::TABLE_USER_SUBSCRIPTIONS)->insert([
            [
                'user_id' => $userId->value,
                'subscription_id' => $subId,
                'subscription_plan_id' => $planId,
                'started_at' => $start->format(DATE_FORMAT),
                'trial_ended_at' => $trial?->format(DATE_FORMAT),
                'ended_at' => $end->format(DATE_FORMAT),
            ]
        ]);
    }

    public function updateUserSubscription(int $subId, \DateTimeInterface $end, \DateTimeInterface $trial = null): bool
    {
        return !!$this->db
            ->table(self::TABLE_USER_SUBSCRIPTIONS)
            ->where('id', $subId)
            ->update([
                'trial_ended_at' => $trial?->format(DATE_FORMAT),
                'ended_at' => $end->format(DATE_FORMAT),
            ]);
    }

    public function deleteUserSubscription(int $userSubscriptionId): bool
    {
        return !!$this->db->table(self::TABLE_USER_SUBSCRIPTIONS)->delete($userSubscriptionId);
    }

    private function mapRowToPlan(array $row): Plan
    {
        return new Plan(
            id: $row['id'],
            subscription_id: $row['subscription_id'],
            alias: $row['alias'],
            name: $row['name'],
            duration: $row['duration'],
            trial: $row['trial'],
            price: (int)$row['price'],
            currency: $row['currency']
        );
    }

    private function mapRowToSubscription(array $row): Sub
    {
        return new Sub(
            id: $row['id'],
            alias: $row['alias'],
            name: $row['name'],
            description: $row['description'],
        );
    }
}
