<?php

declare(strict_types=1);

namespace App\Console\Commands\HWM\Lot;

use App\Console\Commands\Command;
use App\Jobs\Lot\LotUpdate;
use App\Models\Lot;
use App\Models\LotOperation;
use Exception;

class ParseMissOperations extends Command
{
    private const FETCHING_STEP = 1000;

    public $signature = 'hwm:lot:parse_missed_operations';
    public $description = 'Collect and parse operations';

    /**
     * @throws Exception
     */
    public function handle(): void
    {
        $this->info('Start');

        // get lot id from last 3 month
        $from = now()->subDays(3);
        $startedAt = $from->format(DATE_FORMAT);

        $max = Lot::where('started_at', '<=', $startedAt)->orderBy('lot_id', 'desc')->first()?->lot_id;
        $min = 1;

        if (!$max) {
            throw new Exception('Not found max lot id from ' . $from);
        }

        for ($i = $max; $i > $min; $i -= self::FETCHING_STEP) {
            $start = $i - self::FETCHING_STEP + 1;
            $end = $i;

            if ($start < $min) {
                $start = $min;
            }

            $this->info("Fetching from $start to $end");
            $lotIds = Lot::whereBetween('lot_id', [$start, $end])
                ->pluck('lot_id')
                ->toArray();

            $operations = LotOperation::whereIn('lot_id', $lotIds)
                ->where('type', \App\HWM\Objects\LotOperation::OPERATION_END)
                ->pluck('lot_id')
                ->toArray();

            $missedIds = array_diff($lotIds, $operations);

            if (empty($missedIds)) {
                continue;
            }

            $this->info(sprintf('Found %s missed operations', count($missedIds)));

            foreach ($missedIds as $lotId) {
                dispatch(new LotUpdate((int) $lotId));
            }
        }
    }
}
