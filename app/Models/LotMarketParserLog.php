<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LotMarketParserLog extends Model
{
    /** @var string  */
    protected $dateFormat = 'Y-m-d H:i:s.u';

    /** @var array  */
    protected $fillable = [
        'entity_type',
        'entity_id',
        'lots_count',
        'new_lots_count',
        'attempts',
        'errors',
        'time_execute',
    ];
}
