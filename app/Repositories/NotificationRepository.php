<?php

declare(strict_types=1);

namespace App\Repositories;

use App\ValueObjects\Notification\Channel  as ChannelEntity;
use App\Services\Notifications\Enums\Channel;
use App\Exceptions\InvalidArgumentException;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Support\Collection;

class NotificationRepository
{
    private Collection $channels;

    private Translator $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;

        $this->initChannels();
    }

    /**
     * @return ChannelEntity[]
     */
    public function getAllChannels(): array
    {
        return $this->channels->toArray();
    }

    public function getChannelByAlias(string $alias): ChannelEntity
    {
        if (Channel::tryFrom($alias) === null) {
            throw new InvalidArgumentException('alias', $alias);
        }

        return $this->channels->filter(function (ChannelEntity $channel) use ($alias) {
            return $channel->getAlias() === $alias;
        })->first();
    }

    public function getChannelAliases(): array
    {
        return $this->channels->map(function (ChannelEntity $channel) {
            return $channel->getAlias();
        })->toArray();
    }

    private function initChannels(): void
    {
        $this->channels = collect([]);

        foreach (Channel::cases() as $channel) {
            $this->channels->push(new ChannelEntity(
                $channel,
                $this->translator->get('app.notifications.channels.' . $channel->value),
            ));
        }
    }
}
