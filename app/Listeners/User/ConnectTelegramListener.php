<?php

declare(strict_types=1);

namespace App\Listeners\User;

use App\Events\User\ConnectedTelegramEvent as Event;
use App\Jobs\AdminNotify;
use App\Notifications\Admin\Notify;
use App\Notifications\User\TelegramNotify;
use App\Services\ActivityLogger\ActivityLogger;
use App\Services\ActivityLogger\ValueObjects\Activity;

class ConnectTelegramListener
{
    public function __construct(private readonly ActivityLogger $activityLogger)
    {
    }

    public function handle(Event $event): void
    {
        $this->activityLogger->log(
            Activity::action('USER_TELEGRAM_CONNECT')
                ->causedBy($event->user)
                ->context([
                    'telegramAccount' => $event->telegramUser->toArray(),
                ])
        );

        $event->user->notify(
            new TelegramNotify(
                trans(
                    'app.user.success_connect_telegram_bot',
                    [],
                    $event->user->preferredLocale()
                ) . ': ' . $event->user->email
            ),
        );

        AdminNotify::dispatch(new Notify(trans('User ' . $event->user->email . ' connected Telegram bot')));
    }
}
