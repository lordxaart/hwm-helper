<?php

declare(strict_types=1);

namespace App\Services\Notifications\Channels\Webpush;

use App\Core\ValueObject\UserId;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $endpoint
 * @property int $user_id
 * @property string|null $public_key
 * @property string|null $auth_token
 * @property string|null $content_encoding
 */
class PushSubscriptionModel extends Model
{
    public const TABLE = 'webpush_subscription';

    protected $table = self::TABLE;

    protected $fillable = [
        'user_id',
        'endpoint',
        'public_key',
        'auth_token',
        'content_encoding',
    ];

    public static function findByEndpoint($endpoint): ?static
    {
        return static::where('endpoint', $endpoint)->first();
    }

    public function isUserOwnSubscription(UserId $userId): bool
    {
        return $this->user_id === $userId->value;
    }

    public function toArray()
    {
        return [
            'user_id' => $this->user_id,
            'endpoint' => $this->endpoint,
        ];
    }
}
