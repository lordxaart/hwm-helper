<?php

declare(strict_types=1);

namespace App\Services\Notifications\Channels\Telegram\Enums;

enum Emoji: string
{
    case FIRE = "\u{1F525}";
    case FIRECRACKER = "\u{1F9E8}";
    case COLLISION = "\u{1F4A5}";
    case WARNING = "\u{26A0}";
    case BELL = "\u{1F514}";
    case LOUDSPEAKER = "\u{1F4E2}";
    case NEW_BUTTON = "\u{1F195}";
    case OK_HAND = "\u{1F44C}";
    case INFORMATION = "\u{2139}";
}
