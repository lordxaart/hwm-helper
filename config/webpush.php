<?php

declare(strict_types=1);

return [

    /**
     * These are the keys for authentication (VAPID).
     * These keys must be safely stored and should not change.
     */
    'vapid' => [
        'subject' => env('VAPID_SUBJECT', ''),
        'public_key' => env('VAPID_PUBLIC_KEY', ''),
        'private_key' => env('VAPID_PRIVATE_KEY', ''),
        'pem_file' => env('VAPID_PEM_FILE', ''),
    ],

    /**
     * This is model that will be used to for push subscriptions.
     */
    'model' => \NotificationChannels\WebPush\PushSubscription::class,

    'table_name' => 'webpush_subscription',

    /**
     * The Guzzle client options used by Minishlink\WebPush.
     */
    'client_options' => [],
];