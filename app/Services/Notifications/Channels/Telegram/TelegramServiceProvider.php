<?php

declare(strict_types=1);

namespace App\Services\Notifications\Channels\Telegram;

use App\Services\Notifications\Enums\Channel;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Notifications\ChannelManager;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\ServiceProvider;

/**
 * @copy from https://github.com/laravel-notification-channels/telegram
 */
class TelegramServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     */
    public function register(): void
    {
        $this->app->bind(Telegram::class, static fn () => new Telegram(
            config('services.telegram-bot-api.token'),
            app(HttpClient::class),
            config('services.telegram-bot-api.base_uri')
        ));

        Notification::resolved(static function (ChannelManager $service) {
            $service->extend(Channel::TELEGRAM->getLaravelChannelName(), static fn ($app) => $app->make(TelegramChannel::class));
        });
    }
}
