import config from 'JS/app/config';

export const sanitazeInt = (value, defValue = 0) => {
    let val = value;
    val = Number.parseInt(val, 10);
    if (Number.isNaN(val)) {
        val = defValue;
    }

    return val;
};

export const sanitazeFloat = (value, defValue = 0) => {
    let val = value;
    val = Number.parseFloat(val);
    if (Number.isNaN(val)) {
        val = defValue;
    }

    return val;
};

export const addStyleToDocument = (src) => {
    const linkTag = document.createElement('link');

    linkTag.setAttribute('rel', 'stylesheet');
    linkTag.setAttribute('type', 'text/css');
    linkTag.setAttribute('href', src);

    linkTag.onload = () => {};

    document.getElementsByTagName('head')[0].appendChild(linkTag);
};

// From stackowerflow
export const addParamsToQuery = (key, value, uri = null) => {
    if (!uri) {
        // eslint-disable-next-line no-param-reassign
        uri = window.location.search;
    }

    const re = new RegExp(`([?&])${key}=.*?(&|$)`, 'i');
    const separator = uri.indexOf('?') !== -1 ? '&' : '?';
    let query;

    if (uri.match(re)) {
        query = uri.replace(re, `$1${key}=${value}$2`);
    } else {
        query = `${uri + separator + key}=${value}`;
    }

    window.history.pushState({}, '', query);
};

export const errorLoadArtImg = (event) => {
    // eslint-disable-next-line no-param-reassign
    if (event.target.src !== config.app.default_img_url) {
        event.target.src = config.app.default_img_url;
    }
};

export const objectToQueryString = (obj) => Object.keys(obj).reduce((str, key, i) => {
    const delimiter = (i === 0) ? '?' : '&';
    return [str, delimiter, key, '=', obj[key]].join('');
}, '');

export const parseIncludes = (data) => {
    const includes = [];

    if (data && Array.isArray(data)) {
        data.forEach((item) => {
            includes.push(item);
        });
    }

    return includes.length ? includes.join(',') : '';
};

export default {
    sanitazeFloat, sanitazeInt, addParamsToQuery, addStyleToDocument, errorLoadArtImg, objectToQueryString, parseIncludes,
};
