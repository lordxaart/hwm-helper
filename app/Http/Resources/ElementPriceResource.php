<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Http\Resources\Core\ApiResource;
use Illuminate\Http\Request;

/**
 * @property \App\Models\ElementPrice $resource
 */
class ElementPriceResource extends ApiResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return $this->transformByRequest([
            'hwmId' => $this->element,
            'title' => trans('hwm.elements.' . $this->element),
            'minPrice' => $this->min_price,
            'maxPrice' => $this->max_price,
            'avgPrice' => $this->avg_price,
            'countLots' => $this->count_lots,
            'createdAt' => $this->created_at?->format(config('app.date_format')),
        ], $request);
    }
}
