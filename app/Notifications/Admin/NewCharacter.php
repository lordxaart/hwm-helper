<?php

declare(strict_types=1);

namespace App\Notifications\Admin;

use App\Core\Enums\Queue;
use App\Services\Notifications\Channels\Telegram\Entities\TelegramMessage;
use App\Services\Notifications\Enums\Channel;
use App\Models\Character;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class NewCharacter extends Notification implements ShouldQueue
{
    use Queueable;

    public Character $character;

    public function __construct(Character $character)
    {
        $this->queue = Queue::NOTIFICATIONS->value;

        $this->character = $character;
    }

    public function via(): array
    {
        return ['database', Channel::TELEGRAM->getLaravelChannelName()];
    }

    public function toTelegram(): TelegramMessage
    {
        $content = $this->character->user ? $this->character->user->email . ' ' : '';
        $content .= "Successfully added *character* [{$this->character->login}]({$this->character->getHwmLink()})";

        return TelegramMessage::create()
            ->setTypeContent('new')
            ->content($content);
    }

    public function toArray(): array
    {
        return [
            'character' => $this->character->only(['hwm_id', 'login', 'user_id', 'status']),
        ];
    }
}
