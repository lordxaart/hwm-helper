<?php

use App\HWM\Enums\EntityType;
use App\HWM\Enums\LotType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use App\HWM\Entities\BaseLot;

class AddEnumColumnsInLotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::getColumnType('lots', 'lot_type') !== 'enum') {
            $types = implode('\', \'', enumToArrayOfValues(LotType::cases()));
            DB::statement("ALTER TABLE lots MODIFY COLUMN lot_type ENUM('$types')");
        };


        if (Schema::getColumnType('lots', 'entity_type') !== 'enum') {
            $types = implode(
                '\', \'',
                enumToArrayOfValues(EntityType::cases()),
            );
            DB::statement("ALTER TABLE lots MODIFY COLUMN entity_type ENUM('$types')");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
