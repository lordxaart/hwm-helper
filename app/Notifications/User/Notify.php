<?php

declare(strict_types=1);

namespace App\Notifications\User;

use App\Core\Enums\Queue;
use App\Services\Notifications\Channels\Telegram\Entities\TelegramMessage;
use App\Services\Notifications\Channels\Webpush\WebPushMessage;
use App\Services\Notifications\Enums\Channel;
use App\Services\Notifications\Enums\NotifyType;
use App\Services\Notifications\UserNotifyService;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class Notify extends Notification implements ShouldQueue
{
    use Queueable;

    public string $message;

    public string $type;

    public function __construct(string $message, string $type = 'notice')
    {
        $this->queue = Queue::NOTIFICATIONS->value;

        $this->message = $message;

        $this->type = $type;
    }

    public function via(User $user): array
    {
        return [
            ...app(UserNotifyService::class)
                ->getConfiguredChannels($user, NotifyType::DEFAULT),
            Channel::DATABASE
        ];
    }

    public function toMail(): MailMessage
    {
        return (new MailMessage())
            ->subject($this->message)
            ->line($this->message);
    }

    public function toTelegram(): TelegramMessage
    {
        return TelegramMessage::create()
            ->setTypeContent($this->type)
            ->content($this->message);
    }

    public function toWebPush(): WebPushMessage
    {
        return (new WebPushMessage())
            ->title('Notify [' . $this->type .']')
            ->body($this->message);
    }

    public function toArray(): array
    {
        return [
            'message' => $this->message,
            'type' => $this->type,
        ];
    }
}
