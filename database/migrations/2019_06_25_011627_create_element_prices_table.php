<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElementPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('element_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('element', config('hwm.rules.max_hwm_id_length'))->index(); // element hwm id
            $table->unsignedInteger('avg_price')->default(0);
            $table->unsignedInteger('min_price')->default(0);
            $table->unsignedInteger('max_price')->default(0);
            $table->unsignedInteger('count_lots');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('element_prices');
    }
}
