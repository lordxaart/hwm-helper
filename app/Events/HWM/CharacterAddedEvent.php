<?php

declare(strict_types=1);

namespace App\Events\HWM;

use App\Models\Character;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CharacterAddedEvent
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    public Character $character;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Character $character)
    {
        $this->character = $character;
    }
}
