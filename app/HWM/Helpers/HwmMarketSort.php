<?php

declare(strict_types=1);

namespace App\HWM\Helpers;

enum HwmMarketSort: int
{
    case NEWEST = 2;
    case OLDEST = 1;
    case RICHEST = 3;
    case CHEAPEST = 4;
}
