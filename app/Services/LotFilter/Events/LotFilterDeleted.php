<?php

declare(strict_types=1);

namespace App\Services\LotFilter\Events;

use App\Core\ValueObject\UserId;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class LotFilterDeleted
{
    use Dispatchable;
    use SerializesModels;

    public function __construct(
        public int $lotFilterId,
        public UserId $userId,
    ) {
        //
    }
}
