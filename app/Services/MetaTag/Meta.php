<?php

declare(strict_types=1);

namespace App\Services\MetaTag;

use Butschster\Head\Contracts\MetaTags\MetaInterface;
use Butschster\Head\MetaTags\Entities\Favicon;
use Butschster\Head\Packages\Entities\OpenGraphPackage;
use Butschster\Head\Packages\Entities\TwitterCardPackage;
use Butschster\Head\Packages\Manager;
use Illuminate\Contracts\Config\Repository;

class Meta extends \Butschster\Head\MetaTags\Meta implements MetaInterface
{
    public OpenGraphPackage $og;

    public TwitterCardPackage $twitter;

    public function __construct(Manager $packageManager, Repository $config = null)
    {
        parent::__construct($packageManager, $config);

        $this->og = new OpenGraphPackage('open_graph');
        $this->twitter = new TwitterCardPackage('twitter');
    }

    public function initialize(): static
    {
        parent::initialize();

        if (!count($favicons = $this->config('favicons'))) {
            $this->setFavicons($favicons);
        }

        if (!empty($type = $this->config('og.type'))) {
            $this->og->setType($type);
        }

        if (!empty($name = $this->config('og.site_name'))) {
            $this->og->setSiteName($name);
        }

        if (!empty($type = $this->config('twitter.card'))) {
            $this->twitter->setType($type);
        }

        if (!empty($domain = $this->config('twitter.domain'))) {
            $this->twitter->addMeta('domain', $domain);
        }

        return $this;
    }

    /**
     * @param string $url
     *
     * @return \Butschster\Head\MetaTags\Meta
     */
    public function setImage(string $url): self
    {
        $this->og->addImage($url);
        $this->twitter->setImage($url);
        $this->twitter->addMeta('image:src', $url);

        return $this->addMeta('image', [
            'content' => $url,
        ]);
    }

    public function setCanonical(string $url): \Butschster\Head\MetaTags\Meta
    {
        $this->og->setUrl($url);

        return parent::setCanonical($url);
    }

    public function setTitle(?string $title, int $maxLength = null): Meta
    {
        $this->og->setTitle($title ?: '');
        $this->twitter->setTitle($title ?: '');

        return parent::setTitle($title, $maxLength);
    }

    public function addToTitle(string $title): Meta
    {
        /** @phpstan-ignore-next-line  */
        $title = $title . ' ' . $this->config('title.separator') . ' ' . $this->getTitle();

        return $this->setTitle($title);
    }

    public function setDescription(?string $description, ?int $maxLength = null): Meta|\Butschster\Head\MetaTags\Meta
    {
        $this->og->setDescription($description ?: '');
        $this->twitter->setDescription($description ?: '');

        if (is_null($maxLength)) {
            $maxLength = $this->config('description.max_length');
        }

        return parent::setDescription($description, $maxLength);
    }

    /**
     * @param string $keywords
     */
    public function setKeywords($keywords, ?int $maxLength = null): self
    {
        $this->twitter->addMeta('keywords', $keywords);

        $this->addMeta('keywords', [
            'content' => $keywords,
            'data-vmid' => 'keywords',
        ]);

        return $this;
    }

    public function setUserApiToken(string $token): \Butschster\Head\MetaTags\Meta
    {
        return $this->addMeta('api-token', [
            'content' => $token,
        ]);
    }

    public function setFavicons(array $favicons = []): static
    {
        foreach ($favicons as $key => $favicon) {
            $href = $favicon['href'];
            unset($favicon['href']);

            $this->addTag('favicon.' . $key, new Favicon($href, array_filter($favicon) ?: []));
        }

        return $this;
    }

    public function toHtml(): string
    {
        $this->registerPackage($this->og);
        $this->registerPackage($this->twitter);

        return $this->head()->toHtml();
    }
}
