<?php

declare(strict_types=1);

namespace App\Models;

use App\HWM\Entities\Interfaces\HwmEntityInterface;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

/**
 * Class BaseCategory
 * @package App\Models
 * @property string|int $hwm_id
 * @property string $title
 */
abstract class BaseCategory extends Model implements HwmEntityInterface
{
    use CrudTrait;
    use HasTranslations;

    public array $translatable = ['title'];

    public function getHwmId(): string
    {
        return (string)$this->hwm_id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public static function findByHwmId(string $id = null): ?static
    {
        return $id ? static::where('hwm_id', $id)->first() : null;
    }

    public function getKeyName(): string
    {
        return 'hwm_id';
    }

    public function getKeyType(): string
    {
        return 'string';
    }

    protected function asJson(mixed $value): bool|string
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }
}
