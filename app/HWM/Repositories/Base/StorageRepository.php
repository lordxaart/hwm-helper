<?php

declare(strict_types=1);

namespace App\HWM\Repositories\Base;

use App\HWM\Entities\Interfaces\HwmEntityInterface;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use JsonException;

abstract class StorageRepository extends EntityRepository
{
    protected function getStoragePath(): string
    {
        return hwm_storage_path($this->getStorageKey() . '.json');
    }

    protected function getStorageKey(): string
    {
        return Str::plural(Str::snake($this->getEntityAsString()));
    }

    /**
     * Map data to HwmEntities Collection
     * @throws FileNotFoundException
     * @throws JsonException
     */
    protected function loadDataFromRepository(): Collection
    {
        $storagePath = $this->getStoragePath();
        if (!file_exists($storagePath)) {
            throw new FileNotFoundException('File ' . $storagePath . ' not found');
        }

        $json = file_get_contents($storagePath) ?: '';
        $data = json_decode($json, true);

        if (!$data) {
            throw new JsonException('Invalid json from ' . $this->getStorageKey());
        }

        return $this->mapData($data);
    }

    protected function mapData(array $data): Collection
    {
        return $this->mapDataFromArray($data);
    }

    /**
     * Map data from simple array: [value1, value2, value3]
     */
    protected function mapDataFromArray(array $data): Collection
    {
        return collect($data)->map(function ($hwmId) {
            $title = trans('hwm.' . $this->getStorageKey() . '.' . $hwmId, [], $this->locale);
            return $this->createElement($hwmId, $title);
        });
    }

    protected function createElement(string $hwmId, string $title = null): HwmEntityInterface
    {
        $class = $this->getEntityClass();

        return new $class($hwmId, $title ? ['title' => $title] : []);
    }

    protected function getElementInstance(): HwmEntityInterface
    {
        return $this->createElement(UNDEFINED, UNDEFINED);
    }
}
