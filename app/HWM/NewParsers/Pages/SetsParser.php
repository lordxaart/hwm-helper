<?php

declare(strict_types=1);

namespace App\HWM\NewParsers\Pages;

use App\HWM\Exceptions\ParserErrors\UnexpectedPage;
use App\HWM\NewParsers\ParserException;

class SetsParser extends AbstractPageParser
{
    public const SETS_BLOCK = '.help_right_block .help_container_right .help_text_inside';

    protected array $texts = [
        RU => [
            'text_from_set_page' => 'Комплект выдается дополнительным бонусом игрокам первого и второго уровней',
        ],
        EN => [
            'text_from_set_page' => 'The set provides additional bonuses to Hero of first and second level',
        ],
    ];

    public function handle(): mixed
    {
        $setsBlock = $this->htmlNode->find(self::SETS_BLOCK);

        if (!$setsBlock) {
            throw new UnexpectedPage('sets');
        }

        $sets = explode('<a name=', $this->htmlNode->asHtml());
        unset($sets[0]);
        unset($sets[1]);
        $sets = array_map(function ($item) {
            return '<a name=' . $item;
        }, $sets);

        $parsedSets = [];

        foreach ($sets as $set) {
            try {
                $setId = getTextBetweenValues($set, '<a name="', '"');

                if ($setId === 'undgr_set' && array_key_exists('undgr_set', $parsedSets)) {
                    $setId  = 'razb_set';
                }

                $regex = '/^(.+)<font\scolor=\'green\'>(.+)<\/font><\/strong>(.+)$/';
                $setName = strip_tags(preg_replace($regex, '${2}', $set) ?: '');

                if ($setId === 'forest_set') {
                    $setName = 'Комплект леса';
                }

                $regex = '/^(.+)<\/font><\/strong><br\s\/>\s(.+)\s<br\s\/><br\s\/>\s<table\sclass="wb"(.+)$/';
                $description = trim(strip_tags(preg_replace($regex, '${2}', $set) ?: ''));
                $arts = [];
                if ($pos = mb_strpos($set, '<td class="tbl-ati_brd-all tbl-sts_bg-light" valign="top">')) {
                    $set = mb_substr($set, $pos);
                }
                preg_match_all('/<a\shref=\"\/?art_info.php\?id=.*?>.*?<img.*?\/>.*?<\/a>/i', $set, $links);
                foreach ($links[0] as $item) {
                    $artId = getTextBetweenValues($item, 'art_info.php?id=', '"');
                    if (str_contains($item, 'title=\'')) {
                        $artName = getTextBetweenValues($item, 'title=\'', '\'');
                    } else {
                        $artName = getTextBetweenValues($item, 'title="', '"');
                    }

                    $artImage = str_replace('//', '', getTextBetweenValues($item, 'src="', '"', true, true));

                    if ($artId) {
                        $arts[$artId] = [
                            'art_id' => $artId,
                            'art_name' => $artName,
                            'art_image' => $artImage,
                        ];
                    }
                }
            } catch (\Exception $e) {
                throw new ParserException($e->getMessage(), $e);
            }

            $parsedSets[$setId] = [
                'hwm_id' => $setId,
                'title' => $setName,
                'description' => $description,
                'arts' => array_values($arts),
            ];
        }

        $parsedSets = array_values($parsedSets);

        $this->validator->validateEntity('sets', $parsedSets);

        return $parsedSets;
    }
}
