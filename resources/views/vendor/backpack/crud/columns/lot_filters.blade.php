@php
    $entity = $column['entity'] ?? 'filters';
@endphp
All: <strong>{{ $entry->$entity->count() }}</strong> | Active: <strong>{{ $entry->$entity->where('is_active')->count() }}</strong> |  Inactive: <strong>{{ $entry->$entity->where('is_active', 0)->count() }}</strong>
<ul class="list-group">
@foreach($entry->$entity as $filter)
    @php
        /** @var \App\Models\LotFilter $filter */
    @endphp
    <li class="list-group-item mb-0">
        <span>{{ !$filter->is_active ? 'inactive' : '' }}</span>
        <strong>[{{ $filter->lot_type->value }}]</strong>
        <strong>[{{ $filter->entity_type->value }}]</strong>
        <strong>[{{ $filter->entity_id }}]</strong>
        <code>{{ json_encode($filter->terms->toArrayOfValues()) }}</code>
    </li>
@endforeach
</ul>
