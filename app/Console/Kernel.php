<?php

declare(strict_types=1);

namespace App\Console;

use App\Models\Lot;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Symfony\Component\Finder\Finder;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var string[]
     */
    protected $commands = [
        //
    ];

    protected string $startBackgroundTime = '02:00';

    protected Carbon $startTime;

    protected string $output;

    public function __construct(Application $app, Dispatcher $events)
    {
        parent::__construct($app, $events);

        $this->startTime = Carbon::parse($this->startBackgroundTime);
        // TODO move this logics to logger or another place
        $this->output = storage_path('logs/cron-' . date('Y-m-d') . '.log');
        @chmod($this->output, 777);
    }

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Set default locale as english
        app()->setLocale(EN);

        // set cron check file
        $schedule->call(function () {
            Cache::set('cron_check', date('Y-m-d H:i:s'));
        })->everyMinute();

        // skip for local env
        if (isLocalEnv()) {
            return;
        }

        // Update db art list from sets and market
        $schedule
            ->command('hwm:art:update:from_market')
            ->everyFiveMinutes()
            ->runInBackground();

        // Update elements
        $schedule
            ->command('hwm:elements:parse_price')
            ->cron('*/10 */4 * * *'); // every 4 hours 10 minutes

        // collect missed lots
        $schedule->command('hwm:lot:collect_missed_lots')
            ->everyFifteenMinutes()
            ->runInBackground();

        // check parser lots status and es indexing
        $schedule->command('hwm:parser:check-status')
            ->hourly()
            ->runInBackground();

        // Update Unparsed Lots
        $schedule
            ->command('hwm:lots:update:unparsed')
            ->dailyAt($this->getCronTime())
            ->runInBackground();

        // Logs clear
        $schedule
            ->command('logs:clear')
            ->dailyAt($this->getCronTime())
            ->runInBackground();

        $schedule
            ->command('horizon:snapshot')
            ->everyFiveMinutes();

        // Run work bot
        //$schedule->command('hwm:work')->everyMinute()->runInBackground();

        // Sitemap
        $schedule
            ->command('generate:sitemap')
            ->dailyAt($this->getCronTime())
            ->appendOutputTo($this->output)
            ->runInBackground();

        $schedule
            ->command('hwm:set:update')
            ->dailyAt($this->getCronTime())
            ->appendOutputTo($this->output)
            ->runInBackground();

        $schedule
            ->command('hwm:art:translate all')
            ->dailyAt($this->getCronTime())
            ->appendOutputTo($this->output)
            ->runInBackground();

        // Reindex last lots
        $schedule
            ->command('es:reindex:lots', ['--from' => Lot::getLastWeekMinLotId()])
            ->dailyAt($this->getCronTime())
            ->appendOutputTo($this->output)
            ->runInBackground();

        // Backup
        $params = !isProdEnv() ? ['--disable-notifications'] : [];
        $schedule
            ->command('backup:clean', $params)
            ->dailyAt($this->getCronTime());
        $schedule
            ->command('backup:run', $params)
            ->dailyAt($this->getCronTime(60))
            ->runInBackground();

        $schedule
            ->command('notify:subscription:expire')
            ->dailyAt('09:00');

        $schedule
            ->command('arts-filters:clear:limits', ['--soft'])
            ->dailyAt('10:00');
    }

    protected function getCronTime(int $delayMinutes = 5): string
    {
        $startTime = $this->startTime->format('H:i');
        $this->startTime->addMinutes($delayMinutes);

        return $startTime;
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     * @throws \ReflectionException
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        // load actions commands
        $this->loadActionsCommand();
    }

    /**
     * @throws \ReflectionException
     */
    protected function loadActionsCommand(): void
    {
        $paths = [app_path('/Actions')];
        $namespace = $this->app->getNamespace();

        foreach ((new Finder())->in($paths)->files() as $command) {
            /** @phpstan-var class-string $class */
            $class = $namespace . str_replace(
                ['/', '.php'],
                ['\\', ''],
                Str::after(strval($command->getRealPath()), realpath(app_path()) . DIRECTORY_SEPARATOR)
            );

            $reflection = new \ReflectionClass($class);

            if (
                $reflection->hasProperty('commandSignature')
                && $reflection->hasMethod('asCommand')
            ) {
                \Illuminate\Console\Application::starting(function ($artisan) use ($class) {
                    $artisan->resolve($class);
                });
            }
        }
    }
}
