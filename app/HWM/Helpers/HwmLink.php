<?php

declare(strict_types=1);

namespace App\HWM\Helpers;

use App\HWM\Entities\Certificate;
use App\HWM\Entities\House;
use App\HWM\Entities\ObjectShare;
use App\HWM\Entities\Res;
use Illuminate\Support\Arr;
use InvalidArgumentException;

class HwmLink
{
    public const LOGIN = 'login.php';
    public const HOME = 'home.php';
    public const CHARACTER_INFO = 'pl_info.php';
    public const ART_INFO = 'art_info.php';
    public const LOT_INFO = 'auction_lot_protocol.php';
    public const ECOSTAT_DETAILS = 'ecostat_details.php'; // r
    public const MARKET = 'auction.php';
    public const ACTION_BUY = 'auction_buy_now.php';
    public const HELP = 'help.php';
    public const SETS = 'help.php?section=40';
    public const SEARCH_CHARACTER = 'search.php';
    public const OBJECT_INFO = 'object-info.php';
    public const HOUSE_INFO = 'house_info.php';

    public const BASE_LINK = 'https://my.lordswm.com';
    public const BASE_LINK_EN = 'https://www.lordswm.com';
    public const PARSER_LINK_EN = 'https://www.heroeswm.ru';

    public const AVAILABLE_LINKS = [
        'https://my.lordswm.com',
        'https://www.heroeswm.ru',
        'https://www.lordswm.com',
    ];

    public static function get(string $link, array $params = [], string $locale = null): string
    {
        if (!str_starts_with('http', $link)) {
            $link = (($locale && $locale === 'en') ? self::BASE_LINK_EN : self::BASE_LINK).'/'.ltrim($link, '/');
        }

        if ($link === self::MARKET && !empty($params['art_type'])) {
            $params['art_type'] = 'part_' . $params['art_type'];
        }

        $params = self::filterParams($link, $params);

        if (count($params)) {
            $link .= '?'.http_build_query($params);
        }

        return $link;
    }

    public static function filterParams(string $link, array $params = []): array
    {
        $required = [
            self::ART_INFO => ['id'],
            self::LOT_INFO => ['id'],
            self::OBJECT_INFO => ['id'],
            self::HOUSE_INFO => ['id'],
            self::CHARACTER_INFO => ['id'],
            self::ECOSTAT_DETAILS => ['r'],
        ];

        $allowed = array_merge($required, [
            self::MARKET => [
                'cat', // category
                'art_type', // entity id
                'sort', // 0 - cheapest
                'sbn', // Only sold
                'sau', // Only bargaining
                'snew', // Only whole
            ],
            self::ART_INFO => ['uid', 'crc'],
        ]);

        if (array_key_exists($link, $required)) {
            foreach ($required[$link] as $param) {
                if (!array_key_exists($param, $params)) {
                    throw new InvalidArgumentException("Missing required param '$param' for link '$link'");
                }
            }
        }

        if (array_key_exists($link, $allowed)) {
            $params = Arr::only($params, $allowed[$link]);
        }

        if (isset($params['art_type']) && isset($params['cat'])) {
            if ($params['cat'] === Res::MARKET_CATEGORY_ID) {
                $params['type'] = self::mappedResources()[$params['art_type']] ?? 0;
                unset($params['art_type']);
            }

            if ($params['cat'] === Certificate::MARKET_CATEGORY_ID) {
                $params['art_type'] = self::mappedCerts()[$params['art_type']] ?? 0;
            }

            if ($params['cat'] === House::MARKET_CATEGORY_ID || $params['cat'] === ObjectShare::MARKET_CATEGORY_ID) {
                unset($params['art_type']);
            }
        }

        return $params;
    }

    public static function mappedResources(): array
    {
        return [
            'wood' => 1,
            'ore' => 2,
            'mercury' => 3,
            'sulfur' => 4,
            'crystal' => 5,
            'gem' => 6,
        ];
    }

    public static function mappedCerts(): array
    {
        return [
            'empire-capital' => 'sec_01',
            'east-river' => 'sec_02',
            'tiger-lake' => 'sec_03',
            'rogues-wood' => 'sec_04',
            'wolf-dale' => 'sec_05',
            'peaceful-camp' => 'sec_06',
            'lizard-lowland' => 'sec_07',
            'green-wood' => 'sec_08',
            'eagle-nest' => 'sec_09',
            'portal-ruins' => 'sec_10',
            'dragons-caves' => 'sec_11',
            'shining-spring' => 'sec_12',
            'sunny-city' => 'sec_13',
            'magma-mines' => 'sec_14',
            'bear-mountain' => 'sec_15',
            'fairy-trees' => 'sec_16',
            'harbour-city' => 'sec_17',
            'mithril-coast' => 'sec_18',
            'great-wall' => 'sec_19',
            'titans-valley' => 'sec_20',
            'fishing-village' => 'sec_21',
            'kingdom-castle' => 'sec_22',
            'ungovernable-steppe' => 'sec_23',
            'crystal-garden' => 'sec_24',
            0 => 'sec_25',
            'the-wilderness' => 'sec_26',
            'sublime-arbor' => 'sec_27',
        ];
    }
}
