<?php
return [
    "calculator" => [
        "desc" => "Calculator of the optimal price per battle :art, calculating the optimal scrapping for artifacts, finding the optimal price, calculating the cost of crafting",
        "title" => "Optimal artifact breakage calculator"
    ],
    "contacts" => ["desc" => "Administrator contacts, feedback form"],
    "default" => [
        "desc" => "A set of services for trading in the game Heroes of War and Money heroeswm.ru - calculator for optimal scrapping of artifacts, price market, monitoring artifacts with notifications of new lots, archive of all lots",
        "keys" => "Heroes of war and money, wholesale scrapping, optimal scrapping, scrapping calculator, heroeswm, market, monitoring, calculator, services, heroeswm.ru, heroes, heroeswm, heroes"
    ],
    "index" => ["title" => "Online services for trading in the HWM game"],
    "login" => [
        "desc" => "Login or register on the website hwmh.space. Login with social networks: google, vkontakte, facebook. Recover password",
        "title" => "Enter to site"
    ],
    "market" => [
        "desc" => "Artifact market, search for lots for artifact :art",
        "title" => "Artifact Market"
    ],
    "monitoring" => [
        "desc" => "Monitoring lots by artifact, auto-updating the list of lots, setting notifications with filters",
        "title" => "Artifact monitoring"
    ],
    "privacy" => ["desc" => "WEBSITE PRIVACY POLICY"],
    "register" => [
        "desc" => "Register or log in to the hwmh.space website. Sign up using social networks: google, vkontakte, facebook",
        "title" => "Register on the site"
    ],
    "stat" => [
        "desc" => "Archive of all lots, 110,000,000 lots since 2007, search for lots by filters",
        "title" => "Archive of all lots"
    ],
    "terms" => ["desc" => "Site Terms of Use"]
];
