import Vue from 'vue';
import { BootstrapVue } from 'bootstrap-vue';
import VTooltip from 'v-tooltip';
import SelectLocale from 'JS/app/components/global/SelectLocale.vue';
import statuspage from './Components/StatusPage.vue';

window.Vue = Vue;

Vue.use(BootstrapVue);
Vue.use(VTooltip);

// eslint-disable-next-line no-new
new Vue({
    el: '.app-body',
    components: {
        statuspage,
        SelectLocale,
    },
});

new Vue({
    el: '.app-header',
    components: {
        statuspage,
        SelectLocale,
    },
});
