<?php

namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Seeder;

abstract class FactorySeeder extends Seeder
{
    protected $faker;
    protected array $staticData = [];

    public function __construct()
    {
        $this->faker = Factory::create();

        $this->initStaticData();
    }

    abstract public function initStaticData();
    abstract public function runChunk(): int;

    final public function run()
    {
        for ($i = 1; $i <= $this->getCountChunks(); $i++) {
            $count = $this->runChunk();
            $this->command->info(static::class . ' success insert ' . $count . ' records!');
        }
    }

    public function getCountChunks(): int
    {
        return 1;
    }
}
