<?php

declare(strict_types=1);

namespace App\Listeners\User;

use App\Jobs\AdminNotify;
use App\Models\SocialAccount;
use App\Models\User;
use App\Notifications\Admin\Notify;
use App\Notifications\User\Register;
use App\Services\ActivityLogger\ActivityLogger;
use App\Services\ActivityLogger\ValueObjects\Activity;
use Illuminate\Auth\Events\Registered as Event;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisteredListener implements ShouldQueue
{
    public function __construct(private readonly ActivityLogger $activityLogger)
    {
    }

    public function handle(Event $event): void
    {
        /** @var User|null $user */
        $user = $event->user;

        $this->activityLogger->log(
            Activity::action('USER_REGISTER')
                ->causedBy($user)
                ->context([
                    'provider' => $user?->profiles()?->first()?->provider ?: 'email',
                ])
        );

        if (!$user) {
            return;
        }

        if (!$user->hasVerifiedEmail()) {
            $user->sendEmailVerificationNotification();
        } else {
            $user->notify(new Register());
        }

        // if this user is super admin
        if ($user->isSuperAdmin()) {
            return;
        }

        $this->notifyForAdmin($user);
    }

    public function notifyForAdmin(User $user): void
    {
        $message = trans('Success Registered') . " - $user->email.";

        if ($profile = $user->profiles()->first()) {
            /** @var SocialAccount $profile */
            $message .= " " . trans('With social network') . " - $profile->provider ($profile->provider_id)";
        }

        AdminNotify::dispatchSync(new Notify($message));
    }
}
