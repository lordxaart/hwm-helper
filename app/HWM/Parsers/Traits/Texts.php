<?php

declare(strict_types=1);

namespace App\HWM\Parsers\Traits;

use Illuminate\Support\Facades\Lang;

trait Texts
{
    protected function getText(string $key): string
    {
        if (!Lang::hasForLocale('hwm.parser.' . $key, $this->locale)) {
            throw new \InvalidArgumentException("Invalid key '$key' for text [$this->locale]");
        }

        return trans('hwm.parser.' . $key, [], $this->locale);
    }
}
