<?php

declare(strict_types=1);

namespace App\Services\LotFilter\ValueObjects;

use App\Core\ValueObject\UserId;
use App\HWM\Enums\EntityType;
use App\HWM\Enums\LotType;

class LotFilterRequest
{
    public function __construct(
        public readonly UserId $userId,
        public readonly LotType $lotType,
        public readonly EntityType $entityType,
        public readonly ?string $entityId,
        public readonly FilterTermCollection $terms,
        public readonly bool $isActive = true,
    ) {
        //
    }
}
