<?php

declare(strict_types=1);

use App\HWM\HwmClient;
use App\Models\User;
use App\Services\Settings\GeneralSettings;
use Illuminate\Support\HtmlString;
use App\Services\UserFeatures\UserFeatureService;

function assetEmail(string $path): string
{
    if (str_starts_with($path, '/')) {
        $path = substr($path, 0, 1);
    }

    return isLocalEnv() ? 'https://hwmh.space/' . $path : asset($path);
}

function isAssocArray(array $arr): bool
{
    if ([] === $arr) {
        return false;
    }

    return array_keys($arr) !== range(0, count($arr) - 1);
}

// Cut a string between two values in the text
function getTextBetweenValues(
    string $text,
    string $startText,
    string $endText,
    bool $stripTags = true,
    bool $fromEnd = false
): string {
    $strPos = !$fromEnd ? mb_strpos($text, $startText) : mb_strrpos($text, $startText);
    $startPos = $strPos + mb_strlen($startText);
    $strPos2 = mb_strpos($text, $endText, $startPos);
    $cutText = trim(mb_substr($text, $startPos, $strPos2 - $startPos));

    if ($stripTags) {
        $cutText = strip_tags($cutText);
    }

    return $cutText;
}

// Cut text but regex
function getTextByRegex(string $text, string $regex): ?string
{
    preg_match($regex, $text, $result);

    return $result[0] ?? null;
}

function getDefaultTimezone(): string
{
    return config('app.timezone');
}

function formatBytes(int $bytes, int $precision = 2): string
{
    $units = ["b", "kb", "mb", "gb", "tb"];

    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);

    $bytes /= (1 << (10 * $pow));

    return round($bytes, $precision) . " " . $units[$pow];
}

function hwm_storage_path(string $path = ''): string
{
    if ($path && !str_starts_with($path, '/')) {
        $path = '/' . $path;
    }

    return resource_path('assets/hwm' . $path);
}

function checkRemoteImageExists(string $url): bool
{
    $ch = curl_init($url);
    if (!$ch) {
        return false;
    }
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    return $code == 200;
}

function randomChance(int $chance): bool
{
    return mt_rand(1, 100) <= $chance;
}

function getClassShortName(object $class): string
{
    return (new ReflectionClass($class))->getShortName();
}

function hwmClient(bool $withCredentials = true): HwmClient
{
    return $withCredentials ? app('HwmDefaultClient') : app(HwmClient::class);
}

function user(): ?User
{
    return \Illuminate\Support\Facades\Auth::user();
}

function getUserIp(): ?string
{
    return Request::ip();
}

function icon(string $name): string
{
    return config('icons.' . $name) ?: config('icons.default');
}

function settings(): GeneralSettings
{
    return app(GeneralSettings::class);
}

/**
 * @throws Exception
 */
function newMix(string $path, string $manifestDirectory = ''): HtmlString|string
{
    try {
        return mix($path, $manifestDirectory);
    } catch (Throwable) {
        try {
            return asset($path);
        } catch (Throwable) {
            return '';
        }
    }
}

function validateDateFormat(string $date, string $format): bool
{
    $d = DateTime::createFromFormat($format, $date);

    return $d && $d->format($format) === $date;
}

function objectHasTrait(object $obj, string $trait): bool
{
    return in_array(
        $trait,
        class_uses_recursive($obj)
    );
}

function getServerIp(): ?string
{
    return Cache::rememberForever('SERVER_IP', function () {
        try {
            return file_get_contents('http://ifconfig.me') ?: null;
        } catch (Throwable) {
            return null;
        }
    });
}

function containsRussian(string $text): bool
{
    return boolval(preg_match('/[А-Яа-яЁё]/u', $text));
}

// Save content to tmp directory. Enable detect auto ext for images
function saveTmpFile(string $content, bool $autoExt = true): ?string
{
    $tmpFileName = tempnam("/tmp", uniqid());
    if (!$tmpFileName) {
        return null;
    }

    $handle = fopen($tmpFileName, "w");
    if (!$handle) {
        return null;
    }

    fwrite($handle, $content);
    fclose($handle);

    if ($autoExt) {
        // map image types
        $types = [
            IMAGETYPE_GIF => '.gif',
            IMAGETYPE_PNG => '.png',
            IMAGETYPE_JPEG => '.jpg',
        ];
        $info = getimagesize($tmpFileName) ?: [];
        $type = $info[2] ?? null;
        // if detect need type then rename tmp file with ext and return path with ext
        if (array_key_exists($type, $types)) {
            $newName = $tmpFileName . $types[$type];
            rename($tmpFileName, $newName);
            $tmpFileName = $newName;
        }
    }

    return $tmpFileName;
}

function getLastVersion(): string
{
    return 'v1.1';
}

function enumToArrayOfValues(array $enums): array
{
    return array_map(function ($enum) {
        if (property_exists($enum, 'value')) {
            return $enum->value;
        }

        return null;
    }, $enums);
}


function features(): UserFeatureService
{
    return app(UserFeatureService::class);
}
