<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnsInMarketLotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('market_lots', function (Blueprint $table) {
            $table->renameColumn('art_type', 'entity_type');
            $table->renameColumn('art_name', 'entity_title');
            $table->renameColumn('art_id', 'entity_id');
            $table->renameColumn('start_quantity', 'quantity');
            $table->dropColumn('current_quantity');
            $table->dropColumn('current_price');
            $table->renameColumn('start_price', 'price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('market_lots', function (Blueprint $table) {
            //
        });
    }
}
