<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Models\User;
use App\Components\Validators\Rules\OldPasswordConfirmed;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePassword extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $this->redirect = url()->previous() . '#password';

        return [
            'old_password' => ['nullable', new OldPasswordConfirmed()],
            'password' => User::$createRules['password']
        ];
    }
}
