<?php

declare(strict_types=1);

namespace App\Notifications\Admin;

use App\Core\Enums\Queue;
use App\Services\Notifications\Channels\Telegram\Entities\TelegramFile;
use App\Services\Notifications\Enums\Channel;
use App\Models\Art;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class NewArt extends Notification implements ShouldQueue
{
    use Queueable;

    public Art $art;

    public function __construct(Art $art)
    {
        $this->queue = Queue::NOTIFICATIONS->value;

        $this->art = $art;
    }

    public function via(): array
    {
        return ['database', Channel::TELEGRAM->getLaravelChannelName()];
    }

    public function toTelegram(): TelegramFile
    {
        $content = "Successfully added *art* [{$this->art->getTitle()}({$this->art->hwm_id})]({$this->art->getLink()})";

        return TelegramFile::create()
            ->setTypeContent('new')
            ->content($content)
            ->photo($this->art->getImageUrl());
    }

    public function toArray(): array
    {
        return [
            'art' => $this->art->only(['hwm_id', 'title', 'original_image', 'category_id', 'market_category_id', 'set_id']),
        ];
    }
}
