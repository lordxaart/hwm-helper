<?php

declare(strict_types=1);

namespace App\Services\Parser\Exceptions;

use Exception;

class NotFoundMissedLots extends Exception
{
}
