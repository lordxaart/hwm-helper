<?php

declare(strict_types=1);

namespace App\Services\Parser;

use App\Models\EmptyLot;
use App\Services\Parser\Storages\StorageForMissedLots;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @property LotRunner $currentRunner
 */
class NewMissedLotsParser extends AbstractParser
{
    protected ?StorageForMissedLots $storage;
    protected array $missedIds = [];

    public function __construct(OutputInterface $output = null)
    {
        $this->storage = new StorageForMissedLots();

        parent::__construct($output);
    }

    protected function setRunner(int $lotId): LotRunner
    {
        return $this->currentRunner = new LotRunner(LotRunner::TYPE_NEW, $lotId, false);
    }

    protected function getNextLotId(): int
    {
        if (!$lotId = $this->storage?->pop()) {
            return 0;
        }

        // miss not exists lots
        if (EmptyLot::where('lot_id', $lotId)->exists()) {
            $lotId = $this->getNextLotId();
        }

        return $lotId ?: 0;
    }

    protected function setNextRunner(): void
    {
        $this->setRunner($this->getNextLotId());
    }

    protected function beforeRun(): bool
    {
        /** @phpstan-ignore-next-line  */
        if (!$this->currentRunner) {
            $this->setRunner($this->getNextLotId());
        }

        if (!$this->currentRunner->getLotId()) {
            return false;
        }

        return true;
    }
}
