<?php

declare(strict_types=1);

namespace App\Exceptions\User;

use App\Exceptions\ClientError;
use App\Models\User;
use Throwable;

class UserWasBlocked extends ClientError
{
    public function __construct(User $user, Throwable $previous = null)
    {
        $time = $user->blockedUntilTo()?->format('Y-m-d H:i');

        /** @var string $message */
        $message = trans('app.notifications.change_status.your_account_was', [
            'action' => trans('app.user.actions.blocked'),
            'until_to' => $user->blockedUntilTo()
                ? ', ' . trans('app.notifications.change_status.until_to_time', ['time' => $time])
                : '',
        ]);

        // find reason and add to message

        parent::__construct(ClientError::USER_IS_BLOCKED, $message, $previous);
    }
}
