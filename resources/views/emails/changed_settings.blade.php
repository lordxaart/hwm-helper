@component('mail::message')

<h1>{{ __('app.notifications.value_for_account_changed') }}</h1>

<ul>
    @foreach($items as $item)
        <li>{!! __('app.notifications.key_changed_from_to', [
            'key' => '<strong>' . __('validation.attributes.' . $item[0]) . '</strong>',
            'old_value' => '<strong>' . $item[1] . '</strong>',
            'new_value' => '<strong>' . $item[2] . '</strong>'
        ]) !!}</li>
    @endforeach
</ul>

@endcomponent
