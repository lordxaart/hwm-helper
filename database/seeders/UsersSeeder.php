<?php

namespace Database\Seeders;

use App\Models\User;

class UsersSeeder extends FactorySeeder
{
    public function getCountChunks(): int
    {
        return 20;
    }

    public function initStaticData()
    {
        $this->staticData['password'] = \Hash::make('111111');
    }

    public function runChunk(): int
    {
        $count = 500;
        $users = [];

        for ($i = 1; $i <= $count; $i++) {
            $users[] = $this->getUser();
        }

        shuffle($users);

        \DB::table(User::getModel()->getTable())->insert($users);

        return $count;
    }

    private function getUser(array $attributes = []): array
    {
        $verified = !mt_rand(0, 9) ? $this->faker->dateTimeBetween('-2 years', 'now')->format(config('app.date_format')) : null;
        $deleted = $verified && !mt_rand(0, 9) ? $this->faker->dateTimeBetween('-1 years', 'now') : null;
        $blocked = null;
        $blockedUntil = null;

        if ($verified && !$deleted) {
            $blocked = !mt_rand(0, 10) ? $this->faker->dateTimeBetween('-1 years', 'now') : null;
            $blockedUntil = $blocked ? $this->faker->dateTimeBetween($blocked, 'now') : null;
        }

        return array_merge([
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => $verified,
            'password' => $this->staticData['password'],
            'created_at' => date(config('app.date_format')),
            'updated_at' => date(config('app.date_format')),
            'deleted_at' => $deleted,
            'blocked_at' => $blocked,
            'blocked_until_to' => $blockedUntil,
        ], $attributes);
    }
}
