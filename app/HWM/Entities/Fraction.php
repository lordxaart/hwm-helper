<?php

declare(strict_types=1);

namespace App\HWM\Entities;

use App\HWM\Helpers\HwmHelper;
use App\HWM\Entities\Interfaces\HasImage;
use App\HWM\Entities\Interfaces\HasRepository;
use App\HWM\Repositories\FractionRepository;

class Fraction extends HwmEntity implements HasImage, HasRepository
{
    public function getImage(bool $realPath = false): string
    {
        $path = 'images/icons/hwm/fractions/' . $this->getHwmId() . '.png';

        return !$realPath ? asset($path) : public_path($path);
    }

    public function getRepository(): FractionRepository
    {
        return app(FractionRepository::class);
    }

    public static function expToNextLevel(int $currentExp): float
    {
        $expTable = HwmHelper::getJson('exp_tables')['fractions'] ?? [];

        foreach ($expTable as $exp) {
            if ($exp > $currentExp) {
                return round($exp - $currentExp, 2);
            }
        }

        return 0;
    }
}
