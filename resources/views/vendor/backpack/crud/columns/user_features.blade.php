@php
    function showFeature($value) {
        $type = gettype($value);

        switch ($type) {
            case 'boolean':
                return $value ? 'true' : 'false';
            default:
                return $value;
        }
    }
@endphp

<ul class="list-group">
    @foreach($entry->features()->toArray() as $key => $value)
        <li class="list-group-item d-flex justify-content-between">
            <span>{{ ucwords(str_replace('_', ' ', $key)) }}:</span>
            <span>{{ showFeature($value) }}</span>
        </li>
    @endforeach
</ul>
