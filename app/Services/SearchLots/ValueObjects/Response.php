<?php

declare(strict_types=1);

namespace App\Services\SearchLots\ValueObjects;

use App\HWM\Entities\Collections\LotCollection;

class Response
{
    private LotCollection $data;
    private int $limit;
    private int $offset;
    private int $total;
    private array $extra;

    public function __construct(LotCollection $data, int $limit, int $offset, int $total, array $extra = [])
    {
        $this->data = $data;
        $this->limit = $limit;
        $this->offset = $offset;
        $this->total = $total;

        $this->extra = array_merge([
            'elastic_exec' => 0,
            'mysql_exec' => 0,
        ], $extra);
    }

    public function getData(): LotCollection
    {
        return $this->data;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * @return array ['elastic_exec' => float, 'mysql_exec' => float]
     */
    public function getExtra(): array
    {
        return $this->extra;
    }
}
