@php
    /**
     * @var \App\Models\LotFilter[] $filters
     * @var \App\HWM\Entities\BaseLot $lot
     * @var \App\Models\User $user
     * @var \App\Services\Settings\User\GameLinkDecorator $linkDecorator
     */
@endphp

@component('mail::message')

<h1 class="text-center">{{ __('app.notifications.found_new_lot') }}</h1>

<div class="text-center mb-4"><img src="{{ $lot->getEntityObject()->getImage() }}"></div>

<p class="text-center"><a
            href="{{ $linkDecorator->replaceGameLinkForUser($lot->getLink(), $user) }}">{{ $lot->getTitle() }}</a>{{ $lot->isHasStrength() ? ' ' . $lot->current_strength . '/' . $lot->base_strength : '' }}{{ $lot->craft ? ' [' . $lot->craft . ']' : '' }} {{ $lot->quantity }}{{ __('app.main.pc') }}
</p>
<br/>

@if($lot->lot_type !== \App\HWM\Enums\LotType::SALE->value)
    <p>
        <strong>{{ mb_ucfirst(__('app.lots.filters.lot_type')) }}</strong>: {{ trans('app.lots.lot_types.' . $lot->lot_type) }}
    </p>
@endif
<p><strong>{{ mb_ucfirst(__('app.main.price')) }}</strong>: {{ $lot->price }} <img width="16"
                                                                                   src="{{ config('hwm.gold_img') }}">
</p>
@if($lot->price_per_fight)
    <p><strong>{{ mb_ucfirst(__('app.main.price_per_fight')) }}</strong>: {{ round($lot->price_per_fight, 2) }} <img
                width="16" src="{{ config('hwm.gold_img') }}"></p>
@endif

<p><strong>{{ mb_ucfirst(__('app.main.seller')) }}</strong>: <a
            href="{{ $linkDecorator->replaceGameLinkForUser($lot->getSellerProfileLink(), $user) }}">{{ $lot->seller_name }}</a>
</p>
@if ($lot->started_at)
<p>
    <strong>{{ mb_ucfirst(__('app.main.put_for_sale')) }}</strong>: {{ $lot->started_at->setTimezone(\App\HWM\Helpers\HwmHelper::HEROES_TZ) }}
    ({{ $lot->started_at->diffForHumans() }})
</p>
@endif
@if($lot->ended_at)
<p>
    <strong>{{ mb_ucfirst(__('app.main.ended')) }}</strong>: {{ $lot->ended_at->setTimezone(\App\HWM\Helpers\HwmHelper::HEROES_TZ) }}
    ({{ $lot->ended_at->diffForHumans() }})
</p>
@endif

@if(!empty($filters))
<strong>{{ __('app.main.terms_filter') }}:</strong>
<ul>
    @foreach($filters as $filter)
        @foreach($filter->terms->toArray() as $term)
            <li>{{ $term->toTransString() }}</li>
        @endforeach
    @endforeach
</ul>
@endif
@endcomponent

