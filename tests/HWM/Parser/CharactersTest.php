<?php

namespace Tests\HWM\Parser;

use App\HWM\Parsers\HwmParser;
use Tests\TestCase;

class CharactersTest extends TestCase
{
    /**
     * @test
     */
    public function test_algor_character()
    {
        $this->test_character('algor');
    }

    /**
     * @test
     */
    public function test_galager_character()
    {
        $this->test_character('galager');
    }

    protected function test_character(string $login)
    {
        $parser = new HwmParser(file_get_contents(hwm_storage_path("pages/characters/$login.html")));
        $character = $parser->getCharacterInfo($login);
        $this->assertNotEmpty($character);
    }
}
