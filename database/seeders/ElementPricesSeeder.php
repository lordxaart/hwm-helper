<?php

namespace Database\Seeders;

use App\HWM\Repositories\ElementRepository;
use App\Models\ElementPrice;

class ElementPricesSeeder extends FactorySeeder
{
    public function initStaticData()
    {
        $this->staticData['elements'] = app(ElementRepository::class)->asArrayKeys();
    }

    public function getCountChunks(): int
    {
        return 10;
    }

    public function runChunk(): int
    {
        $count = 300;
        $data = [];

        for ($i = 1; $i <= $count; $i++) {
            $created = $this->faker->dateTimeBetween('-1 year', 'now')->format(config('app.date_format'));
            foreach ($this->staticData['elements'] as $element) {
                if (mt_rand(0, 99)) {
                    $data[] = $this->getItem($element, $created);
                }
            }
        }

        \DB::table(ElementPrice::getModel()->getTable())->insert($data);

        return $count;
    }

    private function getItem(string $element, $created): array
    {
        $avg = mt_rand(200, 10000);

        return [
            'element' => $element,
            'avg_price' => $avg,
            'min_price' => mt_rand(10, $avg),
            'max_price' => mt_rand($avg, 20000),
            'count_lots' => mt_rand(1, 1000),
            'created_at' => $created,
            'updated_at' => $created,
        ];
    }
}
