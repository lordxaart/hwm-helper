<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V1;

use App\Models\User;
use App\Services\Notifications\Channels\Webpush\WebPushSubscriptionService;
use Illuminate\Http\Request;

class WebPushSubscriptionsController extends ApiController
{
    public function subscribe(Request $request, WebPushSubscriptionService $subscriptionService)
    {
        $request->validate([
            'endpoint' => 'required|string',
            'public_key' => 'nullable|string',
            'auth_token' => 'nullable|string',
            'encoding' => 'nullable|string',
        ]);

        /** @var User $user */
        $user = user();
        $subscription = $subscriptionService->updateOrCreateSubscription(
            $user->getId(),
            $request->input('endpoint'),
            $request->input('public_key'),
            $request->input('auth_token'),
            $request->input('encoding'),
        );

        return $this->response($subscription->toArray(), $subscription->wasRecentlyCreated ? 201 : 200);
    }

    public function unsubscribe(Request $request, WebPushSubscriptionService $subscriptionService)
    {
        /** @var User $user */
        $user = user();
        $subscriptionService->deleteSubscription($user->getId(), $request->input('endpoint'));

        return $this->responseNoContent();
    }
}