<?php

declare(strict_types=1);

if (!defined('IS_CLI')) {
    define('IS_CLI', str_contains(php_sapi_name() ?: '', 'cli'));
}

if (!defined('ADMIN_PREFIX')) {
    define('ADMIN_PREFIX', 'admin');
}

if (!defined('TELEGRAM')) {
    define('TELEGRAM', 'telegram');
}

if (!defined('UNDEFINED')) {
    define('UNDEFINED', 'undefined');
}

if (!defined('RU')) {
    define('RU', 'ru');
}

if (!defined('EN')) {
    define('EN', 'en');
}

if (!defined('DB_UNIQUE_DUPLICATE_ERROR_CODE')) {
    define('DB_UNIQUE_DUPLICATE_ERROR_CODE', 23000);
}

if (!defined('DATE_FORMAT')) {
    define('DATE_FORMAT', 'Y-m-d H:i:s');
}

if (!defined('DATE_SHORT_FORMAT')) {
    define('DATE_SHORT_FORMAT', 'Y-m-d');
}
