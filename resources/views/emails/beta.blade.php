@component('mail::message')
# Привет

# Запущена **обновленная** версия сайта с новым функционалом
@component('mail::button', ['url' => 'https://dev.hwmh.space/calculator?from=email&email=' . $user])
Перейти на тестовый сайт
@endcomponent

Если есть желание и время протестирвать новые функции буду очень благодарен. При обнаружении любых багов, неточностей или предложений - просьба писать администратору

Сайт новый поэтому данных пользователей с главного сайта нету, регистрацию нужно проходить сначала

Как только новая версия будет протестирована - она будет залита на основной сайт

Основные нововведения:
 - Нотификации на разные события с настройкой в личном кабинете
 - Фильтры для артефактов мониторинга с нотификациями
 - База данных все лотов с обновлением (новые лоты появляються в течении 5-30 секунд)
 - Нотификации на новые лоты

@component('mail::button', ['url' => 'https://dev.hwmh.space/calculator?from=email&email=' . $user])
Перейти на тестовый сайт
@endcomponent

@endcomponent
