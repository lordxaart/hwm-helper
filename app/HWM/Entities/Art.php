<?php

declare(strict_types=1);

namespace App\HWM\Entities;

use App\HWM\Entities\Interfaces\CanBeLot;
use App\HWM\Entities\Interfaces\HasLinkOnPage;
use App\HWM\Entities\Interfaces\HasTranslations;
use App\HWM\Enums\EntityType;
use App\HWM\Helpers\HwmLink;
use App\HWM\Repositories\ArtRepository;
use App\Models\Art as ArtModel;

/**
 * Class Art
 * @package App\HWM\Objects
 * @property ?ArtModel $model
 */
class Art extends HwmEntityWithModel implements CanBeLot, HasLinkOnPage, HasTranslations
{
    public ?string $image = null;
    public ?int $need_level = null;
    public ?int $strength = null;
    public ?int $oa = null;
    public ?int $repair = null;
    public ?string $description = null;
    public ?array $modifiers = null;
    public ?string $set_id = null;
    public ?string $set_name = null;
    public bool $from_shop = false;
    public bool $as_from_shop = false;
    public ?int $shop_price = null;

    public function getImage(bool $realPath = false, string $size = null): string
    {
        $image = $this->model?->getDefaultImage();

        if ($size === 'small') {
            $image = $this->model?->getSmallImage();
        }

        if ($realPath) {
            return $this->model && $image ? $image->getRealPath() : public_path('/images/art_default_small.png');
        }

        return $this->model && $image ? $image->getUrl() : asset('images/art_default_small.png');
    }

    public function getEntityType(): EntityType
    {
        return EntityType::ART;
    }

    public function getMarketCategoryId(): string
    {
        return $this->model && $this->model->market_category_id ? $this->model->market_category_id : UNDEFINED;
    }

    public function getHwmLink(): string
    {
        return HwmLink::get(HwmLink::ART_INFO, ['id' => $this->getHwmId()]);
    }

    public function getRepository(): ArtRepository
    {
        return app(ArtRepository::class);
    }

    public function translateAttributes(): void
    {
        if (!$this->title && $this->model) {
//            $this->title = $this->model->title;
        }

        if (!$this->description && $this->model) {
//            $this->description = $this->model->description;
        }
    }
}
