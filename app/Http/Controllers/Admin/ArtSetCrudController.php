<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

class ArtSetCrudController extends BaseCategoryCrudController
{
    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel('App\Models\ArtSet');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/art-sets');
        $this->crud->setEntityNameStrings('art-set', 'art-sets');

        parent::setup();
    }
}
