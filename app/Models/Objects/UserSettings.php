<?php

declare(strict_types=1);

namespace App\Models\Objects;

use App\HWM\Helpers\HwmLink;
use App\HWM\Helpers\HwmLinkMarket;
use App\HWM\Helpers\HwmMarketSort;
use App\Repositories\NotificationRepository;
use App\Services\Notifications\Enums\NotifyType;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Validation\ValidationException;
use JsonSerializable;

class UserSettings implements JsonSerializable, Arrayable
{
    public const NOTIFICATIONS = 'notifications';
    public const GAME_LINK = 'game_link';
    public const LANG = 'lang';
    public const GAME_SETTINGS = 'game_settings';
    public const CUSTOM_NOTIFY_DELAY = 'custom_notify_delay';

    private const DEFAULT_GAME_SETTINGS = [
        'link_market_sort' => HwmMarketSort::CHEAPEST->value,
    ];

    protected array $data = [];

    protected array $oldData = [];

    protected array $baseSettings;

    public static function parseFromJson(string|null $json): self
    {
        $data = $json ? json_decode($json, true) : [];

        return new self($data);
    }

    public function __construct(array $data)
    {
        $this->initBaseSettings();

        $this->initSettings($data);
    }

    protected function initSettings(array $data): void
    {
        $this->data = [];
        $this->oldData = [];

        foreach ($this->baseSettings as $key => $item) {
            if (array_key_exists('default', $item)) {
                $this->data[$key] = $item['default'];
            } else {
                $this->data[$key] = null;
            }
        }

        $this->update($this->sanitizeRules($data));
    }

    /**
     * @param array|string $key
     * @param mixed|null $value
     * @throws \Illuminate\Validation\ValidationException
     */
    public function set(array|string $key, mixed $value = null): void
    {
        $settings = is_array($key) ? $key : [$key => $value];

        // need for notification about Change Settings
        $this->oldData = $this->data;

        // sanitize
        $settings = $this->sanitizeRules($settings);

        // validate
        try {
            \Validator::make($settings, $this->rules())->validate();
        } catch (ValidationException $e) {
            throw new \InvalidArgumentException($e->getMessage(), $e->getCode(), $e);
        }

        // set new value for all keys in attributes
        $this->update($settings);

        $this->checkIfHasNewSettings();
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key): mixed
    {
        $value = $this->data[$key] ?? null;

        return $this->sanitizeRules([$key => $value])[$key];
    }

    // todo: delete after complete migrate settings @see MigrateUserFeaturesFromUserSettings
    public function getBase(): array
    {
        return $this->baseSettings;
    }

    public function all(): array
    {
        return $this->sanitizeRules($this->data);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->data;
    }

    public function toJson(): bool|string
    {
        return json_encode($this->jsonSerialize(), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function setNotifications(array $data = []): void
    {
        $settings = $this->get(self::NOTIFICATIONS);
        foreach ($settings as $notify => $channels) {
            if (array_key_exists($notify, $data)) {
                foreach ($channels as $channel => $value) {
                    $settings[$notify][$channel] = (bool) ($data[$notify][$channel] ?? $value);
                }
            }
        }

        $this->set(self::NOTIFICATIONS, $settings);
    }

    protected function update(array $data): void
    {
        foreach ($data as $key => $item) {
            if (array_key_exists($key, $this->data)) {
                $this->data[$key] = $item;
            }
        }
    }

    public function checkIfHasNewSettings(): ?array
    {
        // Notification if set new value some settings key
        $diff = [];
        foreach ($this->oldData as $key => $value) {
            if ($value !== $this->data[$key]) {
                $diff[$key] = $value;
            }
        }

        return count($diff) ? $diff : null;
    }

    public function getEditableKeys(): array
    {
        return collect($this->baseSettings)->filter(function ($item) {
            return $item['editable'] ?? false;
        })->keys()->toArray();
    }

    public function getAdminEditableKeys(): array
    {
        return collect($this->baseSettings)->filter(function ($item) {
            return $item['admin_editable'] ?? false;
        })->keys()->toArray();
    }

    protected function initBaseSettings(): array
    {
        /** @var NotificationRepository $repository */
        $repository = app(NotificationRepository::class);
        $channels = array_fill_keys($repository->getChannelAliases(), true);

        $notifyTypes = array_map(function (NotifyType $notifyType) {
            return $notifyType->value;
        }, NotifyType::cases());
        $notifications = array_fill_keys($notifyTypes, $channels);

        $this->baseSettings = [
            self::NOTIFICATIONS => [
                'default' => $notifications,
                'rule' => 'array',
                'type' => 'array',
                'editable' => true,
            ],
            self::GAME_LINK => [
                'default' => HwmLink::BASE_LINK,
                'rule' => 'nullable',
                'type' => 'string',
            ],
            self::LANG => [
                'default' => config('app.fallback_locale'),
                'rule' => 'string|in:' . implode(',', array_keys(config('localization.locales'))),
                'type' => 'string',
            ],
            self::GAME_SETTINGS => [
                'default' => self::DEFAULT_GAME_SETTINGS,
                'rule' => 'array',
                'type' => 'array',
                'editable' => false,
            ],
            self::CUSTOM_NOTIFY_DELAY => [
                'default' => false,
                'rule' => 'boolean',
                'type' => 'boolean',
                'editable' => false,
            ],
        ];

        return $this->baseSettings;
    }

    protected function rules(): array
    {
        return array_map(function ($item) {
            return $item['rule'] ?? '';
        }, $this->baseSettings);
    }

    protected function sanitizeRules(array $attributes): array
    {
        foreach ($attributes as $key => $value) {
            $type = isset($this->baseSettings[$key]) ? $this->baseSettings[$key]['type'] ?? null : null;

            if (in_array($type, ['int', 'integer'])) {
                $attributes[$key] = intval($value);
            }

            if (in_array($type, ['string'])) {
                $attributes[$key] = strval($value);
            }

            if (in_array($type, ['bool', 'boolean'])) {
                $attributes[$key] = boolval($value);
            }
        }

        return $attributes;
    }
}
