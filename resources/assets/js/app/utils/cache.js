import ls from 'local-storage';

const cache = {
    get: (key, defValue = null) => (ls.get(key) !== null ? ls.get(key) : defValue),
    set: (key, value) => ls.set(key, value),
    has: (key) => ls.get(key) !== null,
    flush: () => ls.clear(),
};

export default cache;
