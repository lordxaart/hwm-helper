<?php

declare(strict_types=1);

namespace App\HWM\Repositories;

use App\HWM\Entities\Res;
use App\HWM\Repositories\Base\StorageRepository;
use Illuminate\Support\Collection;

class ResourceRepository extends StorageRepository
{
    public const GOLD = 'gold';
    public const DIAMOND = 'diamond';

    public function allOnlyFromMarket(): Collection
    {
        return $this->all()->filter(function (Res $item) {
            return $item->getHwmId() !== self::GOLD && $item->getHwmId() !== self::DIAMOND;
        });
    }

    protected function getEntityClass(): string
    {
        return '\App\HWM\Entities\Res';
    }
}
