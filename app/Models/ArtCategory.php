<?php

declare(strict_types=1);

namespace App\Models;

class ArtCategory extends BaseCategory
{
    public function arts(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Art::class, 'category_id', 'hwm_id');
    }
}
