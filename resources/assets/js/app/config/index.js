import cache from './cache';

const config = {
    cache,
    app: global.APP_CONFIG,
};

export default config;
