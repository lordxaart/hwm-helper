@extends('layout.app', ['js' => false])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-4 mx-auto">
            @include('elements.notification')

            <div class="card">
                <div class="card-body">
                    <form class="form-signup" method="POST" action="{{ route('register') }}">
                        @csrf

                        <legend class="text-center mb-4">@{{ $t('app.auth.registration') }}</legend>

                        <div class="form-group">
                            <label for="email" class="col-md-4 col-form-label text-md-right sr-only">@{{ $t('validation.attributes.email') }}</label>

                            <input
                                id="email"
                                type="email"
                                class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                name="email"
                                value="{{ old('email') }}"
                                :placeholder="$t('validation.attributes.email')"
                                required
                                autocomplete="username"
                            >

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!! $errors->first('email') !!}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 col-form-label text-md-right sr-only">@{{ $t('validation.attributes.password') }}</label>

                            <input
                                id="password"
                                type="password"
                                class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                name="password"
                                :placeholder="$t('validation.attributes.password')"
                                minlength="6"
                                required
                                autocomplete="new-password"
                            >

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{!!  $errors->first('password') !!}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right sr-only">@{{ $t('validation.attributes.password_confirmation') }}</label>

                            <input
                                id="password-confirm"
                                class="form-control"
                                type="password"
                                name="password_confirmation"
                                :placeholder="$t('validation.attributes.password_confirmation')"
                                minlength="6"
                                required
                                autocomplete="off"
                            >
                        </div>

                        {{-- Google reCAPTCHA --}}
{{--                        @include('auth.recaptcha')--}}

                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary btn-block mb-2">
                                @{{ $t('app.auth.registration') }}
                            </button>
                            <a class="btn btn-sm btn-link d-block" href="/login">
                                @{{ $t('app.auth.already_has_account') }}
                            </a>
                        </div>

                        <div class="form-group mb-0">
                            @include('auth.socials', ['action' => 'register'])
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
