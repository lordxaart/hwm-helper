<?php

declare(strict_types=1);

namespace App\Services\Parser\Interfaces;

interface RunnerInterface
{
    public function run(): mixed;
    public function getExecuteTime(): float;
    public function getLastError(): ?\Throwable;
    public function getRunAttempts(): int;
}
