<?php

declare(strict_types=1);

namespace App\Components\Elastic\Enum;

enum ResponseStatus: string
{
    case CREATED = 'created';
    case UPDATED = 'updated';
    case DELETED = 'deleted';
}
