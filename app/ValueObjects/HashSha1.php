<?php

namespace App\ValueObjects;

class HashSha1
{
    private const VALUE_LENGTH = 40;

    private string $value;

    public function __construct(string $value)
    {
        if (mb_strlen($value) !== self::VALUE_LENGTH) {
            throw new \InvalidArgumentException(sprintf(
                "Length of value must be %s, mb_strlen('%s') = %s",
                self::VALUE_LENGTH,
                $value,
                mb_strlen($value),
            ));
        }

        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
