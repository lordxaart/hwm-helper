<?php

declare(strict_types=1);

namespace App\Console\Commands\HWM\Lot;

use App\Console\Commands\Command;
use App\HWM\Enums\EntityType;
use App\HWM\Enums\LotType;
use App\Jobs\Lot\LotUpdate;
use App\Models\Lot;
use Illuminate\Database\Eloquent\Collection;

/**
 * @deprecated NOT ACTUAL
 */
class UpdateActiveLots extends Command
{
    protected $signature = 'hwm:lots:update:active';

    protected $description = 'Add to lot:update queue active lots (now only sale and arts)';

    public function handle(): void
    {
        $fromStartedAt = now()->subDay()->format(DATE_FORMAT); // subHour - щоб був запас 1 година щоб зпарсити лоти

        $count = 0;

        Lot::select('lot_id')
            ->where('ended_at', '>=', $fromStartedAt)
            ->where('is_parsed', 0)
            ->where('lot_type', LotType::SALE->value)
            ->where('entity_type', EntityType::ART->value)
            ->chunk(10_000, function (Collection $lots) use (&$count) {
                /** @var Lot $lot */
                foreach ($lots as $lot) {
                    dispatch(new LotUpdate($lot->lot_id));
                    $count++;
                }
            });

        $this->log("Added $count lots");
    }
}
