<?php

declare(strict_types=1);

namespace App\Services\UserFeatures\Enums;

enum Feature: string
{
    case MAX_MONITORED_ARTS = 'max_monitored_arts';
    case MAX_MONITORED_FILTERS = 'max_monitored_filters';
    case LOT_FILTERS_NOTIFY_DELAY = 'lot_filters_notify_delay';
    case CAN_EDIT_GAME_SETTINGS = 'can_edit_game_settings';
    case IS_SHOW_ADS = 'is_show_ads';

    public function getType(): FeatureType
    {
        return match ($this) {
            self::MAX_MONITORED_ARTS => FeatureType::Number,
            self::MAX_MONITORED_FILTERS => FeatureType::Number,
            self::LOT_FILTERS_NOTIFY_DELAY => FeatureType::Number,
            self::CAN_EDIT_GAME_SETTINGS => FeatureType::Bool,
            self::IS_SHOW_ADS => FeatureType::Bool,
        };
    }

    public function getDefault(): mixed
    {
        return match ($this) {
            self::MAX_MONITORED_ARTS => 2,
            self::MAX_MONITORED_FILTERS => 1,
            self::LOT_FILTERS_NOTIFY_DELAY => 60,
            self::CAN_EDIT_GAME_SETTINGS => false,
            self::IS_SHOW_ADS => true,
        };
    }
}
