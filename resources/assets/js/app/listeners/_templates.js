import i18n from 'JS/app/utils/localization';
import capitalize from 'lodash/capitalize';
import numeral from 'numeral';

export const LotToNotifyBlock = (lot) => {
    const block = `
    <ul class="">
        <li class=""><a href="${lot.entity.hwmLink}" target="_blank"><img src="${lot.entity.image}" alt="${lot.entity.title}" width="20px" class="mr-1"></a><strong>${lot.entity.title}</strong> ${lot.currentStrength}/${lot.baseStrength} ${lot.quantity}${i18n.t('app.main.pc')} ${lot.craft ? `[${lot.craft}]` : ''} </li>
        <li class=""><strong>${i18n.t('app.art.price')}</strong>: ${lot.price} з.</li>
        <li class=""><strong>${i18n.t('app.art.price_per_fight')}</strong>: ${numeral(lot.pricePerFight).format('0,0')} з.</li>
        <li class=""><strong>${capitalize(i18n.t('app.main.seller'))}</strong>: <a href="${lot.sellerLink}" target="_blank">${lot.sellerName}</a></li>
        <li class=""><strong>${capitalize(i18n.t('app.main.ended'))}</strong>: ${lot.endedAtHuman}</li>
    </ul>
    <div class="text-center">
        <a href="${lot.marketLink}" target="_blank" class="btn btn-primary btn-sm">${i18n.t('app.main.market')}</a>
    </div>
    `;

    return block;
};

export const LotToNotifyBlockShort = (lot) => {
    const block = `
    <div class="">
        <a class="mb-1" href="${lot.entity.link}" target="_blank">
            <img src="${lot.entity.image}" alt="${lot.entity.hwmId}" title="${lot.entity.title}" width="20px" class="mr-1" @error="errorLoadArtImg">
            <strong>${lot.entity.title}</strong>
        </a> ${lot.currentStrength}/${lot.baseStrength}, ${lot.quantity}, ${i18n.t('app.main.pc')}, ${lot.craft ? `[${lot.craft}], ` : ''} ${i18n.t('app.art.price')} ${lot.price} з. <span v-tooltip="${i18n.t('app.art.price_per_fight')}">${numeral(lot.pricePerFight).format('0,0')} з.</span>, <a href="${lot.sellerLink}" target="_blank">${lot.sellerName}</a>
    </div>
    <div class="text-center mt-2">
        <a href="${lot.marketLink}" target="_blank" class="btn btn-primary btn-sm">${i18n.t('app.main.market')}</a>
    </div>
    `;

    return block;
};

export const ArtToNotifyBlockShort = (art) => {
    const block = `
    <div class="d-flex flex-column">
        <a class="mb-1" href="${art.link}" target="_blank">
            <img src="${art.images.small.fullUrl}" alt="${art.hwmId}" title="${art.title}" width="20px" class="mr-1" @error="errorLoadArtImg">
            <strong>${art.title}</strong>
        </a>
    </div>
    `;

    return block;
};

export const ChangeSettings = (items) => {
    let block = '<ul>';
    items.forEach((item) => {
        block += `<li>${i18n.t('app.notifications.key_changed_from_to', {
            Key: `<strong>${i18n.t(`validation.attributes.${item[0]}`)}</strong>`,
            old_value: `<strong>${item[1]}</strong>`,
            new_value: `<strong>${item[2]}</strong>`,
        })}</li>`;
    });

    block += '</ul>';

    return block;
};

export default {
    LotToNotifyBlock,
    LotToNotifyBlockShort,
    ArtToNotifyBlockShort,
    ChangeSettings,
};
