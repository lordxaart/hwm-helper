import VueI18n from 'vue-i18n';
import messages from 'JS/other/lang.generated';
import config from 'JS/app/config';

export default new VueI18n({
    locale: config.app.locale,
    fallbackLocale: config.app.fallback_locale,
    messages,
});
