<?php

declare(strict_types=1);

namespace App\HWM\Repositories\Base;

use App\HWM\Entities\Interfaces\HwmEntityInterface;
use App\HWM\Repositories\Interfaces\EntityRepositoryInterface;
use Cache;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use ReflectionClass;
use TypeError;

abstract class EntityRepository implements EntityRepositoryInterface
{
    public const CACHE_PREFIX = 'hwm_repository_data_';
    public const DEFAULT_TTL = 1440; // 1 day

    protected Collection $data;
    protected bool $isCachedData = false;
    protected int $cacheTtl = 0;
    protected string $locale;

    // Fetch data from repository
    abstract protected function loadDataFromRepository(): Collection;
    abstract protected function getElementInstance(): HwmEntityInterface;

    public function __construct(string $locale = null)
    {
        $this->locale = $locale ?: app()->getLocale();

        $this->initCache();

        $this->validateData();
    }

    public function all(): Collection
    {
        return $this->data;
    }

    public function findByHwmId(string $id): ?HwmEntityInterface
    {
        return $this->data->where('hwm_id', $id)->first();
    }

    public function findByTitle(string $title): ?HwmEntityInterface
    {
        return $this->data->where('title', $title)->first();
    }

    public function has(string $hwmId): bool
    {
        return (bool) $this->data->firstWhere('hwm_id', $hwmId);
    }

    public function getIdByTitle(string $title): ?string
    {
        $entity = $this->findByTitle($title);
        return $entity ? $entity->getHwmId() : null;
    }

    public function getTitleById(string $hwmId): ?string
    {
        return $this->findByHwmId($hwmId)?->getTitle();
    }

    public function asArrayKeys(): array
    {
        return $this->data->map(function (HwmEntityInterface $entity) {
            return $entity->getHwmId();
        })->toArray();
    }

    public function asAssocArray(): array
    {
        return $this->data->keyBy(function (HwmEntityInterface $item) {
            return $item->getHwmId();
        })->map(function (HwmEntityInterface $entity) {
            return $entity->getTitle();
        })->toArray();
    }

    public static function refreshCache(): void
    {
        /** @var EntityRepository $instance */
        $instance = app(static::class);
        $instance->updateCache();
    }

    protected function initCache(): void
    {
        if (Cache::has($this->getCacheKey())) {
            $this->data = $this->getDataFromCache();
            $this->isCachedData = true;
        } else {
            $this->data = $this->loadDataFromRepository();
            $this->updateCache();
            $this->isCachedData = false;
        }

        $this->cacheTtl = $this->cacheTtl ?: self::DEFAULT_TTL;
    }

    protected function getEntityClass(): string
    {
        return '\App\HWM\Entities\\' . $this->getEntityAsString();
    }

    protected function getEntityAsString(): string
    {
        // Example: MarketCategoryRepository -> MarketCategory
        return (
            str_replace(
                'Repository',
                '',
                (new ReflectionClass(static::class))->getShortName()
            )
        );
    }

    protected function getTtlForCache(): int
    {
        return $this->cacheTtl ?: self::DEFAULT_TTL;
    }

    protected function getCacheKey(): string
    {
        return self::CACHE_PREFIX . Str::snake(self::getEntityAsString()) . '_' . $this->locale;
    }

    protected function validateData(): void
    {
        $instance = $this->getElementInstance();
        $this->data->each(function ($item) use ($instance) {
            if (!$item instanceof $instance) {
                $this->clearCache();
                throw new TypeError(
                    "Each item in repository must be instance of " . get_class($instance)
                    . ', found ' . (is_object($item) ? get_class($item) : gettype($item))
                );
            }
        });
    }

    final protected function updateCache(): void
    {
        Cache::set($this->getCacheKey(), $this->data->all(), $this->getTtlForCache());
    }

    final protected function getDataFromCache(): Collection
    {
        return collect(Cache::get($this->getCacheKey(), []));
    }

    final protected function clearCache(): void
    {
        Cache::delete($this->getCacheKey());
    }
}
