<?php

declare(strict_types=1);

use Monolog\Handler\StreamHandler;

$save_days = 31;

return [

    'default' => env('LOG_CHANNEL', 'stack'),

    // Date string
    'duration_save_logs' => $save_days . ' days',

    // Max file size save (in bytes) . Save only last $bytes from every log file (start from new line)
    'max_file_size' => 500000000, // 0.5GB

    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'channels' => ['daily', 'telegram', 'sentry'],
        ],

        'local_stack' => [
            'driver' => 'stack',
            'channels' => ['daily', 'telegram'],
        ],

        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
            'permission' => 0777,
        ],

        'sentry' => [
            'driver' => 'sentry',
            'bubble' => true, // Whether the messages that are handled can bubble up the stack or not
            'level' => 'error',
        ],

        'telegram' => [
            'driver' => 'monolog',
            'handler' => Monolog\Handler\TelegramBotHandler::class,
            'formatter' => \App\Services\Notifications\Channels\Telegram\TelegramMessageFormatter::class,
            'formatter_with' => [
                'dateFormat' => 'Y-m-d H:i:s',
            ],
            'with' => [
                'apiKey' => env('TELEGRAM_BOT_TOKEN', null),
                'channel' => env('TELEGRAM_LOG_CHAT_ID'),
                'delayBetweenMessages' => true,
            ],
            'level' => 'error',
        ],

        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
            'days' => $save_days,
            'permission' => 0777,
        ],

        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'HWM Log ' . env('APP_NAME'),
            'emoji' => ':boom:',
            'level' => 'error',
        ],

        'stderr' => [
            'driver' => 'monolog',
            'handler' => StreamHandler::class,
            'formatter' => env('LOG_STDERR_FORMATTER'),
            'with' => [
                'stream' => 'php://stderr',
            ],
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => 'debug',
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => 'debug',
        ],

        'testing' => [
            'driver' => 'errorlog',
            'level' => 'emergency',
        ],
        'console' => [
            'driver' => 'daily',
            'path' => storage_path('logs/console.log'),
            'level' => 'debug',
            'days' => $save_days,
            'permission' => 0777,
        ],
        'activity' => [
            'driver' => 'custom',
            'via' => \App\Services\ActivityLogger\CreateActivityLogger::class,
        ]
    ],
];
