@extends(backpack_view('blank'))

@php
  $breadcrumbs = [
    trans('backpack::crud.admin') => backpack_url('dashboard'),
    'Settings' => false,
  ];
@endphp

@section('header')
	<section class="container-fluid">
	  <h2>
        <span class="text-capitalize">App Settings</span>
        <small>Edit.</small>
	  </h2>
	</section>
@endsection

@section('content')
<div class="row">
	<div class="col-12">
		<div class="card">

			@if ($errors->any())
				<div class="alert alert-danger pb-0 mb-1">
					<ul class="list-unstyled">
						@foreach($errors->all() as $error)
							<li><i class="fa fa-info-circle"></i> {{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif

		  <form method="post" action="{{ backpack_url('app-settings') }}" class="card-body">
		  {!! csrf_field() !!}
		  {!! method_field('PUT') !!}

			  @foreach($settings->toArray() as $key => $value)
				  <div class="form-group">
					  <label class="d-block">{{ ucwords(str_replace('_', ' ', $key)) }}</label>
					  @if(is_bool($value))
						  <div class="custom-control custom-checkbox">
							  <input type="checkbox" class="custom-control-input" id="{{ $key }}" name="settings[{{ $key }}]" {{ $value ? 'checked' : '' }}>
							  <label class="custom-control-label" for="{{ $key }}"></label>
						  </div>
					  @else
					  <input type="text" class="form-control" name="settings[{{ $key }}]" value="{{ $value }}">
					  @endif
				  </div>
			  @endforeach

            <div class="form-group">
				<button type="submit" class="btn btn-success">
					<span class="fa fa-save" role="presentation" aria-hidden="true"></span> &nbsp;
					Save
				</button>
			</div>
		  </form>
		</div>
	</div>
</div>
@endsection
