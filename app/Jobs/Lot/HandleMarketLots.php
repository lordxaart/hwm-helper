<?php

declare(strict_types=1);

namespace App\Jobs\Lot;

use App\Core\Enums\Queue;
use App\Events\Lot\CreatedNewLotEvent;
use App\HWM\Entities\Certificate;
use App\HWM\Entities\Element;
use App\HWM\Entities\House;
use App\HWM\Entities\MarketLot;
use App\HWM\Entities\ObjectShare;
use App\HWM\Enums\EntityType;
use App\HWM\Helpers\HwmMarketSort;
use App\Models\Art;
use App\Models\Lot;
use App\Models\LotMarketParserLog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class HandleMarketLots implements ShouldQueue, ShouldBeUnique
{
    use InteractsWithQueue;
    use Queueable;

    public $uniqueFor = 3600;
    private array $state = [];
    private int $lotsCount = 0;
    private int $newLotsCount = 0;
    private int $maxResultsLimit = 100;

    public function __construct(public string $entityType, public ?string $entityId = null)
    {
        $this->queue = Queue::HWM_NEW_MARKET_LOTS->value;
    }

    public function uniqueId(): string
    {
        return $this->entityType . ':' . $this->entityId;
    }

    /**
     * @return void
     */
    public function handle(): void
    {
        $start = microtime(true);
        $error = null;
        try {
            $this->entityType === EntityType::ART->value
                ? $this->handleArt($this->entityId)
                : $this->handleEntity($this->entityType, $this->entityId);
        } catch (\Throwable $e) {
            if (isLocalEnv()) {
                throw $e;
            }
            $error = $e->getMessage();
        }

        LotMarketParserLog::insert([
            'entity_type' => $this->entityType,
            'entity_id' => $this->entityId ?? '',
            'lots_count' => $this->lotsCount,
            'new_lots_count' => $this->newLotsCount,
            'attempts' => 1,
            'errors' => $error,
            'time_execute' => round(microtime(true) - $start, 3),
            'created_at' => now()->format(LotMarketParserLog::getModel()->getDateFormat()),
            'updated_at' => now()->format(LotMarketParserLog::getModel()->getDateFormat()),
        ]);
    }

    private function handleArt(string $hwmId): void
    {
        // skip art filter without art_id
        if (!$this->entityId) {
            return;
        }

        $art = Art::findByHwmId($hwmId);
        if (!$art) {
            throw new \InvalidArgumentException('Not found art ' . $hwmId);
        }
        $lots = hwmClient()->getMarketLots(
            category: $art->market_category_id,
            entityId: $art->hwm_id,
            repair: $art->repair,
            sort: HwmMarketSort::NEWEST->value,
            resultLimit: $this->maxResultsLimit,
        );
        $this->lotsCount = $lots->count();

        /** @var MarketLot $lot */
        foreach ($lots as $lot) {
            $this->handleLot($lot);
        }
    }

    private function handleEntity(string $type, string $id = null): void
    {
        $disallowCats = ['res', 'elements', 'obj_share'];
        if ( in_array($type, $disallowCats) )
            return;

        $category = match ($type) {
            EntityType::HOUSE->value => House::MARKET_CATEGORY_ID,
            EntityType::OBJECT_SHARE->value => ObjectShare::MARKET_CATEGORY_ID,
            EntityType::CERTIFICATE->value => Certificate::MARKET_CATEGORY_ID,
            EntityType::ELEMENT->value => Element::MARKET_CATEGORY_ID,
            default => $type,
        };

        if (!$category) {
            return;
        }

        $lots = hwmClient()->getMarketLots(
            category: $category,
            entityId: $id,
            sort: HwmMarketSort::NEWEST->value,
            resultLimit: $this->maxResultsLimit,
        );
        $this->lotsCount = $lots->count();
        $lots_ids = [];
        foreach ($lots as $lot) {
            $lots_ids[$lot->lot_id] = $lot;
        }

        $exists = Lot::whereIn('lot_id', array_keys($lots_ids))->pluck('lot_id');
        foreach ($exists as $lotId) {
            if (!empty($lots_ids[$lotId])) {
                unset($lots_ids[$lotId]);
            }
        }

        /** @var MarketLot $lot */
        foreach ($lots_ids as $lot) {
            $this->handleLot($lot);
        }
    }

    private function handleLot(MarketLot $lotEntity): void
    {
        if (!$lotEntity->started_at) {
            return;
        }

        $lot = Lot::createFromLotEntity($lotEntity);
        $this->newLotsCount++;

        event(new CreatedNewLotEvent($lot));

        dispatch(new LotUpdate($lot->lot_id));
    }
}
