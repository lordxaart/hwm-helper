<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\HWM\Entities\Lot;
use Illuminate\Http\Request;

/**
 * @property Lot $resource
 */
class LotResource extends LotBaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $lot = $this;

        $data = array_merge(parent::toArray($request), [
            'quantity' => $lot->quantity,
            'startedAtHuman' => $lot->started_at->diffForHumans(),
            'startedAt' => $lot->started_at->format(config('app.date_format')),
            'buyedAtHuman' => $lot->buyed_at ? $lot->buyed_at->diffForHumans() : null,
            'buyedAt' => $lot->buyed_at ? $lot->buyed_at->format(config('app.date_format')) : null,
            'updatedAt' => $lot->updated_at ? $lot->updated_at->format(config('app.date_format')) : null,
            'isBuyed' => !!$lot->buyed_at,
            'isActive' => $lot->isActive(),
            'isParsed' => $lot->is_parsed,
        ]);

        return $this->transformByRequest($data, $request);
    }
}
