@php
/** @var \App\Models\Feedback $feedback */
@endphp
@component('mail::message')

<h1>{{ __('app.feedback.new_feedback') }}</h1>

<div class="jumbotron">{{ $feedback->data['message'] }}</div>

<ul>
    @foreach($feedback->data as $key => $value)
    @if($key != 'message')
        <li><strong>{{ mb_ucfirst(str_replace('_', ' ', $key)) }}</strong>: {{ $value }}</li>
    @endif
    @endforeach
    <li><strong>{{ mb_ucfirst(__('app.main.attachments')) }}</strong>: {{ __('app.main.count_files', ['count' => count($feedback->attachments)]) }}</li>
    <li><strong>{{ mb_ucfirst(__('app.main.created')) }}</strong>: {{ $feedback->created_at->diffForHumans() }} ({{ $feedback->created_at->format(config('app.date_format')) }})</li>
</ul>

@endcomponent
