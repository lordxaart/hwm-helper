(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["privacy"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/pages/Privacy.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/pages/Privacy.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var JS_app_config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! JS/app/config */ "./resources/assets/js/app/config/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  metaInfo() {
    return {
      title: this.$t('app.main.privacy')
    };
  },

  computed: {
    config() {
      return JS_app_config__WEBPACK_IMPORTED_MODULE_0__["default"];
    }

  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/pages/Privacy.vue?vue&type=template&id=e96e8d26&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/pages/Privacy.vue?vue&type=template&id=e96e8d26&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c("h1", { staticClass: "mb-4" }, [
      _vm._v("Политика конфиденциальности для HWM Helper")
    ]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "В " +
          _vm._s(_vm.config.app.name) +
          ", доступном из " +
          _vm._s(_vm.config.app.url) +
          ", одним из наших главных приоритетов является конфиденциальность наших посетителей. Этот документ Политики конфиденциальности содержит типы информации, которая собирается и регистрируется " +
          _vm._s(_vm.config.app.name) +
          ", и способы ее использования."
      )
    ]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Если у вас есть дополнительные вопросы или вам нужна дополнительная информация о нашей Политике конфиденциальности, не стесняйтесь обращаться к нам по электронной почте по адресу " +
          _vm._s(_vm.config.app.owner.email)
      )
    ]),
    _vm._v(" "),
    _c("h3", [_vm._v("Лог-файлы")]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        _vm._s(_vm.config.app.name) +
          " следует стандартной процедуре использования файлов журнала. Эти файлы регистрируют посетителей, когда они посещают веб-сайты. Все хостинговые компании делают это и часть аналитики хостинговых услуг. Информация, собранная с помощью файлов журнала, включает адреса интернет-протокола (IP), тип браузера, поставщика услуг Интернета (ISP), отметку даты и времени, страницы перехода / выхода и, возможно, количество кликов. Они не связаны с какой-либо информацией, позволяющей установить личность. Целью этой информации является анализ тенденций, администрирование сайта, отслеживание движения пользователей на сайте и сбор демографической информации."
      )
    ]),
    _vm._v(" "),
    _c("h3", [_vm._v("Cookies и веб-маяки")]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Как и любой другой веб-сайт, " +
          _vm._s(_vm.config.app.name) +
          " использует файлы cookie. Эти файлы cookie используются для хранения информации, включая предпочтения посетителей и страницы на веб-сайте, которые посетитель посещал или посещал. Эта информация используется для оптимизации работы пользователей путем настройки содержимого нашей веб-страницы на основе типа браузера посетителей и / или другой информации."
      )
    ]),
    _vm._v(" "),
    _c("h3", [_vm._v("Политика конфиденциальности")]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Сторонние рекламные серверы или рекламные сети используют такие технологии, как файлы cookie, JavaScript или веб-маяки, которые используются в их соответствующих рекламных объявлениях и ссылках, которые появляются в " +
          _vm._s(_vm.config.app.name) +
          ", которые отправляются непосредственно пользователям в браузер. Они автоматически получают ваш IP-адрес, когда это происходит. Эти технологии используются для измерения эффективности их рекламных кампаний и / или для персонализации рекламного контента, который вы видите на посещаемых вами веб-сайтах."
      )
    ]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Обратите внимание, что " +
          _vm._s(_vm.config.app.name) +
          " не имеет доступа или контроля над этими файлами cookie, которые используются сторонними рекламодателями."
      )
    ]),
    _vm._v(" "),
    _c("h3", [_vm._v("Политика конфиденциальности третьих лиц")]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Политики конфиденциальности третьих сторонПолитика конфиденциальности " +
          _vm._s(_vm.config.app.name) +
          " не распространяется на другие рекламодатели или веб-сайты. Мы рекомендуем вам ознакомиться с этой политикой конфиденциальности. Это может включать в себя их методы и инструкции. Ссылки на политику конфиденциальности и их ссылки здесь."
      )
    ]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Вы можете отключить куки-файлы в настройках вашего браузера. Чтобы узнать более подробную информацию об управлении cookie-файлами в определенных веб-браузерах, ее можно найти на соответствующих веб-сайтах браузеров. Что такое куки?"
      )
    ]),
    _vm._v(" "),
    _c("h3", [_vm._v("Дополнительная информация")]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Еще одной частью нашего приоритета является добавление защиты детей при использовании Интернета. Мы призываем родителей и опекунов наблюдать, участвовать и / или отслеживать и направлять их деятельность в Интернете."
      )
    ]),
    _vm._v(" "),
    _c("h3", [_vm._v("Только политика конфиденциальности онлайн")]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Настоящая Политика конфиденциальности применяется только к нашей онлайн-деятельности и действительна для посетителей нашего веб-сайта в отношении информации, которую они предоставили и / или собрали в " +
          _vm._s(_vm.config.app.name) +
          ". Эта политика не распространяется на любую информацию, собранную в автономном режиме или по каналам, отличным от данного веб-сайта."
      )
    ]),
    _vm._v(" "),
    _c("h3", [_vm._v("Cогласие")]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Используя наш сайт, вы тем самым соглашаетесь с нашей Политикой конфиденциальности и соглашаетесь с ее Условиями."
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/assets/js/app/pages/Privacy.vue":
/*!***************************************************!*\
  !*** ./resources/assets/js/app/pages/Privacy.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Privacy_vue_vue_type_template_id_e96e8d26_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Privacy.vue?vue&type=template&id=e96e8d26&scoped=true& */ "./resources/assets/js/app/pages/Privacy.vue?vue&type=template&id=e96e8d26&scoped=true&");
/* harmony import */ var _Privacy_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Privacy.vue?vue&type=script&lang=js& */ "./resources/assets/js/app/pages/Privacy.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Privacy_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Privacy_vue_vue_type_template_id_e96e8d26_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Privacy_vue_vue_type_template_id_e96e8d26_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "e96e8d26",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/app/pages/Privacy.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/app/pages/Privacy.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/assets/js/app/pages/Privacy.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Privacy_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Privacy.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/pages/Privacy.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Privacy_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/app/pages/Privacy.vue?vue&type=template&id=e96e8d26&scoped=true&":
/*!**********************************************************************************************!*\
  !*** ./resources/assets/js/app/pages/Privacy.vue?vue&type=template&id=e96e8d26&scoped=true& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Privacy_vue_vue_type_template_id_e96e8d26_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Privacy.vue?vue&type=template&id=e96e8d26&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/pages/Privacy.vue?vue&type=template&id=e96e8d26&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Privacy_vue_vue_type_template_id_e96e8d26_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Privacy_vue_vue_type_template_id_e96e8d26_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);