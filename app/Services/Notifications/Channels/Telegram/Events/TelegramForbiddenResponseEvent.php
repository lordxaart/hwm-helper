<?php

declare(strict_types=1);

namespace App\Services\Notifications\Channels\Telegram\Events;

use App\Core\Contracts\Notifable;
use Illuminate\Foundation\Bus\Dispatchable;

class TelegramForbiddenResponseEvent
{
    use Dispatchable;

    public function __construct(public readonly int $chatId, public readonly Notifable $notifable )
    {
    }
}
