<?php

declare(strict_types=1);

namespace App\HWM\Entities;

use App\HWM\Entities\Interfaces\HasRepository;
use Illuminate\Database\Eloquent\Model;

abstract class HwmEntityWithModel extends HwmEntity implements HasRepository
{
    protected ?Model $model;

    public function __construct(string $hwmId, array $parameters = [])
    {
        /** @phpstan-var Model model */
        $model = $this->getRepository()->findByHwmId($hwmId);
        $this->model = $model;

        parent::__construct($hwmId, $parameters);
    }

    public function getModel(): ?Model
    {
        return $this->model;
    }
}
