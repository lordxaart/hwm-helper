<?php

declare(strict_types=1);

namespace App\Listeners\Lot;

use App\Core\Enums\Queue;
use App\Events\Lot\CreatedNewLotEvent as Event;
use App\Notifications\User\AccordedFilters;
use App\Services\ActivityLogger\ActivityLogger;
use App\Services\ActivityLogger\ValueObjects\Activity;
use App\Services\LotFilter\LotAccordChecker;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreatedLotListener implements ShouldQueue
{
    public string $queue;

    public function __construct(
        private readonly LotAccordChecker $lotAccordChecker,
        private readonly ActivityLogger $activityLogger
    ) {
        $this->queue = Queue::HWM_NEW_LOTS->value;
    }

    public function handle(Event $event): void
    {
//        $this->activityLogger->log(new Activity('HWM_LOT_CREATE', ['lot_id' => $event->lot->lot_id]));

        if (now()->diffInSeconds($event->lot->started_at) < AccordedFilters::NOT_ACTUAL_LOT_DELAY) {
            $this->lotAccordChecker->handleFiltersForLot($event->lot->asLotEntity());
        }
    }
}
