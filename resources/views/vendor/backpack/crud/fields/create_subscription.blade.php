<div id="vue-app">
    <div class="form-group">
        <label>User</label>
        <select class="select2 form-control custom-select" name="user_id">
            @foreach(\App\Models\User::all() as $user)
                <option value="{{ $user->id }}">{{ $user->email }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label>Subscription</label>
        <select class="form-control custom-select subscription-select" name="plan_id">
            @foreach(\App\Http\Controllers\Admin\Models\SubscriptionModel::all() as $s)
                <optgroup label="{{ $s->name }}">
                    @foreach($s->plans as $plan)
                        <option value="{{ $plan->id }}">{{ $plan->name }}</option>
                    @endforeach
                </optgroup>
            @endforeach
        </select>
        <div class="help-block small plan-info"></div>
    </div>
    <div class="form-group">
        <label>
            With Trial
            <input type="checkbox" class="custom-checkbox" name="with_trial">
        </label>
    </div>
</div>

@section('after_scripts')
    <link rel="stylesheet" href="/packages/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="/packages/select2-bootstrap-theme/dist/select2-bootstrap.min.css">
    <script src="/packages/select2/dist/js/select2.js"></script>

    <script>
        const plans = JSON.parse('{!!\App\Http\Controllers\Admin\Models\SubscriptionPlanModel::all()->toJson()!!}');
        console.log(plans);
        $(document).ready(() => {
            setPlan($('.subscription-select').val());
            $('.select2').select2({
                theme: 'bootstrap'
            });
            $('.subscription-select').change(function (val) {
                setPlan($(this).val());
                console.log($(this).val());
            });
        });

        function setPlan(planId) {
            const plan = plans.find(item => item.id === Number.parseInt(planId));
            if (!plan) {
                $('.plan-info').html('');
                return;
            }
            const now = new Date();
            let html = `
            <div class="card p-2">
            <div class="mt-1">Start: <strong>${now.toDateString()}</strong></div>
            <div>END: <strong>${(new Date(plan.end_at)).toDateString()}</strong></div>
            <div>Price: <strong>${plan.price / 100} ${plan.currency}</strong></div>
            <div>Alias: <strong>${plan.alias}</strong></div>
            </div>
        `;
            $('.plan-info').html(html);
        }
    </script>
@endsection
