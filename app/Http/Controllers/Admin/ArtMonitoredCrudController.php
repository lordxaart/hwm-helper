<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

/**
 * Class UserCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ArtMonitoredCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;

    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel('App\Models\MonitoredArt');
        $this->crud->addClause('orderBy', 'created_at', 'desc');
        $this->crud->with(['user', 'art']);

        $this->crud->setRoute(config('backpack.base.route_prefix') . '/monitored-arts');
        $this->crud->setEntityNameStrings('monitored-art', 'monitored-arts');

        $this->crud->set('show.setFromDb', false);
    }

    protected function setupListOperation()
    {
        $this->crud->setColumns([
            ['name' => 'id', 'label' => 'ID', 'type' => 'number'],
            ['name' => 'user', 'label' => 'User', 'type' => 'view', 'view' => 'crud::columns.user'],
            ['name' => 'art', 'label' => 'Art', 'type' => 'view', 'view' => 'crud::columns.art'],
            ['name' => 'is_active', 'label' => 'Active', 'type' => 'check',],
            ['name' => 'created_at', 'label' => 'Created At', 'type' => 'datetime'],
            ['name' => 'updated_at', 'label' => 'Updated At', 'type' => 'datetime'],
        ]);
    }
}
