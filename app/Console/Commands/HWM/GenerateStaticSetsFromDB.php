<?php

declare(strict_types=1);

namespace App\Console\Commands\HWM;

use App\Console\Commands\Command;
use App\Models\ArtSet;

class GenerateStaticSetsFromDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hwm:set:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate sets list from db to json (hwm storage)';

    public function handle(): void
    {
        $sets = ArtSet::orderBy('title')->get([
            'hwm_id',
            'title',
        ]);

        if (!$sets->count()) {
            $this->warn('Not found any sets');
            return;
        }

        $setsFormatted = [];
        foreach ($sets as $set) {
            $setsFormatted[$set->hwm_id] = $set->title;
        }

        $content = json_encode($setsFormatted, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

        file_put_contents(hwm_storage_path('sets.json'), $content);

        $this->info('Count sets - ' . $sets->count());
    }
}
