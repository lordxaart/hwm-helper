@php
    /** @var \App\Services\Subscriptions\SubscriptionRepository $repo */
    $repo = app(\App\Services\Subscriptions\SubscriptionRepository::class);

    $subs = $repo->getSubscriptions();
@endphp
<div class="card">
    <div class="card-body">
        <ul class="d-flex flex-column flex-lg-row">
            @foreach($subs as $sub)
                <li>
                    <h5>{{ $sub->name }} <small>({{ $sub->alias }})</small></h5>
                    <ul>
                    @foreach($repo->getPlansBySubscription($sub->id) as $plan)
                            <li>{{ $plan->name }} ({{ $plan->alias }}) <strong>{{ $plan->price }} {{ $plan->currency }}</strong> | <span>{{ $plan->duration }} <small>({{ $plan->trial }})</small></span></li>
                    @endforeach
                    </ul>
                </li>
            @endforeach
        </ul>
    </div>
</div>

