<?php

declare(strict_types=1);

namespace App\HWM\Repositories\Interfaces;

interface StorageRepositoryInterface
{
    public function getStoragePath(): string;
}
