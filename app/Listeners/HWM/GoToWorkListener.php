<?php

declare(strict_types=1);

namespace App\Listeners\HWM;

use App\Events\HWM\WorkEvent;
use Illuminate\Contracts\Queue\ShouldQueue;

class GoToWorkListener implements ShouldQueue
{
    public function handle(WorkEvent $event): void
    {
        //
    }
}
