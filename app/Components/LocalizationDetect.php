<?php

declare(strict_types=1);

namespace App\Components;

use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Http;

class LocalizationDetect
{
    public const COOKIE_KEY = 'locale';

    /**
     * @var array
     */
    public array $locales;

    private string $ipStackAccessKey;

    public function __construct(array $locales)
    {
        $this->locales = $locales;

        $this->ipStackAccessKey = env('IP_STACK_ACCESS_KEY', '');
    }

    public function getLocaleFromCookie(bool $clearUnsupportedLocale = true): ?string
    {
        $locales = array_keys($this->locales);

        // check if locale set in cookie
        if (Cookie::has(self::COOKIE_KEY)) {
            $locale = strval(Cookie::get(self::COOKIE_KEY));

            if (in_array($locale, $locales)) {
                return $locale;
            }
            // if locale from cookie not supported then clear this cookie
            if ($clearUnsupportedLocale) {
                Cookie::queue(Cookie::forget(self::COOKIE_KEY));
            }
        }

        return null;
    }

    public function detectLocaleFromHttpHeader(): ?string
    {
        $availableLocales = [];
        foreach ($this->locales as $locale => $value) {
            if (isset($value['regional'])) {
                $availableLocales[] = $value['regional'];
            }
            $availableLocales[] = $locale;
        }

        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $langs = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);

            foreach ($langs as $lang) {
                if (str_contains($lang, ';')) {
                    $lang = explode(';', $lang)[0];
                }

                if (in_array($lang, $availableLocales)) {
                    return $lang;
                }
            }
        }

        return null;
    }

    public function detectLocaleFromIp(): ?string
    {
        if (!$this->ipStackAccessKey || (!$userIp = getUserIp())) {
            return null;
        }

        $availableLocales = [];
        foreach ($this->locales as $locale => $value) {
            if (isset($value['regional'])) {
                $availableLocales[] = $value['regional'];
            }
            $availableLocales[] = $locale;
        }

        $url = "http://api.ipstack.com/$userIp?access_key=$this->ipStackAccessKey&fields=location.languages";
        $langs = Http::get($url)->json()['location']['languages'];

        foreach ($langs as $lang) {
            if (isset($lang['code']) && in_array($lang['code'], $availableLocales)) {
                return $lang['code'];
            }
        }

        return null;
    }
}
