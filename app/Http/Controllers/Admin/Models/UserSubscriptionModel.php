<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin\Models;

use App\Models\User;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property Carbon $ended_at
 */
class UserSubscriptionModel extends Model
{
    use CrudTrait;

    protected $table = 'user_subscriptions';
    protected $casts = [
        'started_at' => 'datetime',
        'ended_at' => 'datetime',
        'trial_ended_at' => 'datetime',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function subscription()
    {
        return $this->belongsTo(SubscriptionModel::class, 'subscription_id');
    }

    public function plan()
    {
        return $this->belongsTo(SubscriptionPlanModel::class, 'subscription_plan_id');
    }

    public function isActive(): bool
    {
        return $this->ended_at->getTimestamp() >= time();
    }

    public function isTrialActive(): bool
    {
        return $this->trial_ended_at && $this->trial_ended_at->getTimestamp() >= time();
    }
}
