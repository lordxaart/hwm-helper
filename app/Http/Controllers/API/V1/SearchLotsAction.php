<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V1;

use App\Components\Elastic\Exceptions\ElasticConnectionException;
use App\Components\LotCache;
use App\Components\Pagination\LimitOffsetPaginator;
use App\Http\Resources\LotCollection;
use App\HWM\Entities\Lot;
use App\HWM\Helpers\HwmHelper;
use App\Services\LotFilter\LotAccordChecker;
use App\Services\LotFilter\Repositories\LotFiltersRepository;
use App\Services\SearchLots\SearchLotService;
use App\Services\SearchLots\ValueObjects\LimitOffset;
use App\Services\SearchLots\ValueObjects\LotFilters;
use App\Services\SearchLots\ValueObjects\OrderBy;
use DateTimeZone;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class SearchLotsAction extends ApiController
{
    private const DEFAULT_LIMIT = 100;
    private const MAX_LIMIT = 100;

    public function __invoke(Request $request, Application $app, LoggerInterface $logger)
    {
        $start = microtime(true);

        try {
            /** @var SearchLotService $searchLotService */
            $searchLotService = $app->get(SearchLotService::class);
        } catch (ElasticConnectionException $e) {
            $logger->error($e->getMessage(), ['exception' => $e]);
            return $this->response(['error' => trans('errors.503') . '. ' . trans('errors.500_message')])
                ->setStatusCode(HttpResponse::HTTP_SERVICE_UNAVAILABLE);
        }

        $timezone = new DateTimeZone(HwmHelper::HEROES_TZ);
        $filters = new LotFilters($request->all(), $timezone);

        $sortField = $request->get('order_by');
        $sortDirection = $request->get('order_desc', OrderBy::ASC);
        $orderBy = new OrderBy([$sortField => $sortDirection]);

        $offset = (int)$request->get('offset', 0);
        $limit = (int)$request->get('limit', self::DEFAULT_LIMIT);
        $limitOffset = new LimitOffset(
            min($limit, self::MAX_LIMIT),
            $offset,
        );

        $response = $searchLotService->searchLotsByFilter($filters, $limitOffset, $orderBy);
        $paginator = new LimitOffsetPaginator($response->getData()->toBase(), $limit, $offset);

        $paginator
            ->setPath($request->url())
            ->setQuery($request->query->all());

        $actionExec = microtime(true) - $start;

        $collection = new LotCollection($paginator);

        $entities = [];
        /** @var Lot $lot */
        foreach ($paginator->getItems() as $lot) {
            $entityId = $lot->entity_type . '_' . $lot->entity_id;
            if (empty($entities[$entityId])) {
                $entities[$entityId] = $lot->entityAsArray();
            }
        }
        $collection->setEntities($entities);

        $collection->additional([
            'meta' => [
                'extra' => array_merge($response->getExtra(), ['search_exec' => $actionExec]),
                'maxLotId' => LotCache::getMaxLotId(),
                'minLotStartedAt' => LotCache::getOldestLot()?->started_at?->format('Y-m-d'),
            ]
        ]);

        // resolve filters and set to collection
        if (user() && in_array('accorded_filters', $collection->includes)) {
            /** @var LotFiltersRepository $lotFiltersRepository */
            $lotFiltersRepository = app(abstract: LotFiltersRepository::class);
            $accordedFilters = [];
            $filters = $lotFiltersRepository->findAllByUserId(user()->getId());

            if (!empty($filters)) {
                /** @var LotAccordChecker $filterChecker */
                $filterChecker = app(LotAccordChecker::class);
                foreach ($response->getData() as $lot) {
                    $accordedFilters[$lot->getHwmId()] = [];
                    foreach ($filters as $filter) {
                        if ($filterChecker->isLotAccordedToFilter($lot, $filter)) {
                            $accordedFilters[$lot->getHwmId()][] = $filter;
                        }
                    }
                }
            }

            $collection->setAccordedFilters($accordedFilters);
        }

        return $this->response($collection);
    }
}
