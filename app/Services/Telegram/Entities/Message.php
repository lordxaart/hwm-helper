<?php

declare(strict_types=1);

namespace App\Services\Telegram\Entities;

class Message
{
    public readonly int $message_id;
    public readonly int $date;
    public readonly Chat $chat;
    public readonly User|null $from;
    public readonly string|null $text;
    /** @var MessageEntity[]|null */
    public readonly array|null $entities;

    public function __construct(array $message)
    {
        $this->message_id = (int) $message['message_id'];
        $this->date = (int) $message['date'];
        $this->chat = new Chat($message['chat']);

        $this->from = isset($message['from']) ? new User($message['from']) : null;

        $this->text = $message['text'] ?? null;

        if (isset($message['entities']) && is_array($message['entities'])) {
            $entities = [];
            foreach ($message['entities'] as $entity) {
                $entities[] = new MessageEntity($entity);
            }
            $this->entities = $entities;
        } else {
            $this->entities = null;
        }
    }

    public function isCommand(): bool
    {
        if (empty($this->entities)) {
            return false;
        }

        foreach ($this->entities as $entity) {
            if ($entity->isBotCommand()) {
                return true;
            }
        }

        return false;
    }

    public function getCommand(): ?string
    {
        if (!$this->entities || !$this->isCommand()) {
            return null;
        }

        $command = null;

        foreach ($this->entities as $entity) {
            if ($entity->isBotCommand()) {
                $command = $entity;
            }
        }

        if (!$command) {
            return null;
        }

        return substr((string)$this->text, $command->offset, $command->length);
    }
}
