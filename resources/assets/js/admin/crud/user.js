/* eslint-env jquery */

import notify from '../helpers/notify';
import handleError from '../helpers/handleError';

// Change category
$('body').on('change', '.change_category', function () {
    const option = $(this).find('option:selected')[0];
    const url = $(this).data('url');
    const data = {};
    data[$(this).data('column')] = option.getAttribute('value') ? option.getAttribute('value') : null;
    global.axios.put(url, data).then(() => {
        notify('Category was updated');
    }).catch(handleError);
});

window.changeUserStatus = (el) => {
    const button = $(el);
    const route = button.data('route');
    const type = button.data('type');
    const action = type.charAt(0).toUpperCase() + type.slice(1);

    global.swal({
        title: `${action} account`,
        text: button.data('text'),
        icon: 'warning',
        buttons: {
            cancel: {
                text: 'Cancel',
                value: null,
                visible: true,
                className: 'bg-secondary',
                closeModal: true,
            },
            delete: {
                text: action,
                value: true,
                visible: true,
                className: 'bg-danger',
            },
        },
    }).then((value) => {
        if (value) {
            $.ajax({
                url: route,
                type: type === 'delete' ? 'DELETE' : 'POST',
                success() {
                    notify(`Success ${action} account`);
                    if (global.crudTable()) {
                        global.crudTable().fnDraw();
                    } else {
                        window.location.reload();
                    }
                    // Hide the modal, if any
                    $('.modal').modal('hide');
                },
                error: handleError,
            });
        }
    });
};
