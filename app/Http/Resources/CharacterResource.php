<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Http\Resources\Core\ApiResource;
use Illuminate\Http\Request;

/**
 * @extends \App\Models\Character
 */
class CharacterResource extends ApiResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return $this->transformByRequest([
            'id' => $this->id,
            'hwmId' => $this->hwm_id,
            'login' => $this->login,
            'level' => $this->level,
            'experience' => $this->experience,
            'status' => $this->status,
            'resources' => $this->resources_formatted,
            'guilds' => $this->guilds_formatted,
            'fractions' => $this->fractions_formatted,
            'createdAt' => $this->created_at?->format(config('app.date_format')),
            'updatedAt' => $this->updated_at?->format(config('app.date_format')),
        ], $request);
    }
}
