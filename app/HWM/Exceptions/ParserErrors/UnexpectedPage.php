<?php

declare(strict_types=1);

namespace App\HWM\Exceptions\ParserErrors;

use App\HWM\Exceptions\ParserError;

class UnexpectedPage extends ParserError
{
    public function __construct(string $expectedPage, ?string $realPage = '')
    {
        $message = trans('errors.hwm.it_is_nota_expected_page', ['page' => $expectedPage]);

        if ($realPage) {
            $message .= '. ' . trans('errors.hwm.expected_page', ['page' => $realPage]);
        }

        parent::__construct($message);
    }
}
