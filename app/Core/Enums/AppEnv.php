<?php

declare(strict_types=1);

namespace App\Core\Enums;

enum AppEnv: string
{
    case LOCAL = 'local';
    case DEV = 'dev';
    case PROD = 'prod';
    case TEST = 'testing';
}
