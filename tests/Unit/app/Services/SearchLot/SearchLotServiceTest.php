<?php

declare(strict_types=1);

namespace Tests\Unit\app\Services\SearchLot;

use App\Services\SearchLots\Repositories\Converter;
use App\Services\SearchLots\ValueObjects\LotFilters;
use App\Services\SearchLots\ValueObjects\OrderBy;
use Tests\SimpleTestCase;
use Tests\Traits\AssertArray;

class SearchLotServiceTest extends SimpleTestCase
{
    use AssertArray;

    public function testConverterFilters()
    {
        foreach ($this->successDataProviderForFilters() as [$inputFilters, $expectedFilters]) {
            $converter = new Converter();
            $this->assertArraySimilar($expectedFilters, $converter->parseFilters(new LotFilters($inputFilters)));
        }
    }

    public function testConverterOrders()
    {
        foreach ($this->successDataProviderForOrders() as [$inputOrders, $expectedOrders]) {
            $converter = new Converter();
            if (is_array($expectedOrders)) {
                $this->assertArraySimilar($expectedOrders, $converter->parseOrders(new OrderBy($inputOrders)));
            }

            if (is_string($expectedOrders)) {
                $this->expectException($expectedOrders);
                $converter->parseOrders(new OrderBy($inputOrders));
            }
        }
    }

    public function successDataProviderForFilters(): array
    {
        return [
            [
                ['price' => '>=127', 'lot_id' => 100],
                ['term' => ['_id' => 100]],
            ],
            [
                ['lot_type' => 'sale,auction', 'entity_type' => 'art,resource,element', 'entity_id' => 'cold_ring,1,4', 'seller_id' => 12321312],
                [
                    'bool' => [
                        'filter' => [
                            [
                                'terms' => [
                                    'lot_type' => ['sale', 'auction']
                                ]
                            ],
                            [
                                'terms' => [
                                    'entity_type' => ['art', 'resource', 'element']
                                ]
                            ],
                            [
                                'terms' => [
                                    'entity_id' => ['cold_ring', '1', '4']
                                ]
                            ],
                            [
                                'terms' => [
                                    'seller_id' => ['12321312']
                                ]
                            ]
                        ]
                    ]
                ],
            ],
            [
                ['is_buyed' => '1', 'craft' => '0'], //input
                [
                    'bool' => [
                        'filter' => [
                            [
                                'exists' => [
                                    'field' => 'buyed_at'
                                ]
                            ]
                        ],
                        'must_not' => [
                            [
                                'exists' => [
                                    'field' => 'craft'
                                ]
                            ]
                        ]
                    ]
                ], //output
            ],
            [
                ['price' => '1', 'ended_at' => '2018-06-25'], //input
                [
                    'bool' => [
                        'filter' => [
                            [
                                'term' => [
                                    'price' => 1
                                ]
                            ],
                            [
                                'term' => [
                                    'ended_at' => '2018-06-25',
                                ]
                            ]
                        ],
                    ]
                ], //output
            ],
            [
                ['is_buyed' => null], //input
                [], //output
            ],
            [
                ['price' => '1', 'price_per_fight' => '<200', 'current_strength' => '>=51', 'base_strength' => '>411'], //input
                [
                    'bool' => [
                        'filter' => [
                            [
                                'term' => [
                                    'price' => 1
                                ]
                            ],
                            [
                                'range' => [
                                    'price_per_fight' => [
                                        'lt' => 200
                                    ]
                                ]
                            ],
                            [
                                'range' => [
                                    'current_strength' => [
                                        'gte' => 51
                                    ]
                                ]
                            ],
                            [
                                'range' => [
                                    'base_strength' => [
                                        'gt' => 411
                                    ]
                                ]
                            ]
                        ],
                    ]
                ], //output
            ],
            [
                ['started_at' => '>2021-01-01', 'ended_at' => '>=2021-02-21', 'buyed_at' => '<2017-10-02'], //input
                [
                    'bool' => [
                        'filter' => [
                            [
                                'range' => [
                                    'started_at' => [
                                        'gt' => '2021-01-02 00:00:00',
                                        'format' => 'yyyy-MM-dd HH:mm:ss',
                                    ]
                                ]
                            ],
                            [
                                'range' => [
                                    'ended_at' => [
                                        'gte' => '2021-02-21 00:00:00',
                                        'format' => 'yyyy-MM-dd HH:mm:ss',
                                    ]
                                ]
                            ],
                            [
                                'range' => [
                                    'buyed_at' => [
                                        'lt' => '2017-10-01 23:59:59',
                                        'format' => 'yyyy-MM-dd HH:mm:ss',
                                    ]
                                ]
                            ]
                        ],
                    ]
                ], //output
            ],
            [
                ['started_at' => '<=2021-01-01', 'ended_at' => '2021-01-01,2021-02-03'], //input
                [
                    'bool' => [
                        'filter' => [
                            [
                                'range' => [
                                    'started_at' => [
                                        'lte' => '2021-01-01 23:59:59',
                                        'format' => 'yyyy-MM-dd HH:mm:ss',
                                    ]
                                ]
                            ],
                            [
                                'range' => [
                                    'ended_at' => [
                                        'gte' => '2021-01-01 00:00:00',
                                        'lte' => '2021-02-03 23:59:59',
                                        'format' => 'yyyy-MM-dd HH:mm:ss',
                                    ]
                                ]
                            ],
                        ],
                    ]
                ], //output
            ],
        ];
    }

    public function successDataProviderForOrders(): array
    {
        return [
            [
                ['started_at' => 'DESC','price' => null,], // input
                [
                    ['started_at' => ['order' => 'desc', 'format' => 'yyyy-MM-dd']],
                    ['price' => 'asc']
                ] // output
            ],
            [
                ['lot_id' => 'asc'], // input
                \InvalidArgumentException::class,
            ],
        ];
    }
}
