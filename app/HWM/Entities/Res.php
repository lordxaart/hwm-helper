<?php

declare(strict_types=1);

namespace App\HWM\Entities;

use App\HWM\Entities\Interfaces\CanBeLot;
use App\HWM\Entities\Interfaces\HasRepository;
use App\HWM\Enums\EntityType;
use App\HWM\Repositories\ElementRepository;

class Res extends HwmEntity implements CanBeLot, HasRepository
{
    public const MARKET_CATEGORY_ID = 'res';

    public function getEntityType(): EntityType
    {
        return EntityType::RESOURCE;
    }

    public function getMarketCategoryId(): string
    {
        return self::MARKET_CATEGORY_ID;
    }

    public function getImage(bool $realPath = false, string $size = null): string
    {
        $path = 'images/icons/hwm/resources/' . $this->getHwmId() . '.png';

        return !$realPath ? asset($path) : public_path($path);
    }

    public function getRepository(): ElementRepository
    {
        return app(ElementRepository::class);
    }

    public static function convertToGold(string $resource, int $value): int
    {
        $rate = 1;

        if (in_array($resource, ['wood', 'ore'])) {
            $rate = 180;
        }

        if (in_array($resource, ['mercury', 'sulfur', 'crystal', 'gem'])) {
            $rate = 360;
        }

        return $value * $rate;
    }
}
