<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', ADMIN_PREFIX),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
    'as' => ADMIN_PREFIX . '.'
], function () {
    // custom admin routes
    Route::get('/', function () {
        return redirect(backpack_url('statuspage'));
    })->name('backpack');

    Route::get('statuspage', 'StatusPageController@index')->name('statuspage');

    // CRUD
    Route::crud('arts', 'ArtCrudController');

    Route::crud('art-categories', 'ArtCategoryCrudController');
    Route::crud('market-categories', 'MarketCategoryCrudController');
    Route::crud('art-sets', 'ArtSetCrudController');

    Route::crud('bots', 'HwmBotCrudController');

    Route::crud('users/subscriptions', 'UserSubscriptionController');
    Route::crud('users', 'UserCrudController');
    Route::post('users/{id}/restore', 'UserCrudController@restore');
    Route::post('users/{id}/block', 'UserCrudController@block');
    Route::post('users/{id}/unblock', 'UserCrudController@unblock');

    Route::crud('monitored-arts', 'ArtMonitoredCrudController');

    Route::crud('email-log', 'EmailLogCrudController');

    Route::get('app-settings', 'AppSettingController@index');
    Route::put('app-settings', 'AppSettingController@update');
});

// logout
//Route::get(ADMIN_PREFIX . '/logout', 'Auth\LoginController@logout');
