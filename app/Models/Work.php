<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Http\Client;
use Exception;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Crypt;

class Work extends Model
{
    public const STATUS_SUCCESS = 'SUCCESS';
    public const STATUS_PENDING = 'PENDING';
    public const STATUS_ALREADY_ON_WORK = 'ALREADY_ON_WORK';
    public const STATUS_BOT_ERROR = 'BOT_ERROR';
    public const STATUS_FAILED_WORK = 'FAILED_WORK';
    public const STATUS_UNDEFINED = 'UNDEFINED';

    /** @var string */
    protected $table = 'hwm_works';

    /** @var string[]  */
    protected $fillable = ['character_id', 'login', 'password', 'status', 'proxy', 'user_agent'];

    protected $casts = ['info_work' => 'array', 'next_work_at' => 'datetime'];

    public static string $botLink = 'http://127.0.0.1:3000/work';

    public function character(): BelongsTo
    {
        return $this->belongsTo(Character::class);
    }

    public static function scopeUser(Builder $builder, string $login): void
    {
        $builder->where('login', $login);
    }

    public function setPasswordAttribute(string $value): void
    {
        $this->attributes['password'] = Crypt::encryptString($value);
    }

    public function getPasswordAttribute(string $value): string
    {
        return Crypt::decryptString($value);
    }

    /**
     * @throws Exception
     */
    public static function goToWork(string|Character $loginOrCharacter, string $password = null): bool
    {
        if ($loginOrCharacter instanceof Character) {
            $login = $loginOrCharacter->login;
            $password = $loginOrCharacter->password;
            $character_id = $loginOrCharacter->id;
        } else {
            if (!$password) {
                throw new Exception('Password argument is required for login enter');
            }
            $login = $loginOrCharacter;
            $character_id = null;
        }

        $lastWork = self::getModel()->user($login)->latest()->first();

        // If next_work_at grather then now skipp iteration
        if ($lastWork && $lastWork->next_work_at && $lastWork->next_work_at->gt(now())) {
            return false;
        }

        $client = new Client();

        $currentWork = self::create([
            'login' => $login,
            'password' => $password,
            'character_id' => $character_id,
            'status' => self::STATUS_PENDING,
            'user_agent' => $client->getUserAgent(true),
        ]);

        try {
            set_time_limit(180);
            $response = $client->request('POST', self::$botLink, [
                'login' => $currentWork->login,
                'password' => $currentWork->password,
                'proxy' => $currentWork->proxy,
                'userAgent' => $currentWork->user_agent,
            ]);

            $res = json_decode($response->getBody()->getContents(), true);

            $currentWork->status = self::STATUS_SUCCESS;
            $currentWork->info_work = $res['infoWork'];

            // Create next work 1 hour and about some seconds (180-600)
            $currentWork->next_work_at = now()->addHour()->addSeconds(mt_rand(180, 600));
            $currentWork->save();

            event(new \App\Events\HWM\WorkEvent($currentWork));

            return true;
        } catch (ClientException | ServerException $e) {
            $res = json_decode($e->getResponse()->getBody()->getContents(), true);
        } catch (Exception $e) {
            $res = ['message' => $e->getMessage()];
        }

        $currentWork->error = $res;

        if (isset($res['errorType']) && $res['errorType'] === 'ALREADY_ON_WORK') {
            $currentWork->status = self::STATUS_ALREADY_ON_WORK;
        } else {
            $currentWork->status = self::STATUS_BOT_ERROR;
        }

        $lastFiveWork = self::getModel()->user($currentWork->login)
            ->latest()
            ->take(5)
            ->get();

        if ($lastFiveWork->where('status', self::STATUS_BOT_ERROR)->count() === 5) {
            $currentWork->next_work_at = now()->addHour()->addSeconds(mt_rand(180, 600));
        } else {
            $currentWork->next_work_at = now()->addSeconds(mt_rand(180, 600));
        }

        $currentWork->save();

        event(new \App\Events\HWM\WorkEvent($currentWork));

        return false;
    }
}
