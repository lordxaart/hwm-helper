<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LotParserLog extends Model
{
    /** @var string  */
    protected $dateFormat = 'Y-m-d H:i:s.u';

    /** @var array  */
    protected $fillable = [
        'parser_id',
        'parser_type',
        'lot_id',
        'state',
        'attempts',
        'server_ip',
        'errors',
        'time_execute',
    ];
}
