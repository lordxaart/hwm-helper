<?php

declare(strict_types=1);

namespace App\Http\Resources;

class LotCollection extends LotBaseCollection
{
    public $collects = LotResource::class;
}
