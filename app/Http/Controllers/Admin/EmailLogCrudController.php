<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Requests\EmailLogRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;

/**
 * Class Email-logCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class EmailLogCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;

    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel('App\Models\EmailLog');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/email-log');
        $this->crud->setEntityNameStrings('email-log', 'email-logs');
    }

    protected function setupListOperation()
    {
        $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(EmailLogRequest::class);

        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
