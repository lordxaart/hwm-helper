<?php

declare(strict_types=1);

namespace App\Listeners\HWM;

use App\Jobs\AdminNotify;
use App\Events\HWM\SetAddedEvent;
use App\Events\HWM\CharacterAddedEvent;
use App\Notifications\Admin\NewCharacter;
use App\Notifications\Admin\NewSet;
use App\Services\ActivityLogger\ActivityLogger;
use App\Services\ActivityLogger\ValueObjects\Activity;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewEntityListener implements ShouldQueue
{
    public function __construct(
        private readonly Dispatcher $dispatcher,
        private readonly ActivityLogger $activityLogger
    ) {
    }

    public function handleSet(SetAddedEvent $event): void
    {
        $this->activityLogger->log(
            new Activity('HWM_SET_CREATE', [
                'set' => $event->set->toArray(),
            ])
        );

        $this->dispatcher->dispatchSync(new AdminNotify(new NewSet($event->set)));
    }

    public function handleCharacter(CharacterAddedEvent $event): void
    {
        $this->activityLogger->log(
            new Activity('HWM_CHARACTER_CREATE', [
                'set' => $event->character->toArray(),
            ])
        );

        $this->dispatcher->dispatchSync(new AdminNotify(new NewCharacter($event->character)));

        // todo: think about this logics
//        if ($event->character->created_at->diffInSeconds(now()) > 30) {
//            $event->character->updateFromOriginal();
//        }
    }
}
