<?php

declare(strict_types=1);

namespace App\HWM\Parsers\Traits;

use App\HWM\Exceptions\ParserError;
use App\HWM\Exceptions\ParserErrors\UnexpectedPage;
use App\HWM\Exceptions\RequestErrors\InavailableProfile;
use App\HWM\Helpers\HwmHelper;
use App\HWM\Entities\Character;
use App\HWM\Repositories\FractionRepository;
use App\HWM\Repositories\GuildRepository;
use App\HWM\Repositories\ResourceRepository;
use Carbon\Carbon;

trait CharacterTrait
{
    public function isIncorrectLogin(): bool
    {
        $a = $this->find('a[href=login.php]');

        if ($a && str_contains($a->innerHtml, $this->getText('text_for_incorrect_login'))) {
            return true;
        }

        return false;
    }

    public function getPlivValue(): ?string
    {
        $input = $this->find('input[name=pliv]');

        return $input ? $input->getAttribute('value') : null;
    }

    public function isMovingInMap(): bool
    {
        return str_contains($this->getContent(), $this->getText('text_from_moving_map'));
    }

    public function isWar(): bool
    {
        return str_contains($this->getContent(), '<title>' . $this->getText('title_from_war') . '</title>');
    }

    public function isTavernGame(): bool
    {
        return str_contains($this->getContent(), '<title>' . $this->getText('title_from_tower_game') . '</title>');
    }

    public function hiddenCharacterByClanWar(): bool
    {
        return str_contains($this->getContent(), $this->getText('hero_clan_in_war'));
    }

    /**
     * @throws InavailableProfile
     * @throws ParserError
     * @throws UnexpectedPage
     */
    public function getCharacterInfo(string $hwmId = null): array
    {
        if ($this->locale !== RU) {
            throw new ParserError('Method supported only RU locale');
        }

        if (!$this->isCharacterPage()) {
            throw new UnexpectedPage('character');
        }

        if ($this->hiddenCharacterByClanWar()) {
            throw new InavailableProfile(InavailableProfile::CLAN_IN_WAR);
        }

        $level = intval(getTextBetweenValues($this->getContent(), 'Боевой уровень: ', '</b>'));
        if (!$level) {
            throw new ParserError(trans('errors.hwm.undefined_character'));
        }

        // Status
        $status = Character::STATUS_UNDEFINED;
        if ($block = getTextBetweenValues($this->getContent(), '<td align="right" valign="top">', '</td>')) {
            $text = html_entity_decode(strip_tags($block));
            if (
                str_contains($block, 'Персонаж сейчас в игре')
                || str_contains($block, 'Персонаж сейчас в бою')
                || str_contains($block, 'Персонаж сейчас за карточным столом')
            ) {
                $status = Character::STATUS_ONLINE;
            }
            if (str_contains($block, 'заблокирован')) {
                $status = Character::STATUS_BLOCKED;
            }
            if (str_contains($block, 'помещен в тюрьму') || str_contains($block, 'Заключенный')) {
                $status = Character::STATUS_PRISON;
            }

            if (str_contains($text, 'В последний раз')) {
                try {
                    $date = Carbon::createFromFormat(
                        'H:i d-m-y',
                        trim(preg_replace('/[^\d\s\-:]*/', '', $text)),
                        HwmHelper::HEROES_TZ
                    )->timezone(getDefaultTimezone());
                    if (now()->diffInDays($date) > 30) {
                        $status = Character::STATUS_INACTIVE;
                    } else {
                        $status = Character::STATUS_OFFLINE;
                    }
                } catch (\Exception) {
                    //
                }
            }
        }

        // Resources
        $resources = [];
        $resArray = app(ResourceRepository::class)->asArrayKeys();
        $items = $this->find('body center table tr td table:nth-child(1).wblight tr:nth-child(1) td:nth-child(2).wb[align=right] table tr td', null);
        foreach ($items as $node) {
            if (!$node->find('img', 0)) {
                $resources[array_shift($resArray)] = HwmHelper::clearIntValue(strip_tags($node->innerHtml));
            }
        }

        $guildArray = app(GuildRepository::class)->asArrayKeys();
        $fractionArray = app(FractionRepository::class)->asArrayKeys();

        // Fractions
        $regex = '/(\d+)\s\((\d+\.?\d*)\)/';
        $contentFractions = getTextBetweenValues($this->getContent(), '<td valign="top" class="wb">&nbsp;&nbsp;', '</font><br /><br />&nbsp;&nbsp;');
        $fractions = [];
        collect(explode('&nbsp;&nbsp;', $contentFractions))->each(function ($item) use ($regex, &$fractions, &$fractionArray) {
            preg_match($regex, $item, $matches);
            if (count($matches) === 3) {
                $fractions[array_shift($fractionArray)] = round(floatval($matches[2]), 2);
            }
        });
        // Guilds
        $contentGuilds = getTextBetweenValues($this->getContent(), '</font><br /><br />&nbsp;&nbsp;', '(<a href="#" onclick="a();');
        $guilds = [];

        collect(explode('&nbsp;&nbsp;', $contentGuilds))->each(function ($item) use ($regex, &$guilds, &$guildArray) {
            preg_match($regex, $item, $matches);
            if (count($matches) === 3) {
                $guilds[array_shift($guildArray)] = round(floatval($matches[2]), 2);
            }
        });

        $character = [
            'hwm_id' => intval(getTextBetweenValues($this->getContent(), 'pl_transfers.php?id=', '">')),
            'login' => str_replace('&nbsp;', ' ', (string)$this->find('h1')?->text()),
            'status' => $status,
            'level' => $level,
            'experience' => HwmHelper::clearIntValue(getTextBetweenValues($this->getContent(), '<b>Боевой уровень: ' . $level . '</b> (', ')')),
            'resources' => $resources,
            'guilds' => $guilds,
            'fractions' => $fractions,
        ];

        $this->validateData('character', $character);

        return $character;
    }

    /**
     * @throws ParserError
     */
    public function getCharacterIdFromHomePage(): int
    {
        if (!$this->isHomePage()) {
            throw new UnexpectedPage('home');
        }

        return intval(getTextBetweenValues($this->getContent(), 'pl_transfers.php?id=', '"'));
    }

    protected function isCharacterPage(): bool
    {
        return str_contains($this->getContent(), $this->getText('text_from_profile_page'));
    }

    public function searchCharacterNotFound(): bool
    {
        if ($this->locale === 'en') {
            return preg_match('/No character named\s.{3,100}\sfound!/', $this->getContent()) !== false;
        }

        return preg_match('/Персонаж\s.{3,100}не\sнайден!/', $this->getContent()) !== false;
    }
}
