<?php

declare(strict_types=1);

namespace App\HWM\Repositories;

use App\HWM\Helpers\HwmHelper;
use App\HWM\Repositories\Base\StorageRepository;

class GuildRepository extends StorageRepository
{
    protected array $extTable;

    public function __construct()
    {
        parent::__construct();

        $this->extTable = HwmHelper::getJson('exp_tables')['guilds'] ?? [];
    }

    public function expToNextLevel(string $guildKey, int $currentExp): float
    {
        if (isset($this->extTable[$guildKey])) {
            foreach ($this->extTable[$guildKey] as $exp) {
                if ($exp > $currentExp) {
                    return round($exp - $currentExp, 2);
                }
            }
        }

        return 0;
    }
}
