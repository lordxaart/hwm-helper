<?php

declare(strict_types=1);

namespace App\HWM\Exceptions\RequestErrors;

use App\HWM\Exceptions\RequestError;

class InavailableProfile extends RequestError
{
    public const IN_WAR = 'in_war';
    public const IN_CARD_GAME = 'in_card_game';
    public const MOVING_IN_MAP = 'moving_in_map';
    public const CLAN_IN_WAR = 'clan_in_war';

    public string $reason;

    public function __construct(string $reason)
    {
        $this->reason = $reason;

        parent::__construct(trans('errors.hwm.profile_unavailable_by_reason', ['reason' => trans('errors.hwm.reasons.' . $reason)]));
    }
}
