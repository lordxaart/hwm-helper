<?php

declare(strict_types=1);

namespace App\Notifications\User;

use App\Core\Enums\Queue;
use App\Services\Notifications\Channels\Telegram\Entities\TelegramMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use App\Services\Notifications\Enums\Channel;

class InviteToPublicChannel extends Notification implements ShouldQueue
{
    use Queueable;

    public function __construct(public string $inviteLink)
    {
        $this->queue = Queue::NOTIFICATIONS->value;
    }

    public function via(): array
    {
        return [
            Channel::TELEGRAM->getLaravelChannelName(),
        ];
    }

    public function toTelegram(): TelegramMessage
    {
        $message = new TelegramMessage();
        $message->line('');
        $message->line(__('notifications.telegram_invite_link.important') . '!!!');
        $message->line('');
        $message->line(__('notifications.telegram_invite_link.p'));
        $message->line(__('notifications.telegram_invite_link.thanks'));
        $message->button(__('notifications.telegram_invite_link.join'), $this->inviteLink);

        return $message;
    }
}
