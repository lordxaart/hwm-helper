<?php

declare(strict_types=1);

namespace App\Services\LotFilter;

use App\Core\ValueObject\UserId;
use App\Exceptions\User\UserFilterLimitExceeded;
use App\Models\LotFilter;
use App\Models\LotFilterNotifyChannel;
use App\Services\LotFilter\Events\LotFilterCreated;
use App\Services\LotFilter\Events\LotFilterDeleted;
use App\Services\LotFilter\Events\LotFilterUpdated;
use App\Services\LotFilter\Repositories\LotFilterNotifyChannelRepository;
use App\Services\LotFilter\ValueObjects\LotFilterRequest;
use App\Services\LotFilter\ValueObjects\NotifyChannels;
use App\Services\Notifications\Enums\Channel;
use App\Services\UserFeatures\UserFeatureService;
use Illuminate\Contracts\Events\Dispatcher;

class LotFiltersService
{
    public function __construct(
        private readonly Dispatcher $eventDispatcher,
        private readonly UserFeatureService $userFeatureService,
        private readonly LotFilterNotifyChannelRepository $lotFilterNotifyChannelRepository,
    ) {
        //
    }

    /**
     * @throws UserFilterLimitExceeded
     */
    public function createFilter(LotFilterRequest $request): LotFilter
    {
        $this->checkLimitMaxFilters($request->userId);

        $lotFilter = LotFilter::create([
            'user_id' => $request->userId->value,
            'lot_type' => $request->lotType->value,
            'entity_type' => $request->entityType->value,
            'entity_id' => $request->entityId,
            'terms' => $request->terms,
            'is_active' => $request->isActive,
        ]);

        $this->eventDispatcher->dispatch(new LotFilterCreated($lotFilter));

        return $lotFilter;
    }

    public function updateFilter(LotFilter $lotFilter, LotFilterRequest $request): LotFilter
    {
        // if toggle to active state and check for max active filters
        if (!$lotFilter->is_active && $request->isActive) {
            $this->checkLimitMaxFilters($request->userId);
        }

        $lotFilter->update([
            'terms' => $request->terms,
            'is_active' => $request->isActive,
        ]);

        if ($lotFilter->isDirty()) {
            $lotFilter->save();

            $this->eventDispatcher->dispatch(new LotFilterUpdated($lotFilter));

            return $lotFilter;
        }

        return $lotFilter;
    }

    public function deleteFilter(LotFilter $lotFilter): bool
    {
        $lotFilter->delete();

        $this->eventDispatcher->dispatch(new LotFilterDeleted($lotFilter->id, new UserId($lotFilter->user_id)));

        return true;
    }

    public function getNotificationSettingsByFiltersIds(array $filtersIds): array
    {
        $notifyCollection = $this->lotFilterNotifyChannelRepository
            ->getMapCollectionByFilterIds($filtersIds)
            ->groupBy('lot_filter_id')
            ->all();

        $result = [];

        foreach ($filtersIds as $filterId) {
            $notifyChannels = new NotifyChannels();

            if (!empty($notifyCollection[$filterId])) {
                /** @var LotFilterNotifyChannel $notifyChannel */
                foreach ($notifyCollection[$filterId] as $notifyChannel) {
                    $notifyChannels->setChannelAvailability(Channel::from($notifyChannel->notify_channel), $notifyChannel->is_available);
                }
            }

            $result[$filterId] = $notifyChannels->getChannels();
        }

        return $result;
    }

    /**
     * @throws UserFilterLimitExceeded
     */
    private function checkLimitMaxFilters(UserId $userId): void
    {
        $query = LotFilter::query()->where('user_id', $userId->value);
        $maxMonitoredFilters = $this
            ->userFeatureService
            ->getUserFeatures($userId)
            ->max_monitored_filters;

        if ($query->count() >= $maxMonitoredFilters) {
            throw new UserFilterLimitExceeded($userId, $maxMonitoredFilters);
        }
    }
}
