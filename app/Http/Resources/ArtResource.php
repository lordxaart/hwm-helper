<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Http\Resources\Core\ApiResource;
use App\Models\Objects\ArtImage;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

/**
 * @property \App\Models\Art $resource
 */
class ArtResource extends ApiResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return $this->transformByRequest([
            'hwmId' => $this->hwm_id,
            'title' => $this->title,
            'link' => $this->getLink(),
            'image' => $this->original_image,
            'images' => array_map(function (ArtImage $image) {
                return Arr::only($image->toArray(), ['fullUrl']);
            }, $this->images),
            'repair' => $this->repair,
            'strength' => $this->strength,
            'shopPrice' => $this->shop_price,
            'pricePerFight' => $this->price_per_fight,
            'fromShop' => $this->from_shop,
            'asFromShop' => $this->as_from_shop,
            'categoryId' => $this->category_id,
            'marketCategoryId' => $this->market_category_id,
            'setId' => $this->set_id,
            'createdAt' => $this->created_at?->format(config('app.date_format')),
            'updatedAt' => $this->updated_at?->format(config('app.date_format')),
        ], $request);
    }
}
