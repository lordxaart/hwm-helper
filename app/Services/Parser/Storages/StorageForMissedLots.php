<?php

declare(strict_types=1);

namespace App\Services\Parser\Storages;

use Illuminate\Support\Facades\Redis;

class StorageForMissedLots
{
    public const STORAGE_CACHE_KEY = 'missed_parsing_lot_ids';

    public function add(string $id): bool
    {
        /** @phpstan-ignore-next-line */
        return (bool) Redis::sadd(self::STORAGE_CACHE_KEY, (array)$id);
    }

    public function count(): int
    {
        /** @phpstan-ignore-next-line */
        return Redis::scard(self::STORAGE_CACHE_KEY);
    }

    public function pop(): int
    {
        /** @phpstan-ignore-next-line */
        return (int) Redis::spop(self::STORAGE_CACHE_KEY);
    }
}
