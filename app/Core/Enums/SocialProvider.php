<?php

declare(strict_types=1);

namespace App\Core\Enums;

enum SocialProvider: string
{
    case FB = 'facebook';
    case GOOGLE = 'google';
    case VK = 'vkontakte';

    public function getTitle(): string
    {
        return match ($this) {
            self::FB => 'Facebook',
            self::GOOGLE => 'Google',
            self::VK => 'Vkontakte',
        };
    }
}
