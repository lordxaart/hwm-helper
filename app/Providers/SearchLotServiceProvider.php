<?php

declare(strict_types=1);

namespace App\Providers;

use App\Components\Elastic\ElasticClient;
use App\Services\SearchLots\Repositories\AggsConverter;
use App\Services\SearchLots\Repositories\Converter;
use App\Services\SearchLots\Repositories\ElasticLotRepository;
use App\Services\SearchLots\Repositories\LotRepository;
use App\Services\SearchLots\SearchLotService;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class SearchLotServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(LotRepository::class, function (Application $app) {
            return new ElasticLotRepository(
                $app->get(ElasticClient::class),
                new Converter(),
                new AggsConverter(),
            );
        });

        $this->app->singleton(SearchLotService::class, function (Application $app) {
            return new SearchLotService($app->get(LotRepository::class));
        });
    }
}
