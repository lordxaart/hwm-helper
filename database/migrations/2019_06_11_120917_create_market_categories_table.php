<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('art_market_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hwm_id', config('hwm.rules.max_hwm_id_length'))->unique();
            $table->string('title', config('hwm.rules.max_hwm_id_length'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('market_categories');
    }
}
