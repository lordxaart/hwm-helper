<?php

declare(strict_types=1);

namespace App\Notifications\Admin;

use App\Core\Enums\Queue;
use App\Services\Notifications\Channels\Telegram\Entities\TelegramMessage;
use App\Services\Notifications\Enums\Channel;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class Notify extends Notification implements ShouldQueue
{
    use Queueable;

    public string $message;

    public string $type;

    public function __construct(string $message, string $type = 'notice')
    {
        $this->queue = Queue::NOTIFICATIONS->value;

        $this->message = $message;

        $this->type = $type;
    }

    public function via(): array
    {
        return ['database', Channel::TELEGRAM->getLaravelChannelName()];
    }

    public function toTelegram(User $user): TelegramMessage
    {
        return TelegramMessage::create()
            ->setTypeContent($this->type)
            ->content($this->message);
    }

    public function toSlack(): SlackMessage
    {
        if ($this->type === 'error') {
            return (new SlackMessage())
                ->from(config('app.name'), ':information_source:')
                ->error()
                ->content($this->message);
        } else {
            return (new SlackMessage())
                ->from(config('app.name'), ':information_source:')
                ->success()
                ->content($this->message);
        }
    }

    public function toArray(): array
    {
        return [
            'message' => $this->message,
            'type' => $this->type
        ];
    }
}
