<?php

declare(strict_types=1);

namespace App\Listeners\User;

use App\Events\User\DisconnectedTelegramEvent as Event;
use App\Jobs\AdminNotify;
use App\Notifications\Admin\Notify;
use App\Notifications\User\DetectTelegramChatBlocked;
use App\Notifications\User\TelegramNotify;
use App\Services\ActivityLogger\ActivityLogger;
use App\Services\ActivityLogger\ValueObjects\Activity;
use App\Services\Notifications\Enums\Channel;
use Illuminate\Support\Facades\Notification;

class DisconnectTelegramListener
{
    public function __construct(private readonly ActivityLogger $activityLogger)
    {
    }

    public function handle(Event $event): void
    {
        $this->activityLogger->log(
            Activity::action('USER_TELEGRAM_DISCONNECT')->causedBy($event->user)->context([
                'telegramAccount' => $event->telegramAccount,
            ])
        );

        if ($event->telegramAccount->is_blocked) {
            $event->user->notify(new DetectTelegramChatBlocked());

            AdminNotify::dispatchSync(
                new Notify(sprintf('Telegram bot disconnected, channel forbbiden, user [%s]', $event->user->email))
            );
        } else {
            $telegramNotify = new TelegramNotify(
                trans('app.user.succes_delete_telegram_account', [], $event->user->preferredLocale())
            );
            Notification::route(Channel::TELEGRAM->getLaravelChannelName(), $event->telegramAccount->chat_id)->notify(
                $telegramNotify
            );

            AdminNotify::dispatchSync(new Notify(trans('User ' . $event->user->email . ' disconnected Telegram bot')));
        }
    }
}
