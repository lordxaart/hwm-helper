<?php

declare(strict_types=1);

namespace App\Http\Resources\Core;

use App\Components\Pagination\LimitOffsetPaginator;
use Illuminate\Http\Resources\Json\ResourceResponse;
use Illuminate\Support\Arr;

/**
 * Copy from Illuminate\Http\Resources\Json\PaginatedResponse
 *
 * @property LimitOffsetPaginator $paginator
 */
class PaginatedResponse extends ResourceResponse
{
    /**
     * Create an HTTP response that represents the object.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function toResponse($request)
    {
        return tap(
            response()->json(
                $this->wrap(
                    $this->resource->resolve($request),
                    array_merge_recursive(
                        $this->getMetaPagination(),
                        $this->resource->with($request),
                        $this->resource->additional
                    )
                ),
                $this->calculateStatus(),
                [],
                $this->resource->jsonOptions()
            ),
            function ($response) use ($request) {
                $response->original = $this->resource->resource->map(function ($item) {
                    return is_array($item) ? Arr::get($item, 'resource') : $item->resource;
                });

                $this->resource->withResponse($request, $response);
            }
        );
    }

    protected function getMetaPagination(): array
    {
        /** @var LimitOffsetPaginator $paginator */
        $paginator = $this->resource->resource;

        return [
            'meta' => [
                'pagination' => [
                    'count' => $paginator->count(),
                    'hasMore' => $paginator->hasMorePages(),
                    'limit' => $paginator->getLimit(),
                    'offset' => $paginator->getOffset(),
                    'nextUrl' => $paginator->nextPageUrl(),
                    'prevUrl' => $paginator->previousPageUrl(),
                ]
            ]
        ];
    }
}
