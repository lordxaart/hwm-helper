<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTablesForTransableFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            Schema::table('arts', function (Blueprint $table) {
                $table->dropIndex(['title']);
            });
        } catch (\Exception $e) {
            //
        }


        Schema::table('arts', function (Blueprint $table) {
            $table->text('title')->change();
            $table->text('description')->change();
        });
        Schema::table('art_categories', function (Blueprint $table) {
            $table->text('title')->change();
        });
        Schema::table('art_market_categories', function (Blueprint $table) {
            $table->text('title')->change();
        });
        Schema::table('art_sets', function (Blueprint $table) {
            $table->text('title')->change();
        });

        Schema::table('arts', function (Blueprint $table) {
            $table->json('title')->change();
            $table->json('description')->change();
        });
        Schema::table('art_categories', function (Blueprint $table) {
            $table->json('title')->change();
        });
        Schema::table('art_market_categories', function (Blueprint $table) {
            $table->json('title')->change();
        });
        Schema::table('art_sets', function (Blueprint $table) {
            $table->json('title')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
