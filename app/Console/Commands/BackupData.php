<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\Art;
use App\Models\ArtCategory;
use App\Models\MarketCategory;
use App\Models\ArtSet;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

/**
 * @deprecated
 */
class BackupData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup arts, categories to json';

    public function handle(): void
    {
        $this->backupArts();
        $this->backupCategories();
        $this->backupSets();
    }

    protected function backupArts(): void
    {
        $arts = Art::all();

        $this->saveAsJson($arts, 'arts.json');
    }

    protected function backupCategories(): void
    {
        $cats = ArtCategory::get();
        $this->saveAsJson($cats, 'categories.json');

        $cats = MarketCategory::get();
        $this->saveAsJson($cats, 'market_categories.json');
    }

    protected function backupSets(): void
    {
        $sets = ArtSet::get();
        $this->saveAsJson($sets, 'sets.json');
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection $data
     * @param string $path
     */
    private function saveAsJson(Collection $data, string $path): void
    {
        $data = collect($data->toArray())->map(function ($item) {
            unset($item['id']);
            unset($item['created_at']);
            unset($item['updated_at']);

            return $item;
        });

        file_put_contents(hwm_storage_path($path), json_encode($data->toArray(), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
    }
}
