<?php

declare(strict_types=1);

namespace App\Components\Validators\Rules\Lot;

use App\HWM\Enums\EntityType;
use Illuminate\Contracts\Validation\Rule;

class IsEntityType implements Rule
{
    /**
     * @inheritDoc
     */
    public function passes($attribute, $value)
    {
        return EntityType::tryFrom((string) $value) !== null;
    }

    /**
     * @inheritDoc
     */
    public function message()
    {
        return 'Is not valid lot type. Allowed values: ' . implode(',', enumToArrayOfValues(EntityType::cases()));
    }
}
