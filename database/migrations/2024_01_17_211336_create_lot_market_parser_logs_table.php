<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lot_market_parser_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('entity_type');
            $table->string('entity_id')->nullable();
            $table->unsignedInteger('lots_count');
            $table->unsignedInteger('new_lots_count');
            $table->tinyInteger('attempts')->default(0);
            $table->string('time_execute')->nullable();
            $table->text('errors')->nullable();

            $table->timestamps(6);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lot_parser_logs');
    }
};
