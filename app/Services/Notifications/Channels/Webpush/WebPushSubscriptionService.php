<?php

declare(strict_types=1);

namespace App\Services\Notifications\Channels\Webpush;

use App\Core\ValueObject\UserId;
use Illuminate\Database\Eloquent\Collection;

class WebPushSubscriptionService
{
    public function getAllSubscriptions(UserId $userId): Collection
    {
        return PushSubscriptionModel::query()->where('user_id', $userId->value)->get();
    }

    /**
     * Update (or create) subscription.
     */
    public function updateOrCreateSubscription(
        UserId $userId,
        string $endpoint,
        string $key = null,
        string $token = null,
        string $contentEncoding = null
    ): PushSubscriptionModel {
        $subscription = PushSubscriptionModel::findByEndpoint($endpoint);

        if ($subscription && $subscription->isUserOwnSubscription($userId)) {
            $subscription->public_key = $key;
            $subscription->auth_token = $token;
            $subscription->content_encoding = $contentEncoding;
            $subscription->save();

            return $subscription;
        }

        if ($subscription && !$subscription->isUserOwnSubscription($userId)) {
            $subscription->delete();
        }

        return PushSubscriptionModel::create([
            'user_id' => $userId->value,
            'endpoint' => $endpoint,
            'public_key' => $key,
            'auth_token' => $token,
            'content_encoding' => $contentEncoding,
        ]);
    }

    public function deleteSubscription(UserId $userId, string $endpoint): void
    {
        PushSubscriptionModel::query()
            ->where('user_Id', $userId->value)
            ->where('endpoint', $endpoint)
            ->delete();
    }
}