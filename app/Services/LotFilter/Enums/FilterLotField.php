<?php

declare(strict_types=1);

namespace App\Services\LotFilter\Enums;

enum FilterLotField: string
{
    // case LOT_TYPE = 'lot_type';
    // case ENTITY_TYPE = 'entity_type';
    // case ENTITY_ID = 'entity_id';
    case CRAFT = 'craft';
    case C_STRENGTH = 'current_strength';
    case B_STRENGTH = 'base_strength';
    case PRICE = 'price';
    case PPF = 'price_per_fight';
    case BLITZ_PRICE = 'blitz_price';
    case SELLER_ID = 'seller_id';
    case SELLER_NAME = 'seller_name';
}
