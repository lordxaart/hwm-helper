<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\TelegramAccount;
use App\Models\User;
use App\Services\Telegram\UserTelegramService;

class NotifyUsersAboutPublicChannel extends Command
{
    protected $signature = 'users:notify:public-channel';

    /** @var string  */
    protected $description = 'Clear old and big logs files, old records from db';

    public function handle(): void
    {
        /** @var UserTelegramService $service */
        $service = app(UserTelegramService::class);
        TelegramAccount::chunk(100, function ($items) use ($service) {
            /** @var TelegramAccount $item */
            foreach ($items as $item) {
                $user = User::find($item->user_id);
                if (!empty($item->user_data['notified_about_public_channel'])) {
                    $this->warn("User {$user->email} already notified");
                    continue;
                }
                if ($service->userIsConnectedToPublicChannel($user)) {
                    $this->warn("User {$user->email} already member of public channel");
                    continue;
                }
                if (!$this->confirm("Do you want send invite link to user {$user->email}", true)) {
                    continue;
                }
                $service->sendInviteToPublicChannel($user);
                $this->info("Send invite link " . $user->email);
            }
        });
    }
}
