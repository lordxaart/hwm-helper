<?php

declare(strict_types=1);

namespace App\Services\Notifications\Channels\Telegram\Exceptions;

use Exception;
use GuzzleHttp\Exception\ClientException;
use JsonException;

/**
 * Class CouldNotSendNotification.
 */
final class CouldNotSendNotification extends Exception
{
    public const TOO_MANY_REQUESTS_CODE = 429;
    public const CACHE_KEY = 'telegram_too_many_requests';
    /**
     * Thrown when there's a bad request and an error is responded.
     *
     * @param  ClientException  $exception
     * @return self
     *
     * @throws JsonException
     */
    public static function telegramRespondedWithAnError(ClientException $exception): self
    {
        if (! $exception->hasResponse()) {
            return new self('Telegram responded with an error but no response body found');
        }

        $statusCode = $exception->getResponse()->getStatusCode();

        $result = json_decode($exception->getResponse()->getBody()->getContents(), false, 512, JSON_THROW_ON_ERROR);
        $description = $result->description ?? 'no description given';

        if ($statusCode === self::TOO_MANY_REQUESTS_CODE) {
            \Cache::set(self::CACHE_KEY, intval($description), 30);
        }

        return new self("Telegram responded with an error `{$statusCode} - {$description}`", 0, $exception);
    }

    /**
     * Thrown when there's no bot token provided.
     *
     * @param  string  $message
     * @return self
     */
    public static function telegramBotTokenNotProvided(string $message): self
    {
        return new self($message);
    }

    /**
     * Thrown when we're unable to communicate with Telegram.
     *
     * @param  string  $message
     * @return self
     */
    public static function couldNotCommunicateWithTelegram(string $message): self
    {
        return new self("The communication with Telegram failed. `{$message}`");
    }
}
