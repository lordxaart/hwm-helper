<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V1;

use App\Services\Notifications\Enums\Channel;
use App\Jobs\AdminNotify;
use App\Jobs\TestJob;
use App\Models\Lot;
use App\Notifications\Test;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redis;
use Spatie\Backup\BackupDestination\BackupDestination;
use Spatie\Backup\BackupDestination\BackupDestinationFactory;

class StatusController extends ApiController
{
    public int $criticalDiffInSecondsForQueue = 30;

    public int $criticalDiffInMinutesForParser = 5;

    public function service(Request $request, string $service)
    {
        if (!method_exists($this, $service)) {
            abort(404);
        }

        return $this->$service($request);
    }

    public function web()
    {
        return isLocalEnv() ? $this->success() : $this->request(route('calculator'));
    }

    public function files()
    {
        $files = collect([
            public_path('sitemap.xml') => true,
            public_path('robots.txt') => true,
            public_path('manifest.json') => true,
            public_path('css/app.css') => true,
            public_path('css/critical.css') => true,
            public_path('css/mail.css') => true,
            public_path('css/admin.css') => true,
            public_path('js/app/js/app.js') => true,
            public_path('storage/sprite_small.css') => true,
            public_path('storage/sprite_small.png') => true,
        ]);

        $files = $files->map(function ($status, $file) {
            if (!file_exists($file)) {
                $status = false;
            }
            return $status;
        });

        if (
            $files->filter(function ($item) {
                return $item === false;
            })->count()
        ) {
            return $this->error('Missed files: ' . $files->filter(function ($status) {
                return !$status;
            })->keys()->map(function ($file) {
                return str_replace(base_path(), '', $file);
            })->implode(', '));
        }

        return $this->success('Exists files: ' . $files->filter(function ($status) {
            return $status;
        })->keys()->map(function ($file) {
            return str_replace(base_path(), '', $file);
        })->implode(', '));
    }

    public function api()
    {
        return $this->request(isLocalEnv() ? 'http://127.0.0.1/api' : url('/api'));
    }

    public function botServer()
    {
        return $this->request('http://127.0.0.1:3000');
    }

    public function websocketsServer()
    {
        try {
            fsockopen(
                (string)config('broadcasting.connections.pusher.options.host'),
                (int)config('broadcasting.connections.pusher.options.port'),
            );
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }

        return $this->success();
    }

    public function parser()
    {
        $lastParser = Lot::whereLotId(Lot::max('lot_id'))->first();

        if (!$lastParser) {
            return $this->error('Not found any parser records');
        }

        if ($lastParser->created_at->lt(now()->subMinutes(5))) {
            return $this->error('Last parse at ' . $lastParser->started_at->diffForHumans());
        }

        return $this->success('Last parse art "' . $lastParser->entity_title . '" at ' . $lastParser->started_at->diffForHumans());
    }

    public function marketLots()
    {
        $count = Lot::getCountMissedLots();

        return $count ? $this->error("Find $count missed lot ids") : $this->success('Not found missed lot ids');
    }

    public function redis()
    {
        try {
            Redis::connection()->client()->get('key');

            return $this->success();
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
    }

    public function cron()
    {
        if (!\Cache::get('cron_check')) {
            return $this->error('Not found cron check value');
        }

        $lastCronJob = trim((string)\Cache::get('cron_check'));
        $lastCronTime = Carbon::parse($lastCronJob);

        if (now()->diffInMinutes($lastCronTime) > 2) {
            return $this->error('Last cron job ' . $lastCronTime->format('H:i:s'));
        }

        return $this->success('Last cron job ' . $lastCronTime->format('H:i:s'));
    }

    public function queue()
    {
        if (config('queue.default') === 'sync') {
            return $this->success('Connection: ' . config('queue.default'));
        }

        set_time_limit($this->criticalDiffInSecondsForQueue + 10);

        $id = uniqid() . '_check_queue';
        dispatch(new TestJob($id));
        $queue = app()->make('queue.connection');
        $size = $queue->size();

        if (!$size) {
            return $this->error('Not found size of default queue');
        }

        $startTime = now();
        while (!\Cache::has($id)) {
            sleep(1);

            if (now()->diffInSeconds($startTime) >= $this->criticalDiffInSecondsForQueue) {
                return $this->error('Dispatch job more than 60 seconds');
            }
        }

        \Cache::delete($id);

        return $this->success('Connection: ' . config('queue.default'));
    }

    public function notify(Request $request)
    {
        $channel = Channel::tryFrom($request->get('channel'));

        if (!$channel) {
            return $this->error('Unsupported notification channel ' . $request->get('channel'));
        }

        $notification = new Test([$channel->value]);

        if ($channel === Channel::TELEGRAM) {
            AdminNotify::dispatch($notification);
        }

        return $this->success('Success send notify via ' . $channel);
    }

    public function backups()
    {
        /** @var BackupDestination[] $backupDestinations */
        $backupDestinations = BackupDestinationFactory::createFromArray(config('backup.backup'));

        if (!count($backupDestinations)) {
            return $this->error('Not found any backup destinations');
        }

        $message = '';
        foreach ($backupDestinations as $backupDestination) {
            $backup = $backupDestination->newestBackup();
            $disk = '[' . $backupDestination->diskName() . ']';
            if (!$backup) {
                return $this->error('Not found backups for ' . $disk);
            }

            if ($backup->date()->diffInDays() >= 1) {
                return $this->error($disk . ' Last backup ' . $backup->date()->format('Y-m-d H:i'));
            }

            $message .= $disk . ' Last backup ' . $backup->date()->format('Y-m-d H:i') . '. ' . PHP_EOL;
        }

        return $this->success($message);
    }

    protected function request(string $url)
    {
        try {
            $res = Http::get($url);
        } catch (\Exception $e) {
            return $this->error('Error Request: ' . $e->getMessage());
        }

        if ($res->status() === 200) {
            return $this->success();
        }

        return $this->error('Response code for main page ' . $res->status());
    }

    protected function success(string $message = '')
    {
        return $this->response([
            'success' => true,
            'message' => $message,
        ]);
    }

    protected function error(string $message = '')
    {
        return $this->response([
            'success' => false,
            'message' => $message,
        ], 400);
    }
}
