const beforeEach = (app, to, from, next) => {
    if (!from.matched.length) {
        window.location.href = to.path;
    }

    app.loadingPage = true;

    if (to.matched.some((record) => record.meta.auth)) {
        if (!app.user) {
            window.location.href = '/login?only_auth=1';
        } else if (!app.user.email_verified_at) {
            // redirect to must verified page
            window.location.href = '/email/verify';
        }

        next();
    } else if (to.matched.some((record) => record.meta.guest)) {
        if (!app.user) {
            next();
        } else {
            next({ name: 'profile.index' });
        }
    }

    next();
};

export default beforeEach;
