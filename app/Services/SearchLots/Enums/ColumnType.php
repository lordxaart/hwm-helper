<?php

declare(strict_types=1);

namespace App\Services\SearchLots\Enums;

enum ColumnType: string
{
    case EQUAL = 'equal';
    case ONE_FROM_ARRAY = 'one_from_array';
    case EXISTS = 'exists';
    case TEXT = 'text';
    case NUMBER = 'number';
    case DATE = 'date';
}
