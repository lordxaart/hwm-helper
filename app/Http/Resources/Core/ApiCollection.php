<?php

declare(strict_types=1);

namespace App\Http\Resources\Core;

use App\Components\Pagination\LimitOffsetPaginator;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\PaginatedResourceResponse;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ApiCollection extends ResourceCollection
{
    public readonly array $includes;

    public function __construct($resource)
    {
        parent::__construct($resource);

        if ($resource instanceof LimitOffsetPaginator) {
            $this->resource = new LimitOffsetPaginator(
                $this->collection,
                $resource->getLimit(),
                $resource->getOffset()
            );
        }

        $this->includes = $this->parseIncludes();
    }

    /**
     * Create an HTTP response that represents the object.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function toResponse($request)
    {
        if ($this->resource instanceof LimitOffsetPaginator) {
            return $this->preparePaginatedResponse($request);
        }

        return parent::toResponse($request);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function preparePaginatedResponse($request)
    {
        if ($this->preserveAllQueryParameters) {
            $this->resource->appends($request->query());
        } elseif (!is_null($this->queryParameters)) {
            $this->resource->appends($this->queryParameters);
        }

        if ($this->resource instanceof LimitOffsetPaginator) {
            return (new PaginatedResponse($this))->toResponse($request);
        }

        return (new PaginatedResourceResponse($this))->toResponse($request);
    }

    protected function parseIncludes(Request $request = null): array
    {
        $request = $request ?: Container::getInstance()->make(Request::class);

        return array_filter(explode(',', $request->query(ApiResource::QUERY_INCLUDES_KEY, '')));
    }
}
