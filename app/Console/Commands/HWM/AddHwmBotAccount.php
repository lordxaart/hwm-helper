<?php

declare(strict_types=1);

namespace App\Console\Commands\HWM;

use App\Console\Commands\Command;
use App\Models\HwmBotAccount;

class AddHwmBotAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hwm:add:hwm-bot';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add HWM Bot Account for parsers';

    public function handle(): void
    {
        $login = $this->ask('Login');
        if (!$login) {
            $this->error('[login] can`t be empty');
            return;
        }
        if (HwmBotAccount::query()->where('login', $login)->exists()) {
            $this->error("login [$login] already exists in DB");
            return;
        }

        $password = $this->ask('Password');
        if (!$password) {
            $this->error("[password] can`t be empty");
            return;
        }

        $email = $this->ask('Email (can be empty)');

        $targets = [
            'all' => HwmBotAccount::TARGET_ALL,
            'browser' => HwmBotAccount::TARGET_WEB,
            'cli' => HwmBotAccount::TARGET_CLI,
        ];
        $targetStr = $this->choice('Target', ['all', 'browser', 'cli'], 'all');
        $target = $targets[$targetStr];

        HwmBotAccount::create([
            'login' => $login,
            'password' => $password,
            'email' => $email,
            'target' => $target,
        ]);

        $this->info("Account [$login] successfully created");
    }
}
