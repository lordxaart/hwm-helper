<?php

declare(strict_types=1);

namespace App\Components\Validators\Rules;

use App\Services\LotFilter\ValueObjects\FilterTerm;
use Illuminate\Contracts\Validation\Rule;

class ValidFilterValue implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        try {
            if (is_array($value)) {
                foreach ($value as $item) {
                    if (is_array($item)) {
                        FilterTerm::parseItemFromArray($item);
                    } else {
                        FilterTerm::parseItemFromString($item);
                    }
                }
            } else {
                FilterTerm::parserCollectionFromString($value);
            }
        } catch (\InvalidArgumentException) {
            return false;
        }


        return true;
    }

    /**
     * Get the validation error message.
     */
    public function message(): string
    {
        return trans('validation.monitored_filter');
    }
}
