<?php

declare(strict_types=1);

namespace App\Events\HWM;

use App\Models\Work;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;

class WorkEvent
{
    use Dispatchable;
    use InteractsWithSockets;

    public Work $work;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Work $work)
    {
        $this->work = $work;
    }
}
