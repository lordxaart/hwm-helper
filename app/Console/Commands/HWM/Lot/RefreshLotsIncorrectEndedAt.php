<?php

declare(strict_types=1);

namespace App\Console\Commands\HWM\Lot;

use App\Console\Commands\Command;
use App\Models\Lot;
use App\Models\LotOperation;

class RefreshLotsIncorrectEndedAt extends Command
{
    public $signature = 'hwm:lots:refresh:ended_at';

    public $description = 'Many records parsed with ended_at as current date (fixed for new lots). Set from operation end';

    public function handle(): void
    {
        $query = Lot::query()
            ->select('lot_id')
            ->whereRaw('DATEDIFF(ended_at, started_at) > 3');

        $this->output->progressStart();

        $query->chunk(1000, function ($items) {
            /** @var Lot $item */
            foreach ($items as $item) {
                $operation = LotOperation::where([
                    'lot_id' => $item->lot_id,
                    'type' => \App\HWM\Objects\LotOperation::OPERATION_END,
                ])->first();

                if (!$operation) {
                    $this->error("Can not find operation {$item->lot_id}");
                    continue;
                }
                $item->timestamps = false;
                $item->update(['ended_at' => $operation->time]);

                $this->output->progressAdvance();
            }
        });

        $this->output->progressFinish();
    }
}
