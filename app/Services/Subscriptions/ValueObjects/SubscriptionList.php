<?php

declare(strict_types=1);

namespace App\Services\Subscriptions\ValueObjects;

use App\Services\Subscriptions\Entities\Sub;
use InvalidArgumentException;

class SubscriptionList
{
    /** @var Sub[] */
    public readonly array $subscriptions;

    public function __construct(array $subscriptions)
    {
        foreach ($subscriptions as $sub) {
            if (!($sub instanceof Sub)) {
                throw new InvalidArgumentException(
                    sprintf('Each element of array must be instance of %s, given %s', Sub::class, gettype($sub))
                );
            }
        }
        $this->subscriptions = $subscriptions;
    }
}
