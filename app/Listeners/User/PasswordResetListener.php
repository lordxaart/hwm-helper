<?php

declare(strict_types=1);

namespace App\Listeners\User;

use App\Services\ActivityLogger\ActivityLogger;
use App\Services\ActivityLogger\ValueObjects\Activity;
use Illuminate\Auth\Events\PasswordReset as Event;
use Illuminate\Contracts\Support\Arrayable;

class PasswordResetListener
{
    public function __construct(private readonly ActivityLogger $activityLogger)
    {
    }

    public function handle(Event $event): void
    {
        $this->activityLogger->log(
            Activity::action('USER_PASSWORD_RESET')
                ->causedBy($event->user)
        );
    }
}
