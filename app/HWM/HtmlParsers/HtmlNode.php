<?php

declare(strict_types=1);

namespace App\HWM\HtmlParsers;

use App\HWM\HtmlParsers\Interfaces\HtmlNodeInterface;
use PHPHtmlParser\Dom;
use PHPHtmlParser\Exceptions\ChildNotFoundException;
use stringEncode\Encode;

class HtmlNode implements HtmlNodeInterface
{
    private Dom\AbstractNode $node;

    public function __construct(string $html)
    {
        // initialize dom object
        $dom = new Dom();

        try {
            $dom->loadStr($html);
            $this->node = $dom->root;
            $firstNode = $dom->firstChild();

            if ($firstNode instanceof Dom\HtmlNode && $firstNode->tag->name() !== 'head') {
                $this->node = $firstNode;
            }

            $encoding = new Encode();
            $encoding->from('UTF-8');
            $this->node->propagateEncoding($encoding);
        } catch (\Exception $e) {
            throw new HtmlParserException($e);
        }
    }

    public static function loadFromString(string $html): HtmlNodeInterface
    {
        return new self($html);
    }

    public function find(string $selector): ?HtmlNodeInterface
    {
        try {
            $node = $this->node->find($selector, 0);

            return $node ? $this->wrapForDomNode($node) : null;
        } catch (ChildNotFoundException) {
            return null;
        }
    }

    public function findAll(string $selector): array
    {
        try {
            $nodes = $this->node->find($selector);
        } catch (ChildNotFoundException) {
            return [];
        }

        if (!$nodes) {
            return [];
        }

        return array_map(function (Dom\AbstractNode $node) {
            return $this->wrapForDomNode($node);
        }, $nodes->toArray());
    }

    public function getNextSibling(): ?HtmlNodeInterface
    {
        try {
            $node = $this->node->nextSibling();

            return $this->wrapForDomNode($node);
        } catch (\Exception) {
            return null;
        }
    }

    public function getFirstChild(): ?HtmlNodeInterface
    {
        // children available only in InnerNode
        if (! $this->node instanceof Dom\InnerNode) {
            return null;
        }

        try {
            $child = $this->node->firstChild();

            return $this->wrapForDomNode($child);
        } catch (\Exception) {
            return null;
        }
    }

    public function getAttribute(string $attribute): ?string
    {
        return $this->node->getAttribute($attribute);
    }

    public function asHtml(): string
    {
        return $this->node->outerHtml();
    }

    public function asString(): string
    {
        return strip_tags($this->node->innerHtml());
    }

    private function wrapForDomNode(Dom\AbstractNode $node): self
    {
        return new self($node->outerHtml());
    }
}
