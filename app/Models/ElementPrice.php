<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class ElementPrice extends Model
{
    use HasFactory;

    /** @var array  */
    protected $fillable = [
        'element',
        'avg_price',
        'min_price',
        'max_price',
        'count_lots',
    ];

    public function scopeLastPrices(Builder $builder, string $date): void
    {
        $builder->whereIn('id', function (\Illuminate\Database\Query\Builder $query) use ($date) {
            $query->from(ElementPrice::getModel()->getTable())->selectRaw('MAX(id) as id');

            if ($date) {
                $query->whereRaw('Date(created_at) = ?', [$date]);
            }

            $query->groupBy('element');
        });
    }
}
