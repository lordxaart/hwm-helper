<?php

namespace Tests\Feature\Pages;

use Tests\TestCase;

class MarketPageTest extends TestCase
{
    /**
     * @test
     */
    public function can_view_market_page()
    {
        $response = $this->get(self::MARKET_URL);

        $response->assertStatus(200);
    }
}
