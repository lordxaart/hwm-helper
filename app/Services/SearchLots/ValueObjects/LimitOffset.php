<?php

declare(strict_types=1);

namespace App\Services\SearchLots\ValueObjects;

class LimitOffset
{
    private int $limit;
    private int $offset;

    /**
     * @param int $limit
     * @param int $offset
     */
    public function __construct(int $limit, int $offset = 0)
    {
        $this->limit = $limit;
        $this->offset = $offset;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }
}
