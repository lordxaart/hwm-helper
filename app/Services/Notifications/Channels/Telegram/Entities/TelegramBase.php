<?php

declare(strict_types=1);

namespace App\Services\Notifications\Channels\Telegram\Entities;

use App\Services\Notifications\Channels\Telegram\Telegram;
use App\Services\Notifications\Channels\Telegram\Traits\HasSharedLogic;
use App\Services\Notifications\Channels\Telegram\Traits\MessageTrait;
use JsonSerializable;

/**
 * Class TelegramBase.
 */
class TelegramBase implements JsonSerializable
{
    use HasSharedLogic;
    use MessageTrait;

    public Telegram $telegram;

    public function __construct()
    {
        $this->telegram = app(Telegram::class);
    }

    protected function wrapContentWithHead(string $content): string
    {
        $head = $this->messageType ? $this->getEmojiByType() : '';
        $head .= '*' . config('app.name') . "*\n";

        $content = trim($content, PHP_EOL);

        return $head . $content;
    }
}
