<?php

declare(strict_types=1);

namespace App\Console\Commands\HWM\Lot;

use App\Console\Commands\Command;
use App\Jobs\AdminNotify;
use App\Models\Lot;
use App\Notifications\Admin\Notify;
use App\Services\SearchLots\SearchLotService;
use App\Services\SearchLots\ValueObjects\LimitOffset;
use App\Services\SearchLots\ValueObjects\LotFilters;
use App\Services\SearchLots\ValueObjects\OrderBy;
use Exception;
use Illuminate\Support\Arr;

use const DATE_FORMAT;

class CheckParserStatus extends Command
{
    private const OLD_LOT_MINUTES = 5;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = 'hwm:parser:check-status';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update arts list from market and sets page';

    /**
     * @throws Exception
     */
    public function handle(): void
    {
        if (($lot = $this->isParserLotsDisactive())) {
            $text = 'Parser lots is late';
            if ($lot instanceof Lot) {
                $date = $lot->started_at->format(DATE_FORMAT);
                $text .= ". Last lot #{$lot->lot_id} [{$date}]";
            }

            $this->warn($text);

            if (isProdEnv()) {
                AdminNotify::dispatchSync(
                    new Notify($text)
                );
            }
        }

        if (($lot = $this->isEsLotsDisactive())) {
            $text = 'Elasticsearch lots repository is late';
            if ($lot instanceof Lot) {
                $date = $lot->started_at->format(DATE_FORMAT);
                $text .= ". Last lot #{$lot->lot_id} [{$date}]";
            }

            $this->warn($text);

            if (isProdEnv()) {
                AdminNotify::dispatchSync(
                    new Notify($text)
                );
            }
        }
    }

    private function isParserLotsDisactive(): bool | Lot | null
    {
        $lot = Lot::whereLotId(Lot::max('lot_id'))->first();

        if (!$lot || $lot->started_at->lt(now()->subMinutes(self::OLD_LOT_MINUTES))) {
            return $lot ?: true;
        }

        return false;
    }

    private function isEsLotsDisactive(): bool | Lot | null
    {
        $filters = new LotFilters([]);
        $limitOffset = new LimitOffset(1, 0);
        $orderBy = new OrderBy(['started_at' => 'desc']);
        $lots = app(SearchLotService::class)->searchLotsByFilter(
            $filters,
            $limitOffset,
            $orderBy,
        );
        /** @var Lot | null $lot */
        $lot = Arr::first($lots->getData());

        if (!$lot || $lot->started_at->lt(now()->subMinutes(self::OLD_LOT_MINUTES))) {
            $lot = Lot::where('lot_id', $lot->lot_id)->first();

            return $lot ?: true;
        }

        return false;
    }
}
