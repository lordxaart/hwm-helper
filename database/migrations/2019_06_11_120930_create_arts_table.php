<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hwm_id', config('hwm.rules.max_hwm_id_length'))->unique();
            $table->string('title', config('hwm.rules.max_art_title_length'))->index();
            $table->string('original_image'); // full path to original image (can be change to original big image after handle)
            $table->tinyInteger('need_level')->unsigned()->nullable();
            $table->tinyInteger('oa')->unsigned()->nullable();
            $table->integer('repair')->unsigned()->nullable();
            $table->integer('strength')->unsigned()->default(1);
            $table->integer('shop_price')->unsigned()->nullable();
            $table->decimal('price_per_fight', 10, 2)->unsigned()->nullable();
            $table->tinyInteger('from_shop')->default(0);
            $table->tinyInteger('as_from_shop')->default(0);
            $table->text('description')->nullable();
            $table->text('modifiers')->nullable();
            $table->text('extra_modifiers')->nullable();

            $table->string('category_id', config('hwm.rules.max_hwm_id_length'))->nullable();
            $table->string('market_category_id', config('hwm.rules.max_hwm_id_length'))->nullable();
            $table->string('set_id', config('hwm.rules.max_hwm_id_length'))->nullable();

            $table->timestamps();

            $table->foreign('category_id')
                ->references('hwm_id')
                ->on('art_categories')
                ->onDelete('set null');

            $table->foreign('market_category_id')
                ->references('hwm_id')
                ->on('art_market_categories')
                ->onDelete('set null');

            $table->foreign('set_id')
                ->references('hwm_id')
                ->on('art_sets')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arts');
    }
}
