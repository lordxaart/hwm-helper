<?php

declare(strict_types=1);

namespace App\Listeners\ArtMonitor;

use App\Events\ArtMonitor\AddArtToMonitorEvent as Event;
use App\Services\ActivityLogger\ActivityLogger;
use App\Services\ActivityLogger\ValueObjects\Activity;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddedArtToMonitorListener implements ShouldQueue
{
    public function __construct(private readonly ActivityLogger $activityLogger)
    {
    }

    public function handle(Event $event): void
    {
        $this->activityLogger->log(
            Activity::action('HWM_MONITORED_ART_CREATE')
                ->context([
                    'monitoredArt' => $event->art->toArray(),
                ])->causedBy($event->art->user)
        );
    }
}
