<?php

declare(strict_types=1);

namespace App\Actions\User;

use App\Models\User;
use Carbon\Carbon;
use Lorisleiva\Actions\Concerns\AsAction;

class BlockUser
{
    use AsAction;

    public function handle(User $user, Carbon $untilTo = null): bool
    {
        if ($user->isBlocked()) {
            throw new \InvalidArgumentException('User already blocked');
        }

        if ($untilTo && $untilTo->isPast()) {
            throw new \InvalidArgumentException('UntilTo parameter must be future time');
        }

        if ($user->fireEvent('blocking') === false) {
            return false;
        }

        $user->blocked_at = $user->freshTimestamp();
        $user->blocked_until_to = $untilTo;

        $user->exists = true;

        $res = $user->save();

        $user->fireEvent(User::ADMIN_ACTION_BLOCKED, false);

        return $res;
    }
}
