<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Notifications\DatabaseNotification;

class Notification extends DatabaseNotification
{
    public function setDataAttribute(array $value): void
    {
        $this->attributes['data'] = json_encode($value, JSON_UNESCAPED_UNICODE);
    }
}
