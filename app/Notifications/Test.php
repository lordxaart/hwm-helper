<?php

declare(strict_types=1);

namespace App\Notifications;

use App\Services\Notifications\Channels\Telegram\Entities\TelegramMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;

class Test extends Notification implements ShouldQueue
{
    use Queueable;

    public array $channels = [];

    public function __construct(array $channels)
    {
        $this->channels = $channels;
    }

    public function via(): array
    {
        return array_merge($this->channels, ['database']);
    }

    public function toMail(): MailMessage
    {
        return (new MailMessage())
                ->greeting(trans('app.notifications.test_notification'));
    }

    public function toSlack(): SlackMessage
    {
        return (new SlackMessage())
            ->from(config('app.name'), ':ghost:')
            ->success()
            ->content(trans('app.notifications.test_notification'));
    }

    public function toTelegram(): TelegramMessage
    {
        return TelegramMessage::create()
            ->success()
            ->content(trans('app.notifications.test_notification'));
    }

    public function toBroadcast(): BroadcastMessage
    {
        return new BroadcastMessage([
            'message' => trans('app.notifications.test_notification'),
        ]);
    }

    public function broadcastType(): string
    {
        return 'test';
    }

    public function toArray(): array
    {
        return [
            'message' => trans('app.notifications.test_notification'),
        ];
    }
}
