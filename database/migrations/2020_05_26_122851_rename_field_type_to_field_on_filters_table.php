<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameFieldTypeToFieldOnFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('art_monitored_filters', function (Blueprint $table) {
            $table->renameColumn('type', 'field');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('art_monitored_filters', function (Blueprint $table) {
            $table->renameColumn('field', 'type');
        });
    }
}
