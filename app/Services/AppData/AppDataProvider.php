<?php

declare(strict_types=1);

namespace App\Services\AppData;

use App\Console\Commands\HWM\GenerateArtImagesSprite;
use App\Core\Enums\SocialProvider;
use App\Http\Resources\UserResource;
use App\HWM\Enums\EntityType;
use App\HWM\Enums\LotType;
use App\Models\User;
use App\Services\LotFilter\Enums\FilterLotField;
use App\Services\LotFilter\Enums\FilterOperator;
use App\Services\Notifications\Enums\Channel;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class AppDataProvider
{
    private ?User $user;

    public function __construct(Request $request)
    {
        $this->user = $request->user();
    }

    public function getData(): array
    {
        return [
            'app' => $this->getAppData(),
            'user' => $this->user ? (new UserResource($this->user))->setIncludes(['filters']) : null,
            'features' => $this->getAppFeatures(),
        ];
    }

    private function getAppData(): array
    {
        $appConfig = Arr::only(config('app'), [
            'env',
            'debug',
            'timezone',
            'dateformat',
            'date_format_short',
            'locale',
        ]);

        return [
            'name' => config('app.name'),
            'owner' => config('app.owner'),
            'default_img_url' => asset(config('app.default_img_url')),
            'url' => config('app.url'),
            'domain' => config('app.domain'),
            ...$appConfig,
            'csrf_token' => csrf_token(),
            'ws_host' => env('WS_HOST'),
            'ws_port' => env('WS_PORT'),
            'sprite_key' => $this->getSpriteKey(),
            'pusher_app_key' => env('PUSHER_APP_KEY'),
            'amplitude_api_key' => env('AMPLITUDE_API_KEY'),
            'hwm' => [
                'default_master' => config('hwm.default_master'),
                'lot_types' => LotType::cases(),
                'entity_types' => EntityType::cases(),
                'oldest_lot_date' => '2007-04-25',
                'count_lots' => 111_283_447,
                'lot_filters' => [
                    'fields' => FilterLotField::cases(),
                    'operators' => FilterOperator::cases(),
                ],
            ],
            'socials_providers' => array_map(
                fn (SocialProvider $provider) => ['provider' => $provider->value, 'title' => $provider->getTitle()],
                SocialProvider::cases(),
            ),
            'notification_channels' => Channel::cases(),
            'locales' => config('localization.locales'),
            'vapid_public_key' => config('webpush.vapid.public_key')
        ];
    }

    private function getSpriteKey(): string
    {
        return \Storage::exists(GenerateArtImagesSprite::KEY_STORAGE_PATH)
            ? \Storage::get(GenerateArtImagesSprite::KEY_STORAGE_PATH)
            : date('Y-m-d-H');
    }

    private function getAppFeatures(): array
    {
        return [
        ];
    }
}
