<?php

declare(strict_types=1);

namespace App\HWM\Exceptions;

class RequestError extends HwmException
{
    //
}
