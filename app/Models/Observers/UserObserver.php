<?php

declare(strict_types=1);

namespace App\Models\Observers;

use App\Exceptions\ActionDeny;
use App\Models\Role;
use App\Notifications\User\ChangeStatus;
use App\Models\User;

class UserObserver
{
    public function creating(User $user): void
    {
        //
    }

    public function created(User $user): void
    {
        $user->updateApiToken();

        if (!$user->isSuperAdmin()) {
            $user->assignRole(Role::USER);
        }
    }

    public function updated(User $user): void
    {
        //
    }

    /**
     * @throws ActionDeny
     */
    public function deleting(User $user): void
    {
        if ($user->isSuperAdmin()) {
            throw new ActionDeny('delete super admin');
        }
    }

    public function deleted(User $user): void
    {
        // Notify user about delete
        $user->notify(new ChangeStatus(User::ADMIN_ACTION_DELETED, user()?->getId()));
    }

    public function restored(User $user): void
    {
        // Notify user about restore
        $user->notify(new ChangeStatus(User::ADMIN_ACTION_RESTORED, user()?->getId()));
    }

    public function blocked(User $user): void
    {
        // Notify user about block
        $user->notify(new ChangeStatus(User::ADMIN_ACTION_BLOCKED, user()?->getId()));
    }

    public function unblocked(User $user): void
    {
        // Notify user about unblock
        $user->notify(new ChangeStatus(User::ADMIN_ACTION_UNBLOCKED, user()?->getId()));
    }
}
