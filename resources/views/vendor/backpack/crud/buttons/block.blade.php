@if(!$entry->isDeleted())
    @if (!$entry->isBlocked() && !$entry->isSuperAdmin())
        <a
            href="#"
            onclick="changeUserStatus(this)"
            data-route="{{ url($crud->route.'/'.$entry->getKey()).'/block' }}"
            class="btn btn-sm btn-link"
            data-type="block"
            title="Block"
		    data-toggle="tooltip"
            data-text="You are want block account {{ $entry->email }}?"
        >
            <i class="fas fa-lock"></i>
        </a>
    @endif
    @if ($entry->isBlocked())
        <a
            href="#"
            onclick="changeUserStatus(this)"
            data-route="{{ url($crud->route.'/'.$entry->getKey()).'/unblock' }}"
            class="btn btn-sm btn-link"
            data-type="unblock"
            title="Unblock"
		    data-toggle="tooltip"
            data-text="You are want unblock account {{ $entry->email }}?"
        >
            <i class="fas fa-unlock"></i>
        </a>
    @endif
@endif