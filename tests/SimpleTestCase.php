<?php

namespace Tests;

use PHPUnit\Framework\TestCase as BaseTestCase;
use Tests\Traits\CreatesApplication;

abstract class SimpleTestCase extends BaseTestCase
{
    use CreatesApplication;
}
