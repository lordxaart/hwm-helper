<?php

declare(strict_types=1);

namespace App\Components\Validators\Rules;

use Illuminate\Contracts\Validation\Rule;

class OldPasswordConfirmed implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        if (!auth()->check()) {
            return false;
        }

        return \Hash::check($value, (string)user()?->password);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return trans('validation.old_password');
    }
}
