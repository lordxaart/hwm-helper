<?php

namespace Tests\Feature\Pages;

use Tests\TestCase;
use Tests\Traits\WithStudUser;

class CalculatorPageTest extends TestCase
{
    use WithStudUser;

    /**
     * @test
     */
    public function can_view_calculator_page()
    {
        $response = $this->get(self::CALCULATOR_URL);

        $response->assertStatus(200)
            ->assertSeeText(__('app.main.calculator'));
    }

    /**
     * @test
     */
    public function can_view_index_page()
    {
        $response = $this->get('/');

        $response->assertStatus(302);
    }
}
