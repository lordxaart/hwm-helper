<?php

namespace Tests\Feature\API;

use App\HWM\Repositories\ArtRepository;
use App\Models\Art;
use App\Models\MonitoredArt;
use App\Models\User;

class MonitoringArtsTest extends ApiTestCase
{
    const COUNT_MONITORED_ARTS = 4;

    /**
     * @test
     */
    public function check_arts_monitored_response()
    {
        $user = User::factory()
            ->has(MonitoredArt::factory()->count(self::COUNT_MONITORED_ARTS), 'monitoredArts')
            ->create();

        $response = $this->actingAs($user)->getJson(self::API_GET_PROFILE_MONITOR_ARTS);

        $response->assertJsonStructure(['data' => [$this->getElementsResponseJsonStructure()]]);

        $response->assertJsonCount(self::COUNT_MONITORED_ARTS, 'data');
    }

    /**
     * @test
     */
    public function check_add_art_to_monitored_response()
    {
        $user = User::factory()->create();

        $artId = app(ArtRepository::class)->all()->random()->getHwmId();

        $response = $this->actingAs($user)->post(self::API_GET_PROFILE_MONITOR_ARTS, ['art_id' => $artId,]);

        $response->assertStatus(201);

        $response->assertJsonStructure(['data' => $this->getElementsResponseJsonStructure()]);
    }

    public function getElementsResponseJsonStructure()
    {
        return [
            'hwmId',
            'isActive',
            'createdAt',
            'updatedAt',
        ];
    }
}
