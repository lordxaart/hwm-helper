<?php

declare(strict_types=1);

namespace App\Console\Commands\HWM\Lot;

use App\Console\Commands\Command;
use App\HWM\Enums\LotType;
use App\Jobs\Lot\LotUpdate;
use App\Services\SearchLots\Repositories\LotRepository;
use App\Services\SearchLots\ValueObjects\LimitOffset;
use App\Services\SearchLots\ValueObjects\LotFilters;
use App\Services\SearchLots\ValueObjects\OrderBy;

class RefreshLotsAuction extends Command
{
    /** @var string  */
    public $signature = 'hwm:lots:refresh:auction';

    /** @var string  */
    public $description = 'Some auction, blitz lots incorrect parsed. Reparse';

    public function handle(): void
    {
        /** @var LotRepository $repository */
        $repository = app(LotRepository::class);

        $limit = 10;
        $offset = 0;

        $lotFilters = new LotFilters(['lot_type' => implode(',', [LotType::AUCTION->value, LotType::BLITZ->value])]);
        $order = new OrderBy(['started_at' => OrderBy::DESC]);
        $limitOffset = new LimitOffset($limit, $offset);

        $totalCount = $repository->findByFilters($lotFilters, $limitOffset, $order, true)->getTotal() ?: 0;

        $this->output->progressStart($totalCount);

        for ($i = $offset; $i <= $totalCount; $i += $limit) {
            $limitOffset = new LimitOffset($limit, $i);
            $response = $repository->findByFilters($lotFilters, $limitOffset, $order);

            /** @var \App\HWM\Entities\Lot $lot */
            foreach ($response->getData() as $lot) {
                dispatch(new LotUpdate($lot->lot_id));
                $this->output->progressAdvance();
            }
        }

        $this->output->progressFinish();
    }
}
