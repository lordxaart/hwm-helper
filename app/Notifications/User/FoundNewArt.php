<?php

declare(strict_types=1);

namespace App\Notifications\User;

use App\Core\Contracts\Notifable;
use App\Core\Enums\Queue;
use App\HWM\Helpers\HwmLink;
use App\Models\Art;
use App\Services\Notifications\Channels\Telegram\Entities\TelegramFile;
use App\Services\Notifications\Enums\Channel;
use App\Services\Notifications\Enums\NotifyType;
use App\Services\Notifications\UserNotifyService;
use App\Services\Settings\User\GameLinkDecorator;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class FoundNewArt extends Notification implements ShouldQueue
{
    use Queueable;

    public Art $art;

    public function __construct(Art $art)
    {
        $this->queue = Queue::NOTIFICATIONS->value;

        $this->art = $art;

        $this->delay(now()->addSeconds(mt_rand(0, 10)));
    }

    public function via(Notifable $notifable): array
    {
        if ($notifable instanceof User)
            return [...app(UserNotifyService::class)
                ->getConfiguredChannels($notifable, NotifyType::NEW_ART), Channel::DATABASE];
        return [
            Channel::TELEGRAM->getLaravelChannelName(),
        ];
    }

    public function toMail(Notifable $notifable): MailMessage
    {
        return (new MailMessage())
            ->subject(trans('app.notifications.found_new_art'))
            ->markdown('emails.found_new_art', [
                'art' => $this->art,
                'user' => $notifable instanceof User ? $notifable : null,
                'linkDecorator' => app(GameLinkDecorator::class),
            ]);
    }

    public function toTelegram(Notifable $notifable): TelegramFile
    {
        $content = trans('app.notifications.found_new_art') . ': ' . "[{$this->art->getTitle()}]({$this->art->getLink()})" . PHP_EOL;

        $message = TelegramFile::create()
            ->content($content);

        if ($this->art->market_category_id) {
            /** @var GameLinkDecorator $linkDecorator */
            $linkDecorator = app(GameLinkDecorator::class);

            $marketLink = HwmLink::get(HwmLink::MARKET, [
                'cat' => $this->art->market_category_id,
                'art_type' => $this->art->hwm_id,
            ]);
            $btnUrl = $notifable instanceof User ? $linkDecorator->replaceGameLinkForUser($marketLink, $notifable) : $marketLink;
            $message->button(trans('app.main.market'), $btnUrl);
        }

        $image = $this->art->asArtEntity()->getImage(true);
        if (str_contains($image, 'art_default')) {
            $image = $this->art->original_image;
        }
        $message->photo($image);

        return $message;
    }

    public function toArray(): array
    {
        return [
            'art' => $this->art->hwm_id,
        ];
    }

    public function toBroadcast(): BroadcastMessage
    {
        return new BroadcastMessage([
            'art' => $this->art->transform(),
        ]);
    }

    public function broadcastType(): string
    {
        return 'found_new_art';
    }
}
