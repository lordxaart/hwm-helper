<?php

declare(strict_types=1);

namespace App\Notifications\Backup;

use Spatie\Backup\Notifications\Notifiable;

class BackupNotifiableNotification extends Notifiable
{
    public function routeNotificationForTelegram(): ?string
    {
        return config('services.telegram-bot-api.notify_chat_id');
    }
}
