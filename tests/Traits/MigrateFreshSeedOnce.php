<?php

namespace Tests\Traits;

trait MigrateFreshSeedOnce
{
    /**
    * If true, setup has run at least once.
    * @var boolean
    */
    protected static $setUpHasRunOnce = false;

    protected function setUp(): void
    {
        parent::setUp();

        if (!static::$setUpHasRunOnce) {
            $this->artisan('cache:clear');

            $this->artisan('migrate', [
                '--seed' => true,
            ]);

            static::$setUpHasRunOnce = true;
         }
    }
}
