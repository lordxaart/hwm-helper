<?php

declare(strict_types=1);

namespace App\Actions\Lot;

use App\Actions\Art\ArtAdd;
use App\Events\Lot\CreatedNewLotEvent;
use App\HWM\Enums\EntityType;
use App\HWM\Repositories\ArtRepository;
use App\Jobs\Lot\IndexLotToElasticSearch;
use App\Models\Lot;
use App\Models\LotOperation;
use Illuminate\Console\Command;
use Illuminate\Database\QueryException;
use Lorisleiva\Actions\Concerns\AsAction;

/**
 * @property string $art_id
 */
class LotAdd
{
    use AsAction;

    public const DB_DUPLICATE_ERROR_CODE = '23000';

    public string $commandSignature = 'hwm:lot:add {lot_id : Lot ID} {crc? : Lot CRC hash}';
    public string $commandDescription = 'Add lot by :lot_id';

    public function asCommand(Command $command): int
    {
        $lot = $this->handle(intval($command->argument('lot_id')), $command->argument('crc'));

        $command->info("Lot $lot->lot_id was successfully added");

        return 0;
    }

    public function handle(int $lotId, string $crc = null, bool $fireEvent = true): Lot
    {
        $lotEntity = \hwmClient()->getLot($lotId, $crc);

        try {
            $lot = Lot::createFromLotEntity($lotEntity);

            if ($lot->is_parsed) {
                LotOperation::createFromArray($lotEntity->lot_id, $lotEntity->operations);
            }
        } catch (QueryException $e) {
            if ($e->getCode() === self::DB_DUPLICATE_ERROR_CODE) {
                $lot = Lot::where('lot_id', $lotEntity->lot_id)->first();

                if (!$lot) {
                    throw $e;
                }

                if ($lot->is_parsed) {
                    LotOperation::createFromArray($lotEntity->lot_id, $lotEntity->operations);
                }

                return $lot;
            } else {
                throw $e;
            }
        }

        if ($fireEvent && $lot->wasRecentlyCreated) {
            event(new CreatedNewLotEvent($lot));
        }

        $entityType = EntityType::tryFrom($lotEntity->entity_type);
        if (
            $entityType
            && $entityType->isArtEntity()
            && $lotEntity->entity_id
            && !app(ArtRepository::class)->has($lotEntity->entity_id)
        ) {
            ArtAdd::dispatch($lotEntity->entity_id);
        }

        IndexLotToElasticSearch::dispatch($lot->asLotEntity());

        return $lot;
    }
}
