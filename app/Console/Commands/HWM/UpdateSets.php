<?php

declare(strict_types=1);

namespace App\Console\Commands\HWM;

use App\Actions\Art\ArtSetAdd;
use App\Actions\Art\ArtAdd;
use App\Console\Commands\Command;
use App\HWM\Entities\Set;
use App\Models\Art;
use App\Models\ArtSet;
use Exception;

class UpdateSets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hwm:set:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update sets list and update their arts';

    /**
     * @throws Exception
     */
    public function handle(): void
    {
        retry(3, function () {
            $hwm = hwmClient();

            /** @var iterable $sets */
            $sets = $hwm->getSets();
            $newSetsCount = 0;
            $newArtsCount = 0;
            $arts = [];

            /** @var Set $set */
            foreach ($sets as $set) {
                if (!ArtSet::findByHwmId($set->getHwmId())) {
                    /** @var ArtSet $newSet */
                    $newSet = ArtSetAdd::run($set->getHwmId(), $set->getTitle());
                    if ($newSet->wasRecentlyCreated) {
                        $newSetsCount++;
                        $this->info('Add new set - ' . $newSet->title);
                    }
                }
                $arts = array_merge($arts, $set->arts ?: []);
            }

            $this->log('Count new sets - ' . $newSetsCount);

            foreach ($arts as $item) {
                if (!Art::findByHwmId($item['art_id'])) {
                    $newArt = ArtAdd::run($item['art_id']);
                    if ($newArt->wasRecentlyCreated) {
                        $newArtsCount++;
                        $this->info('Add new art - ' . $newArt->title);
                    }
                }
            }

            $this->info('Count new arts - ' . $newArtsCount);
        }, 5);
    }
}
