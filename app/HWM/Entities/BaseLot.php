<?php

declare(strict_types=1);

namespace App\HWM\Entities;

use App\HWM\Entities\Interfaces\CanBeLot;
use App\HWM\Entities\Interfaces\HasLinkOnPage;
use App\HWM\Entities\Interfaces\HasTranslations;
use App\HWM\Entities\Interfaces\HwmEntityInterface;
use App\HWM\Enums\EntityType;
use App\HWM\Enums\LotType;
use App\HWM\Helpers\HwmHelper;
use App\HWM\Helpers\HwmLink;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Lang;

abstract class BaseLot extends HwmEntity implements HasTranslations
{
    public int $lot_id;
    public string $lot_type;
    public string $entity_type;
    public ?string $entity_id;
    public string $entity_title;
    public int $price;
    public int $quantity;
    public ?Carbon $ended_at;
    public string $seller_name;
    public int $seller_id;
    public ?int $art_uid;
    public ?string $art_crc;
    public ?string $craft;
    public ?int $current_strength;
    public ?int $base_strength;
    public ?int $blitz_price;
    public ?float $price_per_fight = null;
    public ?string $crc = null;
    private ?HwmEntityInterface $entity = null;

    public function __construct(array $parameters = [])
    {
        if (isset($parameters['ended_at'])) {
            $parameters['ended_at'] = Carbon::parse($parameters['ended_at']);
        }

        if (!isset($parameters['title'])) {
            $parameters['title'] = $parameters['entity_title'];
        }

        if (!$parameters['entity_id']) {
            $parameters['entity_id'] = Str::slug($parameters['entity_title']);
        }

        parent::__construct(strval($parameters['lot_id']), $parameters);
    }

    public static function getLotTypes(): array
    {
        return enumToArrayOfValues(LotType::cases());
    }

    public static function getEntityTypes(): array
    {
        return enumToArrayOfValues(EntityType::cases());
    }

    public function setPPF(int $repair): self
    {
        if (!$this->price || !$repair || !$this->current_strength || !$this->base_strength) {
            return $this;
        }

        $this->price_per_fight = HwmHelper::pricePerFight(
            $this->price,
            $repair,
            $this->current_strength,
            $this->base_strength,
        );

        return $this;
    }

    public function isHasStrength(): bool
    {
        return $this->entity_type === EntityType::ART->value;
    }

    public function getSellerProfileLink(): string
    {
        return HwmLink::get(HwmLink::CHARACTER_INFO, ['id' => $this->seller_id]);
    }

    public function getLink(): string
    {
        return HwmLink::get(HwmLink::LOT_INFO, ['id' => $this->lot_id, 'crc' => $this->crc]);
    }

    public function getMarketLink(string $marketCategoryId = null): string
    {
        return HwmLink::get(HwmLink::MARKET, [
            'cat' => $marketCategoryId ?: $this->getEntityObject()->getMarketCategoryId(),
            'art_type' => $this->entity_id,
            'hwmh[lot_id]' => $this->lot_id,
        ]);
    }

    public function getParsedCraft(): ?array
    {
        return $this->craft ? HwmHelper::craftToArray($this->craft) : null;
    }

    public function entityAsArray(): array
    {
        /** @var HwmEntityInterface&CanBeLot $entity */
        $entity = $this->getEntityObject();

        $entityLink = !$entity instanceof HasLinkOnPage
            ? null
            : $entity->getHwmLink();

        // add crc and uid parameters
        if ($this->art_crc) {
            $entityLink .= '&art_crc=' . $this->art_crc;
        }
        if ($this->art_uid) {
            $entityLink .= '&art_uid=' . $this->art_uid;
        }

        return [
            'hwm_id' => $entity->getHwmId(),
            'type' => $entity->getEntityType()->value,
            'title' => $entity->getTitle(),
            'image' => $entity->getImage(false, 'small'),
            'market_category_id' => $entity->getMarketCategoryId(),
            'hwm_link' => $entityLink,
            'hwm_market_link' => HwmLink::get(HwmLink::MARKET, [
                'cat' => $entity->getMarketCategoryId(),
                'art_type' => $entity->getHwmId(),
            ]),
        ];
    }

    /**
     * @return HwmEntityInterface&CanBeLot
     */
    public function getEntityObject(): HwmEntityInterface
    {
        if (!$this->entity) {
            $class = ucfirst(Str::camel($this->entity_type));
            $class = str_replace('Resource', 'Res', $class);
            $class = str_replace('ArtPart', 'Art', $class);
            $class = '\App\HWM\Entities\\' . $class;

            $this->entity = new $class($this->entity_id, [
                'title' => $this->getTitle(),
            ]);
        }

        return $this->entity;
    }

    public function translateAttributes(): void
    {
        $newTitle = null;
        switch ($this->entity_type) {
            case EntityType::ELEMENT->value:
                $key = 'hwm.elements.' . $this->entity_id;
                if (Lang::has($key)) {
                    $newTitle = trans($key);
                }
                break;
            case EntityType::RESOURCE->value:
                $key = 'hwm.resources.' . $this->entity_id;
                if (Lang::has($key)) {
                    $newTitle = trans($key);
                }
                break;
            case EntityType::SKELETON->value:
                $newTitle = trans('app.lots.entity_types.skeleton');
                break;
            case EntityType::CERTIFICATE->value:
                $key = 'hwm.map.areas.' . $this->entity_id;
                if (Lang::has($key)) {
                    $newTitle = trans($key);
                }
                break;
        }

        if ($newTitle) {
            $this->title = $newTitle;
            $this->entity_title = $newTitle;
        }
    }
}
