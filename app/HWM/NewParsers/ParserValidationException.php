<?php

declare(strict_types=1);

namespace App\HWM\NewParsers;

class ParserValidationException extends \Exception
{
    public function __construct(string $parser, string $errors)
    {
        $message = trans('errors.hwm.invalid_response_after_parsing', ['type' => $parser]);
        $message .= '. ' . $errors;

        parent::__construct($message);
    }
}
