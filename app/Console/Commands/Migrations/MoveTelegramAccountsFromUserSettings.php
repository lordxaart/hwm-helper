<?php

declare(strict_types=1);

namespace App\Console\Commands\Migrations;

use App\Console\Commands\Command;
use App\Models\User;
use App\Services\Telegram\UserTelegramService;

class MoveTelegramAccountsFromUserSettings extends Command
{
    /** @var string */
    protected $signature = 'migrate:user:telegram:account';

    /** @var string  */
    protected $description = 'Move telegram account from user settings to separate table';

    public function handle(): void
    {
        User::all()->each(function (User $user) {
            $json = $user->getRawOriginal('settings');
            if (!$json) {
                return;
            }

            $settings = @json_decode($json, true);

            if (!isset($settings['telegram_chat_id']) || !$settings['telegram_chat_id']) {
                return;
            }

            $this->info(sprintf('Start migrate account for user %s', $user->email));

            $chatId = (int) $settings['telegram_chat_id'];

            $data = [
                'id' => $settings['telegram_user_id'] ?? null,
                'first_name' => $settings['telegram_first_name'] ?? null,
                'last_name' => $settings['telegram_last_name'] ?? null,
                'username' => $settings['telegram_username'] ?? null,
                'lang' => $settings['telegram_lang_code'] ?? null,
                'is_bot' => false,
            ];

            $telegramUser = new \App\Services\Telegram\Entities\User($data);

            app(UserTelegramService::class)->connectTelegramToUser($user, $chatId, $telegramUser, false);
        });
    }
}
