<?php

return [
    'settings_tabs' => [
        ['id' => 'personal', 'title' => 'app.user.personal_information'],
        ['id' => 'notifications', 'title' => 'app.main.notifications'],
        ['id' => 'password', 'title' => 'app.user.change_password'],
//        ['id' => 'filters', 'title' => 'app.main.filters', 'disabled' => true],
//        ['id' => 'monitoring', 'title' => 'app.main.monitoring', 'disabled' => true],
    ],
];
