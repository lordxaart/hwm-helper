import Vue from 'vue';
import HWM from 'JS/app/utils/hwm/objects/HWM';
import config from 'JS/app/config';
import { objectToQueryString } from 'JS/app/utils/helpers';
import { logEvent } from '../utils/amplitude';

const modals = {
    methods: {
        openModal(selector) {
            this.$bvModal.show(selector);
        },
        hideModal(selector) {
            this.$bvModal.hide(selector);
        },
    },
};

const hwm = new HWM();

const hwmHelpers = {
    data() {
        return {
            hwm,
        };
    },
    methods: {
        filterTermAsString(term, sellers = null) {
            const fields = {
                price: this.$t('app.art.columns.start_price'),
                price_per_fight: this.$t('app.art.columns.price_per_fight'),
                base_strength: this.$t('app.art.columns.base_strength'),
                current_strength: this.$t('app.art.columns.current_strength'),
                craft: this.$t('app.art.columns.craft'),
                quantity: this.$t('app.art.columns.quantity'),
                seller_id: this.$t('app.art.columns.seller_id'),
            };

            const operator = term.operator === '==' ? '=' : term.operator;

            if (term.operator === 'has') {
                const value = term.value ? this.$t('app.main.yes') : this.$t('app.main.no');
                return `${fields[term.field]} ${value}`;
            }

            if (term.field === 'seller_id' && sellers && sellers[`_${term.value}`]) {
                const seller = sellers[`_${term.value}`];
                const field = fields[term.field].toString().replace('(ID)', '').trim();
                return `${field} ${operator} ${seller}`;
            }

            return `${fields[term.field]} ${operator} ${term.value}`;
        },
    },
};

const tabs = {
    methods: {
        tabIsActive(hash) {
            return window.location.hash ? window.location.hash === hash : false;
        },
        clickOnTab(evt) {
            if (evt.target && evt.target.id) {
                const id = evt.target.id.replace('___BV_tab_button__', '');
                window.location.hash = `#${id}`;
            }
        },
    },
};

const route = {
    methods: {
        route(index, params = {}) {
            const routes = {
                'profile.updatePassword': '/profile/settings/password',
                'profile.connectSocial': '/profile/connect-social',
            };

            let link = routes[index];

            if (Object.keys(params).length) {
                link += objectToQueryString(params);
            }

            return link;
        },
    },
};

const metaTags = {
    data() {
        return {
            metaTagsSeparator: config.app.meta_tags_separator,
            metaTagsPreviousTitle: null,
            metaTagsPreviousImage: null,
        };
    },
    methods: {
        metaSetTitle(title = null, prepend = true) {
            if (!title) {
                if (this.metaTagsPreviousTitle) {
                    this.$metaInfo.title = this.metaTagsPreviousTitle;
                    this.metaTagsPreviousTitle = null;
                }
            } else {
                this.metaTagsPreviousTitle = this.$metaInfo.title;

                if (prepend) {
                    this.$metaInfo.title = `${title} ${this.metaTagsSeparator} ${this.$metaInfo.title}`;
                } else {
                    this.$metaInfo.title = title;
                }
            }
            this.$meta().refresh();
        },
        metaSetImage(src = null) {
            const previousSrc = document.querySelector('meta[name=image]').content;
            const elements = [
                document.querySelector('meta[name="image"]'),
                document.querySelector('meta[name="og:image"]'),
                document.querySelector('meta[name="twitter:image"]'),
                document.querySelector('meta[name="twitter:image:src"]'),
            ];

            if (!src) {
                if (this.metaTagsPreviousImage) {
                    elements.forEach((element) => {
                        if (element) {
                            element.content = this.metaTagsPreviousImage;
                        }
                    });
                    this.metaTagsPreviousImage = null;
                }
            } else {
                elements.forEach((element) => {
                    if (element) {
                        element.content = src;
                    }
                });
                this.metaTagsPreviousImage = previousSrc;
            }
        },
    },
};

const tracker = {
    methods: {
        logEvent(eventName, payload) {
            logEvent(eventName, payload);
        },
    },
};

// Init all mixins
Vue.mixin(modals);
Vue.mixin(hwmHelpers);
Vue.mixin(tabs);
Vue.mixin(route);
Vue.mixin(metaTags);
Vue.mixin(tracker);
