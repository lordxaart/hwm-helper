import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'index',
            component: () => import('JS/app/pages/Index'),
        },
        {
            path: '/calculator',
            name: 'calculator',
            component: () => import('JS/app/pages/Calculator'),
        },
        {
            path: '/market',
            name: 'market',
            component: () => import('JS/app/pages/Market'),
        },
        {
            path: '/monitoring',
            name: 'monitoring',
            component: () => import('JS/app/pages/Monitoring'),
            meta: { auth: true },
        },
        {
            path: '/filters',
            name: 'filters',
            component: () => import('JS/app/pages/Filters'),
            meta: { auth: true },
        },
        {
            path: '/stat',
            name: 'stat',
            component: () => import('JS/app/pages/Stat'),
            meta: { auth: false },
        },
        {
            path: '/analytics',
            name: 'analytics',
            component: () => import('JS/app/pages/Analytics'),
        },
        {
            path: '/profile/settings',
            name: 'profile.index',
            component: () => import('JS/app/pages/UserSettings'),
            meta: { auth: true },
        },
        {
            path: '/contacts',
            name: 'contacts',
            component: () => import('JS/app/pages/Contacts'),
        },
        {
            path: '/privacy',
            name: 'privacy',
            component: () => import('JS/app/pages/Privacy'),
        },
        {
            path: '/terms',
            name: 'terms',
            component: () => import('JS/app/pages/Terms'),
        },
    ],
});

export default router;
