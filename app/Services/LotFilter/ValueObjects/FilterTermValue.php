<?php

declare(strict_types=1);

namespace App\Services\LotFilter\ValueObjects;

class FilterTermValue
{
    public readonly int | string | float | null $value;

    public function __construct(int|string|float|null $value)
    {
        $this->value = $value;
    }

    public function __toString(): string
    {
        return (string) $this->value;
    }
}
