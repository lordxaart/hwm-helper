<?php

declare(strict_types=1);

namespace App\HWM\Request;

use App\HWM\Exceptions\HwmException;
use App\HWM\Exceptions\RequestErrors\LoginDuringAnotherScriptLogin;
use App\HWM\Exceptions\RequestError;
use App\HWM\Exceptions\RequestErrors\InavailableProfile;
use App\HWM\Exceptions\RequestErrors\LoginException;
use App\HWM\Exceptions\RequestLimitExceededError;
use App\HWM\Helpers\HwmLink;
use App\HWM\Parsers\HwmParser;
use App\Models\HwmBotAccount;
use App\Services\Http\Client;
use Closure;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Cache;
use Psr\Http\Message\ResponseInterface;

class HwmRequest extends Client
{
    public const LOGIN_CACHE_KEY_PREFIX = 'hwm.parser.loging_as';
    public const MAX_TRIES = 3;

    public int $maxTries;

    protected HwmParser $parser;

    protected ?string $characterLogin = null;

    protected ?string $characterPassword = null;

    protected bool $isLogged = false;

    protected int $currentTries = 0;

    protected string $locale;

    protected ?HwmBotAccount $botAccount = null;

    /**
     * Пост обробка контента (для великих response)
     * Callback type: function (string $content): string
     * @var null|callable Тип
     */
    protected $responsePostHandleCallback = null;

    /**
     * @throws \App\HWM\Exceptions\ParserError
     */
    public function __construct(string $locale = RU, int $maxTries = self::MAX_TRIES, bool $useProxy = false)
    {
        parent::__construct();

        $this->setLocale($locale);

        $this->setParser();

        $this->maxTries = $maxTries;

        if ($useProxy) {
            $this->setRandomProxy();
        }

        $this->setRandomUserAgent();
    }

    public function setLocale(string $locale): void
    {
        $this->locale = $locale;
        $this->config = array_merge($this->config, ['base_uri' => $this->getBaseLink()]);
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function getParser(): HwmParser
    {
        return $this->parser;
    }

    protected function getBaseLink(): string
    {
        return $this->locale === EN ? HwmLink::BASE_LINK_EN : HwmLink::PARSER_LINK_EN;
    }

    /**
     * @throws HwmException
     * @throws InavailableProfile
     * @throws LoginDuringAnotherScriptLogin
     * @throws LoginException
     */
    public function setCredentials(HwmBotAccount $botAccount): void
    {
        $this->botAccount = $botAccount;

        $this->characterLogin = $botAccount->login;
        $this->characterPassword = $botAccount->password;

        $this->setCookieFile();
        $cookieFile = strval($this->cookieFile);

        if (
            file_exists($cookieFile)
            && str_contains(file_get_contents($cookieFile) ?: '', 'PHPSESSID')
        ) {
            $this->isLogged = true;
        } else {
            $this->login();
        }
    }

    /**
     * @throws HwmException
     * @throws InavailableProfile
     * @throws LoginDuringAnotherScriptLogin
     * @throws LoginException
     * @throws RequestError
     */
    public function request(string $method, string $url, array $data = [], bool $loginRedirect = true): ResponseInterface
    {
        $this->currentTries ++;

        if (!$this->checkCookieFile()) {
            $this->clearCookies();
        }

        if ($this->characterLogin) {
            $data['botLogin'] = $this->characterLogin;
        }

        try {
            parent::request($method, $url, $data);
        } catch (RequestError $e) {
            if ($this->botAccount) {
                $this->botAccount->touchErrorRequest();
            }

            //
            if ($this->currentTries < $this->maxTries) {
                sleep(1);
                $this->request($method, $url, $data, $loginRedirect);
            } else {
                throw new RequestLimitExceededError($e, $this->maxTries);
            }
        }

        $this->setParser();

        // If this page is main (/) and we send request to another page - you must login in page
        if ($loginRedirect && $this->parser->isMainPage() && $url !== $this->getBaseLink()) {
            if ($this->botAccount) {
                $this->botAccount->touchErrorRequest();
            }

            $this->isLogged = false;
            $this->login();

            $this->request($method, $url, $data, false);
        }

        $this->checkResponse();

        $this->currentTries = 0;

        if ($this->botAccount) {
            $this->botAccount->touchSuccessRequest();
        }

        return $this->response;
    }

    /**
     * @throws HwmException
     * @throws InavailableProfile
     * @throws LoginDuringAnotherScriptLogin
     * @throws LoginException
     */
    public function login(): bool
    {
        if (!$this->characterLogin || !$this->characterPassword) {
            throw new LoginException(trans('errors.hwm.you_must_setup_login_and_password'));
        }

        if ($this->isLogingNow()) {
            throw new LoginDuringAnotherScriptLogin();
        }

        $this->setLoginCache(true);

        $this->clearCookies();

        if (!$this->parser->getContent() || !$this->parser->getPlivValue()) {
            $this->request('get', $this->getBaseLink());
        }

        $data = [
            'LOGIN_redirect' => 1,
            'pliv' => $this->parser->getPlivValue(),
            'login' => $this->characterLogin,
            'pass' => $this->characterPassword,
        ];

        $this->request('post', HwmLink::LOGIN, $data);

        $this->setLoginCache(false);

        if ($this->parser->isIncorrectLogin()) {
            throw new LoginException(strval($this->parser->getErrorOnPage()));
        }

        if (!$this->parser->isHomePage()) {
            $this->checkResponse();
            $this->clearCookies();

            if ($this->parser->isNeedCaptcha()) {
                throw new LoginException("Нужен ввод каптчи для логина [{$this->botAccount?->login}]");
            }

            throw new LoginException(
                trans('errors.hwm.after_login_redirect_to_not_home_page') . ' [' . $this->lastRequestUrl . ']'
            );
        }

        return $this->isLogged = true;
    }

    protected function setLoginCache(mixed $value): void
    {
        Cache::put(
            self::LOGIN_CACHE_KEY_PREFIX . '.' . $this->getCurrentLoginWithLocale(),
            $value,
            intval($this->baseConfig[RequestOptions::CONNECT_TIMEOUT] + $this->baseConfig[RequestOptions::TIMEOUT]),
        );
    }

    protected function isLogingNow(): bool
    {
        return Cache::get(self::LOGIN_CACHE_KEY_PREFIX . '.' . $this->getCurrentLoginWithLocale(), false);
    }

    /**
     * @throws \App\HWM\Exceptions\ParserError
     */
    protected function setParser(bool $newInstance = false): HwmParser
    {
        if (!isset($this->parser) || $newInstance) {
            $this->parser = new HwmParser($this->content ?? '');
        } else {
            $this->parser->setContent($this->content ?: '');
        }

        return $this->parser;
    }

    protected function getCurrentLoginWithLocale(): ?string
    {
        return $this->characterLogin ? "{$this->locale}_$this->characterLogin" : null;
    }

    protected function setCookieFile(): void
    {
        if (!$this->characterLogin) {
            $this->cookieFile = null;
            return;
        }

        $cookiesPath = storage_path('app/public/cookies');

        if (!file_exists($cookiesPath)) {
            mkdir($cookiesPath);
        }

        $this->cookieFile = "$cookiesPath/{$this->getCurrentLoginWithLocale()}.json";
    }

    protected function checkCookieFile(): bool
    {
        if (!$this->cookieFile) {
            return true;
        }

        // Check valid json
        json_decode(file_get_contents($this->cookieFile) ?: '');
        if (json_last_error() !== JSON_ERROR_NONE) {
            $this->clearCookies();
            return false;
        }

        return true;
    }

    protected function clearCookies(): void
    {
        if ($this->cookieFile) {
            file_put_contents($this->cookieFile, '[]');
        }
    }

    /**
     * @throws InavailableProfile
     */
    protected function checkResponse(): void
    {
        if ($this->parser->isWar()) {
            throw new InavailableProfile(InavailableProfile::IN_WAR);
        }
        if ($this->parser->isTavernGame()) {
            throw new InavailableProfile(InavailableProfile::IN_CARD_GAME);
        }
        if ($this->parser->isMovingInMap()) {
            throw new InavailableProfile(InavailableProfile::MOVING_IN_MAP);
        }
    }

    protected function setResponse(ResponseInterface $response): void
    {
        $this->response = $response;

        $content = mb_convert_encoding($response->getBody()->getContents(), 'utf-8', 'windows-1251');
        $this->content = is_callable($this->responsePostHandleCallback)
            ? call_user_func($this->responsePostHandleCallback, $content)
        : $content;
    }

    public function setResponsePostHandleCallback(callable|null $callback): self
    {
        $this->responsePostHandleCallback = $callback;

        return $this;
    }
}
