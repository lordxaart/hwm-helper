<?php

declare(strict_types=1);

namespace App\Models\Casts;

use App\HWM\Objects\LotOperation;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class CastLotOperation implements CastsAttributes
{
    public function get($model, string $key, $value, array $attributes): array
    {
        $data = json_decode($value, true) ?: [];
        $operations = [];

        foreach ($data as $item) {
            $operations[] = new LotOperation($item);
        }

        return $operations;
    }

    public function set($model, string $key, $value, array $attributes)
    {
        if (!is_array($value)) {
            throw new \InvalidArgumentException('Invalid value for lot operation, expect array');
        }

        foreach ($value as $item) {
            if (! $item instanceof LotOperation) {
                throw new \InvalidArgumentException('Invalid value for lot operation, expect array of LotOperation objects');
            }
        }

        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }
}
