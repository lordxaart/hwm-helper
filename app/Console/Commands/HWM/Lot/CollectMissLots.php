<?php

declare(strict_types=1);

namespace App\Console\Commands\HWM\Lot;

use App\Console\Commands\Command;
use App\Models\EmptyLot;
use App\Models\Lot;
use App\Services\Parser\Storages\StorageForMissedLots;
use Carbon\Carbon;
use Exception;

class CollectMissLots extends Command
{
    private const MAX_STORAGE_CAPACITY = 100000;
    private const FETCHING_STEP = 1000;

    public $signature = 'hwm:lot:collect_missed_lots';
    public $description = 'Collect missed lots in redis';

    private StorageForMissedLots $storage;

    public function __construct()
    {
        parent::__construct();

        $this->storage = new StorageForMissedLots();
    }

    /**
     * @throws Exception
     */
    public function handle(): int
    {
        $this->log('Start');

        // get lot id from last 3 month
        $from = now()->subMonths(3);
        $count = $this->collectFrom($from);

        $this->log('End. Count missed lots ' . $count);

        return 0;
    }

    private function collectFrom(Carbon $from): int
    {
        $count = 0;

        $min = Lot::where('started_at', '>=', $from->format(DATE_FORMAT))->min('lot_id');
        $max = Lot::getMaxEndedEnd()?->lot_id ?: 0;

        if (!$min || !$max) {
            throw new Exception('Not found min lot id from ' . $from);
        }

        for ($i = $max; $i > $min; $i -= self::FETCHING_STEP) {
            $start = $i - self::FETCHING_STEP + 1;
            $end = $i;

            if ($start < $min) {
                $start = $min;
            }

            $this->log("Fetching from $start to $end");
            $fetchIds = Lot::whereBetween('lot_id', [$start, $end])
                ->pluck('lot_id')
                ->toArray();

            $emptyIds = EmptyLot::whereBetween('lot_id', [$start, $end])
                ->pluck('lot_id')
                ->toArray();

            $needIds = range($start, $end);
            $missedIds = array_diff($needIds, $fetchIds);
            $missedIds = array_diff($missedIds, $emptyIds); // clear empty lots from array

            foreach ($missedIds as $id) {
                if ($id <= $min) {
                    break 2;
                }

                if ($this->storage->add(strval($id))) {
                    $count++;
                }
            }

            if ($this->storage->count() >= self::MAX_STORAGE_CAPACITY) {
                break;
            }
        }

        return $count;
    }
}
