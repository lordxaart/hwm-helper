<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSettingsColumnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->json('settings')->nullable();
        });

        $this->copySettingsFromPropertyBag();
    }

    public function copySettingsFromPropertyBag()
    {
        if (!Schema::hasTable('property_bag')) {
            return;
        }

        $records = DB::table('property_bag')->get();

        foreach ($records as $record) {
            if ($record->resource_type !== 'App\Models\User') {
                continue;
            }

            $user = \App\Models\User::find($record->resource_id);

            if (!$user) {
                continue;
            }

            $value = preg_replace('/[^0-9]/', '', $record->value);

            $user->settings($record->key, $value);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('settings');
        });
    }
}
