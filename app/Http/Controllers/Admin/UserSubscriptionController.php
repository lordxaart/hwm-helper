<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Core\ValueObject\UserId;
use App\Http\Controllers\Admin\Models\UserSubscriptionModel;
use App\Services\Subscriptions\SubscriptionService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use http\Client\Curl\User;

/**
 * Class UserCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class UserSubscriptionController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation { destroy as traitDestroy; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }
//    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * @throws \Exception
     */
    public function setup(): void
    {
        $this->crud->setModel(UserSubscriptionModel::class);
        $this->crud->with(['user']);
        $this->crud->orderBy('started_at', 'desc');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/users/subscriptions');

        $this->crud->setEntityNameStrings('subscription', 'subscriptions');
    }

    protected function setupListOperation()
    {
        $this->crud->addButtonFromView('top_page', 'subscriptions', 'subscriptions');
        $this->crud->addColumns([
            [
                'type' => 'custom_html',
                'label' => 'User',
                'value' => fn($entity) => "<a href='/admin/users/{$entity->user_id}/show'>{$entity->user->email}</a>"
            ],
            [
                'label' => 'Subscription',
                'type' => 'closure',
                'function' => fn(UserSubscriptionModel $entity) => $entity->subscription->name . ' (' . $entity->plan->name . ')'
            ],
            [
                'label' => 'Ended',
                'type' => 'closure',
                'function' => fn(UserSubscriptionModel $entity) => $entity->ended_at->isoFormat(
                        config('backpack.base.default_date_format')
                    ) . ' (' . $entity->ended_at->diffForHumans() . ')'
            ],
            [
                'label' => 'Trial',
                'type' => 'closure',
                'function' => fn(UserSubscriptionModel $entity) => $entity->isTrialActive(
                ) ? $entity->trial_ended_at->isoFormat(
                        config('backpack.base.default_date_format')
                    ) . ' (' . $entity->trial_ended_at->diffForHumans() . ')' : 'No'
            ],
            [
                'label' => 'Active',
                'type' => 'closure',
                'function' => fn(UserSubscriptionModel $entity) => $entity->isActive() ? 'Yes' : 'No',
            ],
            [
                'name' => 'started_at',
                'label' => 'Created',
                'type' => 'date',
            ],
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->addField([
            'type' => 'view',
            'name' => 'user',
            'view' => 'crud::fields.create_subscription',
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->crud->addField([
            'type' => 'select',
            'label' => 'User',
            'name' => 'user_id',
            'attribute' => 'email',
            'attributes' => [
                'disabled' => 'disabled'
            ],
        ]);
        $this->crud->addField([
            'type' => 'select',
            'label' => 'Subscription',
            'name' => 'subscription_id',
            'attribute' => 'name',
            'attributes' => [
                'disabled' => 'disabled'
            ],
        ]);
        $this->crud->addField([
            'type' => 'select',
            'label' => 'Plan',
            'name' => 'plan_subscription_id',
            'entity' => 'plan',
            'attribute' => 'name',
            'attributes' => [
                'disabled' => 'disabled'
            ],
        ]);

        $this->crud->addField([
            'type' => 'date',
            'name' => 'started_at',
            'attributes' => [
                'disabled' => 'disabled'
            ],
        ]);
        $this->crud->addField([
            'type' => 'date',
            'name' => 'trial_ended_at',
            'attributes' => [
                'disabled' => 'disabled'
            ],
        ]);
        $this->crud->addField([
            'type' => 'date',
            'name' => 'ended_at',
        ]);
    }

    public function store()
    {
        $this->crud->hasAccessOrFail('create');

        // execute the FormRequest authorization and validation, if one is required
        $request = $this->crud->validateRequest();

        // register any Model Events defined on fields
        $this->crud->registerFieldEvents();

        try {
            /** @var SubscriptionService $service */
            $service = app(SubscriptionService::class);
            $service->subscribe(new UserId((int)$request->user_id), (int)$request->plan_id, $request->has('with_trial'));
            // show a success message
            \Alert::success(trans('backpack::crud.insert_success'))->flash();
            // save the redirect choice for next time
            $this->crud->setSaveAction();
        } catch(\Throwable $exception) {
            \Alert::error($exception->getMessage())->flash();
        }

        return $this->crud->performSaveAction();
    }

    public function update()
    {
        $this->crud->hasAccessOrFail('update');

        // execute the FormRequest authorization and validation, if one is required
        $request = $this->crud->validateRequest();

        // register any Model Events defined on fields
        $this->crud->registerFieldEvents();

        try {
            // update the row in the db
            $subId = (int)$request->get($this->crud->model->getKeyName());
            /** @var SubscriptionService $service */
            $service = app(SubscriptionService::class);
            $endAt = $request->ended_at ? new \DateTimeImmutable($request->ended_at) : null;
            $trialEndedAt = $request->trial_ended_at ? new \DateTimeImmutable($request->trial_ended_at) : null;
            if ($endAt || $trialEndedAt) {
                $service->updateSubscription(subId: $subId, endedAt: $endAt, trialEndedAt: $trialEndedAt);
            }
            // show a success message
            \Alert::success(trans('backpack::crud.update_success'))->flash();
            // save the redirect choice for next time
            $this->crud->setSaveAction();
        } catch(\Throwable $exception) {
            \Alert::error($exception->getMessage())->flash();
        }

        return $this->crud->performSaveAction($subId);
    }

    public function destroy($id): ?bool
    {
        /** @var SubscriptionService $service */
        $service = app(SubscriptionService::class);
        return $service->unsubscribe((int)$id);
    }
}
