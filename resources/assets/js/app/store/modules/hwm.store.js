import i18n from 'JS/app/utils/localization';
// import elements from '../../../../hwm/elements.json';
const elements = require('../../../../hwm/elements.json');
const resources = require('../../../../hwm/resources.json');
const map = require('../../../../hwm/map.json');

const initialState = {
    elements: elements.map((element) => ({
        hwmId: element,
        title: i18n.t(`hwm.elements.${element}`),
        image: `/images/icons/hwm/elements/${element}.png`,
    })),
    resources: resources.map((resource) => ({
        hwmId: resource,
        title: i18n.t(`hwm.resources.${resource}`),
        image: `/images/icons/hwm/resources/${resource}.png`,
    })),
    mapAreas: map.map((area) => ({
        hwmId: area.hwmId,
        title: i18n.t(`hwm.map.areas.${area.hwmId}`),
    })),
    isLoading: false,
};

const getters = {
    getElements(state) {
        return state.elements;
    },
    getResources(state) {
        return state.resources;
    },
    getMapAreas: (state) => state.mapAreas,
    getIsLoading: (state) => state.isLoading,
};

const actions = {
    //
};

const mutations = {
    //
};

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations,
};
