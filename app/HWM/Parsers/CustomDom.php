<?php

declare(strict_types=1);

namespace App\HWM\Parsers;

use App\HWM\Exceptions\ParserError;
use PHPHtmlParser\Dom;

class CustomDom extends Dom
{
    /**
     * @param string $selector
     * @param int|null $nth
     * @return mixed|CustomDom|Dom\Collection|null
     * @throws ParserError
     */
    public function find(string $selector, int $nth = null): mixed
    {
        try {
            return parent::find($selector, $nth);
        } catch (\Exception $e) {
            throw new ParserError($e->getMessage());
        }
    }

    public function getOriginalContent(): string
    {
        return $this->raw;
    }
}
