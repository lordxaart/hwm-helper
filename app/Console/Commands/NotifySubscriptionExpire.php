<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Http\Controllers\Admin\Models\UserSubscriptionModel;
use App\Models\User;
use App\Notifications\User\Subscriptions\SubscriptionExpires;

class NotifySubscriptionExpire extends Command
{
    private const NOTIFY_SUB_EXPIRE_DAYS = 3;

    protected $signature = 'notify:subscription:expire {--trial}';

    /** @var string  */
    protected $description = 'Generate public/manifest.json file';

    public function handle()
    {
        $this->handleActiveSubs();
    }

    private function handleActiveSubs(): void
    {
        UserSubscriptionModel::where('ended_at', '>=', date('Y-m-d H:i:s'))
            ->chunk(100, function ($items) {
                /** @var UserSubscriptionModel $sub */
                foreach ($items as $sub) {
                    $this->validateSubscriptionExpire($sub);
                }
            });
    }

    private function validateSubscriptionExpire(UserSubscriptionModel $model)
    {
        /** @var User $user */
        $user = $model->user;
        $notify = new SubscriptionExpires($model->id);
        // Subscription ended today
        if (now()->diffInDays($model->ended_at) === 0) {
            $user->notify($notify);
        }

        // Subscription ends in NOTIFY_SUB_EXPIRE_DAYS days
        if (now()->diffInDays($model->ended_at) === self::NOTIFY_SUB_EXPIRE_DAYS) {
            $user->notify($notify);
        }
    }
}
