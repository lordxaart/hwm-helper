@if(!isAcceptedCookies())
    <div class="alert text-center cookiealert" role="alert">
        <div class="cookiealert-container">
            {{ __('app.main.we_use_cookies') }} <a href="{{ route('privacy') }}" cl target="_blank">{{ __('app.main.learn_more') }}</a>
            <button type="button" class="btn btn-primary btn-sm acceptcookies ml-2" aria-label="Close">
                {{ __('app.main.i_agree') }}
            </button>
        </div>
    </div>
@endif
