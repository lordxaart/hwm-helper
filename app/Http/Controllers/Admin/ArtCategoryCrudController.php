<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

class ArtCategoryCrudController extends BaseCategoryCrudController
{
    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel('App\Models\ArtCategory');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/art-categories');
        $this->crud->setEntityNameStrings('art-category', 'art-categories');

        parent::setup();
    }
}
