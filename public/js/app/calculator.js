(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["calculator"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/global/ArtBlock.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/components/global/ArtBlock.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! JS/app/utils/helpers */ "./resources/assets/js/app/utils/helpers.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    art: Object
  },
  methods: {
    errorLoadArtImg(event) {
      Object(JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_0__["errorLoadArtImg"])(event);
    },

    close() {
      this.$emit('close');
    }

  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/pages/calculator/Result.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/components/pages/calculator/Result.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    results: Array,
    currentMode: Number
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/pages/Calculator.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/pages/Calculator.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var lodash_isEqual__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash/isEqual */ "./node_modules/lodash/isEqual.js");
/* harmony import */ var lodash_isEqual__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash_isEqual__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash/cloneDeep */ "./node_modules/lodash/cloneDeep.js");
/* harmony import */ var lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var JS_app_components_pages_calculator_Result__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! JS/app/components/pages/calculator/Result */ "./resources/assets/js/app/components/pages/calculator/Result.vue");
/* harmony import */ var JS_app_components_global_ArtBlock__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! JS/app/components/global/ArtBlock */ "./resources/assets/js/app/components/global/ArtBlock.vue");
/* harmony import */ var JS_app_components_global_SelectArt__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! JS/app/components/global/SelectArt */ "./resources/assets/js/app/components/global/SelectArt.vue");
/* harmony import */ var JS_app_config__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! JS/app/config */ "./resources/assets/js/app/config/index.js");
/* harmony import */ var JS_app_utils_cache__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! JS/app/utils/cache */ "./resources/assets/js/app/utils/cache.js");
/* harmony import */ var JS_app_utils_clipboard__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! JS/app/utils/clipboard */ "./resources/assets/js/app/utils/clipboard.js");
/* harmony import */ var JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! JS/app/utils/helpers */ "./resources/assets/js/app/utils/helpers.js");
/* harmony import */ var JS_app_utils_notify__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! JS/app/utils/notify */ "./resources/assets/js/app/utils/notify.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//












const initData = () => ({
  loadingCalculator: false,
  SelectArtComponent: null,
  currentMode: 1,
  modesNames: {
    1: 'optimal_scrapping',
    '-1': 'optimal_price'
  },
  art: {
    price: 0,
    currentStrength: 0,
    baseStrength: 0,
    repair: 0,
    resonablePricePerFight: 0,
    pricePerFight: 0
  },
  selectedArt: null,
  selectArtModalId: 'select_art_calculator',
  masters: [],
  mastersUrl: [],
  maxMasters: 10,
  mastersSource: 'custom',
  includeMasterCommission: false,
  masterCommission: 1,
  checkSaveMastersToStorage: true,
  checkAutoCalculate: true,
  showCraftBlock: false,
  craftPrice: 0,
  craftString: '',
  userMasters: [],
  results: []
});

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    ArtBlock: JS_app_components_global_ArtBlock__WEBPACK_IMPORTED_MODULE_4__["default"],
    CraftBlock: () => __webpack_require__.e(/*! import() */ 1).then(__webpack_require__.bind(null, /*! JS/app/components/pages/calculator/Craft */ "./resources/assets/js/app/components/pages/calculator/Craft.vue")),
    ResultBlock: JS_app_components_pages_calculator_Result__WEBPACK_IMPORTED_MODULE_3__["default"]
  },

  metaInfo() {
    return {
      title: this.$t('app.main.calculator'),
      meta: [{
        vmid: 'description',
        name: 'description',
        content: this.$t('app.meta.calculator_desc')
      }]
    };
  },

  data() {
    return initData();
  },

  methods: _objectSpread(_objectSpread({
    async init() {
      this.loadingCalculator = true;

      try {
        if (JS_app_utils_cache__WEBPACK_IMPORTED_MODULE_7__["default"].has(JS_app_config__WEBPACK_IMPORTED_MODULE_6__["default"].cache.keys.calculator_save_data_status)) {
          this.checkSaveMastersToStorage = JS_app_utils_cache__WEBPACK_IMPORTED_MODULE_7__["default"].get(JS_app_config__WEBPACK_IMPORTED_MODULE_6__["default"].cache.keys.calculator_save_data_status);
        }

        if (JS_app_utils_cache__WEBPACK_IMPORTED_MODULE_7__["default"].has(JS_app_config__WEBPACK_IMPORTED_MODULE_6__["default"].cache.keys.calculator_auto_calculate)) {
          this.checkAutoCalculate = JS_app_utils_cache__WEBPACK_IMPORTED_MODULE_7__["default"].get(JS_app_config__WEBPACK_IMPORTED_MODULE_6__["default"].cache.keys.calculator_auto_calculate);
        }

        if (JS_app_utils_cache__WEBPACK_IMPORTED_MODULE_7__["default"].has(JS_app_config__WEBPACK_IMPORTED_MODULE_6__["default"].cache.keys.calculator_master_commission)) {
          this.includeMasterCommission = JS_app_utils_cache__WEBPACK_IMPORTED_MODULE_7__["default"].get(JS_app_config__WEBPACK_IMPORTED_MODULE_6__["default"].cache.keys.calculator_master_commission);
        }

        if (this.checkSaveMastersToStorage && JS_app_utils_cache__WEBPACK_IMPORTED_MODULE_7__["default"].has(JS_app_config__WEBPACK_IMPORTED_MODULE_6__["default"].cache.keys.masters)) {
          this.masters = JS_app_utils_cache__WEBPACK_IMPORTED_MODULE_7__["default"].get(JS_app_config__WEBPACK_IMPORTED_MODULE_6__["default"].cache.keys.masters);
        }

        if (this.userMasters.length && this.hwm.master.checkMaster(this.userMasters)) {
          this.masters = [...this.userMasters];
          this.mastersSource = 'user';
        }

        await this.parseUrl();

        if (!this.masters.length) {
          this.addNewMaster();
        }

        this.calculate();
      } catch (e) {
        this.loadingCalculator = false;
        throw e;
      }

      this.loadingCalculator = false;
    },

    toggleMode() {
      this.currentMode *= -1;
      this.rotateTitle();
    },

    loadSelectArt() {
      if (!this.SelectArtComponent) {
        this.SelectArtComponent = JS_app_components_global_SelectArt__WEBPACK_IMPORTED_MODULE_5__["default"];
      }

      this.openModal(this.selectArtModalId);
    },

    calculate(withErrors = null) {
      if (withErrors !== true && !this.checkAutoCalculate) {
        return;
      }

      this.loadingCalculator = true;
      this.results = [];
      const errors = this.checkFields();

      if (errors.length) {
        this.loadingCalculator = false;

        if (withErrors === true) {
          let message = '';
          errors.forEach(error => {
            message += `<p>${error}</p>`;
          });
          JS_app_utils_notify__WEBPACK_IMPORTED_MODULE_10__["default"].warning(message, this.$t('app.calculator.calcualtion_error'));
        }

        return;
      }

      const art = _objectSpread({}, this.art);

      art.price += this.currentCraftPrice;
      const masters = lodash_cloneDeep__WEBPACK_IMPORTED_MODULE_1___default()(this.masters);

      if (this.includeMasterCommission) {
        masters.map(master => {
          const m = master;
          m.pRate += this.masterCommission;
          return m;
        });
      }

      if (this.defaultMode) {
        this.results = this.hwm.calculator(art, masters);
      } else if (this.reverseMode) {
        this.results = this.hwm.calculatorReverse(art, masters);
      }

      this.loadingCalculator = false;
    },

    addNewMaster() {
      if (this.masters.length >= this.maxMasters) {
        JS_app_utils_notify__WEBPACK_IMPORTED_MODULE_10__["default"].warning(this.$t('app.calculator.max_masters', {
          max: this.maxMasters
        }));
        return false;
      }

      this.masters.push(this.hwm.master.createNew());
      this.changeMasters();
      return true;
    },

    deleteMaster(index) {
      if (index <= 0) {
        JS_app_utils_notify__WEBPACK_IMPORTED_MODULE_10__["default"].warning(this.$t('app.calculator.need_one_more_master'));
        return false;
      }

      this.masters.splice(index, 1);
      this.changeMasters();
      return true;
    },

    changeMasters() {
      this.masters.map(master => {
        const m = master;
        m.pRate = Object(JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_9__["sanitazeInt"])(master.pRate);
        m.pRepair = Object(JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_9__["sanitazeInt"])(master.pRepair);
        return m;
      });

      if (this.checkSaveMastersToStorage && this.mastersSource === 'custom') {
        JS_app_utils_cache__WEBPACK_IMPORTED_MODULE_7__["default"].set(JS_app_config__WEBPACK_IMPORTED_MODULE_6__["default"].cache.keys.masters, this.uniqueMasters);
      }

      this.calculate();
    },

    canDeleteThisMater(index) {
      if (index === 0) return false;

      if (this.mastersSource === 'custom') {
        return true;
      }

      if (index >= this.userMasters.length && (this.mastersSource === 'user' || this.mastersSource === 'url')) {
        return true;
      }

      return false;
    },

    checkFields() {
      this.art.baseStrength = Object(JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_9__["sanitazeInt"])(this.art.baseStrength);
      this.art.currentStrength = Object(JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_9__["sanitazeInt"])(this.art.currentStrength);
      this.art.repair = Object(JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_9__["sanitazeInt"])(this.art.repair);
      this.art.resonablePricePerFight = Object(JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_9__["sanitazeFloat"])(this.art.resonablePricePerFight);
      this.art.pricePerFight = Object(JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_9__["sanitazeFloat"])(this.art.pricePerFight);
      const errors = [];

      if (this.defaultMode) {
        if (this.art.baseStrength <= 0) {
          errors.push(this.$t('app.calculator.attribute_must_be_more_then_zero', {
            attribute: this.$t('app.art.columns.base_strength'),
            value: this.art.baseStrength
          }));
        }

        if (this.art.repair <= 0) {
          errors.push(this.$t('app.calculator.attribute_must_be_more_then_zero', {
            attribute: this.$t('app.art.repair_cost'),
            value: this.art.repair
          }));
        }

        if (!this.art.resonablePricePerFight && this.art.price <= 0) {
          errors.push(this.$t('app.calculator.attribute_must_be_more_then_zero', {
            attribute: this.$t('app.art.base_price'),
            value: this.art.price
          }));
        }
      }

      if (this.reverseMode) {
        if (this.art.pricePerFight <= 0) {
          errors.push(this.$t('app.calculator.attribute_must_be_more_then_zero', {
            attribute: this.$t('app.art.columns.price_per_fight'),
            value: this.art.pricePerFight
          }));
        }

        if (this.art.baseStrength <= 0) {
          errors.push(this.$t('app.calculator.attribute_must_be_more_then_zero', {
            attribute: this.$t('app.art.columns.base_strength'),
            value: this.art.baseStrength
          }));
        }

        if (this.art.repair <= 0) {
          errors.push(this.$t('app.calculator.attribute_must_be_more_then_zero', {
            attribute: this.$t('app.art.repair_cost'),
            value: this.art.repair
          }));
        }
      }

      return errors;
    },

    checkCurrentStrength() {
      if (this.art.currentStrength > this.art.baseStrength) {
        this.art.baseStrength = this.art.currentStrength;
      }
    },

    checkBaseStrength() {
      if (this.art.baseStrength < this.art.currentStrength) {
        this.art.currentStrength = this.art.baseStrength;
      } else if (this.art.currentStrength === 0 && this.art.baseStrength > 0) {
        this.art.currentStrength = this.art.baseStrength;
      }
    },

    refreshAllData() {
      const answer = window.confirm(this.$t('app.main.you_sure_to_clear_all'));

      if (!answer) {
        return;
      }

      this.data = initData();
      this.init();
    },

    changeSelectedArt(art) {
      this.hideModal(this.selectArtModalId);
      this.loadArt(art, true);
    },

    loadArt(art, rewriteData = true) {
      if (art && art.hwmId) {
        if (!this.art.currentStrength || rewriteData) {
          this.art.currentStrength = art.strength;
        }

        if (!this.art.baseStrength || rewriteData) {
          this.art.baseStrength = art.strength;
        }

        if (!this.art.repair || rewriteData) {
          this.art.repair = art.repair;
        }

        if (art.shopPrice && (!this.art.price || rewriteData)) {
          this.art.price = art.shopPrice;
        }

        if (!this.art.price) {
          this.$refs.price.$refs.input.focus();
        }

        this.selectedArt = art;
      }

      this.calculate();
    },

    async loadArtById(hwmId) {
      const art = await this.findArtByHwmId(hwmId);

      if (art) {
        this.loadArt(art, false);
      }
    },

    copyLinkToClipboard(withMasters = false) {
      Object(JS_app_utils_clipboard__WEBPACK_IMPORTED_MODULE_8__["default"])(this.getDataAsUrlString(withMasters));
    },

    getDataAsUrlString(withMasters = false) {
      let {
        search
      } = window.location;
      const params = {
        price: this.art.price,
        current_strength: this.art.currentStrength,
        base_strength: this.art.baseStrength,
        repair: this.art.repair,
        price_per_fight: this.defaultMode ? this.art.resonablePricePerFight : 0,
        mode: this.currentMode
      };

      if (this.reverseMode) {
        params.price_per_fight = this.art.pricePerFight;
      }

      if (withMasters) {
        params.masters = this.mastersAsUrlString;
      }

      if (this.selectedArt) {
        params.art = this.selectedArt.hwmId;
      }

      if (this.includeMasterCommission) {
        params.commission = 1;
      }

      if (this.currentCraftString()) {
        params.craft = this.currentCraftString();
      }

      search = Object(JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_9__["objectToQueryString"])(params);
      return window.location.origin + window.location.pathname + search;
    },

    async parseUrl() {
      const searchString = window.location.search;

      if (!searchString) {
        return;
      }

      const paramsString = decodeURIComponent(searchString.substr(1));
      const params = new URLSearchParams(paramsString);

      if (params.get('mode')) {
        const mode = Number.parseInt(params.get('mode'), 10);

        if (mode === 1 || mode === -1) {
          this.currentMode = mode;
        }
      }

      if (params.get('art')) {
        if (!this.selectedArt) {
          await this.loadArtById(params.get('art'));
        }

        if (this.selectedArt) {
          if (params.get('art_uid')) {
            this.selectedArt.art_uid = params.get('art_uid');
          }

          if (params.get('art_crc')) {
            this.selectedArt.art_crc = params.get('art_crc');
          }
        }
      }

      if (params.get('price')) this.art.price = Object(JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_9__["sanitazeInt"])(params.get('price'));
      if (params.get('current_strength')) this.art.currentStrength = Object(JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_9__["sanitazeInt"])(params.get('current_strength'));
      if (params.get('base_strength')) this.art.baseStrength = Object(JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_9__["sanitazeInt"])(params.get('base_strength'));
      if (params.get('repair')) this.art.repair = Object(JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_9__["sanitazeInt"])(params.get('repair'));
      if (params.get('commission')) this.includeMasterCommission = true;

      if (params.get('price_per_fight')) {
        if (this.defaultMode) {
          this.art.resonablePricePerFight = Object(JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_9__["sanitazeFloat"])(params.get('price_per_fight'));
        }

        if (this.reverseMode) {
          this.art.pricePerFight = Object(JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_9__["sanitazeFloat"])(params.get('price_per_fight'));
        }
      }

      if (params.get('craft')) {
        this.showCraftBlock = true;
        this.craftString = params.get('craft');
      }

      if (params.get('masters')) {
        const vm = this;
        const items = params.get('masters').split(';');
        items.forEach(item => {
          const arr = item.split('/');

          if (arr.length === 2) {
            const master = vm.hwm.master.createNew();
            master.pRepair = Object(JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_9__["sanitazeInt"])(arr[0]);
            master.pRate = Object(JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_9__["sanitazeInt"])(arr[1]);
            vm.mastersUrl.push(master);
          }
        });

        if (this.mastersUrl.length) {
          this.masters = [...this.mastersUrl];
          this.mastersSource = 'url';
        }
      }
    },

    rotateTitle() {
      if (this.defaultMode) {
        this.$refs.calculatorTitle.classList.remove('rotate-reverse-once');
        this.$refs.calculatorTitle.classList.add('rotate-once');
      } else if (this.reverseMode) {
        this.$refs.calculatorTitle.classList.remove('rotate-once');
        this.$refs.calculatorTitle.classList.add('rotate-reverse-once');
      }
    },

    currentCraftString() {
      return this.$refs.craft ? this.$refs.craft.craftObject.asString() : '';
    }

  }, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapActions"])('Arts', ['findArtByHwmId'])), {}, {
    setOgTags() {
      if (this.selectedArt) {
        this.metaSetTitle(this.selectedArt.title);
        this.metaSetImage(this.selectedArt.images.default.fullUrl);
      } else {
        this.metaSetTitle();
        this.metaSetImage();
      }
    }

  }),
  watch: {
    checkSaveMastersToStorage() {
      JS_app_utils_cache__WEBPACK_IMPORTED_MODULE_7__["default"].set(JS_app_config__WEBPACK_IMPORTED_MODULE_6__["default"].cache.keys.calculator_save_data_status, this.checkSaveMastersToStorage);
    },

    checkAutoCalculate() {
      JS_app_utils_cache__WEBPACK_IMPORTED_MODULE_7__["default"].set(JS_app_config__WEBPACK_IMPORTED_MODULE_6__["default"].cache.keys.calculator_auto_calculate, this.checkAutoCalculate);
    },

    showCraftBlock() {
      this.calculate();
    },

    currentMode() {
      this.calculate();
    },

    selectedArt() {
      this.setOgTags();
    },

    includeMasterCommission() {
      if (!this.loadingCalculator) {
        JS_app_utils_cache__WEBPACK_IMPORTED_MODULE_7__["default"].set(JS_app_config__WEBPACK_IMPORTED_MODULE_6__["default"].cache.keys.calculator_master_commission, this.includeMasterCommission);
      }

      this.calculate();
    }

  },
  computed: _objectSpread({
    currentCraftPrice() {
      return this.showCraftBlock ? Object(JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_9__["sanitazeInt"])(this.craftPrice) : 0;
    },

    uniqueMasters() {
      const uniqueMasters = [];
      this.masters.forEach((master, index) => {
        if (index === 0) {
          uniqueMasters.push(master);
        } else {
          const status = uniqueMasters.find(item => lodash_isEqual__WEBPACK_IMPORTED_MODULE_0___default()(item, master));

          if (!status) {
            uniqueMasters.push(master);
          }
        }
      });
      return uniqueMasters;
    },

    mastersAsUrlString() {
      let mastersString = '';
      this.masters.forEach(master => {
        if (!mastersString) {
          mastersString += `${master.pRepair}/${master.pRate};`;
        } else {
          mastersString += `${master.pRepair}/${master.pRate}`;
        }
      });
      return mastersString;
    },

    canAddNewMaster() {
      return this.masters.length <= 9;
    },

    defaultMode() {
      return this.currentMode === 1;
    },

    reverseMode() {
      return this.currentMode === -1;
    }

  }, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapGetters"])('Arts', {
    arts: 'getArts',
    loadingArts: 'getLoadingArts'
  })),

  mounted() {
    this.init();
  },

  created() {
    this.modesNames['1'] = this.$t('app.calculator.optimal_scrapping');
    this.modesNames['-1'] = this.$t('app.calculator.optimal_price');
  }

});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/global/ArtBlock.vue?vue&type=template&id=3eccec46&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/components/global/ArtBlock.vue?vue&type=template&id=3eccec46& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "card art-info" }, [
    _c(
      "div",
      { staticClass: "card-body" },
      [
        _c(
          "a",
          {
            directives: [
              {
                name: "tooltip",
                rawName: "v-tooltip",
                value: _vm.$t("app.art.open_art_in_hwm"),
                expression: "$t('app.art.open_art_in_hwm')"
              }
            ],
            attrs: { href: _vm.hwm.link.art(_vm.art.hwmId), target: "_blank" }
          },
          [
            _c("img", {
              staticClass: "art-img",
              attrs: {
                src: _vm.art.images.small.fullUrl,
                alt: _vm.art.hwmId,
                title: _vm.art.title
              },
              on: { error: _vm.errorLoadArtImg }
            })
          ]
        ),
        _vm._v(" "),
        _c("h6", { staticClass: "card-title d-inline-block" }, [
          _c(
            "a",
            {
              directives: [
                {
                  name: "tooltip",
                  rawName: "v-tooltip",
                  value: _vm.$t("app.art.open_market_in_hwm"),
                  expression: "$t('app.art.open_market_in_hwm')"
                }
              ],
              attrs: {
                href: _vm.hwm.link.market(
                  _vm.art.hwmId,
                  _vm.art.marketCategoryId
                ),
                target: "_blank"
              }
            },
            [_vm._v(_vm._s(_vm.art.title) + "\n            ")]
          )
        ]),
        _vm._v(" "),
        _c("icon", {
          staticClass: "close",
          attrs: { name: "close", size: "sm" },
          nativeOn: {
            click: function($event) {
              return _vm.close($event)
            }
          }
        })
      ],
      1
    ),
    _vm._v(" "),
    _c("ul", { staticClass: "list-group list-group-flush" }, [
      _c("li", { staticClass: "list-group-item" }, [
        _vm._v(_vm._s(_vm.$t("app.art.columns.base_strength")) + ": "),
        _c("strong", [
          _vm._v(_vm._s(_vm.art.strength) + "/" + _vm._s(_vm.art.strength))
        ])
      ]),
      _vm._v(" "),
      _c("li", { staticClass: "list-group-item" }, [
        _vm._v(_vm._s(_vm.$t("app.art.repair_cost")) + ": "),
        _c("strong", [_vm._v(_vm._s(_vm.art.repair))])
      ]),
      _vm._v(" "),
      _vm.art.fromShop
        ? _c("li", { staticClass: "list-group-item" }, [
            _vm._v(_vm._s(_vm.$t("app.art.base_price")) + ": "),
            _c("strong", [_vm._v(_vm._s(_vm.art.shopPrice))])
          ])
        : _vm._e(),
      _vm._v(" "),
      _vm.art.fromShop
        ? _c("li", { staticClass: "list-group-item" }, [
            _vm._v(_vm._s(_vm.$t("app.art.base_price_per_fight")) + ": "),
            _c("strong", [_vm._v(_vm._s(_vm.art.pricePerFight))])
          ])
        : _vm._e()
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/pages/calculator/Result.vue?vue&type=template&id=36d213c6&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/components/pages/calculator/Result.vue?vue&type=template&id=36d213c6& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "results" }, [
    _c(
      "div",
      { staticClass: "text-center" },
      [
        _vm.$parent.reverseMode
          ? _c(
              "v-popover",
              { staticClass: "align-top", attrs: { trigger: "hover click" } },
              [
                _c("icon", {
                  staticClass: "tooltip-target small text-muted info",
                  attrs: { name: "info" }
                }),
                _vm._v(" "),
                _c("template", { slot: "popover" }, [
                  _vm._v(
                    "\n                " +
                      _vm._s(_vm.$t("app.calculator.info_about_result")) +
                      "\n            "
                  )
                ])
              ],
              2
            )
          : _vm._e(),
        _vm._v(" "),
        _c("h5", { staticClass: "d-inline-block ml-1" }, [
          _vm._v(
            "\n            " +
              _vm._s(_vm.$t("app.calculator.result")) +
              "\n        "
          )
        ])
      ],
      1
    ),
    _vm._v(" "),
    _c("div", { staticClass: "table-responsive-md" }, [
      _c("table", { staticClass: "table light table-striped table-bordered" }, [
        _c("thead", [
          _c("tr", [
            _c("th", [_vm._v(_vm._s(_vm.$t("app.art.master")))]),
            _vm._v(" "),
            _vm.$parent.reverseMode
              ? _c("th", [
                  _c("strong", [
                    _vm._v(_vm._s(_vm.$t("app.calculator.optimal_cost")))
                  ])
                ])
              : _vm._e(),
            _vm._v(" "),
            _c("th", [
              _vm._v(
                "\n                        " +
                  _vm._s(_vm.$t("app.art.columns.price_per_fight")) +
                  "\n                    "
              )
            ]),
            _vm._v(" "),
            _c("th", [_vm._v(_vm._s(_vm.$t("app.art.strength")))]),
            _vm._v(" "),
            _c("th", [_vm._v(_vm._s(_vm.$t("app.art.number_of_fights")))]),
            _vm._v(" "),
            _c("th", [_vm._v(_vm._s(_vm.$t("app.art.total_cost")))])
          ])
        ]),
        _vm._v(" "),
        _c(
          "tbody",
          _vm._l(_vm.results, function(result, index) {
            return _c("tr", { key: index }, [
              _c("td", [_vm._v("# " + _vm._s(index + 1))]),
              _vm._v(" "),
              _vm.$parent.reverseMode
                ? _c("td", [_vm._v(_vm._s(result.optimalPrice))])
                : _vm._e(),
              _vm._v(" "),
              _c("td", { staticClass: "text-nowrap" }, [
                _vm.$parent.defaultMode
                  ? _c(
                      "div",
                      [
                        _c("strong", [
                          _vm._v(_vm._s(result.pricePerFight.toFixed(2)))
                        ]),
                        _vm._v(" "),
                        _c(
                          "v-popover",
                          {
                            staticClass: "ml-2 d-inline-block",
                            attrs: { popoverClass: "steps-details" }
                          },
                          [
                            _c(
                              "a",
                              {
                                directives: [
                                  {
                                    name: "tooltip",
                                    rawName: "v-tooltip",
                                    value: _vm.$t(
                                      "app.calculator.show_all_iteration"
                                    ),
                                    expression:
                                      "$t('app.calculator.show_all_iteration')"
                                  }
                                ],
                                staticClass: "tooltip-target text-muted curp",
                                attrs: { href: "#" },
                                on: {
                                  click: function($event) {
                                    $event.preventDefault()
                                  }
                                }
                              },
                              [_c("icon", { attrs: { name: "level_down" } })],
                              1
                            ),
                            _vm._v(" "),
                            _c("template", { slot: "popover" }, [
                              _c("span", [
                                _vm._v(
                                  _vm._s(
                                    _vm.$t("app.calculator.detailed_statistics")
                                  )
                                )
                              ]),
                              _vm._v(" "),
                              _c(
                                "span",
                                {
                                  directives: [
                                    {
                                      name: "close-popover",
                                      rawName: "v-close-popover"
                                    }
                                  ],
                                  staticClass: "close"
                                },
                                [_c("icon", { attrs: { name: "close" } })],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "table",
                                {
                                  staticClass:
                                    "table table-bordered mt-2 text-muted"
                                },
                                [
                                  _c(
                                    "tbody",
                                    _vm._l(result.steps, function(step, index) {
                                      return _c(
                                        "tr",
                                        {
                                          key: index,
                                          class: {
                                            "text-white":
                                              result.strength === step.strength
                                          }
                                        },
                                        [
                                          _c("td", [
                                            _vm._v(":" + _vm._s(step.step))
                                          ]),
                                          _vm._v(" "),
                                          _c("td", [
                                            _vm._v(
                                              _vm._s(
                                                step.pricePerFight.toFixed(2)
                                              )
                                            )
                                          ]),
                                          _vm._v(" "),
                                          _c("td", [
                                            _vm._v(
                                              "00/" + _vm._s(step.strength)
                                            )
                                          ]),
                                          _vm._v(" "),
                                          _c("td", [
                                            _vm._v(_vm._s(step.fights))
                                          ]),
                                          _vm._v(" "),
                                          _c("td", [_vm._v(_vm._s(step.price))])
                                        ]
                                      )
                                    }),
                                    0
                                  )
                                ]
                              )
                            ])
                          ],
                          2
                        )
                      ],
                      1
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm.$parent.reverseMode
                  ? _c("div", [_vm._v(_vm._s(result.pricePerFight.toFixed(2)))])
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("td", [_vm._v("00/" + _vm._s(result.strength))]),
              _vm._v(" "),
              _c("td", [_vm._v(_vm._s(result.fights))]),
              _vm._v(" "),
              _c("td", [_vm._v(_vm._s(result.price.toFixed(2)))])
            ])
          }),
          0
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/pages/Calculator.vue?vue&type=template&id=58f0862d&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/pages/Calculator.vue?vue&type=template&id=58f0862d& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c("div", { staticClass: "row justify-content-md-center" }, [
      _c(
        "div",
        { staticClass: "col-xl-8 col-lg-10 col-12 calculator" },
        [
          _c(
            "b-card",
            { staticClass: "posr" },
            [
              _c(
                "div",
                {
                  staticClass: "flex-justify-content flex-wrap",
                  attrs: { slot: "header" },
                  slot: "header"
                },
                [
                  _c(
                    "div",
                    { staticClass: "posr d-flex align-items-center" },
                    [
                      _vm.reverseMode
                        ? _c("icon", {
                            directives: [
                              {
                                name: "tooltip",
                                rawName: "v-tooltip",
                                value: _vm.$t(
                                  "app.calculator.info_about_test_mode"
                                ),
                                expression:
                                  "$t('app.calculator.info_about_test_mode')"
                              }
                            ],
                            staticClass: "small info posal text-warning",
                            attrs: { name: "info" }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      _c(
                        "h5",
                        {
                          ref: "calculatorTitle",
                          staticClass: "card-title d-inline-block"
                        },
                        [
                          _vm._v(
                            "\n                            " +
                              _vm._s(_vm.$t("app.main.calculator")) +
                              "\n                        "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c("c-button", {
                        staticClass: "ml-2",
                        class: { "text-primary": _vm.reverseMode },
                        attrs: {
                          type: "link",
                          tooltip:
                            _vm.$t("app.calculator.enable_mode") +
                            ": " +
                            _vm.modesNames[_vm.currentMode * -1],
                          icon: "change"
                        },
                        nativeOn: {
                          click: function($event) {
                            return _vm.toggleMode($event)
                          }
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    [
                      _c(
                        "b-button-group",
                        [
                          _c(
                            "c-button",
                            {
                              attrs: {
                                tooltip: _vm.$t(
                                  "app.calculator.copy_link_on_data"
                                )
                              },
                              on: {
                                click: function($event) {
                                  return _vm.copyLinkToClipboard(false)
                                }
                              }
                            },
                            [_c("icon", { attrs: { name: "clipboard" } })],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "c-button",
                            {
                              attrs: {
                                tooltip: _vm.$t(
                                  "app.calculator.copy_link_on_data_with_masters"
                                )
                              },
                              on: {
                                click: function($event) {
                                  return _vm.copyLinkToClipboard(true)
                                }
                              }
                            },
                            [
                              _c("icon", { attrs: { name: "clipboard" } }),
                              _vm._v(" "),
                              _c("small", [_vm._v(" + ")]),
                              _vm._v(" "),
                              _c("icon", { attrs: { name: "repair" } })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("c-button", {
                            attrs: {
                              icon: "art",
                              tooltip: _vm.$t("app.art.select_art"),
                              loading: _vm.loadingArts
                            },
                            on: { click: _vm.loadSelectArt }
                          }),
                          _vm._v(" "),
                          _c("c-button", {
                            staticClass: "text-danger",
                            attrs: {
                              tooltip: _vm.$t("app.main.reset_all_parameters"),
                              icon: "refresh"
                            },
                            on: { click: _vm.refreshAllData }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "row form-group" }, [
                _c("div", { staticClass: "col-lg-6 col-md-7 col-12" }, [
                  _vm.defaultMode
                    ? _c(
                        "div",
                        { staticClass: "row form-group align-items-center" },
                        [
                          _c("label", { staticClass: "col-sm-6" }, [
                            _vm._v(
                              "\n                                " +
                                _vm._s(_vm.$t("app.art.base_price")) +
                                ":\n                            "
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col-sm-6" },
                            [
                              _c("number-input", {
                                ref: "price",
                                attrs: {
                                  classes: "form-control form-control-sm",
                                  min: 0,
                                  max: 9999999
                                },
                                on: { blur: _vm.calculate },
                                model: {
                                  value: _vm.art.price,
                                  callback: function($$v) {
                                    _vm.$set(_vm.art, "price", $$v)
                                  },
                                  expression: "art.price"
                                }
                              })
                            ],
                            1
                          )
                        ]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.reverseMode
                    ? _c(
                        "div",
                        { staticClass: "row form-group align-items-center" },
                        [
                          _c("label", { staticClass: "col-sm-6" }, [
                            _c(
                              "span",
                              { staticClass: "posr" },
                              [
                                _c("icon", {
                                  staticClass: "small text-muted info posal",
                                  attrs: {
                                    name: "info",
                                    tooltip: _vm.$t(
                                      "app.calculator.info_to_price_per_fight_mode_0"
                                    )
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c("span", [
                              _vm._v(
                                _vm._s(
                                  _vm.$t("app.art.columns.price_per_fight")
                                ) + ":"
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col-sm-6" },
                            [
                              _c("number-input", {
                                ref: "pricePerFight",
                                attrs: {
                                  classes: "form-control form-control-sm",
                                  min: 0,
                                  max: 9999999
                                },
                                on: { blur: _vm.calculate },
                                model: {
                                  value: _vm.art.pricePerFight,
                                  callback: function($$v) {
                                    _vm.$set(_vm.art, "pricePerFight", $$v)
                                  },
                                  expression: "art.pricePerFight"
                                }
                              })
                            ],
                            1
                          )
                        ]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "row form-group align-items-center" },
                    [
                      _c("label", { staticClass: "col-sm-6" }, [
                        _vm._v(
                          _vm._s(_vm.$t("app.art.current_strength")) + ": "
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-sm-6" }, [
                        _c("div", { staticClass: "row" }, [
                          _c(
                            "div",
                            { staticClass: "col" },
                            [
                              _c("number-input", {
                                ref: "currentStrength",
                                attrs: {
                                  classes: "form-control form-control-sm",
                                  min: 0,
                                  max: 9999999
                                },
                                on: {
                                  blur: function($event) {
                                    _vm.checkCurrentStrength()
                                    _vm.calculate()
                                  }
                                },
                                model: {
                                  value: _vm.art.currentStrength,
                                  callback: function($$v) {
                                    _vm.$set(_vm.art, "currentStrength", $$v)
                                  },
                                  expression: "art.currentStrength"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "col align-self-center flex-grow-0 p-0 m-0"
                            },
                            [_c("span", {}, [_vm._v("/")])]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col" },
                            [
                              _c("number-input", {
                                ref: "baseStrength",
                                attrs: {
                                  classes: "form-control form-control-sm",
                                  min: 0,
                                  max: 9999999
                                },
                                on: {
                                  blur: function($event) {
                                    _vm.checkBaseStrength()
                                    _vm.calculate()
                                  }
                                },
                                model: {
                                  value: _vm.art.baseStrength,
                                  callback: function($$v) {
                                    _vm.$set(_vm.art, "baseStrength", $$v)
                                  },
                                  expression: "art.baseStrength"
                                }
                              })
                            ],
                            1
                          )
                        ])
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "row form-group align-items-center" },
                    [
                      _c("label", { staticClass: "col-sm-6" }, [
                        _vm._v(_vm._s(_vm.$t("app.art.repair_cost")) + ": ")
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-6" },
                        [
                          _c("number-input", {
                            ref: "repair",
                            attrs: {
                              classes: "form-control form-control-sm",
                              min: 0,
                              max: 9999999
                            },
                            on: { blur: _vm.calculate },
                            model: {
                              value: _vm.art.repair,
                              callback: function($$v) {
                                _vm.$set(_vm.art, "repair", $$v)
                              },
                              expression: "art.repair"
                            }
                          })
                        ],
                        1
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _vm.defaultMode
                    ? _c(
                        "div",
                        { staticClass: "row form-group align-items-center" },
                        [
                          _c("label", { staticClass: "col-sm-6" }, [
                            _c(
                              "span",
                              { staticClass: "posr" },
                              [
                                _c("icon", {
                                  staticClass: "text-muted info",
                                  attrs: {
                                    size: "sm",
                                    name: "info",
                                    tooltip: _vm.$t(
                                      "app.calculator.info_to_price_per_fight_mode_1"
                                    )
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  {
                                    class: {
                                      "text-muted": !_vm.art
                                        .resonablePricePerFight
                                    }
                                  },
                                  [
                                    _vm._v(
                                      "\n                                        " +
                                        _vm._s(
                                          _vm.$t(
                                            "app.art.columns.price_per_fight"
                                          )
                                        ) +
                                        ":\n                                    "
                                    )
                                  ]
                                )
                              ],
                              1
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "col-sm-6" },
                            [
                              _c("number-input", {
                                ref: "resonablePricePerFight",
                                attrs: {
                                  classes:
                                    "form-control form-control-sm" +
                                    (!_vm.art.resonablePricePerFight
                                      ? " bg-muted"
                                      : ""),
                                  min: 0,
                                  max: 9999999,
                                  step: 0.01
                                },
                                on: { blur: _vm.calculate },
                                model: {
                                  value: _vm.art.resonablePricePerFight,
                                  callback: function($$v) {
                                    _vm.$set(
                                      _vm.art,
                                      "resonablePricePerFight",
                                      $$v
                                    )
                                  },
                                  expression: "art.resonablePricePerFight"
                                }
                              })
                            ],
                            1
                          )
                        ]
                      )
                    : _vm._e()
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "col-lg-6 col-md-5 col-12 masters mt-md-0 mt-4"
                  },
                  [
                    _c(
                      "transition-group",
                      { attrs: { name: "el-fade-in", appear: "", tag: "div" } },
                      _vm._l(_vm.masters, function(master, index) {
                        return _c(
                          "div",
                          {
                            key: "master-" + index,
                            staticClass: "row form-group master"
                          },
                          [
                            _c(
                              "div",
                              { staticClass: "col-5 col-sm-6 col-md-5" },
                              [
                                index === 0 && _vm.mastersSource === "url"
                                  ? _c(
                                      "span",
                                      [
                                        _c("icon", {
                                          directives: [
                                            {
                                              name: "tooltip",
                                              rawName: "v-tooltip",
                                              value: _vm.$t(
                                                "app.calculator.delete_parametr_for_show_your_masters"
                                              ),
                                              expression:
                                                "$t('app.calculator.delete_parametr_for_show_your_masters')"
                                            }
                                          ],
                                          staticClass: "mr-2",
                                          attrs: { name: "info" }
                                        })
                                      ],
                                      1
                                    )
                                  : index === 0 && _vm.mastersSource === "user"
                                  ? _c(
                                      "span",
                                      [
                                        _c("icon", {
                                          directives: [
                                            {
                                              name: "tooltip",
                                              rawName: "v-tooltip",
                                              value: _vm.$t(
                                                "app.calculator.setup_masters_in_account_settings"
                                              ),
                                              expression:
                                                "$t('app.calculator.setup_masters_in_account_settings')"
                                            }
                                          ],
                                          staticClass: "mr-2",
                                          attrs: { name: "info" }
                                        })
                                      ],
                                      1
                                    )
                                  : _vm.canDeleteThisMater(index)
                                  ? _c(
                                      "a",
                                      {
                                        staticClass: "text-danger mr-2",
                                        attrs: { href: "#" },
                                        on: {
                                          click: function($event) {
                                            $event.preventDefault()
                                            return _vm.deleteMaster(index)
                                          }
                                        }
                                      },
                                      [
                                        _c("icon", { attrs: { name: "minus" } })
                                      ],
                                      1
                                    )
                                  : _c("span", {
                                      staticClass: "text-danger mr-4"
                                    }),
                                _vm._v(
                                  "\n                                    " +
                                    _vm._s(_vm.$t("app.art.master")) +
                                    " " +
                                    _vm._s(index + 1) +
                                    ":\n                                "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "col-7 col-sm-6 col-md-7" },
                              [
                                _c("div", { staticClass: "row" }, [
                                  _c(
                                    "div",
                                    { staticClass: "col" },
                                    [
                                      _c(
                                        "b-form-select",
                                        {
                                          attrs: { size: "sm" },
                                          on: {
                                            change: function($event) {
                                              return _vm.changeMasters()
                                            }
                                          },
                                          model: {
                                            value: master.pRepair,
                                            callback: function($$v) {
                                              _vm.$set(master, "pRepair", $$v)
                                            },
                                            expression: "master.pRepair"
                                          }
                                        },
                                        _vm._l(
                                          _vm.hwm.master.baseRepairs,
                                          function(repairValue) {
                                            return _c(
                                              "b-form-select-option",
                                              {
                                                key:
                                                  index +
                                                  "master-repair-" +
                                                  repairValue,
                                                attrs: { value: repairValue }
                                              },
                                              [
                                                _vm._v(
                                                  "\n                                                    " +
                                                    _vm._s(repairValue) +
                                                    "%\n                                                "
                                                )
                                              ]
                                            )
                                          }
                                        ),
                                        1
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "col align-self-center flex-grow-0 p-0 m-0"
                                    },
                                    [
                                      _c("span", {}, [
                                        _vm._v(
                                          _vm._s(_vm.$t("app.calculator.for"))
                                        )
                                      ])
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("div", { staticClass: "col" }, [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: master.pRate,
                                          expression: "master.pRate"
                                        }
                                      ],
                                      staticClass:
                                        "form-control form-control-sm master-repair",
                                      attrs: { type: "text" },
                                      domProps: { value: master.pRate },
                                      on: {
                                        blur: function($event) {
                                          return _vm.changeMasters()
                                        },
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            master,
                                            "pRate",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "input-group-append" },
                                      [_vm._v("%")]
                                    )
                                  ])
                                ])
                              ]
                            )
                          ]
                        )
                      }),
                      0
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "d-flex form-group" },
                      [
                        _c("span", { staticClass: "mr-4" }),
                        _vm._v(" "),
                        _vm.mastersSource === "custom"
                          ? _c("b-form-checkbox", {
                              directives: [
                                {
                                  name: "tooltip",
                                  rawName: "v-tooltip",
                                  value:
                                    (_vm.checkSaveMastersToStorage
                                      ? _vm.$t("app.main.disable")
                                      : _vm.$t("app.main.enable")) +
                                    " " +
                                    _vm.$t("app.main.autosave"),
                                  expression:
                                    "(checkSaveMastersToStorage ? $t('app.main.disable') : $t('app.main.enable')) + ' ' + $t('app.main.autosave')"
                                }
                              ],
                              staticClass: "mt-1 mr-4",
                              attrs: { name: "check-button", switch: "" },
                              model: {
                                value: _vm.checkSaveMastersToStorage,
                                callback: function($$v) {
                                  _vm.checkSaveMastersToStorage = $$v
                                },
                                expression: "checkSaveMastersToStorage"
                              }
                            })
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.canAddNewMaster
                          ? _c(
                              "div",
                              { staticClass: "flex-fill" },
                              [
                                _c("c-button", {
                                  attrs: {
                                    icon: "plus",
                                    tooltip: _vm.$t(
                                      "app.calculator.add_master"
                                    ),
                                    block: true
                                  },
                                  on: { click: _vm.addNewMaster }
                                })
                              ],
                              1
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _c("b-form-checkbox", {
                          directives: [
                            {
                              name: "tooltip",
                              rawName: "v-tooltip",
                              value:
                                (_vm.includeMasterCommission
                                  ? _vm.$t("app.main.disable")
                                  : _vm.$t("app.main.enable")) +
                                " " +
                                _vm.$t("app.calculator.commission_repair", {
                                  percents: this.masterCommission
                                }),
                              expression:
                                "(includeMasterCommission ? $t('app.main.disable') : $t('app.main.enable')) + ' ' + $t('app.calculator.commission_repair', {percents: this.masterCommission})"
                            }
                          ],
                          staticClass: "mt-1 ml-4",
                          attrs: { name: "check-button", switch: "" },
                          model: {
                            value: _vm.includeMasterCommission,
                            callback: function($$v) {
                              _vm.includeMasterCommission = $$v
                            },
                            expression: "includeMasterCommission"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "d-flex" },
                      [
                        _c("span", { staticClass: "mr-4" }),
                        _vm._v(" "),
                        _c("b-form-checkbox", {
                          directives: [
                            {
                              name: "tooltip",
                              rawName: "v-tooltip",
                              value:
                                (_vm.checkAutoCalculate
                                  ? _vm.$t("app.main.disable")
                                  : _vm.$t("app.main.enable")) +
                                " " +
                                _vm.$t("app.calculator.auto_calculate"),
                              expression:
                                "(checkAutoCalculate ? $t('app.main.disable') : $t('app.main.enable')) + ' ' + $t('app.calculator.auto_calculate')"
                            }
                          ],
                          staticClass: "mt-1 mr-4",
                          attrs: { name: "check-button", switch: "" },
                          model: {
                            value: _vm.checkAutoCalculate,
                            callback: function($$v) {
                              _vm.checkAutoCalculate = $$v
                            },
                            expression: "checkAutoCalculate"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "flex-fill" },
                          [
                            _c("c-button", {
                              staticClass: "btn-block",
                              attrs: {
                                type: "primary",
                                tooltip: _vm.$t("app.calculator.calculate"),
                                icon: "repair"
                              },
                              on: {
                                click: function($event) {
                                  return _vm.calculate(true)
                                }
                              }
                            })
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "transition",
                      { attrs: { name: "el-zoom-in-top", appear: "" } },
                      [
                        _vm.selectedArt
                          ? _c("art-block", {
                              staticClass: "mt-4",
                              attrs: { art: _vm.selectedArt },
                              on: {
                                close: function($event) {
                                  _vm.selectedArt = null
                                }
                              }
                            })
                          : _vm._e()
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "text-center" },
                      [
                        _c("loading", {
                          staticClass: "mt-4",
                          attrs: { show: _vm.loadingArts }
                        })
                      ],
                      1
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "col-lg-6 col-md-7 col-12 mt-4 mt-md-0" },
                  [
                    _c("div", { staticClass: "row form-group mb-0" }, [
                      _c("label", { staticClass: "col-sm-6" }, [
                        _vm._v(
                          "\n                                " +
                            _vm._s(_vm.$t("app.art.craft")) +
                            "\n                            "
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-6" },
                        [
                          _c("b-form-checkbox", {
                            directives: [
                              {
                                name: "tooltip",
                                rawName: "v-tooltip",
                                value:
                                  (_vm.showCraftBlock
                                    ? _vm.$t("app.main.disable")
                                    : _vm.$t("app.main.enable")) +
                                  " " +
                                  _vm.$t("app.art.craft"),
                                expression:
                                  "(showCraftBlock ? $t('app.main.disable') : $t('app.main.enable')) + ' ' + $t('app.art.craft')"
                              }
                            ],
                            staticClass: "mr-4",
                            attrs: { name: "check-button", switch: "" },
                            model: {
                              value: _vm.showCraftBlock,
                              callback: function($$v) {
                                _vm.showCraftBlock = $$v
                              },
                              expression: "showCraftBlock"
                            }
                          })
                        ],
                        1
                      )
                    ])
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "col-12" },
                  [
                    _c(
                      "transition",
                      { attrs: { name: "el-fade-in", appear: "" } },
                      [
                        _c(
                          "keep-alive",
                          [
                            _vm.showCraftBlock
                              ? _c("craft-block", {
                                  ref: "craft",
                                  staticClass: "mt-3",
                                  attrs: {
                                    "craft-string": _vm.craftString,
                                    "craft-price": _vm.craftPrice,
                                    "art-type": _vm.selectedArt
                                      ? _vm.selectedArt.categoryId
                                      : null
                                  }
                                })
                              : _vm._e()
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ]),
              _vm._v(" "),
              _c("transition", { attrs: { name: "el-fade-in", appear: "" } }, [
                _vm.results.length
                  ? _c(
                      "div",
                      [
                        _c("hr"),
                        _vm._v(" "),
                        _c("result-block", { attrs: { results: _vm.results } })
                      ],
                      1
                    )
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c(
                "b-modal",
                {
                  attrs: {
                    id: _vm.selectArtModalId,
                    title: _vm.$t("app.art.choose_an_art"),
                    "hide-footer": ""
                  }
                },
                [
                  _c(
                    "keep-alive",
                    [
                      _c(_vm.SelectArtComponent, {
                        tag: "component",
                        attrs: {
                          art: _vm.selectedArt,
                          "skip-empty-category": true
                        },
                        on: { "select-art": _vm.changeSelectedArt }
                      })
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/assets/js/app/components/global/ArtBlock.vue":
/*!****************************************************************!*\
  !*** ./resources/assets/js/app/components/global/ArtBlock.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ArtBlock_vue_vue_type_template_id_3eccec46___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ArtBlock.vue?vue&type=template&id=3eccec46& */ "./resources/assets/js/app/components/global/ArtBlock.vue?vue&type=template&id=3eccec46&");
/* harmony import */ var _ArtBlock_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ArtBlock.vue?vue&type=script&lang=js& */ "./resources/assets/js/app/components/global/ArtBlock.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ArtBlock_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ArtBlock_vue_vue_type_template_id_3eccec46___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ArtBlock_vue_vue_type_template_id_3eccec46___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/app/components/global/ArtBlock.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/app/components/global/ArtBlock.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/assets/js/app/components/global/ArtBlock.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArtBlock_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ArtBlock.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/global/ArtBlock.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArtBlock_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/app/components/global/ArtBlock.vue?vue&type=template&id=3eccec46&":
/*!***********************************************************************************************!*\
  !*** ./resources/assets/js/app/components/global/ArtBlock.vue?vue&type=template&id=3eccec46& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ArtBlock_vue_vue_type_template_id_3eccec46___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./ArtBlock.vue?vue&type=template&id=3eccec46& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/global/ArtBlock.vue?vue&type=template&id=3eccec46&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ArtBlock_vue_vue_type_template_id_3eccec46___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ArtBlock_vue_vue_type_template_id_3eccec46___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/assets/js/app/components/pages/calculator/Result.vue":
/*!************************************************************************!*\
  !*** ./resources/assets/js/app/components/pages/calculator/Result.vue ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Result_vue_vue_type_template_id_36d213c6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Result.vue?vue&type=template&id=36d213c6& */ "./resources/assets/js/app/components/pages/calculator/Result.vue?vue&type=template&id=36d213c6&");
/* harmony import */ var _Result_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Result.vue?vue&type=script&lang=js& */ "./resources/assets/js/app/components/pages/calculator/Result.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Result_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Result_vue_vue_type_template_id_36d213c6___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Result_vue_vue_type_template_id_36d213c6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/app/components/pages/calculator/Result.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/app/components/pages/calculator/Result.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************!*\
  !*** ./resources/assets/js/app/components/pages/calculator/Result.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Result_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Result.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/pages/calculator/Result.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Result_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/app/components/pages/calculator/Result.vue?vue&type=template&id=36d213c6&":
/*!*******************************************************************************************************!*\
  !*** ./resources/assets/js/app/components/pages/calculator/Result.vue?vue&type=template&id=36d213c6& ***!
  \*******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Result_vue_vue_type_template_id_36d213c6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Result.vue?vue&type=template&id=36d213c6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/pages/calculator/Result.vue?vue&type=template&id=36d213c6&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Result_vue_vue_type_template_id_36d213c6___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Result_vue_vue_type_template_id_36d213c6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/assets/js/app/pages/Calculator.vue":
/*!******************************************************!*\
  !*** ./resources/assets/js/app/pages/Calculator.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Calculator_vue_vue_type_template_id_58f0862d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Calculator.vue?vue&type=template&id=58f0862d& */ "./resources/assets/js/app/pages/Calculator.vue?vue&type=template&id=58f0862d&");
/* harmony import */ var _Calculator_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Calculator.vue?vue&type=script&lang=js& */ "./resources/assets/js/app/pages/Calculator.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Calculator_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Calculator_vue_vue_type_template_id_58f0862d___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Calculator_vue_vue_type_template_id_58f0862d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/app/pages/Calculator.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/app/pages/Calculator.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/assets/js/app/pages/Calculator.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Calculator_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Calculator.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/pages/Calculator.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Calculator_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/app/pages/Calculator.vue?vue&type=template&id=58f0862d&":
/*!*************************************************************************************!*\
  !*** ./resources/assets/js/app/pages/Calculator.vue?vue&type=template&id=58f0862d& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Calculator_vue_vue_type_template_id_58f0862d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Calculator.vue?vue&type=template&id=58f0862d& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/pages/Calculator.vue?vue&type=template&id=58f0862d&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Calculator_vue_vue_type_template_id_58f0862d___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Calculator_vue_vue_type_template_id_58f0862d___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);