<?php

declare(strict_types=1);

namespace App\HWM\Entities\Interfaces;

interface HwmEntityInterface
{
    public function getHwmId(): string;
    public function getTitle(): string;
}
