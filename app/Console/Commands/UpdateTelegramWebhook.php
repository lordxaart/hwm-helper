<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Services\Telegram\TelegramApi;

class UpdateTelegramWebhook extends Command
{
    /** @var string */
    protected $signature = 'telegram:update:webhook';

    /** @var string  */
    protected $description = 'Update Telegram Webhook';

    public function handle(): void
    {
        /** @var TelegramApi $api */
        $api = app(TelegramApi::class);

        $this->info((string) $api->setWebhook());
    }
}
