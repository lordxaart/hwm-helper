(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[22],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/global/SelectArt.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/components/global/SelectArt.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var lodash_first__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash/first */ "./node_modules/lodash/first.js");
/* harmony import */ var lodash_first__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash_first__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_multiselect__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-multiselect */ "./node_modules/vue-multiselect/dist/vue-multiselect.min.js");
/* harmony import */ var vue_multiselect__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_multiselect__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var JS_app_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! JS/app/config */ "./resources/assets/js/app/config/index.js");
/* harmony import */ var JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! JS/app/utils/helpers */ "./resources/assets/js/app/utils/helpers.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// Multiselect





/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Multiselect: (vue_multiselect__WEBPACK_IMPORTED_MODULE_1___default())
  },
  props: {
    art: {
      required: false
    },
    type: {
      type: String,
      default: 'select'
    },
    categoryType: {
      type: String,
      default: 'art'
    },
    onlyWithCategory: {
      type: Boolean,
      default: false
    },
    skipEmptyCategory: {
      type: Boolean,
      default: false
    },
    clearAfterSelect: {
      type: Boolean,
      default: false
    },
    artHwmId: {
      type: String,
      default: null
    },
    marketCategoryId: {
      type: String,
      default: null
    }
  },

  data() {
    return {
      targetArt: null,
      categoryAlias: '',
      category: ''
    };
  },

  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapActions"])('Arts', ['loadArts', 'loadCategories'])), {}, {
    errorLoadArtImg(event) {
      Object(JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_4__["errorLoadArtImg"])(event);
    },

    inputSelectedArt() {
      if (this.targetArt && this.targetArt.hwmId) {
        this.$emit('select-art', this.targetArt);

        if (this.clearAfterSelect) {
          this.targetArt = null;
        }
      } else {
        this.targetArt = null;
      }
    },

    selectArtFromList(art) {
      if (this.targetArt && art.hwmId === this.targetArt.hwmId) {
        return;
      }

      this.targetArt = art;
      this.inputSelectedArt();
    },

    openSelectedListFromAccordion() {
      if (this.targetArt && this.targetArt.categoryId) {
        // open list
        const element = lodash_first__WEBPACK_IMPORTED_MODULE_0___default()(this.$refs[`collapse-category-${this.targetArt.categoryId}`]);

        if (element) {
          element.show = true;
        }
      }
    }

  }),
  computed: _objectSpread({
    handleArts() {
      const groupedArts = [];
      let categories = this[this.category];

      if (this.categoryType === 'market' && this.marketCategoryId && categories[this.marketCategoryId]) {
        categories = {
          [this.marketCategoryId]: categories[this.marketCategoryId]
        };
      }

      Object.keys(categories).forEach(catId => {
        groupedArts.push({
          category: categories[catId],
          categoryId: catId,
          arts: this.arts.filter(art => art[this.categoryAlias] === catId)
        });
      });

      if (!this.skipEmptyCategory) {
        // Push uncategories arts
        groupedArts.push({
          category: this.$t('app.art.no_category'),
          categoryId: null,
          arts: this.arts.filter(art => !art[this.categoryAlias])
        });
      }

      return groupedArts.filter(group => group.arts.length);
    }

  }, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapGetters"])('Arts', {
    arts: 'getArts',
    marketCategories: 'getMarketCategories',
    categories: 'getCategories',
    loadingArts: 'getLoadingArts',
    loadingMarketCategories: 'getLoadingMarketCategories'
  })),
  watch: {
    art() {
      this.targetArt = this.art;
    },

    targetArt() {
      this.openSelectedListFromAccordion();
    }

  },

  created() {
    Object(JS_app_utils_helpers__WEBPACK_IMPORTED_MODULE_4__["addStyleToDocument"])(`/storage/sprite_small.css?${JS_app_config__WEBPACK_IMPORTED_MODULE_3__["default"].app.sprite_key}`);

    if (this.categoryType === 'market') {
      this.category = 'marketCategories';
      this.categoryAlias = 'marketCategoryId';
    } else {
      this.category = 'categories';
      this.categoryAlias = 'categoryId';
    }

    this.loadCategories(this.category);
    this.loadArts().then(() => {
      this.targetArt = this.art;

      if (!this.targetArt && this.artHwmId) {
        this.targetArt = this.arts.filter(art => art.hwmId === this.artHwmId)[0] || null;
      }
    });
  }

});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/global/SelectArt.vue?vue&type=template&id=2cc361e4&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/components/global/SelectArt.vue?vue&type=template&id=2cc361e4& ***!
  \******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "select-art" }, [
    _vm.type === "select"
      ? _c(
          "div",
          [
            _c("multiselect", {
              attrs: {
                options: _vm.handleArts,
                loading: _vm.loadingArts || _vm.loadingMarketCategories,
                "group-values": "arts",
                "group-label": "category",
                "group-select": false,
                label: "title",
                placeholder: _vm.$t("app.art.enter_art_name"),
                "select-label": "",
                "deselect-label": "",
                "selected-label": "",
                "max-height": 500,
                "allow-empty": true
              },
              on: { input: _vm.inputSelectedArt },
              scopedSlots: _vm._u(
                [
                  {
                    key: "singleLabel",
                    fn: function(props) {
                      return [
                        _c("img", {
                          staticClass: "option__image art-img",
                          attrs: {
                            src: props.option.images.small.fullUrl,
                            alt: props.option.hwmId
                          },
                          on: { error: _vm.errorLoadArtImg }
                        }),
                        _vm._v(" "),
                        _c("span", { staticClass: "option__desc" }, [
                          _c("span", { staticClass: "option__title" }, [
                            _vm._v(_vm._s(props.option.title))
                          ])
                        ])
                      ]
                    }
                  },
                  {
                    key: "option",
                    fn: function(props) {
                      return [
                        props.option.hwmId
                          ? _c("div", {
                              class:
                                " mr-2 art-img art-img-bg art_small-default art_small-" +
                                props.option.hwmId,
                              attrs: { title: props.option.title }
                            })
                          : _vm._e(),
                        _vm._v(" "),
                        props.option.hwmId
                          ? _c("span", { staticClass: "option__desc" }, [
                              _c("span", { staticClass: "option__title" }, [
                                _vm._v(_vm._s(props.option.title))
                              ])
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        !props.option.hwmId
                          ? _c("div", { staticClass: "text-center" }, [
                              _vm._v(
                                "\n                    " +
                                  _vm._s(props.option.$groupLabel) +
                                  "\n                "
                              )
                            ])
                          : _vm._e()
                      ]
                    }
                  }
                ],
                null,
                false,
                2678194276
              ),
              model: {
                value: _vm.targetArt,
                callback: function($$v) {
                  _vm.targetArt = $$v
                },
                expression: "targetArt"
              }
            })
          ],
          1
        )
      : _vm._e(),
    _vm._v(" "),
    _vm.type === "list"
      ? _c("div", { staticClass: "arts-list-accordion" }, [
          _c(
            "div",
            { staticClass: "list-group accordion" },
            _vm._l(_vm.handleArts, function(item) {
              return _c(
                "a",
                {
                  directives: [
                    {
                      name: "b-toggle",
                      rawName: "v-b-toggle",
                      value: "category-" + item.categoryId,
                      expression: "'category-' + item.categoryId"
                    }
                  ],
                  key: item.categoryId,
                  staticClass: "list-group-item list-group-item-action",
                  attrs: { href: "#" },
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                    }
                  }
                },
                [
                  _vm._v(
                    "\n                " +
                      _vm._s(item.category) +
                      " (" +
                      _vm._s(item.arts.length) +
                      ")\n                "
                  ),
                  _c(
                    "b-collapse",
                    {
                      ref: "collapse-category-" + item.categoryId,
                      refInFor: true,
                      staticClass: "list-group",
                      attrs: {
                        id: "category-" + item.categoryId,
                        accordion: "accordion-" + _vm._uid
                      }
                    },
                    _vm._l(item.arts, function(art) {
                      return _c(
                        "a",
                        {
                          key: art.hwmId,
                          staticClass: "btn btn-link text-left",
                          class: {
                            disabled:
                              _vm.targetArt && art.hwmId === _vm.targetArt.hwmId
                          },
                          attrs: {
                            href: "#",
                            disabled:
                              _vm.targetArt && art.hwmId === _vm.targetArt.hwmId
                          },
                          on: {
                            click: function($event) {
                              $event.preventDefault()
                              return _vm.selectArtFromList(art, item.categoryId)
                            }
                          }
                        },
                        [
                          _c("div", {
                            class:
                              " mr-2 art-img art-img-bg art_small-default art_small-" +
                              art.hwmId
                          }),
                          _vm._v(
                            "\n                        " +
                              _vm._s(art.title) +
                              "\n                    "
                          )
                        ]
                      )
                    }),
                    0
                  )
                ],
                1
              )
            }),
            0
          )
        ])
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/assets/js/app/components/global/SelectArt.vue":
/*!*****************************************************************!*\
  !*** ./resources/assets/js/app/components/global/SelectArt.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SelectArt_vue_vue_type_template_id_2cc361e4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SelectArt.vue?vue&type=template&id=2cc361e4& */ "./resources/assets/js/app/components/global/SelectArt.vue?vue&type=template&id=2cc361e4&");
/* harmony import */ var _SelectArt_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SelectArt.vue?vue&type=script&lang=js& */ "./resources/assets/js/app/components/global/SelectArt.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _SelectArt_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SelectArt_vue_vue_type_template_id_2cc361e4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SelectArt_vue_vue_type_template_id_2cc361e4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/app/components/global/SelectArt.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/app/components/global/SelectArt.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/assets/js/app/components/global/SelectArt.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SelectArt_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./SelectArt.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/global/SelectArt.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SelectArt_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/app/components/global/SelectArt.vue?vue&type=template&id=2cc361e4&":
/*!************************************************************************************************!*\
  !*** ./resources/assets/js/app/components/global/SelectArt.vue?vue&type=template&id=2cc361e4& ***!
  \************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SelectArt_vue_vue_type_template_id_2cc361e4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./SelectArt.vue?vue&type=template&id=2cc361e4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/global/SelectArt.vue?vue&type=template&id=2cc361e4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SelectArt_vue_vue_type_template_id_2cc361e4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SelectArt_vue_vue_type_template_id_2cc361e4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);