<?php

declare(strict_types=1);

namespace App\Services\Notifications\Channels\Webpush;

interface ReportHandlerInterface
{
    /**
     * Handle a message sent report.
     *
     * @param  \Minishlink\WebPush\MessageSentReport  $report
     * @param  \App\Services\Notifications\Channels\Webpush\PushSubscriptionModel  $subscription
     * @param  \App\Services\Notifications\Channels\Webpush\WebPushMessage  $message
     * @return void
     */
    public function handleReport($report, $subscription, $message);
}
