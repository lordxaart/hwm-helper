import clipboard from 'JS/app/utils/clipboard';

const disableButton = (element) => {
    if (!element.hasAttribute('disabled')) {
        element.setAttribute('disabled', true);

        // eslint-disable-next-line no-param-reassign
        element.style.width = `${element.offsetWidth}px`;
        // eslint-disable-next-line no-param-reassign
        element.innerHTML = '...';
    }
};

// Disable submit buttons
document.querySelectorAll('.lock-submit').forEach((element) => {
    element.addEventListener('click', () => {
        disableButton(element);
    });
});

document.querySelectorAll('.form-lock-submit').forEach((form) => {
    form.addEventListener('submit', () => {
        const element = form.querySelector('[type=submit]');
        if (element) {
            disableButton(element);
        }
    });
});

// Dismiss alert
setTimeout(() => {
    const alerts = document.querySelectorAll('.alert.alert-dismissible .close');

    alerts.forEach((alert) => {
        alert.click();
    });
}, 15000);

// clipboard helper
document.querySelectorAll('.clipboard').forEach((btn) => {
    btn.addEventListener('click', () => {
        const text = btn.getAttribute('data-text');

        if (text) {
            clipboard(text);
        }
    });
});

// toggleable navbar items
document.querySelectorAll('nav.navbar a:not(.dropdown-toggle)').forEach((link) => {
    link.addEventListener('click', () => {
        const toggler = document.querySelector('.navbar-toggler');
        if (toggler && toggler.classList.contains('not-collapsed')) {
            toggler.click();
        }
    });
});
