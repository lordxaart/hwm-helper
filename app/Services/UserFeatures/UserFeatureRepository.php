<?php

declare(strict_types=1);

namespace App\Services\UserFeatures;

use App\Services\UserFeatures\Enums\Feature;
use App\Services\UserFeatures\Enums\FeatureEntity;
use App\Services\UserFeatures\Enums\FeatureType;
use App\Services\UserFeatures\ValueObjects\FeatureValue;
use Illuminate\Database\DatabaseManager;
use Psr\SimpleCache\CacheInterface;
use DateInterval;

class UserFeatureRepository
{
    private const FEATURES_CACHE_KEY = 'user_features_list';
    private const CACHE_DURATION = 'P1M';

    public function __construct(private DatabaseManager $db, private CacheInterface $cache)
    {
    }

    public function getFeatureList(): array
    {
        if ($this->cache->has(self::FEATURES_CACHE_KEY)) {
            return $this->cache->get(self::FEATURES_CACHE_KEY);
        }

        $data = $this->db->table('user_features')->get()->map(function ($item) {
            $array = (array)$item;
            $array['default_value'] = FeatureValue::convertValueToType(
                $array['default_value'],
                FeatureType::from($array['type'])
            );

            return $array;
        })->keyBy('id')->toArray();
        $data[Feature::LOT_FILTERS_NOTIFY_DELAY->value]['default_value'] = settings()->default_lot_notify_delay;

        $this->cache->set(self::FEATURES_CACHE_KEY, $data, new DateInterval(self::CACHE_DURATION));

        return $data;
    }

    public function addFeature(
        string $id,
        FeatureType $type,
        mixed $default_value,
        ?string $name = null,
    ): void {
        $this->db->table('user_features')->insert([
            [
                'id' => $id,
                'type' => $type->value,
                'default_value' => $default_value,
                'name' => $name,
            ]
        ]);

        $this->clearCache();
    }

    /**
     * Insert or update value
     */
    public function setFeatureValue(
        FeatureEntity $entity,
        string $entityId,
        Feature $feature,
        mixed $value,
    ): void {
        $query = $this->db->table('user_features_values')
            ->where('entity', $entity->value)
            ->where('entity_id', $entityId)
            ->where('feature_id', $feature->value);

        if ($query->exists()) {
            $query->update([
                'value' => $value,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        } else {
            $this->db->table('user_features_values')->insert([
                [
                    'entity' => $entity->value,
                    'entity_id' => $entityId,
                    'feature_id' => $feature->value,
                    'value' => $value,
                    'created_at' => date('Y-m-d H:i:s'),
                ]
            ]);
        }
    }

    public function getFeatureValuesList(FeatureEntity $entity, string $entityId): array
    {
        return $this->db
            ->table('user_features_values')
            ->where('entity', $entity->value)
            ->where('entity_id', $entityId)
            ->get()
            ->keyBy('feature_id')
            ->map(fn($item) => (array)$item)
            ->toArray();
    }

    public function getFeatureValueByEntity(FeatureEntity $entity, string $entityId, Feature $feature): ?FeatureValue
    {
        $value = $this->db
            ->table('user_features_values')
            ->where('entity', $entity->value)
            ->where('entity_id', $entityId)
            ->where('feature_id', $feature->value)
            ->first();

        if (!$value) {
            return null;
        }

        $value = (array) $value;
        $default = $this->getFeatureList()[$feature->value];

        return new FeatureValue(
            key: $default['id'],
            type: $default['type'],
            default: $default['default_value'],
            value: $value['value'],
            entity: $value['entity'],
        );
    }

    public function clearCache(): void
    {
        $this->cache->delete(self::FEATURES_CACHE_KEY);
    }
}
