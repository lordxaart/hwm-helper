<?php

declare(strict_types=1);

namespace App\HWM\NewParsers\Pages;

use App\HWM\HtmlParsers\Interfaces\HtmlNodeInterface;
use App\HWM\NewParsers\ParserException;
use App\HWM\NewParsers\ParserValidator;

abstract class AbstractPageParser
{
    public const EN_CDN_LINK = 'dcdn.lordswm.com';

    private string $locale = RU;

    protected HtmlNodeInterface $htmlNode;

    protected ParserValidator $validator;

    protected array $texts = [
        RU => [],
        EN => [],
    ];

    public function __construct(HtmlNodeInterface $htmlNode)
    {
        ini_set('memory_limit', '256M');

        $this->htmlNode = $htmlNode;

        $this->validator = new ParserValidator();

        $this->detectLocale();
    }

    abstract public function handle(): mixed;

    protected function detectLocale(): void
    {
        if (str_contains($this->htmlNode->asHtml(), self::EN_CDN_LINK)) {
            $this->setLocale(EN);
        }
    }

    protected function setLocale(string $locale): void
    {
        $this->locale = $locale;
    }

    protected function getLocale(): string
    {
        return $this->locale;
    }

    protected function getTransText(string $key, string $locale = null): string
    {
        $locale = $locale ?: $this->locale;

        if (!isset($this->texts[$locale]) || !isset($this->texts[$locale][$key])) {
            throw new ParserException("Not found localization text for locale[$locale] and key[$key]");
        }

        return $this->texts[$locale][$key];
    }
}
