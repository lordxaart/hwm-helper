<?php

declare(strict_types=1);

namespace App\Core\Enums;

enum Cookie: string
{
    case USERID = 'usrid';
    case LOCALE = 'locale';
}
