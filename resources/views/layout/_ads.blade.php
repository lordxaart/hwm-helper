@php
$show = !Crawler::isCrawler() && settings()->enable_adsense_script;
$user = user();
if ($show && $user) {
    $show = $user->features()->is_show_ads;
}
@endphp
@if($show)
<!-- AdSense script -->
<script data-ad-client="ca-pub-6041359441924446" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- End AdSense -->
@endif
