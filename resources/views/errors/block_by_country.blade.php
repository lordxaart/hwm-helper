@extends('layout.empty', ['extra_wrapper_class' => 'error-page'])

@section('content')
    <div class="container error-container">
        <div>
            <h1 class="logo-text-bold text-danger text-center" style="font-size: 4rem; margin-bottom: 2rem">Сайт недоступен в вашем регионе</h1>
            <p class="font-2xl" style="font-size: 1.25rem;">
                Тема войны в Украине была и остаётся актуальной. Если вы готовы заглянуть такой правде в глаза, тогда посмотрите этот документальный фильм и вы поймёте что представляет из себя так называемый русский мир.
            </p>
            <div style="margin: 15px auto;">
                <iframe style="width: 100%; height: 100%; aspect-ratio: 1.7" src="https://www.youtube.com/embed/9H_Fg_5x4ME?si=DTAg_v1qR_rL6KC6" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
            </div>
            <hr>
        </div>
    </div>
@endsection
