<?php

declare(strict_types=1);

namespace App\ValueObjects;

use Spatie\DataTransferObject\DataTransferObject;

abstract class DTO extends DataTransferObject
{
    //
}
