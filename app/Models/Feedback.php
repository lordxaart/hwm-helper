<?php

declare(strict_types=1);

namespace App\Models;

use App\Jobs\AdminNotify;
use App\Notifications\Admin\NewFeedback;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

/**
 * @property array $attachments
 * @property array $data
 */
class Feedback extends Model
{
    public const STATUS_NEW = 0;
    public const STATUS_SENT = 1;
    public const STATUS_READ = 2;

    protected $table = 'feedbacks';

    protected $casts = [
        'data' => 'array',
        'attachments' => 'array',
    ];

    public static function customCreate(array $data): Feedback
    {
        $attachments = [];
        if (isset($data['attachments'])) {
            $attachments = $data['attachments'];
            $data['attachments'] = null;
        }

        $feedback = new self();
        $feedback->data = array_filter($data);

        $feedback->attachments = array_filter(array_map(function ($file) {
            if ($file instanceof UploadedFile && $file->isReadable()) {
                return base64_encode(file_get_contents($file->path()) ?: '');
            }
            return null;
        }, $attachments));

        $feedback->save();

        return $feedback;
    }

    public function sendToAdmin(): void
    {
        AdminNotify::dispatchSync(new NewFeedback($this));

        $this->status = self::STATUS_SENT;
        $this->save();
    }
}
