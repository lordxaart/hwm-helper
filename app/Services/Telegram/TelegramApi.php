<?php

declare(strict_types=1);

namespace App\Services\Telegram;

use App\Models\TelegramAccount;
use App\Services\Notifications\Channels\Telegram\Entities\TelegramMessage;
use App\Services\Telegram\Entities\Update;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class TelegramApi
{
    public const BASE_URI = 'https://api.telegram.org/';
    public const ACTION_SEND_MESSAGE = 'sendMessage';
    public const ACTION_GET_CHAT_MEMBER = 'getChatMember';
    public const ACTION_SET_WEBHOOK = 'setWebhook';
    public const LAST_UPDATE_ID_CACHE_KEY = 'telegram_last_update_id';

    private Client $client;

    private CommandHandler $commandHandler;

    public function __construct(string $botToken)
    {
        $this->client = new Client([
            'base_uri' => self::BASE_URI . 'bot' . $botToken . '/'
        ]);

        $this->commandHandler = new CommandHandler();
    }

    public static function getBotLink(): string
    {
        return 'https://t.me/' . config('services.telegram.bot');
    }

    public static function getWebhookToken(): string
    {
        return md5(config('app.key'));
    }

    public function setWebhook(): string|bool
    {
        $url = route('webhook.telegram', ['token' => self::getWebhookToken()]);
        $response = $this->request('POST', self::ACTION_SET_WEBHOOK, [
            'url' => $url,
        ]);

        if (property_exists($response, 'ok') && !$response->ok) {
            \Log::error('Set telegram webhook', $this->objectToArray($response) + ['url' => $url]);
            return false;
        }

        return $url;
    }

    public function handleUpdate(array $data): void
    {
        $update = new Update($data);

        $this->setLastUpdate($update->update_id);

        // skip if update already exists, because telegram can send many the same exists
        if (!TelegramAccount::logUpdate($update)) {
            return;
        }

        if ($update->message?->isCommand()) {
            $command = $update->message->getCommand();

            $this->commandHandler->handle($command ?: '', $update);
        }
    }

    public function sendConnectLinkToChat(int $chatId, array $params): bool
    {
        $params = array_merge($params, ['chatId' => $chatId]);

        $url = url('api/v1/profile/connect/telegram') . '?' . http_build_query($params);
        $connect = trans('app.main.connect');
        $message = new TelegramMessage(
            trans('app.user.complete_connect_telegram', [
                'link' => $url,
                'text' => $connect,
            ])
        );
        $message->button($connect, $url);

        return $this->sendMessage($chatId, $message);
    }

    public function sendMessage(int $chatId, TelegramMessage $message): bool
    {
        $body = $message->toArray();
        $body['chat_id'] = $chatId;

        $response = $this->request('POST', self::ACTION_SEND_MESSAGE, $body);

        if (property_exists($response, 'ok') && $response->ok) {
            return true;
        }

        \Log::error('Error send telegram message', [
            'response' => $this->objectToArray($response),
            'body' => $body,
        ]);

        return false;
    }

    public function isChatMember(int $chatId, int $userId): bool
    {
        $response = $this->request('POST', self::ACTION_GET_CHAT_MEMBER, [
            'chat_id' => $chatId,
            'user_id' => $userId,
        ]);
        $memberStatuses = ['creator', 'administrator', 'member', 'restricted'];
        if (property_exists($response, 'ok') && $response->ok && in_array($response->result->status, $memberStatuses)) {
            return true;
        }

        return false;
    }

    private function request(string $method, string $action, array $params = []): object
    {
        $options = [
            $method === 'GET' ? 'query' : 'form_params' => $params,
        ];

        try {
            $response = $this->client->request($method, $action, $options);
        } catch (ClientException $e) {
            $response = $e->getResponse();
        }


        return (object)json_decode($response->getBody()->getContents());
    }

    private function setLastUpdate(int $updateId): void
    {
        \Cache::set(self::LAST_UPDATE_ID_CACHE_KEY, $updateId);
    }

    private function objectToArray(object $object): array
    {
        return (array)$object;
    }
}
