<?php

declare(strict_types=1);

namespace App\Services\SearchLots\ValueObjects\Aggregation;

class Aggregation
{
    public readonly string $name;
    public readonly AggregationType $type;
    /** @var Aggregation[] */
    private array $children;

    public function __construct(string $name, AggregationType $type, array $children = [])
    {
        $this->name = $name;
        $this->type = $type;
        $this->setChildren($children);
    }

    public function getChildren(): array
    {
        return $this->children;
    }
    
    private function setChildren(array $children): void
    {
        foreach ($children as $item) {
            if (!($item instanceof Aggregation)) {
                throw new \InvalidArgumentException(
                    sprintf('Each child must be instance of %s, given %s', Aggregation::class, get_class($item))
                );
            }
        }

        $this->children = $children;
    }
}
