<?php

declare(strict_types=1);

namespace App\Events\HWM;

use App\Models\Art;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;

class ArtAddedEvent
{
    use Dispatchable;
    use InteractsWithSockets;

    public Art $art;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Art $art)
    {
        $this->art = $art;
    }
}
