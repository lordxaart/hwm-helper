<?php

declare(strict_types=1);

namespace App\Services\Notifications\Enums;

enum NotifyType: string
{
    case DEFAULT = 'default';
    case IMPORTANT = 'important';
    case ACCORDED_FILTER = 'accorded_filter';
    case CHANGE_SETTINGS = 'change_settings';
    case NEW_ART = 'new_art';
}
