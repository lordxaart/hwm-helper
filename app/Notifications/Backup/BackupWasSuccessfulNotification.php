<?php

declare(strict_types=1);

namespace App\Notifications\Backup;

use App\Services\Notifications\Channels\Telegram\Entities\TelegramMessage;
use Spatie\Backup\Notifications\Notifications\BackupWasSuccessfulNotification as BaseNotification;

class BackupWasSuccessfulNotification extends BaseNotification
{
    public function toTelegram(): TelegramMessage
    {
        $message  = TelegramMessage::create()->success();
        $message->escapedLine(trans('backup::notifications.backup_successful_subject_title'));
        $message->escapedLine('---');

        foreach ($this->backupDestinationProperties()->toArray() as $key => $line) {
            $message->escapedLine("*$key*: $line");
        }

        return $message;
    }

    public function toArray(): array
    {
        return [
            'disk' => $this->diskName(),
            'destination' => $this->backupDestinationProperties(),
        ];
    }
}
