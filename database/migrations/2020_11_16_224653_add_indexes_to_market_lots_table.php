<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexesToMarketLotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('market_lots', function (Blueprint $table) {
            $schema = Schema::getConnection()->getDoctrineSchemaManager();
            $indexesFound = $schema->listTableIndexes('market_lots');

            if (array_key_exists('market_lots_art_type_lot_type_index', $indexesFound)) {
                $table->dropIndex('market_lots_art_type_lot_type_index');
            }

            // new indexes
            $table->index('lot_type');
            $table->index('art_type');
            $table->index('updated_at');
            $table->index('ended_at');
            $table->index('is_parsed');
            $table->index('start_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('market_lots', function (Blueprint $table) {
            $table->dropIndex('lot_type');
            $table->dropIndex('art_type');
            $table->dropIndex('updated_at');
            $table->dropIndex('ended_at');
            $table->dropIndex('is_parsed');
            $table->dropIndex('start_price');
        });
    }
}
