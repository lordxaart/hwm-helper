<?php

declare(strict_types=1);

namespace App\Listeners\ArtMonitor;

use App\Events\ArtMonitor\DeleteMonitoredArtEvent as Event;
use App\Services\ActivityLogger\ActivityLogger;
use App\Services\ActivityLogger\ValueObjects\Activity;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeletedArtFromMonitorListener implements ShouldQueue
{
    public function __construct(private readonly ActivityLogger $activityLogger)
    {
    }

    public function handle(Event $event): void
    {
        $this->activityLogger->log(
            new Activity('HWM_MONITORED_ART_DELETE', [
                'art' => [
                    'hwm_id' => $event->art->getHwmId(),
                    'title' => $event->art->getTitle(),
                ],
                'user' => $event->user->toArray(),
            ])
        );
    }
}
