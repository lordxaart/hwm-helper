<?php

declare(strict_types=1);

namespace App\HWM\Objects;

use InvalidArgumentException;

class Master
{
    protected int $rate;
    protected int $repair;
    protected int $commission;

    public static array $repairs = [ 10, 20, 30, 40, 50, 60, 70, 80, 90 ];

    public function __construct(int $rate, int $repair, int $commission = 0)
    {
        if ($rate < 0 || !in_array($repair, self::$repairs)) {
            throw new InvalidArgumentException("Rate $rate or repair $repair is incorrect");
        }

        $this->rate = $rate + $commission;
        $this->repair = $repair;
    }

    public static function getDefaultMaster(): self
    {
        return new self(
            (int)config('hwm.default_master.rate'),
            (int)config('hwm.default_master.repair'),
            (int)config('hwm.default_master.commission', 0),
        );
    }

    public function getRate(): int
    {
        return $this->rate;
    }

    public function getRepair(): int
    {
        return $this->repair;
    }
}
