<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Http\Resources\Core\ApiResource;
use App\HWM\Entities\BaseLot;
use App\HWM\Entities\MarketLot;
use App\Models\LotFilter;
use App\Models\User;
use App\Services\Settings\User\GameLinkDecorator;
use Illuminate\Http\Request;

/**
 * @property BaseLot $resource
 */
class LotBaseResource extends ApiResource
{
    protected array $accordedFilters = [];
    protected ?array $entity = null;

    public function setFilters(array $filters): void
    {
        $this->accordedFilters = [];

        foreach ($filters as $filter) {
            if (!$filter instanceof LotFilter) {
                throw new \InvalidArgumentException('Each item of filters must be LotFilter instance, get ' . $filter::class);
            }
            $this->accordedFilters[] = $filter;
        }
    }

    public function setEntity(array $entity): void
    {
        $this->entity = $entity;
    }

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        /** @var User $user */
        $user = $request->user();
        /** @var GameLinkDecorator $decorator */
        $decorator = app(GameLinkDecorator::class);
        $lot = $this->resource;
        $marketLink = $this->entity ? $lot->getMarketLink($this->entity['market_category_id']) : '';
        $entityMarketLink = $this->entity ? $this->entity['hwm_market_link'] : null;

        if ($user) {
            $marketLink = $decorator->replaceGameLinkForUser($marketLink, $user);
            $entityMarketLink = $entityMarketLink ? $decorator->replaceGameLinkForUser($entityMarketLink, $user) : null;
        }

        return $this->transformByRequest([
            'lotId' => $lot->lot_id,
            'lotLink' => $lot->getLink(),
            'crc' => $lot instanceof MarketLot ? $lot->crc : null,
            'marketLink' => $marketLink,
            'lotType' => $lot->lot_type,
            'price' => $lot->price,
            'blitzPrice' => $lot->blitz_price,
            'quantity' => $lot->quantity,
            'currentStrength' => $lot->current_strength,
            'baseStrength' => $lot->base_strength,
            'pricePerFight' => $lot->price_per_fight,
            'endedAt' => $lot->ended_at->format(config('app.date_format')),
            'endedAtHuman' => $lot->ended_at->diffForHumans(),
            'craft' => $lot->craft,
            'craftJson' => $lot->getParsedCraft(),
            'sellerName' => $lot->seller_name,
            'sellerId' => $lot->seller_id,
            'sellerLink' => $lot->getSellerProfileLink(),
            'artUid' => $lot->art_uid ?: null,
            'artCrc' => $lot->art_crc,
            'entity' => $this->entity ? [
                'hwmId' => $this->entity['hwm_id'],
                'type' => $this->entity['type'],
                'title' => $this->entity['title'],
                'image' => $this->entity['image'],
                'marketCategoryId' => $this->entity['market_category_id'],
                'hwmLink' => $this->entity['hwm_link'],
                'hwmMarketLink' => $entityMarketLink,
            ] : null,
            'accorded_filters' => !empty($this->accordedFilters) ? LotFilterResource::collection($this->accordedFilters) : [],
        ], $request);
    }
}
