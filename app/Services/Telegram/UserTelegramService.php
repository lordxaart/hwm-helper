<?php

declare(strict_types=1);

namespace App\Services\Telegram;

use App\Events\User\ConnectedTelegramEvent;
use App\Events\User\DisconnectedTelegramEvent;
use App\Exceptions\ClientError;
use App\Models\TelegramAccount;
use App\Models\User;
use App\Notifications\User\InviteToPublicChannel;
use App\Services\Telegram\Entities\User as TelegramUser;
use Illuminate\Contracts\Events\Dispatcher;

class UserTelegramService
{
    public function __construct(private Dispatcher $eventDispatcher)
    {
        //
    }

    public function connectTelegramToUser(User $user, int $chatId, TelegramUser $telegramUser, bool $fireEvents = true): bool
    {
        // TODO: replace with new TelegramAccount and delete info from settings (before migrate data)
        /** @var TelegramAccount | null $telegramAccount */
        $telegramAccount = TelegramAccount::query()
            ->withoutGlobalScope('active')
            ->where('user_id', $user->getId()->value)
            ->first();

        if ($telegramAccount && $telegramAccount->is_active) {
            return false;
        }

        if ($telegramAccount && !$telegramAccount->is_active) {
            $telegramAccount->update([
                'chat_id' => $chatId,
                'user_data' => $telegramUser->toArray(),
                'is_active' => true,
                'is_blocked' => false,
            ]);

            if ($fireEvents) {
                $this->eventDispatcher->dispatch(new ConnectedTelegramEvent($user, $telegramUser));
            }

            return true;
        }

        $user->telegramAccount()->create([
            'chat_id' => $chatId,
            'user_data' => $telegramUser->toArray(),
         ]);

        $socialProfile = $user->profiles()->where('provider', 'telegram')->first();

        if ($socialProfile) {
            $socialProfile->update(['provider_id' => $chatId]);
        } else {
            $user->profiles()->create([
                'provider' => 'telegram',
                'provider_id' => $chatId,
            ]);
        }

        if ($fireEvents) {
            $this->eventDispatcher->dispatch(new ConnectedTelegramEvent($user, $telegramUser));
        }

        return true;
    }

    public function disconnectTelegram(User $user, bool $isBlocked = false): bool
    {
        if (!$user->telegramAccount) {
            throw new ClientError(400, trans('app.user.telegram_is_not_connected'));
        }

        $user->telegramAccount->update(['is_active' => false, 'is_blocked' => $isBlocked]);

        $user->profiles()
            ->where('provider', 'telegram')
            ->where('provider_id', $user->telegramAccount->chat_id)
            ->first()
            ?->delete();

        $this->eventDispatcher->dispatch(new DisconnectedTelegramEvent($user, $user->telegramAccount));

        return true;
    }

    public function getUserByChatId(int $chatId): ?User
    {
        /** @var TelegramAccount | null $telegramAccount */
        $telegramAccount = TelegramAccount::query()->where('chat_id', $chatId)->first('user_id');

        /** @var User | null $user */
        $user = $telegramAccount ? User::query()->where('id', $telegramAccount->user_id)->first() : null;

        return $user;
    }

    public function sendInviteToPublicChannel(User $user): void
    {
        if (!settings()->telegram_public_channel_invite_link) {
            throw new \DomainException('telegram_public_channel_invite_link is empty');
        }

        $user->notify(new InviteToPublicChannel(settings()->telegram_public_channel_invite_link));
    }

    public function userIsConnectedToPublicChannel(User $user): bool
    {
        if (!$user->telegramAccount) {
            return false;
        }

        /** @var TelegramApi $api */
        $api = app(TelegramApi::class);
        return $api->isChatMember((int)config('services.telegram-bot-api.notify_channel_id'), $user->telegramAccount->chat_id);
    }
}
