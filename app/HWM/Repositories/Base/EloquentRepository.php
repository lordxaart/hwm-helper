<?php

declare(strict_types=1);

namespace App\HWM\Repositories\Base;

use App\HWM\Entities\Interfaces\HwmEntityInterface;
use App\HWM\Repositories\Interfaces\EloquentRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class EloquentRepository
 * @method Model|null findByHwmId(string $hwmId)
 * @package App\HWM\Repositories\Base
 */
abstract class EloquentRepository extends EntityRepository implements EloquentRepositoryInterface
{
    /**
     * Model must have fields 'hwm_id' and 'title'
     */
    protected Model $model;

    public function __construct(Model $model)
    {
        if (! $model instanceof Model) {
            throw new \InvalidArgumentException();
        }

        $this->model = $model;

        parent::__construct();
    }

    protected function initCache(): void
    {
        parent::initCache();
    }

    protected function loadDataFromRepository(): Collection
    {
        return $this->mapData($this->model::all());
    }

    protected function mapData(Collection $data): Collection
    {
        return $data->map(function (Model $model) {
            if (method_exists($model, 'setLocale')) {
                $model->setLocale($this->locale);
            }

            return $model;
        });
    }

    protected function validateData(): void
    {
        //
    }

    protected function getElementInstance(): HwmEntityInterface
    {
        if (!$this->model instanceof HwmEntityInterface) {
            throw new \InvalidArgumentException();
        }

        return $this->model;
    }
}
