<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UpdateArtRequest;
use App\HWM\Repositories\ArtCategoryRepository;
use App\HWM\Repositories\ArtSetRepository;
use App\HWM\Repositories\MarketCategoryRepository;
use App\Models\Art;
use Backpack\CRUD\app\Http\Controllers\CrudController;

/**
 * Class ArtCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ArtCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;

    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel('App\Models\Art');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/arts');
        $this->crud->setEntityNameStrings('art', 'arts');

        $this->crud->with(['marketCategory', 'category', 'set']);
        $this->crud->addClause('withCount', 'monitoredArts');

        $this->crud->set('show.setFromDb', false);
    }

    protected function setupListOperation()
    {
        $this->crud->setColumns($this->listColumns([], ['description']));
        $this->crud->removeButton('create');
        $this->crud->addButtonFromView('top', 'add_art', 'add_art');
        $this->crud->addButtonFromView('line', 'refresh_art', 'refresh_art');

        // $this->registerListFilters();
    }

    protected function setupShowOperation()
    {
        $this->crud->setColumns($this->showColumns());
        $this->crud->addButtonFromView('line', 'refresh_art', 'refresh_art');
    }

    protected function setupUpdateOperation()
    {
        $this->crud->setValidation(UpdateArtRequest::class);
        $this->crud->addFields([
            ['name' => 'title', 'label' => 'Title', 'type' => 'text_transable',],
            ['name' => 'need_level', 'label' => 'Level', 'type' => 'number',],
            ['name' => 'oa', 'label' => 'OA', 'type' => 'number',],
            ['name' => 'repair', 'label' => 'Repair', 'type' => 'number',],
            ['name' => 'strength', 'label' => 'Strength', 'type' => 'number',],
            ['name' => 'shop_price', 'label' => 'Price', 'type' => 'number',],
            ['name' => 'from_shop', 'label' => 'From shop', 'type' => 'checkbox',],
            ['name' => 'as_from_shop', 'label' => 'As from shop', 'type' => 'checkbox',],
            ['name' => 'category_id', 'label' => 'M. Category', 'type' => 'select', 'entity' => 'category', 'attribute' => 'title'],
            ['name' => 'market_category_id', 'label' => 'M. Category', 'type' => 'select', 'entity' => 'marketCategory', 'attribute' => 'title'],
            ['name' => 'set_id', 'label' => 'Set', 'type' => 'select', 'entity' => 'set', 'attribute' => 'title'],
            ['name' => 'description', 'label' => 'Description', 'type' => 'text_transable'],
        ]);
    }

    protected function update($id)
    {
        $this->crud->hasAccessOrFail('update');

        $request = $this->crud->validateRequest();

        /** @var Art $art */
        $art = Art::findOrFail($id);
        $fillable = [
            'title',
            'need_level',
            'strength',
            'oa',
            'repair',
            'description',
            'from_shop',
            'as_from_shop',
            'shop_price',
            'market_category_id',
            'set_id',
            'category_id',
        ];

        foreach ($fillable as $key) {
            if ($request->has($key)) {
                $art->$key = $request->input($key);
            }
        }

        $art->save();

        // show a success message
        \Alert::success(trans('backpack::crud.update_success'))->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($art->getKey());
    }

    protected function listColumns(array $includeColumns = [], array $excludeColumns = []): array
    {
        $columns = collect([
            ['name' => 'hwm_id', 'label' => 'ID', 'type' => 'text', 'visibleInTable' => true, 'visibleInExport' => true,],
            ['name' => 'title', 'label' => 'Title', 'type' => 'view', 'view' => 'crud::columns.art', 'link' => 'info', 'searchLogic' => function ($query, $column, $searchTerm) {
                $query->orWhere('title', 'like', '%' . $searchTerm . '%')->orWhere('hwm_id', 'like', '%' . $searchTerm . '%');
            }],
            ['name' => 'description', 'label' => 'Description', 'type' => 'text'],
            ['name' => 'image', 'label' => 'Image', 'type' => 'closure', 'orderable' => false, 'visibleInTable' => false, 'function' => function (Art $art) {
                return "<img src='{$art->images['small']->getUrl()}' style='max-height: 30px;width: auto;' alt='$art->hwm_id' title='$art->hwm_id' data-toggle='tooltip'>";
            }],
            ['name' => 'need_level', 'label' => 'Level', 'type' => 'number', 'decimals' => 0, 'visibleInTable' => false,],
            ['name' => 'oa', 'label' => 'OA', 'type' => 'number', 'decimals' => 0, 'visibleInTable' => false,],
            ['name' => 'repair', 'label' => 'Repair', 'type' => 'number', 'decimals' => 0, 'visibleInTable' => false, 'visibleInExport' => true,],
            ['name' => 'strength', 'label' => 'Strength', 'type' => 'number', 'decimals' => 0, 'visibleInTable' => false, 'visibleInExport' => true,],
            ['name' => 'shop_price', 'label' => 'Price', 'type' => 'number', 'decimals' => 0, 'thousands_sep' => ',', 'visibleInTable' => false, 'visibleInExport' => true,],
            ['name' => 'from_shop', 'label' => 'From shop', 'type' => 'check', 'orderable' => false, 'visibleInTable' => false,],
            ['name' => 'as_from_shop', 'label' => 'As from shop', 'type' => 'check', 'orderable' => false, 'visibleInTable' => false,],
            ['name' => 'price_per_fight', 'label' => 'PPF', 'type' => 'number', 'decimals' => 2, 'visibleInTable' => false, 'visibleInExport' => true,],
            ['name' => 'category_id', 'label' => 'Category', 'type' => 'view', 'view' => 'crud::columns.change_category', 'entity' => 'category'],
            ['name' => 'market_category_id', 'label' => 'M. Category', 'type' => 'view', 'view' => 'crud::columns.change_category', 'entity' => 'marketCategory'],
            ['name' => 'set_id', 'label' => 'Set', 'type' => 'view', 'view' => 'crud::columns.change_category', 'entity' => 'set'],
            ['name' => 'users_monitored_count', 'label' => 'Monitored', 'type' => 'number', 'orderable' => true, 'orderLogic' => function ($query, $column, $columnDirection) {
                return $query->orderBy('users_monitored_count', $columnDirection);
            }],
            ['name' => 'created_at', 'label' => 'Created', 'type' => 'datetime', 'visibleInTable' => false, 'visibleInExport' => true,],
            ['name' => 'updated_at', 'label' => 'Updated', 'type' => 'datetime'],
        ]);

        foreach ($includeColumns as $key => $col) {
            if (is_array($col)) {
                $ind = $columns->search(function ($item) use ($key) {
                    return $item['name'] === $key;
                });
                if ($ind !== false) {
                    $columns = $columns->replace([$ind => $col]);
                }
            }
        }

        $includeColumns = array_map(function ($item) {
            return is_array($item) ? $item['name'] : $item;
        }, $includeColumns);
        $includeColumns = array_values($includeColumns);

        // Filter by attribute
        if (count($includeColumns)) {
            $columns = $columns->whereIn('name', $includeColumns);
        }


        if (count($excludeColumns)) {
            $columns = $columns->whereNotIn('name', $excludeColumns);
        }

        return $columns->toArray();
    }

    protected function showColumns(): array
    {
        return [
            ['name' => 'hwm_id', 'label' => 'ID', 'type' => 'text',],
            ['name' => 'title', 'label' => 'Title', 'type' => 'view', 'view' => 'crud::columns.art', 'link' => 'info',],
            ['name' => 'description', 'label' => 'Description', 'type' => 'text'],
            ['name' => 'image', 'label' => 'Image', 'type' => 'view', 'view' => 'crud::columns.art_image'],
            ['name' => 'need_level', 'label' => 'Level', 'type' => 'number',],
            ['name' => 'oa', 'label' => 'OA', 'type' => 'number',],
            ['name' => 'repair', 'label' => 'Repair', 'type' => 'number',],
            ['name' => 'strength', 'label' => 'Strength', 'type' => 'number',],
            ['name' => 'shop_price', 'label' => 'Price', 'type' => 'number', 'thousands_sep' => ',',],
            ['name' => 'from_shop', 'label' => 'From shop', 'type' => 'check',],
            ['name' => 'as_from_shop', 'label' => 'As from shop', 'type' => 'check',],
            ['name' => 'price_per_fight', 'label' => 'PPF', 'type' => 'number', 'decimals' => 2,],
            ['name' => 'category_id', 'label' => 'Category', 'type' => 'view', 'view' => 'crud::columns.change_category', 'entity' => 'category'],
            ['name' => 'market_category_id', 'label' => 'M. Category', 'type' => 'view', 'view' => 'crud::columns.change_category', 'entity' => 'marketCategory'],
            ['name' => 'set_id', 'label' => 'Set', 'type' => 'view', 'view' => 'crud::columns.change_category', 'entity' => 'set'],
            ['name' => 'created_at', 'label' => 'Created', 'type' => 'datetime', 'visibleInTable' => false, 'visibleInExport' => true,],
            ['name' => 'updated_at', 'label' => 'Updated', 'type' => 'datetime'],
            ['name' => 'users_monitored', 'label' => 'Users Monitored', 'type' => 'closure', 'function' => function (Art $art) {
                return implode(', ', $art->monitoredArts()->get()->map(function ($item) {
                    return "<a href='" . route('admin.users.show', ['id' => $item->user_id]) . "'>{$item->user->email}</a>";
                })->toArray());
            }],
        ];
    }

    protected function registerListFilters()
    {
        $this->crud->addFilter(
            [
                'type' => 'dropdown',
                'name' => 'category_id',
                'label' => 'Category',
            ],
            app(ArtCategoryRepository::class)->asAssocArray(),
            function ($value) {
                $this->crud->addClause('where', 'category_id', $value);
            }
        );
        $this->crud->addFilter(
            [
                'type' => 'dropdown',
                'name' => 'market_category_id',
                'label' => 'M.Category',
            ],
            app(MarketCategoryRepository::class)->asAssocArray(),
            function ($value) {
                $this->crud->addClause('where', 'market_category_id', $value);
            }
        );
        $this->crud->addFilter(
            [
                'type' => 'dropdown',
                'name' => 'set_id',
                'label' => 'Set',
            ],
            app(ArtSetRepository::class)->asAssocArray(),
            function ($value) {
                $this->crud->addClause('where', 'set_id', $value);
            }
        );
        $this->addBooleanFilter('from_shop', 'From Shop');
        $this->addBooleanFilter('as_from_shop', 'As From Shop');
        $this->crud->addFilter(
            [
                'type' => 'range',
                'name' => 'level',
                'label' => 'Level',
                'label_from' => 1,
                'label_to' => Art::max('need_level'),
            ],
            false,
            function ($value) {
                $range = json_decode($value);
                if ($range->from) {
                    $this->crud->addClause('where', 'need_level', '>=', (int) $range->from);
                }
                if ($range->to) {
                    $this->crud->addClause('where', 'need_level', '<=', (int) $range->to);
                }
            }
        );
        $this->crud->addFilter(
            [
                'type' => 'range',
                'name' => 'oa',
                'label' => 'OA',
                'label_from' => 1,
                'label_to' => Art::max('oa'),
            ],
            false,
            function ($value) {
                $range = json_decode($value);
                if ($range->from) {
                    $this->crud->addClause('where', 'oa', '>=', (int) $range->from);
                }
                if ($range->to) {
                    $this->crud->addClause('where', 'oa', '<=', (int) $range->to);
                }
            }
        );
        $this->crud->addFilter(
            [
                'type' => 'range',
                'name' => 'repair',
                'label' => 'Repair',
                'label_from' => 0,
                'label_to' => Art::max('repair'),
            ],
            false,
            function ($value) {
                $range = json_decode($value);
                if ($range->from) {
                    $this->crud->addClause('where', 'repair', '>=', (int) $range->from);
                }
                if ($range->to) {
                    $this->crud->addClause('where', 'repair', '<=', (int) $range->to);
                }
            }
        );
        $this->crud->addFilter(
            [
                'type' => 'range',
                'name' => 'strength',
                'label' => 'Strength',
                'label_from' => 0,
                'label_to' => Art::max('strength'),
            ],
            false,
            function ($value) {
                $range = json_decode($value);
                if ($range->from) {
                    $this->crud->addClause('where', 'strength', '>=', (int) $range->from);
                }
                if ($range->to) {
                    $this->crud->addClause('where', 'strength', '<=', (int) $range->to);
                }
            }
        );
        $this->crud->addFilter(
            [
                'type' => 'range',
                'name' => 'price',
                'label' => 'Price',
                'label_from' => 0,
                'label_to' => Art::max('shop_price'),
            ],
            false,
            function ($value) {
                $range = json_decode($value);
                if ($range->from) {
                    $this->crud->addClause('where', 'shop_price', '>=', (int) $range->from);
                }
                if ($range->to) {
                    $this->crud->addClause('where', 'shop_price', '<=', (int) $range->to);
                }
            }
        );
        $this->crud->addFilter(
            [
                'type' => 'range',
                'name' => 'ppf',
                'label' => 'PPF',
                'label_from' => 0,
                'label_to' => Art::max('price_per_fight'),
            ],
            false,
            function ($value) {
                $range = json_decode($value);
                if ($range->from) {
                    $this->crud->addClause('where', 'price_per_fight', '>=', (int) $range->from);
                }
                if ($range->to) {
                    $this->crud->addClause('where', 'price_per_fight', '<=', (int) $range->to);
                }
            }
        );
        $this->crud->addFilter(
            [
                'type' => 'date_range',
                'name' => 'created_at',
                'label' => 'Created',
            ],
            false,
            function ($value) {
                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', $dates->from);
                $this->crud->addClause('where', 'created_at', '<=', $dates->to . ' 23:59:59');
            }
        );
        $this->crud->addFilter(
            [
                'type' => 'date_range',
                'name' => 'updated_at',
                'label' => 'Updated',
            ],
            false,
            function ($value) {
                $dates = json_decode($value);
                $this->crud->addClause('where', 'updated_at', '>=', $dates->from);
                $this->crud->addClause('where', 'updated_at', '<=', $dates->to . ' 23:59:59');
            }
        );
    }

    protected function addBooleanFilter($name, $label)
    {
        $this->crud->addFilter(
            [
            'type' => 'dropdown',
            'name' => $name,
            'label' => $label
            ],
            [0 => 'No', 1 => 'Yes'],
            function ($value) use ($name) {
                $this->crud->addClause('where', $name, $value);
            }
        );
    }
}
