<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Core\Enums\Queue;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Notifications\Notification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AdminNotify implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected Notification $notification;

    public function __construct(Notification $notification)
    {
        $this->notification = $notification;

        $this->queue = Queue::NOTIFICATIONS->value;
    }

    public function handle(): void
    {
        User::getSuperAdmin()->notifyNow($this->notification);
    }
}
