const path = require('path');

module.exports = {
    env: {
        browser: true,
        es6: true,
    },
    extends: [
        'plugin:vue/recommended',
        'airbnb-base',
    ],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
    },
    parserOptions: {
        parser: 'babel-eslint',
        ecmaVersion: 2018,
        sourceType: 'module',
    },
    plugins: [
        'vue',
    ],
    rules: {
        indent: ['error', 4, { SwitchCase: 1 }],
        'vue/require-prop-types': 'off',
        'valid-jsdoc': 'off',
        'no-invalid-this': 'off',
        'vue/no-v-html': 'off',
        'linebreak-style': ['error', 'unix'],
        'vue/html-indent': ['warn', 4],
        'no-param-reassign': 0,
        'max-len': 0,
        'no-underscore-dangle': 0,
        'import/extensions': [
            'error',
            'always',
            { js: 'never', vue: 'never' },
        ],
    },
    settings: {
        'import/resolver': {
            alias: {
                map: [
                    ['JS', path.resolve(process.cwd(), 'resources/assets/js')],
                    ['SASS', path.resolve(process.cwd(), 'resources/assets/sass')],
                ],
                extensions: ['.vue', '.js', '.json', '.css', '.scss'],
            },
        },
    },
};
