<?php
return [
    "400" => "Bad Request",
    "401" => "Unauthorized",
    "402" => "Payment Required",
    "403" => "Forbidden",
    "404" => "Page Not Found",
    "405" => "Method Not Allowed",
    "413" => "Payload Too Large",
    "419" => "Not valid token",
    "422" => "Data not validated",
    "429" => "Too Many Requests",
    "500" => "Server error",
    "503" => "Service is unavailable",
    "500_message" => "Try reloading the page. If there is an error, inform the administrator about it again.",
    "503_message" => "The site is temporarily unavailable. Technical work in progress",
    "action_not_allowed" => "Action [:action] not allowed",
    "auth" => [
        "bad_vk_response" => "The answer from VK has an error",
        "failed" => "Username and password do not match.",
        "throttle" => "Too many login attempts. Please try again in :seconds seconds."
    ],
    "cant_connect_with_driver" => "Can't connect with :Driver now, try later or change method",
    "email_scope_is_required" => "Access to e-mail is required",
    "exceeded_filters_of_monitored_arts" => "Limit of filters on monitored arts is exceeded, maximum :max_filters filters",
    "exceeded_monitored_arts" => "Limit of monitored arts is exceeded, maximum :max_arts artifacts",
    "exceeded_subscription_lots" => "Exceeded the subscription limit for new artifacts, maximum :max_arts artifacts",
    "hwm" => [
        "after_login_redirect_to_not_home_page" => "After login redirect to not Home Page",
        "character_not_found" => "Character :character not found",
        "dom_content_is_not_loaded" => "Dom content is not loaded",
        "expected_page" => "Expected [:page] page",
        "invalid_response_after_parsing" => "Invalid \":type\" response structure after parsing",
        "it_is_nota_expected_page" => "It is not a [:page] page",
        "limit_requests_is_exceeded" => "Limit number requests is exceeded",
        "not_found_art_page" => "Artifact page not found",
        "not_found_lot_page" => "Not found lot page",
        "profile_unavailable_by_reason" => "Profile is unavailable, reason [:reason]",
        "reasons" => [
            "clan_in_war" => "the hero's clan participates in hostilities",
            "in_card_game" => "in a card game",
            "in_war" => "in battle",
            "moving_in_map" => "move around the map"
        ],
        "undefined_character" => "Undefined character",
        "unsupported_category_for_art" => "Unsupported category for art",
        "you_must_setup_login_and_password" => "You must setup login and password for login in profile"
    ],
    "image_optimize_error" => "Error during optimize image",
    "invalid_state" => "Invalid state",
    "model_not_found" => ":Model not found",
    "passwords" => [
        "password" => "The password must be at least six characters long and match the confirmation.",
        "reset" => "Your password has been reset!",
        "sent" => "Password reset link has been sent!",
        "token" => "Incorrect password reset code.",
        "user" => "The user with the specified email address could not be found."
    ],
    "something_went_wrong" => "Something went wrong. Please try again or contact your administrator"
];
