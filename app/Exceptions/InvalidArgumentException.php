<?php

declare(strict_types=1);

namespace App\Exceptions;

class InvalidArgumentException extends \InvalidArgumentException
{
    /**
     * InvalidArgumentException constructor.
     * @param string $argument
     * @param mixed|null $invalidValue
     */
    public function __construct(string $argument, $invalidValue = null)
    {
        $invalidValue = is_scalar($invalidValue) ? ' ' . (string) $invalidValue : '';
        $message = "Argument $argument has invalid value$invalidValue";

        parent::__construct($message);
    }
}
