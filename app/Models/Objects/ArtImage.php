<?php

declare(strict_types=1);

namespace App\Models\Objects;

use App\Services\ImageOptimize\ImageOptimize;
use App\Exceptions\ServerErrors\ImageOptimizeError;
use App\Models\Art;
use Illuminate\Support\Arr;
use Image;
use Log;
use Storage;

class ArtImage
{
    protected string $artStoragePath = 'arts';
    protected Art $art;
    protected ?string $prefix;
    protected string $filename;
    protected string $ext;
    protected int $width;
    protected int $height;

    public function __construct(Art $art, int $width, string $prefix = null)
    {
        $this->art = $art;
        $this->width = $width;
        $this->height = $width;
        $this->prefix = $prefix !== 'default' ? $prefix : null;

        $this->ext = $this->getImageExt();
        $this->filename = (string) preg_replace('/\?.*/', '', $this->getImageFilename());
    }

    public function toArray(): array
    {
        return [
            'filename' => $this->filename,
            'fullUrl' => $this->getUrl(),
            'prefix' => $this->prefix,
            'width' => $this->width,
            'height' => $this->height,
        ];
    }

    public function getImageFilename(): string
    {
        return $this->art->hwm_id . $this->prefix .  '.' . $this->ext;
    }

    public function getImageExt(): string
    {
        return $this->art->original_image ? Arr::last(explode('.', $this->art->original_image)) : 'png';
    }

    public function getRealPath(): string
    {
        return Storage::path($this->artStoragePath . '/' . $this->filename);
    }

    public function getUrl(): string
    {
        return Storage::url($this->artStoragePath . '/' . $this->filename);
    }

    public function exists(): bool
    {
        return file_exists($this->getRealPath());
    }

    /**
     * @throws ImageOptimizeError
     */
    public function handleImage(): bool
    {
        if ($this->prefix) {
            $source = $this->art->images['default']->getRealPath();
        } else {
            $source = $this->art->original_image;
        }

        // remove query string from img
        $source = preg_replace('/\?.*/', '', $source);

        if (!$source) {
            return false;
        }

        $originalImage = Image::make($source);


        if ($originalImage->getWidth() < $this->width || $originalImage->getHeight() < $this->height) {
            Log::critical("Art image $source less then minimum requirements");
        }

        if ($originalImage->getWidth() != $this->width || $originalImage->getHeight() != $this->height) {
            $originalImage->resize($this->width, $this->height);
        }

        $originalImage->save(Storage::path($this->artStoragePath . '/' . $this->filename));

        app(ImageOptimize::class)->optimize($this->getRealPath(), ImageOptimize::QUICKLY_TYPE);

        return true;
    }
}
