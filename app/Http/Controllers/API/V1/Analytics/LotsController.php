<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V1\Analytics;

use App\Http\Controllers\API\V1\ApiController;
use App\Http\Requests\API\Analytics\LotsRequest;
use App\HWM\Enums\EntityType;
use App\Services\SearchLots\SearchLotService;
use App\Services\SearchLots\ValueObjects\Aggregation\AggregationsByEntityRequest;
use App\Services\SearchLots\ValueObjects\Aggregation\AggregationType;
use App\ValueObjects\DateRange;

class LotsController extends ApiController
{
    public function __invoke(LotsRequest $request, SearchLotService $searchLotService)
    {
        $entityType = EntityType::from($request->get('entity_type'));
        $from = $request->get('from');
        $to = $request->get('to');
        $metricsRaw = $request->get('metrics');

        $metrics = array_map(function ($item) {
            $array = explode(':', $item);

            return new AggregationType($array[1], $array[0]);
        }, explode(';', $metricsRaw));

        $interval = $request->get('interval');
        $entityId = $request->get('entity_id');

        $aggsRequest = new AggregationsByEntityRequest(
            $entityType,
            new DateRange(new \DateTimeImmutable($from), new \DateTimeImmutable($to)),
            $metrics,
            $interval,
            $entityId,
        );

        $response = $searchLotService->getAggregationsByEntity($aggsRequest);

        return $this->response(['data' => $response]);
    }
}
