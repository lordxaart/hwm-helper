<?php

declare(strict_types=1);

namespace App\HWM\Helpers;

use Cache;
use Carbon\Carbon;
use Carbon\CarbonInterval;

class HwmHelper
{
    use Calculator;

    public const HEROES_TZ = 'Europe/Moscow';
    public const CACHE_PREFIX = 'hwm.static.';

    // get specific resource from json (resources/json/*.json) with cached
    public static function getJson(string $key, bool $fromCache = true): ?array
    {
        $result = function () use ($key) {
            $json = file_get_contents(hwm_storage_path("$key.json")) ?: '';
            try {
                return json_decode($json, true, 512, JSON_THROW_ON_ERROR) ?: [];
            } catch (\JsonException) {
                return [];
            }
        };

        return $fromCache ? Cache::remember(self::CACHE_PREFIX . $key, intval(CarbonInterval::day()->totalSeconds), $result) : $result();
    }

    public static function refreshCache(): void
    {
        /** @var string[] $files */
        $files = scandir(hwm_storage_path());
        foreach ($files as $file) {
            if (str_contains($file, '.json')) {
                $key = str_replace('.json', '', $file);
                $cacheKey = self::CACHE_PREFIX . $key;
                if (Cache::has($cacheKey)) {
                    Cache::delete($cacheKey);
                }
                self::getJson($key);
            }
        }
    }

    public static function getRegex(string $key): string
    {
        $regex = [
            'craft_full' => '/\[([IieEaAfFwWdDnN]\d{1,2}){1,5}\]/',
            'craft' => '/([IieEaAfFwWdDnN]\d{1,2}){1,5}/',
        ];

        return $regex[$key];
    }

    public static function craftToArray(string $craft = null): ?array
    {
        if (!$craft) {
            return null;
        }

        preg_match_all('/[IieEaAfFwWdDnN]|\d{1,2}/', $craft, $results);
        $results = $results[0];
        $craft = [];

        for ($i = 0; $i < count($results); $i += 2) {
            $craft[$results[$i]] = intval($results[$i + 1]);
        }

        return $craft;
    }

    public static function clearIntValue(string $val): int
    {
        $val = str_replace('&nbps;', '', $val);
        $val = str_replace(',', '', $val);

        return intval($val);
    }

    public static function parseEntityLink(string $text, string $link, bool $nick = false): array
    {
        $link = str_replace('.', '\.', $link);
        if ($nick) {
            preg_match('/<a.*?href="' . $link . '\?nick=(.+).*?">(.*?)<\/a>/', $text, $matches);
        } else {
            preg_match('/<a.*?href="' . $link . '\?id=([\w-]+).*?">(.*?)<\/a>/', $text, $matches);
        }

        return [$matches[1] ?? null, $matches[2] ?? null];
    }
}
