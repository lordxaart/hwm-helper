import Vue from 'vue';
import VueI18n from 'vue-i18n';
import VueMeta from 'vue-meta';

// localization
Vue.use(VueI18n);
// Meta tags
Vue.use(VueMeta);

Vue.directive('focus', {
    inserted(el) {
        el.focus();
    },
});
