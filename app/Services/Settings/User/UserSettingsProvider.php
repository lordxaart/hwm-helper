<?php

declare(strict_types=1);

namespace App\Services\Settings\User;

use App\Core\ValueObject\UserId;
use App\Models\Objects\UserSettings;
use App\Models\User;
use Illuminate\Database\ConnectionInterface;

class UserSettingsProvider
{
    private const TABLE_NAME = User::TABLE_NAME;

    public function __construct(private ConnectionInterface $connection)
    {
        //
    }

    public function getSettingsByUserId(UserId $userId): UserSettings
    {
        $raw = $this->connection
            ->table(self::TABLE_NAME)
            ->where('id', $userId->value)
            ->pluck('settings')
            ->first();

        return UserSettings::parseFromJson($raw);
    }
}
