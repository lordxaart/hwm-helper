<?php

declare(strict_types=1);

namespace App\Actions\Art;

use App\Events\HWM\SetAddedEvent;
use App\Models\ArtSet;
use Illuminate\Console\Command;
use Lorisleiva\Actions\Concerns\AsAction;

class ArtSetAdd
{
    use AsAction;

    public string $commandSignature = 'hwm:set:add {set_id : Set ID} {set_title : Set Title}';
    public string $commandDescription = 'Add art set by :set_id and :set_title';

    public function asCommand(Command $command): int
    {
        $set = $this->handle(strval($command->argument('set_id')), strval($command->argument('set_title')));

        $command->info("Set {$set->getTitle()} was successfully added");

        return 0;
    }

    public function handle(string $hwmId, string $title): ArtSet
    {
        if ($set = ArtSet::findByHwmId($hwmId)) {
            /** @var ArtSet $set */
            return $set;
        }

        ArtSet::create([
            'hwm_id' => $hwmId,
            'title' => $title,
        ]);

        $set = ArtSet::findByHwmId($hwmId);

        event(new SetAddedEvent($set));

        return $set;
    }
}
