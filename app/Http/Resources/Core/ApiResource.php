<?php

declare(strict_types=1);

namespace App\Http\Resources\Core;

use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class ApiResource extends JsonResource
{
    public const QUERY_INCLUDES_KEY = 'include';
    public const QUERY_FIELDS_KEY = 'fields';

    public static $wrap = 'data';

    protected array $includes = [];

    public function __construct($resource)
    {
        parent::__construct($resource);

        $this->includes = $this->parseIncludes();
    }

    public function setIncludes(array $includes): static
    {
        $this->includes = $includes;

        return $this;
    }

    protected function transformByRequest(array $data, Request $request = null): array
    {
        $request = $request ?: Container::getInstance()->make(Request::class);

        $changedData = $data;

        // return only selected fields
        if ($request->query(self::QUERY_FIELDS_KEY)) {
            $fields = explode(',', $request->query(self::QUERY_FIELDS_KEY));

            if (!empty($fields)) {
                $changedData = Arr::only($changedData, $fields);
            }
        }

        return $changedData;
    }

    protected function parseIncludes(Request $request = null): array
    {
        $request = $request ?: Container::getInstance()->make(Request::class);

        return array_filter(explode(',', $request->query(self::QUERY_INCLUDES_KEY, '')));
    }
}
