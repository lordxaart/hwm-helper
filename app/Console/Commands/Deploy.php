<?php

declare(strict_types=1);

namespace App\Console\Commands;

use Symfony\Component\Process\Process;

class Deploy extends Command
{
    /** @var string  */
    protected $signature = 'app:deploy {--front}';

    /** @var string  */
    protected $description = 'Basic actions for deploy';

    public function handle(): void
    {
        $this->env();

        $this->storageLink();

        $this->cache();

        $this->call('migrate');

        if (isWebApp()) {
            $this->staticFiles();

            $this->cache();
        }

        if (isWebApp()) {
            $this->front();
        }

        $this->call('telegram:update:webhook');

        $this->info('Deploy successfully');
    }

    protected function env(): void
    {
        if (!file_exists(base_path('.env'))) {
            copy(base_path('.env.example'), base_path('.env'));
        }

        if (!env('APP_KEY')) {
            $this->call('key:generate');
        }

        $this->call('generate:env', ['testing']);
    }

    protected function storageLink(): void
    {
        if (file_exists(public_path('storage'))) {
            unlink(public_path('storage'));
        }

        $this->call('storage:link');
    }

    protected function cache(): void
    {
        $this->call('cache:clear');
        $this->call('config:clear');
        $this->call('view:clear');
        $this->call('route:clear');
        $this->call('event:cache');
    }

    protected function staticFiles(): void
    {
        $this->call('generate:sitemap');
        $this->call('generate:static');
    }

    protected function front(): void
    {
        $this->call('generate:lang');

        if ($this->option('front')) {
            $this->info('Start npm build');
            $this->exec();
            $this->info('End npm build');
        }
    }

    private function exec(): void
    {
        (new Process(explode(' ', 'npm run all')))->setTimeout(600)->run();
    }
}
