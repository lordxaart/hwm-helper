<?php

declare(strict_types=1);

namespace App\Exceptions\ServerErrors;

use App\Exceptions\ServerError;

/**
 * Class ImageOptimizeError
 * @package App\Exceptions\ServerErrors
 */
class ImageOptimizeError extends ServerError
{
    public function __construct(string $message = '')
    {
        $message = $message ?: trans('errors.image_optimize_error');

        parent::__construct($message);
    }
}
