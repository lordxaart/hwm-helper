<?php

declare(strict_types=1);

namespace Tests\Feature\API;

use App\HWM\Enums\EntityType;
use App\HWM\Enums\LotType;
use App\Models\LotFilter;
use App\Models\User;

class LotFiltersTest extends ApiTestCase
{
    private const API_LOT_FILTERS = '/api/v1/market/lots/filters';

    public function testCreateFilter()
    {
        $user = User::factory()->create();
        $filter = [
            'user_id' => $user->id,
            'lot_type' => LotType::AUCTION->value,
            'entity_type' => EntityType::ART->value,
            'entity_id' => 'coldring_n',
            'terms' => [
                'price_per_fight > 11.1'
            ],
            'active' => true,
        ];

        $response = $this->actingAs($user)->postJson(self::API_LOT_FILTERS, $filter);

        $response->assertStatus(201);

        $response->assertJsonStructure(['data' => [
            'id',
            'lotType',
            'entityType',
            'entityId',
            'terms',
            'isActive',
        ]]);
    }

    public function testUpdateFilter()
    {
        $user = User::factory()->create();
        $filter = LotFilter::factory()->for($user)->create();

        $response = $this->actingAs($user)->postJson(self::API_LOT_FILTERS . '/' . $filter->id, [
            'terms' => [
                'price <= 100',
            ],
        ]);

        $response->assertStatus(200);
    }

    public function testDeleteFilter()
    {
        $user = User::factory()->create();
        $filter = LotFilter::factory()->for($user)->create();

        $response = $this->actingAs($user)->deleteJson(self::API_LOT_FILTERS . '/' . $filter->id);

        $response->assertStatus(204);
    }

    public function testGetListFiltersForUser()
    {
        $count = 5;
        $user = User::factory()->create();
        LotFilter::factory()->count($count)->for($user)->create();

        $response = $this->actingAs($user)->getJson(self::API_LOT_FILTERS);

        $response->assertStatus(200);
        $this->assertCount($count, $response->json('data'));
    }
}
