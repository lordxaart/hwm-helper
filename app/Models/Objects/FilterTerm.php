<?php

declare(strict_types=1);

namespace App\Models\Objects;

use App\HWM\Entities\BaseLot;
use Illuminate\Contracts\Support\Arrayable;

class FilterTerm implements Arrayable
{
    public const OPERATOR_MORE = '>';
    public const OPERATOR_MORE_OR_EQUAL = '>=';
    public const OPERATOR_LESS = '<';
    public const OPERATOR_LESS_OR_EQUAL = '<=';
    public const OPERATOR_EQUAL = '==';
    public const OPERATOR_HAS = 'has';

    public const FIELD_PRICE = 'price';
    public const FIELD_PPF = 'price_per_fight';
    public const FIELD_B_STRENGTH = 'base_strength';
    public const FIELD_C_STRENGTH = 'current_strength';
    public const FIELD_SELLER_NAME = 'seller_name';
    public const FIELD_LOT_TYPE = 'lot_type';
    public const FIELD_CRAFT = 'craft';

    protected string $field;

    protected string $operator;

    protected mixed $value;

    public string $string;

    public function __construct(string $field, string $operator, mixed $value)
    {
        $this->setField($field);
        $this->setOperator($operator);
        $this->setValue($value);
    }

    public static function operators(): array
    {
        return [
            self::OPERATOR_MORE_OR_EQUAL,
            self::OPERATOR_LESS_OR_EQUAL,
            self::OPERATOR_EQUAL,
            self::OPERATOR_MORE,
            self::OPERATOR_LESS,
            self::OPERATOR_HAS,
        ];
    }

    public static function fields(): array
    {
        return [
            self::FIELD_PRICE,
            self::FIELD_PPF,
            self::FIELD_B_STRENGTH,
            self::FIELD_C_STRENGTH,
            self::FIELD_SELLER_NAME,
            self::FIELD_CRAFT,
        ];
    }

    public static function parserFromString(string $str): self
    {
        $array = explode(' ', trim($str));

        if (count($array) !== 3) {
            throw new \InvalidArgumentException('Invalid string for filter term, must have three positions separate by space');
        }

        $obj = new self($array[0], $array[1], $array[2]);
        $obj->string = $str;

        return $obj;
    }

    /**
     * @param string $str
     * @return FilterTerm[]
     */
    public static function parseToArray(string $str): array
    {
        $values = array_unique(array_map(function ($item) {
            return trim($item);
        }, explode(';', trim($str))));

        $data = [];

        foreach ($values as $item) {
            $data[] = self::parserFromString($item);
        }

        return $data;
    }

    public function setField(string $field): self
    {
        if (!in_array($field, self::fields())) {
            throw new \InvalidArgumentException("Invalid field value [$field] for filter term");
        }

        $this->field = $field;

        return $this;
    }

    public function setOperator(string $operator): static
    {
        if ($operator === '=') {
            $operator = self::OPERATOR_EQUAL;
        }

        if (!in_array($operator, self::operators())) {
            throw new \InvalidArgumentException("Invalid operator value [$operator] for filter term");
        }

        $this->operator = $operator;

        return $this;
    }

    public function setValue(mixed $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getField(): string
    {
        return $this->field;
    }

    public function getOperator(): string
    {
        return $this->operator;
    }

    /**
     * @return mixed
     */
    public function getValue(): mixed
    {
        return $this->value;
    }

    public function toArray(): array
    {
        return [
            'field' => $this->field,
            'operator' => $this->operator,
            'value' => $this->value,
        ];
    }

    public function isLotAccording(BaseLot $lot): bool
    {
        if (!array_key_exists($this->getField(), $lot->toArray())) {
            throw new \InvalidArgumentException('Undefined field ' . $this->getField() . ' in lot');
        }

        $value = $lot->{$this->getField()};

        $expectedValue = $this->getValue();

        return self::checkOperatorForExpectedValue($this->getOperator(), $value, $expectedValue);
    }

    /**
     * @param string $operator
     * @param mixed $value
     * @param mixed $expectedValue
     * @return bool
     */
    public static function checkOperatorForExpectedValue(string $operator, mixed $value, mixed $expectedValue): bool
    {
        return match ($operator) {
            self::OPERATOR_MORE => $value > $expectedValue,
            self::OPERATOR_MORE_OR_EQUAL => $value >= $expectedValue,
            self::OPERATOR_LESS => $value < $expectedValue,
            self::OPERATOR_LESS_OR_EQUAL => $value <= $expectedValue,
            self::OPERATOR_EQUAL => $value == $expectedValue,
            self::OPERATOR_HAS => boolval($value),
            default => throw new \InvalidArgumentException('Invalid operator ' . $operator),
        };
    }
}
