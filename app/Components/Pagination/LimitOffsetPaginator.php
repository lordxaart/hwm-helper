<?php

declare(strict_types=1);

namespace App\Components\Pagination;

use Illuminate\Support\Collection;
use Illuminate\Support\Traits\ForwardsCalls;

/**
 * @mixin Collection
 */
class LimitOffsetPaginator
{
    use ForwardsCalls;

    private Collection $items;
    private int $limit;
    private int $offset;
    private string $path = '/';
    private array $query = [];

    public function __construct(Collection $items, int $limit, int $offset)
    {
        $this->items = $items;
        $this->limit = $limit;
        $this->offset = $offset;
    }

    public function getItems(): Collection
    {
        return $this->items;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function hasMorePages(): bool
    {
        return $this->items->count() === $this->limit;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function setQuery(array $query): self
    {
        $this->query = $query;

        return $this;
    }

    public function nextPageUrl(): ?string
    {
        if (!$this->hasMorePages()) {
            return null;
        }

        return $this->resolvePath(['offset' => $this->offset + $this->limit]);
    }

    public function previousPageUrl(): ?string
    {
        if ($this->offset <= 0) {
            return null;
        }

        $offset = $this->offset >= $this->limit
            ? $this->offset - $this->limit
            : $this->offset;

        return $this->resolvePath(['offset' => $offset]);
    }

    public function resolvePath(array $query): string
    {
        $query = array_merge($this->query, $query);

        return $this->path . '?' . http_build_query($query);
    }

    /**
     * Make dynamic calls into the collection.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        return $this->forwardCallTo($this->items, $method, $parameters);
    }
}
