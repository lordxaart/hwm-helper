@php
    $entity = $column['entity'] ?? 'arts';

    $repository = app(\App\HWM\Repositories\ArtRepository::class);
@endphp
<div class="flex">
@foreach($entry->$entity as $art)
    @php
        if ($art instanceof \App\Models\MonitoredArt) {
            $art = $art->art;
        }

        /** @var \App\Models\Art $art */
    @endphp
    <a href="{{ route('admin.arts.show', ['id' => $art->id]) }}" class="mr-1 mb-1">
        <img
            src="{{ $art->images['small']->getUrl() }}"
            alt="{{ $art->hwm_id }}"
            title="{{ $art->title }}"
            data-toggle="tooltip"
            class="art-img"
        >
    </a>
@endforeach
</div>
