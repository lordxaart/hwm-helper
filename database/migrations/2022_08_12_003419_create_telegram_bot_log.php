<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telegram_bot_log', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->bigInteger('update_id')->unique();
            $table->json('message')->nullable();
            $table->string('command')->nullable();
            $table->string('from')->nullable();
            $table->timestamp('created_at', 6)->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('telegram_bot_log');
    }
};
