<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHashAndStatusToEmailLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('email_log', function (Blueprint $table) {
            $table->string('email_id')->after('id');
            $table->boolean('status')->after('email_id');
            $table->timestamp('date_sent')->after('date')->nullable();

            $table->index('email_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('email_log', function (Blueprint $table) {
            $table->dropColumn('email_id');
            $table->dropColumn('status');
            $table->dropColumn('date_sent');
        });
    }
}
