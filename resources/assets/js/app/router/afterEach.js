// eslint-disable-next-line no-unused-vars
import { logEvent } from 'JS/app/utils/amplitude';

const afterEach = (app, to, from) => {
    setTimeout(() => {
        app.loadingPage = false;
    }, 200);

    logEvent('PAGE_LOADED', {
        page: to.path,
        previous: from.path,
    });
};

export default afterEach;
