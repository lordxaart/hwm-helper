<?php

namespace Database\Factories;

use App\Models\Character;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CharacterFactory extends Factory
{
    /** @var string */
    protected $model = Character::class;

    public function definition()
    {
        return [
            'hwm_id' => mt_rand(1000, 999999),
            'login' => $this->faker->userName,
            'password' => '111111',
            'user_id' => User::factory(),
            'level' => mt_rand(5, 20),
            'experience' => mt_rand(1000, 99999999),
            'status' => $this->faker->randomElement(\App\HWM\Entities\Character::getStatuses()),
        ];
    }
}
