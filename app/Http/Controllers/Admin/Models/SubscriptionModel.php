<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriptionModel extends Model
{
    protected $table = 'subscriptions';

    public function plans()
    {
        return $this->hasMany(SubscriptionPlanModel::class, 'subscription_id', 'id');
    }
}
