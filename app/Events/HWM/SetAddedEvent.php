<?php

declare(strict_types=1);

namespace App\Events\HWM;

use App\Models\ArtSet;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SetAddedEvent
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    public ArtSet $set;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ArtSet $set)
    {
        $this->set = $set;
    }
}
