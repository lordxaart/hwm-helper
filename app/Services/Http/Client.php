<?php

declare(strict_types=1);

namespace App\Services\Http;

use App\HWM\Exceptions\RequestError;
use App\Models\RequestSpeedLog;
use App\Services\Other\RandomUserAgent;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Cookie\FileCookieJar;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\TransferStats;
use Illuminate\Support\Arr;
use Psr\Http\Message\ResponseInterface;

class Client
{
    protected ?string $cookieFile = null;

    protected GuzzleClient $client;

    protected ResponseInterface $response;

    protected ?string $content = null;

    protected array $cookies = [];

    protected ?string $proxy = null;

    protected ?string $userAgent = null;

    protected ?string $lastRequestUrl = null;

    protected array $proxiesList = [];

    protected array $defaultHeaders = [];

    protected array $baseConfig = [
        RequestOptions::COOKIES => true,
        RequestOptions::ALLOW_REDIRECTS => [
            'max'             => 5,
            'strict'          => false,
            'referer'         => true,
            'protocols'       => ['http', 'https'],
            'track_redirects' => true
        ],
        RequestOptions::CONNECT_TIMEOUT => 2,
        RequestOptions::TIMEOUT => 10,
    ];

    protected array $config = [];

    protected string $requestId = UNDEFINED;

    public function __construct(array $options = [])
    {
        $this->client = new GuzzleClient($this->baseConfig);
        $this->config = $options;
        $this->requestId = uniqid();
    }

    private function loadProxies(): void
    {
        $handle = fopen(resource_path('assets/proxy_list.txt'), 'r');

        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $this->proxiesList[] = 'ticketsolutions:eap0Ishe@' . trim($line) . ':6745';
            }

            fclose($handle);
        }
    }

    public function getRandomProxy(string $uniqid = 'random_string'): ?string
    {
        if (empty($this->proxiesList)) {
            $this->loadProxies();
        }

        /** @var string $proxy */
        $proxy = Arr::random($this->proxiesList);
        return str_replace(':uniqeid', $uniqid, $proxy);
    }

    public function getUserAgent(bool $isMobile = false): ?string
    {
        try {
            return app(RandomUserAgent::class)->random([
                'device_type' => $isMobile ? 'Mobile' : 'Desktop',
            ]);
        } catch (\Exception $e) {
            unset($e);
            return null;
        }
    }

    public function setRandomProxy(): self
    {
        $this->proxy = $this->getRandomProxy();

        return $this;
    }

    public function getProxy(): ?string
    {
        return $this->proxy;
    }

    public function setRandomUserAgent(bool $isMobile = false): void
    {
        $this->userAgent = $this->getUserAgent($isMobile);
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @throws RequestError
     */
    public function request(string $method, string $url, array $data = []): ResponseInterface
    {
        // save in request logs, need for monitoring errors (redirect)
        $extraLog = $data['log'] ?? null;

        // todo: temp, need add new column in table and save extra params
        $bot = null;
        if (isset($data['botLogin'])) {
            $bot = $data['botLogin'];
            unset($data['botLogin']);
        }

        try {
            $options = [
                'headers' => $this->getDefaultHeaders(),
                // call after each request (with redirect)
               'on_stats' => function (TransferStats $stats) use ($extraLog, $bot) {
                   $this->lastRequestUrl = (string) $stats->getEffectiveUri();
                   // Save each request in db logs
                try {
                    if (settings()->enable_speed_log) {
                        RequestSpeedLog::addRecord([
                         'uniqid' => $this->requestId,
                         'speed' => floatval($stats->getTransferTime()),
                         'url' => $stats->getEffectiveUri(),
                         'method' => $stats->getRequest()->getMethod(),
                         'code' => $stats->getResponse()?->getStatusCode() ?: 000,
                         'proxy' => $this->proxy ?: null,
//                         'user_agent' => $this->userAgent ?: null,
                         'html' => $bot,
                         'headers' => $extraLog ? json_encode(['log' => $extraLog]) : null,
                        ]);
                    }
                } catch (\Exception $e) {
                    unset($e);
                }
               },
            ];

            if (count($data)) {
                if ($method === 'get') {
                    $options['query'] = $data;
                } else {
                    $options['form_params'] = $data;
                }
            }

            // Set cookies sync
            if ($this->cookieFile) {
                $jar = new FileCookieJar($this->cookieFile, true);
                $options['cookies'] = $jar;
            }
            // Proxy
            if ($this->proxy) {
                $options['proxy'] = $this->proxy;
            }

            $options = array_merge($this->config, $options);

            $response = $this->client->request($method, $url, $options);

            // set response and convert body charset
            $this->setResponse($response);
        } catch (GuzzleException $e) {
            throw new RequestError($e->getMessage(), $e->getCode(), $e->getPrevious());
        }

        return $this->response;
    }

    protected function getDefaultHeaders(): array
    {
        if ($this->userAgent) {
            $this->defaultHeaders['User-Agent'] = $this->userAgent;
        }

        return $this->defaultHeaders;
    }

    protected function setResponse(ResponseInterface $response): void
    {
        $this->response = $response;

        $this->content = $response->getBody()->getContents();
    }
}
