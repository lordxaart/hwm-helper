<?php

declare(strict_types=1);

namespace App\Services\Parser;

use App\Models\Lot;
use App\Services\Parser\Storages\StorageForExistLots;
use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\Console\Output\OutputInterface;

class ExistsLotParser extends AbstractParser
{
    public const SIZE_BATCH_IDS = 1000;
    public const SLEEP_ON_WAITING_QUERY = 1;

    protected StorageForExistLots $storage;
    protected Builder $query;
    protected array $batchIds = [];
    private int $currentPage = 1;
    private array $scopes = [
        'onlyMonitored' => false,
        'withoutMonitored' => false,
        'active' => false,
        'onlySale' => false,
        'withoutSale' => false,
        'onlyArts' => false,
        'withoutArts' => false,
        'missParsing' => false,
        'undefined' => false,
    ];

    public function __construct(OutputInterface $output = null, array $scopes = [])
    {
        foreach ($this->scopes as $scope => $val) {
            $this->scopes[$scope] = in_array($scope, $scopes);
        }

        $this->scopes = array_keys(array_filter($this->scopes));

        $this->storage = new StorageForExistLots($this->getType());

        parent::__construct($output);

        $this->initQuery();

        $this->setNextRunner();
    }

    public function getType(): string
    {
        $type = parent::getType();

        if (count($this->scopes)) {
            $type .= '_' . implode('_', $this->scopes);
        }

        return mb_strtolower($type);
    }

    public function hasMoreIds(): bool
    {
        return $this->storage->hasMore();
    }

    protected function initQuery(): void
    {
        $this->query = Lot::query()->notFinished();

        if (count($this->scopes)) {
            $this->query->scopes($this->scopes);
        }
    }

    protected function setNextRunner(): void
    {
        $lotId = $this->getNextLotId();

        if (!$lotId) {
            sleep(self::SLEEP_ON_WAITING_QUERY);
            return;
        }

        $this->currentRunner = new LotRunner(LotRunner::TYPE_REVIEW, $lotId);
    }

    protected function getNextLotId(): int
    {
        if (!$id = $this->storage->pop()) {
            $this->fetchLotsFromQuery();

            if (!$id = $this->storage->pop()) {
                return 0;
            }
        }

        return $id;
    }

    protected function fetchLotsFromQuery(): void
    {
        $cacheKey = $this->getType() . '_locked';

        if ($this->storage->isLocked($cacheKey)) {
            return;
        }

        $this->storage->locked($cacheKey);

        $query = $this->query->clone();

        $query
            ->limit(self::SIZE_BATCH_IDS)
            ->offset(($this->currentPage - 1) * self::SIZE_BATCH_IDS)
            ->orderBy('updated_at');

        $this->log('Fetch batch ids');
        $this->storage->set($query->pluck('lot_id')->toArray());

        $this->currentPage++;

        $this->storage->unlocked($cacheKey);
    }
}
