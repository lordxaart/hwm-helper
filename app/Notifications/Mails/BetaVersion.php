<?php

declare(strict_types=1);

namespace App\Notifications\Mails;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BetaVersion extends Mailable
{
    use Queueable;
    use SerializesModels;

    public User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function build(): void
    {
        $this->subject('Новая версия сайта')->markdown('emails.beta', ['user' => $this->user]);
    }
}
