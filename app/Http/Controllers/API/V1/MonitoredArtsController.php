<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V1;

use App\Actions\User\AddArtToMonitoring;
use App\Http\Requests\API\MonitoredArtStore;
use App\Http\Resources\MonitoredArtResource;
use App\Models\MonitoredArt;
use App\Models\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class MonitoredArtsController extends ApiController
{
    public function index(Request $request)
    {
        /** @var User $user */
        $user = user();
        $query = $user->monitoredArts()->withoutGlobalScope('active');

        if ($request->query('only_active')) {
            $query->active();
        }

        return $this->response(MonitoredArtResource::collection($query->get()));
    }

    public function store(MonitoredArtStore $request)
    {
        $monitoredArt = AddArtToMonitoring::run(user(), $request->input('art_id'));

        return $this->response(new MonitoredArtResource($monitoredArt))
            ->setStatusCode($monitoredArt->wasRecentlyCreated ? 201 : 200);
    }

    public function enable(string $artId)
    {
        return $this->changeStatus($artId, true);
    }

    public function disable(string $artId)
    {
        return $this->changeStatus($artId, false);
    }

    /**
     * @throws \Exception
     */
    public function delete(string $artId)
    {
        $art = $this->checkMonitoredArt($artId);

        $art->delete();

        return $this->responseNoContent();
    }

    private function changeStatus(string $artId, bool $status)
    {
        $art = $this->checkMonitoredArt($artId);

        $status ? $art->activate() : $art->deactivate();

        return $this->responseNoContent();
    }

    /**
     * @throws AuthenticationException
     */
    private function checkMonitoredArt(string $artId): MonitoredArt
    {
        $user = user();

        if (!$user) {
            throw new AuthenticationException();
        }

        /** @var MonitoredArt $art */
        $art = $user->monitoredArts()->withoutGlobalScope('active')->where('art_hwm_id', $artId)->first();

        if (!$art) {
            throw (new ModelNotFoundException())->setModel(MonitoredArt::class, [$artId]);
        }

        return $art;
    }
}
