<?php

declare(strict_types=1);

namespace App\HWM\Entities;

use App\HWM\Entities\Interfaces\HasImage;
use App\HWM\Entities\Interfaces\HasRepository;
use App\HWM\Repositories\SkillRepository;

class Skill extends HwmEntity implements HasImage, HasRepository
{
    public function getImage(bool $realPath = false): string
    {
        $path = 'images/icons/hwm/skills/' . $this->getHwmId() . '.png';

        return !$realPath ? asset($path) : public_path($path);
    }

    public function getRepository(): SkillRepository
    {
        return app(SkillRepository::class);
    }
}
