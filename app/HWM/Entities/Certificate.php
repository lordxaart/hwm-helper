<?php

declare(strict_types=1);

namespace App\HWM\Entities;

use App\HWM\Entities\Interfaces\CanBeLot;
use App\HWM\Enums\EntityType;

class Certificate extends HwmEntity implements CanBeLot
{
    public const MARKET_CATEGORY_ID = 'cert';

    public function getEntityType(): EntityType
    {
        return EntityType::CERTIFICATE;
    }

    public function getMarketCategoryId(): string
    {
        return self::MARKET_CATEGORY_ID;
    }

    public function getImage(bool $realPath = false, string $size = null): string
    {
        return !$realPath ? asset('images/other/house_cert.jpg') : public_path('images/other/house_cert.jpg');
    }
}
