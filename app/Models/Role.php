<?php

declare(strict_types=1);

namespace App\Models;

use Spatie\Permission\Models\Role as SpatieRole;

class Role extends SpatieRole
{
    public const SUPER_ADMIN = 'super-admin';
    public const ADMIN = 'admin';
    public const USER = 'user';

    public static function getBasePermissions(): array
    {
        return [
            self::SUPER_ADMIN => [],
            self::ADMIN => [],
            self::USER => [],
        ];
    }
}
