<div class="form-group">
    <div class="g-recaptcha" data-callback="sumbitRecaptcha" data-error-callback="errorRecaptcha" data-sitekey="{{ config('services.google.recaptcha.site_key') }}"></div>

    @if ($errors->has('g-recaptcha-response'))
        <span class="invalid-feedback d-block" role="alert">
            <strong>{!! $errors->first('g-recaptcha-response') !!}</strong>
        </span>
    @endif
</div>

@push('js')
{{--    <script src="https://www.google.com/recaptcha/api.js" async defer></script>--}}

{{--    <script>--}}
{{--        function sumbitRecaptcha() {--}}
{{--            document.querySelector('button[type=submit]').removeAttribute('disabled');--}}
{{--        }--}}
{{--        function errorRecaptcha() {--}}
{{--            document.querySelector('button[type=submit][disabled]').removeAttribute('disabled');--}}
{{--            document.querySelector('.g-recaptcha').remove();--}}
{{--        }--}}
{{--    </script>--}}
@endpush