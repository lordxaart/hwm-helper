@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            {{ config('app.name') }}
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Subcopy --}}
    @php
    $unsubsribe = __('app.mail.settings_of_subscription', ['link' => '<a href="' . route('profile.index') . '">' . __('app.user.account_settings') . '</a>']);
    $subcopy = $subcopy ?? __('app.mail.regards') . ', ' . config('app.name') . '<br>' . $unsubsribe;
    @endphp
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {!! $subcopy !!}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.name') }}
        @endcomponent
    @endslot
@endcomponent
