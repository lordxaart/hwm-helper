<?php

declare(strict_types=1);

namespace App\Core\ValueObject;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

abstract class EntityCollection extends Collection
{
    public function __construct($items = [])
    {
        foreach ($items as $item) {
            $this->validateItem($item);
        }

        parent::__construct($items);
    }

    /**
     * @param mixed $item
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    abstract protected function validateItem(mixed $item): void;
    
    public function map(callable $callback): Collection
    {
        return new Collection(Arr::map($this->items, $callback));
    }
}