<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V1;

use App\Http\Requests\API\ElementPricesRequest;
use App\Http\Resources\ArtResource;
use App\Http\Resources\ElementPriceResource;
use App\Http\Resources\LotBaseCollection;
use App\Http\Resources\LotResource;
use App\HWM\Entities\MarketLot;
use App\HWM\Enums\EntityType;
use App\HWM\Enums\LotType;
use App\HWM\Helpers\HwmLinkMarket;
use App\Jobs\Lot\LotUpdate;
use App\Models\Art;
use App\Models\ElementPrice;
use App\Models\Lot;
use App\Services\LotFilter\LotAccordChecker;
use App\Services\LotFilter\Repositories\LotFiltersRepository;
use App\Services\Settings\User\GameLinkDecorator;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MarketController extends ApiController
{
    public function elements(ElementPricesRequest $request)
    {
        $elements = ElementPrice::lastPrices(strval($request->query('date', '')))->get();

        return $this->response(ElementPriceResource::collection($elements));
    }

    public function lotsByArt(Request $request)
    {
        $art = Art::findByHwmId(strval($request->query('art_id')));

        if (!$art || !$art->market_category_id) {
            throw new NotFoundHttpException();
        }

        $user = user();

        $lots = hwmClient()
            ->getMarketLots($art->market_category_id, $art->hwm_id, $art->repair)
            ->sortBy('price_per_fight');

        $entities = [];
        foreach ($lots as $lot) {
            $entityId = $lot->entity_type . '_' . $lot->entity_id;
            if (empty($entities[$entityId])) {
                $entities[$entityId] = $lot->entityAsArray();
            }
        }

        $marketLink = HwmLinkMarket::link(
            category: $art->market_category_id,
            art_type: $art->hwm_id,
            type: HwmLinkMarket::TYPE_ONLY_SALE,
        );

        /** @var GameLinkDecorator $gameLinkDecorator */
        $gameLinkDecorator = app(GameLinkDecorator::class);

        if ($user) {
            $marketLink = $gameLinkDecorator->replaceGameLinkForUser($marketLink, $user);
        }

        $collection = new LotBaseCollection($lots->toBase());
        // todo: Перевірити швидкість роботи коли багато лотів
        // resolve filters and set to collection
//        if ($user && in_array('accorded_filters', $collection->includes)) {
//            /** @var LotFiltersRepository $lotFiltersRepository */
//            $lotFiltersRepository = app(LotFiltersRepository::class);
//            $accordedFilters = [];
//            $filters = $lotFiltersRepository->findAllByUserId($user->getId());
//
//            if (!empty($filters)) {
//                /** @var LotAccordChecker $filterChecker */
//                $filterChecker = app(LotAccordChecker::class);
//                foreach ($lots as $lot) {
//                    $accordedFilters[$lot->getHwmId()] = [];
//                    foreach ($filters as $filter) {
//                        if ($filterChecker->isLotAccordedToFilter($lot, $filter)) {
//                            $accordedFilters[$lot->getHwmId()][] = $filter;
//                        }
//                    }
//                }
//            }
//
//            $collection->setAccordedFilters($accordedFilters);
//        }
        $collection->setEntities($entities);

        $collection->additional([
            'meta' => [
                'total' => $lots->count(),
                'art' => new ArtResource($art),
                'marketLink' => $marketLink,
            ]
        ]);

        return $this->response($collection);
    }

    public function updateLot(Request $request)
    {
        $lotId = (int)$request->input('lot_id');

        $this->dispatchSync(new LotUpdate($lotId));

        $lot = Lot::where('lot_id', $lotId)->firstOrFail();

        return $this->response(new LotResource($lot->asLotEntity()));
    }
}
