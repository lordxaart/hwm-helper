<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('lots', function (Blueprint $table) {
            $table->string('crc', 50)->nullable()->after('lot_id');
            $table->unsignedTinyInteger('parsing_status')->default(0)->after('entity_title');
            $table->unsignedTinyInteger('status')->default(0)->after('entity_title');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('lots', function (Blueprint $table) {
            $table->dropColumn('crc');
            $table->dropColumn('parsing_status');
            $table->dropColumn('status');
        });
    }
};
