<?php

declare(strict_types=1);

namespace App\Console\Commands\HWM;

use App\Console\Commands\Command;
use App\Models\Art;

class TranslateArtsToEn extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hwm:art:translate {artId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Translate arts title and description to english';

    public function handle(): void
    {
        $artId = $this->argument('artId');
        $query = Art::query();

        if ($artId !== 'all') {
            $query->where('hwm_id', $artId);
        }

        $query->each(function (Art $art) {
            if ($art->autoTranslate()) {
                $this->log('Complete: ' . $art->getTranslation('title', RU));
            }
        });
    }
}
