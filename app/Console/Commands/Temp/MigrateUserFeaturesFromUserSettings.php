<?php

declare(strict_types=1);

namespace App\Console\Commands\Temp;

use App\Console\Commands\Command;
use App\Models\User;
use App\Services\UserFeatures\Enums\Feature;
use App\Services\UserFeatures\UserFeatureService;

class MigrateUserFeaturesFromUserSettings extends Command
{
    protected $signature = 'tmp:migrate:user_features_from_settings';

    /**
     * @var string
     */
    protected $description = 'Migrate user_settings to user_features';

    public function handle(): void
    {
        User::each(function (User $user) {
            $this->handleUser($user);
        });
    }

    private function handleUser(User $user)
    {
        /** @var UserFeatureService $service */
        $service = app(UserFeatureService::class);
        $mapping = [
            'max_monitored_arts' => Feature::MAX_MONITORED_ARTS,
            'max_monitored_filters' => Feature::MAX_MONITORED_FILTERS,
            'lot_filters_notify_delay' => Feature::LOT_FILTERS_NOTIFY_DELAY,
            'can_edit_game_settings' => Feature::CAN_EDIT_GAME_SETTINGS,
        ];

        foreach ($mapping as $from => $to) {
            $value = $user->settings($from);
            $default = $user->settings->getBase()[$from]['default'];

            if ($value !== $default) {
                $service->setUserFeature($user->getId(), $to, $value);
                $this->info("Set [$user->id] [$to->value => $value]");
            }
        }

        $this->info("Handle user [$user->id]");
    }
}
