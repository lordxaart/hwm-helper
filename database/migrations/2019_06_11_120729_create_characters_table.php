<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharactersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('characters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hwm_id')->unique();
            $table->string('login', config('hwm.rules.max_login_length'))->unique();
            $table->string('password');
            $table->unsignedInteger('user_id')->nullable();
            $table->tinyInteger('auto_work')->default(0); // need for auto work bot
            $table->tinyInteger('level')->nullable();
            $table->bigInteger('experience')->nullable();
            $table->string('status')->nullable();
            $table->json('resources')->nullable();
            $table->json('guilds')->nullable();
            $table->json('fractions')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('characters');
    }
}
