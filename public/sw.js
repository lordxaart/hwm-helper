console.log('Service Worker is working');

self.addEventListener('install', (event) => {
    self.skipWaiting();
});

self.addEventListener('activate', (event) => {
    event.waitUntil(self.clients.claim());
});

self.addEventListener('push', (event) => {
    if (!(self.Notification && self.Notification.permission === 'granted')) {
        return;
    }
    const payload = event.data ? event.data.json() : {};
    console.log('SW NOTIFY', payload);
    event.waitUntil(self.registration.showNotification(payload.title, payload));
});

self.addEventListener('notificationclick', (event) => {
    event.notification.close();

    switch (event.action) {
        case 'open_link':
        default:
            const url = event.notification?.data?.url;
            if (url) {
                event.waitUntil(openUrl(url));
                return;
            }
    }

    // default action
    event.waitUntil(openUrl(self.location.origin + '/calculator'));
});

/**
 * @see https://developer.mozilla.org/en-US/docs/Web/API/Clients/openWindow
 */
const openUrl = async (url) => {
    const clientsArr = await clients.matchAll({type: "window"});

    // If a Window tab matching the targeted URL already exists, focus that;
    const hadWindowToFocus = clientsArr.some((windowClient) =>
        windowClient.url === url
            ? (windowClient.focus(), true)
            : false
    );

    // Otherwise, open a new tab to the applicable URL and focus it.
    if (!hadWindowToFocus) {
        const windowClient = await clients.openWindow(url);
        if (windowClient) {
            windowClient.focus()
        }
    }
}
