<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ValidatorProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Custom rule
        \Validator::extend('social_provider', 'App\Components\Validators\CustomValidator@validateSocialProvider');
        \Validator::replacer('social_provider', 'App\Components\Validators\CustomValidator@replaceSocials');
        \Validator::extend('craft', 'App\Components\Validators\CustomValidator@validateCraftString');
    }
}
