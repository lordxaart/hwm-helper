<?php

namespace Database\Seeders;

use App\HWM\Repositories\FractionRepository;
use App\HWM\Repositories\GuildRepository;
use App\HWM\Repositories\ResourceRepository;
use App\Models\Character;
use App\Models\User;
use Illuminate\Support\Arr;

class CharactersSeeder extends FactorySeeder
{
    public function getCountChunks(): int
    {
        return 10;
    }

    public function initStaticData()
    {
        $this->staticData['password'] = \Hash::make('111111');
        $this->staticData['levels'] = range(1, 23);
        $this->staticData['maxExp'] = 80000000;
        $this->staticData['statuses'] = ['online', 'blocked', 'prison', 'inactive', 'offline', 'undefined'];
        $this->staticData['resources'] = app(ResourceRepository::class)->asAssocArray();
        $this->staticData['fractions'] = app(FractionRepository::class)->asAssocArray();
        $this->staticData['guilds'] = app(GuildRepository::class)->asAssocArray();
        $this->staticData['users'] = User::pluck('id')->toArray();
    }

    public function runChunk(): int
    {
        $count = 100;
        $data = [];

        for ($i = 1; $i <= $count; $i++) {
            $data[] = $this->getCharacter();
        }

        shuffle($data);

        try {
            \DB::table(Character::getModel()->getTable())->insert($data);
        } catch (\Exception $e) {
            $this->command->error('Error during insert ' . $count . ' records');
            return 0;
        }


        return $count;
    }

    private function getCharacter(array $attributes = []): array
    {
        return array_merge([
            'hwm_id' => $this->faker->unique()->numberBetween(99999, 999999999),
            'login' => substr($this->faker->unique()->userName, 0, 18),
            'user_id' => Arr::random($this->staticData['users']),
            'level' => Arr::random($this->staticData['levels']),
            'experience' => mt_rand(0, 9999999999),
            'status' => Arr::random($this->staticData['statuses']),
            'experience' => mt_rand(0, 9999999999),
            'resources' => json_encode(array_map(function ($item) { return mt_rand(0, 1000); }, $this->staticData['resources'])),
            'guilds' => json_encode(array_map(function ($item) { return mt_rand(0, 1000); }, $this->staticData['guilds'])),
            'fractions' => json_encode(array_map(function ($item) { return mt_rand(0, 1000); }, $this->staticData['fractions'])),
            'password' => $this->staticData['password'],
            'created_at' => date(config('app.date_format')),
            'updated_at' => date(config('app.date_format')),
        ], $attributes);
    }
}
