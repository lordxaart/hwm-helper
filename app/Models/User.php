<?php

declare(strict_types=1);

namespace App\Models;

use App\Core\Contracts\Notifable;
use App\Core\Enums\SocialProvider;
use App\Core\ValueObject\UserId;
use App\Services\Notifications\Channels\Webpush\PushSubscriptionModel;
use App\Services\Notifications\Enums\Channel;
use App\Services\Notifications\Enums\NotifyType;
use App\Models\Casts\CastSettings;
use App\Models\Objects\UserSettings;
use App\Notifications\User\ChangeSettings;
use App\Notifications\User\ResetPassword;
use App\Notifications\User\VerifyEmail;
use App\Services\UserFeatures\Enums\Feature;
use App\Services\UserFeatures\UserFeatureService;
use App\Services\UserFeatures\ValueObjects\FeatureList;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Exception;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Contracts\Translation\HasLocalePreference;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class User
 * @package App\Models
 * @property UserSettings $settings
 * @property TelegramAccount | null $telegramAccount
 * @property Carbon email_verified_at
 * @method static notBlocked()
 * @method static role($role)
 */
class User extends Authenticatable implements MustVerifyEmail, HasLocalePreference, Notifable
{
    use CrudTrait;
    use HasApiTokens;
    use HasFactory;
    use HasRoles;
    use Notifiable;
    use SoftDeletes;

    public const ADMIN_ACTION_DELETED = 'deleted';
    public const ADMIN_ACTION_RESTORED = 'restored';
    public const ADMIN_ACTION_BLOCKED = 'blocked';
    public const ADMIN_ACTION_UNBLOCKED = 'unblocked';
    public const ADMIN_ACTION_UNBLOCKING = 'unblocking';
    public const ADMIN_ALL_ACTIONS = [
        self::ADMIN_ACTION_BLOCKED,
        self::ADMIN_ACTION_UNBLOCKED,
        self::ADMIN_ACTION_DELETED,
        self::ADMIN_ACTION_RESTORED,
    ];

    public const TABLE_NAME = 'users';

    /** @var array */
    public static array $createRules = [
        'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        'password' => ['required', 'string', 'min:6', 'confirmed'],
    ];

    protected static array $recordEvents = self::ADMIN_ALL_ACTIONS;

    protected $fillable = ['email', 'password',];

    /** @var string[] */
    protected $hidden = ['password', 'remember_token',];

    /** @var array */
    protected $observables = [self::ADMIN_ACTION_BLOCKED, self::ADMIN_ACTION_UNBLOCKED,];

    /** @var string[] */
    protected $casts = [
        'settings' => CastSettings::class,
        'email_verified_at' => 'datetime',
        'deleted_at' => 'datetime',
        'blocked_at' => 'datetime',
        'blocked_until_to' => 'datetime',
    ];

    protected static ?User $superAdmin = null;

    protected bool $notifyForSuperAdmin = false;

    public function getId(): UserId
    {
        return new UserId($this->id);
    }

    public function getNameAttribute()
    {
        return \Arr::first(explode('@', $this->email));
    }

    public function getCustomNotifyDelayAttribute()
    {
        return $this->settings(UserSettings::CUSTOM_NOTIFY_DELAY);
    }

    public function profiles(): HasMany
    {
        return $this->hasMany(SocialAccount::class);
    }

    public function characters(): HasMany
    {
        return $this->hasMany(Character::class);
    }

    public function monitoredArts(): HasMany
    {
        return $this->hasMany(MonitoredArt::class);
    }

    public function filters(): HasMany
    {
        return $this->hasMany(LotFilter::class);
    }

    public function lotFilters(): HasMany
    {
        return $this->hasMany(LotFilter::class);
    }

    public function notifications(): MorphMany
    {
        return $this->morphMany(Notification::class, 'notifiable')->orderBy('created_at', 'desc');
    }

    public function webPushSubscriptions(): HasMany
    {
        return $this->hasMany(PushSubscriptionModel::class);
    }

    public function telegramAccount(): HasOne
    {
        return $this->hasOne(TelegramAccount::class);
    }

    public function setPasswordAttribute(mixed $value): void
    {
        $this->attributes['password'] = Hash::make(strval($value));
    }

    public function updatePassword(string $password): void
    {
        $this->password = $password;

        $this->save();
    }

    public function routeNotificationForTelegram(): mixed
    {
        return $this->notifyForSuperAdmin
            ? config('services.telegram-bot-api.notify_chat_id')
            : $this->telegramAccount?->chat_id;
    }

    public function routeNotificationForWebPush()
    {
        return $this->webPushSubscriptions;
    }

    public function preferredLocale()
    {
        return $this->settings->get(UserSettings::LANG);
    }

    public function updateApiToken(): void
    {
        $token = $this->createToken('api-token');

        $this->api_token = $token->plainTextToken;
    }

    public function getApiToken(): ?string
    {
        if (!$this->api_token) {
            $this->updateApiToken();
            $this->save();
        }

        return $this->api_token;
    }

    public function isSuperAdmin(): bool
    {
        return $this->hasRole(Role::SUPER_ADMIN);
    }

    public function isAdmin(): bool
    {
        return $this->isSuperAdmin() || $this->hasRole(Role::ADMIN);
    }

    public function scopeHasSocial(Builder $builder, string $social): void
    {
        $builder->whereHas('profiles', function (Builder $builderInner) use ($social) {
            $builderInner->where('provider', $social);
        });
    }

    public function scopeOnlyActiveEmail(Builder $builder): void
    {
        $builder->whereNotNull('email_verified_at');
    }

    /**
     * @param Builder|User $builder
     */
    public function scopeActive(Builder $builder): void
    {
        $builder->notBlocked();
    }

    public function scopeNotBlocked(Builder $builder): void
    {
        $builder->whereNull('blocked_at');
    }

    public function scopeOnlyBlocked(Builder $builder): void
    {
        $builder->whereNotNull('blocked_at');
    }

    public function hasSocialProvider(SocialProvider $provider): bool
    {
        return $this->profiles->where('provider', $provider->value)->count() >= 1;
    }

    public function isDeleted(): bool
    {
        return $this->trashed();
    }

    public function isBlocked(): bool
    {
        return !is_null($this->blocked_at);
    }

    public function blockedUntilTo(): ?Carbon
    {
        if ($this->isBlocked()) {
            return $this->blocked_until_to;
        }

        return null;
    }

    public function fireEvent(string $event, bool $halt = true): mixed
    {
        return $this->fireModelEvent($event, $halt);
    }

    public static function blocked(\Closure $callback): void
    {
        static::registerModelEvent(self::ADMIN_ACTION_BLOCKED, $callback);
    }

    public static function unblocked(\Closure $callback): void
    {
        static::registerModelEvent(self::ADMIN_ACTION_UNBLOCKED, $callback);
    }

    public static function getSuperAdmin(): User
    {
        if (!self::$superAdmin) {
            self::$superAdmin = User::role(Role::SUPER_ADMIN)->first();
        }

        if (!self::$superAdmin) {
            throw new ModelNotFoundException('Not found super admin');
        }

        self::$superAdmin->markedAsSuperAdmin();

        return self::$superAdmin;
    }

    protected function markedAsSuperAdmin(): void
    {
        $this->notifyForSuperAdmin = true;
    }

    public function checkBlockExceeded(): bool
    {
        return $this->blocked_until_to && $this->blocked_until_to->isPast();
    }

    public function canSendTelegramNotify(): bool
    {
        return !!$this->routeNotificationForTelegram();
    }

    public function canSendBrowserNotify(): bool
    {
        return !!$this->routeNotificationForWebPush();
    }

    public function getNotifyChannels(NotifyType $notifyKey, array $onlyChannels = [], bool $withoutSettings = false): array
    {
        $settings = $this->settings->get(UserSettings::NOTIFICATIONS);
        $channelsByKeys = [];
        foreach (Channel::cases() as $channel) {
            $channelsByKeys[$channel->value] = $channel->getLaravelChannelName();
        }
        $availableChannels = [];

        foreach ($settings[NotifyType::DEFAULT->value] as $key => $value) {
            if ($value || $withoutSettings) {
                $availableChannels[] = $channelsByKeys[$key];
            }
        }

        if (count($onlyChannels)) {
            $availableChannels = array_intersect($availableChannels, $onlyChannels);
        }

        if (!$withoutSettings && $notifyKey !== NotifyType::DEFAULT) {
            foreach ($settings[$notifyKey->value] as $key => $value) {
                if (!$value && in_array($channelsByKeys[$key], $availableChannels) !== false) {
                    unset($availableChannels[array_search($channelsByKeys[$key], $availableChannels)]);
                }
            }
        }

        if (in_array(Channel::TELEGRAM->value, $availableChannels) && !$this->canSendTelegramNotify()) {
            unset($availableChannels[array_search(Channel::TELEGRAM->value, $availableChannels)]);
        }

        $browser = $channelsByKeys[Channel::BROWSER->value];
        if (in_array($browser, $availableChannels) && !$this->canSendBrowserNotify()) {
            unset($availableChannels[array_search($browser, $availableChannels)]);
        }

        return array_merge(
            $availableChannels,
            ['database'],
        );
    }

    /**
     * @param string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    /**
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail());
    }

    public function settings(string|array $key = null, mixed $value = null): mixed
    {
        if (is_null($key)) {
            return $this->settings->all();
        }

        if (!$value && !is_array($key)) {
            return $this->settings->get($key);
        }

        $this->settings->set($key, $value);

        $this->updateSettings();

        return true;
    }

    public function settingsAll(): array
    {
        return $this->settings->all();
    }

    public function updateSettings(): void
    {
        if ($this->settings->checkIfHasNewSettings()) {
            $this->update(['settings' => $this->settings]);
        }
    }

    public function getEncryptedEmail(): string
    {
        $hash = Str::uuid()->toString();
        Cache::remember($hash, 60 * 60, function () {
            return $this->getId()->value;
        });

        return $hash;
    }

    public static function getUserByHash(string $value): ?User
    {
        $userId = Cache::get($value);

        return User::query()->where('id', $userId)->first();
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
        ];
    }

    public function features(): FeatureList
    {
        return features()->getUserFeatures($this->getId());
    }
}
