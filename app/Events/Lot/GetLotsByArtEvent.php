<?php

declare(strict_types=1);

namespace App\Events\Lot;

use App\Models\Art;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Support\Collection;

class GetLotsByArtEvent
{
    use Dispatchable;

    public Collection $lots;
    public Art $art;

    public function __construct(Art $art, Collection $lots)
    {
        $this->art = $art;
        $this->lots = $lots;
    }
}
