<?php

declare(strict_types=1);

namespace App\Services\ActivityLogger;

use App\Services\ActivityLogger\ValueObjects\Activity;

interface ActivityLogger
{
    public function log(Activity $activity): void;
}
