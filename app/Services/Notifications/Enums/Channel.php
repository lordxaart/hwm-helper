<?php

declare(strict_types=1);

namespace App\Services\Notifications\Enums;

enum Channel: string
{
    public const DATABASE = 'database';

    case MAIL = 'email';
    case BROWSER = 'browser';
    case TELEGRAM = 'telegram';

    public function getLaravelChannelName(): string
    {
        return match ($this) {
            Channel::MAIL => 'mail',
            /** @see \App\Services\Notifications\Channels\Webpush\WebPushChannel */
            Channel::BROWSER => 'webpush',
            /** @see \App\Services\Notifications\Channels\Telegram\TelegramChannel */
            Channel::TELEGRAM => 'telegram',
        };
    }

    public function defaultAvailability(): bool
    {
        return match ($this) {
            Channel::MAIL => false,
            default => true,
        };
    }
}
