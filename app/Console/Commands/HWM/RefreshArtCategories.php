<?php

declare(strict_types=1);

namespace App\Console\Commands\HWM;

use App\Console\Commands\Command;
use App\Models\Art;

class RefreshArtCategories extends Command
{
    /** @var string  */
    protected $signature = 'hwm:art:update:categories_from_market';

    /** @var string  */
    protected $description = 'Refresh art categories';

    public function handle(): void
    {
        Art::all()->each(function (Art $art) {
            $art->refreshArtCategory();
        });
    }
}
