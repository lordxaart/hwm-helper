<div class="form-group">
    <label>User</label>
    <select class="select2 form-control custom-select" name="user_id">
        @foreach(\App\Models\User::all() as $user)
            <option value="{{ $user->id }}">{{ $user->email }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label>Subscription</label>
    <select class="form-control custom-select subscription-select" name="plan_id">
        @foreach(\App\Http\Controllers\Admin\Models\SubscriptionModel::all() as $s)
            <optgroup label="{{ $s->name }}">
                @foreach($s->plans as $plan)
                    <option value="{{ $plan->id }}">{{ $plan->name }}</option>
                @endforeach
            </optgroup>
        @endforeach
    </select>
    <div class="help-block small plan-info"></div>
</div>

<script src="https://cdn.jsdelivr.net/npm/vue@2.7.8/dist/vue.js"></script>
<script>
    var app = new Vue({
        el: '#vue-app',
        data: {
            isNew: true,
        }
    })
</script>
