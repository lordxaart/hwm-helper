<?php

namespace Database\Seeders;

use App\HWM\Repositories\ArtRepository;
use App\Models\MonitoredArt;
use App\Models\Objects\FilterTerm;
use App\Models\User;
use Illuminate\Support\Arr;

class MonitoredArtsAndFiltersSeeder extends FactorySeeder
{
    public int $countArts = 1000;
    public int $countFilters = 2000;

    public function initStaticData()
    {
        $this->staticData['arts'] = app(ArtRepository::class)->asArrayKeys();
        $this->staticData['users'] = User::active()->inRandomOrder()->pluck('id')->toArray();
    }

    public function runChunk(): int
    {
        $this->seedArts();


        return $this->countArts;
    }

    public function seedArts(): void
    {
        $arts = [];

        for ($i = 1; $i <= $this->countArts; $i++) {
            $arts[] = [
                'user_id' => Arr::random($this->staticData['users']),
                'art_hwm_id' => Arr::random($this->staticData['arts']),
                'is_active' => mt_rand(0, 10) ? 1 : 0,
                'created_at' => date(config('app.date_format')),
                'updated_at' => date(config('app.date_format')),
            ];
        }

        shuffle($arts);

        \DB::table(MonitoredArt::getModel()->getTable())->insert($arts);
    }

    public function seedFilters(): void
    {
        // todo
    }

    private function generateTerms(): string
    {
        $fields = [
            FilterTerm::FIELD_B_STRENGTH,
            FilterTerm::FIELD_C_STRENGTH,
            FilterTerm::FIELD_PPF,
            FilterTerm::FIELD_PRICE,
        ];
        $operators = FilterTerm::operators();

        return Arr::random($fields) . ' ' . Arr::random($operators) . ' ' . mt_rand(50, 100000);
    }
}
