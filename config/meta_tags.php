<?php

use Butschster\Head\MetaTags\Viewport;

return [
    /*
     * Meta title section
     */
    'title' => [
        'default' => config('app.name'),
        'separator' => '|',
        'max_length' => 255,
    ],


    /*
     * Meta description section
     */
    'description' => [
        'default' => null,
        'max_length' => 255,
    ],


    /*
     * Meta keywords section
     */
    'keywords' => [
        'default' => null,
        'max_length' => 255
    ],

    /*
     * Default packages
     *
     * Packages, that should be included everywhere
     */
    'packages' => [
        //
    ],

    'charset' => 'utf-8',
    'robots' => isProdEnv() ? 'index, follow' : 'noindex, nofollow',
    'viewport' => Viewport::RESPONSIVE,
    'csrf_token' => true,

    'favicons' => [
        ['href' => '/favicon.ico'],
        ['href' => '/images/favicons/apple-icon-57x57.png', 'sizes' => '57x57', 'rel' => 'apple-touch-icon'],
        ['href' => '/images/favicons/apple-icon-60x60.png', 'sizes' => '60x60', 'rel' => 'apple-touch-icon'],
        ['href' => '/images/favicons/apple-icon-72x72.png', 'sizes' => '72x72', 'rel' => 'apple-touch-icon'],
        ['href' => '/images/favicons/apple-icon-76x76.png', 'sizes' => '76x76', 'rel' => 'apple-touch-icon'],
        ['href' => '/images/favicons/apple-icon-114x114.png', 'sizes' => '114x114', 'rel' => 'apple-touch-icon'],
        ['href' => '/images/favicons/apple-icon-120x120.png', 'sizes' => '120x120', 'rel' => 'apple-touch-icon'],
        ['href' => '/images/favicons/apple-icon-144x144.png', 'sizes' => '144x144', 'rel' => 'apple-touch-icon'],
        ['href' => '/images/favicons/apple-icon-152x152.png', 'sizes' => '152x152', 'rel' => 'apple-touch-icon'],
        ['href' => '/images/favicons/apple-icon-180x180.png', 'sizes' => '180x180', 'rel' => 'apple-touch-icon'],
        ['href' => '/images/favicons/android-icon-192x192.png', 'sizes' => '192x192', 'rel' => 'apple-touch-icon'],
        ['href' => '/images/favicons/favicon-32x32.png', 'sizes' => '32x32', 'rel' => 'apple-touch-icon'],
        ['href' => '/images/favicons/favicon-96x96.png', 'sizes' => '96x96', 'rel' => 'apple-touch-icon'],
        ['href' => '/images/favicons/favicon-16x16.png', 'sizes' => '16x16', 'rel' => 'apple-touch-icon'],
    ],

    'og' => [
        'type' => 'website',
        'site_name' => config('app.name'),
    ],
    'twitter' => [
        'card' => 'summary',
        'domain' => env('APP_DOMAIN'),
    ],
];
