<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lot_filter_notifications', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->unsignedInteger('user_id');
            $table->string('channel');
            $table->timestamp('created_at', 6);
            $table->unsignedInteger('created_diff_seconds');
            $table->unsignedInteger('lot_id');
            $table->json('data');

            $table->index('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lot_notifications');
    }
};
