<?php

declare(strict_types=1);

namespace App\Events\User;

use App\Models\TelegramAccount;
use App\Models\User;
use Illuminate\Foundation\Bus\Dispatchable;

class DisconnectedTelegramEvent
{
    use Dispatchable;

    public User $user;
    public TelegramAccount $telegramAccount;

    public function __construct(User $user, TelegramAccount $telegramAccount)
    {
        $this->user = $user;
        $this->telegramAccount = $telegramAccount;
    }
}
