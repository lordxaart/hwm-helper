<?php

declare(strict_types=1);

namespace App\Notifications\User;

use App\Core\Contracts\Notifable;
use App\Services\Notifications\Channels\Telegram\Entities\TelegramMessage;
use App\Services\Notifications\Enums\Channel;
use Illuminate\Notifications\Notification;

class PublicMessage extends Notification
{
    public function __construct(public string $message)
    {
    }

    public function via()
    {
        return [
            Channel::TELEGRAM->getLaravelChannelName(),
        ];
    }

    public function toTelegram(Notifable $notifable): TelegramMessage
    {
        return new TelegramMessage($this->message);
    }
}
