<script>
    {!! file_get_contents(resource_path('assets/js/other/bootstrap_app.js')) !!}

    (() => {
        const MIX_CSS = '{{newMix('css/app.css')}}';
        const MIX_MANIFEST = '{{newMix('js/app/manifest.js')}}';
        const MIX_VENDOR = '{{newMix('js/app/vendor.js')}}';
        const MIX_APP = '{{newMix('js/app//js/app.js')}}';
        const HIDE_PAGE_LOADER = {{ isset($js) && $js === false ? 'true' : 'false' }};

        initLoadedResources(MIX_CSS, MIX_MANIFEST, MIX_VENDOR, MIX_APP, HIDE_PAGE_LOADER);
    })();

    // define global variables;
    window.APP_CONFIG = JSON.parse('{!! json_encode(\App\Services\Settings\GeneralSettings::getFrontAppConfig() ) !!}');
    window.APP_CONFIG.is_crawler = {{ Crawler::isCrawler() ? 'true' : 'false' }}
    window.CSRF_TOKEN = document.querySelector('meta[name=csrf-token]').content;
    @if(user())
    window.USER = JSON.parse('{!! json_encode([
        'id' => user()->id,
        'email' => user()->email,
        'email_verified_at' => user()->email_verified_at?->format(config('app.date_format')),
    ]) !!}');
    @else
    window.USER = null;
    @endif
</script>
