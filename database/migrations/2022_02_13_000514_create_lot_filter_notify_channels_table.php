<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLotFilterNotifyChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lot_filter_notify_channels', function (Blueprint $table) {
            $table->unsignedInteger('lot_filter_id');
            $table->string('notify_channel');
            $table->boolean('is_available');

            $table->primary(['lot_filter_id', 'notify_channel']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lot_filter_notify_channels');
    }
}
