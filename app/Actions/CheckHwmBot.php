<?php

declare(strict_types=1);

namespace App\Actions;

use App\HWM\HwmClient;
use App\HWM\Request\HwmRequest;
use App\Models\HwmBotAccount;
use App\Models\Lot;
use Illuminate\Console\Command;
use Lorisleiva\Actions\Concerns\AsAction;

/**
 * @property string $art_id
 */
class CheckHwmBot
{
    use AsAction;

    public string $commandSignature = 'hwm:bot:check {login : Account login}';
    public string $commandDescription = 'Check hwm bot';

    public function asCommand(Command $command): int
    {
        $login = $command->argument('login');
        $info = $this->handle($login);
        $command->info("Checked [$login] Bot");
        $command->table(array_keys($info), [array_values($info)]);

        return 0;
    }

    public function handle(string $login, string $password = null): array
    {
        /** @var HwmBotAccount $botAccount */
        $botAccount = HwmBotAccount::query()->withoutGlobalScope('active')->where('login', $login)->firstOrFail();

        $result = [
            'exist' => true,
            'delay' => null,
        ];
        $client = new HwmClient($botAccount, app(HwmRequest::class));

        /** @var Lot $lastLot */
        $lastLot = Lot::query()->orderBy('lot_id', 'DESC')->first();
        $diff = now()->diffInSeconds($lastLot->started_at);
        $result['delay'] = $diff;

        try {
            $clientLot = $client->getLot($lastLot->lot_id, $lastLot->crc);
            if (!$clientLot) {
                $result['error']  = "Cant find lot {$lastLot->lot_id}";
            }
        } catch (\Throwable $e) {

            $result['error']  = $e->getMessage();
        }

        return $result;
    }
}
