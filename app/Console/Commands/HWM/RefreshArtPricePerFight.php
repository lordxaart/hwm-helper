<?php

declare(strict_types=1);

namespace App\Console\Commands\HWM;

use App\Console\Commands\Command;
use App\Models\Art;

class RefreshArtPricePerFight extends Command
{
    /** @var string  */
    protected $signature = 'hwm:art:update:ppf';

    /** @var string  */
    protected $description = 'Refresh art price per fight';

    public function handle(): void
    {
        Art::all()->each(function (Art $art) {
            $art->refreshPPF();
        });
    }
}
