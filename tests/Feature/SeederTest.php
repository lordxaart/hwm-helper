<?php

namespace Tests\Feature;

use App\HWM\Helpers\HwmHelper;
use App\HWM\Repositories\ArtCategoryRepository;
use App\HWM\Repositories\ArtRepository;
use App\HWM\Repositories\ArtSetRepository;
use App\HWM\Repositories\MarketCategoryRepository;
use App\Models\SuperAdmin;
use App\Models\Art;
use App\Models\ArtCategory;
use App\Models\MarketCategory;
use App\Models\ArtSet;
use App\Models\Character;
use App\Models\Role;
use App\Models\User;
use Tests\TestCase;

class SeederTest extends TestCase
{
    /**
     * @test
     */
    public function check_super_admin_exists()
    {
        $this->assertNotNull(User::getSuperAdmin());
        $this->assertEquals(config('auth.super-admin.email'), User::getSuperAdmin()->email);
    }

    /**
     * @test
     */
    public function check_roles_exists()
    {
        $roles = Role::pluck('name')->toArray();
        $needRoles = array_keys(Role::getBasePermissions());

        $this->assertArraySimilar($needRoles, $roles);
    }

    /**
     * @test
     */
    public function check_art_categories()
    {
        $categories = app(ArtCategoryRepository::class)->all()->pluck('hwm_id')->toArray();
        $needCategories = array_map(function ($item) {
            return $item['hwm_id'];
        }, HwmHelper::getJson('categories', false));

        $this->assertArraySimilar($needCategories, $categories);
    }

    /**
     * @test
     */
    public function check_art_market_categories()
    {
        $categories = app(MarketCategoryRepository::class)->all()->pluck('hwm_id')->toArray();
        $needCategories = array_map(function ($item) {
            return $item['hwm_id'];
        }, HwmHelper::getJson('market_categories', false));

        $this->assertArraySimilar($needCategories, $categories);
    }

    /**
     * @test
     */
    public function check_art_sets()
    {
        $categories = app(ArtSetRepository::class)->all()->pluck('hwm_id')->toArray();
        $needCategories = array_map(function ($item) {
            return $item['hwm_id'];
        }, HwmHelper::getJson('sets', false));

        $this->assertArraySimilar($needCategories, $categories);
    }
}
