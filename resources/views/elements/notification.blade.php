{{-- send error, success,  info, warning  variable--}}
@php
    $alerts = [];

    if (session('success')) {
        $alerts[] = [
            'type' => 'success',
            'message' => session('success'),
        ];
    }
    if (session('info')) {
        $alerts[] = [
            'type' => 'info',
            'message' => session('info'),
        ];
    }
    if (session('warning')) {
        $alerts[] = [
            'type' => 'warn',
            'message' => session('warning'),
        ];
    }
    if (session('error')) {
        $alerts[] = [
            'type' => 'danger',
            'message' => session('error'),
        ];
    }
    if (isset($errors) && $errors->count()) {
        if ($errors instanceof \Illuminate\Support\ViewErrorBag) {
            $errors = $errors->all();
        }
        foreach($errors as $error) {
            $alerts[] = [
                'type' => 'danger',
                'message' => $error,
            ];
        }
    }
@endphp

@if(count($alerts))
    @foreach($alerts as $alert)
        <b-alert
            class="last-mb-0"
            variant="{{ $alert['type'] }}"
            show
            fade
            dismissible
        >
            @if(is_array($alert['message']))
                @foreach($alert['message'] as $mes)
                    <p>{!! $mes !!}</p>
                @endforeach
            @else
                <p>{!! $alert['message'] !!}</p>
            @endif
        </b-alert>
    @endforeach
@endif
