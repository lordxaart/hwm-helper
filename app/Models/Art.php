<?php

declare(strict_types=1);

namespace App\Models;

use App\Http\Resources\ArtResource;
use App\HWM\Helpers\HwmHelper;
use App\HWM\Repositories\ArtRepository;
use App\Models\Objects\ArtImage;
use App\HWM\Helpers\HwmLink;
use App\HWM\Entities\Interfaces\HwmEntityInterface;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;
use Spatie\Translatable\HasTranslations;
use App\Models\Traits\HwmEntity as HwmEntityTrait;

/**
 * @property string $title
 * @property string $description
 */
class Art extends Model implements HwmEntityInterface
{
    use HwmEntityTrait;
    use CrudTrait;
    use HasTranslations;

    public const ARTS_CACHE_KEY = 'art_list';

    public array $translatable = ['title', 'description'];

    protected $table = 'arts';

    /** @var array  */
    protected $fillable = [
        'hwm_id',
        'title',
        'original_image',
        'need_level',
        'strength',
        'oa',
        'repair',
        'description',
        'modifiers',
        'extra_modifiers',
        'from_shop',
        'as_from_shop',
        'shop_price',
        'market_category_id',
        'set_id',
        'category_id',
    ];

    /** @var array  */
    protected $casts = [
        'modifiers' => 'array',
        'extra_modifiers' => 'array',
        'as_from_shop' => 'bool',
        'from_shop' => 'bool',
    ];

    protected array $defaultOrder = ['title'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Art $art) {
            $art->refreshPPF(false);
            $art->refreshArtCategory(false);
        });

        static::created(function (Art $art) {
            ArtRepository::refreshCache();
        });

        static::updating(function (Art $art) {
            $art->refreshPPF(false);
            $art->refreshArtCategory(false);
        });

        static::updated(function (Art $art) {
            ArtRepository::refreshCache();
        });

        static::deleted(function (Art $art) {
            ArtRepository::refreshCache();
        });
    }

    public function getHwmId(): string
    {
        return $this->hwm_id;
    }

    public function getTitle(): string
    {
        return (string)$this->title;
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(ArtCategory::class, 'category_id', 'hwm_id');
    }

    public function marketCategory(): BelongsTo
    {
        return $this->belongsTo(MarketCategory::class, 'market_category_id', 'hwm_id');
    }

    public function set(): BelongsTo
    {
        return $this->belongsTo(ArtSet::class, 'set_id', 'hwm_id');
    }

    public function monitoredArts(): HasMany
    {
        return $this->hasMany(MonitoredArt::class, 'art_hwm_id', 'hwm_id');
    }

    /**
     * @return ArtImage[]
     */
    public function getImagesAttribute(): array
    {
        return [
            'default' => new ArtImage($this, 100),
            'small' => new ArtImage($this, 30, '_small'),
        ];
    }

    public function getImageAttribute(): ?string
    {
        return isset($this->images['default']) ? $this->images['default']->getUrl() : null;
    }

    public function setSetIdAttribute(string $setId = null): void
    {
        // Check set exists
        if ($setId && !ArtSet::findByHwmId($setId)) {
            \Log::error("Not found set $setId in DB for $this->hwm_id");
            $this->attributes['set_id'] = null;
        } else {
            $this->attributes['set_id'] = $setId;
        }
    }

    public function scopeSearch(Builder $builder, string $q): void
    {
        if (!$q) {
            return;
        }

        $qLike = "%$q%";
        $builder->where('title', 'LIKE', $qLike)->orWhere('hwm_id', 'LIKE', $qLike);
    }

    public function getLink(): string
    {
        return HwmLink::get(HwmLink::ART_INFO, ['id' => $this->getHwmId()]);
    }

    public function getMarketLink(): string
    {
        return HwmLink::get(HwmLink::MARKET, [
            'cat' => $this->market_category_id,
            'art_type' => $this->hwm_id,
            'sort' => 0, // 0 - cheapest
            'sbn' => 1, // Only sold
            'sau' => 0, // Only bargaining
        ]);
    }

    public function refreshPPF(bool $withSave = true): void
    {
        if (!$this->shop_price || !$this->strength || !$this->repair) {
            return;
        }

        $this->price_per_fight = HwmHelper::pricePerFight(
            $this->shop_price,
            $this->repair,
            $this->strength,
            $this->strength
        );

        if ($withSave) {
            $this->save();
        }
    }

    public function refreshArtCategory(bool $withSave = true): void
    {
        if (!$this->market_category_id) {
            if (str_contains($this->getTitle(), 'Медаль')) {
                $this->market_category_id = 'medals';

                $this->save();
            }
        }

        if (!$this->category_id && $this->market_category_id) {
            if (ArtCategory::all()->firstWhere('hwm_id', $this->market_category_id)) {
                $this->category_id = $this->market_category_id;
            } elseif ($this->market_category_id == 'medals') {
                $this->category_id = 'necklace';
            }

            if ($withSave) {
                $this->save();
            }
        }
    }

    public function imagesExist(): bool
    {
        foreach ($this->images as $image) {
            if (!$image->exists()) {
                return false;
            }
        }

        return true;
    }

    public function checkOriginalImage(): void
    {
        $originalImageUrl = $this->original_image;

        // check extension
        if (str_contains($originalImageUrl, '.jpg')) {
            $originalImageUrlPng = str_replace('.jpg', '.png', $originalImageUrl);
            if (checkRemoteImageExists($originalImageUrlPng)) {
                $originalImageUrl = $originalImageUrlPng;
            }
        }

        // check size
        if (!str_contains($originalImageUrl, '_b.')) {
            $originalImageUrlBig = Str::replaceLast('.', '_b.', $originalImageUrl);
            if (checkRemoteImageExists($originalImageUrlBig)) {
                $originalImageUrl = $originalImageUrlBig;
            }
        }

        // If new url is not equal with previous then update this value
        if ($this->original_image !== $originalImageUrl) {
            $this->original_image = $originalImageUrl;
            $this->save();
        }
    }

    public function handleImages(bool $force = false): void
    {
        foreach ($this->images as $image) {
            if ($force || !$image->exists()) {
                $image->handleImage();
            }
        }
    }

    public function getImageUrl(): string
    {
        return $this->original_image;
    }

    public function transform(): array
    {
        return (new ArtResource($this))->toArray(request());
    }

    protected function asJson($value): bool|string
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }

    public function getSmallImage(): ArtImage
    {
        return $this->images['small'];
    }

    public function getDefaultImage(): ArtImage
    {
        return $this->images['default'];
    }

    public function asArtEntity(): \App\HWM\Entities\Art
    {
        $data = $this->toArray();
        $data['title'] = $this->title;
        $data['description'] = $this->description;

        return new \App\HWM\Entities\Art($this->hwm_id, $data);
    }

    /**
     * @throws \App\HWM\Exceptions\HwmException
     */
    public function autoTranslate(): bool
    {
        $hwm = hwmClient();

        foreach ([EN, RU] as $locale) {
            $art = null;

            foreach (['title'] as $item) {
                $text = $this->getTranslation($item, $locale, false);
                if (!$text) {
                    $art = $art ?: $hwm->setLocale($locale)->getArtInfo($this->hwm_id);
                    $this->setTranslation($item, $locale, $art->{$item});
                }
            }
        }

        if ($this->isDirty(['title'])) {
            $saved = $this->save();
            ArtRepository::refreshCache();

            return $saved;
        }


        return false;
    }
}
