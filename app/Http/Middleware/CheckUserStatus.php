<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Actions\User\UnblockUser;
use App\Exceptions\User\UserWasBlocked;
use App\Exceptions\User\UserWasDeleted;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckUserStatus
{
    /**
     * @throws UserWasBlocked
     * @throws UserWasDeleted
     */
    public function handle(Request $request, Closure $next): mixed
    {
        if (Auth::check()) {
            /** @var User $user */
            $user = Auth::user();

            if ($user->checkBlockExceeded()) {
                UnblockUser::run($user);
            }

            if ($user->isBlocked()) {
                \Auth::logout();

                throw new UserWasBlocked($user);
            }
            if ($user->isDeleted()) {
                \Auth::logout();

                throw new UserWasDeleted($user);
            }
        }
        return $next($request);
    }
}
