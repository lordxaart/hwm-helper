<?php

namespace Tests\Traits;

use App\Models\User;

trait WithStudUser
{
    /**
     * @var User
     */
    protected $user;

    public function createStubUser(string $role = null)
    {
        $this->user = User::factory()->create();

        if ($role) {
            $this->user->assignRole($role);
        }

        return $this->user;
    }

    public function deleteStubUser()
    {
        $this->user->forceDelete();
    }
}
