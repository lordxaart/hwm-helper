<?php

declare(strict_types=1);

namespace App\HWM;

use App\HWM\Entities\Art;
use App\HWM\Entities\Character;
use App\HWM\Entities\Collections\LotCollection;
use App\HWM\Entities\Lot;
use App\HWM\Entities\MarketLot;
use App\HWM\Entities\Set;
use App\HWM\Exceptions\HwmException;
use App\HWM\Exceptions\ParserError;
use App\HWM\Helpers\HwmLink;
use App\HWM\HtmlParsers\HtmlNode;
use App\HWM\HtmlParsers\Interfaces\HtmlNodeInterface;
use App\HWM\NewParsers\Pages\ArtParser;
use App\HWM\NewParsers\Pages\SetsParser;
use App\HWM\Parsers\HwmParser;
use App\HWM\Repositories\ArtRepository;
use App\Models\HwmBotAccount;
use Illuminate\Support\Collection;
use App\HWM\Request\HwmRequest;

class HwmClient
{
    protected HwmRequest $request;

    /**
     * @throws Exceptions\RequestErrors\InavailableProfile
     * @throws HwmException
     * @throws Exceptions\RequestErrors\LoginException
     * @throws Exceptions\RequestErrors\LoginDuringAnotherScriptLogin
     */
    public function __construct(HwmBotAccount $botAccount = null, HwmRequest $request)
    {
        $this->request = $request;

        if ($botAccount) {
            $this->setCredentials($botAccount);
        }
    }

    /**
     * @throws Exceptions\RequestErrors\InavailableProfile
     * @throws HwmException
     * @throws Exceptions\RequestErrors\LoginException
     * @throws Exceptions\RequestErrors\LoginDuringAnotherScriptLogin
     */
    public function setCredentials(HwmBotAccount $botAccount): self
    {
        $this->request->setCredentials($botAccount);

        return $this;
    }

    public function setLocale(string $locale): self
    {
        $this->request->setLocale($locale);

        return $this;
    }

    public function getLocale(): string
    {
        return $this->request->getLocale();
    }

    /**
     * @throws HwmException|NewParsers\ParserException
     */
    public function getArtInfo(string $artId): Art
    {
        $this->request->request('get', HwmLink::ART_INFO, ['id' => $artId]);

        $data = (new ArtParser(
            $this->getHtmlNodeFromString(
                $this->request->getContent() ?: '',
            ),
            $artId,
        ))->handle();

        return new Art($artId, $data);
    }

    public function getMarketLots(
        string $category,
        string $entityId = null,
        int $repair = null,
        bool $withPPf = true,
        int $sort = 0,
        int $resultLimit = null,
    ): LotCollection {
        $this->request
            ->setResponsePostHandleCallback(fn($content) => HwmParser::handleLongMarkerResponse($content))
            ->request('get', HwmLink::MARKET, [
            'art_type' => $entityId, // art hwm id
            'cat' => $category, // art market category
            'sort' => $sort, // 0 - cheapest
            'sbn' => 1, // Only sold
            'sau' => 0, // Only bargaining
            // 'snew' => 1, // Only whole
        ]);
        $this->request->setResponsePostHandleCallback(null);
        $lots = $this->getParser()->getMarketLots();
        if ($resultLimit) {
            $lots = array_slice($lots, 0, $resultLimit);
        }
        $arrayOfMarketLots = array_map(function (array $item) use ($repair, $withPPf) {
            $lot = new MarketLot($item);
            if ($repair && $withPPf) {
                $lot->setPPF($repair);
            }
            return $lot;
        }, $lots);

        return new LotCollection($arrayOfMarketLots);
    }

    /**
     * @throws HwmException
     */
    public function getListArtsFromMarket(): Collection
    {
        $this->request->request('get', HwmLink::MARKET);

        return collect($this->getParser()->getListArtsFromMarket());
    }

    public function getSets(): Collection
    {
        $this->request->request('get', HwmLink::SETS);

        $data = (new SetsParser(
            $this->getHtmlNodeFromString(
                $this->request->getContent() ?: '',
            ),
        ))
            ->handle();

        return collect($data)
            ->map(function (array $item) {
                return new Set($item['hwm_id'], $item);
            });
    }

    /**
     * @throws HwmException
     */
    public function getLot(int $lotId, string $crc = null, bool $withPPF = true): Lot
    {
        $this->request->request('get', HwmLink::LOT_INFO, ['id' => $lotId, 'crc' => $crc]);
        $lot = new Lot($this->getParser()->getLotInfo($lotId));
        $lot->crc = $crc;
        if ($withPPF) {
            /** @var ArtRepository $repository */
            $repository = app(ArtRepository::class);
            $art = $repository->findByHwmId($lot->entity_id);
            if ($art && $art->repair) {
                $lot->setPPF($art->repair);
            }
        }

        if ($lot->started_at->addSecond()->diffInDays($lot->ended_at) >= 3) {
            \Log::error('[Lot Parser] Diff between started_at and ended_at more then 3 days', [
                'lot_id' => $lot->lot_id,
                'started_at' => $lot->started_at->format(DATE_FORMAT),
                'ended_at' => $lot->ended_at->format(DATE_FORMAT),
            ]);
        }

        return $lot;
    }

    /**
     * @throws HwmException
     */
    public function getCharacterId(): int
    {
        if (!$this->getParser()->getContent()) {
            $this->request->request('get', HwmLink::HOME);
        }

        return $this->getParser()->getCharacterIdFromHomePage();
    }

    /**
     * @throws HwmException
     */
    public function getCharacterInfo(int $hwmId): Character
    {
        try {
            $cacheKey = 'hwm:character:id:' . $hwmId;
            if (\Cache::has($cacheKey)) {
                $character = \Cache::get($cacheKey);
                if (is_array($character))
                    return new Character((string)$character['hwm_id'], $character);
            }
        } catch (\Throwable) {
            \Cache::delete($cacheKey);
        }

        $this->request->request('get', HwmLink::CHARACTER_INFO, ['id' => $hwmId]);
        try {
            $character = $this->getParser()->getCharacterInfo();
        } catch (ParserError $e) {
            if ($this->getParser()->searchCharacterNotFound()) {
                throw new ParserError(trans('errors.hwm.character_not_found', ['character' => $hwmId]));
            }
            throw $e;
        }

        \Cache::set($cacheKey, $character, 60*60*24*30); // 1 month
        return new Character((string)$character['hwm_id'], $character);
    }

    public function getCharacterByLogin(string $login): Character
    {
        try {
            $cacheKey = 'hwm:character:login:' . $login;
            if (\Cache::has($cacheKey)) {
                $data = \Cache::get($cacheKey);
                if (is_array($data))
                    return new Character((string)$data['hwm_id'], $data);
            }
        } catch (\Throwable) {
            \Cache::delete($cacheKey);
        }

        $this->request->request('get', HwmLink::CHARACTER_INFO, ['nick' => mb_convert_encoding($login, 'windows-1251')]);
        try {
            $character = $this->getParser()->getCharacterInfo();
        } catch (ParserError $e) {
            if ($this->getParser()->searchCharacterNotFound()) {
                throw new ParserError(trans('errors.hwm.character_not_found', ['character' => $login]));
            }
            throw $e;
        }

        \Cache::set($cacheKey, $character, 60*60*24*30); // 1 month
        return new Character(strval($character['hwm_id']), $character);
    }

    public function getParser(): HwmParser
    {
        return $this->request->getParser();
    }

    private function getHtmlNodeFromString(string $html): HtmlNodeInterface
    {
        return new HtmlNode($html);
    }
}
