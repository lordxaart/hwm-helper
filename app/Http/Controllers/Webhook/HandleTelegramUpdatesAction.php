<?php

declare(strict_types=1);

namespace App\Http\Controllers\Webhook;

use App\Http\Controllers\Controller;
use App\Services\Telegram\TelegramApi;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class HandleTelegramUpdatesAction extends Controller
{
    public function handle(Request $request, string $token): JsonResponse
    {
        Log::info('TELEGRAM', $request->all());
        if ($token !== TelegramApi::getWebhookToken()) {
            return response()->json(false, 404);
        }

        app(TelegramApi::class)->handleUpdate($request->all());

        return response()->json(true);
    }
}
