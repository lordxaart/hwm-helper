<?php

declare(strict_types=1);

namespace App\Services\UserFeatures\ValueObjects;

use App\Services\UserFeatures\Enums\FeatureType;

readonly class FeatureValue
{
    public string $key;
    public string $type;
    public mixed $default;
    public mixed $value;
    public ?string $entity;

    public function __construct(
        mixed $key,
        string $type,
        mixed $default,
        mixed $value,
        ?string $entity = null,
    ) {
        $this->key = $key;
        $this->type = $type;
        $this->default = $default;
        $this->entity = $entity;

        if (!is_null($value)) {
            $this->value = self::convertValueToType(
                $value,
                $this->getType(),
            );
        } else {
            $this->value = $value;
        }
    }

    /** Return value or default value */
    public function getValue(): mixed
    {
        return !$this->isDefault() ? $this->value : $this->default;
    }

    public function getType(): FeatureType
    {
        return FeatureType::from($this->type);
    }

    public function isDefault(): bool
    {
        return is_null($this->value);
    }

    public static function convertValueToType(mixed $value, FeatureType $type): mixed
    {
        return match ($type) {
            FeatureType::Bool => boolval($value),
            FeatureType::Number => intval($value),
            default => $value,
        };
    }
}
