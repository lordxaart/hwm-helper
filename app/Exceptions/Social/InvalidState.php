<?php

declare(strict_types=1);

namespace App\Exceptions\Social;

use App\Exceptions\ClientError;

class InvalidState extends ClientError
{
    public function __construct(string $socialDriver)
    {
        $code = ClientError::SOCIAL_INVALID_STATE;
        $message = trans('errors.invalid_state') . '. ' . trans('errors.cant_connect_with_driver', ['driver' => $socialDriver]);

        parent::__construct($code, $message);
    }
}
