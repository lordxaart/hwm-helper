<?php

declare(strict_types=1);

namespace App\Services\Subscriptions\Entities;

use App\Core\ValueObject\UserId;
use DateTimeImmutable;
use Illuminate\Contracts\Support\Arrayable;

class UserSubscription implements Arrayable
{
    public function __construct(
        public readonly int $id,
        public readonly UserId $user_id,
        public readonly Sub $subscription,
        public readonly Plan $plan,
        public readonly DateTimeImmutable $started_at,
        public readonly DateTimeImmutable $ended_at,
        public readonly ?DateTimeImmutable $trial_ended_at
    ) {
    }

    public function isActive(): bool
    {
        return $this->ended_at->getTimestamp() >= time();
    }

    public function isTrialActive(): bool
    {
        return $this->trial_ended_at && $this->trial_ended_at->getTimestamp() >= time();
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id->value,
            'subscription' => $this->subscription->toArray(),
            'plan' => $this->plan->toArray(),
            'started_at' => $this->started_at->format(DATE_FORMAT),
            'ended_at' => $this->ended_at->format(DATE_FORMAT),
            'trial_ended_at' => $this->trial_ended_at?->format(DATE_FORMAT),
            'is_active' => $this->isActive(),
        ];
    }
}
