<?php

declare(strict_types=1);

namespace App\Models;

use App\Components\LotCache;
use App\HWM\Entities\Interfaces\HwmEntityInterface;
use App\HWM\Entities\Lot as LotEntity;
use App\HWM\Entities\MarketLot;
use App\HWM\Enums\EntityType;
use App\HWM\Enums\LotType;
use App\HWM\Helpers\HwmHelper;
use App\Services\LotFilter\Repositories\LotFiltersRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Lot
 * @package App\Models
 * @property string|null $started_at_year
 * @property string|null $crc
 * @property int $parsing_status
 * @property int $status
 */
class Lot extends Model implements HwmEntityInterface
{
    public const FIRST_YEAR = 2007;
    public const PARSING_STATUS_NEW = 0; // Lot parsing from lot page. Can be modified in future
    public const PARSING_STATUS_NEW_MARKET = 1; // Lot parsing from market page - without correct started_at, eneded_at, etc. Can be modified in future
    public const PARSING_STATUS_ENDED = 2; // Lot parsing more than 3 days ago, has full information . Can't be modified . Equals is_parsed = 1
    public const STATUS_ON_MARKET = 0;
    public const STATUS_BUYED = 1;
    public const STATUS_COMPLETE = 2;

    protected $table = 'lots';

    /** @var array  */
    protected $fillable = [
        'lot_id',
        'crc',
        'lot_type',
        'entity_type',
        'entity_id',
        'entity_title',
        'title',
        'art_uid',
        'art_crc',
        'craft',
        'current_strength',
        'base_strength',
        'quantity',
        'price_per_fight',
        'price',
        'blitz_price',
        'seller_name',
        'seller_id',
        'started_at',
        'buyed_at',
        'ended_at',
        'is_parsed',
        'status',
        'parsing_status',
    ];

    /** @var array  */
    protected $casts = [
        'is_parsed' => 'bool',
        'price_per_fight' => 'float',
        'started_at' => 'datetime',
        'buyed_at' => 'datetime',
        'ended_at' => 'datetime',
    ];

    public $incrementing = false;

    protected $primaryKey = 'lot_id';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('lot_id', 'desc');
        });

        static::creating(function (Lot $lot) {
            $lot->setParsedStatus(false);
            $lot->updatePPF();

            $lot->started_at_year = $lot->started_at?->format('Y');
        });

        static::created(function (Lot $lot) {
            LotCache::setMaxLotId($lot->lot_id);
        });

        static::updating(function (Lot $lot) {
            $lot->setParsedStatus(false);
            $lot->updatePPF();
        });
    }

    public static function countApproximate(): int
    {
        return (int) self::max('lot_id') - EmptyLot::count();
    }

    public static function getLastWeekMinLotId(): int
    {
        return self::where('started_at', '>=', now()->subWeek()->format('Y-m-d'))->min('lot_id') ?: self::max('lot_id') - 50000;
    }

    public function art(): HasOne
    {
        return $this->hasOne(Art::class, 'hwm_id', 'entity_id');
    }

    public function filters(): Collection
    {
        return app(LotFiltersRepository::class)->findAll(
            LotType::from($this->lot_type),
            EntityType::from($this->entity_type),
            $this->entity_id,
        );
    }

    public function monitoredArts(): HasMany
    {
        return $this->hasMany(MonitoredArt::class, 'art_hwm_id', 'entity_id');
    }

    public function getHwmId(): string
    {
        return strval($this->lot_id);
    }

    public function getTitle(): string
    {
        return $this->entity_title;
    }

    public function setParsedStatus(bool $withSave = true): void
    {
        $is_complete = $this->buyed_at || now()->gte($this->ended_at);
        $this->is_parsed = $is_complete ? 1 : 0;

        $this->status = $this->buyed_at
            ? self::STATUS_BUYED
            : ($is_complete ? self::STATUS_COMPLETE : self::STATUS_ON_MARKET);

        if ($is_complete || now()->gte($this->started_at->addDays(3))) {
            $this->parsing_status = self::PARSING_STATUS_ENDED;
        }

        if ($withSave) {
            $this->save();
        }
    }

    public function checkPPF(): void
    {
        $this->price_per_fight = $this->calculatePricePerFight();
    }

    public function updatePPF(bool $withSave = false): bool
    {
        $price_per_fight = $this->calculatePricePerFight();

        if ($this->price_per_fight !== $price_per_fight) {
            $this->price_per_fight = $price_per_fight;
            if ($withSave) {
                $this->update(['price_per_fight' => $price_per_fight]);
            }

            return true;
        }

        return false;
    }

    public static function getCountMissedLots(): int
    {
        $row = \DB::selectOne("
            SELECT (MAX(lot_id) - MIN(lot_id) - COUNT(*)) as count from lots
        ");

        return $row->count;
    }

    /**
     * @throws \Throwable
     */
    public static function createFromLotEntity(LotEntity|MarketLot $lot): self
    {
        $data = $lot->toArray();
        if ($lot instanceof MarketLot) {
            $data['parsing_status'] = self::PARSING_STATUS_NEW_MARKET;
        }
        $model = self::create($data);

        return $model;
    }

    public function asLotEntity(): LotEntity
    {
        $lot = $this->toArray();

        $lot['operations'] = [];

        return new LotEntity($lot);
    }

    public function scopeNotFinished(Builder $builder): Builder
    {
        return $builder->whereIn('parsing_status', [self::PARSING_STATUS_NEW, self::PARSING_STATUS_NEW_MARKET]);
    }

    public function scopeActive(Builder $builder): Builder
    {
        return $builder->whereNull('buyed_at')
            ->where('ended_at', '>', now()->format(config('app.date_format')));
    }

    public function scopeOnlySale(Builder $builder): Builder
    {
        return $builder->where('lot_type', LotType::SALE->value);
    }

    public function scopeWithoutSale(Builder $builder): Builder
    {
        return $builder->where('lot_type', '<>', LotType::SALE->value);
    }

    public function scopeOnlyArts(Builder $builder): Builder
    {
        return $builder->where('entity_type', EntityType::ART->value);
    }

    public function scopeWithoutArts(Builder $builder): Builder
    {
        return $builder->where('entity_type', '<>', EntityType::ART->value);
    }

    public function scopeOnlyMonitored(Builder $builder): Builder
    {
        $monitoredArts = MonitoredArt::groupBy('art_hwm_id')->pluck('art_hwm_id')->toArray();

        return $builder->whereIn('entity_id', $monitoredArts);
    }

    public function scopeWithoutMonitored(Builder $builder): Builder
    {
        $monitoredArts = MonitoredArt::groupBy('art_hwm_id')->pluck('art_hwm_id')->toArray();

        return $builder->whereNotIn('entity_id', $monitoredArts);
    }

    public function scopeMissParsing(Builder $builder): Builder
    {
        return $builder->whereIn('parsing_status', [self::PARSING_STATUS_NEW, self::PARSING_STATUS_NEW_MARKET])
            ->where('ended_at', '<', now()->format(config('app.date_format')));
    }

    public function scopeParsedFromMarker(Builder $builder): Builder
    {
        return $builder->where('parsing_status', self::PARSING_STATUS_NEW_MARKET)->orderBy('lot_id', 'DESC');
    }

    public static function getMaxEndedEnd(): ?self
    {
        return self::query()
            ->where('parsing_status', self::PARSING_STATUS_ENDED)
            ->where('started_at', '<=', now()->subDays(3)->format(DATE_FORMAT))
            ->orderBy('lot_id', 'DESC')
            ->take(1)
            ->first();
    }

    public function scopeUndefined(Builder $builder): Builder
    {
        return $builder->where('entity_type', UNDEFINED);
    }

    public function calculatePricePerFight(): ?float
    {
        if (!$this->art || !$this->art->repair || is_null($this->current_strength) || is_null($this->base_strength)) {
            return null;
        }

        return HwmHelper::pricePerFight(
            $this->price,
            $this->art->repair,
            $this->current_strength,
            $this->base_strength
        );
    }

    public static function getYearsByPartitions(): array
    {
        return range(self::FIRST_YEAR, date('Y'));
    }

    // Copy from default chunk() method
    public static function chunkByYear(callable $callback, array $yearsRange = []): bool
    {
        $years = count($yearsRange) ? $yearsRange : self::getYearsByPartitions();

        foreach ($years as $year) {
            $query = self::getModel()->newQuery()
                ->where('started_at_year', $year);

            if ($callback($query, $year) === false) {
                return false;
            }
        }

        return true;
    }
}
