<?php

declare(strict_types=1);

namespace App\Services\ActivityLogger;

use Elasticsearch\ClientBuilder;
use Monolog\Handler\ElasticsearchHandler;
use Monolog\Logger;

class CreateActivityLogger
{
    private const ES_INDEX = 'activity_log';

    /**
     * Create a custom Monolog instance.
     *
     * @param array $config
     * @return \Monolog\Logger
     */
    public function __invoke(array $config)
    {
        $logger = new Logger('elasticsearch');

        $options = [
            'index' => self::ES_INDEX,
            'type' => '_doc'
        ];
        $client = ClientBuilder::create()->setHosts([config('services.elastic.hosts')]);
        $username = config('services.elastic.user');
        $password = config('services.elastic.password');

        if ($username && $password) {
            $client->setBasicAuthentication($username, $password);
        }

        $esClient = $client->build();
        $handler = new ElasticsearchHandler($esClient, $options, Logger::INFO, true);

        $logger->setHandlers(array($handler));

        return $logger;
    }
}
