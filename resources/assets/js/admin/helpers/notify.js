import Noty from 'noty';

Noty.overrideDefaults({
    layout: 'topRight',
    theme: 'backstrap',
    timeout: 2500,
    closeWith: ['click', 'button'],
});

const notify = (message, type = 'success') => {
    new Noty({
        type,
        text: message,
    }).show();
};

export default notify;
