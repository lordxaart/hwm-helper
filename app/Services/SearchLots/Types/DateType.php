<?php

declare(strict_types=1);

namespace App\Services\SearchLots\Types;

use App\Services\SearchLots\Enums\CompareOperator;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use DateTimeZone;
use InvalidArgumentException;

class DateType
{
    private CompareOperator $operator;
    private mixed $value;
    private ?DateTimeZone $timeZone;

    public function __construct(CompareOperator $operator, mixed $value)
    {
        $this->operator = $operator;

        if ($this->operator === CompareOperator::BETWEEN) {
            if (!is_array($value) || count($value) !== 2) {
                throw new InvalidArgumentException(
                    'Value must be array and contains 2 elements when CompareOperator BETWEEN'
                );
            }

            foreach ($value as $item) {
                $this->validateValue($item);
            }
        } else {
            $this->validateValue($value);
        }

        $this->value = $value;
    }

    public function getOperator(): CompareOperator
    {
        return $this->operator;
    }

    public function getValue(): mixed
    {
        if ($this->timeZone) {
            return $this->handleValueToTimezone($this->value, $this->timeZone);
        }

        return $this->value;
    }

    public function setTimezone(DateTimeZone $timeZone): self
    {
        $this->timeZone = $timeZone;

        return $this;
    }

    private function validateValue(mixed $value): void
    {
        if (!($value instanceof Carbon)) {
            throw new InvalidArgumentException('Value must be instance of ' . Carbon::class);
        }
    }

    /**
     * @return CarbonImmutable|array
     */
    public function handleValueToTimezone(mixed $value, DateTimeZone $timeZone): mixed
    {
        /** @var $value Carbon[] */
        if ($this->operator === CompareOperator::BETWEEN) {
            $min = $value[0]->toImmutable();
            $max = $value[1]->toImmutable();

            return [
                $min->startOfDay()->shiftTimezone($timeZone)->setTimezone('UTC'),
                $max->endOfDay()->shiftTimezone($timeZone)->setTimezone('UTC'),
            ];
        }

        /** @var $value Carbon */
        if ($this->operator === CompareOperator::MORE_OR_EQUAL) {
            return $value
                ->toImmutable()
                ->startOfDay()
                ->shiftTimezone($timeZone)
                ->setTimezone('UTC');
        }

        if ($this->operator === CompareOperator::MORE) {
            return $value
                ->toImmutable()
                ->addDay()
                ->startOfDay()
                ->shiftTimezone($timeZone)
                ->setTimezone('UTC');
        }

        if ($this->operator === CompareOperator::LESS_OR_EQUAL) {
            return $value
                ->toImmutable()
                ->endOfDay()
                ->shiftTimezone($timeZone)
                ->setTimezone('UTC');
        }

        if ($this->operator === CompareOperator::LESS) {
            return $value
                ->toImmutable()
                ->subDay()
                ->endOfDay()
                ->shiftTimezone($timeZone)
                ->setTimezone('UTC');
        }

        throw new InvalidArgumentException("Unsupported CompareOperator [{$this->operator->value}]");
    }
}
