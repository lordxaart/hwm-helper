<?php

declare(strict_types=1);

namespace App\HWM\Parsers\Traits;

use App\HWM\Exceptions\ParserError;
use App\HWM\Exceptions\ParserErrors\EmptyArtPage;
use App\HWM\Exceptions\ParserErrors\UnexpectedPage;
use App\HWM\Helpers\HwmHelper;
use App\HWM\Entities\Res;
use App\HWM\NewParsers\Pages\ArtParser;
use App\HWM\Parsers\CustomDom as Dom;
use App\HWM\Repositories\ResourceRepository;
use App\HWM\Repositories\SkillRepository;
use PHPHtmlParser\Dom\HtmlNode;

/**
 * @deprecated
 * @see ArtParser
 */
trait ArtTrait
{
    /**
     * @throws ParserError
     * @throws UnexpectedPage
     */
    public function getArtInfo(string $artId): array
    {
        if (!$this->getArtBlock()) {
            throw new UnexpectedPage('art');
        }

        $artTitle = strip_tags($this->find('.global_container_block_header')->innerHtml);

        if (!$artTitle) {
            throw new EmptyArtPage($artId);
        }

        /** @var HtmlNode $tr */
        $imgBlock = $this->find('.shop_art_info');
        $descBlock = $this->find('.art_info_desc');
        $descHtml = $descBlock->innerHtml();

        try {
            // Title
            $title = $artTitle;

            // Image
            $imgTag = $imgBlock->find('img.cre_mon_image2', 0);
            if (!$imgTag) {
                $imgTag = $imgBlock->find('img', 0);
            }

            $image = $imgTag->getAttribute('src');

            // Need level
            $cutText = getTextBetweenValues($descHtml, '<b>' . $this->getText('need_level') . ':</b>', '<br');
            $needLevel = intval($cutText);

            // Strength
            $cutText = getTextBetweenValues($descHtml, '<b>' . $this->getText('durability') . ':</b>', '<br');
            $strength = intval($cutText);

            // OA
            $cutText = getTextBetweenValues($descHtml, '<b> ' . $this->getText('ammunition_points') . ':</b>', '<br');
            $oa = intval($cutText) ?: null;

            // Repair
            $repair = null;
            $repairText = $this->getText('repair_cost');
            if (str_contains($descHtml, $repairText . ':')) {
                $cutText = getTextBetweenValues($descHtml, $repairText . ':', '<br />');
                $repair = HwmHelper::clearIntValue($cutText);
            }

            // Description
            $cutText = getTextBetweenValues($descHtml, '<b>' . $this->getText('description') . ':</b><br />', '<br');
            $description = $cutText;

            // From shop
            $fromShop = str_contains($descHtml, '<b> ' . $this->getText('cost') . ':</b>');
            $asFromShop = str_contains($descHtml, $this->getText('as_art_from_shop'));

            $shopPrice = null;
            if (str_contains($descHtml, $this->getText('cost') . ':')) {
                $shopPrice = $this->getArtPrice();
            }

            // Features
            $features = [];
            $table = (new Dom())
                ->loadStr(
                    getTextBetweenValues(
                        $descHtml,
                        '<b>' . $this->getText('modificators') . ':</b>',
                        '<br',
                        false,
                    )
                );
            $lastIndex = null;
            /** @var SkillRepository $skills */
            $skills = app(SkillRepository::class);
            foreach ($table->find('td') as $key => $td) {
                if ($key % 2) {
                    $features[$lastIndex] = str_replace('&nbsp;', '', $td->firstChild()->text);
                } else {
                    $lastIndex = $skills->getIdByTitle($td->firstChild()->getAttribute('title')) ?: UNDEFINED;
                }
            }

            // Set id
            $setId = null;
            $setName = null;
            $link = $descBlock->find('i font[color=green] a', 0);

            if ($link) {
                $setId = getTextBetweenValues($link->outerHtml, 'help.php?section=40#', '\'>');
                $setName = trim(strip_tags($link->innerHtml), '"');
            }
        } catch (\Exception $e) {
            throw new ParserError($artId . ': ' . $e->getMessage());
        }

        $art = [
            'hwm_id' => $artId,
            'title' => $title,
            'image' => $image,
            'need_level' => $needLevel,
            'strength' => $strength,
            'oa' => $oa,
            'repair' => $repair,
            'description' => $description,
            'modifiers' => $features,
            'set_id' => $setId,
            'set_name' => $setName,
            'from_shop' => $fromShop,
            'as_from_shop' => $asFromShop,
            'shop_price' => $shopPrice ?: null,
        ];

        $this->validateData('art', $art);

        return $art;
    }

    protected function getArtPrice(): ?int
    {
        if (!$artBlock = $this->getArtBlock()) {
            return null;
        }

        $table = $artBlock->find('table', 0);

        if (!$table) {
            return null;
        }

        $list = $table->find('td');

        /** @var ResourceRepository $resources */
        $resourcesRepository = app(ResourceRepository::class);
        $resources = [];

        for ($i = 0; $i < count($list); $i++) {
            if ($img = $list[$i]->find('img', 0)) {
                $key = $resourcesRepository->getIdByTitle($img->getAttribute('title'));
                $resources[$key] = intval(str_replace(',', '', $list[$i + 1]->innerHtml));
            }
        }

        $price = 0;
        foreach ($resources as $resource => $value) {
            $price += Res::convertToGold($resource, $value);
        }

        return $price;
    }

    /**
     * @return CustomDom|mixed|\PHPHtmlParser\Dom\Collection|null
     */
    protected function getArtBlock(): mixed
    {
        return $this->find('.art_info_container_block');
    }
}
