<?php

declare(strict_types=1);

namespace App\Services\Subscriptions\Entities;

class Sub
{
    public readonly int $id;
    public readonly string $alias;
    public readonly string $name;
    public readonly ?string $description;

    public function __construct(int $id, string $alias, string $name, ?string $description)
    {
        $this->id = $id;
        $this->alias = $alias;
        $this->name = $name;
        $this->description = $description;
    }

    public function toArray(): array
    {
        return [
            'alias' => $this->alias,
            'name' => $this->name,
        ];
    }
}
