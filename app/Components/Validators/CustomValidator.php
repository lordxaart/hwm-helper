<?php

declare(strict_types=1);

namespace App\Components\Validators;

use App\Core\Enums\SocialProvider;
use App\HWM\Helpers\HwmHelper;

class CustomValidator
{
    public function validateSocialProvider(string $field, string $value): bool
    {
        return SocialProvider::tryFrom($value) !== null;
    }

    public function replaceSocials(string $message): array|string
    {
        $socials = [];
        foreach (SocialProvider::cases() as $provider) {
            $socials[] = $provider->value;
        }

        $availableSocials = implode(',', $socials);

        return str_replace(':values', $availableSocials, $message);
    }


    public function validateCraftString(string $field, string $value): mixed
    {
        return filter_var($value, FILTER_VALIDATE_REGEXP, ['options' => ['regexp' => HwmHelper::getRegex('craft')]]);
    }
}
