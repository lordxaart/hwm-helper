<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\Request;

/**
 * @link https://github.com/laracraft-tech/laravel-xhprof
 */
class XHProfMiddleware
{
    public $exceptUrls = [
        '_debugbar'
    ];
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws Exception
     */
    public function handle(Request $request, Closure $next)
    {
        foreach ($this->exceptUrls as $url) {
            if (str_contains($request->getUri(), $url)) {
                return $next($request);
            }
        }

        if (config('app.xhprof_enabled')) {
            if (!extension_loaded('xhprof')) {
                throw new Exception('xhprof is enabled but extension is not installed or disabled! Please install or enable xhprof extension!');
            }

            //this needs to be declared as global!
            global $_xhprof;

            //if profiler is enabled in config, don't wait for ?_profile=1 get parameter (which sets the cookie)
            //to start profiling, just enable it immediately
            $_COOKIE['_profile'] = 1;
            $file = public_path('/vendor/xhprof/external/header.php');
            if (!file_exists($file)) {
                throw new Exception('Not found public/vendor/xhprof. Please run command: git clone git@github.com:preinheimer/xhprof.git ./public/vendor/xhprof');
            }

            require_once $file;
        }

        return $next($request);
    }
}
