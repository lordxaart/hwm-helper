<?php

declare(strict_types=1);

namespace App\Services\ImageOptimize;

use Spatie\ImageOptimizer\OptimizerChainFactory;
use Spatie\ImageOptimizer\Optimizers\Jpegoptim;

class SpatieOptimize implements OptimizerInterface
{
    public function optimize(string $path, int $quality): bool
    {
        $optimizerChain = OptimizerChainFactory::create()
            ->addOptimizer(new Jpegoptim([
                '-m' . $quality,
                '--strip-all',
                '--all-progressive',
            ]));

        $optimizerChain->optimize($path);

        return true;
    }
}
