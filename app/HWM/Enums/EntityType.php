<?php

declare(strict_types=1);

namespace App\HWM\Enums;

enum EntityType: string
{
    case ART = 'art';
    case ART_PART = 'art_part';
    case RESOURCE = 'resource';
    case ELEMENT = 'element';
    case CERTIFICATE = 'certificate';
    case HOUSE = 'house';
    case OBJECT_SHARE = 'object_share';
    case SKELETON = 'skeleton';

    public function isArtEntity(): bool
    {
        return match ($this) {
            self::ART, self::ART_PART => true,
            default => false,
        };
    }
}
