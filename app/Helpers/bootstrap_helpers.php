<?php

declare(strict_types=1);

if (!function_exists('trans')) {
    /**
     * Translate the given message.
     */
    function trans(string $key, array $replace = [], string $locale = null): string
    {
        $mapping = [
            'passwords.' => 'errors.passwords.',
            'auth.' => 'errors.auth.',
            'pagination.' => 'app.pagination.',
        ];

        foreach ($mapping as $k => $v) {
            if (str_starts_with($key, $k)) {
                $key = \Illuminate\Support\Str::replaceFirst($k, $v, $key);
            }
        }

        return strval(app('translator')->get($key, $replace, $locale));
    }
}
