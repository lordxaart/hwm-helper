<?php

declare(strict_types=1);

namespace App\ValueObjects\Notification;

use App\Services\Notifications\Enums\Channel as ChannelEnum;

class Channel
{
    public readonly ChannelEnum $channel;
    public readonly string $title;

    public function __construct(ChannelEnum $channel, string $title)
    {
        $this->channel = $channel;
        $this->title = $title;
    }

    public function getAlias(): string
    {
        return $this->channel->value;
    }

    public function getChannel(): string
    {
        return $this->channel->getLaravelChannelName();
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
