<?php

declare(strict_types=1);

namespace App\HWM\Parsers;

use App\HWM\Exceptions\ParserError;
use App\HWM\Entities\Element;
use App\HWM\Entities\Res;
use App\HWM\Parsers\Traits\ArtTrait;
use App\HWM\Parsers\Traits\MarketTrait;
use App\HWM\Parsers\Traits\CharacterTrait;
use App\HWM\Parsers\Traits\LotTrait;
use App\HWM\Parsers\Traits\Rules;
use App\HWM\Parsers\Traits\Texts;
use App\HWM\Repositories\ElementRepository;
use App\HWM\Repositories\ResourceRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use App\HWM\Parsers\CustomDom as Dom;

class HwmParser
{
    use Rules;
    use Texts;
    use ArtTrait;
    use LotTrait;
    use MarketTrait;
    use CharacterTrait;

    private Dom $domObject;
    protected array $errors;
    public ?string $locale;

    /**
     * HwmParser constructor.
     * @throws ParserError
     */
    public function __construct(string $html = null)
    {
        ini_set('memory_limit', '2048M');

        $this->domObject = new Dom();

        if ($html) {
            $this->setContent($html);
        }

        $this->setLocale();
    }

    /**
     * @param string $selector
     * @param int|null $index
     * @return mixed
     * @throws ParserError
     */
    public function find(string $selector, ?int $index = 0): mixed
    {
        if (!$this->domObject) {
            throw new ParserError(trans('errors.hwm.dom_content_is_not_loaded'));
        }

        return $this->domObject->find($selector, $index);
    }

    public function getContent(): string
    {
        try {
            return $this->domObject->root ? $this->domObject->root->innerHtml() : '';
        } catch (\Exception) {
            return '';
        }
    }

    public function getDom(): CustomDom
    {
        return $this->domObject;
    }

    /**
     * @throws ParserError
     */
    public function isHomePage(): bool
    {
        try {
            return str_contains($this->getContent(), $this->getText('text_for_home_page'))
                || str_contains($this->getContent(), 'Инвентарь');
        } catch (\Exception $e) {
            throw new ParserError($e->getMessage());
        }
    }

    /**
     * @throws ParserError
     */
    public function getErrorOnPage(): ?string
    {
        $this->find('font[color=red]');

        return null;
    }

    /**
     * @throws ParserError
     */
    public function isMainPage(): bool
    {
        return $this->isMobileMainPage() || $this->find('.trygame');
    }

    /**
     * @throws ParserError
     */
    public function isMobileMainPage(): bool
    {
        return boolval($this->find('.formFieldContainer > .form_input_field_Container > .form_input_field'));
    }

    public function setLocale(string $locale = null): void
    {
        $enLink = 'dcdn.lordswm.com';
        if (!$locale && str_contains($this->getContent(), $enLink)) {
            $locale = 'en';
        }

        $this->locale = $locale ?: config('app.fallback_locale');
    }

    /**
     * @throws ParserError
     */
    public function setContent(string $html): void
    {
        $html = str_replace('windows-1251', 'utf-8', $html);

        try {
            $this->domObject->loadStr($html);
        } catch (\Exception $e) {
            throw new ParserError($e->getMessage());
        }

        $this->setLocale();
    }

    public function isNeedCaptcha(): bool
    {
        return boolval($this->find('input[name="pcode"]'));
    }

    protected function setParserOptions(): void
    {
        $options = [
            'cleanupInput' => false,
            'removeScripts' => true,
            'removeStyles' => true,
        ];
        $this->domObject->setOptions($options);
    }

    /**
     * @throws ParserError
     */
    protected function validateData(string $type, array $data): bool
    {
        $errors = [];

        $items = isAssocArray($data) ? [$data] : $data;

        foreach ($items as $item) {
            $validator = Validator::make($item, $this->getRules($type));
            if ($validator->fails()) {
                $errors[] = json_encode($validator->errors()->getMessages(), JSON_UNESCAPED_UNICODE) . '. Item: ' . json_encode($item, JSON_UNESCAPED_UNICODE);
            }
        }

        if (count($errors)) {
            if (count($items) === 1) {
                $errors = Arr::first($errors);
            }
            throw new ParserError(
                trans('errors.hwm.invalid_response_after_parsing', ['type' => $type])
                .  '. ' . json_encode($errors, JSON_UNESCAPED_UNICODE)
            );
        }

        return true;
    }

    protected function stringHasResourceTitle(string $string): ?Res
    {
        $resources = new ResourceRepository($this->locale);

        /** @var Res $resource */
        foreach ($resources->allOnlyFromMarket() as $resource) {
            if (mb_stripos($string, $resource->getTitle())) {
                return $resource;
            }
        }

        if ($this->locale !== EN) {
            $resourcesEn = new ResourceRepository(EN);

            /** @var Res $resource */
            foreach ($resourcesEn->allOnlyFromMarket() as $resource) {
                if (mb_stripos($string, $resource->getTitle())) {
                    return $resource;
                }
            }
        }

        return null;
    }

    protected function stringHasElementTitle(string $string): ?Element
    {
        $elements = new ElementRepository($this->locale);
        /** @var Element $element */
        foreach ($elements->all() as $element) {
            if (mb_stripos($string, $element->getTitle())) {
                return $element;
            }
        }

        return null;
    }
}
