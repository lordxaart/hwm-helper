<?php

declare(strict_types=1);

namespace App\Console\Commands\Temp;

use App\Console\Commands\Command;
use App\HWM\Entities\Certificate;
use App\HWM\Entities\Element;
use App\HWM\Entities\House;
use App\HWM\Entities\MarketLot;
use App\HWM\Entities\ObjectShare;
use App\HWM\Enums\EntityType;
use App\HWM\Helpers\HwmMarketSort;
use App\HWM\Parsers\HwmParser;
use App\Jobs\Lot\HandleMarketLots;
use App\Models\Lot;
use App\Services\Telegram\TelegramApi;
use GuzzleHttp\Promise\Each;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Illuminate\Contracts\Console\Kernel;

class TestCommand extends Command
{
    /** @var string */
    protected $signature = 'test-command';

    /** @var string */
    protected $description = 'Test command';

    public function handle(Kernel $kernel): void
    {
        $this->testWeaponRequest();
        return;

        // тестування multi curl requests with promise
        $start = microtime(true);
        $client = new \GuzzleHttp\Client(['timeout' => 20]);
        $iterations = 100;

        $iterator = function () use ($client, $iterations) {
            $index = 0;
            while (true) {
                if ($index === $iterations) {
                    break;
                }

                $url = 'https://random-data-api.com/api/v2/users?size=1&index=' . $index++;
                $request = new Request('GET', $url, []);

                $this->info(now()->format('Y-m-d H:i:s.u') . " REQUEST $url");
                usleep(40000);
                yield $client
                    ->sendAsync($request)
                    ->then(function (Response $response) use ($request) {
                        return [$request, $response];
                    });
            }
        };

        $promise = Each::ofLimit(
            $iterator(),
            10,  /// concurrency,
            function ($result, $index) {
                /** @var Response $response */
                list($request, $response) = $result;
                $user = json_decode($response->getBody()->getContents(), true);
                $this->info(now()->format('Y-m-d H:i:s.u') . ' RESPONSE ' . (string)$request->getUri() . ' ID:' . $user['id']);
            }
        );
        $promise->wait();

        $this->info(round(microtime(true) - $start, 3));
    }

    private function testWeaponRequest(): void
    {
        $start = microtime(true);
        $type = 'weapon';
        $category = match ($type) {
            EntityType::HOUSE->value => House::MARKET_CATEGORY_ID,
            EntityType::OBJECT_SHARE->value => ObjectShare::MARKET_CATEGORY_ID,
            EntityType::CERTIFICATE->value => Certificate::MARKET_CATEGORY_ID,
            EntityType::ELEMENT->value => Element::MARKET_CATEGORY_ID,
            default => $type,
        };

        if (!$category) {
            return;
        }

        $lots = hwmClient()->getMarketLots(
            category: $category,
            sort: HwmMarketSort::NEWEST->value,
            resultLimit: 100,
        );
        $this->info('[TIME] Request: ' . (microtime(true) - $start));
        $this->info('Count lots: ' . $lots->count());
        $lots_ids = [];
        foreach ($lots as $lot) {
            $lots_ids[$lot->lot_id] = $lot;
        }

        $exists = Lot::whereIn('lot_id', array_keys($lots_ids))->pluck('lot_id');
        foreach ($exists as $lotId) {
            if (!empty($lots_ids[$lotId])) {
                unset($lots_ids[$lotId]);
            }
        }

        $handle = 0;
        /** @var MarketLot $lot */
        foreach ($lots_ids as $lot) {
            $handle++;
        }
        $this->info('Handle: ' . $handle);
    }
}
