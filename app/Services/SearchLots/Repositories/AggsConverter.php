<?php

declare(strict_types=1);

namespace App\Services\SearchLots\Repositories;

use App\Services\SearchLots\ValueObjects\Aggregation\Aggregation;

class AggsConverter
{
    /**
     * @param Aggregation[] $aggregations
     */
    public function parseAggregations(array $aggregations): array
    {
        $result = [];

        foreach ($aggregations as $aggregation) {
            $aggBody = [
                $aggregation->type->type => [
                    'field' => $aggregation->type->field,
                    ...$aggregation->type->attributes,
                ],
            ];

            if (!empty($aggregation->getChildren())) {
                $aggBody['aggs'] = $this->parseAggregations($aggregation->getChildren());
            }

            $result[$aggregation->name] = $aggBody;
        }

        return $result;
    }
}
