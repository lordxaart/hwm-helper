@extends('layout.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @include('elements.notification')

            <div class="card">
                <div class="card-header text-warning">
                    <icon name="attention"></icon> @{{ $t('app.auth.email_is_not_verified') }}
                </div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            @{{ $t('app.auth.on_email_sent_link_for_verify') }}
                        </div>
                    @endif

                    @{{ $t('app.auth.check_your_email_for_verify_link') }}
                </div>
                <div class="card-footer small">
                    <form action="{{ route('verification.resend') }}" method="POST">
                        {{ csrf_field() }}

                        @{{ $t('app.auth.if_you_not_received_letter') }}, <button type="submit" class="btn btn-sm btn-link p-0 m-0 align-baseline">{{ __('app.auth.click_here_to_request_one_more') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
