<?php

declare(strict_types=1);

namespace App\Models\Casts;

use App\Repositories\NotificationRepository;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class CastFilterNotifications implements CastsAttributes
{
    public function getNotificationsChannels(): array
    {
        return app(NotificationRepository::class)->getChannelAliases();
    }

    public function get($model, string $key, $value, array $attributes)
    {
        $data = $value ? (json_decode($value, true) ?: []) : [];

        foreach ($this->getNotificationsChannels() as $channel) {
            if (array_key_exists($channel, $data)) {
                $data[$channel] = boolval($data[$channel]);
            } else {
                $data[$channel] = true;
            }
        }

        return $data;
    }

    public function set($model, string $key, $value, array $attributes)
    {
        $data = [];

        foreach ($this->getNotificationsChannels() as $channel) {
            if (array_key_exists($channel, $value)) {
                $data[$channel] = boolval($value[$channel]);
            } else {
                $data[$channel] = true;
            }
        }

        return json_encode($data);
    }
}
