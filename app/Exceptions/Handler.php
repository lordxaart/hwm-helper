<?php

declare(strict_types=1);

namespace App\Exceptions;

use App\HWM\Exceptions\HwmException;
use App\HWM\Exceptions\RequestError;
use App\HWM\Exceptions\RequestErrors\LoginDuringAnotherScriptLogin;
use App\HWM\Exceptions\RequestErrors\LoginException;
use App\HWM\Exceptions\RequestLimitExceededError;
use App\Services\Notifications\Channels\Telegram\Exceptions\CouldNotSendNotification;
use Exception;
use Illuminate\Cache\RateLimiter;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Cache\RateLimiting\Unlimited;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Queue\MaxAttemptsExceededException;
use Illuminate\Support\Arr;
use Illuminate\Support\Lottery;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Mailer\Exception\TransportException;
use Throwable;

class Handler extends ExceptionHandler
{
    use ApiExceptionHandle;

    public array $context = [];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        LoginDuringAnotherScriptLogin::class,
        MaxAttemptsExceededException::class,
        TransportException::class,
    ];

    /**
     * Don't report with throttle
     * [ExceptionClass => [times, minutes]]
     * @var array[]
     */
    protected $throttleLimits = [
        LoginException::class => [1000, 1],
        RequestLimitExceededError::class => [1000,1],
        CouldNotSendNotification::class => [100, 1],
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    public function register()
    {
        $this->renderable(function (Throwable $e, $request) {
            if ($request->is('api/*')) {
                return $this->renderApiException($e, $request);
            }
        });
    }

    public function render($request, Throwable $e): mixed
    {
        if ($e instanceof ClientError && $request->expectsJson()) {
            return redirect()->guest(route('login'))->with('error', $e->getMessage());
        }

        if ($e instanceof ValidationException) {
            $e = new ValidationError($e->validator);
        }

        if (isDebugMode()) {
            return parent::render($request, $e);
        }

        if ($e instanceof LoginException) {
            $e = new ServerError();
        }

        return $this->handleException($request, $e);
    }


    protected function throttle(Throwable $e): mixed
    {
        foreach ($this->throttleLimits as $class => $rate) {
            if ($e instanceof $class) {
                return Limit::perMinutes($rate[1], $rate[0]);
            }
        }

        return null;
    }

    public function report(Throwable $e)
    {
        $e = $this->mapException($e);
        if ($this->shouldntReport($e)) {
            return;
        }

        $this->reportThrowable($e);
    }

    /**
     * @extends parent::shouldntReport
     *
     * Змінив логіку тротлинга.
     * Було: Дозволено в за певний час певну кількість report [60,1] - за 1хв максимум 60 report
     * Стало: [60,1] За 1хв один report якщо виключень більше ніж 60
     */
    protected function shouldntReport(Throwable $e)
    {
        if ($this->withoutDuplicates && ($this->reportedExceptionMap[$e] ?? false)) {
            return true;
        }

        $dontReport = array_merge($this->dontReport, $this->internalDontReport);
        if (! is_null(Arr::first($dontReport, fn ($type) => $e instanceof $type))) {
            return true;
        }

        return rescue(fn () => with($this->throttle($e), function ($throttle) use ($e) {
            if ($throttle instanceof Unlimited || $throttle === null) {
                return false;
            }

            if ($throttle instanceof Lottery) {
                return ! $throttle($e);
            }

            /** @var RateLimiter $limiter */
            $limiter = $this->container->make(RateLimiter::class);
            $key = with($throttle->key ?: 'illuminate:foundation:exceptions:'.$e::class, fn ($key) => $this->hashThrottleKeys ? md5($key) : $key);
            $val = $limiter->attempt(
                $key,
                $throttle->maxAttempts,
                fn () => true,
                60 * $throttle->decayMinutes
            );

            if (!$val) {
                $limiter->clear($key);
            }

            return $val;
        }), rescue: false, report: false);
    }

    protected function getHttpExceptionView(HttpExceptionInterface $e): string
    {
        return isAdminPanel() ? "admin.errors.{$e->getStatusCode()}" : "errors::{$e->getStatusCode()}";
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|Response
     * @throws Throwable
     */
    protected function handleException(Request $request, Throwable $e): mixed
    {
        if ($e instanceof HwmException) {
            $e = $this->prepareHwmException($e);
        }

        if ($e instanceof ClientError) {
            return redirect(url()->previous())->withErrors([$e->getMessage()]);
        }

        // write custom render method
        return parent::render($request, $e);
    }

    protected function prepareHwmException(HwmException $e): Exception
    {
        return new ClientError(Response::HTTP_BAD_REQUEST, $e->getMessage(), $e->getPrevious(), $e->getCode());
    }

    protected function context(): array
    {
        try {
            return array_filter($this->context);
        } catch (Throwable) {
            return [];
        }
    }

    protected function exceptionContext(Throwable $e): array
    {
        $context = [];

        if (method_exists($e, 'getContext') && is_array($e->getContext())) {
            $context = $e->getContext();
        }

        if (app()->runningInConsole()) {
            $context['cli'] = true;

            return $context;
        }

        return array_merge($context, $this->getContextFromRequest(app(Request::class)));
    }

    protected function getContextFromRequest(Request $request): array
    {
        $context = [
            'user_id' => $request->user()?->id ?: config('app.userid', UNDEFINED),
            'ip' => $request->getClientIp(),
        ];

        return array_filter($context);
    }

}
