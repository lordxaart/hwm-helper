<?php

declare(strict_types=1);

namespace App\ValueObjects;

class DateRange
{
    public readonly \DateTimeImmutable $start;
    public readonly \DateTimeImmutable $end;

    public function __construct(\DateTimeImmutable $start, \DateTimeImmutable $end)
    {
        if ($start > $end) {
            throw new \InvalidArgumentException('[End] must be after [start]');
        }

        $this->start = $start;
        $this->end = $end;
    }

    /**
     * @return \DateTimeImmutable[]
     */
    public function toArray(): array
    {
        return [
            $this->start,
            $this->end,
        ];
    }

    /**
     * @return string[]
     */
    public function toArrayFormatted(string $format): array
    {
        return [
            $this->start->format($format),
            $this->end->format($format),
        ];
    }
}
