<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hwm_bot_accounts', function (Blueprint $table) {
            $table->id();
            $table->string('login');
            $table->string('password');
            $table->string('email')->nullable();
            $table->boolean('is_blocked')->default(0);
            $table->integer('target')->default(0);
            $table->unsignedInteger('success_requests')->default(0);
            $table->unsignedInteger('error_requests')->default(0);
            $table->timestamp('last_success_request', 6)->nullable();
            $table->timestamp('last_error_request', 6)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hwm_bot_accounts');
    }
};
