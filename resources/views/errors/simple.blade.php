@extends('errors.layout', ['title' => "$code | " . __('errors.' . $code)])

@section('code', $code)
@section('title', __('errors.' . $code))
@if(\Illuminate\Support\Facades\Lang::has('errors.' . $code . '_message'))
    @section('message', __('errors.' . $code . '_message'))

    @if(isDebugMode() || Auth::check() && Auth::user()->isSuperAdmin())
        @section('extra_message', $exception->getMessage())
    @endif
@endif
