<?php

declare(strict_types=1);

namespace App\Listeners\User;

use App\Services\ActivityLogger\ActivityLogger;
use App\Services\ActivityLogger\ValueObjects\Activity;
use Illuminate\Auth\Events\Logout as Event;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogoutListener implements ShouldQueue
{
    public function __construct(private readonly ActivityLogger $activityLogger)
    {
    }

    public function handle(Event $event): void
    {
        $this->activityLogger->log(
            Activity::action('USER_LOGOUT')
                ->causedBy($event->user)
                ->context([
                    'guard' => $event->guard,
                ])
        );
    }
}
