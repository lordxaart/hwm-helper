<?php

declare(strict_types=1);

namespace App\Components\ImageSprite;

use App\Services\ImageOptimize\ImageOptimize;
use App\Exceptions\ServerErrors\ImageOptimizeError;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Image;
use InvalidArgumentException;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class ImageSpriteGenerator
{
    /** @var int  */
    public int $maxSpriteWidth = 5000;

    public string $cssPrefix = 'sprite-';

    public string $spriteImagePath;

    public string $spriteCssPath;

    /** Add to begin css file */
    public ?string $customStyle;

    /** @var Collection<array>  */
    protected Collection $files;

    protected int $spriteWidth;

    protected int $spriteHeight;

    public function __construct(array $files)
    {
        $this->files = collect([]);

        // handle and save all files
        foreach ($files as $key => $file) {
            if (!file_exists($file)) {
                throw new FileNotFoundException($file);
            }
            $fileInfo = getimagesize($file) ?: [0, 0];
            [$width, $height] = $fileInfo;
            // handle each file and map it
            $this->files[] = [
                'key' => $key,
                'path' => $file,
                'ext' => Arr::last(explode('.', $file)),
                'width' => $width,
                'height' => $height,
            ];
        }

        $this->files = collect($this->files)->sortBy('height');
    }

    /**
     * @throws Exception
     */
    public function generate(): self
    {
        if (!$this->spriteImagePath || !$this->spriteCssPath) {
            throw new InvalidArgumentException('Sprite image and css path are required');
        }

        $this->updateSpritePositions();

        $this->generateImageSprite($this->spriteImagePath);
        $this->generateCssSprite($this->spriteCssPath);

        return $this;
    }

    public function optimize(): self
    {
        $imageOptimize = app(ImageOptimize::class);
        try {
            $imageOptimize->optimize($this->spriteImagePath, ImageOptimize::BEST_TYPE);
        } catch (ImageOptimizeError) {
            //
        }

        return $this;
    }

    // Generate base options for sprite: width, height, positions for each files (add 'x','y' parameters for each file
    protected function updateSpritePositions(): void
    {
        $rows = [];

        $this->files->each(/**
         * @throws Exception
         */            function ($file) use (&$rows) {
            if ($file['width'] > $this->maxSpriteWidth) {
                throw new Exception(
                    "Width of image {$file['path']} more than max sprite width $this->maxSpriteWidth"
                );
            }

            if (!count($rows)) { // Create row empty if not exists yet
                $rows[] = [
                    'width' => 0,
                    'height' => 0,
                    'base_y' => 0,
                    'files' => [],
                    'count_files' => 0,
                ];
            } elseif (Arr::last($rows)['width'] > $this->maxSpriteWidth) {
                // Or create next row if previous row to long
                // Cut last row
                $previousRow = array_pop($rows);
                // Get last file from last row
                $lastFile = Arr::last($previousRow['files']);
                // We delete file because with last width of row more than maxSpriteWidth
                // and must insert this file to next row
                $this->removeFileFromRow($previousRow, $lastFile);
                // Insert last row to all rows after changes
                $rows[] = $previousRow;
                // Create new (next row) with height based on previous row
                $row = [
            'width' => 0,
            'height' => 0,
            'base_y' => $previousRow['base_y'] + $previousRow['height'],
            'files' => [],
            'count_files' => 0,
                ];
                // Add last file to new row
                $this->addFileToRow($row, $lastFile);
                // save to all rows
                $rows[] = $row;
            }

            // Cut last row from general list
            $currentRow = array_pop($rows);
            // Add current file to row
            $this->addFileToRow($currentRow, $file);
            // Save row in general list
            $rows[] = $currentRow;
        }
        );

        $this->spriteWidth = max(array_column($rows, 'width'));
        $this->spriteHeight = intval(Arr::last($rows)['base_y'] + Arr::last($rows)['height']);

        $this->files = collect([]);
        foreach ($rows as $row) {
            $this->files = $this->files->merge($row['files']);
        }
    }

    public function generateImageSprite(string $path): void
    {
        // Create empty canvas
        $sprite = Image::canvas($this->spriteWidth, $this->spriteHeight);
        // Insert each file on canvas
        $this->files->each(function ($file) use ($sprite) {
            $sprite->insert(
                $file['path'],
                'top-left',
                $file['x'],
                $file['y'],
            );
        });

        // Save and optimize generated image
        $sprite->save($path);
    }

    public function generateCssSprite(string $path, bool $prettyPrint = false): void
    {
        // Delete previous file and create new
        $css = fopen($path, 'w+');

        if (!$css) {
            throw new FileException();
        }
        // Insert css style for each file

        // Write custom style to begin file
        if ($this->customStyle) {
            fwrite($css, $this->customStyle . PHP_EOL);
        }

        // Generate css selector for each file based on cssPrefix and file['key']
        $this->files->each(function ($file) use ($css) {
            fwrite($css, '.' . $this->cssPrefix . $file['key'] . ' {' . PHP_EOL);
            fwrite($css, '    background-position: -' . $file['x'] . 'px -' . $file['y'] . 'px;' . PHP_EOL);
            fwrite($css, '}' . PHP_EOL);
        });
        fclose($css);

        if (!$prettyPrint) {
            $this->minifyCss();
        }
    }

    private function minifyCss(): void
    {
        $css = file_get_contents($this->spriteCssPath) ?: '';
        // delete breaks line
        $css = str_replace(PHP_EOL, '', $css);
        file_put_contents($this->spriteCssPath, $css);
    }

    // Updated general row parameters and 'x' & 'y' positions for file (only for file in row['files'])
    private function addFileToRow(array &$row, array $file): void
    {
        $file['x'] = $row['width'];
        $row['width'] += $file['width'];

        $row['height'] = max($row['height'], $file['height']);
        $file['y'] = $row['base_y'];

        $row['files'][] = $file;
        $row['count_files'] = count($row['files']);
    }

    // Updated general row parameters
    private function removeFileFromRow(array &$row, array $file): void
    {
        $row['width'] -= $file['width'];
        $key = array_search($file['key'], array_column($row['files'], 'key'));
        unset($row['files'][$key]);
        $row['count_files'] = count($row['files']);
        $row['height'] = max(array_column($row['files'], 'height'));
    }
}
