<?php

use Illuminate\Database\Migrations\Migration;

class UpdateLotOperationsColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // replace json column rateDiff > rate_diff
        DB::connection()->getPdo()->exec("UPDATE `lots` SET `operations` = REPLACE(`operations`, 'rateDiff', 'rate_diff') WHERE INSTR(`operations`, 'rateDiff') > 0;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
