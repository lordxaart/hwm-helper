<?php

use App\HWM\Enums\EntityType;
use Illuminate\Database\Migrations\Migration;

class UpdateLotsValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::connection()->getPdo()->exec("UPDATE `lots` SET `entity_type`='" . EntityType::OBJECT_SHARE->value . "' WHERE `entity_type` = 'stock'");
        DB::connection()->getPdo()->exec("UPDATE `lots` SET `entity_type`='" . EntityType::CERTIFICATE->value . "' WHERE `entity_type` = 'cert'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
