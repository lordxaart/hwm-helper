<?php

declare(strict_types=1);

namespace App\Notifications\User;

use App\Core\Enums\Queue;
use App\Http\Resources\LotBaseResource;
use App\HWM\Entities\Lot;
use App\HWM\Enums\LotType;
use App\HWM\Helpers\HwmHelper;
use App\Jobs\AdminNotify;
use App\Models\LotFilter;
use App\Models\LotFilterNotification;
use App\Models\User;
use App\Notifications\MarkdownHelpers;
use App\Services\LotFilter\LotFiltersService;
use App\Services\Notifications\Channels\Telegram\Entities\TelegramFile;
use App\Services\Notifications\Channels\Webpush\WebPushMessage;
use App\Services\Notifications\Enums\Channel;
use App\Services\Notifications\Enums\NotifyType;
use App\Services\Notifications\UserNotifyService;
use App\Services\Settings\User\GameLinkDecorator;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use InvalidArgumentException;

class AccordedFilters extends Notification implements ShouldQueue
{
    use Queueable;
    use MarkdownHelpers;

    public const NOT_ACTUAL_LOT_DELAY = 60 * 30; // 30 minutes

    public Lot $lot;

    /** @var LotFilter[] */
    public array $filters;

    public function __construct(Lot $lot, array $filters)
    {
        foreach ($filters as $filter) {
            if (!($filter instanceof LotFilter)) {
                throw new InvalidArgumentException('Each filter must be instance of LotFilter');
            }
        }

        $this->lot = $lot;
        $this->filters = $filters;
    }

    public function shouldSend(User $user, string $channel): bool
    {
        if (now()->diffInSeconds($this->lot->started_at) > self::NOT_ACTUAL_LOT_DELAY) {
            AdminNotify::dispatch(new \App\Notifications\Admin\Notify(sprintf('Lot notify is not actual. Lot: %s ', $this->lot->lot_id)));
            return false;
        }
        return true;
    }

    public function via(User $user): array
    {
        $userViaArray = app(UserNotifyService::class)
            ->getConfiguredChannels($user, NotifyType::ACCORDED_FILTER);

        $filterNotifications = app(LotFiltersService::class)
            ->getNotificationSettingsByFiltersIds(array_map(fn(LotFilter $filter) => $filter->id, $this->filters));

        $filtersVias = $this->getViasFromNotifications($filterNotifications);
        foreach ($userViaArray as $channel => $channelName) {
            if (!in_array($channel, $filtersVias)) {
                unset($userViaArray[$channel]);
            }
        }

        return [...$userViaArray];
    }

    public function viaQueues(): array
    {
        return [
            Channel::MAIL->getLaravelChannelName() => Queue::NOTIFICATIONS->value,
            Channel::BROWSER->getLaravelChannelName() => Queue::NOTIFICATIONS->value,
            Channel::TELEGRAM->getLaravelChannelName() => Queue::USER_NOTIFY_NEW_LOTS->value,
            Channel::DATABASE => Queue::NOTIFICATIONS->value,
        ];
    }

    public function toMail(User $user): MailMessage
    {
        LotFilterNotification::addRecord(
            $user->getId(),
            Channel::MAIL,
            $this->lot,
            $this->filters,
        );

        return (new MailMessage())
            ->subject(trans('app.notifications.found_lot_for_filters'))
            ->markdown('notifications.FoundNewLot.email', [
                'lot' => $this->lot,
                'filters' => $this->filters,
                'user' => $user,
                'linkDecorator' => app(GameLinkDecorator::class),
            ]);
    }

    public function toTelegram(User $user): TelegramFile
    {
        /** @var GameLinkDecorator $linkDecorator */
        $linkDecorator = app(GameLinkDecorator::class);

        $content = trans('app.notifications.found_lot_for_filters') . ':' . $this->br();

        $firstLine = $this->link($this->lot->getTitle(), $this->lot->getLink());

        if ($this->lot->isHasStrength()) {
            $firstLine .= $this->space() . $this->lot->current_strength . '/' . $this->lot->base_strength;
        }

        if ($this->lot->craft) {
            $firstLine .= $this->space() . $this->bold('[' . $this->lot->craft . ']') . $this->space();
        }

        $firstLine .= $this->space() . $this->lot->quantity . trans('app.main.pc');

        $content .= $this->li($firstLine);

        if ($this->lot->lot_type !== LotType::SALE->value) {
            $content .= $this->li(
                $this->bold(mb_ucfirst(trans('app.lots.filters.lot_type'))) . ':' . $this->space() . trans(
                    'app.lots.lot_types.' . $this->lot->lot_type
                )
            );
        }

        $content .= $this->li(
            $this->bold(mb_ucfirst(trans('app.main.price'))) . ':' . $this->space() . $this->lot->price . $this->space(
            ) . 'з.'
        );

        if ($this->lot->price_per_fight) {
            $content .= $this->li(
                $this->bold(mb_ucfirst(trans('app.main.price_per_fight'))) . ':' . $this->space() . round(
                    $this->lot->price_per_fight ?: 0,
                    2
                ) . $this->space() . 'з.'
            );
        }

        $content .= $this->li(
            $this->bold(mb_ucfirst(trans('app.main.seller'))) . ':' . $this->space() . $this->link(
                $this->lot->seller_name,
                $this->lot->getSellerProfileLink()
            )
        );

        $diffHuman = $this->lot->ended_at ? $this->lot->started_at->diffForHumans() : trans('app.notifications.less_minute_ago');
        $content .= $this->li(
            $this->bold(mb_ucfirst(trans('app.main.put_for_sale'))) . ':' . $this->space()
            . $this->lot->started_at->setTimezone(HwmHelper::HEROES_TZ)->format('Y-m-d H:i') . $this->space(
            ) . '(' . $diffHuman . ')'
        );

        if ($this->lot->ended_at) {
            $content .= $this->li(
                $this->bold(mb_ucfirst(trans('app.main.ended'))) . ':' . $this->space()
                . $this->lot->ended_at->setTimezone(HwmHelper::HEROES_TZ) . $this->space(
                ) . '(' . $this->lot->ended_at->diffForHumans() . ')'
            );
        }

        if (!empty($this->filters)) {
            $content .= $this->br() . $this->bold(trans('app.main.terms_filter')) . ':' . $this->br();
        }

        $terms = [];
        foreach ($this->filters as $filter) {
            foreach ($filter->terms->toArray() as $term) {
                if (!in_array($term->toTransString(), $terms)) {
                    $terms[] = $term->toTransString();
                    $content .= $this->li($term->toTransString());
                }
            }
        }

        $content = trim($content, "\n");

        $content = $linkDecorator->replaceGameLinkForUser($content, $user);

        $message = TelegramFile::create()->content($content);

        $marketLink = $this->lot->getMarketLink();
        $message->button(trans('app.main.market'), $linkDecorator->replaceGameLinkForUser($marketLink, $user));
        $img_path = $this->lot->getEntityObject()->getImage(true);
        if (!file_exists($img_path)) {
            $img_path = public_path('images/art_default.png');
        }
        $message->photo($img_path);

        LotFilterNotification::addRecord(
            $user->getId(),
            Channel::TELEGRAM,
            $this->lot,
            $this->filters,
        );

        return $message;
    }

    public function toWebPush(User $user): WebPushMessage
    {
        $message = new WebPushMessage();

        /** @var GameLinkDecorator $linkDecorator */
        $linkDecorator = app(GameLinkDecorator::class);

        $title = trans('app.notifications.found_lot_for_filters');

        $content = $this->lot->getTitle();

        if ($this->lot->isHasStrength()) {
            $content .= ' ' . $this->lot->current_strength . '/' . $this->lot->base_strength;
        }

        if ($this->lot->craft) {
            $content .= ' ' . '[' . $this->lot->craft . ']';
        }

        $content .= ' ' . $this->lot->quantity . trans('app.main.pc') . ', ';

        $content .= $this->lot->price . ' з.';

        if ($this->lot->price_per_fight) {
            $content .=
                ' (' . round(
                    $this->lot->price_per_fight ?: 0,
                    2
                ) . ' з.)';
        }

        $content .= ', ' . $this->lot->seller_name . PHP_EOL;

        if ($this->lot instanceof Lot) {
            $content .= $this->lot->started_at->setTimezone(HwmHelper::HEROES_TZ)
                . ' (' . $this->lot->started_at->diffForHumans() . ')' . PHP_EOL;
        }

        $content = trim($content, PHP_EOL);

        $content = $linkDecorator->replaceGameLinkForUser($content, $user);

        $marketLink = $this->lot->getMarketLink();

        $message->title($title)
            ->body($content)
            ->action(trans('app.main.market'), 'open_link')
            ->image($this->lot->getEntityObject()->getImage())
            ->icon($this->lot->getEntityObject()->getImage())
            ->data([
                'url' => $linkDecorator->replaceGameLinkForUser($marketLink, $user),
            ]);

        LotFilterNotification::addRecord(
            $user->getId(),
            Channel::BROWSER,
            $this->lot,
            $this->filters,
        );

        return $message;
    }

    public function toBroadcast(User $notifiable): BroadcastMessage
    {
        /** @var GameLinkDecorator $linkDecorator */
        $linkDecorator = app(GameLinkDecorator::class);

        $lot = (new LotBaseResource($this->lot))->resolve();
        $lot['lotLink'] = $linkDecorator->replaceGameLinkForUser($lot['lotLink'], $notifiable);
        $lot['marketLink'] = $linkDecorator->replaceGameLinkForUser($lot['marketLink'], $notifiable);

        LotFilterNotification::addRecord(
            $notifiable->getId(),
            Channel::BROWSER,
            $this->lot,
            $this->filters,
        );

        return new BroadcastMessage([
            'filter_id' => $this->filters[0]->id,
            'lot' => $lot,
        ]);
    }

    public function broadcastType(): string
    {
        return 'accorded_filter';
    }


    private function getViasFromNotifications(array $notifications): array
    {
        if (empty($notifications)) {
            return [];
        }

        $vias = [];

        foreach ($notifications as $channels) {
            $vias = array_merge(
                $vias,
                array_keys(
                    array_filter($channels, fn(bool $value) => $value),
                ),
            );
        }

        return array_unique($vias);
    }
}
