<?php

namespace Tests\Feature\Pages;

use Tests\TestCase;
use Tests\Traits\WithStudUser;

class MonitoringPageTest extends TestCase
{
    use WithStudUser;
    /**
     * @test
     */
    public function redirect_to_login_from_monitoring_for_guest()
    {

        $this->assertAuthenticationRequired(self::MONITORING_URL);
    }

    /**
     * @test
     */
    public function can_see_monitoring_page_for_auth_user()
    {
        $response = $this->actingAs($this->createStubUser())
            ->get(self::MONITORING_URL);

        $response->assertStatus(200);
    }
}
