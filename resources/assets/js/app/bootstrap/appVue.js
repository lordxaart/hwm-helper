// Import store
import Vue from 'vue';
import store from 'JS/app/store';
import router from 'JS/app/router/index';
import i18n from 'JS/app/utils/localization';
import selectLocale from 'JS/app/components/global/SelectLocale';
import { mapActions, mapGetters } from 'vuex';
import beforeEach from 'JS/app/router/beforeEach';
import afterEach from 'JS/app/router/afterEach';
import config from 'JS/app/config';
import { init, setUserId, setUserProperties } from '../utils/amplitude';

const Feedback = () => import('JS/app/components/global/Feedback');

// Set global variables
Vue.prototype.$config = config;

// Main App
// eslint-disable-next-line no-new
const app = new Vue({
    el: '#app',
    store,
    router,
    i18n,
    components: {
        selectLocale,
    },
    metaInfo() {
        return {
            title: i18n.t('seo.index.title'),
            titleTemplate: `%s | ${config.app.name}`,
            meta: [{
                vmid: 'description',
                name: 'description',
                content: i18n.t('seo.default.desc'),
            }, { vmid: 'keywords', name: 'keywords', content: i18n.t('seo.default.keys') }],
        };
    },
    data: {
        FeedbackComponent: Feedback, feedbackModalId: 'feedback_modal', loadingPage: false,
    },
    methods: {
        ...mapActions('User', ['initUser']),
        openFeedbackModal() {
            this.openModal(this.feedbackModalId);
        },
        async initTracking() {
            init(this.$config.app.amplitude_api_key);

            setUserProperties({
                logged: !!this.user?.id,
                locale: this.$config.app.locale,
                env: this.$config.app.env,
            });

            if (this.user) {
                setUserId(this.user.id);
                setUserProperties({
                    email: this.user.email,
                });
            }
        },
    },
    computed: {
        ...mapGetters('User', {
            user: 'getUser',
        }),
    },
    async created() {
        if (window.USER) {
            this.initUser();
        }
        this.hwm.setGameLink(this.$config.app.game_link);

        await this.initTracking();
    },
});

router.beforeEach((to, from, next) => {
    beforeEach(app, to, from, next);
});
router.afterEach((to, from, next) => {
    afterEach(app, to, from, next);
});
