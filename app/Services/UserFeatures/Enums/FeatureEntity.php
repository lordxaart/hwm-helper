<?php

declare(strict_types=1);

namespace App\Services\UserFeatures\Enums;

enum FeatureEntity: string
{
    case User = 'user';
    case Subscription = 'subscription';
}
