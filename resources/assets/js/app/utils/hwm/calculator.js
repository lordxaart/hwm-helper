import has from 'lodash/has';
import hwmMaster from './masters';

const calculator = (art, masters) => {
    const results = [];

    if (!Array.isArray(masters)) {
        // eslint-disable-next-line no-param-reassign
        masters = [masters];
    }

    if (!hwmMaster.checkMaster(masters)) {
        return results;
    }

    if (!has(art, 'baseStrength')
        || !has(art, 'currentStrength')
        || !has(art, 'repair')
        || (!has(art, 'resonablePricePerFight') && !has(art, 'price'))) {
        console.error('Missed required property');
        return results;
    }

    masters.forEach((master) => {
        const repairCost = (art.repair * master.pRate) / 100;
        let strength = art.baseStrength;
        const steps = [];
        let step = 1;

        steps.push({
            strength: art.baseStrength,
            fights: art.currentStrength,
            price: art.price,
            pricePerFight: art.price / art.currentStrength,
            step,
        });

        let optimalStep = steps[0];

        let minPricePerFight = Math.abs(art.resonablePricePerFight - steps[0].pricePerFight);

        do {
            step += 1;

            const lastStep = steps[steps.length - 1];

            const fights = Math.floor((strength * master.pRepair) / 100);
            strength -= 1;

            const currentStep = {
                strength,
                fights: lastStep.fights + fights,
                price: lastStep.price + repairCost,
                pricePerFight: (lastStep.price + repairCost) / (lastStep.fights + fights),
                step,
            };

            steps.push(currentStep);

            if (!art.resonablePricePerFight) {
                if (currentStep.pricePerFight < lastStep.pricePerFight) {
                    optimalStep = currentStep;
                }
            } else {
                const currentMinPricePerFight = Math.abs(art.resonablePricePerFight - currentStep.pricePerFight);
                if (minPricePerFight > currentMinPricePerFight || currentStep.pricePerFight < lastStep.pricePerFight) {
                    optimalStep = currentStep;
                    minPricePerFight = currentMinPricePerFight;
                }
            }
        } while (strength > 0);

        results.push({
            steps,
            fights: optimalStep.fights,
            price: optimalStep.price,
            pricePerFight: optimalStep.pricePerFight,
            strength: optimalStep.strength,
        });
    });

    return results;
};

export default calculator;
