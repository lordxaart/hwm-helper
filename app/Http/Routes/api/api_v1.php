<?php

declare(strict_types=1);

use App\Http\Controllers\API\V1\ApiController;
use App\Http\Controllers\API\V1\ArtsController;
use App\Http\Controllers\API\V1\CharactersController;
use App\Http\Controllers\API\V1\LotFiltersController;
use App\Http\Controllers\API\V1\MarketController;
use App\Http\Controllers\API\V1\SearchLotsAction;
use App\Http\Controllers\API\V1\MonitoredArtsController;
use App\Http\Controllers\API\V1\UsersController;
use App\Http\Controllers\API\V1\StatusController;
use App\Http\Controllers\API\V1\Analytics\LotsController;
use App\Http\Controllers\API\V1\WebPushSubscriptionsController;

Route::group([], function () {
    Route::get('/', [ApiController::class, 'home']);
    Route::get('/app', [ApiController::class, 'getApp']);

    // Arts
    Route::group(['prefix' => 'arts'], function () {
        Route::get('/', [ArtsController::class, 'index']);
        Route::get('categories', [ArtsController::class, 'categories']);
        Route::get('marketCategories', [ArtsController::class, 'marketCategories']);
        Route::get('sets', [ArtsController::class, 'sets']);

        Route::post('/', [ArtsController::class, 'store'])->middleware('api.auth');
        Route::post('{artId}/refreshFromOriginal', [ArtsController::class, 'updateFromOriginal'])
            ->middleware('api.auth');
    });

    // Market controller
    Route::group(['prefix' => 'market'], function () {
        Route::get('elements', [MarketController::class, 'elements']);

        Route::group(['prefix' => 'lots'], function () {
            Route::get('/', [MarketController::class, 'lotsByArt']);
            Route::post('updateLot', [MarketController::class, 'updateLot']);
            Route::get('search', SearchLotsAction::class);

            Route::group(['prefix' => 'filters', 'middleware' => 'api.auth'], function () {
                Route::get('/', [LotFiltersController::class, 'index']);
                Route::post('/', [LotFiltersController::class, 'store']);
                Route::post('{filterId}', [LotFiltersController::class, 'update']);
                Route::delete('{filterId}', [LotFiltersController::class, 'delete']);
                Route::post('{filterId}/notification', [LotFiltersController::class, 'updateNotificationChannel']);
            });
        });
    });

    Route::group(['prefix' => 'analytics'], function () {
        Route::get('lots', LotsController::class);
    });

    Route::group(['prefix' => 'monitor/arts', 'middleware' => 'api.auth'], function () {
        Route::get('/', [MonitoredArtsController::class, 'index']);
        Route::post('/', [MonitoredArtsController::class, 'store']);

        Route::group(['prefix' => '{monitoredArtId}'], function () {
            Route::put('disable', [MonitoredArtsController::class, 'disable']);
            Route::put('enable', [MonitoredArtsController::class, 'enable']);
            Route::delete('/', [MonitoredArtsController::class, 'delete']);
        });
    });

    Route::group(['prefix' => 'profile', 'middleware' => 'api.auth'], function () {
        Route::get('/', [UsersController::class, 'index']);

        Route::get('connect/telegram', [UsersController::class, 'connectTelegram']);
        Route::post('disconnect/telegram', [UsersController::class, 'disconnectTelegram']);

        Route::group(['prefix' => 'settings'], function () {
            Route::post('/', [UsersController::class, 'updateSettings']);
            Route::post('notifications', [UsersController::class, 'updateNotificationsSettings']);
        });
    });

    Route::group(['prefix' => 'notifications', 'middleware' => 'api.auth'], function () {
        Route::post('web-push/subscribe', [WebPushSubscriptionsController::class, 'subscribe']);
        Route::post('web-push/unsubscribe', [WebPushSubscriptionsController::class, 'unsubscribe']);
    });

    Route::group(['prefix' => 'characters'], function () {
        Route::get('find', [CharactersController::class, 'find']);
    });

    Route::post('sendFeedback', [UsersController::class, 'sendFeedback']);

    Route::group(['prefix' => 'status', 'middleware' => ['api.auth', 'role:super-admin']], function () {
        Route::get('{service}', [StatusController::class, 'service']);
    });
});
