<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtMonitoredFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('art_monitored_filters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('monitoring_art_id')->unsigned();
            $table->string('type')->nullable();
            $table->string('value', 500)->nullable();
            $table->tinyInteger('is_active')->default(1);
            $table->timestamps();

            $table->foreign('monitoring_art_id')
                ->references('id')
                ->on('art_monitored_by_user')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('art_monitored_filters');
    }
}
