@php
    /** @var \App\Models\Art  $art */
    $art = $entry->art ?: $entry;
    $link = $column['link'] ?? 'admin';
    switch ($link) {
        case 'market':
            $url = $art->getMarketLink();
            break;
        case 'info':
            $url = $art->getLink();
            break;
        default:
            $url = route('admin.arts.show', ['id' => $art->id]);
    }
@endphp

@if($art && $art instanceof \App\Models\Art)
    <a href="{{ $url }}">{{ $art->title }}</a>
@endif