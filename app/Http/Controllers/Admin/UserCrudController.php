<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Actions\User\BlockUser;
use App\Actions\User\RestoreUser;
use App\Actions\User\UnblockUser;
use App\Core\Enums\SocialProvider;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Objects\UserSettings;
use App\Models\Role;
use App\Models\User;
use App\Services\UserFeatures\Enums\Feature;
use App\Services\Subscriptions\SubscriptionService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;

/**
 * Class UserCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class UserCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;

    /**
     * @throws \Exception
     */
    public function setup(): void
    {
        $this->crud->setModel('App\Models\User');
        $this->crud->addClause('withTrashed');
        $this->crud->with(['profiles']);
        $this->crud->addClause('withCount', 'monitoredArts');
        $this->crud->addClause('withCount', 'filters');

        $this->crud->setRoute(config('backpack.base.route_prefix') . '/users');
        $this->crud->setEntityNameStrings('user', 'users');

        $this->crud->addClause('orderBy', 'id', 'asc');
        $this->crud->addButtonFromView('line', 'block', 'block', 'end');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumns([
            [
                'name' => 'id',
                'type' => 'text',
                'label' => 'ID',
            ],
            [
                'name' => 'email',
                'type' => 'email',
                'label' => 'Email',
            ],
            [
                'name' => 'profiles',
                'type' => 'select_multiple_socials',
                'label' => 'Socials',
                'entity' => 'profiles',
                'attribute' => 'provider',
                'model' => 'App\Models\SocialAccount',
                'visibleInTable' => false,
            ],
            [
                'name' => 'monitored_arts_count',
                'type' => 'closure',
                'label' => 'Arts (current / max)',
                'function' => function (User $user) {
                    return $user->monitored_arts_count . ' / ' . $user->features()->max_monitored_arts;
                },
                'orderable' => true,
                'orderLogic' => function ($query, $column, $columnDirection) {
                    return $query->orderBy('monitored_arts_count', $columnDirection);
                },
            ],
            [
                'name' => 'filters_count',
                'type' => 'closure',
                'label' => 'Filters (current / max)',
                'function' => function (User $user) {
                    return $user->filters_count . ' / ' . $user->features()->max_monitored_filters;
                },
                'orderable' => true,
                'orderLogic' => function ($query, $column, $columnDirection) {
                    return $query->orderBy('filters_count', $columnDirection);
                }
            ],
            [
                'name' => 'email_verified_at',
                'type' => 'check_date',
                'label' => 'Verified',
                'visibleInTable' => false,
            ],
            [
                'name' => 'blocked_at',
                'type' => 'check_date',
                'label' => 'Blocked',
                'visibleInTable' => false,
            ],
            [
                'name' => 'deleted_at',
                'type' => 'check_date',
                'label' => 'Deleted',
                'visibleInTable' => false,
            ],
            [
                'name' => 'created_at',
                'label' => 'Created',
                'type' => 'datetime',
            ],
        ]);

        // $this->crud->removeAllButtonsFromStack('top');
        // $this->setupFilters();
    }

    protected function setupShowOperation()
    {
        /** @var SubscriptionService $s */
        $s = app(SubscriptionService::class);
        $this->crud->setColumns([
            ['name' => 'id', 'type' => 'text', 'label' => 'ID'],
            ['name' => 'email', 'type' => 'email', 'label' => 'Email'],
            ['type' => 'closure', 'label' => 'Subscription', 'function' => function($entity) use ($s) {
                $subs = [];
                foreach ($s->getSubscriptions($entity->getId()) as $subscription) {
                    if ($subscription->isActive()) {
                        $subs[] = $subscription->subscription->name . ($subscription->isTrialActive() ? ' (Trial)' : '') . ' (' . Carbon::parse($subscription->ended_at)->diffForHumans() . ')';
                    }
                }

                return implode(' | ', $subs);
            }],
            ['name' => 'profiles', 'type' => 'select_multiple_socials', 'label' => 'Socials', 'entity' => 'profiles', 'attribute' => 'provider', 'model' => 'App\Models\SocialAccount'],
            ['name' => 'email_verified_at', 'type' => 'datetime', 'label' => 'Verified'],
            ['name' => 'api_token', 'type' => 'text', 'label' => 'API Token'],
            ['name' => 'deleted_at', 'type' => 'datetime', 'label' => 'Deleted'],
            ['name' => 'created_at', 'label' => 'Created', 'type' => 'datetime'],
            ['name' => 'updated_at', 'label' => 'Updated', 'type' => 'datetime'],
            ['type' => 'closure', 'label' => 'Custom delay', 'function' => fn($entity) => $entity->settings(UserSettings::CUSTOM_NOTIFY_DELAY) ? '✅' : '❌'],
            ['name' => 'monitoredArts', 'label' => 'Monitored Arts', 'type' => 'view', 'view' => 'crud::columns.arts_preview', 'entity' => 'monitoredArts'],
            ['name' => 'features_original', 'label' => 'Features', 'type' => 'view', 'view' => 'crud::columns.user_features'],
            ['name' => 'lotFilters', 'label' => 'Lot Filters', 'type' => 'view', 'view' => 'crud::columns.lot_filters', 'entity' => 'filters'],
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->crud->addFields([
            [
                'name' => 'email',
                'label' => 'Email',
                'type' => 'text',
                'attributes' => ['disabled' => 'disabled']
            ],
            [
                // two interconnected entities
                'label'             => 'User Role Permissions',
                'field_unique_name' => 'user_role_permission',
                'type'              => 'checklist_dependency',
                'name'              => ['roles', 'permissions'],
                'subfields'         => [
                    'primary' => [
                        'label'            => 'Roles',
                        'name'             => 'roles', // the method that defines the relationship in your Model
                        'entity'           => 'roles', // the method that defines the relationship in your Model
                        'entity_secondary' => 'permissions', // the method that defines the relationship in your Model
                        'attribute'        => 'name', // foreign key attribute that is shown to user
                        'model'            => config('permission.models.role'), // foreign key model
                        'pivot'            => true, // on create&update, do you need to add/delete pivot table entries?]
                        'number_columns'   => 3, //can be 1,2,3,4,6
                    ],
                    'secondary' => [
                        'label'          => mb_ucfirst('Permissions'),
                        'name'           => 'permissions', // the method that defines the relationship in your Model
                        'entity'         => 'permissions', // the method that defines the relationship in your Model
                        'entity_primary' => 'roles', // the method that defines the relationship in your Model
                        'attribute'      => 'name', // foreign key attribute that is shown to user
                        'model'          => config('permission.models.permission'), // foreign key model
                        'pivot'          => true, // on create&update, do you need to add/delete pivot table entries?]
                        'number_columns' => 3, //can be 1,2,3,4,6
                    ],
                ],
            ],
            [
                'name' => 'custom_notify_delay',
                'label' => 'Custom delay',
                'type' => 'boolean',
            ],
            [
                'name' => 'features_original',
                'label' => 'Features',
                'type' => 'view',
                'view' => 'crud::fields.user_features'
            ],
        ]);

        $this->crud->setValidation(UpdateUserRequest::class);
    }

    public function update($id): \Illuminate\Http\Response|array|\Illuminate\Http\RedirectResponse
    {
        $this->crud->hasAccessOrFail('update');

        $request = $this->crud->getRequest();

        /** @var User $user */
        $user = User::withTrashed()->findOrFail($id);
        $roles = $request->input('roles');

        if (!$roles || !count($roles)) {
            \Alert::error('User must have one role at least')->flash();
            return back();
        }

        $superAdminId = Role::findByName(Role::SUPER_ADMIN)->id;

        if ($user->isSuperAdmin() && !in_array($superAdminId, $roles)) {
            $roles[] = $superAdminId;
        }

        if (!$user->isSuperAdmin() && in_array($superAdminId, $roles)) {
            $key = array_search($superAdminId, $roles);
            unset($roles[$key]);
        }

        $user->syncRoles($roles);

        if ($request->has('settings')) {
            $user->settings(Arr::except($request->input('settings'), $user->settings->getEditableKeys()));
            $user->save();
        }

        $user->settings([UserSettings::CUSTOM_NOTIFY_DELAY => $request->input('custom_notify_delay') === '1']);
        $user->save();

        if ($request->has('features')) {
            $features = $request->input('features');
            foreach ($user->features()->features as $feature) {
                if (!isset($features) && $feature->type !== 'bool')
                    continue;

                $value = $feature->type === 'bool' ? isset($features[$feature->key]) : $features[$feature->key];
                features()->setUserFeature($user->getId(), Feature::tryFrom($feature->key), $value);
            }

            $user->save();
        }

        // show a success message
        \Alert::success(trans('backpack::crud.update_success'))->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($user->getKey());
    }

    public function destroy($id): ?bool
    {
        $user = User::findOrFail($id);

        return $user->delete();
    }

    public function restore($id)
    {
        $user = User::onlyTrashed()->findOrFail($id);

        return RestoreUser::run($user);
    }

    public function block($id)
    {
        $user = User::notBlocked()->findOrFail($id);

        $request = $this->crud->validateRequest();

        $until_to = null;
        if ($request->input('until_to')) {
            $until_to = Carbon::parse($request->input('until_to'));
        }

        return BlockUser::run($user, $until_to);
    }

    public function unblock($id)
    {
        $user = User::onlyBlocked()->findOrFail($id);

        return UnblockUser::run($user);
    }

    protected function setupFilters()
    {
        $this->crud->addFilter(
            [
                'type' => 'simple',
                'name' => 'email_verified_at',
                'label' => 'Verified',
            ],
            false,
            function () {
                $this->crud->addClause('whereNotNull', 'email_verified_at');
            }
        );
        $this->crud->addFilter(
            [
            'type' => 'simple',
            'name' => 'deleted_at',
            'label' => 'Deleted',
            ],
            false,
            function () {
                $this->crud->addClause('whereNotNull', 'deleted_at');
            }
        );
        $this->crud->addFilter(
            [
            'type' => 'simple',
            'name' => 'has_monitored_arts',
            'label' => 'Monitored Arts',
            ],
            false,
            function () {
                $this->crud->addClause('has', 'monitoredArts');
            }
        );
        $this->crud->addFilter(
            [
                'type' => 'select2_multiple',
                'name' => 'socials',
                'label' => 'Socials',
            ],
            array_map(function (SocialProvider $provider) {
                return $provider->getTitle();
            }, SocialProvider::cases()),
            function ($values) {
                foreach (json_decode($values) as $key => $value) {
                    $this->crud->addClause('HasSocial', $value);
                }
            }
        );
        $this->crud->addFilter(
            [
                'type' => 'select2_multiple',
                'name' => 'roles',
                'label' => 'Roles',
            ],
            array_keys(Role::getBasePermissions()),
            function ($values) {
                foreach (json_decode($values) as $value) {
                    $this->crud->addClause('hasRole', $value);
                }
            }
        );
        $this->crud->addFilter(
            [
                'type' => 'date_range',
                'label' => 'Created',
                'name' => 'created_at',
            ],
            false,
            function ($value) {
                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', $dates->from);
                $this->crud->addClause('where', 'created_at', '<=', $dates->to . ' 23:59:59');
            }
        );
        $this->crud->addFilter(
            [
                'type' => 'date_range',
                'name' => 'updated_at',
                'label' => 'Updated',
            ],
            false,
            function ($value) {
                $dates = json_decode($value);
                $this->crud->addClause('where', 'updated_at', '>=', $dates->from);
                $this->crud->addClause('where', 'updated_at', '<=', $dates->to . ' 23:59:59');
            }
        );
    }

    protected function addBooleanFilter($name, $label)
    {
        $this->crud->addFilter(
            [
            'type' => 'dropdown',
            'name' => $name,
            'label' => $label
            ],
            [0 => 'No', 1 => 'Yes'],
            function ($value) use ($name) {
                $this->crud->addClause('where', $name, $value);
            }
        );
    }
}
