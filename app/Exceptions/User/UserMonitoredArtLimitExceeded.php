<?php

declare(strict_types=1);

namespace App\Exceptions\User;

use App\Exceptions\ClientError;
use App\Models\User;
use Throwable;

class UserMonitoredArtLimitExceeded extends ClientError
{
    public function __construct(User $user, int $maxMonitoredArtCount, Throwable $previous = null)
    {
        $message = trans(
            'errors.exceeded_monitored_arts',
            ['max_arts' => $maxMonitoredArtCount]
        );

        $message .= ' : ' . $user->email;

        parent::__construct(ClientError::MAX_MONITORED_ARTS, $message, $previous);
    }
}
