<?php

declare(strict_types=1);

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;

trait HasActiveState
{
    public function getActiveStateColumn(): string
    {
        return 'is_active';
    }

    public function scopeActive(Builder $builder): void
    {
        $builder->where($this->getTable() . '.' . $this->getActiveStateColumn(), 1);
    }

    public function scopeInactive(Builder $builder): void
    {
        $builder->where($this->getTable() . '.' . $this->getActiveStateColumn(), 0);
    }

    public function activate(bool $withSave = true): self
    {
        $this->{$this->getActiveStateColumn()} = 1;

        if ($withSave) {
            $this->save();
        }

        return $this;
    }

    public function deactivate(bool $withSave = true): self
    {
        $this->{$this->getActiveStateColumn()} = 0;

        if ($withSave) {
            $this->save();
        }

        return $this;
    }

    public function toggleActiveState(bool $withSave = true): self
    {
        $this->{$this->getActiveStateColumn()} = $this->{$this->getActiveStateColumn()} ? 0 : 1;

        if ($withSave) {
            $this->save();
        }

        return $this;
    }
}
