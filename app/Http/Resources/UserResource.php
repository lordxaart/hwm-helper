<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Core\Enums\SocialProvider;
use App\Http\Resources\Core\ApiResource;
use App\Models\User;
use App\Services\Notifications\Channels\Webpush\PushSubscriptionModel;
use App\Services\Subscriptions\Entities\UserSubscription;
use App\Services\Subscriptions\SubscriptionService;
use App\Services\Telegram\TelegramApi;
use App\Services\UserFeatures\Enums\FeatureEntity;
use App\Services\UserFeatures\UserFeatureService;
use Illuminate\Http\Request;

/**
 * @property User $resource
 */
class UserResource extends ApiResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $socialProviders = [];
        $user = $this->resource;

        foreach (SocialProvider::cases() as $provider) {
            $socialProviders[] = [
                'provider' => $provider->value,
                'active' => $user->hasSocialProvider($provider),
            ];
        }

        /** @var SubscriptionService $service */
        $service = app(SubscriptionService::class);
        $subscriptions = $service->getSubscriptions($user->getId());
        /** @var UserFeatureService $featuresService */
        $featuresService = app(UserFeatureService::class);
        return $this->transformByRequest([
            'id' => $user->id,
            'email' => $user->email,
            // 'apiToken' => $user->getApiToken(),
            'socialProviders' => $socialProviders,
            'email_verified_at' => $user->email_verified_at?->format(config('app.date_format')),
            'settings' => $user->settings->toArray(),
            'features' => $user->features()->toArray(),
            'hasPassword' => !!$user->password,
            'telegramLinkWithUser' => TelegramApi::getBotLink() . '?start=' . $user->getEncryptedEmail(),
            'monitored_arts_count' => $user->monitoredArts()->withoutGlobalScope('active')->count(),
            'monitored_filters_count' => $user->filters()->count(),
            'telegramAccount' => $user->telegramAccount?->telegram_user?->toArray(),
            'notifications' => [
                'subscriptions' => [
                    'web_push' => $user->webPushSubscriptions()->get()
                        ->map(function (PushSubscriptionModel $subscription) {
                            return [
                                'endpoint' => $subscription->endpoint,
                            ];
                        })
                        ->toArray()
                ],
            ],
            'subscriptions' => array_map(fn(UserSubscription $s) => [
                ...$s->toArray(),
                'features' => $featuresService
                    ->getEntityFeatures(FeatureEntity::Subscription, (string)$s->subscription->id)
                    ->features
            ], $subscriptions),
        ], $request);
    }
}
