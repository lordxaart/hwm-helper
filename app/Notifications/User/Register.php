<?php

declare(strict_types=1);

namespace App\Notifications\User;

use App\Core\Enums\Queue;
use App\Services\Notifications\Enums\Channel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class Register extends Notification implements ShouldQueue
{
    use Queueable;

    public function __construct()
    {
        $this->queue = Queue::NOTIFICATIONS->value;
    }

    public function via(): array
    {
        return [
            Channel::MAIL->getLaravelChannelName(),
        ];
    }

    public function toMail(): MailMessage
    {
        return (new MailMessage())
            ->subject(trans('app.notifications.register.subject'))
            ->greeting(trans('app.notifications.register.greeting'))
            ->line(trans('app.notifications.register.line_1'))
            ->action(trans('app.notifications.register.action'), route('profile.index'))
            ->line(trans('app.notifications.register.line_2'))
            ->line(trans('app.notifications.register.line_3'));
    }
}
