<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V1;

use App\Actions\Art\ArtAdd;
use App\Actions\Art\ArtUpdate;
use App\Http\Requests\API\ArtStoreRequest;
use App\Http\Resources\ArtCategoryResource;
use App\Http\Resources\ArtResource;
use App\HWM\Exceptions\RequestError;
use App\HWM\Repositories\ArtRepository;
use App\HWM\Repositories\MarketCategoryRepository;
use App\Models\Art;
use App\Models\ArtCategory;
use App\Models\ArtSet;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ArtsController extends ApiController
{
    public function index()
    {
        $arts = app(ArtRepository::class)->all();

        return $this->response(ArtResource::collection($arts));
    }

    public function store(ArtStoreRequest $request)
    {
        $art = ArtAdd::run($request->input('art_id'));

        return $this->response(new ArtResource($art))->setStatusCode($art->wasRecentlyCreated ? 201 : 200);
    }

    public function updateFromOriginal(string $artId)
    {
        $art = Art::findByHwmId($artId);
        if (!$art) {
            throw (new ModelNotFoundException())->setModel(Art::class, [$artId]);
        }

        $art = ArtUpdate::run($art);

        return $this->response(new ArtResource($art));
    }

    public function categories()
    {
        return $this->response(ArtCategoryResource::collection(ArtCategory::all()));
    }

    public function marketCategories(MarketCategoryRepository $marketCategoryRepository)
    {
        return $this->response(ArtCategoryResource::collection($marketCategoryRepository->all()));
    }

    public function sets()
    {
        return $this->response(ArtCategoryResource::collection(ArtSet::all()));
    }
}
