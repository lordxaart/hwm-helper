<?php

declare(strict_types=1);

namespace App\Services\UserFeatures\Enums;

enum FeatureType: string
{
    case Bool = 'bool';
    case Number = 'number';
}
