<?php

declare(strict_types=1);

namespace App\Services\Subscriptions;

use App\Core\ValueObject\UserId;
use App\Services\Subscriptions\Entities\UserSubscription;
use App\Services\Subscriptions\Enums\Subscription;
use DateTimeImmutable;

class SubscriptionService
{
    public function __construct(private SubscriptionRepository $repository)
    {
    }

    public function getActiveSubscriptionIds(UserId $userId): array
    {
        return $this->repository->getActiveSubscriptionIds($userId);
    }

    public function getSubscriptionById(int $subId): ?UserSubscription
    {
        $row = $this->repository->getUserSubscriptionById($subId);

        return $row ? $this->mapRowToUserSubscription($row) : null;
    }

    /**
     * @return UserSubscription[]
     */
    public function getSubscriptions(UserId $userId, bool $active = true): array
    {
        $userSubs = $this->repository->getUserSubscriptions(userId: $userId, active: $active, limit: 100);

        return array_map(fn($userSub) => $this->mapRowToUserSubscription($userSub), $userSubs);
    }

    public function subscribe(UserId $userId, int $planId, bool $withTrial = true): UserSubscription
    {
        $plan = $this->repository->getPlan($planId);
        $subscription = $this->repository->getSubscription($plan->subscription_id);

        $activeIds = $this->repository->getActiveSubscriptionIds($userId);

        if (!empty($activeIds)) {
            throw new \DomainException("User already has active subscription");
        }

        $start = new DateTimeImmutable();
        $end = $start->add($plan->duration_interval)->setTime(23, 59, 59); // end of day
        $trial = $withTrial && $plan->trial_interval ? $start->add($plan->trial_interval)->setTime(23, 59, 59) : null; // end of day
        $start = $start->setTime(0,0); // start of day

        if ($trial && $trial->getTimestamp() > $end->getTimestamp()) {
            $trial = clone $end;
        }

        $this->repository->createUserSubscription(
            userId: $userId,
            subId: $subscription->id,
            planId: $plan->id,
            start: $start,
            end: $end,
            trial: $trial,
        );
        // todo: fire event
        $userSub = $this->repository->getUserSubscriptions(userId: $userId, subId: $subscription->id, active: true, limit: 1)[0] ?? null;

        return $this->mapRowToUserSubscription($userSub);
    }

    public function unsubscribe(int $userSubscriptionId): bool
    {
        // todo: fire event
        return !!$this->repository->deleteUserSubscription($userSubscriptionId);
    }

    public function updateSubscription(int $subId, DateTimeImmutable $endedAt = null, DateTimeImmutable $trialEndedAt = null): bool
    {
        if (!$endedAt && !$trialEndedAt) {
            return false;
        }

        $row = $this->repository->getUserSubscriptionById($subId);
        $sub = $this->mapRowToUserSubscription($row);
        $activeIds = $this->repository->getActiveSubscriptionIds($sub->user_id);

        if (!$sub->isActive() && !empty($activeIds)) {
            throw new \DomainException("User already has active subscription");
        }

        if ($trialEndedAt && $endedAt && $trialEndedAt->getTimestamp() > $endedAt->getTimestamp()) {
            $trialEndedAt = clone $endedAt;
        }

        if ($endedAt) {
            $endedAt = $endedAt->setTime(23, 59, 59);
        }

        if ($trialEndedAt) {
            $trialEndedAt = $trialEndedAt->setTime(23, 59, 59);
        }

        $res = $this->repository->updateUserSubscription($subId, $endedAt, $trialEndedAt);
        // todo: Уведомить польозвателя что подписка изменена

        return $res;
    }

    private function mapRowToUserSubscription($data): UserSubscription
    {
        $plan = $this->repository->getPlan($data['subscription_plan_id']);
        $subscription = $this->repository->getSubscription($data['subscription_id']);

        return new UserSubscription(
            (int)$data['id'],
            new UserId($data['user_id']),
            $subscription,
            $plan,
            new DateTimeImmutable($data['started_at']),
            new DateTimeImmutable($data['ended_at']),
            $data['trial_ended_at'] ? new DateTimeImmutable($data['trial_ended_at']) : null,
        );
    }
}
