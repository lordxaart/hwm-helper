<?php

declare(strict_types=1);

namespace App\Services\LotFilter\ValueObjects;

use App\Services\LotFilter\Enums\FilterLotField;
use App\Services\LotFilter\Enums\FilterOperator;
use Illuminate\Contracts\Support\Arrayable;
use InvalidArgumentException;
use Throwable;

class FilterTerm implements Arrayable
{
    public readonly FilterLotField $field;

    public readonly FilterOperator $operator;

    public readonly FilterTermValue $value;

    public function __construct(FilterLotField $field, FilterOperator $operator, FilterTermValue $value)
    {
        $this->field = $field;
        $this->operator = $operator;
        $this->value = $value;
    }

    /**
     * @param string[]|array[] $data
     */
    public static function parseCollectionFromArray(array $data): FilterTermCollection
    {
        $terms = [];

        foreach ($data as $item) {
            if (is_string($item)) {
                $terms[] = self::parseItemFromString($item);
            }
            if (is_array($item)) {
                $terms[] = self::parseItemFromArray($item);
            }
        }

        return new FilterTermCollection($terms);
    }

    public static function parserCollectionFromString(string $string): FilterTermCollection
    {
        $filterTerms = [];

        $pieces = explode(';', $string);

        foreach ($pieces as $filterTermString) {
            try {
                $filterTerms[] = self::parseItemFromString($filterTermString);
            } catch (Throwable $e) {
                throw new InvalidArgumentException(
                    "Cannot parse string to filter term. Correctly format: ':term; :term'. [$filterTermString]",
                    0,
                    $e
                );
            }
        }

        return (new FilterTermCollection($filterTerms))->unique();
    }

    public static function parseItemFromString(string $filterTermString): FilterTerm
    {
        try {
            [$field, $operator, $value] = explode(' ', trim($filterTermString));

            if ($operator === '=') {
                $operator = FilterOperator::EQUAL->value;
            }

            return new FilterTerm(
                FilterLotField::from($field),
                FilterOperator::from($operator),
                new FilterTermValue($value),
            );
        } catch (\Throwable $e) {
            throw new InvalidArgumentException(
                "Cannot parse string to filter term. Correctly format: ':field :operator :value'. [$filterTermString]",
                0,
                $e
            );
        }
    }

    public static function parseItemFromArray(array $term): FilterTerm
    {
        try {
            [$field, $operator, $value] = array_values($term);

            if ($operator === '=') {
                $operator = FilterOperator::EQUAL;
            }

            return new FilterTerm(
                FilterLotField::from($field),
                FilterOperator::from($operator),
                new FilterTermValue($value),
            );
        } catch (\Throwable $e) {
            throw new InvalidArgumentException(
                "Cannot parse string to filter term. Correctly format: ':field :operator :value'. [" . implode(' ', $term) . "]",
                0,
                $e
            );
        }
    }

    public function toArray(): array
    {
        return [
            'field' => $this->field->value,
            'operator' => $this->operator->value,
            'value' => $this->value->value,
        ];
    }

    public function toString(): string
    {
        return sprintf('%s %s %s', $this->field->value, $this->operator->value, $this->value->value);
    }

    public function toTransString(): string
    {
        $field = trans('app.art.columns.' . $this->field->value);

        if ($this->operator === FilterOperator::HAS) {
            return $field . ' ' . trans('app.main.' . $this->value->value ? 'yes' : 'no');
        }

        return sprintf('%s %s %s', $field, $this->operator->value, $this->value->value);
    }
}
