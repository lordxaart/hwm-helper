<?php

declare(strict_types=1);

namespace App\HWM\Entities;

use App\HWM\Entities\Interfaces\CanBeLot;
use App\HWM\Enums\EntityType;

class Skeleton extends HwmEntity implements CanBeLot
{
    public const MARKET_CATEGORY_ID = 'skeloton';

    public function getEntityType(): EntityType
    {
        return EntityType::SKELETON;
    }

    public function getMarketCategoryId(): string
    {
        return self::MARKET_CATEGORY_ID;
    }

    public function getImage(bool $realPath = false, string $size = null): string
    {
        $path = 'images/icons/hwm/other/skeloton.png';

        return !$realPath ? asset($path) : public_path($path);
    }
}
