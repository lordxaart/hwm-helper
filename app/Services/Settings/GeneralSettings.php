<?php

declare(strict_types=1);

namespace App\Services\Settings;

use App\HWM\Helpers\HwmLink;
use App\Models\Objects\UserSettings;
use Illuminate\Support\Arr;
use Spatie\LaravelSettings\Settings;

class GeneralSettings extends Settings
{
    public bool $enable_speed_log;
    public bool $enable_request_proxy;
    public float $max_execution_time_for_query_for_log;
    public string $app_full_name;
    public int $optimize_img_quality;
    public int $default_max_monitored_arts;
    public int $default_max_monitored_filters;
    public bool $enable_adsense_script;
    public int $default_lot_notify_delay;
    public string $telegram_public_channel_invite_link;

    public static function group(): string
    {
        return 'general';
    }

    public static function getFrontAppConfig(): array
    {
        $config = config('app');
        $noAccessConfig = ['key', 'cipher', 'providers', 'aliases', 'allowed_front_config'];

        foreach ($noAccessConfig as $item) {
            if (isset($config[$item])) {
                unset($config[$item]);
            }
        }

        foreach (config('app.allowed_front_config') as $key => $value) {
            if (is_string($key)) {
                $config[$key] = config($value);
            } else {
                $config[Arr::last(explode('.', $value))] = config($value);
            }
        }

        $config['game_link'] = user()?->settings(UserSettings::GAME_LINK) ?: HwmLink::BASE_LINK;

        return $config;
    }
}
