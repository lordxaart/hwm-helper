<?php

namespace Tests\Feature\API;

use App\HWM\Repositories\ElementRepository;
use App\Models\ElementPrice;

class MarketTest extends ApiTestCase
{
    const COUNT_SEED_ELEMENTS = 1000;
    const SEARCH_ART_ID = 'cold_sword2014';

    public function setUp(): void
    {
        parent::setUp();
        /** @var ElementRepository $elementRepository */
        $elementRepository = app(ElementRepository::class);
        foreach ($elementRepository->asArrayKeys() as $element) {
            ElementPrice::factory()->count(5)->create([
                'element' => $element,
            ]);
        }
    }

    /**
     * @test
     */
    public function check_elements_response()
    {
        $response = $this->getJson(self::API_ELEMENT_PRICES_URL);

        // check json structure for response
        $response->assertJsonStructure(['data' => [$this->getElementsResponseJsonStructure()]]);

        $data = collect($response->json('data'));

        $elementRepository = app(ElementRepository::class);
        $needElements = $elementRepository->asArrayKeys();
        $elements = $data->pluck('hwmId')->toArray();

        $this->assertEquals([], array_diff($needElements, $elements));
    }

    public function check_lots_response()
    {
        $response = $this->getJson(self::API_GET_LOTS_URL . '?art_id=' . self::SEARCH_ART_ID);
        $response->dump();
        // check json structure for response
        $response->assertJsonStructure($this->getLotsResponseJsonStructure());
    }

    public function getElementsResponseJsonStructure()
    {
        return [
            'hwmId',
            'title',
            'minPrice',
            'maxPrice',
            'avgPrice',
            'countLots',
            'createdAt',
        ];
    }

    public function getLotsResponseJsonStructure()
    {
        return [
            'data' => [
                [
                    'lotId',
                    'lotLink',
                    'artName',
                    'currentStrength',
                    'baseStrength',
                    'price',
                    'pricePerFight',
                    'quantity',
                    'endedAtHuman',
                    'endedAt',
                    'craft',
                    'craftJson',
                    'sellerName',
                    'sellerId',
                    'sellerLink',
                    'artImage',
                    'artUid',
                    'artCrc',
                    'artLink',
                ],
            ],
            'meta' => [
                'total',
                'avgPrice',
                'avgPricePerFight',
                'minPrice',
                'maxPrice',
                'art',
                'marketLink',
            ],
        ];
    }
}
