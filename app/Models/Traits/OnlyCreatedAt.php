<?php

declare(strict_types=1);

namespace App\Models\Traits;

trait OnlyCreatedAt
{
    public function setCreatedAtAttribute(): void
    {
        $this->attributes['created_at'] = $this->freshTimestamp();
    }

    public function setUpdatedAtAttribute(): void
    {
        // skipp this action
    }
}
