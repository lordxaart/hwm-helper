<?php

declare(strict_types=1);

namespace App\Models\Casts;

use App\Models\Objects\UserSettings;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class CastSettings implements CastsAttributes
{
    public function get($model, string $key, $value, array $attributes)
    {
        return UserSettings::parseFromJson(strval($value));
    }

    public function set($model, string $key, $value, array $attributes)
    {
        if (is_string($value)) {
            $value = UserSettings::parseFromJson($value);
        }

        if (!$value instanceof UserSettings) {
            throw new \InvalidArgumentException('Invalid value for settings, expect Settings instance');
        }

        return $value->toJson();
    }
}
