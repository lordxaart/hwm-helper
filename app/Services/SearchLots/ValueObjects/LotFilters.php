<?php

declare(strict_types=1);

namespace App\Services\SearchLots\ValueObjects;

use App\Services\SearchLots\Enums\ColumnType;
use DateTimeZone;

class LotFilters
{
    private array $filters = [];

    private DateTimeZone $timezone;

    public function __construct(array $filters, DateTimeZone $timezone = null)
    {
        $this->timezone = $timezone ?: new DateTimeZone('UTC');

        foreach ($filters as $column => $value) {
            $type = $this->getTypeForColumn((string)$column);

            if ($type) {
                $this->filters[$column] = [
                    'type' => $type,
                    'value' => $value,
                ];
            }
        }
    }

    public function getFilters(): array
    {
        return $this->filters;
    }

    public function getTimezone(): DateTimeZone
    {
        return $this->timezone;
    }

    private function getColumnTypes(): array
    {
        return [
            ColumnType::EQUAL->value => ['lot_id', 'art_uid', 'art_crc', 'entity_title'],
            ColumnType::ONE_FROM_ARRAY->value => ['lot_type', 'entity_type', 'entity_id', 'seller_id'],
            ColumnType::EXISTS->value => ['craft', 'is_buyed'],
            ColumnType::TEXT->value => ['seller_name'],
            ColumnType::NUMBER->value => [
                'current_strength',
                'base_strength',
                'price',
                'price_per_fight',
            ],
            ColumnType::DATE->value => ['started_at', 'buyed_at', 'ended_at'],
        ];
    }

    private function getTypeForColumn(string $column): ?string
    {
        foreach ($this->getColumnTypes() as $columnType => $columns) {
            if (in_array($column, $columns)) {
                return (string)$columnType;
            }
        }

        return null;
    }
}
