<?php

declare(strict_types=1);

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Throwable;
use Symfony\Component\HttpFoundation\Response;

class ClientError extends BaseException implements HttpExceptionInterface
{
    public const SOCIAL_EMAIL_SCOPE_REQUIRED = Response::HTTP_BAD_REQUEST; // 400
    public const SOCIAL_INVALID_STATE = Response::HTTP_INTERNAL_SERVER_ERROR; // 500
    public const USER_IS_DELETED = Response::HTTP_FORBIDDEN;
    public const USER_IS_BLOCKED = Response::HTTP_FORBIDDEN;
    public const MAX_MONITORED_ARTS = Response::HTTP_FORBIDDEN;
    public const MAX_FILTERS = Response::HTTP_FORBIDDEN;

    protected int $statusCode;

    /** @var array */
    protected array $headers = [];

    public function __construct(int $statusCode, string $message = "", Throwable $previous = null, int $code = 0)
    {
        $this->statusCode = $statusCode;

        parent::__construct($message, $code, $previous);
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }
}
