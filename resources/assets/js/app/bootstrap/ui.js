import Vue from 'vue';
// Vue bootstrap plugins
import {
    NavbarPlugin,
    FormSelectPlugin,
    FormCheckboxPlugin,
    FormInputPlugin,
    ModalPlugin,
    ButtonGroupPlugin,
    AlertPlugin,
    BCard,
    VBToggle,
    CollapsePlugin,
    ProgressPlugin,
    TabsPlugin,
} from 'bootstrap-vue';
// Vue tooltip
import VTooltip from 'v-tooltip';

// Tooltip
Vue.use(VTooltip, {
    defaultTrigger: window.innerWidth > 992 ? 'hover' : 'click',
});

// Bootstrap Vue
Vue.use(NavbarPlugin);
Vue.use(FormSelectPlugin);
Vue.use(FormCheckboxPlugin);
Vue.use(FormInputPlugin);
Vue.use(ModalPlugin);
Vue.use(ButtonGroupPlugin);
Vue.use(AlertPlugin);
Vue.use(CollapsePlugin);
Vue.use(ProgressPlugin);
Vue.use(TabsPlugin);
Vue.component('b-card', BCard);
Vue.directive('b-toggle', VBToggle);

// CUSTOM COMPONENTS
// Automatically imports all the UI modules. globalComponents/UI/Icon = <icon>
const requireModule = require.context('JS/app/components/ui', false, /\.vue$/);

requireModule.keys().forEach((filename) => {
    const component = requireModule(filename).default || requireModule(filename);

    Vue.component(filename.replace(/(\.\/|\.vue)/g, ''), component);
});
