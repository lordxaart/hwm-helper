<a href="#" data-toggle="tooltip" title="Refresh from original data" class="btn btn-sm btn-link refresh_from_original" data-art-id="{{ $entry->hwm_id }}">
    <i class="{{ icon('refresh') }}"></i>
</a>