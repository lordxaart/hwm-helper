<?php

declare(strict_types=1);

return [
    'telegram_invite_link' => [
        'important' => 'Important',
        'p' => 'A channel has appeared in Telegram where notifications about new artifacts, new functions and important news regarding service updates will be broadcast.',
        'thanks' => 'I will be grateful for sharing',
        'join' => 'Join',
    ]
];
