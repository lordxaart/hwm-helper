<?php

declare(strict_types=1);

namespace App\Events\ArtMonitor;

use App\Models\MonitoredArt;
use Illuminate\Foundation\Events\Dispatchable;

class AddArtToMonitorEvent
{
    use Dispatchable;

    public MonitoredArt $art;

    public function __construct(MonitoredArt $art)
    {
        $this->art = $art;
    }
}
