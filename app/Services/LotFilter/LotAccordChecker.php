<?php

declare(strict_types=1);

namespace App\Services\LotFilter;

use App\HWM\Entities\BaseLot;
use App\HWM\Entities\Lot;
use App\Models\LotFilter;
use App\Models\Objects\UserSettings;
use App\Models\User;
use App\Notifications\User\AccordedFilters;
use App\Services\LotFilter\ValueObjects\FilterTerm;
use App\Services\LotFilter\ValueObjects\FilterTermValue;
use Illuminate\Database\Eloquent\Collection;
use InvalidArgumentException;

class LotAccordChecker
{
    public function handleFiltersForLot(Lot $lot): void
    {
        $filtersQuery = LotFilter::query()
            ->where('lot_type', $lot->lot_type)
            ->where('entity_type', $lot->entity_type)
            ->where('is_active', 1)
            ->orderBy('order', 'desc')
            ->orderBy('created_at', 'desc');

        if (!$filtersQuery->exists()) {
            return;
        }

        $accordedFilters = [];

        $filtersQuery->with('user')->chunk(1000, function (Collection $filters) use ($lot, &$accordedFilters) {
            /** @var LotFilter $filter */
            foreach ($filters as $filter) {
                if ($this->isLotAccordedToFilter($lot, $filter)) {
                    if (!isset($accordedFilters[$filter->user_id])) {
                        $accordedFilters[$filter->user_id] = [
                            'user' => $filter->user,
                            'filters' => [],
                        ];
                    }

                    $accordedFilters[$filter->user_id]['filters'][] = $filter;
                }
            }
        });
        foreach ($accordedFilters as $item) {
            /** @var User $user */
            $user = $item['user'];
            /** @var LotFilter[] $filters */
            $filters = $item['filters'];

            $delay = $user->features()->lot_filters_notify_delay;
            $notification = new AccordedFilters($lot, $filters);

            if (!$delay && $user->settings(UserSettings::CUSTOM_NOTIFY_DELAY)) {
                 $delay = rand(30, 60);
            }

            if ($delay) {
                $notification->delay(now()->addSeconds($delay));
            }

            $user->notify($notification);
        }
    }

    public function isLotAccordedToFilter(BaseLot $lot, LotFilter $filter): bool
    {
        if ($lot->lot_type !== $filter->lot_type->value) {
            return false;
        }

        if ($lot->entity_type !== $filter->entity_type->value) {
            return false;
        }

        if ($filter->entity_id && $filter->entity_id !== $lot->entity_id) {
            return false;
        }

        /** @var FilterTerm $term */
        foreach ($filter->terms as $term) {
            if (!$this->isLotAccordedToFilterTerm($lot, $term)) {
                return false;
            }
        }


        return true;
    }

    private function isLotAccordedToFilterTerm(BaseLot $lot, FilterTerm $filterTerm): bool
    {
        if (!property_exists($lot, $filterTerm->field->value)) {
            throw new InvalidArgumentException(sprintf('Not found field %s in lot %s', $filterTerm->field->value, $lot::class));
        }

        $currentValue = new FilterTermValue($lot->{$filterTerm->field->value});
        $expectedValue = $filterTerm->value;

        return $filterTerm->operator->compareValue($currentValue->value, $expectedValue->value);
    }
}
