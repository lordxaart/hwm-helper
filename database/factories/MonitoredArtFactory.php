<?php

namespace Database\Factories;

use App\HWM\Repositories\ArtRepository;
use App\Models\MonitoredArt;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class MonitoredArtFactory extends Factory
{
    /** @var string */
    protected $model = MonitoredArt::class;

    /** @var array */
    protected static $arts = [];

    public function configure()
    {
        if (!count(self::$arts)) {
            self::$arts = app(ArtRepository::class)->asArrayKeys();
        }

        return $this;
    }

    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'art_hwm_id' => $this->faker->randomElement(self::$arts),
            'is_active' => 1,
        ];
    }
}
