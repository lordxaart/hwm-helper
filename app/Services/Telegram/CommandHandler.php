<?php

declare(strict_types=1);

namespace App\Services\Telegram;

use App\Services\Telegram\Commands\DisconnectCommand;
use App\Services\Telegram\Commands\StartCommand;
use App\Services\Telegram\Entities\Update;

class CommandHandler
{
    private array $handlers;

    public function __construct()
    {
        $this->handlers = [
            StartCommand::COMMAND => StartCommand::class,
            DisconnectCommand::COMMAND => DisconnectCommand::class,
        ];
    }

    public function handle(string $command, Update $update): void
    {
        if (!isset($this->handlers[$command])) {
            throw new \InvalidArgumentException("Not found handler $command for command");
        }

        /** @var TelegramCommand $handler */
        $handler = new $this->handlers[$command]();
        $handler->handle($update);
    }
}
