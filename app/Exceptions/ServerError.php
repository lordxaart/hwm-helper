<?php

declare(strict_types=1);

namespace App\Exceptions;

use Throwable;

class ServerError extends BaseException
{
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        $message = $message ?: trans('errors.500');

        parent::__construct($message, $code, $previous);
    }
}
