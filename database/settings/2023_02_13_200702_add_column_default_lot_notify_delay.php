<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

class AddColumnDefaultLotNotifyDelay extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('general.default_lot_notify_delay', 60);
    }
}
