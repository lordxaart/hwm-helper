<?php

declare(strict_types=1);

namespace App\Services\ImageOptimize;

use App\Exceptions\ServerErrors\ImageOptimizeError;
use Exception;
use Spatie\ImageOptimizer\Optimizer;

class ImageOptimize
{
    public const QUICKLY_TYPE = 'quickly';
    public const BEST_TYPE = 'best';

    public int $quality = 85;

    protected array $optimizers = [
        'spatie' => SpatieOptimize::class,
        'tiny' => TinyOptimize::class,
    ];

    protected string $defaultType = 'spatie';

    protected string $bestType = 'tiny';

    protected string $quicklyType = 'spatie';

    protected bool $onQueue = false;

    protected bool $debug = false;

    protected float $executeTime;

    protected int $originalSize;

    protected int $resultSize;

    public function __construct(array $options = [])
    {
        $this->setOptions($options);
    }

    public function setOptions(array $options = []): self
    {
        foreach ($options as $key => $value) {
            if (property_exists($this, (string)$key)) {
                $this->$key = $value;
            }
        }

        return $this;
    }

    public function withQueue(): self
    {
        $this->onQueue = true;

        return $this;
    }

    public function withInspect(): self
    {
        $this->debug = true;

        return $this;
    }

    /**
     * @throws ImageOptimizeError
     */
    public function optimize(string $path, string $type = null): bool
    {
        $startTime = microtime(true);

        if ($type === self::BEST_TYPE) {
            $type = $this->bestType;
        } elseif ($type === self::QUICKLY_TYPE) {
            $type = $this->quicklyType;
        }

        // Get optimizer instance
        /** @var OptimizerInterface $optimizer */
        $optimizer = new $this->optimizers[$type ?: $this->defaultType]();

        // Add job if detect queue
        if ($this->onQueue) {
            dispatch(new \App\Jobs\ImageOptimize($optimizer, $path, $this->quality));
            $this->onQueue = false;

            return true;
        }

        if ($this->debug) {
            $this->originalSize = filesize($path) ?: 0;
        }

        try {
            $res = $this->handleOptimizer($optimizer, $path);
        } catch (Exception $e) {
            if (!$e instanceof ImageOptimizeError) {
                $e = new ImageOptimizeError($e->getMessage());
            }
            throw $e;
        }

        if ($this->debug) {
            $this->resultSize = filesize($path) ?: 0;
            $this->executeTime = microtime(true) - $startTime;
            $savedPercents = number_format(
                ($this->originalSize - $this->resultSize) * 100 / $this->originalSize,
                2
            );

            \Log::info('Image optimize inspect', [
                'optimizer' => $type,
                'originalSize' => $this->originalSize,
                'resultSize' => $this->resultSize,
                'savedInBytes' => $this->originalSize - $this->resultSize,
                'savedInPercents' => $savedPercents  . '%',
                'executeTime' => $this->executeTime,
                'quality' => $this->quality,
                'file' => $path,
            ]);
        }

        return $res ?: false;
    }

    protected function handleOptimizer(OptimizerInterface $optimizer, string $path): bool
    {
        return $optimizer->optimize($path, $this->quality);
    }
}
