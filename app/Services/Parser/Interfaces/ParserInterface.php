<?php

declare(strict_types=1);

namespace App\Services\Parser\Interfaces;

interface ParserInterface
{
    public function run(): void;
}
