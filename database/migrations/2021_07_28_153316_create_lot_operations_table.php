<?php

use App\HWM\Objects\LotOperation;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLotOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lot_operations', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->unsignedInteger('lot_id')->index();
            $table->enum('type', [
                LotOperation::OPERATION_CREATE,
                LotOperation::OPERATION_SALE,
                LotOperation::OPERATION_RATE,
                LotOperation::OPERATION_END,
            ]);
            $table->timestamp('time');
            $table->unsignedSmallInteger('buyed_quantity')->nullable();
            $table->unsignedSmallInteger('current_quantity')->nullable();
            $table->unsignedInteger('current_price')->nullable();
            $table->unsignedInteger('rate_diff')->nullable();
            $table->unsignedInteger('buyer_id')->nullable();
            $table->string('buyer_name')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
