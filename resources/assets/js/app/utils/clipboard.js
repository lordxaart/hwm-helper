import copy from 'copy-to-clipboard';
import notify from 'JS/app/utils/notify';
import i18n from 'JS/app/utils/localization';

const clipboard = (text, message = '') => {
    copy(text);
    notify.info(message, i18n.t('app.main.copied'), 'clipboard');
};

export default clipboard;
