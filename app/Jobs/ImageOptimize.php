<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Services\ImageOptimize\OptimizerInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImageOptimize implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public OptimizerInterface $optimizer;

    public string $path;

    public int $quality;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(OptimizerInterface $optimizer, string $path, int $quality)
    {
        $this->optimizer = $optimizer;
        $this->path = $path;
        $this->quality = $quality;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->optimizer->optimize($this->path, $this->quality);
    }
}
