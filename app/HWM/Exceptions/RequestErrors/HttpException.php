<?php

declare(strict_types=1);

namespace App\HWM\Exceptions\RequestErrors;

use App\HWM\Exceptions\RequestError;

class HttpException extends RequestError
{
    //
}
