<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Core\Enums\Cookie;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie as CookieQueue;

class SetUserId
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        $user = $request->user();

        $cookieValue = $request->cookie(Cookie::USERID->value);

        $userid = $user ? $user->email : $this->getRandomUserid();

        if (!$cookieValue || ($user && $cookieValue !== $userid)) {
            // Set new user id
            CookieQueue::queue(Cookie::USERID->value, $userid, time() + (3600 * 24 * 365));
        }

        // share user id to config
        config(['app.userid' => $userid]);

        return $next($request);
    }

    private function getRandomUserid(): string
    {
        return dechex(crc32(uniqid() . time()));
    }
}
