<?php

declare(strict_types=1);

namespace App\Providers;

use App\HWM\Exceptions\RequestErrors\LoginException;
use App\HWM\HwmClient;
use App\HWM\Repositories\ArtCategoryRepository;
use App\HWM\Repositories\ArtSetRepository;
use App\HWM\Repositories\ElementRepository;
use App\HWM\Repositories\FractionRepository;
use App\HWM\Repositories\GuildRepository;
use App\HWM\Repositories\MarketCategoryRepository;
use App\HWM\Repositories\ResourceRepository;
use App\HWM\Repositories\SkillRepository;
use App\HWM\Request\HwmRequest;
use App\Models\HwmBotAccount;
use Illuminate\Support\ServiceProvider;

class HwmServiceProvider extends ServiceProvider
{
    public array $singletons = [
        MarketCategoryRepository::class => MarketCategoryRepository::class,
        ArtCategoryRepository::class => ArtCategoryRepository::class,
        ArtSetRepository::class => ArtSetRepository::class,
        ElementRepository::class => ElementRepository::class,
        FractionRepository::class => FractionRepository::class,
        GuildRepository::class => GuildRepository::class,
        ResourceRepository::class => ResourceRepository::class,
        SkillRepository::class => SkillRepository::class,
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(HwmRequest::class, function () {
            return new HwmRequest(config('app.fallback_locale'), 3, settings()->enable_request_proxy);
        });

        $this->app->singleton('HwmDefaultClient', function () {
            $botAccount = HwmBotAccount::getRandomForTarget(IS_CLI ? HwmBotAccount::TARGET_CLI : HwmBotAccount::TARGET_WEB);
            if (!$botAccount) {
                throw new LoginException('Not found HWM Bot for ' . (IS_CLI ? 'CLI' : 'WEB'));
            }

            return new HwmClient($botAccount, $this->app->get(HwmRequest::class));
        });
    }
}
