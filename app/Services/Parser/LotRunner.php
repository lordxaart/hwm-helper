<?php

declare(strict_types=1);

namespace App\Services\Parser;

use App\Actions\Lot\LotAdd;
use App\Models\Lot;
use App\Services\Parser\Interfaces\RunnerInterface;
use Illuminate\Support\Arr;

class LotRunner implements RunnerInterface
{
    public const TYPE_NEW = 'new';
    public const TYPE_REVIEW = 'review';

    protected string $type;
    protected int $lotId;
    protected int $attempts = 0;
    protected float $initTime = 0;
    /** @var \Throwable[]  */
    protected array $errors = [];
    public bool $success = false;
    public bool $fileEvent;

    public function __construct(string $type, int $lotId, bool $fireEvent = true)
    {
        if (!in_array($type, [self::TYPE_NEW, self::TYPE_REVIEW])) {
            throw new \TypeError("Unavailable type $type");
        }

        $this->type = $type;
        $this->lotId = $lotId;
        $this->initTime = microtime(true);
        $this->fileEvent = $fireEvent;
    }

    public function handle(): ?Lot
    {
        return match ($this->type) {
            self::TYPE_NEW => LotAdd::run($this->lotId, null, $this->fileEvent),
            default => null,
        };
    }

    /**
     * @return Lot|null
     * @throws \Throwable
     */
    public function run(): ?Lot
    {
        if (!$this->initTime) {
            $this->initTime = microtime(true);
        }

        $this->attempts++;

        try {
            $res = $this->handle();
            $this->success = true;

            return $res;
        } catch (\Throwable $e) {
            $this->errors[] = $e;
            throw $e;
        }
    }

    public function getExecuteTime(): float
    {
        return microtime(true) - $this->initTime;
    }

    public function getLastError(): ?\Throwable
    {
        return count($this->errors) ? Arr::last($this->errors) : null;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function hasError(): bool
    {
        return count($this->errors) > 0;
    }

    public function getRunAttempts(): int
    {
        return $this->attempts;
    }

    public function getLotId(): int
    {
        return $this->lotId;
    }
}
