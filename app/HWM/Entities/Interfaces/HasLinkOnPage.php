<?php

declare(strict_types=1);

namespace App\HWM\Entities\Interfaces;

interface HasLinkOnPage
{
    public function getHwmLink(): string;
}
