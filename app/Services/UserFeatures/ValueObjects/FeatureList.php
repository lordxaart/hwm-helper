<?php

declare(strict_types=1);

namespace App\Services\UserFeatures\ValueObjects;

use App\Services\UserFeatures\Enums\Feature;
use Illuminate\Contracts\Support\Arrayable;

/**
 * @property bool $is_show_ads
 * @property bool $can_edit_game_settings
 * @property int $max_monitored_arts
 * @property int $max_monitored_filters
 * @property int $lot_filters_notify_delay
 */
class FeatureList implements Arrayable
{
    /** @var array<string, FeatureValue> */
    public readonly array $features;

    public function __construct(array $features)
    {
        $this->features = $features;

        foreach (Feature::cases() as $feature) {
            if (!array_key_exists($feature->value, $this->features)) {
                throw new \InvalidArgumentException(sprintf('Miss feature [%s] in arguments: %s', $feature->value, implode(', ', array_keys($features))));
            }
        }
    }

    public function __get(string $name)
    {
        if (isset($this->features[$name])) {
            return $this->features[$name]->getValue();
        }

        if (property_exists($this, $name)) {
            return $this->$name;
        }

        throw new \InvalidArgumentException("Property [$name] not found");
    }

    public function get(Feature $feature): mixed
    {
        return $this->{$feature->value};
    }

    public function toArray(): array
    {
        return array_map(fn ($item) => $item->getValue(), $this->features);
    }
}
