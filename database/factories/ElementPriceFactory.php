<?php

namespace Database\Factories;

use App\HWM\Repositories\ElementRepository;
use App\Models\ElementPrice;
use Illuminate\Database\Eloquent\Factories\Factory;

class ElementPriceFactory extends Factory
{
    protected $model = ElementPrice::class;

    /** @var array */
    public $elements;
    /** @var int[]  */
    protected $rangeCountLots = [10, 1100];
    /** @var int[]  */
    protected $maxPrice = 999999;
    /** @var int[]  */
    protected $minPrice = 10;
    /** @var int[]  */
    protected $rangeAvgPrice = [200, 9999];

    public function configure()
    {
        $elementRepository = app(ElementRepository::class);
        $this->elements = $elementRepository->asArrayKeys();

        return $this;
    }

    public function definition()
    {
        $this->configure();

        $avgPrice = mt_rand($this->rangeAvgPrice[0], $this->rangeAvgPrice[1]);
        $minPrice = mt_rand(mt_rand($this->minPrice, $avgPrice), $avgPrice);
        $maxPrice = mt_rand($avgPrice, mt_rand($avgPrice, $this->maxPrice));

        return [
            'element' => $this->faker->randomElement($this->elements),
            'avg_price' => $avgPrice,
            'min_price' => $minPrice,
            'max_price' => $maxPrice,
            'count_lots' => mt_rand($this->rangeCountLots[0], $this->rangeCountLots[1]),
            'created_at' => $this->faker->dateTimeBetween('-3 months'),
            'updated_at' => now()->format('Y-m-d H:i:s'),
        ];
    }
}
