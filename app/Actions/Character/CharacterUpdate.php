<?php

declare(strict_types=1);

namespace App\Actions\Character;

use App\Events\HWM\CharacterUpdatedEvent;
use App\HWM\HwmClient;
use App\Models\Character;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Lorisleiva\Actions\Concerns\AsAction;

/**
 * @property Character|null $character
 */
class CharacterUpdate
{
    use AsAction;

    public string $commandSignature = 'hwm:character:update {login : Character ID (or login)}';

    public string $commandDescription = 'Update character by :login';

    public function asCommand(Command $command): int
    {
        $character = $this->handle(strval($command->argument('login')));

        $command->info("Character $character->login was successfully updated");

        return 0;
    }

    public function handle(Character|string $character): Character
    {
        $character = $character instanceof Character ? $character : Character::findByKey($character);

        if (!$character) {
            throw new ModelNotFoundException(Character::class);
        }

        $entity = app(HwmClient::class)->getCharacterInfo($character->hwm_id);

        $character->update($entity->only(
            'login',
            'level',
            'status',
            'experience',
            'resources',
            'guilds',
            'fractions',
        )->toArray());

        if ($character->wasChanged()) {
            event(new CharacterUpdatedEvent($character));
        }

        return $character;
    }
}
