<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Http\Resources\Core\ApiResource;
use App\HWM\Enums\EntityType;
use App\HWM\Enums\LotType;
use App\Models\MonitoredArt;
use App\Services\LotFilter\Repositories\LotFiltersRepository;
use Illuminate\Http\Request;

/**
 * @property MonitoredArt $resource
 */
class MonitoredArtResource extends ApiResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $artMonitored = $this->resource;

        return $this->transformByRequest([
            'hwmId' => $artMonitored->art_hwm_id,
            'isActive' => $artMonitored->is_active,
            'createdAt' => $artMonitored->created_at?->format(config('app.date_format')),
            'updatedAt' => $artMonitored->updated_at?->format(config('app.date_format')),
            'art' => $this->when(in_array('art', $this->includes), fn() => new ArtResource($artMonitored->art)),
            'filters' => $this->when(in_array('filters', $this->includes), function () use ($artMonitored) {
                $filters = app(LotFiltersRepository::class)->findAllByUserIdAndLotEntity(
                    $artMonitored->user->getId(),
                    LotType::SALE,
                    EntityType::ART,
                    $artMonitored->art_hwm_id,
                );

                return LotFilterResource::collection($filters);
            }),
        ], $request);
    }
}
