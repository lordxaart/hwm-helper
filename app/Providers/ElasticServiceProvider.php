<?php

declare(strict_types=1);

namespace App\Providers;

use App\Components\Elastic\ElasticClient;
use App\Jobs\Lot\IndexLotToElasticSearch;
use App\Services\SearchLots\Repositories\LotRepository;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class ElasticServiceProvider extends ServiceProvider
{
    public function register(): void
    {
    }

    public function boot(): void
    {
        $this->app->singleton(ElasticClient::class, function (Application $app) {
            return new ElasticClient(
                [config('services.elastic.hosts')],
                config('services.elastic.user'),
                config('services.elastic.password'),
            );
        });

        $this->app
            ->bindMethod(
                [IndexLotToElasticSearch::class, 'handle'],
                function (IndexLotToElasticSearch $job, Application $app) {
                    return $job->handle($app->get(LotRepository::class));
                },
            );
    }
}
