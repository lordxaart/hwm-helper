<?php

declare(strict_types=1);

namespace App\Services\SearchLots\ValueObjects\Aggregation;

use App\HWM\Enums\EntityType;
use App\ValueObjects\DateRange;

class AggregationsByEntityRequest
{
    // see https://www.elastic.co/guide/en/elasticsearch/reference/7.17/search-aggregations-bucket-datehistogram-aggregation.html#calendar_intervals
    public const AVAILABLE_INTERVALS = ['1h','1d','1w', '1M', '1q', '1y'];

    public readonly EntityType $entityType;
    public readonly DateRange $dateRange;
    /**
     * @var AggregationType[]
     */
    public readonly array $metrics;
    public readonly string $interval;
    public readonly string|null $entityId;

    public function __construct(EntityType $entityType, DateRange $dateRange, array $metrics, string $interval, string $entityId = null)
    {
        $this->entityType = $entityType;
        $this->dateRange = $dateRange;

        $this->validateMetrics($metrics);
        $this->metrics = $metrics;

        $this->validateInterval($interval);
        $this->interval = $interval;
        $this->entityId = $entityId;
    }

    private function validateMetrics(array $metrics): void
    {
        foreach ($metrics as $metric) {
            if (!($metric instanceof AggregationType)) {
                throw new \InvalidArgumentException('Each metric must be instance of AggregationType');
            }
        }
    }

    private function validateInterval(string $interval): void
    {
        if (!in_array($interval, self::AVAILABLE_INTERVALS)) {
            throw new \InvalidArgumentException(sprintf('Interval [%s] has not available value.', $interval));
        }
    }
}
