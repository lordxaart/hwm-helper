<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnArtIdToArtMonitoredFilters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('art_monitored_filters', function (Blueprint $table) {
            $table->string('art_hwm_id', config('hwm.rules.max_hwm_id_length'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('art_monitored_filters', function (Blueprint $table) {
            $table->removeColumn('art_hwm_id');
        });
    }
}
