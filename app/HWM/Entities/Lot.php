<?php

declare(strict_types=1);

namespace App\HWM\Entities;

use App\HWM\Objects\LotOperation;
use Carbon\Carbon;

class Lot extends BaseLot
{
    public Carbon $started_at;
    public ?Carbon $buyed_at;
    public ?Carbon $updated_at = null;
    public array $operations;
    public bool $is_parsed = false;
    public ?int $duration = null;

    public function __construct(array $parameters = [])
    {
        if (isset($parameters['started_at'])) {
            $parameters['started_at'] = Carbon::parse($parameters['started_at']);
        }

        if (isset($parameters['buyed_at'])) {
            $parameters['buyed_at'] = Carbon::parse($parameters['buyed_at']);
        }

        if (isset($parameters['updated_at'])) {
            $parameters['updated_at'] = Carbon::parse($parameters['updated_at']);
        } else {
            $parameters['updated_at'] = Carbon::now();
        }

        if (isset($parameters['operations'])) {
            foreach ($parameters['operations'] as $index => $operation) {
                if (!$operation instanceof LotOperation) {
                    $parameters['operations'][$index] = new LotOperation($operation);
                }
            }
        } else {
            $parameters['operations'] = [];
        }

        parent::__construct($parameters);

        $this->duration = $this->getDuration();
    }

    public function isActive(): bool
    {
        return !$this->buyed_at && now()->lt($this->ended_at);
    }

    public function getDuration(): int
    {
        if (!$this->started_at || !$this->ended_at) {
            return 0;
        }

        $start = $this->started_at->copy();
        $start->second = 0;
        $end = $this->ended_at->copy();
        $end->second = 0;

        return $end->diffInMinutes($start);
    }

    public function toArray(): array
    {
        $array = parent::toArray();
        unset($array['title']);

        $array['started_at'] = $this->started_at->format(DATE_FORMAT);
        $array['buyed_at'] = $this->buyed_at?->format(DATE_FORMAT);
        $array['ended_at'] = $this->ended_at->format(DATE_FORMAT);

        return $array;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }
}
