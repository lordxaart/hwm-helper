<?php

declare(strict_types=1);

namespace App\Services\UserFeatures;

use App\Core\ValueObject\UserId;
use App\Services\Subscriptions\SubscriptionService;
use App\Services\UserFeatures\Enums\Feature;
use App\Services\UserFeatures\Enums\FeatureEntity;
use App\Services\UserFeatures\ValueObjects\FeatureValue;
use App\Services\UserFeatures\ValueObjects\FeatureList;

class UserFeatureService
{
    /**
     * @var FeatureList[]
     */
    private array $runtimeFeatures = [];

    public function __construct(private UserFeatureRepository $repository, private SubscriptionService $subscriptionService)
    {
    }

    public function getUserFeatures(UserId $userId): FeatureList
    {
        if (isset($this->runtimeFeatures[$userId->toString()])) {
            return $this->runtimeFeatures[$userId->toString()];
        }

        // return from runtime if already set
        $defaultValues = $this->repository->getFeatureList();
        $userValues = $this->repository->getFeatureValuesList(FeatureEntity::User, $userId->toString());
        // get first active subscription
        $subValues = [];
        $subIds = array_reverse($this->subscriptionService->getActiveSubscriptionIds($userId));
        foreach ($subIds as $subId) {
            $values = $this->repository->getFeatureValuesList(FeatureEntity::Subscription, (string)$subId);
            if ($values) {
                $subValues = $values;
                break;
            }
        }

        $features = [];
        foreach (Feature::cases() as $feature) {
            $value = null;
            $entity = FeatureEntity::User->value;

            if (isset($subValues[$feature->value])) { // Set value from subscription if exists
                $value = $subValues[$feature->value]['value'];
                $entity = $subValues[$feature->value]['entity'];
            } elseif (isset($userValues[$feature->value])) { // else set value from user if exists
                $value = $userValues[$feature->value]['value'];
                $entity = $userValues[$feature->value]['entity'];
            }

            $default = $defaultValues[$feature->value];
            $features[$feature->value] = new FeatureValue(
                key: $default['id'],
                type: $default['type'],
                default: $default['default_value'],
                value: $value,
                entity: $entity,
            );
        }

        $list = new FeatureList($features);

        $this->runtimeFeatures[$userId->toString()] = $list;

        return $list;
    }

    public function getEntityFeatures(FeatureEntity $entity, string $entityId): FeatureList
    {
        $rows = $this->repository->getFeatureValuesList($entity, $entityId);
        $defaultValues = $this->repository->getFeatureList();
        return new FeatureList(array_map(fn($row) => new FeatureValue(
            key: $row['feature_id'],
            type: $defaultValues[$row['feature_id']]['type'],
            default: $defaultValues[$row['feature_id']]['default_value'],
            value: $row['value'],
            entity: $row['entity'],
        ), $rows));
    }

    public function setUserFeature(UserId $userId, Feature $feature, mixed $value): void
    {
        $exists = $this->repository->getFeatureValueByEntity(FeatureEntity::User, $userId->toString(), $feature);
        $default = $this->repository->getFeatureList()[$feature->value];
        $value = FeatureValue::convertValueToType($value, $feature->getType());

        // miss for default value
        if (!$exists && $default['default_value'] === $value) {
            return;
        }

        if ($exists && $exists->value === $value) {
            return;
        }

        $this->repository->setFeatureValue(FeatureEntity::User, $userId->toString(), $feature, $value);

        if (isset($this->runtimeFeatures[$userId->toString()])) {
            unset($this->runtimeFeatures[$userId->toString()]);
        }
        // todo: add notify user about new features
    }

    public function setSubscriptionFeatures(string $subId, array $features): void
    {
        foreach ($features as $feature => $value) {
            $this->repository->setFeatureValue(
                FeatureEntity::Subscription,
                $subId,
                Feature::from($feature),
                FeatureValue::convertValueToType($value, Feature::from($feature)->getType()),
            );
        }
    }
}
