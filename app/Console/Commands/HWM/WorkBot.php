<?php

declare(strict_types=1);

namespace App\Console\Commands\HWM;

use App\Console\Commands\Command;
use App\Models\Character;
use App\Models\Work;
use Exception;

class WorkBot extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hwm:work';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run hwm work bot';

    /**
     * @throws Exception
     */
    public function handle(): void
    {
        Character::where('auto_work', 1)->each(function (Character $character) {
            $this->info('Start working for ' . $character->login);

            if (Work::goToWork($character)) {
                $this->log('Success work for ' . $character->login);
            } else {
                $this->log('Failed work for ' . $character->login);
            }
        });
    }
}
