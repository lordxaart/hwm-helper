<?php

declare(strict_types=1);

namespace App\Http\Requests\API;

use App\Components\Validators\Rules\MaxTermsInFilter;
use App\Components\Validators\Rules\ValidFilterValue;
use Illuminate\Foundation\Http\FormRequest;

class FilterUpdate extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'terms' => [
                'nullable',
                new ValidFilterValue(),
                new MaxTermsInFilter(5),
            ],
            'is_active' => 'nullable|boolean',
        ];
    }
}
