<?php

declare(strict_types=1);

namespace App\HWM\BotApi;

use Exception;
use Illuminate\Support\Facades\Http;

class BaseBotApi
{
    protected string $apiUrl = '127.0.0.1:3000';

    private int $timeout = 30;

    private array $retry = [3, 100];

    protected function get(string $path, array $params = []): array
    {
        return $this->request('GET', $path, $params);
    }

    protected function post(string $path, array $params = []): array
    {
        return $this->request('POST', $path, $params);
    }

    protected function request(string $method, string $path, array $params = []): array
    {
        if ($method === 'GET' && count($params)) {
            $path .= '?' . http_build_query($params);
            $params = [];
        }

        try {
            return Http::timeout(30)
                ->retry($this->retry[0], $this->retry[1])
                ->send($method, $this->getUrl($path), $params)
                ->json();
        } catch (Exception) {
            return [];
        }
    }

    protected function getUrl(string $path): string
    {
        if ($path[0] !== '/') {
            $path = '/' . $path;
        }

        return $this->apiUrl . $path;
    }

    public function setTimeout(int $seconds): self
    {
        $this->timeout = $seconds > 0 && $seconds < 100000 ? $seconds : $this->timeout;

        return $this;
    }

    public function setRetry(int $times, int $delayMilliseconds): self
    {
        $this->retry = [$times, $delayMilliseconds];

        return $this;
    }
}
