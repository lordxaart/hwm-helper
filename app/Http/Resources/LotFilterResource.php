<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Http\Resources\Core\ApiResource;
use App\HWM\Entities\Certificate;
use App\HWM\Entities\Element;
use App\HWM\Entities\House;
use App\HWM\Entities\MarketLot;
use App\HWM\Entities\ObjectShare;
use App\HWM\Entities\Res;
use App\HWM\Entities\Skeleton;
use App\HWM\Enums\EntityType;
use App\HWM\Helpers\HwmLink;
use App\HWM\Repositories\ArtRepository;
use App\Models\LotFilter;
use App\Models\MarketCategory;
use App\Services\LotFilter\Enums\FilterLotField;
use App\Services\LotFilter\Enums\FilterOperator;
use App\Services\LotFilter\ValueObjects\FilterTerm;
use App\Services\Settings\User\GameLinkDecorator;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;

/**
 * @property LotFilter $resource
 */
class LotFilterResource extends ApiResource
{
    private array $notifications = [];

    public function setFilterNotifications(array $notifications): void
    {
        $this->notifications = $notifications;
    }

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $sellers = [];
        try {
            /** @var FilterTerm $term */
            foreach ($this->terms->toArray() as $term) {
                if ($term->field === FilterLotField::SELLER_ID && $term->operator === FilterOperator::EQUAL) {
                    $character = hwmClient()->getCharacterInfo((int)$term->value->value);
                    $sellers['_' . $term->value->value] = $character?->login ?: UNDEFINED;
                }
            }
        } catch (\Throwable $exception) {
            \Log::error($exception->getMessage());
        }

        return $this->transformByRequest([
            'id' => $this->id,
            'lotType' => $this->lot_type->value,
            'entityType' => $this->entity_type->value,
            'entityId' => $this->entity_id,
            'entity' => $this->getFilterEntity($this->resource),
            'terms' => array_map(fn (FilterTerm $term) => $term->toArray(), $this->terms->toArray()),
            'isActive' => $this->is_active,
            'notifications' => $this->notifications,
            'sellers' => $sellers,
        ], $request);
    }

    private function getFilterEntity(LotFilter $filter): array
    {
        $entity = [
            'title' => '',
            'image' => '',
            'hwmMarketLink' => '',
        ];

        if (!$filter->entity_id) {
            switch ($filter->entity_type) {
                case EntityType::ART:
                    $entity['title'] = trans('app.pages.lot_filters.entity_type.arts');
                    $entity['image'] = asset('/images/art_default.png');
                    break;
                case EntityType::ART_PART:
                    $entity['title'] = trans('app.pages.lot_filters.entity_type.art_parts');
                    $entity['image'] = asset('/images/art_default.png');
                    $entity['hwmMarketLink'] = HwmLink::get(HwmLink::MARKET, [
                        'cat' => MarketCategory::ART_PART_MARKET_ID,
                    ]);
                    break;
                case EntityType::RESOURCE:
                    $entity['title'] = trans('app.pages.lot_filters.entity_type.resources');
                    $entity['image'] = asset('/images/icons/hwm/other/resources.png');
                    $entity['hwmMarketLink'] = HwmLink::get(HwmLink::MARKET, [
                        'cat' => Res::MARKET_CATEGORY_ID,
                    ]);
                    break;
                case EntityType::ELEMENT:
                    $entity['title'] = trans('app.pages.lot_filters.entity_type.elements');
                    $entity['image'] = asset('/images/icons/hwm/other/elements.png');
                    $entity['hwmMarketLink'] = HwmLink::get(HwmLink::MARKET, [
                        'cat' => Element::MARKET_CATEGORY_ID,
                    ]);
                    break;
                case EntityType::CERTIFICATE:
                    $entity['title'] = trans('app.pages.lot_filters.entity_type.certificates');
                    $entity['image'] = asset('/images/other/house_cert.jpg');
                    $entity['hwmMarketLink'] = HwmLink::get(HwmLink::MARKET, [
                        'cat' => Certificate::MARKET_CATEGORY_ID,
                    ]);
                    break;
                case EntityType::HOUSE:
                    $entity['title'] = trans('app.pages.lot_filters.entity_type.houses');
                    $entity['image'] = asset('/images/other/auc_dom.png');
                    $entity['hwmMarketLink'] = HwmLink::get(HwmLink::MARKET, [
                        'cat' => House::MARKET_CATEGORY_ID,
                    ]);
                    break;
                case EntityType::OBJECT_SHARE:
                    $entity['title'] = trans('app.pages.lot_filters.entity_type.object_shares');
                    $entity['image'] = asset('/images/other/obj_share_pic.png');
                    $entity['hwmMarketLink'] = HwmLink::get(HwmLink::MARKET, [
                        'cat' => ObjectShare::MARKET_CATEGORY_ID,
                    ]);
                    break;
                case EntityType::SKELETON:
                    $entity['title'] = trans('app.pages.lot_filters.entity_type.skeletons');
                    $entity['image'] = asset('/images/icons/hwm/other/skeleton.png');
                    $entity['hwmMarketLink'] = HwmLink::get(HwmLink::MARKET, [
                        'cat' => Skeleton::MARKET_CATEGORY_ID,
                    ]);
                    break;
            }
        }

        if ($filter->entity_id) {
            $lotEntity = (new MarketLot([
                'lot_id' => 1,
                'lot_type' => $filter->lot_type->value,
                'entity_type' => $filter->entity_type->value,
                'entity_id' => $filter->entity_id,
                'entity_title' => $this->getEntityTitle($filter),
                'price' => 1,
                'quantity' => 1,
                'ended_at' => Carbon::now(),
                'seller_name' => 'seller',
                'seller_id' => 1,
            ]))->entityAsArray();

            $entity['title'] = $lotEntity['title'];
            $entity['image'] = $lotEntity['image'];
            $entity['hwmMarketLink'] = $lotEntity['hwm_market_link'];
        }

        if (user()) {
            $linkDecorator = app(GameLinkDecorator::class);

            $entity['hwmMarketLink'] = $linkDecorator->replaceGameLinkForUser($entity['hwmMarketLink'], user());
        }

        return $entity;
    }


    private function getEntityTitle(LotFilter $filter): string
    {
        if ($filter->entity_type->isArtEntity() && $filter->entity_id) {
            return app(ArtRepository::class)->findByHwmId($filter->entity_id)?->getTitle() ?: UNDEFINED;
        }

        return UNDEFINED;
    }
}
