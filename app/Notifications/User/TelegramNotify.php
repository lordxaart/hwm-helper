<?php

declare(strict_types=1);

namespace App\Notifications\User;

use App\Core\Enums\Queue;
use App\Services\Notifications\Channels\Telegram\Entities\TelegramMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use App\Services\Notifications\Enums\Channel;

class TelegramNotify extends Notification implements ShouldQueue
{
    use Queueable;

    public string $message;

    public string $type;

    public function __construct(string $message, string $type = 'notice')
    {
        $this->queue = Queue::NOTIFICATIONS->value;

        $this->message = $message;

        $this->type = $type;
    }

    public function via(): array
    {
        return [
            Channel::TELEGRAM->getLaravelChannelName(),
        ];
    }

    public function toTelegram(): TelegramMessage
    {
        return TelegramMessage::create()
                ->setTypeContent($this->type)
                ->content($this->message);
    }
}
