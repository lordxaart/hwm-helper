<?php

declare(strict_types=1);

namespace App\Exceptions;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class ValidationError extends ValidationException
{
    public function __construct(Validator|string $field, string $message = null)
    {
        if (!$field instanceof Validator) {
            $validator = \Validator::make([], []);
            $validator->errors()->add($field, strval($message));
        } else {
            $validator = $field;
        }

        parent::__construct($validator);

        $this->message = trans('errors.422');
    }
}
