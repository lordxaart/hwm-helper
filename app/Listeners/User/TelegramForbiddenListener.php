<?php

declare(strict_types=1);

namespace App\Listeners\User;

use App\Services\ActivityLogger\ActivityLogger;
use App\Services\ActivityLogger\ValueObjects\Activity;
use App\Services\Notifications\Channels\Telegram\Events\TelegramForbiddenResponseEvent as Event;
use App\Services\Telegram\UserTelegramService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Psr\Log\LoggerInterface;

class TelegramForbiddenListener implements ShouldQueue
{
    public function __construct(
        private readonly UserTelegramService $userTelegramService,
        private readonly LoggerInterface $logger,
        private readonly ActivityLogger $activityLogger
    ) {
    }

    public function handle(Event $event): void
    {
        $user = $this->userTelegramService->getUserByChatId($event->chatId);

        $this->activityLogger->log(
            Activity::action('USER_TELEGRAM_FORBIDDEN')->causedBy($user)->context([
                'chat_id' => $event->chatId,
            ])
        );

        if (!$user) {
            $this->logger->error(sprintf('[TelegramForbiddenListener] Not found users with chatId %s', $event->chatId));
            return;
        }

        $this->userTelegramService->disconnectTelegram($user, true);
    }
}
