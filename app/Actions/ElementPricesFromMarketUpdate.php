<?php

declare(strict_types=1);

namespace App\Actions;

use App\HWM\Entities\BaseLot;
use App\HWM\Entities\Element;
use App\Models\ElementPrice;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Lorisleiva\Actions\Concerns\AsAction;

/**
 * Class AddArtSet
 * @property string $hwm_id
 * @property string $title
 * @package App\Actions\HWM
 */
class ElementPricesFromMarketUpdate
{
    use AsAction;

    public string $commandSignature = 'hwm:elements:parse_price';
    public string $commandDescription = 'Collect elements price from market';

    /**
     * @param Command $command
     * @return int
     */
    public function asCommand(Command $command): int
    {
        $result = $this->handle();

        $command->info('End parsing element pricing, count pricing ' . count($result));

        return 0;
    }

    public function handle(): array
    {
        $prices = [];

        $elements = json_decode(file_get_contents(resource_path('assets/hwm/elements.json')), true);
        foreach ($elements as $element) {
            $lots = hwmClient()
            ->getMarketLots(Element::MARKET_CATEGORY_ID, $element)
            ->map(function (BaseLot $lot) {
                return [
                    'lot_id' => $lot->lot_id,
                    'hwm_id' => $lot->entity_id,
                    'price' => $lot->price,
                    'quantity' => $lot->quantity,
                ];
            });

            if (!$lots->count()) {
                Log::warning("Not found lots for '$element'");
                return [];
            }

            $lotsForStatistic = $lots->sortBy('price')->take(15);

            $sumPrice = $lotsForStatistic->sum(function ($item) {
                return $item['price'] * $item['quantity'];
            });

            $quantity = intval($lotsForStatistic->sum('quantity'));

            $prices[] = ElementPrice::create([
                'element' => $element,
                'avg_price' => $quantity !== 0 ? intval($sumPrice / $quantity) : 0,
                'min_price' => $lots->min('price'),
                'max_price' => $lots->max('price'),
                'count_lots' => $lots->sum('quantity'),
            ]);
        }

        return $prices;
    }
}
