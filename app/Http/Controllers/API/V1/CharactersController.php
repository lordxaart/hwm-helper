<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V1;

use App\Actions\Character\CharacterAdd;
use App\Http\Requests\API\CharacterStoreRequest;
use App\Http\Resources\CharacterResource;
use Illuminate\Http\Request;

class CharactersController extends ApiController
{
    public function index()
    {
        $characters = user()?->characters()?->get() ?: collect([]);

        return $this->response(CharacterResource::collection($characters));
    }

    public function find(Request $request)
    {
        $res = null;

        if ($request->get('id')) {
            $res = hwmClient()->getCharacterInfo((int)$request->get('id'));
        } elseif ($request->get('login')) {
            $res = hwmClient()->getCharacterByLogin($request->get('login'));
        }

        return $this->response($res->toArray());
    }

    /**
     */
    public function store(CharacterStoreRequest $request)
    {
        $character = CharacterAdd::run(
            user(),
            $request->input('login'),
            $request->input('password')
        );

        return $this->response(new CharacterResource($character));
    }

    public function show(string $characterId)
    {
        $character = user()?->characters()?->findOrFail($characterId);

        if (!$character) {
            abort(404);
        }

        return $this->response(new CharacterResource($character));
    }
}
