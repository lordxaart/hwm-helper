<?php

declare(strict_types=1);

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;

class FeedbackStore extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'nullable|string|max:255',
            'email' => 'nullable|email',
            'message' => 'required|string|max:10000',
            'user_id' => 'nullable|numeric',
            'cookie_userid' => 'nullable|string|max:255',
            'source_page' => 'required|string|max:2000',
            'attachments' => 'array|max:10',
            'attachments.*' => 'image|max:3000',
        ];
    }
}
