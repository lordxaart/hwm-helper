import findL from 'lodash/find';
import mapValues from 'lodash/mapValues';
import keyBy from 'lodash/keyBy';
import upperFirst from 'lodash/upperFirst';

import config from 'JS/app/config';
import cache from 'JS/app/utils/cache';
import request from 'JS/app/utils/request';

const cacheKey = (key) => `${key}_${config.app.locale}`;

const st = {
    arts: [],
    loadingArts: false,
    categories: [],
    marketCategories: [],
    sets: [],
    loadingCategories: false,
    loadingMarketCategories: false,
    loadingSets: false,
};

const getters = {
    getArts(state) {
        return state.arts;
    },
    getLoadingArts(state) {
        return state.loadingArts;
    },
    getCategories(state) {
        return state.categories.length ? mapValues(keyBy(state.categories, 'hwmId'), 'title') : {};
    },
    getMarketCategories(state) {
        return state.marketCategories.length ? mapValues(keyBy(state.marketCategories, 'hwmId'), 'title') : {};
    },
    getSets(state) {
        return state.sets.length ? mapValues(keyBy(state.sets, 'hwmId'), 'title') : {};
    },
    getLoadingMarketCategories(state) {
        return state.loadingMarketCategories;
    },
};

const actions = {
    async loadArts({ state, commit, dispatch }) {
        if (state.loadingArts || state.arts.length) {
            return;
        }

        // Load arts from cache is they exists
        dispatch('loadArtsFromStorage');

        commit('START_LOADING', 'arts');
        const params = {
            fields: 'hwmId,title,images,repair,strength,shopPrice,pricePerFight,fromShop,asFromShop,categoryId,marketCategoryId,setId',
        };
        try {
            const response = await request.get('/api/v1/arts', { params });
            commit('SET_ARTS', response.data.data);
            commit('UPDATE_CACHE');
            commit('END_LOADING', 'arts');
        } catch (e) {
            commit('END_LOADING', 'arts');
            throw e;
        }
    },
    loadArtsFromStorage({ commit }) {
        if (cache.has(cacheKey(config.cache.keys.store.arts))) {
            commit('SET_ARTS', cache.get(cacheKey(config.cache.keys.store.arts)));
        }
    },
    loadCategoriesFromStorage({ commit }, type) {
        const key = cacheKey(config.cache.keys.store[type]);
        if (cache.has(key)) {
            const commits = {
                categories: 'SET_CATEGORIES',
                marketCategories: 'SET_MARKET_CATEGORIES',
                sets: 'SET_SETS',
            };
            commit(commits[type], cache.get(key));
        }
    },
    async findArtByHwmId({ state, dispatch }, hwmId) {
        await dispatch('loadArts');

        return findL(state.arts, (art) => art.hwmId === hwmId);
    },
    async loadCategories({ state, commit, dispatch }, type = 'categories') {
        dispatch('loadCategoriesFromStorage', type);

        const typeUpper = upperFirst(type);

        if (state[`loading${typeUpper}`] || state[type].length) {
            return;
        }

        commit('START_LOADING', typeUpper);
        const commits = {
            categories: 'SET_CATEGORIES',
            marketCategories: 'SET_MARKET_CATEGORIES',
            sets: 'SET_SETS',
        };

        try {
            const response = await request.get(`/api/v1/arts/${type}`);
            commit(commits[type], response.data.data);
            cache.set(cacheKey(config.cache.keys.store[type]), state[type]);
            commit('END_LOADING', typeUpper);
        } catch (e) {
            commit('END_LOADING', typeUpper);
            throw e;
        }
    },
};

const mutations = {
    SET_ARTS(state, data) {
        if (!Array.isArray(data)) {
            throw new Error('Data is not array');
        }
        if (!data.length) {
            return;
        }

        state.arts = data;
    },
    UPDATE_CACHE(state) {
        cache.set(cacheKey(config.cache.keys.store.arts), state.arts);
    },
    START_LOADING(state, type) {
        state[`loading${upperFirst(type)}`] = true;
    },
    END_LOADING(state, type) {
        state[`loading${upperFirst(type)}`] = false;
    },
    SET_CATEGORIES(state, data) {
        if (!Array.isArray(data)) {
            throw new Error('Categories data is not array');
        }
        if (!data.length) {
            return;
        }

        state.categories = data;
    },
    SET_MARKET_CATEGORIES(state, data) {
        if (!Array.isArray(data)) {
            throw new Error('Market Categories data is not array');
        }
        if (!data.length) {
            return;
        }

        state.marketCategories = data;
    },
    SET_SETS(state, data) {
        if (!Array.isArray(data)) {
            throw new Error('Sets data is not array');
        }
        if (!data.length) {
            return;
        }

        state.sets = data;
    },
};

export default {
    namespaced: true,
    state: st,
    getters,
    actions,
    mutations,
};
