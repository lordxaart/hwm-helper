// webpack/custom.js
const path = require('path');
// const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
// const ESLintPlugin = require('eslint-webpack-plugin');

module.exports = {
    // plugins: [
    //     new ESLintPlugin({
    //         exclude: [
    //             '/node_modules/',
    //         ],
    //         extensions: ['js', 'vue'],
    //     }),
    // ],
    resolve: {
        alias: {
            JS: path.resolve(process.cwd(), 'resources/assets/js'),
            SASS: path.resolve(process.cwd(), 'resources/asset/sass'),
        },
        extensions: ['.js', '.vue', '.json'],
    },
    output: {
        chunkFilename: process.env.NODE_ENV === 'production' ? 'js/app/[name].js?id=[hash]' : 'js/app/[name].js',
    },
    optimization: {
        runtimeChunk: {
            name: 'js/app/manifest',
        },
        chunkIds: 'named',
        splitChunks: {
            cacheGroups: {
                commons: {
                    chunks: 'initial',
                    minChunks: 2,
                    maxInitialRequests: 5,
                    minSize: 0,
                },
                vendor: {
                    chunks: 'all',
                    name: 'vendor',
                    test: (mod) => {
                        const vendor = 'node_modules';

                        if (!mod.context.includes(vendor)) {
                            return false;
                        }

                        const pkg = mod.context.split(`/${vendor}/`).pop();
                        const packages = [
                            'vue',
                            'vuex',
                            'vue-i18n',
                            // 'noty',
                            'nprogress',
                            'axios',
                            'bootstrap-vue',
                            'lodash',
                            'popper.js',
                            'v-tooltip',
                            '@fortawesome',
                            'pusher-js',
                            'laravel-echo',
                            'vue-router',
                            'vue-meta',
                            // 'vue-tables-2',
                        ];

                        if (pkg && packages.some((str) => pkg.includes(`${str}/`) || pkg === str)) {
                            return true;
                        }
                        return false;
                    },
                },
            },
        },
    },
};
