<?php

declare(strict_types=1);

namespace App\Services\Telegram\Commands;

use App\Models\User;
use App\Services\Notifications\Channels\Telegram\Entities\TelegramMessage;
use App\Services\Telegram\Entities\Update;
use App\Services\Telegram\TelegramApi;
use App\Services\Telegram\TelegramCommand;
use App\Services\Telegram\UserTelegramService;

class StartCommand implements TelegramCommand
{
    public const COMMAND = '/start';

    private TelegramApi $telegramApi;
    private UserTelegramService $userTelegramService;

    public function __construct()
    {
        $this->telegramApi = app(TelegramApi::class);
        $this->userTelegramService = app(UserTelegramService::class);
    }

    public function handle(Update $update): void
    {
        if (!$update->message || !$update->message->from) {
            return;
        }

        $text = (string)$update->message->text;
        $userHash = trim(str_replace(self::COMMAND, '', $text));

        $chatId = $update->message->chat->id;
        $user = $this->userTelegramService->getUserByChatId($chatId);

        if ($user) {
            $this->userAlreadyHasAccount($user, $chatId);
            return;
        }

        $user = User::getUserByHash($userHash);

        if ($user) {
            $this->userTelegramService->connectTelegramToUser(
                $user,
                $update->message->chat->id,
                $update->message->from,
            );
            return;
        }

        \Log::warning('Not found user hash in telegram message', ['hash' => $userHash]);

        $from = $this->objectToArray($update->message->from);
        if ($this->telegramApi->sendConnectLinkToChat($chatId, ['from' => $from])) {
            \Log::info('Sent telegram connect link', $this->objectToArray($update));
        }
    }

    private function userAlreadyHasAccount(User $user, int $chatId)
    {
        $message = new TelegramMessage(trans('app.user.telegram_already_connected', ['email' => $user->email]));
    }

    private function objectToArray(object $object): array
    {
        return (array)$object;
    }
}
