<?php

declare(strict_types=1);

namespace App\Listeners\HWM;

use App\HWM\Repositories\ArtRepository;
use App\Jobs\AdminNotify;
use App\Notifications\Admin\NewArt;
use App\Notifications\User\FoundNewArt;
use App\Events\HWM\ArtAddedEvent;
use App\Services\ActivityLogger\ActivityLogger;
use App\Services\ActivityLogger\ValueObjects\Activity;
use App\Services\Notifications\UserNotifyService;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Psr\Log\LoggerInterface;

class NewArtListener implements ShouldQueue
{
    public function __construct(
        private readonly Kernel $artisan,
        private readonly Dispatcher $dispatcher,
        private readonly ActivityLogger $activityLogger,
        private readonly LoggerInterface $logger
    ) {
    }

    public function handle(ArtAddedEvent $event): void
    {
        try {
            $this->activityLogger->log(
                new Activity('HWM_ART_CREATE', ['art' => $event->art->getHwmId()])
            );
        } catch (\Throwable $exception) {
            $this->logger->error($exception->getMessage());
        }

        $this->dispatcher->dispatchSync(new AdminNotify(new NewArt($event->art)));

        // Public broadcast
        /** @var UserNotifyService $service */
        $service = app(UserNotifyService::class);
        $service->sendNotifyToPublicChannel(new FoundNewArt($event->art));

        $this->artisan->call('hwm:art:generate:sprite');
        $event->art->autoTranslate();

        ArtRepository::refreshCache();
    }
}
