<?php

declare(strict_types=1);

namespace App\Models;

class MarketCategory extends BaseCategory
{
    public const ART_PART_MARKET_ID = 'part';

    protected $casts = [
        'only_art' => 'bool',
    ];

    public function arts(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Art::class, 'market_category_id', 'hwm_id');
    }
}
