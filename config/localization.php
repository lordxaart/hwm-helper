<?php

return [
    'locales' => [
        'ru' => ['name' => 'Russian', 'script' => 'Cyrl', 'native' => 'Русский', 'regional' => 'ru_RU'],
        'en' => ['name' => 'English', 'script' => 'Latn', 'native' => 'English', 'regional' => 'en_GB'],
        'uk' => ['name' => 'Ukrainian', 'script' => 'Cyrl', 'native' => 'Українська', 'regional' => 'uk_UA'],
    ],

    // If false, system will take app.php locale attribute
    'useAcceptLanguageHeader' => true,
    'useIpForDetectLocale' => true,
];
