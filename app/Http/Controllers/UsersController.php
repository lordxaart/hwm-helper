<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Core\Enums\SocialProvider;
use App\Exceptions\BaseException;
use App\Exceptions\Social\InvalidState;
use App\Http\Requests\UpdatePassword;
use App\Models\SocialAccount;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\RedirectResponse;

class UsersController extends Controller
{
    public function index(): mixed
    {
        return redirect(route('profile.settings'));
    }

    public function updatePassword(UpdatePassword $request): RedirectResponse
    {
        /** @var User $user */
        $user = user();

        if (Hash::check((string)$request->input('password'), (string)$user->password)) {
            return redirect()->back()->with('error', trans('validation.same_password'))->withFragment('#password');
        }

        $user->updatePassword($request->input('password'));

        return redirect()->back()
            ->with('success', trans('app.user.successfully_update_password'))
            ->withFragment('#password]');
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function connectSocialNetwork(string $social): RedirectResponse
    {
        /** @var User $user */
        $user = user();

        \Validator::make(['social' => $social], [
            'social' => 'required|social_provider',
        ])->validate();

        $enum = SocialProvider::tryFrom($social);

        if ($enum && $user->hasSocialProvider($enum)) {
            return redirect()->back()->with('error', trans('app.user.social_network_already_connected'));
        }

        redirect()->setIntendedUrl(url()->previous());

        return \Socialite::driver($social)->redirect();
    }

    /**
     * @param string $social
     * @return \Illuminate\Http\RedirectResponse
     * @throws InvalidState
     * @throws \Illuminate\Validation\ValidationException
     */
    public function handleConnectSocialNetwork(string $social): RedirectResponse
    {
        /** @var User $user */
        $user = user();

        \Validator::make(['social' => $social], [
            'social' => 'required|social_provider',
        ])->validate();

        try {
            $socialAccount = SocialAccount::firstOrCreateCustom($social);
        } catch (BaseException $e) {
            session()->flash('error', $e->getMessage());

            return redirect()->intended();
        }

        if ($user->email !== $socialAccount->user->email) {
            $socialAccount->forceDelete();

            throw new InvalidState($social);
        }

        session()->flash('success', trans('app.user.success_connect_with_social_network', ['social' => $social]));

        return redirect()->intended();
    }
}
