<?php

declare(strict_types=1);

namespace App\Services\SearchLots\Repositories;

use App\Services\SearchLots\ValueObjects\Aggregation\Aggregation;
use App\Services\SearchLots\ValueObjects\Aggregation\AggregationResponse;
use App\Services\SearchLots\ValueObjects\LotFilters;

interface LotAggregationsRepository
{
    /**
     * @param Aggregation[] $aggregations
     */
    public function getAggregations(
        array $aggregations,
        LotFilters $filters,
    ): AggregationResponse;
}
