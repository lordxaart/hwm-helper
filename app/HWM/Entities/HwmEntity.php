<?php

declare(strict_types=1);

namespace App\HWM\Entities;

use App\HWM\Entities\Interfaces\HasTranslations;
use App\HWM\Helpers\HwmLink;
use App\HWM\Entities\Interfaces\HasImage;
use App\HWM\Entities\Interfaces\HasLinkOnPage;
use App\HWM\Entities\Interfaces\CanBeLot;
use App\HWM\Entities\Interfaces\HwmEntityInterface;
use Illuminate\Support\Arr;
use Spatie\DataTransferObject\DataTransferObject;
use TypeError;

abstract class HwmEntity extends DataTransferObject implements HwmEntityInterface
{
    private string $hwmId;
    public string $title;

    public function __construct(string $hwmId, array $parameters = [])
    {
        $this->hwmId = $hwmId;

        $properties = array_filter(array_map(function (\ReflectionProperty $property) {
            return $property->isPublic() ? $property->getName() : null;
        }, (new \ReflectionClass($this))->getProperties()));

        $parameters = Arr::only($parameters, $properties);

        parent::__construct($parameters);

        $this->checkAttributes();
    }

    public function getHwmId(): string
    {
        return $this->hwmId;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    protected function checkAttributes(): void
    {
        // HwmLink
        if ($this instanceof HasLinkOnPage) {
            $link = $this->getHwmLink();

            if (!filter_var($link, FILTER_VALIDATE_URL) || !str_starts_with($link, HwmLink::BASE_LINK)) {
                throw new TypeError("Invalid hwm link '$link'");
            }
        }

        // Image
        if ($this instanceof HasImage || $this instanceof CanBeLot) {
            $image = $this->getImage();

            if (!filter_var($image, FILTER_VALIDATE_URL)) {
                throw new TypeError("Invalid image url '$image'");
            }
        }

        if ($this instanceof HasTranslations) {
            $this->translateAttributes();
        }
    }
}
