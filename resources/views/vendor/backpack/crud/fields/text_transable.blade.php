@php
$input_type = $field['name'] === 'description' ? 'textarea' : 'input';
@endphp
<!-- text input -->
@include('crud::fields.inc.wrapper_start')
    <label>{!! $field['label'] !!}</label>
    <b-tabs content-class="mt-3">
        @foreach(config('localization.locales') as $locale => $item)
        <b-tab {{ $locale === app()->getLocale() ? 'active' : '' }}>
            <template v-slot:title>
                <img src="{{ asset('images/icons/flags/' . $locale . '.svg') }}" alt="{{ $item['name'] }}" width="20px"  v-tooltip="'{{ $item['native'] }}'">
            </template>
            @if($input_type === 'input')
            <input
                type="text"
                name="{{ $field['name'] }}[{{ $locale}}]"
                value="{{ old($field['name'])[$locale] ?? $entry->getTranslation($field['name'], $locale, false) ?? '' }}"
                @include('crud::fields.inc.attributes')
            >
            @endif
            @if($input_type === 'textarea')
            <textarea
                name="{{ $field['name'] }}[{{ $locale}}]"
                rows="4"
                @include('crud::fields.inc.attributes')
            >{{ old($field['name'])[$locale] ?? $entry->getTranslation($field['name'], $locale, false) ?? '' }}</textarea>
            @endif
        </b-tab>
        @endforeach
    </b-tabs>
</div>
