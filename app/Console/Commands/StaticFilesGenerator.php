<?php

declare(strict_types=1);

namespace App\Console\Commands;

class StaticFilesGenerator extends Command
{
    public const MANIFEST = 'manifest.json';
    public const ROBOTS = 'robots.txt';

    /** @var string  */
    protected $signature = 'generate:static';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate next files: ' . self::MANIFEST . ', ' . self::ROBOTS;

    /**
     * Disallow rules for robots.txt
     * @var string[]
     */
    protected array $robotsDisallowRules = [
        '/admin/',
    ];

    public function handle(): void
    {
        $this->generateManifest();

        $this->generateRobots();
    }

    protected function generateManifest(): void
    {
        $path = public_path(self::MANIFEST);

        // prepare data for manifest.json
        $data = [
            'name' => config('app.name'),
            'short_name' => config('app.name'),
            'start_url' => '/calculator',
            'display' => 'standalone',
            'scope' => '/',
            'background_color' => '#252525',
            'theme_color' => '#00bc8c',
            'description' => 'Вспомогательный сервис для сайта heroeswm.ru, калькулятор артефактов, мониторинг и статистика лотов',
            'icons' => [
                [
                    'src' => '/images/favicons/android-icon-36x36.png',
                    'sizes' => '36x36',
                    'type' => 'image/png',
                ],
                [
                    'src' => '/images/favicons/android-icon-48x48.png',
                    'sizes' => '48x48',
                    'type' => 'image/png',
                ],
                [
                    'src' => '/images/favicons/android-icon-72x72.png',
                    'sizes' => '72x72',
                    'type' => 'image/png',
                ],
                [
                    'src' => '/images/favicons/android-icon-96x96.png',
                    'sizes' => '96x96',
                    'type' => 'image/png',
                ],
                [
                    'src' => '/images/favicons/android-icon-144x144.png',
                    'sizes' => '144x144',
                    'type' => 'image/png',
                ],
                [
                    'src' => '/images/favicons/android-icon-192x192.png',
                    'sizes' => '192x192',
                    'type' => 'image/png',
                ],
                [
                    'src' => '/images/logo/logo_512x512.png',
                    'sizes' => '512x512',
                    'type' => 'image/png',
                ],
            ],
        ];

        file_put_contents($path, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));

        $this->log('Generated public/' . self::MANIFEST);
    }

    protected function generateRobots(): void
    {
        $path = public_path(self::ROBOTS);

        $content = 'User-agent: *' . PHP_EOL;

        if (!isProdEnv()) {
            $content .= 'Disallow: /' . PHP_EOL;
        }

        if (count($this->robotsDisallowRules)) {
            $content .= 'Disallow: ' . implode(PHP_EOL . 'Disallow: ', $this->robotsDisallowRules);
        }

        file_put_contents($path, $content);

        $this->log('Generated public/' . self::ROBOTS);
    }
}
