<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Components\SelectArt;
use Tests\DuskTestCase;

class ExampleTest extends DuskTestCase
{
    /**
     * @test
     */
    public function testBasicExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/calculator')
                    ->pause(200)
                    ->assertSee(__('app.main.calculator'));
        });
    }
}
