import notify from 'JS/app/utils/notify';
import i18n from 'JS/app/utils/localization';

(() => {
    const searchString = window.location.search;
    if (!searchString) {
        return;
    }

    const paramsString = decodeURIComponent(searchString.substr(1));
    const params = new URLSearchParams(paramsString);

    if (params.get('only_auth')) {
        notify.warning(i18n.t('app.user.access_only_for_auth_users'));
    }
})();
