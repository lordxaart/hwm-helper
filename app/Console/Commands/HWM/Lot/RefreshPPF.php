<?php

declare(strict_types=1);

namespace App\Console\Commands\HWM\Lot;

use App\Console\Commands\Command;
use App\HWM\Enums\EntityType;
use App\Models\Lot;
use Illuminate\Database\Eloquent\Collection;

class RefreshPPF extends Command
{
    public $signature = 'hwm:lot:update:ppf';

    public $description = 'Many records parsed with ended_at as current date (fixed for new lots). Set from operation end';

    public function handle(): void
    {
        Lot::select(['lot_id', 'entity_type', 'entity_id', 'price_per_fight', 'current_strength', 'base_strength', 'price'])
            ->where('entity_type', EntityType::ART->value)
            ->orderByDesc('lot_id')
            ->chunkById(100_000, function (Collection $lots) {
                $this->info(sprintf(
                    'Start parse %s lots from %s to %s',
                    $lots->count(),
                    $lots->min('lot_id'),
                    $lots->max('lot_id'),
                ));

                /** @var Lot $lot */
                foreach ($lots as $lot) {
                    $lot->updatePPF(true);
                }

                $this->info(sprintf('Last parsed lot #%s', $lots->max('lot_id')));
            }, 'lot_id');
    }
}
