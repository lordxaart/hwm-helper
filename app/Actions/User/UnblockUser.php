<?php

declare(strict_types=1);

namespace App\Actions\User;

use App\Models\User;
use InvalidArgumentException;
use Lorisleiva\Actions\Concerns\AsAction;

class UnblockUser
{
    use AsAction;

    public function handle(User $user): bool
    {
        if (!$user->isBlocked()) {
            throw new InvalidArgumentException('Cannot unblock not blocked user');
        }

        if ($user->fireEvent(User::ADMIN_ACTION_UNBLOCKING) === false) {
            return false;
        }

        $user->blocked_at = null;
        $user->blocked_until_to = null;

        $user->exists = true;

        $result = $user->save();

        $user->fireEvent(User::ADMIN_ACTION_UNBLOCKED, false);

        return $result;
    }
}
