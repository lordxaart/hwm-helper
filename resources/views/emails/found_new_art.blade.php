@php
    /**
     * @var \App\Models\Art $art
     * @var \App\Models\User $user
     * @var \App\Services\Settings\User\GameLinkDecorator $linkDecorator
    */
@endphp

@component('mail::message')
<h1>{{ __('app.notifications.found_new_art') }}</h1>
<div class="text-center mb-4">
    <p>
        <a href="{{ $linkDecorator->replaceGameLinkForUser($art->getLink(), $user) }}">{{ $art->getTitle() }}</a>
    </p>
    <img src="{{ $art->asArtEntity()->getImage() }}" width="100">
</div>
@endcomponent
