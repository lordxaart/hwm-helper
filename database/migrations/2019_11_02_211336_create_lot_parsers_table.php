<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLotParsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lots_parser', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('lot_id')->index();
            $table->boolean('is_success')->default(0); // status after parsing
            $table->boolean('is_active')->default(0); // status run parser
            $table->tinyInteger('attempts')->default(0); // count attempts of parsing
            $table->string('action')->default('new_lot')->index(); // action of parser (new lot or update current lot)
            $table->string('server_ip')->nullable();

            // extra fields
            $table->text('errors')->nullable();
            $table->string('time_execute')->nullable();

            $table->timestamps(6);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lot_parsers');
    }
}
