@php
    /** @var \App\Models\User $user */
    $user = data_get($entry, $column['name']);
@endphp

@if($user && $user instanceof \App\Models\User)
    <a href="{{ route('admin.users.show', ['id' => $user->id]) }}">{{ $user->email }}</a>
@endif