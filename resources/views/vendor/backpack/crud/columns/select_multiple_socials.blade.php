@php
    $results = data_get($entry, $column['name']);
@endphp

<span>
    <?php
        if ($results && $results->count()) {
            foreach ($results as $item) {
                $icon = config('icons.' . $item->provider);
                echo "<a
                        href='#'
                        class='btn btn-link social-{$item->provider} active'
                        title='{$item->provider}'
                        data-toggle='tooltip'
                        onclick='event.preventDefault()'
                    >
                        <i class='{$icon} fa-sm'></i>
                    </a>";
            }
        } else {
            echo '-';
        }
    ?>
</span>