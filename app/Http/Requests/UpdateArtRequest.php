<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateArtRequest extends FormRequest
{
    public function authorize(): bool
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    public function rules(): array
    {
        return [
            'title' => 'nullable|array',
            'description' => 'nullable|array',
            'need_level' => 'nullable|integer',
            'oa' => 'nullable|integer',
            'repair' => 'nullable|integer',
            'strength' => 'nullable|integer',
            'price' => 'nullable|integer',
            'category_id' => 'nullable|exists:art_categories,hwm_id',
            'market_category_id' => 'nullable|exists:market_categories,hwm_id',
            'set_id' => 'nullable|exists:art_sets,hwm_id',
        ];
    }
}
