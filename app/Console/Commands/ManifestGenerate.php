<?php

declare(strict_types=1);

namespace App\Console\Commands;

class ManifestGenerate extends Command
{
    /** @var string  */
    protected $signature = 'manifest:generate';

    /** @var string  */
    protected $description = 'Generate public/manifest.json file';

    public function handle(): void
    {
        $this->manifest();

        $this->robots();
    }

    protected function manifest(): void
    {
        $path = public_path('manifest.json');

        $data = [
            'name' => config('app.name'),
            'short_name' => config('app.name'),
            'start_url' => '/calculator',
            'display' => 'standalone',
            'background_color' => '#252525',
            'theme_color' => '#00bc8c',
            'description' => 'Helpers functions for heroeswm.ru site',
            'icons' => [
                [
                    'src' => '/images/favicons/android-icon-36x36.png',
                    'sizes' => '36x36',
                    'type' => 'image/png',
                    'density' => '0.75'
                ],
                [
                    'src' => '/images/favicons/android-icon-48x48.png',
                    'sizes' => '48x48',
                    'type' => 'image/png',
                    'density' => '1.0'
                ],
                [
                    'src' => '/images/favicons/android-icon-72x72.png',
                    'sizes' => '72x72',
                    'type' => 'image/png',
                    'density' => '1.5'
                ],
                [
                    'src' => '/images/favicons/android-icon-96x96.png',
                    'sizes' => '96x96',
                    'type' => 'image/png',
                    'density' => '2.0'
                ],
                [
                    'src' => '/images/favicons/android-icon-144x144.png',
                    'sizes' => '144x144',
                    'type' => 'image/png',
                    'density' => '3.0'
                ],
                [
                    'src' => '/images/favicons/android-icon-192x192.png',
                    'sizes' => '192x192',
                    'type' => 'image/png',
                    'density' => '4.0'
                ],
            ],
        ];

        file_put_contents($path, json_encode($data, JSON_PRETTY_PRINT));

        $this->log('Generated public/manifest.json');
    }

    protected function robots(): void
    {
        $path = public_path('robots.txt');

        $content = 'User-agent: *' . PHP_EOL . 'Disallow:';

        if (!isProdEnv()) {
            $content .= ' / ' . PHP_EOL;
        }

        file_put_contents($path, $content);

        $this->log('Generated public/robots.txt');
    }
}
