<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnNotificationsToArtMonitoredFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('art_monitored_filters', function (Blueprint $table) {
            $table->json('notifications')->nullable();
            $table->dropColumn('enable_browser_notification');
            $table->dropColumn('enable_telegram_notification');
            $table->dropColumn('enable_mail_notification');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('art_monitored_filters', function (Blueprint $table) {
            $table->dropColumn('notifications');
        });
    }
}
