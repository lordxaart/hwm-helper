<?php

declare(strict_types=1);

namespace App\HWM\Entities\Collections;

use App\Core\ValueObject\EntityCollection;
use App\HWM\Entities\BaseLot;
use Illuminate\Support\Collection;

/**
 * @extends Collection<array-key, BaseLot>
 */
class LotCollection extends EntityCollection
{
    protected function validateItem(mixed $item): void
    {
        if (!($item instanceof BaseLot)) {
            throw new \InvalidArgumentException(
                sprintf(
                    'Each element of collection must be instance of [%s], get [%s]',
                    BaseLot::class,
                    is_object($item) ? $item::class : gettype($item)
                )
            );
        }
    }
}
