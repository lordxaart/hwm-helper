<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (Role::getBasePermissions() as $roleName => $permissions) {
            $role = Role::firstOrCreate(['name' => $roleName]);

            foreach ($permissions as $name) {
                $permission = Permission::where('name', $name)->first() ?: Permission::create(['name' => $name]);
                $permission->assignRole($role);
            }
        }

        $this->command->info('Success seeds Roles and Permissions');
    }
}
