<?php

declare(strict_types=1);

namespace App\Exceptions;

use App\HWM\Exceptions\RequestErrors\LoginException;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Http\Response;
use Illuminate\Contracts\Debug\ExceptionHandler as ContractHandler;
use Illuminate\Validation\ValidationException;
use ReflectionFunction;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Response as BaseResponse;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Throwable;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

trait ApiExceptionHandle
{
    /**
     * Generic response format.
     *
     * @var array
     */
    protected $format = [
        'message' => ':message',
        'errors' => ':errors',
        'code' => ':code',
        'status_code' => ':status_code',
        'debug' => ':debug',
    ];

    public function renderApiException(Throwable $e, Request $request)
    {
        if ($e instanceof ValidationException) {
            $e = new ValidationError($e->validator);
        }

        if ($e instanceof AuthenticationException) {
            $e = new UnauthorizedHttpException('', $e->getMessage(), $e->getPrevious(), $e->getCode());
        }

        if ($e instanceof ModelNotFoundException) {
            $e = new NotFoundHttpException(
                trans('errors.model_not_found', ['model' => $this->getModelBaseName($e->getModel())])
            );
        }

        if ($e instanceof LoginException) {
            $e = new ServerError('Login Exception');
        }


        // Convert Eloquent's 500 ModelNotFoundException into a 404 NotFoundHttpException
        if ($e instanceof ModelNotFoundException) {
            $e = new NotFoundHttpException($e->getMessage(), $e);
        }

        return $this->genericApiResponse($e)->withException($e);
    }

    private function getModelBaseName(string $model): string
    {
        return Arr::last(explode('\\', $model));
    }

    /**
     * Handle a generic error response if there is no handler available.
     *
     * @param Throwable $exception
     * @return Response
     *
     * @throws Throwable
     */
    protected function genericApiResponse(Throwable $exception): Response
    {
        $replacements = $this->prepareReplacements($exception);

        $response = $this->format;

        array_walk_recursive($response, function (&$value, $key) use ($replacements) {
            if (Str::startsWith($value, ':') && isset($replacements[$value])) {
                $value = $replacements[$value];
            }
        });

        $response = $this->recursivelyRemoveEmptyReplacements($response);

        return new Response($response, $this->getStatusCode($exception), $this->getHeaders($exception));
    }


    /**
     * Prepare the replacements array by gathering the keys and values.
     *
     * @param Throwable $exception
     * @return array
     */
    protected function prepareReplacements(Throwable $exception)
    {
        $statusCode = $this->getStatusCode($exception);

        if (!$message = $exception->getMessage()) {
            $message = sprintf('%d %s', $statusCode, Response::$statusTexts[$statusCode]);
        }

        $replacements = [
            ':message' => $message,
            ':status_code' => $statusCode,
        ];

        if ($exception instanceof ValidationException) {
            $replacements[':errors'] = $exception->errors();
            $replacements[':status_code'] = $exception->status;
        }

        if ($code = $exception->getCode()) {
            $replacements[':code'] = $code;
        }

        if (isDebugMode()) {
            $replacements[':debug'] = [
                'line' => $exception->getLine(),
                'file' => $exception->getFile(),
                'class' => get_class($exception),
                'trace' => explode("\n", $exception->getTraceAsString()),
            ];

            // Attach trace of previous exception, if exists
            if (!is_null($exception->getPrevious())) {
                $currentTrace = $replacements[':debug']['trace'];

                $replacements[':debug']['trace'] = [
                    'previous' => explode("\n", $exception->getPrevious()->getTraceAsString()),
                    'current' => $currentTrace,
                ];
            }
        }

        if (!isDebugMode() && $statusCode === 500) {
            $replacements[':message'] = null;
        }

        return $replacements;
    }

    /**
     * Recursively remove any empty replacement values in the response array.
     *
     * @param array $input
     * @return array
     */
    protected function recursivelyRemoveEmptyReplacements(array $input)
    {
        foreach ($input as &$value) {
            if (is_array($value)) {
                $value = $this->recursivelyRemoveEmptyReplacements($value);
            }
        }

        return array_filter($input, function ($value) {
            if (is_string($value)) {
                return !Str::startsWith($value, ':');
            }

            return true;
        });
    }

    /**
     * Get the status code from the exception.
     *
     * @param Throwable $exception
     * @return int
     */
    protected function getStatusCode(Throwable $exception)
    {
        if ($exception instanceof ValidationException) {
            $statusCode = $exception->status;
        } elseif ($exception instanceof HttpExceptionInterface) {
            $statusCode = $exception->getStatusCode();
        } else {
            // By default throw 500
            $statusCode = 500;
        }

        // Be extra defensive
        if ($statusCode < 100 || $statusCode > 599) {
            $statusCode = 500;
        }

        return $statusCode;
    }

    /**
     * Get the headers from the exception.
     *
     * @param Throwable $exception
     * @return array
     */
    protected function getHeaders(Throwable $exception)
    {
        return $exception instanceof HttpExceptionInterface ? $exception->getHeaders() : [];
    }
}
