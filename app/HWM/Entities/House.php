<?php

declare(strict_types=1);

namespace App\HWM\Entities;

use App\HWM\Enums\EntityType;
use App\HWM\Helpers\HwmLink;
use App\HWM\Entities\Interfaces\CanBeLot;
use App\HWM\Entities\Interfaces\HasLinkOnPage;

class House extends HwmEntity implements CanBeLot, HasLinkOnPage
{
    public const MARKET_CATEGORY_ID = 'dom';

    public function getEntityType(): EntityType
    {
        return EntityType::HOUSE;
    }

    public function getMarketCategoryId(): string
    {
        return self::MARKET_CATEGORY_ID;
    }

    public function getImage(bool $realPath = false, string $size = null): string
    {
        return !$realPath ? asset('images/other/auc_dom.png') : public_path('images/other/auc_dom.png');
    }

    public function getHwmLink(): string
    {
        return HwmLink::get(HwmLink::HOUSE_INFO, ['id' => $this->getHwmId()]);
    }
}
