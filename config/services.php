<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party AppService
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'ses' => [
        'key' => env('AWS_SES_KEY'),
        'secret' => env('AWS_SES_SECRET'),
        'region' => env('AWS_SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'redirect' => '/auth/google/callback',

        'tag_manager' => [
            'id' => env('GOOGLE_TAG_MANAGER_ID'),
        ],
        'analytics' => [
            'id' => env('GOOGLE_A_ID'),
        ],
        'recaptcha' => [
            'site_key' => env('GOOGLE_RECAPTCHA_SITE_KEY'),
            'secret_key' => env('GOOGLE_RECAPTCHA_SECRET_KEY'),
        ]
    ],

    'facebook' => [
        'client_id' => env('FB_CLIENT_ID'),
        'client_secret' => env('FB_CLIENT_SECRET'),
        'redirect' => '/auth/facebook/callback',
    ],

    'twitter' => [
        'client_id' => env('TWITTER_CLIENT_ID'),
        'client_secret' => env('TWITTER_CLIENT_SECRET'),
        'redirect' => '/auth/twitter/callback',
    ],

    'vkontakte' => [
        'client_id' => env('VK_CLIENT_ID'),
        'client_secret' => env('VK_CLIENT_SECRET'),
        'redirect' => '/auth/vkontakte/callback',
    ],

    'slack' => [
        'client_id' => env('SLACK_CLIENT_ID'),
        'client_secret' => env('SLACK_CLIENT_SECRET'),
        'redirect' => '/auth/slack/callback',
        'webhook_url' => env('LOG_SLACK_WEBHOOK_URL'),
        'notification_webhook_url' => env('NOTIFICAION_SLACK_WEBHOOK_URL'),
    ],

    'sendgrid' => [
        'api_key' => env('SENDGRID_API_KEY'),
    ],

    'telegram-bot-api' => [
        'token' => env('TELEGRAM_BOT_TOKEN'),
        'notify_chat_id' => env('TELEGRAM_NOTIFY_CHAT_ID'), // admin channel
        'notify_channel_id' => env('TELEGRAM_NOTIFY_CHANNEL_ID'), // public channel
    ],

    'telegram' => [
        'bot' => env('TELEGRAM_BOT_NAME'),  // The bot's username
        'client_id' => null,
        'client_secret' => env('TELEGRAM_BOT_TOKEN'),
        'redirect' => '/auth/telegram/callback',
    ],

    'kraken' => [
        'key' => env('KRAKEN_KEY'),
        'secret' => env('KRAKEN_SECRET'),
    ],
    'tinypng' => [
        'key' => env('TINYPNG_KEY'),
    ],

    'elastic' => [
        'hosts' => env('ELASTIC_HOST', 'localhost') . ':' . env('ELASTIC_PORT', '9200'),
        'user' => env('ELASTIC_USER'),
        'password' => env('ELASTIC_PASSWORD'),
    ],
    'amplitude' => [
        'key' => env('AMPLITUDE_API_KEY'),
    ]
];
