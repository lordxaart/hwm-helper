<?php

declare(strict_types=1);

namespace App\Console\Commands\HWM\Lot;

use App\Console\Commands\Command;
use App\Jobs\Lot\LotUpdate;
use App\Models\Lot;
use Illuminate\Database\Eloquent\Collection;

class UpdateUnparsedLots extends Command
{
    protected $signature = 'hwm:lots:update:unparsed';

    protected $description = 'Add to lot:update queue unparsed lots';

    public function handle(): void
    {
        Lot::select('lot_id')
        ->parsedFromMarker()
        ->whereNull('ended_at')
        ->chunk(10_000, function (Collection $lots) use (&$count) {
            $currentCount = 0;
            /** @var Lot $lot */
            foreach ($lots as $lot) {
                dispatch(new LotUpdate($lot->lot_id));
                $count++;
                $currentCount++;
            }
            $this->info("Added $currentCount lots");
        });

        $this->info("END. Added $count lots");
    }
}
