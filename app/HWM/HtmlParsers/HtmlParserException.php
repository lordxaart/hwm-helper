<?php

declare(strict_types=1);

namespace App\HWM\HtmlParsers;

use Throwable;

class HtmlParserException extends \Exception
{
    public function __construct(string|Throwable $message)
    {
        $previous = $message instanceof Throwable ? $message : null;

        $message = $message instanceof Throwable ? $message->getMessage() : '';

        parent::__construct($message, 0, $previous);
    }
}
