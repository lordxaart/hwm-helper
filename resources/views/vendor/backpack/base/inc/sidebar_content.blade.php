<li class="nav-item"><a class="nav-link" href="{{ backpack_url('statuspage') }}"><i class="nav-icon {{ icon('statuspage') }}"></i> <span>Status Page</span></a></li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon {{ icon('users') }}"></i> Users</a>
    <ul class="nav-dropdown-items">
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('users') }}"><i class="nav-icon {{ icon('users') }}"></i> <span>Users</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('users/subscriptions') }}"><i class="nav-icon {{ icon('users') }}"></i> <span>Subscriptions</span></a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon {{ icon('art') }}"></i> HWM</a>
    <ul class="nav-dropdown-items">
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('arts') }}"><i class="nav-icon {{ icon('art') }}"></i> <span>Arts</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('monitored-arts') }}"><i class="nav-icon {{ icon('art') }}"></i> <span>Monitored Arts</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('lot-filters') }}"><i class="nav-icon fas fa-list-ul"></i> <span>Filters</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('art-categories') }}"><i class="nav-icon fas fa-list-ul"></i> <span>Categories</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('market-categories') }}"><i class="nav-icon fas fa-list-ul"></i> <span>M. Categories</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('art-sets') }}"><i class="nav-icon fas fa-list-ul"></i> <span>Sets</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('bots') }}"><i class="nav-icon fas fa-list-ul"></i> <span>Bots (Parser)</span></a></li>
    </ul>
</li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('app-settings') }}"><i class="nav-icon {{ icon('statuspage') }}"></i> <span>App Settings</span></a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('backup') }}"><i class="nav-icon {{ icon('backups') }}"></i> <span>Backups</span></a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('log') }}"><i class="nav-icon {{ icon('logs') }}"></i> <span>Logs</span></a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('email-log') }}"><i class="nav-icon {{ icon('email') }}"></i> <span>Emails</span></a></li>

