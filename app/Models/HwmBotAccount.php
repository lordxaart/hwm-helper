<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property string $login
 * @property string $password
 * @property string | null $email
 * @property bool $is_blocked
 * @property int $target
 * @property int $success_requests
 * @property int $error_requests
 * @property Carbon | null $last_success_request
 * @property Carbon | null $last_error_request
 */
class HwmBotAccount extends Model
{
    use CrudTrait;

    public const TARGET_ALL = 0;
    public const TARGET_WEB = 1;
    public const TARGET_CLI = 2;

    public $timestamps = false;

    protected $dateFormat = 'Y-m-d H:i:s.u';

    protected $fillable = [
        'login',
        'password',
        'email',
        'target',
        'is_blocked',
    ];

    protected static function boot()
    {
        parent::boot();

        if (!isAdminPanel()) {
            static::addGlobalScope('active', function (Builder $builder) {
                $builder->where('is_blocked', 0);
            });
        }
    }

    public static function getRandomForTarget($target = self::TARGET_ALL): ?self
    {
        $query = self::query();

        if ($target !== self::TARGET_ALL) {
            $query->whereIn('target', [$target, self::TARGET_ALL]);
        }

        return $query->inRandomOrder()->first();
    }

    public function touchSuccessRequest(): bool
    {
        $this->last_success_request = now()->format($this->dateFormat);
        $this->success_requests = $this->success_requests + 1;

        return $this->save();
    }

    public function touchErrorRequest(): bool
    {
        $this->last_error_request = now()->format($this->dateFormat);
        $this->error_requests = $this->error_requests + 1;

        return $this->save();
    }

    public function block()
    {
        $this->is_blocked = 1;
        $this->save();
    }
}
