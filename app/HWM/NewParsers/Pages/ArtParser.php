<?php

declare(strict_types=1);

namespace App\HWM\NewParsers\Pages;

use App\HWM\Entities\Res;
use App\HWM\Exceptions\ParserErrors\EmptyArtPage;
use App\HWM\Exceptions\ParserErrors\UnexpectedPage;
use App\HWM\Helpers\HwmHelper;
use App\HWM\HtmlParsers\Interfaces\HtmlNodeInterface;
use App\HWM\NewParsers\ParserException;
use App\HWM\Repositories\ResourceRepository;
use App\HWM\Repositories\SkillRepository;
use Illuminate\Support\Arr;

class ArtParser extends AbstractPageParser
{
    public const ART_BLOCK_SELECTOR = '.art_info_container_block';
    public const ART_TITLE_SELECTOR = '.global_container_block_header';
    public const IMG_BLOCK = '.art_info_left_block';
    public const DESC_BLOCK = '.art_info_desc';
    public const TAG_IMG_SECOND = 'img.cre_mon_image2';

    protected array $texts = [
        RU => [
            'need_level' => 'Требуемый уровень',
            'durability' => 'Прочность',
            'ammunition_points' => 'Очки амуниции',
            'repair_cost' => 'Стоимость ремонта',
            'description' => 'Описание',
            'cost' => 'Стоимость',
            'as_art_from_shop' => 'Обладает статусом, как "артефакт из магазина"',
            'modificators' => 'Модификаторы',
            'modifiers' => 'Характеристики',
        ],
        EN => [
            'need_level' => 'Required level',
            'durability' => 'Durability',
            'ammunition_points' => 'Ammunition points',
            'repair_cost' => 'Repairing cost',
            'description' => 'Description',
            'cost' => 'Cost',
            'as_art_from_shop' => 'Qualifies as a shop artifact',
            'modificators' => 'Modifiers',
            'modifiers' => 'Modifiers',
        ],
    ];

    protected string $expectedArtId;

    public function __construct(HtmlNodeInterface $htmlNode, string $artId)
    {
        parent::__construct($htmlNode);

        $this->expectedArtId = $artId;
    }

    public function handle(): array
    {
        if (!$artBlock = $this->htmlNode->find(self::ART_BLOCK_SELECTOR)) {
            throw new UnexpectedPage('art');
        }

        $title = $artBlock->find(self::ART_TITLE_SELECTOR)?->asString();

        if (!$title) {
            throw new EmptyArtPage($this->expectedArtId);
        }

        try {
            $imgBlock = $this->htmlNode->find(self::IMG_BLOCK);
            $descBlock = $this->htmlNode->find(self::DESC_BLOCK);
            $descHtml = $descBlock?->asHtml();

            if (!$imgBlock || !$descBlock || !$descHtml) {
                throw new \Exception('Empty img or desc block');
            }

            // Image
            $imgTag = $imgBlock->find(self::TAG_IMG_SECOND);
            if (!$imgTag) {
                $imgTag = $imgBlock->find('img');
            }
            $image = $imgTag?->getAttribute('src');

            // Need level
            $cutText = getTextBetweenValues($descHtml, '<b>' . $this->getTransText('need_level') . ':</b>', '<br');
            $needLevel = intval($cutText);


            // Strength
            $cutText = getTextBetweenValues($descHtml, '<b>' . $this->getTransText('durability') . ':</b>', '<br');
            $strength = intval($cutText);

            // OA
            $cutText = getTextBetweenValues($descHtml, '<b> ' . $this->getTransText('ammunition_points') . ':</b>', '<br');
            $oa = intval($cutText) ?: null;

            // Repair
            $repair = null;
            $repairText = $this->getTransText('repair_cost');
            $allHtml = $this->htmlNode->asHtml();
            if (str_contains($allHtml, $repairText . ':')) {
                $cutText = getTextBetweenValues($allHtml, $repairText . ':', '<br />');
                $repair = HwmHelper::clearIntValue($cutText);
            }

            // Description
            $description = getTextBetweenValues($descHtml, '<b>' . $this->getTransText('description') . ':</b><br />', '<br');

            // From shop
            $fromShop = str_contains($descHtml, '<b> ' . $this->getTransText('cost') . ':</b>');
            $asFromShop = str_contains($descHtml, $this->getTransText('as_art_from_shop'));

            $shopPrice = null;
            if (str_contains($descHtml, $this->getTransText('cost') . ':')) {
                $shopPrice = $this->getPriceFromResources();
            }
            // Features
            $features = $this->parseFeatures($descHtml);

            // Set id
            $setId = null;
            $setName = null;
            $link = $descBlock->find('i font[color=green] a');

            if ($link) {
                $setId = getTextBetweenValues($link->asHtml(), 'help.php?section=40#', '>');
                $setId = trim($setId, '"');
                $setId = trim($setId, '\'');
                $setName = trim(strip_tags($link->asString()), '"');
            }

            $marketCategory = null;
            $marketLink = $artBlock->find('a[href^=auction.php?]')?->getAttribute('href');
            if ($marketLink) {
                $query = Arr::last(explode('?', $marketLink));
                parse_str($query, $params);
                $marketCategory = $params['cat'] ?? null;
            }

            $artCategory = null;
            $shopLink = $artBlock->find('a[href^=shop.php?]')?->getAttribute('href');
            if ($shopLink) {
                $query = Arr::last(explode('?', $shopLink));
                $query = Arr::first(explode('#', $query));
                parse_str($query, $params);
                $artCategory = $params['cat'] ?? null;
            }
        } catch (\Exception $e) {
            throw new ParserException($this->expectedArtId . ': ' . $e->getMessage(), $e);
        }

        $art = [
            'hwm_id' => $this->expectedArtId,
            'title' => $title,
            'image' => $image,
            'need_level' => $needLevel,
            'strength' => $strength,
            'oa' => $oa,
            'repair' => $repair,
            'description' => $description,
            'modifiers' => $features,
            'set_id' => $setId,
            'set_name' => $setName,
            'from_shop' => $fromShop,
            'as_from_shop' => $asFromShop,
            'shop_price' => $shopPrice ?: null,
            'art_category' => $artCategory,
            'market_category' => $marketCategory,
        ];

        $this->validator->validateEntity('art', $art);

        return $art;
    }

    private function getPriceFromResources(): ?int
    {
        if (!$artBlock = $this->htmlNode->find(self::ART_BLOCK_SELECTOR)) {
            return null;
        }

        $table = $artBlock->find('table');

        if (!$table) {
            return null;
        }

        $list = $table->findAll('td');

        $resourcesRepository = new ResourceRepository($this->getLocale());
        $resources = [];

        for ($i = 0; $i < count($list); $i++) {
            if ($img = $list[$i]->find('img')) {
                $key = $resourcesRepository->getIdByTitle($img->getAttribute('title') ?: '');
                $resources[$key] = HwmHelper::clearIntValue($list[$i + 1]->asString());
            }
        }

        $price = 0;
        foreach ($resources as $resource => $value) {
            $price += Res::convertToGold($resource, $value);
        }

        return $price;
    }

    private function parseFeatures(string $html): array
    {
        $features = [];

        try {
            $parser = $this->htmlNode::loadFromString(
                getTextBetweenValues(
                    $html,
                    $this->getTransText('modifiers') . ':</b>',
                    '<br',
                    false,
                )
            );
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return [];
        }

        $lastIndex = null;
        $skills = new SkillRepository($this->getLocale());

        foreach ($parser->findAll('td') as $key => $td) {
            if ($key % 2) {
                $features[$lastIndex] = str_replace('&nbsp;', '', $td->getFirstChild()?->asString() ?: '');
            } else {
                $lastIndex = $skills->getIdByTitle($td->getFirstChild()?->getAttribute('title') ?: '') ?: UNDEFINED;
            }
        }

        return $features;
    }
}
