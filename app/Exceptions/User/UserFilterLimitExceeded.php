<?php

declare(strict_types=1);

namespace App\Exceptions\User;

use App\Core\ValueObject\UserId;
use App\Exceptions\ClientError;
use Throwable;

class UserFilterLimitExceeded extends ClientError
{
    public function __construct(UserId $userId, int $maxFilterCount, Throwable $previous = null)
    {
        $message = trans(
            'errors.exceeded_filters_of_monitored_arts',
            ['max_filters' => $maxFilterCount]
        );

        $this->context = [
            'user_id' => $userId->value,
        ];

        parent::__construct(ClientError::MAX_FILTERS, $message, $previous);
    }
}
