import * as elements from '../../../../hwm/elements.json';
import * as links from '../../../../hwm/links.json';
import * as videoGuides from '../../../../hwm/video_guides.json';

export {
    elements,
    links,
    videoGuides,
};
