<?php

declare(strict_types=1);

namespace App\Providers;

use App\Core\Enums\LaravelServiceAlias;
use App\Repositories\NotificationRepository;
use App\Services\ActivityLogger\ActivityLogger;
use App\Services\ActivityLogger\ActivityLoggerMonolog;
use App\Services\LotFilter\LotFiltersService;
use App\Services\Other\RandomUserAgent;
use App\Components\LocalizationDetect;
use App\Services\ImageOptimize\ImageOptimize;
use App\Services\Settings\GeneralSettings;
use App\Services\Subscriptions\SubscriptionService;
use App\Services\Telegram\TelegramApi;
use App\Services\UserFeatures\UserFeatureService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Log\LogManager;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public array $singletons = [
        NotificationRepository::class,
        LotFiltersService::class,
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (config('app.secure_scheme', false)) {
            URL::forceScheme('https');
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Register App Settings

        $this->app->bind(ImageOptimize::class, function (Application $app) {
            /** @var GeneralSettings $settings */
            $settings = $app->get(GeneralSettings::class);
            return new ImageOptimize([
                'quality' => $settings->optimize_img_quality,
            ]);
        });

        $this->app->singleton(LocalizationDetect::class, function () {
            return new LocalizationDetect(config('localization.locales'));
        });

        $this->app->singleton(RandomUserAgent::class, function () {
            return new RandomUserAgent(config('app.agent_list_path'));
        });

        $this->app->singleton(TelegramApi::class, function () {
            return new TelegramApi(config('services.telegram.client_secret'));
        });

        $this->app->singleton(ActivityLogger::class, function (Application $app) {
            /** @var LogManager $logger */
            $logger = $app->get(LaravelServiceAlias::LOGGER->value);

            return new ActivityLoggerMonolog($logger->channel('activity'));
        });

        $this->app->scoped(UserFeatureService::class);
        $this->app->scoped(SubscriptionService::class);
    }
}
