<?php

declare(strict_types=1);

namespace App\Services\Parser\Storages;

use Illuminate\Support\Facades\Redis;

class StorageForNewLots
{
    public const STORAGE_CACHE_KEY = 'current_parsing_lot_ids';
    public const MAX_SECONDS_FOR_LIFE_PARSER = 30;

    public function has(string $id): bool
    {
        /** @phpstan-ignore-next-line */
        return Redis::exists($this->getId($id)) == true;
    }

    public function add(string $id): bool
    {
        $inserted = false;

        if (!$this->has($id)) {
            /** @phpstan-ignore-next-line  */
            Redis::transaction(function ($redis) use ($id) {
                $redis->sadd(self::STORAGE_CACHE_KEY, $id);
                $redis->set($this->getId($id), 1, 'ex', self::MAX_SECONDS_FOR_LIFE_PARSER);
            });
            $inserted = true;
        }

        if (randomChance(5)) {
            $items = $this->all();
            foreach ($items as $key) {
                /** @phpstan-ignore-next-line */
                if (!Redis::exists($this->getId($key))) {
                    /** @phpstan-ignore-next-line */
                    Redis::srem(self::STORAGE_CACHE_KEY, $key);
                }
            }
        }

        return $inserted;
    }

    public function delete(string $id): self
    {
        /** @phpstan-ignore-next-line  */
        Redis::transaction(function ($redis) use ($id) {
            $redis->del($this->getId($id));
            $redis->srem(self::STORAGE_CACHE_KEY, $id);
        });

        return $this;
    }

    public function all(): array
    {
        /** @phpstan-ignore-next-line */
        return Redis::smembers(self::STORAGE_CACHE_KEY);
    }

    public function getMaxId(): ?int
    {
        $items = $this->all();

        return count($items) ? intval(max($items)) : null;
    }

    public function getMinId(): ?int
    {
        $items = $this->all();

        return count($items) ? intval(min($items)) : null;
    }

    private function getId(string $id): string
    {
        return self::STORAGE_CACHE_KEY . ':' . $id;
    }
}
