<?php
return [
    "accepted" => "Вы должны принять <strong>:attribute</strong>.",
    "active_url" => "Поле <strong>:attribute</strong> содержит недействительный URL.",
    "after" => "В поле <strong>:attribute</strong> должна быть дата после :date.",
    "after_or_equal" => "В поле <strong>:attribute</strong> должна быть дата после или равняться :date.",
    "alpha" => "Поле <strong>:attribute</strong> может содержать только буквы.",
    "alpha_dash" => "Поле <strong>:attribute</strong> может содержать только буквы, цифры, дефис и нижнее подчеркивание.",
    "alpha_num" => "Поле <strong>:attribute</strong> может содержать только буквы и цифры.",
    "array" => "Поле <strong>:attribute</strong> должно быть массивом.",
    "attributes" => [
        "address" => "Адрес",
        "age" => "Возраст",
        "available" => "Доступно",
        "base_strength" => "Базовая прочность",
        "city" => "Город",
        "content" => "Контент",
        "country" => "Страна",
        "craft" => "Крафт",
        "current_strength" => "Текущая прочность",
        "date" => "Дата",
        "day" => "День",
        "description" => "Описание",
        "email" => "E-Mail адрес",
        "excerpt" => "Выдержка",
        "first_name" => "Имя",
        "gender" => "Пол",
        "hour" => "Час",
        "last_name" => "Фамилия",
        "login" => "Логин",
        "max_monitored_arts" => "Максимальное количество артефактов для мониторинга",
        "max_monitored_filters" => "Максимальное количество фильтров",
        "minute" => "Минута",
        "mobile" => "Моб. номер",
        "month" => "Месяц",
        "name" => "Имя",
        "old_password" => "Cтарый пароль",
        "password" => "Пароль",
        "password_confirmation" => "Подтверждение пароля",
        "phone" => "Телефон",
        "price_per_fight" => "Цена за бой",
        "second" => "Секунда",
        "sex" => "Пол",
        "size" => "Размер",
        "start_price" => "Цена",
        "time" => "Время",
        "title" => "Наименование",
        "username" => "Никнейм",
        "year" => "Год"
    ],
    "before" => "В поле <strong>:attribute</strong> должна быть дата до :date.",
    "before_or_equal" => "В поле <strong>:attribute</strong> должна быть дата до или равняться :date.",
    "between" => [
        "array" => "Количество элементов в поле <strong>:attribute</strong> должно быть между :min и :max.",
        "file" => "Размер файла в поле <strong>:attribute</strong> должен быть между :min и :max Килобайт(а).",
        "numeric" => "Поле <strong>:attribute</strong> должно быть между :min и :max.",
        "string" => "Количество символов в поле <strong>:attribute</strong> должно быть между :min и :max."
    ],
    "boolean" => "Поле <strong>:attribute</strong> должно иметь значение логического типа.",
    "confirmed" => "Поле <strong>:attribute</strong> не совпадает с подтверждением.",
    "craft" => "Формат поля :attribute не соответствует строке крафта.",
    "custom" => ["attribute-name" => ["rule-name" => "custom-message"]],
    "date" => "Поле <strong>:attribute</strong> не является датой.",
    "date_equals" => ":Attribute должен быть датой, равной :date.",
    "date_format" => "Поле <strong>:attribute</strong> не соответствует формату :format.",
    "different" => "Поля <strong>:attribute</strong> и :other должны различаться.",
    "digits" => "Длина цифрового поля <strong>:attribute</strong> должна быть :digits.",
    "digits_between" => "Длина цифрового поля <strong>:attribute</strong> должна быть между :min и :max.",
    "dimensions" => "Поле <strong>:attribute</strong> имеет недопустимые размеры изображения.",
    "distinct" => "Поле <strong>:attribute</strong> содержит повторяющееся значение.",
    "email" => "Поле <strong>:attribute</strong> должно быть действительным электронным адресом.",
    "ends_with" => ":Attribute должен заканчиваться одним из следующих: :values",
    "exists" => "Выбранное значение для <strong>:attribute</strong> некорректно.",
    "file" => "Поле <strong>:attribute</strong> должно быть файлом.",
    "filled" => "Поле <strong>:attribute</strong> обязательно для заполнения.",
    "gt" => [
        "array" => "Количество элементов в поле <strong>:attribute</strong> должно быть больше :value.",
        "file" => "Размер файла в поле <strong>:attribute</strong> должен быть больше :value Килобайт(а).",
        "numeric" => "Поле <strong>:attribute</strong> должно быть больше :value.",
        "string" => "Количество символов в поле <strong>:attribute</strong> должно быть больше :value."
    ],
    "gte" => [
        "array" => "Количество элементов в поле <strong>:attribute</strong> должно быть больше или равно :value.",
        "file" => "Размер файла в поле <strong>:attribute</strong> должен быть больше или равен :value Килобайт(а).",
        "numeric" => "Поле <strong>:attribute</strong> должно быть больше или равно :value.",
        "string" => "Количество символов в поле <strong>:attribute</strong> должно быть больше или равно :value."
    ],
    "image" => "Поле <strong>:attribute</strong> должно быть изображением.",
    "in" => "Выбранное значение для <strong>:attribute</strong> ошибочно.",
    "in_array" => "Поле <strong>:attribute</strong> не существует в :other.",
    "integer" => "Поле <strong>:attribute</strong> должно быть целым числом.",
    "ip" => "Поле <strong>:attribute</strong> должно быть действительным IP-адресом.",
    "ipv4" => "Поле <strong>:attribute</strong> должно быть действительным IPv4-адресом.",
    "ipv6" => "Поле <strong>:attribute</strong> должно быть действительным IPv6-адресом.",
    "json" => "Поле <strong>:attribute</strong> должно быть JSON строкой.",
    "lt" => [
        "array" => "Количество элементов в поле <strong>:attribute</strong> должно быть меньше :value.",
        "file" => "Размер файла в поле <strong>:attribute</strong> должен быть меньше :value Килобайт(а).",
        "numeric" => "Поле <strong>:attribute</strong> должно быть меньше :value.",
        "string" => "Количество символов в поле <strong>:attribute</strong> должно быть меньше :value."
    ],
    "lte" => [
        "array" => "Количество элементов в поле <strong>:attribute</strong> должно быть меньше или равно :value.",
        "file" => "Размер файла в поле <strong>:attribute</strong> должен быть меньше или равен :value Килобайт(а).",
        "numeric" => "Поле <strong>:attribute</strong> должно быть меньше или равно :value.",
        "string" => "Количество символов в поле <strong>:attribute</strong> должно быть меньше или равно :value."
    ],
    "max" => [
        "array" => "Количество элементов в поле <strong>:attribute</strong> не может превышать :max.",
        "file" => "Размер файла в поле <strong>:attribute</strong> не может быть более :max Килобайт(а).",
        "numeric" => "Поле <strong>:attribute</strong> не может быть более :max.",
        "string" => "Количество символов в поле <strong>:attribute</strong> не может превышать :max."
    ],
    "max_monitored_arts" => "Максимум артефактов для мониторинга",
    "max_monitored_filters" => "Максимум фильтров",
    "max_terms_in_filter" => "Максимальное количество условий для фильтра :max",
    "mimes" => "Поле <strong>:attribute</strong> должно быть файлом одного из следующих типов: :values.",
    "mimetypes" => "Поле <strong>:attribute</strong> должно быть файлом одного из следующих типов: :values.",
    "min" => [
        "array" => "Количество элементов в поле <strong>:attribute</strong> должно быть не менее :min.",
        "file" => "Размер файла в поле <strong>:attribute</strong> должен быть не менее :min Килобайт(а).",
        "numeric" => "Поле <strong>:attribute</strong> должно быть не менее :min.",
        "string" => "Количество символов в поле <strong>:attribute</strong> должно быть не менее :min."
    ],
    "monitored_filter" => "Значение фильтра указано неверно.",
    "not_in" => "Выбранное значение для <strong>:attribute</strong> ошибочно.",
    "not_regex" => "Выбранный формат для <strong>:attribute</strong> ошибочный.",
    "numeric" => "Поле <strong>:attribute</strong> должно быть числом.",
    "old_password" => "Старый пароль указано неверно.",
    "present" => "Поле <strong>:attribute</strong> должно присутствовать.",
    "recaptcha" => "Ошибка при проверке Google reCAPTCHA",
    "regex" => "Поле <strong>:attribute</strong> имеет ошибочный формат.",
    "required" => "Поле <strong>:attribute</strong> обязательно для заполнения.",
    "required_if" => "Поле <strong>:attribute</strong> обязательно для заполнения, когда :other равно :value.",
    "required_unless" => "Поле <strong>:attribute</strong> обязательно для заполнения, когда :other не равно :values.",
    "required_with" => "Поле <strong>:attribute</strong> обязательно для заполнения, когда :values указано.",
    "required_with_all" => "Поле <strong>:attribute</strong> обязательно для заполнения, когда :values указано.",
    "required_without" => "Поле <strong>:attribute</strong> обязательно для заполнения, когда :values не указано.",
    "required_without_all" => "Поле <strong>:attribute</strong> обязательно для заполнения, когда ни одно из :values не указано.",
    "same" => "Значения полей <strong>:attribute</strong> и :other должны совпадать.",
    "same_password" => "Старый и новый пароли совпадают. Пожалуйста, введите другой пароль",
    "size" => [
        "array" => "Количество элементов в поле <strong>:attribute</strong> должно быть равным :size.",
        "file" => "Размер файла в поле <strong>:attribute</strong> должен быть равен :size Килобайт(а).",
        "numeric" => "Поле <strong>:attribute</strong> должно быть равным :size.",
        "string" => "Количество символов в поле <strong>:attribute</strong> должно быть равным :size."
    ],
    "social_provider" => "Поле <strong>:attribute</strong> имеет неверное значение. Доступные следующие провайдеры: :values",
    "starts_with" => "Поле :attribute должено начинаться с одного из следующих: :values",
    "string" => "Поле <strong>:attribute</strong> должно быть строкой.",
    "timezone" => "Поле <strong>:attribute</strong> должно быть действительным часовым поясом.",
    "unique" => "Такое значение поля <strong>:attribute</strong> уже существует.",
    "uploaded" => "Загрузка поля <strong>:attribute</strong> не удалась.",
    "url" => "Поле <strong>:attribute</strong> имеет ошибочный формат.",
    "uuid" => "Поле <strong>:attribute</strong> должно быть корректным UUID."
];
