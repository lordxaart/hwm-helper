<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SuperAdminSeeder extends Seeder
{
    public function run()
    {
        try {
            User::getSuperAdmin();

            $this->command->warn('Super admin already exists');
        } catch (ModelNotFoundException $e) {
            $user = new User();
            $user->email = config('auth.super-admin.email');
            $user->password = config('auth.super-admin.password');

            if (!$user->email || !$user->password) {
                $this->command->error('Setup up Super Admin credentials, see config/auth.php[super-admin]');
                exit();
            }

            $user->email_verified_at = $user->freshTimestamp();
            $user->save();

            $user->assignRole(Role::SUPER_ADMIN);

            $this->command->info('Super admin is added');
        }
        // allow only one super admin
    }
}
