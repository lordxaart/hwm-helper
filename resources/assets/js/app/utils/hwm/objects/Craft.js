import has from 'lodash/has';

import { elements } from 'JS/app/utils/hwm/static';
import i18n from 'JS/app/utils/localization';

export default class Craft {
    static modificators = ['I', 'D', 'N', 'E', 'A', 'F', 'W'];

    static artTypes = ['weapon', 'armor', 'neutral'];

    static artTypesTexts = {
        weapon: i18n.t('hwm.craft.weapon'),
        armor: i18n.t('hwm.craft.armor'),
        neutral: i18n.t('hwm.craft.neutral'),
    };

    static countOfElements = {
        1: 1,
        2: 2,
        3: 4,
        4: 6,
        5: 9,
        6: 12,
        7: 15,
        8: 19,
        9: 24,
        10: 30,
        11: 37,
        12: 45,
    };

    static countFernFlower = {
        0: 0,
        1: 2,
        2: 6,
        3: 12,
        4: 20,
        5: 30,
    };

    static elementsForMod = {
        weapon: {
            E: ['meteorit', 'badgrib'],
            A: ['wind_flower', 'witch_flower'],
            F: ['fire_crystal', 'tiger_tusk'],
            W: ['ice_crystal', 'snake_poison'],
            I: ['moon_stone', 'abrasive'],
        },
        armor: {
            E: ['meteorit'],
            A: ['wind_flower'],
            F: ['fire_crystal'],
            W: ['ice_crystal'],
            D: ['meteorit', 'abrasive'],
        },
        neutral: {
            E: ['meteorit', 'tiger_tusk'],
            A: ['wind_flower', 'meteorit'],
            F: ['fire_crystal', 'abrasive'],
            W: ['ice_crystal', 'witch_flower'],
            N: ['wind_flower', 'tiger_tusk'],
        },
    };

    static artCategoryMap = {
        helm: 'armor',
        necklace: 'neutral',
        cuirass: 'armor',
        cloack: 'neutral',
        weapon: 'weapon',
        shield: 'armor',
        boots: 'armor',
        ring: 'neutral',
    };

    craftObject = {};

    elementPrices = {};

    elementsCount = {};

    totalPrice = 0;

    type = 'weapon';

    constructor(stringCraft = '', type = '') {
        this.loadElements();
        this.init(stringCraft, type);
    }

    init(stringCraft = '', type = '') {
        this.stringCraft = stringCraft;
        this.setType(type);
        this.parseString();
        this.calculateElementsCount();
    }

    setType(type) {
        if (Craft.artTypes.includes(type)) {
            this.type = type;
        } else if (has(Craft.artCategoryMap, type)) {
            this.type = Craft.artCategoryMap[type];
        }
    }

    parseString(string = '') {
        // eslint-disable-next-line no-redeclare
        string = string || this.stringCraft;

        string = string.toUpperCase();

        this.craftObject = {};

        Craft.modificators.forEach((mod) => {
            // eslint-disable-next-line no-bitwise
            if (string.includes(mod)) {
                const index = string.indexOf(mod);
                const valueLength = !Number.isNaN(string[index + 2]) ? 2 : 1;
                const value = Number.parseInt(string.substr(index + 1, valueLength), 10);
                if (value >= 1) {
                    this.craftObject[mod] = value;
                }
            }
        });

        if (has(this.craftObject, 'I')) {
            this.type = 'weapon';
        }
        if (has(this.craftObject, 'D')) {
            this.type = 'armor';
        }
        if (has(this.craftObject, 'N')) {
            this.type = 'neutral';
        }

        return this.craftObject;
    }

    craftDesc() {
        const texts = {};
        Object.keys(this.craftObject).forEach((mod) => {
            texts[mod] = this.getParseTextForMode(mod, this.type, this.craftObject[mod]);
        });

        return texts;
    }

    asString() {
        let string = '';
        Object.keys(this.craftObject).forEach((mod) => {
            string += mod.toUpperCase() + this.craftObject[mod];
        });

        return string;
    }

    loadElements() {
        this.elements = {};
        elements.forEach((element) => {
            this.elements[element] = i18n.t(`hwm.elements.${element}`);
        });
    }

    loadPrices(prices) {
        Object.keys(prices).forEach((element) => {
            if (this.elements[element]) {
                this.elementPrices[element] = prices[element].minPrice;
            }
        });

        this.refreshPrice();
    }

    refreshPrice() {
        this.totalPrice = 0;
        const result = {};
        let totalPrice = 0;

        try {
            result.fiveMod = this.elementPrices.fern_flower * (Craft.countFernFlower[Object.keys(this.craftObject).length]);
            totalPrice += result.fiveMod;
            Object.keys(this.craftObject).forEach((mod) => {
                let price = 0;
                Craft.elementsForMod[this.type][mod].forEach((el) => {
                    price += this.elementPrices[el];
                });
                result[mod] = Number.parseInt(price * Craft.countOfElements[this.craftObject[mod]], 10);
                totalPrice += result[mod];
            });
        } catch (e) {
            console.error(e);
        }

        this.totalPrice = totalPrice;

        return this.totalPrice;
    }

    isValid() {
        return Object.keys(this.craftObject).length;
    }

    getParseTextForMode(mod) {
        const value = this.craftObject[mod];
        const transKey = `hwm.craft.${this.type}_${mod.toLowerCase()}`;
        let text = '';

        if (this.type === 'weapon') {
            text = i18n.t(transKey, { value });
        }
        if (this.type === 'armor') {
            if (mod === 'D') {
                text = i18n.t(transKey, { value });
            } else {
                text = i18n.t(transKey, { value: value * 2 });
            }
        }
        if (this.type === 'neutral') {
            if (mod === 'N') {
                text = i18n.t(transKey, { value: value * 2 });
            } else {
                text = i18n.t(transKey, { value1: value * 2, value2: value * 3 });
            }
        }

        return text;
    }

    calculateElementsCount() {
        this.elementsCount = {};
        elements.forEach((el) => {
            this.elementsCount[el] = 0;
        });

        if (Object.keys(this.craftObject).length) {
            this.elementsCount.fern_flower += Craft.countFernFlower[Object.keys(this.craftObject).length];
        }

        Object.keys(this.craftObject).forEach((mod) => {
            Craft.elementsForMod[this.type][mod].forEach((el) => {
                this.elementsCount[el] += Craft.countOfElements[this.craftObject[mod]];
            });
        });
    }
}
