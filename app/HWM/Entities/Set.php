<?php

declare(strict_types=1);

namespace App\HWM\Entities;

use App\HWM\Helpers\HwmLink;
use App\HWM\Entities\Interfaces\HasLinkOnPage;
use App\HWM\Repositories\ArtSetRepository;
use App\HWM\Repositories\Interfaces\EntityRepositoryInterface;

class Set extends HwmEntityWithModel implements HasLinkOnPage
{
    public ?string $description;
    public ?array $arts;

    public function getHwmLink(): string
    {
        return HwmLink::get(HwmLink::SETS) . '#' . $this->getHwmId();
    }

    public function getRepository(): EntityRepositoryInterface
    {
        return app(ArtSetRepository::class);
    }
}
