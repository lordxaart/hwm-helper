<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UpdateSettings;
use App\Services\Settings\GeneralSettings;
use App\Services\UserFeatures\UserFeatureRepository;
use phpDocumentor\Reflection\TypeResolver;

class AppSettingController
{
    public function index()
    {
        return view('admin.settings');
    }

    public function update(UpdateSettings $request, GeneralSettings $settings)
    {
        foreach ($settings->toArray() as $key => $value) {
            $val = $request->input('settings')[$key] ?? false;
            $val = $val == 'on' ? true : $val;
            $currentType = gettype($settings->{$key});
            settype($val, $currentType);
            $settings->{$key} = $val;
        }

        $settings->save();

        /** @var UserFeatureRepository $repo */
        $repo = app(UserFeatureRepository::class);
        $repo->clearCache();

        \Alert::success(trans('backpack::crud.update_success'))->flash();

        return redirect(backpack_url('app-settings'));
    }
}
