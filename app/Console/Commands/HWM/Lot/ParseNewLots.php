<?php

declare(strict_types=1);

namespace App\Console\Commands\HWM\Lot;

use App\Console\Commands\Command;
use App\Services\Parser\NewLotsParser;
use App\Services\Parser\NewMissedLotsParser;

class ParseNewLots extends Command
{
    /** @var string */
    protected $signature = 'hwm:parser:new_lots {--reverse} {--missed} {--ended} {--sleep=}';

    /** @var string  */
    protected $description = 'Parsing new lots. --reverse: start of begin, 1,2...; --missed: parsing from missing storage, collects command CollectMissLots; --ended: parse only ended (3 days of start)';

    protected bool $logStartFinish = false;

    protected int $currentRuns = 0;

    protected int $maxRuns = 100;

    public function handle(): void
    {
        $sleep = intval($this->option('sleep'));
        if ($sleep) {
            sleep($sleep);
        }

        if ($this->option('missed')) {
            $this->missedParser();
        } else {
            $this->endedParser();
        }
    }

    protected function newParser(): void
    {
        throw new \DomainException('New Parser is not available !!!');
    }

    protected function missedParser(): void
    {
        $parser = new NewMissedLotsParser($this->getOutput());

        do {
            $parser->run();
            $this->currentRuns ++;
        } while ($this->currentRuns <= $this->maxRuns);

        sleep(5);
    }

    protected function endedParser(): void
    {
        $parser = new NewLotsParser($this->getOutput());

        do {
            $parser->run();
            $this->currentRuns ++;
        } while ($this->currentRuns <= $this->maxRuns);
    }
}
