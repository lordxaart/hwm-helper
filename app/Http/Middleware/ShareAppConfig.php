<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Repositories\NotificationRepository;
use App\Services\Telegram\TelegramApi;
use Closure;
use Illuminate\Http\Request;

class ShareAppConfig
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle(Request $request, Closure $next): mixed
    {
        // Add custom config
        $this->updateConfig();

        return $next($request);
    }

    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function updateConfig(): void
    {
        $channelRepository = app(NotificationRepository::class);

        \Config::set('view.sprite_key', \Storage::exists('sprite_key.txt') ? \Storage::get('sprite_key.txt') : date('Y-m-d-H'));
        \Config::set('app.locales', config('localization.locales'));
        \Config::set('user.notificationChannels', $channelRepository->getChannelAliases());
        \Config::set('app.telegramBotLink', TelegramApi::getBotLink());
    }
}
