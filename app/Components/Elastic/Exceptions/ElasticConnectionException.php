<?php

declare(strict_types=1);

namespace App\Components\Elastic\Exceptions;

class ElasticConnectionException extends \Exception
{
    //
}
