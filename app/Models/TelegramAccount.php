<?php

namespace App\Models;

use App\Services\Telegram\Entities\Update;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Services\Telegram\Entities\User as TelegramUser;
use Illuminate\Support\Str;

/**
 * @property int $id
 * @property int $user_id
 * @property int $chat_id
 * @property array $user_data
 * @property bool $is_active
 * @property bool $is_blocked
 * @property TelegramUser | null $telegram_user
 * @method static active()
 */
class TelegramAccount extends Model
{
    public $incrementing = false;
    protected $table = 'telegram_accounts';
    protected $casts = [
        'user_data' => 'array'
    ];
    protected $fillable = [
        'chat_id',
        'user_data',
        'is_active',
        'is_blocked',
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('active', function (Builder $builder) {
            /** @var Builder|MonitoredArt $builder */
            $builder->active();
        });
    }

    public static function logUpdate(Update $update): bool
    {
        if (\DB::table('telegram_bot_log')->where('update_id', $update->update_id)->exists()) {
            return false;
        }

        \DB::table('telegram_bot_log')->insert([
            'id' => Str::uuid()->toString(),
            'update_id' => $update->update_id,
            'message' => $update->message ? json_encode($update->message) : null,
            'command' => $update->message?->getCommand(),
            'from' => $update->message?->from?->id,
            'created_at' => now()->format('Y-m-d H:i:s.u'),
        ]);

        return true;
    }

    public function scopeActive(Builder $builder): void
    {
        $builder->where('is_active', 1);
    }

    public function getTelegramUserAttribute(): ?TelegramUser
    {
        if (!isset($this->user_data['id'])) {
            return null;
        }

        return new TelegramUser($this->user_data);
    }
}
