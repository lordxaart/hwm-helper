import config from 'JS/app/config';
import request from 'JS/app/utils/request';

function urlBase64ToUint8Array(base64String) {
    // eslint-disable-next-line no-mixed-operators
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        // eslint-disable-next-line
        .replace(/\-/g, '+')
        .replace(/_/g, '/');

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);
    // eslint-disable-next-line
    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

class WebPushService {
    _vapidPublicKey;

    constructor(validPublicKey) {
        this._vapidPublicKey = validPublicKey;
    }

    async requestForPushSubscription() {
        try {
            const subscription = await this.getSubscription();

            if (subscription) {
                return subscription;
            }

            const registration = await navigator.serviceWorker.ready;
            const serverKey = urlBase64ToUint8Array(this._vapidPublicKey);

            // eslint-disable-next-line no-return-await
            return await registration.pushManager.subscribe({
                userVisibleOnly: true,
                applicationServerKey: serverKey,
            });
        } catch (error) {
            console.error(error);
        }

        return null;
    }

    // eslint-disable-next-line class-methods-use-this
    async getSubscription() {
        try {
            const registration = await navigator.serviceWorker.ready;
            // eslint-disable-next-line no-return-await
            return await registration.pushManager.getSubscription();
        } catch (error) {
            console.error(error);
        }

        return null;
    }

    // eslint-disable-next-line class-methods-use-this
    async subscribe(subscription) {
        const key = subscription.getKey('p256dh');
        const token = subscription.getKey('auth');
        const contentEncoding = (PushManager.supportedContentEncodings || ['aesgcm'])[0];

        const data = {
            endpoint: subscription.endpoint,
            public_key: key ? btoa(String.fromCharCode.apply(null, new Uint8Array(key))) : null,
            auth_token: token ? btoa(String.fromCharCode.apply(null, new Uint8Array(token))) : null,
            encoding: contentEncoding,
        };

        // eslint-disable-next-line no-useless-catch
        try {
            await request.post('/api/v1/notifications/web-push/subscribe', data);
        } catch (e) {
            throw e;
        }
    }

    // eslint-disable-next-line class-methods-use-this
    async unsubscribe(subscription) {
        const data = {
            endpoint: subscription.endpoint,
        };

        // eslint-disable-next-line no-useless-catch
        try {
            await request.post('/api/v1/notifications/web-push/unsubscribe', data);
        } catch (e) {
            throw e;
        }
    }
}

const WebPush = new WebPushService(config.app.vapid_public_key);

export default WebPush;
