<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeArtMonitoredFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('art_monitored_filters', function (Blueprint $table) {
            $table->renameColumn('value', 'terms');
        });

        Schema::table('art_monitored_filters', function (Blueprint $table) {
            $table->text('terms')->change();
            $table->json('terms')->change();
            $table->string('alias')->index();
            $table->boolean('enable_browser_notification')->default(1);
            $table->boolean('enable_telegram_notification')->default(1);
            $table->boolean('enable_mail_notification')->default(1);
            $table->json('notification_shcedule')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('art_monitored_filters', function (Blueprint $table) {
            $table->dropColumn('alias');
            $table->dropColumn('enable_browser_notification');
            $table->dropColumn('enable_telegram_notification');
            $table->dropColumn('enable_mail_notification');
            $table->dropColumn('notification_shcedule');
        });
    }
}
