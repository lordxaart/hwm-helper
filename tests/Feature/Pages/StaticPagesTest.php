<?php

namespace Tests\Feature\Pages;

use Tests\TestCase;

class StaticPagesTest extends TestCase
{
    /**
     * @test
     */
    public function can_view_contacts_page()
    {
        $response = $this->get(self::CONTACTS_URL);

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function can_view_privacy_page()
    {
        $response = $this->get(self::PRIVACY_URL);

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function can_view_terms_page()
    {
        $response = $this->get(self::TERMS_URL);

        $response->assertStatus(200);
    }
}
