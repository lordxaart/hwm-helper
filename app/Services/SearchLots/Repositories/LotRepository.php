<?php

declare(strict_types=1);

namespace App\Services\SearchLots\Repositories;

use App\Services\SearchLots\ValueObjects\LimitOffset;
use App\Services\SearchLots\ValueObjects\LotFilters;
use App\Services\SearchLots\ValueObjects\OrderBy;
use App\Services\SearchLots\ValueObjects\Response;
use Countable;

interface LotRepository extends Countable
{
    public function findByFilters(
        LotFilters $filters,
        LimitOffset $limitOffset,
        OrderBy $orderBy,
        bool $exactTotal = false,
    ): Response;
}
