<?php

declare(strict_types=1);

namespace App\Services\ActivityLogger\ValueObjects;

use App\Models\User;

class Activity
{
    private string $action;
    private array $context = [];
    private ?User $causedBy = null;
    public readonly \DateTimeImmutable $createdAt;

    public function __construct(
        string $action,
        array $context = [],
    ) {
        $this->action = $action;
        $this->context = $context;
        $this->createdAt = new \DateTimeImmutable();
    }

    public static function action(string $action): self
    {
        return new self($action);
    }

    public function context(array $context): static
    {
        $this->context = $context;

        return $this;
    }

    public function causedBy(User $user = null): static
    {
        $this->causedBy = $user;

        return $this;
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function getContext(): array
    {
        return $this->context;
    }

    public function getCausedBy(): ?User
    {
        return $this->causedBy;
    }
}
