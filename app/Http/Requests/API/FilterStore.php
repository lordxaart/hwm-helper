<?php

declare(strict_types=1);

namespace App\Http\Requests\API;

use App\Components\Validators\Rules\Lot\IsEntityType;
use App\Components\Validators\Rules\Lot\IsLotType;
use App\Components\Validators\Rules\MaxTermsInFilter;
use App\Components\Validators\Rules\ValidFilterValue;
use Illuminate\Foundation\Http\FormRequest;

class FilterStore extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'lot_type' => [
                'required',
                new IsLotType(),
            ],
            'entity_type' => [
                'required',
                new IsEntityType(),
            ],
            'entity_id' => 'nullable|string',
            'terms' => [
                'required',
                new ValidFilterValue(),
                new MaxTermsInFilter(5),
            ],
        ];
    }
}
