<?php

declare(strict_types=1);

namespace App\Models;

use App\HWM\Enums\EntityType;
use App\HWM\Enums\LotType;
use App\Models\Casts\CastFilterTerms;
use App\Services\LotFilter\ValueObjects\FilterTermCollection;
use DateTimeImmutable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method static active() see scopeActive()
 * @property int $id
 * @property User $user
 * @property LotType $lot_type
 * @property EntityType $entity_type
 * @property string|null $entity_id
 * @property FilterTermCollection $terms
 * @property bool $is_active
 * @property DateTimeImmutable $created_at
 * @property DateTimeImmutable $updated_at
 */
class LotFilter extends Model
{
    use HasFactory;

    public const TABLE_NAME = 'lot_filters';

    protected $table = LotFilter::TABLE_NAME;

    protected $fillable = [
        'user_id',
        'lot_type',
        'entity_type',
        'entity_id',
        'terms',
        'is_active',
    ];

    /** @var array  */
    protected $casts = [
        'lot_type' => LotType::class,
        'entity_type' => EntityType::class,
        'terms' => CastFilterTerms::class,
        'is_active' => 'boolean',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
