@if(!$entry instanceof \App\Models\User || !$entry->isSuperAdmin())
	@if (!$entry instanceof \App\Models\User || !$entry->isDeleted())
		<a
			href="#"
			onclick="changeUserStatus(this)"
			data-route="{{ url($crud->route.'/'.$entry->getKey()) }}"
			class="btn btn-sm btn-link"
			data-type="delete"
			title="{{ trans('backpack::crud.delete') }}"
			data-toggle="tooltip"
			data-text="You are want delete account {{ $entry->email }}?"
		>
			<i class="fa fa-trash"></i>
		</a>
	@endif
	@if ($entry instanceof \App\Models\User && $entry->isDeleted())
		<a
			href="#"
			onclick="changeUserStatus(this)"
			data-route="{{ url($crud->route.'/'.$entry->getKey()).'/restore' }}"
			class="btn btn-sm btn-link"
			data-type="restore"
			data-text="You are want restore account {{ $entry->email }}?"
			title="Restore"
			data-toggle="tooltip"
		>
			<i class="fa fa-trash-restore"></i>
		</a>
	@endif
@endif