<?php

declare(strict_types=1);

namespace App\Exceptions\Social;

use App\Exceptions\ClientError;

class EmailScopeRequired extends ClientError
{
    public function __construct(string $socialDriver)
    {
        $code = ClientError::SOCIAL_EMAIL_SCOPE_REQUIRED;
        $message = ucfirst($socialDriver) . ': ' . trans('errors.email_scope_is_required') . ' .';

        parent::__construct($code, $message);
    }
}
