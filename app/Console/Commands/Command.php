<?php

declare(strict_types=1);

namespace App\Console\Commands;

use Illuminate\Console\Command as BaseCommand;
use Illuminate\Log\LogManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class Command extends BaseCommand
{
    protected $signature = 'base';
    protected ?LoggerInterface $logger;
    protected bool $logStartFinish = true;
    private ?string $commandId;

    protected function log(string $message): void
    {
        $this->logger?->info($this->messageForLog($message));
        $this->info($message);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->commandId = uniqid();

        $logManager = new LogManager($this->getLaravel());
        $this->logger = $logManager->channel('console');

        $this->beforeExecute();

        $result = parent::execute($input, $output);

        $this->afterExecute();

        return $result;
    }

    private function beforeExecute(): void
    {
        if ($this->logStartFinish) {
            $this->logger?->info($this->messageForLog('START'));
        }
    }

    private function afterExecute(): void
    {
        if ($this->logStartFinish) {
            $this->logger?->info($this->messageForLog('END'));
        }
    }

    private function messageForLog(string $text): string
    {
        $command = $this->getName();
        $options = $this->options();

        unset($options['help']);
        unset($options['quiet']);
        unset($options['verbose']);
        unset($options['version']);
        unset($options['ansi']);
        unset($options['no-interaction']);
        unset($options['env']);

        foreach ($options as $option => $value) {
            if (!is_bool($value) && !is_null($value)) {
                $command .= " --$option=$value";
            } elseif ($value) {
                $command .= " $option";
            }
        }

        return sprintf('[%s](%s) %s', $command, $this->commandId, $text);
    }
}
