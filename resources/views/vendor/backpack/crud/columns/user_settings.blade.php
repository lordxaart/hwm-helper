@php
    function showSetting($key, $value) {
        $type = gettype($value);
        switch ($type) {
            case 'boolean':
                return $value ? 'true' : 'false';
                break;
            case 'array':
                return implode(', ', $value);
                break;
            default:
                return $value;
        }
    }

    function printItem($key, $value, $strongTitle = true) {
        if (is_array($value)) {
            $res = '<ul class="list-group">';
            $res .= '<li class="list-group-item">';
            $res .= '<h6 class="mb-3"><strong>' . ucwords(str_replace('_', ' ', $key))  . ':</strong></h6>';

            foreach ($value as $key1 => $value1) {
                $res .= printItem($key1, $value1, false);
            }
            $res .= '</li></ul>';

            return $res;
        }

        $strongClass = $strongTitle ? 'font-weight-bold' : '';

        return '
            <li class="list-group-item d-flex justify-content-between">
                <span class="' . $strongClass . '">' . ucwords(str_replace('_', ' ', $key))  . ':</span>
                <span>' . showSetting($key, $value) . '</span>
            </li>
        ';
    }
@endphp

<ul class="list-group">
    @foreach($entry->settings->all() as $key => $value)
        {!! printItem($key, $value) !!}
    @endforeach
</ul>
