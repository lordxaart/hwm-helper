@if ($crud->hasAccess('update'))
	@if (!$entry instanceof \App\Models\User || !$entry->isDeleted())
		<a href="{{ url($crud->route.'/'.$entry->getKey().'/edit') }}" class="btn btn-sm btn-link" title="Edit"
			data-toggle="tooltip"><i class="fa fa-edit"></i></a>
	@endif
@endif