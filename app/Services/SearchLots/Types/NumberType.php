<?php

declare(strict_types=1);

namespace App\Services\SearchLots\Types;

use App\Services\SearchLots\Enums\CompareOperator;

class NumberType
{
    private CompareOperator $operator;
    private int $value;

    public function __construct(CompareOperator $operator, int $value)
    {
        $this->operator = $operator;
        $this->value = $value;
    }

    public function getOperator(): CompareOperator
    {
        return $this->operator;
    }

    public function getValue(): int
    {
        return $this->value;
    }
}
