<?php

namespace Database\Seeders;

class LotsSeeder extends FactorySeeder
{
    public int $count = 4000;

    public function getCountChunks(): int
    {
        return 1;
    }

    public function initStaticData()
    {
        //
    }

    public function runChunk(): int
    {
        //

        return $this->count;
    }
}
