<?php

namespace Deployer;

desc('Prepare deploy');
task('artisan:deploy:prepare', function () {
    $output = run('cd {{release_path}} && sudo -u www-data {{bin/php}} artisan app:deploy');

    writeln("<info>$output</info>");
});

desc('Run tests');
task('artisan:test', function () {
    if (get('stage') !== 'parser') {
        run('cd {{release_path}} && {{bin/php}} artisan test --without-tty');
    }
});

desc('Fix permissions');
task('permissions', function () {
    run('chown -R {{http_user}}:{{http_user}} {{deploy_path}}');
});
