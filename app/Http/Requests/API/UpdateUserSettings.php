<?php

declare(strict_types=1);

namespace App\Http\Requests\API;

use App\Services\Notifications\Enums\Channel;
use App\Services\Notifications\Enums\NotifyType;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserSettings extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $rules = [];

        foreach (NotifyType::cases() as $notify) {
            $rules[$notify->value] = 'nullable|array';
            foreach (Channel::cases() as $channel) {
                $rules[$notify->value . '.' . $channel->value] = 'nullable|bool';
            }
        }

        return $rules;
    }
}
