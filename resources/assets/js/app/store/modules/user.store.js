import request from 'JS/app/utils/request';
import config from 'JS/app/config';
import cache from 'JS/app/utils/cache';
import NotificationListener from 'JS/app/listeners/notifications';
import jsCookie from 'js-cookie';

const st = {
    user: null,
    loadingUser: false,
    loadingUpdateSettings: false,
    subscribed: false,
};

// Getter functions
const getters = {
    getUser: (state) => state.user,
    getLoadingUser: (state) => state.loadingUser,
    getLoadingUpdateSettings: (state) => state.loadingUpdateSettings,
};

// Actions
const actions = {
    async initUser({ state, dispatch }) {
        if (!state.user) {
            await dispatch('loadUser');
        }
    },
    async loadUser({ state, commit, dispatch }) {
        if (state.loadingUser) {
            return;
        }

        dispatch('loadUserFromCache');

        commit('START_LOADING', 'loadingUser');
        try {
            const response = await request.get('/api/v1/profile');
            dispatch('setUser', response.data.data);
            commit('END_LOADING', 'loadingUser');
        } catch (e) {
            commit('END_LOADING', 'loadingUser');
            throw e;
        }
    },
    loadUserFromCache({ commit }) {
        if (cache.has(config.cache.keys.store.user)) {
            commit('SET_USER', cache.get(config.cache.keys.store.user));
        }
    },
    async updateNotificationSettings({ dispatch, commit }, settings) {
        commit('START_LOADING', 'loadingUpdateSettings');
        try {
            const response = await request.post('/api/v1/profile/settings/notifications', settings);
            dispatch('setUser', response.data.data);
            commit('END_LOADING', 'loadingUpdateSettings');
        } catch (e) {
            commit('END_LOADING', 'loadingUpdateSettings');
            throw e;
        }
    },
    async updateSettings({ dispatch, commit }, settings) {
        commit('START_LOADING', 'loadingUpdateSettings');
        try {
            const response = await request.post('/api/v1/profile/settings', settings);
            dispatch('setUser', response.data.data);
            commit('END_LOADING', 'loadingUpdateSettings');
        } catch (e) {
            commit('END_LOADING', 'loadingUpdateSettings');
            throw e;
        }
    },
    async connectTelegram({ dispatch, commit }, user) {
        commit('START_LOADING', 'loadingUpdateSettings');
        try {
            const response = await request.post('/api/v1/profile/connect/telegram', { user });
            dispatch('setUser', response.data.data);
            commit('END_LOADING', 'loadingUpdateSettings');
        } catch (e) {
            commit('END_LOADING', 'loadingUpdateSettings');
            throw e;
        }
    },
    async disconnectTelegram({ dispatch, commit }) {
        commit('START_LOADING', 'loadingUpdateSettings');
        try {
            const response = await request.post('/api/v1/profile/disconnect/telegram');
            dispatch('setUser', response.data.data);
            commit('END_LOADING', 'loadingUpdateSettings');
        } catch (e) {
            commit('END_LOADING', 'loadingUpdateSettings');
            throw e;
        }
    },
    setUser({ commit, state }, data) {
        commit('SET_USER', data);
        commit('UPDATE_CACHE');

        // update subscribes
        if (!config.app.is_crawler && state.user && !state.subscribed) {
            commit('SUBSCRIBED_ON');
            NotificationListener(state.user.id);
        }
    },
    // eslint-disable-next-line no-empty-pattern
    async sendFeedback({}, params) {
        const formData = new FormData();

        Object.keys(params).forEach((prop) => {
            if (params[prop]) {
                if (prop === 'attachments') {
                    params[prop].forEach((file) => {
                        formData.append(`${prop}[]`, file, file.name);
                    });
                } else {
                    formData.append(prop, params[prop]);
                }
            }
        });

        const response = await request.post('/api/v1/sendFeedback', formData);

        return response;
    },
    async setLocale({ dispatch, state }, locale) {
        if (window.location.pathname.indexOf('/admin') === 0) {
            jsCookie.set('locale', locale, { path: '/admin' });
        } else {
            jsCookie.set('locale', locale);
        }

        if (state.user && state.user.id) {
            await dispatch('updateSettings', { lang: locale });
        }
    },
};

// Mutations
const mutations = {
    START_LOADING(state, key) {
        state[key] = true;
    },
    END_LOADING(state, key) {
        state[key] = false;
    },
    SET_USER(state, data) {
        if (!data.email) {
            throw new Error('User must have Email');
        }

        state.user = data;
    },
    UPDATE_CACHE(state) {
        cache.set(config.cache.keys.store.user, state.user);
    },
    SUBSCRIBED_ON(state) {
        state.subscribed = 1;
    },
};

export default {
    namespaced: true,
    state: st,
    getters,
    actions,
    mutations,
};
