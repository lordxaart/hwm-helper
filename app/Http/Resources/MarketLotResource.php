<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\HWM\Entities\MarketLot;
use Illuminate\Http\Request;

/**
 * @property MarketLot $resource
 */
class MarketLotResource extends LotBaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return parent::toArray($request);
    }
}
