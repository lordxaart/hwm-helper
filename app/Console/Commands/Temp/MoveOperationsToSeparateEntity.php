<?php

declare(strict_types=1);

namespace App\Console\Commands\Temp;

use App\Console\Commands\Command;
use App\Models\Lot;
use App\Models\LotOperation;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Throwable;

class MoveOperationsToSeparateEntity extends Command
{
    /**
     * @var string
     */
    protected $signature = 'hwm:lots:operations:move {--min=} {--max=}';

    /**
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @throws Throwable
     */
    public function handle(): void
    {
        $step = 100_000;
        $min = 1;
        $max = 120_000_000;

        for ($i = $min; $i <= $max; $i += $step) {
            $from = $i;
            $to = $i + $step - 1;

            $lots = Lot::select(['lot_id', 'operations'])
                ->where([
                    ['lot_id', '>=', $from],
                    ['lot_id', '<=', $to],
                ])
                ->get();

            $this->info("[$from - $to] lots {$lots->count()}");
            $this->handleLots($lots);
        }

        $this->info("End");
    }

    /**
     * @throws Throwable
     */
    private function handleLots(Collection $lots): void
    {
        $insertedBatchSize = 1000;
        $batch = ['operations' => [], 'lots' => []];
        $startTime = microtime(true);
        $lotWithOperations = 0;

        /** @var Lot $lot */
        foreach ($lots as $lot) {
            $lotWithOperations++;
            $operations = json_decode($lot->getOriginal('operations'), true);
            if (!$operations || !is_array($operations)) {
                $this->warn("Empty lot #{$lot->lot_id}");
                continue;
            }
            foreach ($operations as $operation) {
                $batch['operations'][] = LotOperation::prepareFromEntity(
                    $lot->lot_id,
                    new \App\HWM\Objects\LotOperation($operation),
                );
            }
            $batch['lots'][] = $lot->lot_id;

            if (count($batch['operations']) >= $insertedBatchSize || $lots->last()->lot_id === $lot->lot_id) {
                try {
                    DB::table('lot_operations')->insert($batch['operations']);
                    DB::table('lots')
                        ->whereIn('lot_id', $batch['lots'])
                        ->update([
                            'operations' => null,
                            'is_operations_handled' => 1,
                        ]);

                    $this->info(
                        sprintf(
                            'Inserted %s operations in %s lots, %s',
                            count($batch['operations']),
                            $lotWithOperations,
                            strval(microtime(true) - $startTime)
                        ),
                    );

                    $lotWithOperations = 0;
                    $batch = ['operations' => [], 'lots' => []];
                    $startTime = microtime(true);
                } catch (Throwable $e) {
                    $lotWithOperations = 0;
                    $batch = ['operations' => [], 'lots' => []];
                    $this->error('Error: ' . $e->getMessage());
                    continue;
                }
            }
        }

        if (count($batch['operations'])) {
            throw new Exception('Batch not empty');
        }

        if ($lotWithOperations) {
            $this->info('Updated ' . $lotWithOperations);
        }
    }
}
