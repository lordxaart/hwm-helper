@php
    /** @var \App\Models\User $user */
    $user = $entry;
@endphp
<!-- text input -->
<label>{!! $field['label'] !!}</label>

<div class="card">
    <div class="card-body row">
        @foreach($user->settings->getAdminEditableKeys() as $key)
            @if(is_bool($user->settings->get($key)))
                <div class="form-group col-sm-12">
                    <label for="settings[{{ $key }}]">{{ ucwords(str_replace('_', ' ', $key)) }}</label>
                    <input id="settings[{{ $key }}]" type="checkbox" name="settings[{{ $key }}]" {{ $user->settings->get($key) ? 'checked' : '' }} class="custom-checkbox d-block">
                </div>
            @else
                <div class="form-group col-sm-12">
                    <label>{{ ucwords(str_replace('_', ' ', $key)) }}</label>
                    <input type="text" name="settings[{{ $key }}]" value="{{ is_bool($user->settings->get($key)) ? intval($user->settings->get($key)) : $user->settings->get($key) }}" class="form-control">
                </div>
            @endif
        @endforeach
    </div>
</div>
