<?php

declare(strict_types=1);

namespace App\Providers;

use App\Services\MetaTag\Meta;
use Butschster\Head\Contracts\Packages\ManagerInterface;
use Butschster\Head\Providers\MetaTagsApplicationServiceProvider as ServiceProvider;

class MetaTagsServiceProvider extends ServiceProvider
{
    // if you don't want to change anything in this method just remove it
    protected function registerMeta(): void
    {
        $this->app->singleton(Meta::class, function () {
            $meta = new Meta(
                $this->app[ManagerInterface::class],
                $this->app['config']
            );

            // default values
            $meta->addMeta('theme-color', ['content' => '#00bc8c']);
            $meta->addMeta('author', ['content' => 'Lord Xaart']);
            $meta->addLink('manifest', ['href' => '/manifest.json']);
            //$meta->setCanonical(url()->current());
            $meta->addMeta('fb:app_id', ['content' => config('services.facebook.client_id')]);
            $meta->setImage(asset('images/logo/logo.png'));
            $meta->setFavicons();

            // This method gets default values from config and creates tags, includes default packages, e.t.c
            // If you don't want to use default values just remove it.
            $meta->initialize();

            return $meta;
        });
    }
}
