<?php

declare(strict_types=1);

namespace App\Services\Parser;

use App\HWM\Exceptions\ParserErrors\EmptyLotPage;
use App\Models\Lot;
use App\Services\Parser\Storages\StorageForNewLots;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @property ?LotRunner $currentRunner
 */
class NewLotsParser extends AbstractParser
{
    public const USLEEP_TO_WAIT_NEXT_LOT = 10 * 1000000; // 10sec

    protected StorageForNewLots $storage;
    protected bool $waitNextLot = false;
    protected int $maxLotId = 0;

    public function __construct(OutputInterface $output = null)
    {
        $this->storage = new StorageForNewLots();

        parent::__construct($output);

        $this->maxLotId = Lot::getMaxEndedEnd()?->lot_id ?: 1;
        $this->setRunner($this->getNextLotId());
    }

    public function getState(): string
    {
        if ($this->waitNextLot) {
            return 'wait';
        }

        return parent::getState();
    }

    public function __destruct()
    {
        parent::__destruct();

        $this->deleteCurrentRunner();
    }

    protected function setRunner(int $lotId): void
    {
        // set lot id to current parsers
        if ($this->currentRunner) {
            $this->storage->delete((string)$this->currentRunner->getLotId());
        }

        if (!$this->storage->add((string)$lotId)) {
            $this->setNextRunner();
            return;
        }

        $this->currentRunner = new LotRunner(LotRunner::TYPE_NEW, $lotId, true);
    }

    protected function getNextLotId(): int
    {
        $max = $this->storage->getMaxId();
        $lotId = $max && $max > $this->maxLotId ? $max : $this->maxLotId;

        if (!$lotId) {
            $lotId = intval(config('hwm.last_lot_id'));
        }

        $lotId = $lotId + 1;

        if ($this->currentRunner && $this->currentRunner->getLotId() === $lotId) {
            $lotId = $lotId + 1;
        }

        return $lotId;
    }

    protected function setNextRunner(): void
    {
        if ($this->waitNextLot) {
            $this->log('Wait next lot, sleep ' . self::USLEEP_TO_WAIT_NEXT_LOT . ' microseconds');
            usleep(self::USLEEP_TO_WAIT_NEXT_LOT);
            $this->waitNextLot = false;
            $lotId = $this->currentRunner?->getLotId();
        } else {
            // get next lot id
            $lotId = $this->getNextLotId();
        }

        $this->setRunner($lotId ?: 0);
    }

    protected function handleRunException(\Throwable $e): void
    {
        if ($e instanceof EmptyLotPage) {
            $this->waitNextLot = true;
            return;
        }

        parent::handleRunException($e);
    }

    protected function deleteCurrentRunner(): void
    {
        if (!$this->currentRunner) {
            return;
        }

        $this->storage->delete(strval($this->currentRunner->getLotId()));
    }
}
