<?php

declare(strict_types=1);

namespace App\Providers;

use App\Services\Settings\GeneralSettings;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \View::share('settings', $this->app->get(GeneralSettings::class));

        // Run after booted another providers - need RouterServiceProvider for this action
        $this->app->booted(function () {
            \View::share('navbarItems', $this->getNavbarItems());
        });
    }

    protected function getNavbarItems(): \Illuminate\Support\Collection
    {
        $items = collect([
            'calculator',
            'market',
            'monitoring',
        ]);

        return $items->map(function ($item) {
            return [
                'link' => route($item),
                'key' => $item,
                'active' => isRoute($item),
            ];
        });
    }
}
