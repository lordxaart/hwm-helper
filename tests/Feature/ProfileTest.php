<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\Traits\WithStudUser;

class ProfileTest extends TestCase
{
    use WithStudUser;

    /**
     * @test
     */
    public function profile_can_see_only_auth_user(): void
    {
        $this->assertOnlyForAuthUser(self::PROFILE_URL);
    }
}
