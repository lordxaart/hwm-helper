<?php

declare(strict_types=1);

namespace App\Console\Commands\HWM\Lot;

use App\Console\Commands\Command;
use App\Models\Lot;
use App\Services\SearchLots\Repositories\ElasticLotRepository;
use Elasticsearch\Common\Exceptions\BadRequest400Exception;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Process\Process;

class ElasticReindexLots extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'es:reindex:lots {--async} {--from=} {--to=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reindex Elasticsearch Lots Index';

    private ?ElasticLotRepository $lotsRepository = null;

    public function handle(): int
    {
        $this->lotsRepository = app(ElasticLotRepository::class);

        try {
            $this->lotsRepository->initIndex();
        } catch (BadRequest400Exception $e) {
            //
        }

        $step = 1000;
        $from = intval($this->option('from') ?: 1);
        $to = intval($this->option('to') ?: Lot::max('lot_id'));

        if ($this->option('async')) {
            $chunkSize = 10_000_000;
            $start = microtime(true);
            $pool = [];
            for ($i = $from; $i <= $to; $i += $chunkSize) {
                $max = $i + $chunkSize - 1;
                if ($max > $to) {
                    $max = $to;
                }

                $process = new Process(['php', 'artisan', 'es:reindex:lots', '--from', $i, '--to', $max]);
                $process->start();
                $pool[] = $process;
                $this->info("[" . (count($pool) - 1) . "]Start from $i to $max");
            }

            sleep(10);

            while (count($pool)) {
                system('clear');
                $this->info('Running ' . number_format(microtime(true) - $start, 1));

                /**
                 * @var int $i
                 * @var Process $process
                 */
                foreach ($pool as $i => $process) {
                    if (!$process->isRunning()) {
                        unset($pool[$i]);
                        $this->info("[$i] END");
                    } else {
                        $this->info("[$i] still running");
                    }
                }
                sleep(1);
            }
        } else {
            $this->reindexFromTo($from, $to, $step);
        }

        return 0;
    }

    private function reindexFromTo(int $from, int $to, int $step): void
    {
        $this->info(
            sprintf(
                'Start indexing [%s], from %s to %s per %s records',
                ElasticLotRepository::ES_INDEX,
                $from,
                $to,
                $step,
            )
        );
        $progress = $this->getOutput()->createProgressBar($to);
        $progress->advance($from);
        $progress->setFormat(ProgressBar::FORMAT_DEBUG);

        Lot::whereBetween('lot_id', [$from, $to])->chunkById($step, function ($lots) use ($progress) {
            $lots = $lots->map(function (Lot $lot) {
                return $lot->asLotEntity();
            });
            $this->lotsRepository?->addLotBulkToIndex($lots);

            $progress->advance($lots->count());
        }, 'lot_id');

        $progress->finish();
    }
}
