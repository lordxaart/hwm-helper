<?php

declare(strict_types=1);

namespace App\Actions\User;

use App\Exceptions\User\UserMonitoredArtLimitExceeded;
use App\Models\Art;
use App\Models\MonitoredArt;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Lorisleiva\Actions\Concerns\AsAction;

class AddArtToMonitoring
{
    use AsAction;

    /**
     * @throws UserMonitoredArtLimitExceeded
     */
    public function handle(User $user, string $artId): MonitoredArt
    {
        $query = $user->monitoredArts()->withoutGlobalScope('active');
        $featureList = $user->features();

        if ($art = $query->where('art_hwm_id', $artId)->first()) {
            /** @var MonitoredArt $art */
            $art->activate();
        } else {
            if ($query->count() >= $featureList->max_monitored_arts) {
                throw new UserMonitoredArtLimitExceeded($user, $featureList->max_monitored_arts);
            }

            if (!Art::findByHwmId($artId)) {
                throw (new ModelNotFoundException())->setModel(Art::class, [$artId]);
            }

            /** @var MonitoredArt $art */
            $art = $query->create(['art_hwm_id' => $artId]);
            $art->refresh();
        }

        return $art;
    }
}
