@if ($crud->hasAccess('show'))
	@if (!$entry instanceof \App\Models\User || !$entry->isDeleted())
	<a href="{{ url($crud->route.'/'.$entry->getKey().'/show') }}" class="btn btn-sm btn-link" title="Preview"
		data-toggle="tooltip"><i class="fa fa-eye"></i></a>
	@endif
@endif
