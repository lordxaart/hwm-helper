import amplitude from 'amplitude-js';

const isDebug = process.env.NODE_ENV !== 'production';

export function init(apiKey) {
    amplitude.getInstance().init(apiKey);
}

// eslint-disable-next-line import/prefer-default-export
export function logEvent(eventName, payload) {
    if (isDebug) {
        console.log('AMPLITUDE_EVENT', {
            isDebug,
            eventName,
            payload,
        });
    } else {
        amplitude.getInstance().logEvent(eventName, payload);
    }
}

export function setUserId(userId) {
    if (isDebug) {
        console.log('AMPLITUDE_SET_USER_ID', { userId });
    } else {
        amplitude.getInstance().setUserId(userId);
    }
}

export function setUserProperties(properties) {
    if (isDebug) {
        console.log('AMPLITUDE_SET_USER_PROPERTIES', { properties });
    } else {
        amplitude.getInstance().setUserProperties(properties);
    }
}
