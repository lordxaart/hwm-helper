<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Testing\TestResponse;
use Tests\Traits\AssertArray;
use Tests\Traits\CreatesApplication;
use Tests\Traits\MigrateFreshSeedOnce;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, MigrateFreshSeedOnce, AssertArray;

    const HOME_URL = '/';
    const LOGIN_URL = '/login';
    const REGISTER_URL = '/register';
    const RESET_PASSWORD_URL = '/password/reset';
    const MONITORING_URL = '/monitoring';
    const CALCULATOR_URL = '/calculator';
    const MARKET_URL = '/market';
    const PROFILE_URL = '/profile/settings';
    const CONTACTS_URL = '/contacts';
    const PRIVACY_URL = '/privacy';
    const TERMS_URL = '/terms';

    public function assertAuthenticationRequired($uri, $method = 'get', $redirect = self::LOGIN_URL): void
    {
        $method = $this->handleMethod($method);

        $response = $this->$method($uri);

        $this->assertResponseHasRedirect($response, $redirect);
    }

    public function assertGuestRequired($uri, $method = 'get', $redirect = self::HOME_URL): void
    {
        $method = $this->handleMethod($method);

        $user = User::factory()->create();
        $response = $this->actingAs($user)->$method($uri);

        $this->assertResponseHasRedirect($response, $redirect);
    }

    public function assertOnlyForAuthUser(string $url): TestResponse
    {
        $this->assertAuthenticationRequired($url);

        $response = $this->actingAs(User::factory()->create())->get($url);
        $response->assertStatus(200);

        return $response;
    }

    public function assertOnlyForGuest(string $url): TestResponse
    {
        $this->assertGuestRequired($url);
        \Auth::logout();

        $this->assertGuest();

        $response = $this->get($url);
        $response->assertStatus(200);

        return $response;
    }

    protected function assertResponseHasRedirect(
        TestResponse $response,
        string $redirect,
        int $expectStatusCode = 302,
        int $jsonExpectStatusCode = 401
    ): void {
        $expectedJson = $response->baseResponse->headers->get('content-type') === 'application/json';
        if (!$expectedJson) {
            $response->assertStatus($expectStatusCode);
            $response->assertRedirect($redirect);
        } else {
            $response->assertStatus($jsonExpectStatusCode);
        }
    }

    private function handleMethod(string $method): string
    {
        $method = strtolower($method);

        if (!in_array($method, ['get', 'getJson', 'postJson', 'post', 'put', 'update', 'delete'])) {
            throw new \InvalidArgumentException('Invalid method: '.$method);
        }

        return $method;
    }
}
