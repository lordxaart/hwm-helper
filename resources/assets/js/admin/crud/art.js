/* eslint-env jquery */

import notify from '../helpers/notify';
import handleError from '../helpers/handleError';

// Refresh art from original
// eslint-disable-next-line no-undef
$('body').on('click', '.refresh_from_original', function (e) {
    e.preventDefault();
    const artId = this.getAttribute('data-art-id');

    notify('Updating was start');
    global.axios.post(`/api/v1/arts/${artId}/refreshFromOriginal`).then(() => {
        notify(`Art <strong>${artId}</strong> was updated from original value`);

        if (global.crudTable()) {
            global.crudTable().fnDraw();
        } else {
            window.location.reload();
        }
    }).catch(handleError);
});

// Add Art Button
const initAddArtButton = () => {
    const button = $('button[name=addArt]');
    const loadingText = '<i class="fas fa-spin fa-spinner"></i>';
    const defaultButtonText = button.html();

    button.click(() => {
        // eslint-disable-next-line no-undef
        const value = $('input[name=art_id]').val();

        if (value) {
            button.html(loadingText);
            global.axios.post('/api/v1/arts', { art_id: value }).then(() => {
                button.html(defaultButtonText);
                $('input[name=art_id]').val('');
                notify(`Art <strong>${value}</strong> was added`);
            }).catch((e) => {
                button.html(defaultButtonText);
                handleError(e);
            });
        }
    });
};

initAddArtButton();
