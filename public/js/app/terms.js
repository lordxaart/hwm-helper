(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["terms"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/pages/Terms.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/pages/Terms.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var JS_app_config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! JS/app/config */ "./resources/assets/js/app/config/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  metaInfo() {
    return {
      title: this.$t('app.main.privacy')
    };
  },

  computed: {
    config() {
      return JS_app_config__WEBPACK_IMPORTED_MODULE_0__["default"];
    }

  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/pages/Terms.vue?vue&type=template&id=7484372c&scoped=true&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/pages/Terms.vue?vue&type=template&id=7484372c&scoped=true& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c("h1", [_vm._v("Правила и условия")]),
    _vm._v(" "),
    _c("p", [
      _vm._v("Добро пожаловать на " + _vm._s(_vm.config.app.name) + "!")
    ]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "В этих положениях и условиях изложены правила и положения использования веб-сайта HWH Helper, расположенного по адресу " +
          _vm._s(_vm.config.app.url) +
          "."
      )
    ]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Заходя на этот сайт, мы предполагаем, что вы принимаете эти условия. Не продолжайте использовать " +
          _vm._s(_vm.config.app.name) +
          ", если вы не согласны принять все условия, изложенные на этой странице."
      )
    ]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Следующая терминология применяется к настоящим Условиям, Заявлению о конфиденциальности и Уведомлению об отказе от ответственности, а также ко всем соглашениям: «Клиент», «Вы» и «Ваш» относится к вам, лицу, входящему на этот веб-сайт, и соответствует условиям Компании и условия. «Компания», «Мы», «Мы», «Наши» и «Мы» относятся к нашей Компании. «Сторона», «Стороны» или «Нас» относится как к Клиенту, так и к нам. Все условия относятся к предложению, принятию и рассмотрению платежа, необходимому для того, чтобы предпринять процесс нашей помощи Клиенту наиболее подходящим способом для явной цели удовлетворения потребностей Клиента в отношении предоставления заявленных услуг Компании в соответствии с и в соответствии с действующим законодательством Нидерландов. Любое использование вышеуказанной терминологии или других слов в единственном числе, множественном числе, с заглавными буквами и / или он / она или они, считаются взаимозаменяемыми и, следовательно, относятся к ним."
      )
    ]),
    _vm._v(" "),
    _c("h3", [_vm._v("Cookies")]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Мы используем файлы cookie. Получив доступ к " +
          _vm._s(_vm.config.app.domain) +
          ", вы согласились использовать файлы cookie в соответствии с политикой конфиденциальности HWH Helper."
      )
    ]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Большинство интерактивных веб-сайтов используют файлы cookie, чтобы мы могли получать информацию о пользователях для каждого посещения. Файлы cookie используются нашим веб-сайтом, чтобы обеспечить функциональность определенных областей, чтобы облегчить людям посещение нашего веб-сайта. Некоторые из наших аффилированных / рекламных партнеров также могут использовать файлы cookie."
      )
    ]),
    _vm._v(" "),
    _c("h3", [_vm._v("Лицензия")]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Если не указано иное, HWH Helper и / или его лицензиары владеют правами интеллектуальной собственности на все материалы в " +
          _vm._s(_vm.config.app.domain) +
          ". Все права на интеллектуальную собственность защищены. Вы можете получить к нему доступ из " +
          _vm._s(_vm.config.app.domain) +
          " для личного использования с учетом ограничений, установленных в этих условиях."
      )
    ]),
    _vm._v(" "),
    _c("p", [_vm._v("Вы не должны:")]),
    _vm._v(" "),
    _c("ul", [
      _c("li", [
        _vm._v(
          " Повторная публикация материала из " + _vm._s(_vm.config.app.name)
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          " Продажа, аренда или сублицензирование материалов из " +
            _vm._s(_vm.config.app.name)
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          " Воспроизведение, дублирование или копирование материала из " +
            _vm._s(_vm.config.app.name)
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(" Перераспределять контент из " + _vm._s(_vm.config.app.name))
      ])
    ]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Настоящее Соглашение вступает в силу с даты настоящего Соглашения."
      )
    ]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Отдельные части этого сайта предоставляют пользователям возможность публиковать и обмениваться мнениями и информацией в определенных областях сайта. HWH Helper не фильтрует, не редактирует, не публикует и не просматривает комментарии до их присутствия на сайте. Комментарии не отражают взгляды и мнения HWH Helper, его агентов и / или филиалов. Комментарии отражают взгляды и мнения человека, который публикует свои взгляды и мнения. В пределах, допустимых действующим законодательством, HWH Helper не несет ответственности за Комментарии или за любую ответственность, убытки или расходы, вызванные и / или понесенные в результате любого использования и / или публикации и / или появления Комментариев на этот сайт."
      )
    ]),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        _vm._s(_vm.config.app.name) +
          " оставляет за собой право отслеживать все комментарии и удалять любые комментарии, которые могут быть сочтены неуместными, оскорбительными или приводящими к нарушению настоящих Условий."
      )
    ]),
    _vm._v(" "),
    _c("p", [_vm._v("Вы гарантируете и подтверждаете, что:")]),
    _vm._v(" "),
    _vm._m(0),
    _vm._v(" "),
    _c("p", [
      _vm._v(
        "Настоящим вы предоставляете " +
          _vm._s(_vm.config.app.name) +
          " неисключительную лицензию на использование, воспроизведение, редактирование и разрешение другим пользователям использовать, воспроизводить и редактировать любые ваши Комментарии в любых формах, форматах или на любых носителях."
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", [
      _c("li", [
        _vm._v(
          " Вы имеете право размещать комментарии на нашем веб-сайте и иметь для этого все необходимые лицензии и согласия;"
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          " Комментарии не нарушают права интеллектуальной собственности, включая, помимо прочего, авторское право, патент или товарный знак какой-либо третьей стороны;"
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          " Комментарии не содержат клеветнических, клеветнических, оскорбительных, непристойных или иных незаконных материалов, которые являются вторжением в личную жизнь"
        )
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v(
          " Комментарии не будут использоваться для привлечения или продвижения бизнеса или обычаев, а также для представления коммерческой деятельности или незаконной деятельности."
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/assets/js/app/pages/Terms.vue":
/*!*************************************************!*\
  !*** ./resources/assets/js/app/pages/Terms.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Terms_vue_vue_type_template_id_7484372c_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Terms.vue?vue&type=template&id=7484372c&scoped=true& */ "./resources/assets/js/app/pages/Terms.vue?vue&type=template&id=7484372c&scoped=true&");
/* harmony import */ var _Terms_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Terms.vue?vue&type=script&lang=js& */ "./resources/assets/js/app/pages/Terms.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Terms_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Terms_vue_vue_type_template_id_7484372c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Terms_vue_vue_type_template_id_7484372c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "7484372c",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/app/pages/Terms.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/app/pages/Terms.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/assets/js/app/pages/Terms.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Terms.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/pages/Terms.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/app/pages/Terms.vue?vue&type=template&id=7484372c&scoped=true&":
/*!********************************************************************************************!*\
  !*** ./resources/assets/js/app/pages/Terms.vue?vue&type=template&id=7484372c&scoped=true& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_template_id_7484372c_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Terms.vue?vue&type=template&id=7484372c&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/pages/Terms.vue?vue&type=template&id=7484372c&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_template_id_7484372c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Terms_vue_vue_type_template_id_7484372c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);