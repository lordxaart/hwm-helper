<?php

declare(strict_types=1);

namespace App\Components;

use App\Models\Lot;
use Carbon\CarbonInterval;

class LotCache
{
    public const MAX_LOT_ID_CACHE_KEY = 'max_lot_id';
    public const MIN_LOT_ID_CACHE_KEY = 'min_lot_id';
    public const OLDEST_LOT_CACHE_KEY = 'lot_oldest';

    public static function getMaxLotId(): int
    {
        return intval(\Cache::rememberForever(self::MAX_LOT_ID_CACHE_KEY, function () {
            return Lot::max('lot_id');
        }));
    }

    public static function getMinLotId(): int
    {
        return intval(\Cache::rememberForever(self::MIN_LOT_ID_CACHE_KEY, function () {
            return Lot::min('lot_id');
        }));
    }

    public static function setMaxLotId(int $id = null): void
    {
        if (self::getMaxLotId() >= $id) {
            return;
        }

        \Cache::set(self::MAX_LOT_ID_CACHE_KEY, $id);
    }

    public static function getOldestLot(bool $refreshCache = false): ?Lot
    {
        $getOldestLot = function () {
            return Lot::oldest('lot_id')->first();
        };

        if ($refreshCache) {
            $lot = $getOldestLot();
            \Cache::set(self::OLDEST_LOT_CACHE_KEY, $lot);

            return $lot;
        }

        return \Cache::rememberForever(self::OLDEST_LOT_CACHE_KEY, $getOldestLot);
    }
}
