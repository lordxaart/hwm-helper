<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hwm_works', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('character_id')->nullable();
            $table->string('login', config('hwm.rules.max_hwm_id_length'))->index();
            $table->string('password');
            $table->string('status'); // status worked
            $table->text('info_work')->nullable(); // json object from bot
            $table->text('error')->nullable();
            $table->string('proxy')->nullable();
            $table->string('user_agent')->nullable();
            $table->timestamp('next_work_at')->nullable();
            $table->timestamps();

            $table->foreign('character_id')
                ->references('id')
                ->on('characters')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('works');
    }
}
