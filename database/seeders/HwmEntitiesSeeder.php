<?php

namespace Database\Seeders;

use App\Models\Art;
use Illuminate\Database\Seeder;
use App\HWM\Helpers\HwmHelper;
use App\Models\ArtCategory;
use App\Models\MarketCategory;
use App\Models\ArtSet;

class HwmEntitiesSeeder extends Seeder
{
    public function run(): void
    {
        $this->seedEntity(ArtCategory::class, 'categories');
        $this->seedEntity(MarketCategory::class, 'market_categories');
        $this->seedEntity(ArtSet::class, 'sets');
        $this->seedEntity(Art::class, 'arts');
}

    public function seedEntity($model, $staticData)
    {
        $entities = HwmHelper::getJson($staticData, false);

        if (!count($entities)) {
            $this->command->warn('Not found any ' . $staticData);
            return;
        }

        $existsCategories = $model::pluck('hwm_id')->toArray();
        $catsIds = array_column($entities, 'hwm_id');
        $diffIds = array_diff($catsIds, $existsCategories);
        $insertData = [];

        $entities = array_filter($entities, function ($category) use ($diffIds) {
            return in_array($category['hwm_id'], $diffIds);
        });

        $modelObj = $model::getModel();
        $modelObj->updateTimestamps();

        foreach ($entities as $entity) {
            // casts attributes without transable
            foreach ($modelObj->getCasts() as $field => $cast) {
                if (isset($entity[$field]) && !in_array($field, $modelObj->translatable)) {
                    $modelObj->setAttribute($field, $entity[$field]);
                }
            }
            // transable attributes
            foreach ($modelObj->translatable as $field) {
                if (isset($entity[$field])) {
                    $modelObj->setAttribute($field, $entity[$field]);
                }
            }

            $insertData[] = array_merge($entity, $modelObj->getAttributes());
        }

        if (count($insertData)) {
            \DB::table($model::getModel()->getTable())->insert($insertData);
        }

        $count = count($insertData);

        $this->command->info("Successfully added $count $staticData");
    }
}
