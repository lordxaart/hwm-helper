<?php

declare(strict_types=1);

namespace App\HWM\Helpers;

use App\HWM\Objects\Master;

trait Calculator
{
    // Main calculator
    public static function pricePerFight(
        int $price,
        int $repair,
        int $currentStrength,
        int $baseStrength,
        Master $master = null
    ): float {
        if (!$master) {
            $master = Master::getDefaultMaster();
        }

        $repairCost = $repair * $master->getRate() / 100;
        $strength = $baseStrength;
        $steps = [];
        $step = 1;

        $steps[] = [
            'strength' => $strength,
            'fights' => $currentStrength,
            'price' => $price,
            'pricePerFight' => $currentStrength ? $price / $currentStrength : $price,
            'step' => $step,
        ];

        $optimalStep = $steps[0];

        do {
            $step++;

            $lastStep = $steps[count($steps) - 1];

            $fights = floor($strength * $master->getRepair() / 100);
            $strength--;

            $currentStep = [
                'strength' => $strength,
                'fights' => $lastStep['fights'] + $fights,
                'price' => $lastStep['price'] + $repairCost,
                'pricePerFight' => ($lastStep['price'] + $repairCost) / ($lastStep['fights'] + $fights),
                'step' => $step,
            ];

            $steps[] = $currentStep;

            if ($currentStep['pricePerFight'] < $lastStep['pricePerFight']) {
                $optimalStep = $currentStep;
            }
        } while ($strength > 0);

        return $optimalStep['pricePerFight'];
    }
}
