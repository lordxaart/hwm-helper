<?php

declare(strict_types=1);

namespace App\HWM\Entities\Interfaces;

interface HasTranslations
{
    public function translateAttributes(): void;
}
