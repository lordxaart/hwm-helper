@php
    /** @var \App\Models\User $user */
    $user = $entry;
@endphp
<!-- text input -->
<label>{!! $field['label'] !!}</label>

<div class="card">
    <div class="card-body row">
        @foreach($user->features()->features as $feature)
            @php $key = $feature->key; @endphp
            <div class="form-group col-sm-12">
                <label for="features[{{ $key }}]">{{ ucwords(str_replace('_', ' ', $key)) }} {{ $feature->isDefault() ? '(default)' : '' }}</label>
                @if($feature->type === 'bool')
                    <input id="features[{{ $key }}]" type="checkbox" name="features[{{ $key }}]" {{ $feature->getValue() ? 'checked' : '' }} class="custom-checkbox d-block">
                    <p class="help-block text-secondary mb-0">Default: {{ $feature->default ? 'true' : 'false' }}</p>
                @else
                    <input type="{{ $feature->type === 'number' ? 'number' : 'text' }}" name="features[{{ $key }}]" value="{{ $feature->getValue() }}" class="form-control">
                    <p class="help-block text-secondary mb-0">Default: {{ $feature->default }}</p>
                @endif
                <p class="help-block text-secondary">Entity: {{ $feature->entity }}</p>
            </div>
        @endforeach
    </div>
</div>
