<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeleteFieldColumnFromArtMonitoredFilters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('art_monitored_filters', function (Blueprint $table) {
            $table->dropColumn('field');
            $table->string('value', 500)->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('art_monitored_filters', function (Blueprint $table) {
            $table->string('field');
            $table->string('value', 500)->nullable()->change();
        });
    }
}
