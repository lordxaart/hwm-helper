<?php

declare(strict_types=1);

namespace App\HWM\Repositories;

use App\HWM\Repositories\Base\EloquentRepository;
use App\Models\ArtCategory;

class ArtCategoryRepository extends EloquentRepository
{
    public function __construct()
    {
        parent::__construct(new ArtCategory());
    }
}
