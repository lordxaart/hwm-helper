<?php

declare(strict_types=1);

namespace App\Events\User;

use App\Models\User;
use App\Services\Telegram\Entities\User as TelegramUser;
use Illuminate\Foundation\Bus\Dispatchable;

class ConnectedTelegramEvent
{
    use Dispatchable;

    public User $user;
    public TelegramUser $telegramUser;

    public function __construct(User $user, TelegramUser $telegramUser)
    {
        $this->user = $user;
        $this->telegramUser = $telegramUser;
    }
}
