<?php

declare(strict_types=1);

namespace App\HWM\BotApi;

use Illuminate\Support\Arr;

class BotApi extends BaseBotApi
{
    public const ALLOWED_PATH = [
        'GET' => '/',
        'POST' => '/work',
    ];

    public static function api(): self
    {
        return new self();
    }

    public function index(): array
    {
        return $this->get('/');
    }

    public function crawler(string $url): string
    {
        $this->setTimeout(15);
        $this->setRetry(2, 100);
        $res = $this->get('crawler', ['url' => $url]);
        if (Arr::has($res, 'html')) {
            return $res['html'];
        }

        return '';
    }

    public function work(string $login, string $password, string $proxy = null, string $userAgent = null): string
    {
        $res = $this->post('work', [
            'login' => $login,
            'password' => $password,
            'proxy' => $proxy,
            'userAgent' => $userAgent,
        ]);

        return $res['infoWork'];
    }
}
