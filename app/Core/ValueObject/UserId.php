<?php

declare(strict_types=1);

namespace App\Core\ValueObject;

class UserId
{
    public readonly int $value;

    public function __construct(int $id)
    {
        // validate value
        if ($id < 1) {
            throw new \InvalidArgumentException(sprintf('Value [%s] less 1. It is no valid value ', $id));
        }

        $this->value = $id;
    }

    public function toString(): string
    {
        return (string) $this->value;
    }
}
