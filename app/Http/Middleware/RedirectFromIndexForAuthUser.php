<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Crawler;
use Illuminate\Http\Request;

class RedirectFromIndexForAuthUser
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($this->isNeedRedirectFromIndexPage($request)) {
            return redirect()->to($this->getToRoute());
        }

        return $next($request);
    }

    private function isNeedRedirectFromIndexPage(Request $request): bool
    {
        return false;
        if (Crawler::isCrawler()) {
            return false;
        }

        $referer = $request->headers->get('referer');
        $route = $request->route()?->getName();

        if ($route === 'index' && (is_null($referer) || str_contains($referer, $request->getHost()))) {
            return true;
        }

        return false;
    }

    private function getToRoute(): string
    {
        return route('calculator');
    }
}
