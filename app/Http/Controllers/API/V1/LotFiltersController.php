<?php

declare(strict_types=1);

namespace App\Http\Controllers\API\V1;

use App\Exceptions\User\UserFilterLimitExceeded;
use App\Http\Requests\API\FilterStore;
use App\Http\Requests\API\FilterUpdate;
use App\Http\Resources\LotFilterResource;
use App\HWM\Enums\EntityType;
use App\HWM\Enums\LotType;
use App\HWM\Repositories\ArtRepository;
use App\Models\LotFilter;
use App\Models\LotFilterNotifyChannel;
use App\Models\User;
use App\Repositories\NotificationRepository;
use App\Services\LotFilter\LotFiltersService;
use App\Services\LotFilter\Repositories\LotFiltersRepository;
use App\Services\LotFilter\ValueObjects\FilterTerm;
use App\Services\LotFilter\ValueObjects\LotFilterRequest;
use App\Services\SearchLots\ValueObjects\LimitOffset;
use Illuminate\Http\Request;

class LotFiltersController extends ApiController
{
    public function index(
        Request $request,
        LotFiltersRepository $lotFiltersRepository,
        LotFiltersService $lotFiltersProvider
    ) {
        $limitOffset = new LimitOffset(
            (int)$request->get('limit', 200),
            (int)$request->get('offset', 0),
        );

        /** @var User $user */
        $user = $request->user();
        $filters = $lotFiltersRepository->findAllByUserId($user->getId(), $limitOffset);
        $notifications = $lotFiltersProvider->getNotificationSettingsByFiltersIds($filters->pluck('id')->toArray());
        $resources = $filters->map(function (LotFilter $filter) use ($notifications) {
            $resource = new LotFilterResource($filter);
            $resource->setFilterNotifications($notifications[$filter->id ?? []]);

            return $resource;
        });

        return $this->response([
            'data' => $resources,
        ]);
    }

    /**
     * @throws UserFilterLimitExceeded
     */
    public function store(
        FilterStore $request,
        LotFiltersService $lotFiltersProvider,
        ArtRepository $artRepository,
    ) {
        $entityId = $request->get('entity_id');

        if ($request->get('entity_type') === EntityType::ART->value && $entityId && !$artRepository->has($entityId)) {
            throw new \InvalidArgumentException('Entity Id is not valid');
        }
        /** @var User $user */
        $user = user();
        $lotFilterRequest = new LotFilterRequest(
            $user->getId(),
            LotType::from($request->get('lot_type')),
            EntityType::from($request->get('entity_type')),
            $entityId,
            FilterTerm::parseCollectionFromArray($request->get('terms')),
        );
        $filter = $lotFiltersProvider->createFilter($lotFilterRequest);

        return $this->response(new LotFilterResource($filter))
            ->setStatusCode($filter->wasRecentlyCreated ? 201 : 200);
    }

    public function update(
        FilterUpdate $request,
        LotFiltersService $lotFiltersProvider,
        string $filterId
    ) {
        $lotFilter = LotFilter::findOrFail($filterId);
        /** @var User $user */
        $user = user();
        $lotFilterRequest = new LotFilterRequest(
            $user->getId(),
            $lotFilter->lot_type,
            $lotFilter->entity_type,
            $lotFilter->entity_id,
            $request->has('terms') ? FilterTerm::parseCollectionFromArray($request->get('terms')) : $lotFilter->terms,
            $request->get('is_active', $lotFilter->is_active),
        );

        $filter = $lotFiltersProvider->updateFilter($lotFilter, $lotFilterRequest);

        return $this->response(new LotFilterResource($filter));
    }

    public function delete(LotFiltersService $lotFiltersProvider, string $filterId)
    {
        $lotFilter = LotFilter::findOrFail($filterId);

        $lotFiltersProvider->deleteFilter($lotFilter);

        return $this->responseNoContent();
    }

    // todo  Перенести в окремий контроллер. Можливо покращити неймінг для сущностей
    public function updateNotificationChannel(
        Request $request,
        NotificationRepository $notificationRepository,
        string $filterId
    ) {
        $channels = $notificationRepository->getChannelAliases();
        $request->validate([
            'channel' => 'required|in:' . implode(',', $channels),
            'is_available' => 'required|boolean',
        ]);

        if (!LotFilter::find($filterId)) {
            abort(404);
        }

        $query = LotFilterNotifyChannel::query()
            ->where('lot_filter_id', $filterId)
            ->where('notify_channel', $request->get('channel'));

        $channelSettings = null;
        if ($query->exists()) {
            $query->update([
                'is_available' => $request->get('is_available'),
            ]);
        } else {
            $channelSettings = new LotFilterNotifyChannel();
            $channelSettings->lot_filter_id = $filterId;
            $channelSettings->notify_channel = $request->get('channel');
            $channelSettings->is_available = $request->get('is_available');
            $channelSettings->save();
        }

        return $this->response([])->setStatusCode($channelSettings ? 201 : 200);
    }
}
