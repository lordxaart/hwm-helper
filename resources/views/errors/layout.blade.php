@extends('layout.empty', ['extra_wrapper_class' => 'error-page'])

@section('content')
    <div class="container error-container text-center">
        <div>
            <h1 class="logo-text-bold text-danger">@yield('code')</h1>
            <h2 class="">@yield('title')</h2>
            <p class="mt-4 text-muted text-break">@yield('message')</p>
            <p class="mt-4 text-muted text-break">@yield('extra_message')</p>
        </div>
    </div>
@endsection
