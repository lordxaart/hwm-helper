<?php

declare(strict_types=1);

namespace App\Notifications\Backup;

use App\Services\Notifications\Channels\Telegram\Entities\TelegramMessage;
use Spatie\Backup\Notifications\Notifications\BackupHasFailedNotification as BaseNotification;

class BackupHasFailedNotification extends BaseNotification
{
    public function toTelegram(): TelegramMessage
    {
        $message = TelegramMessage::create()->error();
        $message->escapedLine("The backup of {$this->applicationName()} has failed");
        $this->backupDestinationProperties()->each(fn ($value, $name) => $message->escapedLine("{$name}: $value"));

        return $message;
    }

    public function toArray(): array
    {
        return [
            'desc' => $this->backupDestinationProperties()->toArray(),
            'message' => $this->event->exception->getMessage(),
        ];
    }
}
