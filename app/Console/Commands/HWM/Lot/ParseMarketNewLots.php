<?php

declare(strict_types=1);

namespace App\Console\Commands\HWM\Lot;

use App\Console\Commands\Command;
use App\Jobs\Lot\HandleMarketLots;
use App\Models\MarketCategory;

class ParseMarketNewLots extends Command
{
    /** @var string */
    protected $signature = 'hwm:parser:market_lots';

    /** @var string  */
    protected $description = 'Parse lots by monitored arts from market list';

    protected bool $logStartFinish = false;

    protected int $currentRuns = 0;

    protected int $maxRuns = 100;

    public function handle(): void
    {
        $disallowCats = ['res', 'elements', 'obj_share'];
        $cats = MarketCategory::all()->pluck('hwm_id')->all();

        do {
            foreach ($cats as $cat) {
                if (in_array($cat, $disallowCats)) {
                    continue;
                }

                dispatch(new HandleMarketLots($cat));
            }
            $this->currentRuns ++;
        } while ($this->currentRuns <= $this->maxRuns);

        sleep(2);
    }
}
