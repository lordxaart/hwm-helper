<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Models\Art;
use App\Services\MetaTag\Meta;
use Closure;
use Illuminate\Http\Request;

class HandleMetaTags
{
    private Meta $metaTags;

    public function __construct()
    {
        $this->metaTags = app(Meta::class);
    }

    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        $this->metaTags->setTitle(config('app.name'));
        $this->metaTags->setKeywords(trans('seo.default.keys'));
        $this->metaTags->setDescription(trans('seo.default.desc'));

        $art = Art::findByHwmId(strval($request->query('art')));

        $artTitle = $art ? $art->getTitle() : '';

        switch (true) {
            case isRoute('index'):
                $this->metaTags->prependTitle(trans('seo.index.title'));
                break;
            case isRoute('calculator'):
                $this->metaTags->prependTitle(trans('seo.calculator.title'));
                $this->metaTags->setDescription(trans('seo.calculator.desc', ['art' => $artTitle]));
                $this->setCalculatorParams($request);
                if ($art) {
                    $this->setArtToMetaTags($art);
                }
                break;
            case isRoute('market'):
                $this->metaTags->prependTitle(trans('seo.market.title'));
                $this->metaTags->setDescription(trans('seo.market.desc', ['art' => $artTitle]));
                if ($art) {
                    $this->setArtToMetaTags($art);
                }
                break;
            case isRoute('monitoring'):
                $this->metaTags->prependTitle(trans('seo.monitoring.title'));
                $this->metaTags->setDescription(trans('seo.monitoring.desc'));
                break;
            case isRoute('stat'):
                $this->metaTags->prependTitle(trans('seo.stat.title'));
                $this->metaTags->setDescription(trans('seo.stat.desc'));
                break;
            case isRoute('contacts'):
                $this->metaTags->prependTitle(trans('app.main.contacts'));
                $this->metaTags->setDescription(trans('seo.contacts.desc'));
                break;
            case isRoute('privacy'):
                $this->metaTags->prependTitle(trans('app.main.privacy'));
                $this->metaTags->setDescription(trans('seo.privacy.desc'));
                break;
            case isRoute('terms'):
                $this->metaTags->prependTitle(trans('app.main.terms'));
                $this->metaTags->setDescription(trans('seo.terms.desc'));
                break;
            case isRoute('login'):
                $this->metaTags->prependTitle(trans('seo.login.title'));
                $this->metaTags->setDescription(trans('seo.login.desc'));
                break;
            case isRoute('register'):
                $this->metaTags->prependTitle(trans('seo.register.title'));
                $this->metaTags->setDescription(trans('seo.register.desc'));
                break;
        }

        \View::share('metaTags', $this->metaTags);

        return $next($request);
    }

    private function setArtToMetaTags(Art $art = null): void
    {
        if ($art) {
            $this->metaTags->setImage($art->getDefaultImage()->getUrl());
            $this->metaTags->prependTitle($art->getTitle());
        }
    }

    private function setCalculatorParams(Request $request): void
    {
        $text = '';
        $price = $request->query('price');
        $current_strength = $request->query('current_strength');
        $base_strength = $request->query('base_strength');
        $repair = $request->query('repair');

        if ($price) {
            $text .= sprintf('%s %s ', trans('app.art.price'), $price);
        }

        if (!is_null($current_strength) && !is_null($base_strength)) {
            $text .= sprintf('%s %s/%s ', trans('app.art.strength'), $current_strength, $base_strength);
        }

        if ($repair) {
            $text .= sprintf('%s %s ', trans('app.art.repair'), $repair);
        }

        $text = trim($text);

        if ($text) {
            $this->metaTags->prependTitle($text);
        }
    }
}
