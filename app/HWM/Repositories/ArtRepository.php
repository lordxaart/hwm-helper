<?php

declare(strict_types=1);

namespace App\HWM\Repositories;

use App\HWM\Repositories\Base\EloquentRepository;
use App\Models\Art;

/**
 * Class EloquentRepository
 * @method Art|null findByHwmId(string $hwmId)
 * @package App\HWM\Repositories\Base
 */
class ArtRepository extends EloquentRepository
{
    public function __construct()
    {
        parent::__construct(new Art());
    }
}
