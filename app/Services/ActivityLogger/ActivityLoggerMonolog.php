<?php

declare(strict_types=1);

namespace App\Services\ActivityLogger;

use App\Services\ActivityLogger\ValueObjects\Activity;
use Psr\Log\LoggerInterface;

class ActivityLoggerMonolog implements ActivityLogger
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function log(Activity $activity): void
    {
        $context = $activity->getContext();

        if (($user = $activity->getCausedBy())) {
            $context['user']  = [
                'id' => $user->getId()->toString(),
                'email' => $user->email,
            ];
        }

        $this->logger->info($activity->getAction(), $context);
    }
}
