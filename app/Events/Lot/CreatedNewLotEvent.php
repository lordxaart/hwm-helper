<?php

declare(strict_types=1);

namespace App\Events\Lot;

use App\Models\Lot;
use Illuminate\Foundation\Events\Dispatchable;

class CreatedNewLotEvent
{
    use Dispatchable;

    public Lot $lot;

    public function __construct(Lot $lot)
    {
        $this->lot = $lot;
    }
}
