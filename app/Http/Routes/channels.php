<?php

Broadcast::channel('lots', function ($user) {
    return $user;
});

Broadcast::channel('App.Models.User.{userId}', function ($user, $userId) {
    return $user->id === \App\Models\User::find($userId)?->id;
});
