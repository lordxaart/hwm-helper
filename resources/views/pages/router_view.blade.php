@extends('layout.app')

@section('content')
    <div class="container">
        @include('elements.notification')
    </div>

    <loading :show="loadingPage" type="block"></loading>

    <transition name="el-fade-in" mode="out-in" appear>
        <keep-alive>
            <router-view></router-view>
        </keep-alive>
    </transition>
@endsection
