<?php

declare(strict_types=1);

namespace App\Models;

use App\HWM\Entities\Fraction;
use App\HWM\Entities\Guild;
use App\HWM\Helpers\HwmLink;
use App\HWM\Entities\Interfaces\HasLinkOnPage;
use App\HWM\Entities\Interfaces\HwmEntityInterface;
use App\HWM\Repositories\FractionRepository;
use App\HWM\Repositories\GuildRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Character extends Model implements HwmEntityInterface, HasLinkOnPage
{
    /** @var array  */
    public $fillable = ['hwm_id', 'login', 'password', 'level', 'experience', 'status', 'resources', 'guilds', 'fractions'];

    /** @var array  */
    protected $hidden = ['password'];

    /** @var array  */
    protected $casts = [
        'resources' => 'array',
        'guilds' => 'array',
        'fractions' => 'array',
    ];

    public function getTitle(): string
    {
        return $this->login;
    }

    public function getHwmId(): string
    {
        return strval($this->hwm_id);
    }

    public function getHwmLink(): string
    {
        return HwmLink::get(HwmLink::CHARACTER_INFO, ['id' => $this->hwm_id]);
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public static function findByKey(string $value): ?self
    {
        $key = is_numeric($value) ? 'hwm_id' : 'login';

        return self::where($key, $value)->first();
    }

    public function setPasswordAttribute(string $value): void
    {
        $this->attributes['password'] = Crypt::encryptString($value);
    }

    public function getPasswordAttribute(string $value): string
    {
        return Crypt::decryptString($value);
    }

    public function setStatusAttribute(string $status): void
    {
        $this->attributes['status'] = in_array(
            $status,
            \App\HWM\Entities\Character::getStatuses()
        ) ? $status : UNDEFINED;
    }

    public function getResourcesFormattedAttribute(): array
    {
        $formatted = [];
        if ($this->resources) {
            foreach ($this->resources as $key => $value) {
                $formatted[$key] = $value;
            }
        }
        return $formatted;
    }

    public function getGuildsFormattedAttribute(GuildRepository $guildRepository): array
    {
        $formatted = [];
        if ($this->guilds) {
            foreach ($this->guilds as $key => $value) {
                [$level, $experience] = explode(': ', $value);
                $formatted[$key] = [
                    'title' => $guildRepository->getTitleById((string)$key) ?: '',
                    'level' => intval($level),
                    'experience' => floatval($experience),
                    'exp_to_next_level' => Guild::expToNextLevel($key, $experience)
                ];
            }
        }

        return $formatted;
    }

    public function getFractionsFormattedAttribute(FractionRepository $fractionRepository): array
    {
        $formatted = [];
        if ($this->fractions) {
            foreach ($this->fractions as $key => $value) {
                [$level, $experience] = explode(': ', $value);
                $formatted[$key] = [
                    'title' => $fractionRepository->getTitleById((string)$key) ?: '',
                    'level' => intval($level),
                    'experience' => floatval($experience),
                    'exp_to_next_level' => Fraction::expToNextLevel((int)$experience)
                ];
            }
        }

        return $formatted;
    }
}
