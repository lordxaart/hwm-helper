<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\HwmBotAccountCreateUpdateRequest;
use App\Models\Art;
use App\Models\HwmBotAccount;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ArtCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class HwmBotCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;

    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel('App\Models\HwmBotAccount');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/bots');
        $this->crud->setEntityNameStrings('bot', 'bots');

//        $this->crud->set('show.setFromDb', true);
    }

    protected function setupListOperation()
    {
        $this->crud->setColumns([
            ['name' => 'login', 'label' => 'Login', 'orderable' => true,],
            [
                'name' => 'target',
                'label' => 'Target',
                'orderable' => true,
                'type' => 'closure',
                'function' => function ($entity) {
                    return match ($entity->target) {
                        HwmBotAccount::TARGET_ALL => 'ALL',
                        HwmBotAccount::TARGET_WEB => 'BROWSER',
                        HwmBotAccount::TARGET_CLI => 'CLI',
                        default => 'undefined (' . $entity->target . ')',
                    };
                }
            ],
            ['name' => 'is_blocked', 'label' => 'Blocked', 'type' => 'boolean'],
        ]);
    }

    protected function setupShowOperation()
    {
        $this->crud->addColumn([
            'name' => 'target',
            'label' => 'Target',
            'orderable' => true,
            'type' => 'closure',
            'function' => function ($entity) {
                return match ($entity->target) {
                    HwmBotAccount::TARGET_ALL => 'ALL',
                    HwmBotAccount::TARGET_WEB => 'BROWSER',
                    HwmBotAccount::TARGET_CLI => 'CLI',
                    default => 'undefined (' . $entity->target . ')',
                };
            }
        ]);
        $this->crud->setFromDb();
        $this->crud->addColumn([
            'name' => 'is_blocked',
            'label' => 'Blocked',
            'type' => 'boolean'
        ]);
        $this->crud->addColumn([
            'name' => 'success_requests',
            'label' => 'Success Requests',
            'type' => 'number'
        ]);
        $this->crud->addColumn([
            'name' => 'error_requests',
            'label' => 'Error Requests',
            'type' => 'number'
        ]);
        $this->crud->addColumn([
            'name' => 'last_success_request',
            'label' => 'Last Success',
            'type' => 'datetime'
        ]);
        $this->crud->addColumn([
            'name' => 'last_error_request',
            'label' => 'Last Error',
            'type' => 'datetime'
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateFields();
    }

    protected function setupCreateOperation()
    {
        $this->setupCreateFields();
    }

    public function setupCreateFields(): void
    {
        $this->crud->setFromDb();
        $this->crud->addField(
            [
                'name' => 'password',
                'label' => 'Password',
                'type' => 'text',
            ]
        );
        $this->crud->addField(
            [
                'name' => 'target',
                'label' => 'Target',
                'type' => 'select_from_array',
                'options' => [HwmBotAccount::TARGET_ALL => 'ALL', HwmBotAccount::TARGET_WEB => 'BROWSER', HwmBotAccount::TARGET_CLI => 'CLI', -1 => 'undefined'],
            ]
        );

        $this->crud->setValidation(HwmBotAccountCreateUpdateRequest::class);
    }
}
