<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;
use Throwable;

class LimitExceededError extends Exception
{
    public function __construct(Throwable $previous, int $tries)
    {
        $message = sprintf('%s [%d]. (%s: %s])', trans('errors.hwm.limit_requests_is_exceeded'), $tries, $previous::class, $previous->getMessage());

        parent::__construct($message, $previous->getCode(), $previous);
    }
}
