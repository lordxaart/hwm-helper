<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_features', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('name')->nullable();
            $table->enum('type', ['bool', 'number']);
            $table->text('default_value');
        });

        Schema::create('user_features_values', function (Blueprint $table) {
            $table->string('entity');
            $table->string('entity_id');
            $table->string('feature_id');
            $table->text('value');
            $table->timestamps();

            $table->primary(['entity', 'entity_id', 'feature_id']);
            $table->foreign('feature_id')
                ->on('user_features')
                ->references('id')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_features');
        Schema::dropIfExists('user_features_values');
    }
};
