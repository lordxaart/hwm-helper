<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

use App\Http\Controllers\Admin\AppSettingController;
use App\Http\Controllers\Admin\StatusPageAction;
use App\Http\Controllers\Admin\UserCrudController;

Route::group([
    'prefix'     => config('backpack.base.route_prefix', ADMIN_PREFIX),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
    'as' => ADMIN_PREFIX . '.'
], function () {
    // custom admin routes
    Route::get('/', function () {
        return redirect(backpack_url('statuspage'));
    })->name('backpack');

    Route::get('statuspage', [StatusPageAction::class, 'index'])->name('statuspage');

    // CRUD
    Route::crud('arts', 'ArtCrudController');

    Route::crud('art-categories', 'ArtCategoryCrudController');
    Route::crud('market-categories', 'MarketCategoryCrudController');
    Route::crud('art-sets', 'ArtSetCrudController');

    Route::crud('users', 'UserCrudController');
    Route::post('users/{id}/restore', [UserCrudController::class, 'restore']);
    Route::post('users/{id}/block', [UserCrudController::class, 'block']);
    Route::post('users/{id}/unblock', [UserCrudController::class, 'unblock']);

    Route::crud('monitored-arts', 'ArtMonitoredCrudController');

    Route::crud('email-log', 'EmailLogCrudController');

    Route::get('app-settings', [AppSettingController::class, 'index']);
    Route::put('app-settings', [AppSettingController::class, 'update']);
});

// logout
//Route::get(ADMIN_PREFIX . '/logout', 'Auth\LoginController@logout');
