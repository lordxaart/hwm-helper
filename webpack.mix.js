require('laravel-mix-purgecss');
require('laravel-mix-criticalcss');

const mix = require('laravel-mix');
const baseWebpackConfig = require('./resources/assets/js/other/webpack.config.js');
const purgeCssConfig = require('./resources/assets/js/other/purgecss.js');

const type = process.argv.indexOf('-t') !== -1 ? process.argv[process.argv.indexOf('-t') + 1] : 'app';

mix.copyDirectory('resources/assets/images', 'public/images');
mix.copyDirectory('resources/assets/fonts', 'public/fonts');
mix.copyDirectory('resources/assets/packages', 'public/packages');

switch (type) {
    case 'admin':
        mix.webpackConfig({
            resolve: baseWebpackConfig.resolve,
        })
            .js('resources/assets/js/admin.js', 'public/js')
            .sass('resources/assets/sass/admin.scss', 'public/css');
        break;
    case 'mail':
        mix.sass('resources/assets/sass/mail.scss', 'css/mail.css');
        mix.copy('public/css/mail.css', 'resources/views/vendor/mail/html/themes/darkly.css');
        break;
    case 'critical': {
        const criticalConfig = {
            enabled: true,
            folders: ['resources/views/layout', 'resources/views/errors'],
            extensions: ['php'],
            whitelistPatternsChildren: [
                /page-preloader.*/,
                /loader.*/,
                /error-page.*/,
                /page-index.*/,
            ],
        };
        mix.sass('resources/assets/sass/critical.scss', 'public/css').purgeCss(criticalConfig);
        break;
    }
    case 'css':
        mix.sass('resources/assets/sass/app.scss', 'public/css/app.css')
            .purgeCss(purgeCssConfig);

        if (mix.inProduction()) {
            mix.version();
        }
        break;
    default: {
        mix.webpackConfig(baseWebpackConfig)
            .js('resources/assets/js/app.js', 'public/js');

        if (mix.inProduction()) {
            mix.version(['public/css/app.css']);
        }

        break;
    }
}
