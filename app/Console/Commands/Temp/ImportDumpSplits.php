<?php

declare(strict_types=1);

namespace App\Console\Commands\Temp;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ImportDumpSplits extends Command
{
    protected $signature = 'dump:import:splits';

    public function handle(): void
    {
        $files = scandir('/root/out');

        if (!$files) {
            return;
        }

        foreach ($files as $file) {
            if (in_array($file, ['.', '..']) || mb_strlen($file) !== 2) {
                continue;
            }

            $this->handleFile('/root/out/' . $file);
        }
    }

    private function handleFile(string $file): void
    {
        shell_exec("mysql -uroot -proot -h mysql --default-character-set=utf8 -f hwmh < $file");

        unlink($file);

        $this->info($file);
    }
}
