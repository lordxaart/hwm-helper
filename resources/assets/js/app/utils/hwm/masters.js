const defaultMaster = {
    pRepair: 90,
    pRate: 100,
};

const baseRepairs = [10, 20, 30, 40, 50, 60, 70, 80, 90];

const Master = {
    baseRepairs,

    createNew: () => ({ ...defaultMaster }),

    // Return boolean
    checkMaster: (masters) => {
        let status = true;

        if (!Array.isArray(masters)) {
            masters = [masters];
        }

        if (Array.isArray(masters)) {
            masters.forEach((master) => {
                if (typeof master !== 'object') {
                    status = false;
                    return;
                }

                if (!Object.hasOwnProperty.call(master, 'pRepair') || !Object.prototype.hasOwnProperty.call(master, 'pRate')) {
                    status = false;
                }

                if (master.pRepair !== Number.parseInt(master.pRepair, 10)
                    || master.pRate !== Number.parseInt(master.pRate, 10)) {
                    status = false;
                }

                if (master.pRepair <= 0 || master.pRate <= 0) {
                    status = false;
                }

                if (baseRepairs.indexOf(master.pRepair) === -1) {
                    status = false;
                }
            });
        } else {
            console.error('Bad parametr masters. Waiting object or array');
            status = false;
        }

        return status;
    },
};

export default Master;
