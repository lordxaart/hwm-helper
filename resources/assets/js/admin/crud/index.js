/* eslint-env jquery */

global.crudTable = () => {
    try {
        return $('#crudTable') ? $('#crudTable').dataTable() : null;
    } catch (e) {
        return null;
    }
};

$('#crudTable').on('init.dt', () => {
    $('[data-toggle="tooltip"]').tooltip();

    $(this).on('draw', () => {
        $('[data-toggle="tooltip"]').tooltip();
    });
});

$('#crudTable').on('init.dt', () => {
    $('[data-toggle="tooltip"]').tooltip();

    $(this).on('draw', () => {
        $('[data-toggle="tooltip"]').tooltip();
    });
});
