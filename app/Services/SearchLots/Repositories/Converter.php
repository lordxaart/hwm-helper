<?php

declare(strict_types=1);

namespace App\Services\SearchLots\Repositories;

use App\Services\SearchLots\Enums\CompareOperator;
use App\Services\SearchLots\Enums\ColumnType;
use App\Services\SearchLots\Types\DateType;
use App\Services\SearchLots\Types\NumberType;
use App\Services\SearchLots\ValueObjects\LotFilters;
use App\Services\SearchLots\ValueObjects\OrderBy;
use Carbon\Carbon;
use InvalidArgumentException;

class Converter
{
    public const VALUE_SEPARATOR = ',';

    public function parseOrders(OrderBy $orderBy): array
    {
        $sorts = [];
        foreach ($orderBy->getOrders() as $field => $direction) {
            if (in_array($field, ['started_at', 'ended_at', 'buyed_at'])) {
                $sorts[][$field] = [
                    'order' => $direction,
                    'format' => 'yyyy-MM-dd',
                ];
            } else {
                $sorts[][$field] = $direction;
            }
        }

        return $sorts;
    }

    public function parseFilters(LotFilters $lotFilters): array
    {
        $filters = $lotFilters->getFilters();
        $timezone = $lotFilters->getTimezone();

        if (!empty($filters['lot_id'])) {
            return [
                'term' => [
                    '_id' => $filters['lot_id']['value'],
                ],
            ];
        }

        $query = [];

        foreach ($filters as $column => $item) {
            $type = $item['type'];
            $value = $item['value'];

            switch ($type) {
                case ColumnType::EQUAL->value:
                    $query['bool']['filter'][]['term'][$column] = $value;
                    break;
                case ColumnType::ONE_FROM_ARRAY->value:
                    $terms = explode(self::VALUE_SEPARATOR, (string) $value);
                    $terms = array_map('trim', $terms);

                    $query['bool']['filter'][]['terms'][$column] = $terms;
                    break;
                case ColumnType::EXISTS->value:
                    if (!is_null($value)) {
                        if ($column === 'is_buyed') {
                            $column = 'buyed_at';
                        }

                        if ($value) {
                            $query['bool']['filter'][]['exists']['field'] = $column;
                        } else {
                            $query['bool']['must_not'][]['exists']['field'] = $column;
                        }
                    }
                    break;
                case ColumnType::TEXT->value:
                    $query['bool']['must'][]['match'][$column] = $value;
                    break;
                case ColumnType::NUMBER->value:
                    $numberType = null;

                    foreach (CompareOperator::BASE_OPERATORS as $operator) {
                        if (str_starts_with($value, $operator->value)) {
                            $int = (int) ltrim($value, $operator->value);
                            $numberType = new NumberType($operator, $int);
                            break;
                        }
                    }

                    if (!$numberType && $value && is_numeric($value)) {
                        $numberType = new NumberType(CompareOperator::EQUAL, (int) $value);
                    }

                    if (!$numberType) {
                        throw new InvalidArgumentException(sprintf('Unsupported value for number field %s', $value));
                    }

                    if ($numberType->getOperator() === CompareOperator::EQUAL) {
                        $query['bool']['filter'][]['term'][$column] = $numberType->getValue();
                    } else {
                        $query['bool']['filter'][]['range'][$column] = $this->numberToEsRange($numberType);
                    }

                    break;
                case ColumnType::DATE->value:
                    $type = $this->parseDateValue($value);

                    if (!$type) {
                        throw new InvalidArgumentException(sprintf('Unsupported value for date field %s', $value));
                    }

                    $type->setTimezone($timezone);

                    if ($type->getOperator() === CompareOperator::EQUAL) {
                        $query['bool']['filter'][]['term'][$column] = $value;
                    } else {
                        $query['bool']['filter'][]['range'][$column] = array_merge(
                            $this->dateToEsRange($type),
                            ['format' => 'yyyy-MM-dd HH:mm:ss'],
                        );
                    }

                    break;
            }
        }

        return $query;
    }

    private function dateToEsRange(DateType $compare): array
    {
        $mapping = [
            CompareOperator::MORE->value => 'gt',
            CompareOperator::MORE_OR_EQUAL->value => 'gte',
            CompareOperator::LESS->value => 'lt',
            CompareOperator::LESS_OR_EQUAL->value => 'lte',
        ];

        if ($compare->getOperator() === CompareOperator::BETWEEN) {
            return [
                'gte' => $compare->getValue()[0]->format('Y-m-d H:i:s'),
                'lte' => $compare->getValue()[1]->format('Y-m-d H:i:s'),
            ];
        }

        return [
            $mapping[$compare->getOperator()->value] => $compare->getValue()->format('Y-m-d H:i:s'),
        ];
    }

    private function numberToEsRange(NumberType $compare): array
    {
        $mapping = [
            CompareOperator::MORE_OR_EQUAL->value => 'gte',
            CompareOperator::LESS_OR_EQUAL->value => 'lte',
            CompareOperator::MORE->value => 'gt',
            CompareOperator::LESS->value => 'lt',
        ];

        return [
            $mapping[$compare->getOperator()->value] => $compare->getValue(),
        ];
    }

    private function parseDateValue(string $value): ?DateType
    {
        $type = null;

        // 2021-09-14
        if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $value)) {
            $type = new DateType(CompareOperator::EQUAL, Carbon::createFromFormat('Y-m-d', $value));
        }

        // 2021-09-14,2021-09-15
        if (!$type && preg_match('/^\d{4}-\d{2}-\d{2}' . self::VALUE_SEPARATOR . '\d{4}-\d{2}-\d{2}$/', $value)) {
            $values = explode(self::VALUE_SEPARATOR, $value);
            $values = array_map(function ($date) {
                return Carbon::createFromFormat('Y-m-d', $date);
            }, $values);

            $type = new DateType(CompareOperator::BETWEEN, $values);
        }

        // <2021-09-14 | >=2021-09-15
        if (!$type) {
            foreach (CompareOperator::BASE_OPERATORS as $operator) {
                if (str_starts_with($value, $operator->value)) {
                    $date = ltrim($value, $operator->value);
                    return new DateType(
                        $operator,
                        Carbon::createFromFormat('Y-m-d', $date),
                    );
                }
            }
        }

        return $type;
    }
}
