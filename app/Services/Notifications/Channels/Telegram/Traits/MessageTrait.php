<?php

declare(strict_types=1);

namespace App\Services\Notifications\Channels\Telegram\Traits;

use App\Services\Notifications\Channels\Telegram\TelegramMessageFormatter;

trait MessageTrait
{
    protected string $messageType = 'notice';

    public function getEmojiByType(): string
    {
        if (!$this->messageType) {
            return '';
        }

        return TelegramMessageFormatter::getEmoji($this->messageType);
    }

    public function setTypeContent(string $type): self
    {
        $this->messageType = $type;

        return $this;
    }

    public function error(): self
    {
        return $this->setTypeContent('error');
    }

    public function success(): self
    {
        return $this->setTypeContent('success');
    }
}
