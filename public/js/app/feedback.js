(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["feedback"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/global/Feedback.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/components/global/Feedback.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var bootstrap_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! bootstrap-vue */ "./node_modules/bootstrap-vue/esm/index.js");
/* harmony import */ var lodash_find__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash/find */ "./node_modules/lodash/find.js");
/* harmony import */ var lodash_find__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash_find__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var JS_app_utils_notify__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! JS/app/utils/notify */ "./resources/assets/js/app/utils/notify.js");
/* harmony import */ var JS_app_config__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! JS/app/config */ "./resources/assets/js/app/config/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    'b-form-file': bootstrap_vue__WEBPACK_IMPORTED_MODULE_1__["BFormFile"]
  },

  data() {
    return {
      maxAttachments: 10,
      maxFileSize: 3000000,
      loading: false,
      name: null,
      email: null,
      message: '',
      attachments: []
    };
  },

  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])('User', ['sendFeedback'])), {}, {
    removeFile(index) {
      this.attachments.splice(index, 1);
    },

    sendRequest() {
      if (!this.canSendRequest) {
        return;
      }

      const params = {
        name: this.name,
        email: this.email,
        message: this.message,
        attachments: this.attachments,
        user_id: this.user ? this.user.id : null,
        cookie_userid: JS_app_config__WEBPACK_IMPORTED_MODULE_4__["default"].app.userid,
        source_page: window.location.href
      };
      this.loading = true;
      this.sendFeedback(params).then(() => {
        this.loading = false;
        JS_app_utils_notify__WEBPACK_IMPORTED_MODULE_3__["default"].success(this.$t('app.main.successfuly_sent'));
        this.clearFields();
        this.$emit('submit');
      }).catch(() => {
        this.loading = false;
      });
    },

    setAttachments(files) {
      files.forEach(file => {
        if (!lodash_find__WEBPACK_IMPORTED_MODULE_2___default()(this.attachments, item => item.name === file.name && item.size === file.size && item.lastModified === file.lastModified)) {
          if (file.size > this.maxFileSize) {
            JS_app_utils_notify__WEBPACK_IMPORTED_MODULE_3__["default"].warning(this.$t('app.feedback.max_file_size', {
              size: this.$options.filters.formatBytes(this.maxFileSize)
            }), this.$t('app.feedback.file_is_so_big', {
              file: file.name
            }));
            return;
          }

          this.attachments.push(file);
        }
      });

      if (this.attachments.length > this.maxAttachments) {
        this.attachments = files.splice(0, this.maxAttachments);
      }
    },

    clearFields() {
      this.name = null;
      this.email = null;
      this.message = '';
      this.attachments = [];
    },

    totalSize(files) {
      let bytes = 0;
      files.forEach(file => {
        bytes += file.size;
      });
      return bytes;
    }

  }),
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('User', {
    user: 'getUser'
  })), {}, {
    canSendRequest() {
      return this.message.length && this.attachments.length <= this.maxAttachments;
    }

  })
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/global/Feedback.vue?vue&type=style&index=0&lang=scss&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/sass-loader/dist/cjs.js??ref--6-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/components/global/Feedback.vue?vue&type=style&index=0&lang=scss& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".feedback .collapsed > .when-opened,\n.feedback :not(.collapsed) > .when-closed {\n  display: none;\n}\n.feedback .custom-file-label {\n  cursor: pointer;\n}\n.feedback .custom-file-label:after {\n  background-color: #375a7f;\n  border-color: #375a7f;\n  color: #f2f2f2;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/global/Feedback.vue?vue&type=style&index=0&lang=scss&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/sass-loader/dist/cjs.js??ref--6-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/components/global/Feedback.vue?vue&type=style&index=0&lang=scss& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../../node_modules/sass-loader/dist/cjs.js??ref--6-3!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Feedback.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/global/Feedback.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/global/Feedback.vue?vue&type=template&id=364a6010&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/assets/js/app/components/global/Feedback.vue?vue&type=template&id=364a6010& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "feedback" },
    [
      _c("loading", { attrs: { type: "block", show: _vm.loading } }),
      _vm._v(" "),
      _c("form", { ref: "form", attrs: { action: "#", method: "POST" } }, [
        _c("div", { staticClass: "form-group" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.name,
                expression: "name"
              }
            ],
            staticClass: "form-control",
            attrs: {
              type: "text",
              placeholder: _vm.$t("validation.attributes.name"),
              maxlength: "255"
            },
            domProps: { value: _vm.name },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.name = $event.target.value
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.email,
                expression: "email"
              }
            ],
            staticClass: "form-control",
            attrs: {
              type: "email",
              placeholder: _vm.$t("validation.attributes.email"),
              maxlength: "255"
            },
            domProps: { value: _vm.email },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.email = $event.target.value
              }
            }
          }),
          _vm._v(" "),
          _c("small", { staticClass: "form-text text-muted" }, [
            _vm._v(_vm._s(_vm.$t("app.feedback.for_callback")))
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("textarea", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.message,
                expression: "message"
              }
            ],
            staticClass: "form-control",
            class: {
              "is-invalid": !_vm.message.length,
              "is-valid": _vm.message.length
            },
            attrs: {
              rows: "5",
              placeholder: _vm.$t("app.feedback.message_placeholder"),
              maxlength: "10000"
            },
            domProps: { value: _vm.message },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.message = $event.target.value
              }
            }
          }),
          _vm._v(" "),
          !_vm.message.length
            ? _c("small", {
                staticClass: "form-text",
                class: {
                  "text-danger": !_vm.message.length,
                  "text-success": _vm.message.length
                },
                domProps: {
                  innerHTML: _vm._s(
                    "* " + _vm.$t("validation.required", { attribute: "" })
                  )
                }
              })
            : _vm._e()
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "form-group" },
          [
            _c(
              "b-form-file",
              {
                attrs: {
                  multiple: "",
                  accept: "image/jpeg, image/png, image/gif",
                  "browse-text": _vm.$t("app.main.browse"),
                  disabled: _vm.attachments.length >= _vm.maxAttachments
                },
                on: { input: _vm.setAttachments }
              },
              [
                _c(
                  "div",
                  { attrs: { slot: "placeholder" }, slot: "placeholder" },
                  [
                    _vm._v(
                      "\n                    " +
                        _vm._s(_vm.$t("app.feedback.attach_files")) +
                        "\n                "
                    )
                  ]
                ),
                _vm._v(" "),
                _c("div", { attrs: { slot: "file-name" }, slot: "file-name" }, [
                  _c("span", [
                    _vm._v(
                      _vm._s(
                        _vm.attachments.length >= _vm.maxAttachments
                          ? _vm.$t("app.feedback.max_count_files", {
                              max: _vm.maxAttachments
                            })
                          : _vm.$t("app.feedback.attach_files")
                      )
                    )
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    attrs: { slot: "drop-placeholder" },
                    slot: "drop-placeholder"
                  },
                  [
                    _vm._v(
                      "\n                    " +
                        _vm._s(_vm.$t("app.feedback.attach_files")) +
                        "\n                "
                    )
                  ]
                )
              ]
            ),
            _vm._v(" "),
            _vm.attachments.length
              ? _c(
                  "div",
                  [
                    _c(
                      "p",
                      {
                        directives: [
                          {
                            name: "b-toggle",
                            rawName: "v-b-toggle.feedbackAttachments",
                            modifiers: { feedbackAttachments: true }
                          }
                        ],
                        staticClass: "mt-2 mb-1 d-flex justify-content-between"
                      },
                      [
                        _c("span", [
                          _vm._v(
                            _vm._s(
                              _vm.$t("app.feedback.selected_files_with_size", {
                                files: _vm.attachments.length,
                                size:
                                  _vm.totalSize(_vm.attachments) |
                                  _vm.formatBytes
                              })
                            )
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "when-opened" },
                          [_c("icon", { attrs: { name: "arrow_up" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          { staticClass: "when-closed" },
                          [_c("icon", { attrs: { name: "arrow_down" } })],
                          1
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "b-collapse",
                      {
                        attrs: { id: "feedbackAttachments", role: "tabpanel" }
                      },
                      [
                        _c(
                          "ul",
                          { staticClass: "list-group" },
                          _vm._l(_vm.attachments, function(file, index) {
                            return _c(
                              "li",
                              {
                                key: file.name,
                                staticClass: "list-group-item",
                                class: { "bg-secondary": !(index % 2) }
                              },
                              [
                                _vm._v(
                                  "\n                            " +
                                    _vm._s(file.name) +
                                    ", " +
                                    _vm._s(_vm._f("formatBytes")(file.size)) +
                                    "\n                            "
                                ),
                                _c("icon", {
                                  attrs: { name: "close" },
                                  nativeOn: {
                                    click: function($event) {
                                      return _vm.removeFile(index)
                                    }
                                  }
                                })
                              ],
                              1
                            )
                          }),
                          0
                        )
                      ]
                    )
                  ],
                  1
                )
              : _vm._e()
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c("hr"),
      _vm._v(" "),
      _c("c-button", {
        attrs: {
          type: "primary",
          block: "",
          text: _vm.$t("app.main.sent"),
          disabled: !_vm.canSendRequest
        },
        nativeOn: {
          click: function($event) {
            return _vm.sendRequest($event)
          }
        }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/assets/js/app/components/global/Feedback.vue":
/*!****************************************************************!*\
  !*** ./resources/assets/js/app/components/global/Feedback.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Feedback_vue_vue_type_template_id_364a6010___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Feedback.vue?vue&type=template&id=364a6010& */ "./resources/assets/js/app/components/global/Feedback.vue?vue&type=template&id=364a6010&");
/* harmony import */ var _Feedback_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Feedback.vue?vue&type=script&lang=js& */ "./resources/assets/js/app/components/global/Feedback.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Feedback_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Feedback.vue?vue&type=style&index=0&lang=scss& */ "./resources/assets/js/app/components/global/Feedback.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Feedback_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Feedback_vue_vue_type_template_id_364a6010___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Feedback_vue_vue_type_template_id_364a6010___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/assets/js/app/components/global/Feedback.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/assets/js/app/components/global/Feedback.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/assets/js/app/components/global/Feedback.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Feedback_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Feedback.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/global/Feedback.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Feedback_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/assets/js/app/components/global/Feedback.vue?vue&type=style&index=0&lang=scss&":
/*!**************************************************************************************************!*\
  !*** ./resources/assets/js/app/components/global/Feedback.vue?vue&type=style&index=0&lang=scss& ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_sass_loader_dist_cjs_js_ref_6_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Feedback_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/style-loader!../../../../../../node_modules/css-loader!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../../node_modules/sass-loader/dist/cjs.js??ref--6-3!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Feedback.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/global/Feedback.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_sass_loader_dist_cjs_js_ref_6_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Feedback_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_sass_loader_dist_cjs_js_ref_6_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Feedback_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_sass_loader_dist_cjs_js_ref_6_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Feedback_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_sass_loader_dist_cjs_js_ref_6_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Feedback_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/assets/js/app/components/global/Feedback.vue?vue&type=template&id=364a6010&":
/*!***********************************************************************************************!*\
  !*** ./resources/assets/js/app/components/global/Feedback.vue?vue&type=template&id=364a6010& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Feedback_vue_vue_type_template_id_364a6010___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Feedback.vue?vue&type=template&id=364a6010& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/assets/js/app/components/global/Feedback.vue?vue&type=template&id=364a6010&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Feedback_vue_vue_type_template_id_364a6010___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Feedback_vue_vue_type_template_id_364a6010___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);