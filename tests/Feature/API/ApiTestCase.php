<?php

namespace Tests\Feature\API;

use Tests\TestCase;

class ApiTestCase extends TestCase
{
    const HOME_URL = '/api/v1';
    const API_ARTS_URL = '/api/v1/arts';
    const API_CATEGORIES_URL = '/api/v1/arts/categories';
    const API_MARKET_CATEGORIES_URL = '/api/v1/arts/marketCategories';
    const API_SETS_URL = '/api/v1/arts/sets';
    const API_REFRESH_ART_FROM_ORIGINAL_URL = '/api/v1/arts/{artId}/refreshFromOriginal';
    const API_ELEMENT_PRICES_URL = '/api/v1/market/elements';
    const API_GET_LOTS_URL = '/api/v1/market/lots';
    const API_GET_PROFILE_MONITOR_ARTS = '/api/v1/monitor/arts';
}
