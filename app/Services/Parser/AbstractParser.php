<?php

declare(strict_types=1);

namespace App\Services\Parser;

use App\HWM\Exceptions\ParserErrors\UnexpectedPage;
use App\HWM\Exceptions\RequestErrors\LoginDuringAnotherScriptLogin;
use App\Models\LotParserLog;
use App\Services\Parser\Interfaces\ParserInterface;
use Illuminate\Database\QueryException;
use Illuminate\Support\Str;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractParser implements ParserInterface
{
    public const SLEEP_ON_LOGIN_ANOTHER_SCRIPT = 5;
    public const SIZE_OF_LOGS_BATCH = 20;

    public int $maxTries = 3;

    protected ?LotRunner $currentRunner = null;
    protected ?OutputInterface $output = null;

    private ?string $serverIp;
    private float $initTime;
    private ?string $id;
    private array $logs = [];

    abstract protected function setNextRunner(): void;

    public function __construct(OutputInterface $output = null)
    {
        $this->id = uniqid($this->getType() . '.', true);

        $this->serverIp = getServerIp();
        $this->initTime = microtime(true);

        if ($output) {
            $this->setOutput($output);
        }

        $this->log("STARTED");
    }

    public function setOutput(OutputInterface $output): void
    {
        $this->output = $output;
    }

    public function getId(): string
    {
        return strval($this->id);
    }

    public function getType(): string
    {
        $class = (new \ReflectionClass(static::class))->getShortName();
        $class = str_replace('Parser', '', $class);

        return mb_strtolower(Str::snake($class));
    }

    public function getState(): string
    {
        if (!$this->currentRunner || ($this->currentRunner->hasError() && !$this->currentRunner->success)) {
            return 'error';
        }

        return 'success';
    }

    public function getExecutionTime(): float
    {
        return microtime(true) - $this->initTime;
    }

    public function captureState(): void
    {
        if (!$this->currentRunner) {
            return;
        }

        $format = LotParserLog::getModel()->getDateFormat();
        $time = now()->format($format);

        $this->logs[] = [
            'parser_id' => $this->id,
            'parser_type' => $this->getType(),
            'lot_id' => $this->currentRunner->getLotId(),
            'state' => $this->getState(),
            'attempts' => $this->currentRunner->getRunAttempts(),
            'time_execute' => $this->currentRunner->getExecuteTime(),
            'server_ip' => $this->serverIp,
            'errors' => collect($this->currentRunner->getErrors())
                ->map(function (\Throwable $item) {
                    return $item->getMessage() ?: get_class($item);
                })
                ->implode(' | '),
            'created_at' => $time,
            'updated_at' => $time,
        ];

        if (count($this->logs) >= self::SIZE_OF_LOGS_BATCH) {
            $this->saveLogs();
        }
    }

    public function saveLogs(): void
    {
        if (!count($this->logs)) {
            return;
        }

        try {
            LotParserLog::insert($this->logs);
        } catch (\Exception $e) {
            report($e);
            unset($e);
        }

        $this->log('LOGS: ' . count($this->logs));
        $this->logs = [];
    }

    public function __destruct()
    {
        $this->saveLogs();

        $this->log("ENDED");
    }

    public function run(): void
    {
        if (method_exists($this, 'beforeRun')) {
            if (!$this->beforeRun()) {
                return;
            }
        }

        $runner = $this->currentRunner;

        if (!$runner) {
            return;
        }

        $this->log("Start #{$runner->getLotId()}");

        try {
            retry($this->maxTries, function () use ($runner) {
                $runner->run();
            }, $this->getDelayBetweenRetries(), function (\Exception $e) use ($runner) {
                if ($e instanceof LoginDuringAnotherScriptLogin) {
                    sleep(self::SLEEP_ON_LOGIN_ANOTHER_SCRIPT);
                }

                if ($e instanceof QueryException && $e->getCode() == DB_UNIQUE_DUPLICATE_ERROR_CODE) {
                    $this->log('Duplicate lot id #' . $runner->getLotId());
                }

                return false;
            });
        } catch (\Throwable $e) {
            $this->handleRunException($e);

            unset($e);
        }

        $this->captureState();

        $this->log(str_replace(
            [
                ':lot_id',
                ':exec',
                ':memory'
            ],
            [
                $runner->getLotId(),
                round($runner->getExecuteTime(), 3),
                formatBytes(memory_get_usage(true)),

            ],
            "End #:lot_id ! exec: :exec memory :memory"
        ));

        $this->setNextRunner();
    }

    protected function getDelayBetweenRetries(): int
    {
        return 1;
    }

    protected function needToStop(): bool
    {
        return false;
    }

    protected function handleRunException(\Throwable $e): void
    {
        if ($e instanceof QueryException && $e->getCode() == DB_UNIQUE_DUPLICATE_ERROR_CODE) {
            return;
        }

        if ($e instanceof LoginDuringAnotherScriptLogin || $e instanceof UnexpectedPage) {
            return;
        }

//        if ($e instanceof EmptyLotPage && $this->currentRunner) {
//            EmptyLot::create(['lot_id' => $this->currentRunner->getLotId()]);
//            \Log::error('Find empty lot ' . $this->currentRunner->getLotId());
//            return;
//        }

        \Log::error($e->getMessage(), ['exception' => $e]);

        $ee = $this->currentRunner?->getLastError();
        if ($ee && spl_object_id($ee) !== spl_object_id($e)) {
            \Log::error($ee->getMessage(), ['exception' => $ee]);
        }
    }

    public function log(string $message): void
    {
        $style = 'info';

        $message = "[:time] [:id] $message";

        $message = str_replace([':time', ':id'], [now()->format('Y-m-d H:i:s.u'), $this->getId()], $message);

        if ($this->output) {
            $this->output->writeln("<$style>$message</$style>");
        }
    }
}
