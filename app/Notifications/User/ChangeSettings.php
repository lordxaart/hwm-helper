<?php

declare(strict_types=1);

namespace App\Notifications\User;

use App\Core\Enums\Queue;
use App\Services\Notifications\Channels\Telegram\Entities\TelegramMessage;
use App\Services\Notifications\Channels\Webpush\WebPushMessage;
use App\Services\Notifications\Enums\Channel;
use App\Services\Notifications\Enums\NotifyType;
use App\Services\Notifications\UserNotifyService;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use InvalidArgumentException;

class ChangeSettings extends Notification implements ShouldQueue
{
    use Queueable;

    public array $items;

    public function __construct(array $items)
    {
        $this->queue = Queue::NOTIFICATIONS->value;

        foreach ($items as $item) {
            if (!is_array($item) || count($item) !== 3) {
                throw new InvalidArgumentException('Items for ChangeSettings must be array and each must has 3 inner items');
            }
        }

        $this->items = $items;
    }

    public function via(User $user): array
    {
        return [...app(UserNotifyService::class)
            ->getConfiguredChannels($user, NotifyType::CHANGE_SETTINGS), Channel::DATABASE];
    }

    public function toMail(User $user): MailMessage
    {
        return (new MailMessage())
            ->subject(trans('app.notifications.value_for_account_changed'))
            ->markdown('emails.changed_settings', [
                'items' => $this->items,
            ]);
    }

    public function toTelegram(User $user): TelegramMessage
    {
        $content = trans('app.notifications.value_for_account_changed') . ':' . PHP_EOL;

        foreach ($this->items as $item) {
            $content .= ' - ' . trans('app.notifications.key_changed_from_to', [
                'key' => '*' . trans('validation.attributes.' . $item[0]) . '*',
                'old_value' => '*' . $item[1] . '*',
                'new_value' => '*' . $item[2] . '*'
            ]) . PHP_EOL;
        }

        return TelegramMessage::create()->content($content);
    }

    public function toArray(User $user): array
    {
        return [
            'items' => $this->items,
        ];
    }

    public function toBroadcast(User $notifiable): BroadcastMessage
    {
        return new BroadcastMessage([
            'items' => $this->items,
        ]);
    }

    public function toWebPush(): WebPushMessage
    {
        $body = '';

        foreach ($this->items as $item) {
            $body .= trans('app.notifications.key_changed_from_to', [
                'key' => trans('validation.attributes.' . $item[0]),
                'old_value' => $item[1],
                'new_value' => $item[2]
            ]) . PHP_EOL;
        }

        return (new WebPushMessage())
            ->title(trans('app.notifications.value_for_account_changed'))
            ->body($body);
    }

    public function broadcastType(): string
    {
        return 'change_settings';
    }

    protected function getSubject(): string
    {
        return trans('app.notifications.found_lot_for_filters');
    }
}
