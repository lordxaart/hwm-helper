<?php

declare(strict_types=1);

namespace App\Services\Notifications;

use App\Core\Contracts\Notifable;
use Illuminate\Notifications\RoutesNotifications;

/**
 * Notifable class with config for send notifications
 */
class PublicChannelNotifable implements Notifable
{
    use RoutesNotifications;

    public function routeNotificationForTelegram(): string
    {
        return (string)config('services.telegram-bot-api.notify_channel_id');
    }

    public function toArray(): array
    {
        return [];
    }
}
