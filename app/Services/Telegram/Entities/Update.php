<?php

declare(strict_types=1);

namespace App\Services\Telegram\Entities;

class Update
{
    public readonly int $update_id;
    public readonly Message|null $message;

    public function __construct(array $update)
    {
        $this->update_id = (int) $update['update_id'];

        $this->message = isset($update['message']) ? new Message($update['message']) : null;
    }
}
