<?php

declare(strict_types=1);

namespace App\Services\Subscriptions\Enums;

enum Subscription: string
{
    case PREMIUM = 'premium';
    case GOLD = 'gold';
    case PLATINUM = 'platinum';
}
