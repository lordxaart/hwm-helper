<?php

declare(strict_types=1);

namespace App\Services\ImageOptimize;

use Tinify\Tinify;

use function Tinify\fromFile;

class TinyOptimize implements OptimizerInterface
{
    public function optimize(string $path, int $quality): bool
    {
        Tinify::setKey(config('services.tinypng.key'));

        fromFile($path)->toFile($path);

        return true;
    }
}
