<?php

declare(strict_types=1);

namespace App\Core\Enums;

enum LaravelServiceAlias: string
{
    case LOGGER = 'log';
    case EVENT_DISPATCHER = 'events';
}
