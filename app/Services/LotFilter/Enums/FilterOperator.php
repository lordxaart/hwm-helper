<?php

declare(strict_types=1);

namespace App\Services\LotFilter\Enums;

enum FilterOperator : string
{
    case MORE_OR_EQUAL = '>=';
    case MORE = '>';
    case LESS_OR_EQUAL = '<=';
    case LESS = '<';
    case EQUAL = '==';
    case HAS = 'has';

    public function compareValue(mixed $currentValue, mixed $expectedValue): bool
    {
        return match ($this) {
            FilterOperator::MORE => $currentValue > $expectedValue,
            FilterOperator::MORE_OR_EQUAL => $currentValue >= $expectedValue,
            FilterOperator::LESS => $currentValue < $expectedValue,
            FilterOperator::LESS_OR_EQUAL => $currentValue <= $expectedValue,
            FilterOperator::EQUAL => $currentValue == $expectedValue,
            FilterOperator::HAS => $expectedValue ? !is_null($currentValue) : is_null($currentValue),
            default => throw new \InvalidArgumentException('Unsupported operator ' . $this->value),
        };
    }
}
