<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\Exceptions\BaseException;
use App\Exceptions\User\UserWasDeleted;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UsersController;
use App\Models\SocialAccount;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\RedirectResponse;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     */
    protected string $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('guest')->except(['logout', 'handleSocialRedirect']);
    }

    /**
     * @param string $social
     * @return mixed
     * @throws ValidationException
     */
    public function redirectToSocial(string $social): mixed
    {
        \Validator::make(['social' => $social], [
            'social' => 'required|social_provider',
        ])->validate();

        return \Socialite::driver($social)->redirect();
    }

    /**
     * @throws \App\Exceptions\Social\InvalidState|ValidationException
     */
    public function handleSocialRedirect(Request $request, string $social): RedirectResponse
    {
        if (\Auth::check()) {
            return (new UsersController($request))->handleConnectSocialNetwork($social);
        }

        \Validator::make(['social' => $social], [
            'social' => 'required|social_provider',
        ])->validate();

        try {
            $socialAccount = SocialAccount::firstOrCreateCustom($social);

            if ($socialAccount->user->isDeleted()) {
                throw new UserWasDeleted($socialAccount->user);
            }

            \Auth::login($socialAccount->user, true);
        } catch (BaseException $e) {
            session()->flash('error', $e->getMessage());
        }

        return redirect()->intended($this->redirectTo);
    }

    protected function validateLogin(Request $request): void
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
            //'g-recaptcha-response' => 'recaptcha',
        ]);
    }

    /**
     * @param Request $request
     * @throws UserWasDeleted
     * @throws ValidationException
     */
    protected function sendFailedLoginResponse(Request $request): void
    {
        if ($user = User::onlyTrashed()->whereEmail($request->input('email'))->first()) {
            throw new UserWasDeleted($user);
        }

        throw ValidationException::withMessages([
            $this->username() => [trans('errors.auth.failed')],
        ]);
    }

    protected function sendLockoutResponse(Request $request): void
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        throw ValidationException::withMessages([
            $this->username() => [trans('errors.auth.throttle', [
                'seconds' => $seconds,
                'minutes' => ceil($seconds / 60),
            ])],
        ])->status(Response::HTTP_TOO_MANY_REQUESTS);
    }
}
