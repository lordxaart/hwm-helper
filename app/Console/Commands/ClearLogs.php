<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\LotParserLog;
use App\Models\RequestSpeedLog;
use Illuminate\Database\Eloquent\Model;
use Storage;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class ClearLogs extends Command
{
    /** @var string  */
    protected $signature = 'logs:clear';

    /** @var string  */
    protected $description = 'Clear old and big logs files, old records from db';

    public int $deleteAdditionalBytes = 100000;

    /** @var SplFileInfo[]  */
    protected array $logFiles = [];

    public function handle(): void
    {
        $this->clearLogsFromStorage();

        $this->clearLogsFromDatabase();
    }

    public function clearLogsFromStorage(): void
    {
        foreach ((new Finder())->in(storage_path('logs'))->files() as $file) {
            $this->logFiles[] = $file;
        }

        $this->clearOldFiles();

        $this->clearBigFiles();

        $this->clearTempDirectory();
    }

    public function clearLogsFromDatabase(): void
    {
        $minDate = now()->sub(config('logging.duration_save_logs'))->format('Y-m-d');

        $models = [LotParserLog::class, RequestSpeedLog::class];

        /** @var Model $model */
        foreach ($models as $model) {
            $query = $model::where('created_at', '<', $minDate);
            $countDeleteRecordsParser = $query->count();
            $query->delete();

            $this->log("Deleted $countDeleteRecordsParser logs from " . $model);
        }
    }

    protected function clearBigFiles(): void
    {
        $maxFileSize = intval(config('logging.max_file_size'));
        $additionalBytes = $this->deleteAdditionalBytes;

        foreach ($this->logFiles as $file) {
            if ($file->isFile()) {
                $fileSize = $file->getSize();

                if ($fileSize > $maxFileSize) {
                    $realPath = $file->getRealPath();
                    if (!$realPath) {
                        throw new FileException('File real path is empty');
                    }

                    // Open log file
                    $fOriginal = fopen($realPath, 'r');

                    // Create temp log file
                    $fTemp = fopen($realPath . '.txt', 'w+');

                    if (!$fOriginal) {
                        throw new FileException('Cant open file');
                    }

                    if (!$fTemp) {
                        throw new FileException('Cant create temp file');
                    }

                    // Set seek to need file length (from end file) + $additionalBytes ()
                    fseek($fOriginal, -$maxFileSize + $additionalBytes, SEEK_END);
                    // Skipp first line
                    fgets($fOriginal);
                    //write lines to new file
                    while ($line = fgets($fOriginal)) {
                        fputs($fTemp, $line);
                    }
                    // Close file resources
                    fclose($fOriginal);
                    fclose($fTemp);
                    // Delete original log file and rename new file with this name
                    unlink($realPath);
                    rename($realPath . '.txt', $file->getFilename());

                    $this->log("Truncate $realPath log to " . formatBytes($file->getSize()));
                }
            }
        }
    }

    protected function clearOldFiles(): void
    {
        $duration = config('logging.duration_save_logs');
        $secondsToDelete = now()->diffInSeconds(now()->sub($duration));

        foreach ($this->logFiles as $file) {
            // clear only format *2020-01-01.log files, and oldest that $duration
            $fileTimeDiff = intval(now()->timestamp) - $file->getMTime();
            $realPath = $file->getRealPath();

            if (
                $realPath
                && $file->isFile()
                && $fileTimeDiff >= $secondsToDelete
            ) {
                unlink($realPath);

                $this->log("Delete {$file->getRealPath()} file");
            }
        }
    }

    protected function clearTempDirectory(): void
    {
        Storage::deleteDirectory('temp');
        Storage::makeDirectory('temp');

        $this->log('Cleanup storage temp directory');
    }
}
