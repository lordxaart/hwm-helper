// NOT USED
import i18n from 'JS/app/utils/localization';
import notify from 'JS/app/utils/notify';
import { LotToNotifyBlockShort } from './_templates';

export default (subscriptionLots) => {
    if (!window.Echo) {
        return;
    }
    window.Echo.private('lots')
        .listen('.lot.created', (e) => {
            if (!subscriptionLots.includes(e.lot.artId)) {
                return;
            }

            const title = i18n.t('app.notifications.browser.new_lot');
            const message = LotToNotifyBlockShort(e.lot);

            notify.alert(message, title);
        });
};
