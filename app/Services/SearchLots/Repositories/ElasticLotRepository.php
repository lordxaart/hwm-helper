<?php

declare(strict_types=1);

namespace App\Services\SearchLots\Repositories;

use App\Components\Elastic\ElasticClient;
use App\Components\Elastic\Enum\ResponseStatus;
use App\HWM\Entities\Collections\LotCollection;
use App\HWM\Entities\Lot;
use App\Models\Lot as LotModel;
use App\Services\SearchLots\ValueObjects\Aggregation\AggregationResponse;
use App\Services\SearchLots\ValueObjects\LimitOffset;
use App\Services\SearchLots\ValueObjects\LotFilters;
use App\Services\SearchLots\ValueObjects\OrderBy;
use App\Services\SearchLots\ValueObjects\Response;
use Elasticsearch\Common\Exceptions\ClientErrorResponseException;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class ElasticLotRepository implements LotRepository, LotAggregationsRepository
{
    public const ES_INDEX = 'lots';
    private const SEARCH_TIMEOUT = '5s';

    public ElasticClient $esClient;
    private Converter $converter;
    private AggsConverter $aggsConverter;

    public function __construct(ElasticClient $client, Converter $converter, AggsConverter $aggsConverter)
    {
        $this->esClient = $client;
        $this->converter = $converter;
        $this->aggsConverter = $aggsConverter;
    }

    public function initIndex(): bool
    {
        $res = $this->esClient->client->indices()->create([
            'index' => self::ES_INDEX,
            'body' => [
                'mappings' => $this->getMappings(),
                'settings' => [
                    'index' => [
                        'max_result_window' => 100_000,
                    ]
                ]
            ],
        ]);

        return isset($res['acknowledged']) && $res['acknowledged'];
    }

    public function addLotToIndex(Lot $lot): bool
    {
        $res = $this->esClient->client->index([
            'index' => self::ES_INDEX,
            'id' => $lot->lot_id,
            'body' => $this->mapLotToDoc($lot),
        ]);

        return isset($res['result']) && ($res['result'] === ResponseStatus::CREATED->value || $res['result'] === ResponseStatus::UPDATED->value);
    }

    /**
     * @throws ClientErrorResponseException
     */
    public function addLotBulkToIndex(Collection $lots): bool
    {
        $body = [];

        /** @var Lot $lot */
        foreach ($lots as $lot) {
            $body[] = [
                'index' => [
                    '_index' => self::ES_INDEX,
                    '_id' => $lot->lot_id,
                ]
            ];
            $body[] = $this->mapLotToDoc($lot);
        }

        $res = $this->esClient->client->bulk(['body' => $body]);

        if ($res['errors'] === true) {
            foreach ($res['items'] as $item) {
                if (isset($item['index']['error'])) {
                    throw new ClientErrorResponseException($item['index']['error']['reason']);
                }
            }
        }

        return true;
    }

    public function count(): int
    {
        $res = $this->esClient->client->count([
            'index' => self::ES_INDEX,
        ]);

        return $res['count'] ?? 0;
    }

    public function findByFilters(
        LotFilters $filters,
        LimitOffset $limitOffset,
        OrderBy $orderBy,
        bool $exactTotal = false,
    ): Response {
        $filtersArray = $this->converter->parseFilters($filters);
        $orderByArray = $this->converter->parseOrders($orderBy);

        $params = [
            'index' => self::ES_INDEX,
            'size' => $limitOffset->getLimit(),
            'from' => $limitOffset->getOffset(),
            'timeout' => self::SEARCH_TIMEOUT,

            'body' => array_filter([
                'sort' => !empty($orderByArray) ? $orderByArray : null,
                'query' => !empty($filtersArray) ? $filtersArray : null,
            ]),
        ];

        if ($exactTotal) {
            $params['track_total_hits'] = true;
        }

        $response = $this->esClient->client->search($params);

        $lotIds = [];

        if (isset($response['hits']['hits']) && count($response['hits']['hits']) > 0) {
            $lotIds = array_map(function ($hit) {
                return (int) $hit['_id'];
            }, $response['hits']['hits']);
        }

        $lots = [];
        // get lots from DB by ids and map to LotEntity
        if (!empty($lotIds)) {
            $start = microtime(true);
            $lots = LotModel::whereIn('lot_id', $lotIds)->get()->map(function (LotModel $lot) {
                return $lot->asLotEntity();
            })->toArray();
            $mysqlExec = microtime(true) - $start;
        }

        return new Response(
            new LotCollection($lots),
            $limitOffset->getLimit(),
            $limitOffset->getOffset(),
            (int) $response['hits']['total']['value'],
            [
                'elastic_exec' => (float) number_format($response['took'] / 1000, 3),
                'mysql_exec' => $mysqlExec ?? 0,
            ],
        );
    }

    // todo: create separate class for aggregations
    public function getAggregations(
        array $aggregations,
        LotFilters $filters,
    ): AggregationResponse {
        $filtersArray = $this->converter->parseFilters($filters);
        $aggregationsArray = $this->aggsConverter->parseAggregations($aggregations);

        if (empty($aggregationsArray)) {
            throw new \InvalidArgumentException('Invalid aggregations after parsing');
        }

        $params = [
            'index' => self::ES_INDEX,
            'size' => 0,
            'timeout' => self::SEARCH_TIMEOUT,

            'body' => array_filter([
                'query' => !empty($filtersArray) ? $filtersArray : null,
                'aggs' => $aggregationsArray,
            ]),
        ];

        $response = $this->esClient->client->search($params);

        return new AggregationResponse(
            $response['aggregations'] ?? [],
            $aggregations,
            (float) number_format($response['took'] / 1000, 3),
        );
    }

    private function getMappings(): array
    {
        return [
            '_source' => [
                'enabled' => false,
            ],
            'dynamic' => false,
            'properties' => [
                'lot_id' => [
                    'type' => 'integer'
                ],
                'lot_type' => [
                    'type' => 'keyword',
                ],
                'entity_type' => [
                    'type' => 'keyword',
                ],
                'entity_id' => [
                    'type' => 'keyword',
                ],
                'entity_title' => [
                    'type' => 'keyword',
                ],
                'art_uid' => [
                    'type' => 'integer',
                ],
                'art_crc' => [
                    'type' => 'keyword',
                ],
                'craft' => [
                    'type' => 'keyword',
                ],
                'current_strength' => [
                    'type' => 'integer',
                ],
                'base_strength' => [
                    'type' => 'integer',
                ],
                'quantity' => [
                    'type' => 'short',
                ],
                'price' => [
                    'type' => 'long',
                ],
                'blitz_price' => [
                    'type' => 'long',
                ],
                'price_per_fight' => [
                    'type' => 'double',
                ],
                'seller_id' => [
                    'type' => 'integer',
                ],
                'seller_name' => [
                    'type' => 'text',
                    'analyzer' => 'russian',
                    'fields' => [
                        'english' => [
                            'type' => 'text',
                        ],
                    ],
                ],
                'started_at_year' => [
                    'type' => 'short',
                ],
                'started_at' => [
                    'type' => 'date',
                    'format' => 'yyyy-MM-dd HH:mm:ss',
                ],
                'buyed_at' => [
                    'type' => 'date',
                    'format' => 'yyyy-MM-dd HH:mm:ss',
                ],
                'ended_at' => [
                    'type' => 'date',
                    'format' => 'yyyy-MM-dd HH:mm:ss',
                ],
            ],
        ];
    }

    private function mapLotToDoc(Lot $lot): array
    {
        $body = Arr::only($lot->toArray(), $this->getFields());
        $body['started_at_year'] = $lot->started_at->format('Y');

        return $body;
    }

    private function getFields(): array
    {
        return array_keys($this->getMappings()['properties']);
    }
}
